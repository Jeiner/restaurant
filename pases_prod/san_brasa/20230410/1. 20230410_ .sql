DROP TABLE IF EXISTS nube_marcacion;
CREATE TABLE nube_marcacion(
	transaction_id INT NOT NULL,
    emp_id INT NOT NULL,
    emp_code VARCHAR(100) NOT NULL,
    first_name VARCHAR(200) NULL,
    photo VARCHAR(100) NULL,
    department_id INT NULL,
    create_time DATETIME NULL,
    punch_state varchar(05),
    punch_time DATETIME NULL,
    upload_time DATETIME NULL,
    
    registrado_por VARCHAR(100),
    fecha_registro DATETIME,
    modificado_por VARCHAR(100),
    fecha_modificacion DATETIME
);

DROP TABLE IF EXISTS nube_marcacion_empleado;
CREATE TABLE nube_marcacion_empleado(
	emp_id INT NOT NULL,
    emp_code VARCHAR(100) NOT NULL,
    first_name VARCHAR(200) NULL,
    hora_entrada VARCHAR(5),
    hora_salida VARCHAR(5),
    estado bit not null,
    
    registrado_por VARCHAR(100) NULL,
    fecha_registro DATETIME NULL,
    modificado_por VARCHAR(100) NULL,
    fecha_modificacion DATETIME 
);

ALTER TABLE nube_marcacion_empleado
ADD fecha_ingreso DATETIME NULL;


ALTER TABLE caja
ADD fecha_sync_marcaciones DATETIME;

ALTER TABLE caja
ADD fecha_sync_comandas DATETIME;

------------------------------------------------------------------------------------------------

INSERT INTO validacion (validacion_id, nombre_variable, validacion, estado, comentario, valor, tipo, fecha_registro, registrado_por) VALUES
 ('015', 'url_enviar_marcaciones_nube', 'Enviar marcacionesa la nube', 0, ' ', 'http://santorini.com.pe/administracion/webservice/marcacion_api/insert_marcaciones', 'URL', '2022-09-15 00:00:00', 'SYS');

 
INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('2', 'sync_marcaciones', '1', 'A', '2022-01-26', 'jharo');




------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------


ALTER TABLE caja
ADD fecha_procesar_cpbe DATETIME;

ALTER TABLE variable
MODIFY nombre_variable varchar(50);

ALTER TABLE variable
MODIFY valor_variable varchar(150);


UPDATE cpbe_cabecera SET estadoSunat = '02' where cabecera_id >= 1 AND YEAR(fecEmision) <= 2022;



INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('3', 'facsu_generar_xml', 'http://18.221.199.123:50/facsu_sanbrasa/sin_sesion/facturacion_electronica/generar_archivos_sunat', 'A', '2023-02-09 00:00:00', 'jharo');

INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('4', 'facsu_procesar_cpbes', 'http://18.221.199.123:50/facsu_sanbrasa/sin_sesion/facturacion_electronica/procesar_boletas_facturas', 'A', '2023-02-09 00:00:00', 'jharo');

INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('5', 'sync_facsu_procesar_cpbes', '1', 'A', '2023-03-06', 'jharo');


INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('6', 'max_hora_apertura_caja', '15', 'A', '2023-03-06', 'jharo');