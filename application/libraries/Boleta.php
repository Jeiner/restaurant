<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";
 
    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
    class Boleta extends FPDF {
        public function __construct() {
            parent::__construct();
        }
        // El encabezado del PDF
        public function Header(){
           
            $this->SetFont('Arial','B',10);
            // $this->Ln('5');
            // $this->Cell(50);
            // $this->Cell(90,20,"COMPROBANTE DE PAGO",0,0,'C'); 
            // $this->Ln(15);
            // $this->SetFont('Arial','B',8);
            // $this->Cell(30);
            // $this->Cell(120,20,'INFORMACION DE CONTACTO',0,0,'C');
            // $this->Ln(10);
            $tam_letra = 8;
            // * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
            $this->Cell(10);
            $this->SetFont('Arial','B', $tam_letra+5); $this->Cell(100,6, $this->empresa,      '0',0,'L','0');
            $this->SetFont('Arial','B', $tam_letra); $this->Cell(15,6, 'R.U.C.:',           '0',0,'L','0');
            $this->SetFont('Arial','',  $tam_letra); $this->Cell(45,6, $this->RUC,      '0',0,'L','0');
            $this->Ln(6);
            $this->Cell(10);
            $this->SetFont('Arial','', $tam_letra); $this->Cell(50,6, $this->direccion, '0',0,'L','0');
            $this->Cell(50);
            $this->SetFont('Arial','B', $tam_letra); $this->Cell(60,6, 'BOLETA DE VENTA', '0',0,'L','0');
            $this->Ln(6);
            $this->Cell(10);
            $this->SetFont('Arial','', $tam_letra); $this->Cell(50,6, $this->ubigeo, '0',0,'L','0');
            $this->Cell(50);
            $this->SetFont('Arial','B', $tam_letra); $this->Cell(60,6, utf8_decode('___- ______'), '0',0,'L','0');
            $this->Ln(9);
            $this->Cell(10);
            $this->SetFont('Arial','B', $tam_letra); $this->Cell(15,6, 'Sr.(es):',      '0',0,'L','0');
            $this->SetFont('Arial','', $tam_letra); $this->Cell(25,6, $this->cliente_nombre,   '0',0,'L','0');
            $this->Cell(60);
            $this->SetFont('Arial','B', $tam_letra); $this->Cell(15,6, 'Fecha: ',      '0',0,'L','0');
            $this->SetFont('Arial','', $tam_letra); $this->Cell(25,6, $this->fecha_pago,   '0',0,'L','0');
            $this->Ln(6);
            $this->Cell(10);
            $this->SetFont('Arial','B', $tam_letra); $this->Cell(15,6, utf8_decode('Dirección'),      '0',0,'L','0');
            $this->SetFont('Arial','', $tam_letra); $this->Cell(25,6, $this->cliente_direccion,   '0',0,'L','0');
            $this->Cell(60);
            // $this->SetFont('Arial','B', $tam_letra); $this->Cell(20,6, 'Documento: ',      '0',0,'L','0');
            // $this->SetFont('Arial','', $tam_letra); $this->Cell(25,6, $this->cliente_documento,   '0',0,'L','0');
            // $this->Ln(10);
       }
        // El pie del pdf
        public function Footer(){
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
        }
    }
?>