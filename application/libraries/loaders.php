<?php

class Loaders {
	public function verifica_sesion(){
        $instance = & get_instance();
        if($instance->session->userdata('session_usuario') === null){
            redirect(base_url());
            // $data['success'] = false;
            // $data['mensaje'] = "Error inesperado, Debe iniciar sesión.";
            // echo json_encode($data); exit();
        }
        // if($this->session->userdata('logueado'))
        // $fechaGuardada = $CI->session->userdata('ses_usuario'); 
        // $ahora = date("Y-n-j H:i:s"); 
        // $tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada)); 
        
        // if($CI->session->userdata($UserSesion)){
        //     $usuario = $CI->session->userdata($UserSesion);
        //     if (!$CI->session->userdata('SES_Usuario')) {
        //         redirect(base_url());
        //     }
        // }
    }
    public function verifica_sesion_ajax(){
        $instance = & get_instance();
        if($instance->session->userdata('session_usuario') === null){
            $instance->session->sess_destroy();
            $data['success'] = false;
            $data['mensaje'] = "Error inesperado, Debe iniciar sesión.";
            echo json_encode($data); exit();
        }
    }

    // public function verificaLogin($UserSesion){
    //     $CI = & get_instance();
    //     $usuario = $CI->session->userdata($UserSesion);
    //     if ($CI->session->userdata('ses_usuario')) {
    //         redirect(base_url().'acceso/dashboard');
    //     }
    // }

    // generamos la sesion con los datos del usuario
     public function iniciar_sesion($data){
        $instance = & get_instance();
        // $data = $instance->session->userdata();
        // $instance->session->unset_userdata($data);
        // echo json_encode($instance->session->userdata());exit();
        foreach ($data as $key => $value) {
            $instance->session->set_userdata($key,$value);
        }
    }

    public function cerrar_sesion(){
        $instance = & get_instance();
        $instance->session->sess_destroy();
        redirect(base_url());
    }
}