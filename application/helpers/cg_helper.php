<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Application specific global variables
class CG
{
    //..::::::::::::::::::::::::::::::::::::::::::  MENSAJES  ::::::::::::::::::::::::::::::::::::::::::..
    public const M_DebeSeleccionarItem = "Debe seleccionar ítem";
    //Comandas
    public const M_ComandaNoEncontrada = "Error, no se encontró la comanda, por favor comuníquese con el área de soporte.";
    public const M_NoSeRealizaPagoPorComandaPagada = "No se realizó el pago, debido a que la comanda ya está pagada.";
    public const M_SeDebePagarLaTotalidad = "Pago no válido, se debe pagar la totalidad.";
    //Pagos
    public const M_PagoConTarjetaDebeSerExacto = "No se realizó el pago. Cuando se realiza un pago con tarjeta, el pago debe ser exacto.";
    //..::::::::::::::::::::::::::::::::::::::::::  CONTANTES  ::::::::::::::::::::::::::::::::::::::::::..
    public const C_Todos = "Todos";
    public const C_Todas = "Todas";
    public const C_SeleccionarTodo ="*";

    //..::::::::::::::::::::::::::::::::::::::::::  MULTITABLA  ::::::::::::::::::::::::::::::::::::::::::..
    //Facturación electrónica
    public const M_88_Santorini = "Factura electrónica";
    public const M_89_Santorini = "Boleta electrónica";

    // COMANDA
    //Estado de items  de comanda (Productos seleccionados)    
    public const M_39_ItemEnEspera = "E";
    public const M_39_ItemEnPreparacion = "P";
    public const M_39_ItemDespachado = "D";
    public const M_39_ItemAnulado = "X";
    //ESTADO DE ATENCIÓN de comanda
    public const M_4_AtencionEnEspera ='E';
    public const M_4_AtencionAtendido ='A';
    public const M_4_AtencionFinalizado ='F';
    public const M_4_AtencionAnulado ='X';
    //ESTADO DE PAGO  de comanda
    public const M_5_PagoPendiente = 'P';
    public const M_5_PagoCancelado = 'C';
    public const M_5_PagoAnulado = 'X';
}