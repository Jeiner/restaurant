<?php 
    $days_dias = array(
    'Monday'=>'LU',
    'Tuesday'=>'MA',
    'Wednesday'=>'MI',
    'Thursday'=>'JU',
    'Friday'=>'VI',
    'Saturday'=>'SA',
    'Sunday'=>'DO'
    );

    $array_meses = array(
    'January'=>'Enero',
    'February'=>'Febrero',
    'March'=>'Marzo',
    'April'=>'Abril',
    'May'=>'Mayo',
    'June'=>'Junio',
    'July'=>'Julio',
    'August'=>'Agosto',
    'September'=>'Setiembre',
    'October'=>'Octubre',
    'November'=>'Noviembre',
    'December'=>'Diciembre'
    );
?>


<div class="row form-group">
    <div class="col-sm-12">
        <label class="page-title">
            <strong>
                Mis marcaciones
            </strong>
        </label>
        <hr>
    </div>
</div>


<div class="row">
    <div class="col-sm-12 text-center" style="margin-bottom: 10px; margin-top: -10px;">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <?php
                $fecha_inicio = $dFechaInicio;
            ?>
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' -2 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' -2 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' -2 month'))]; ?> <br>
                    <?= date('Y', strtotime($fecha_inicio . ' -2 month')); ?>
                </button>

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' -1 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' -1 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' -1 month'))]; ?> <br>
                    <?= date('Y', strtotime($fecha_inicio . ' -1 month')); ?> 
                </button>

                <button class="btn-mes btn-mes-active">
                    <?= $array_meses[date('F', strtotime($fecha_inicio))]; ?> <br>
                    <?= date('Y', strtotime($fecha_inicio)); ?>
                </button>

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' +1 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' +1 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' +1 month'))]; ?>  <br>
                    <?= date('Y', strtotime($fecha_inicio . ' +1 month')); ?> 
                </button>

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' +2 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' +2 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' +2 month'))]; ?>  <br>
                    <?= date('Y', strtotime($fecha_inicio . ' +2 month')); ?> 
                </button>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <?php foreach ($lstMarcaciones as $key => $oMarcacion): ?>
        <div class="col-sm-2 col-xs-4">
            <div style="" class="card-marcacion-foto">
                <div class="text-center">
                    <img src="<?= $oMarcacion->foto ?>" style="width: 100%">
                </div>
                <div class="card-marcacion-calendar">
                    <span>
                        <i class="menu-icon fa fa-calendar"></i>
                        <?= date('d-m-Y',strtotime($oMarcacion->fecha)) ?>
                    </span>
                    <br>
                    <span>
                        <i class="menu-icon fa fa-clock-o"></i>
                        <?= $oMarcacion->hora ?>
                    </span>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
<br>