


<?php 
    $days_dias = array(
    'Monday'=>'LU',
    'Tuesday'=>'MA',
    'Wednesday'=>'MI',
    'Thursday'=>'JU',
    'Friday'=>'VI',
    'Saturday'=>'SA',
    'Sunday'=>'DO'
    );

    $array_meses = array(
    'January'=>'enero',
    'February'=>'febrero',
    'March'=>'marzo',
    'April'=>'abril',
    'May'=>'mayo',
    'June'=>'junio',
    'July'=>'julio',
    'August'=>'agosto',
    'September'=>'setiembre',
    'October'=>'octubre',
    'November'=>'noviembre',
    'December'=>'diciembre'
    );
?>

<div class="row form-group">
    <div class="col-sm-12">
        <label class="page-title">
            <strong>
                Registro de asistencia
            </strong>
        </label>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div style="display: flex; justify-content: space-between; max-width: 380px; padding: 0px 0px; ">
            <div id="fecha" style="text-align: left;">
                <?php 
                    $diaActual = date("");
                ?>
                <?= date("d")." ".$array_meses[date("F")]." ".date("Y") ?>
            </div>
            <div id="reloj" style="text-align: right;"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div style="display: flex; justify-content: space-between; max-width: 380px; padding: 0px 0px; text-align: center;">
            <video id="video" width="380" height="280" autoplay></video>
        </div>
    </div>
</div>
<br>
<form method="Post" action="<?= base_url('perfil/marcaciones/guardar_marcacion') ?>" class="form-horizontal">
    <div class="row" hidden>
        <div class="col-sm-12">
            <input type="text" name="txt_empleado_id" id="txt_empleado_id" value="<?= $oEmpleado->empleado_id ?>">
            <input type="text" name="txt_nombres" id="txt_nombres" value="<?= $oEmpleado->nombre_completo ?>">
            <input type="text" name="txt_fecha" id="txt_fecha" value="<?= date('Y-m-d') ?>">
            <input type="text" name="txt_hora" id="txt_hora">
            <textarea name="txt_foto" id="txt_foto"></textarea>
        </div>
    </div>
    <button id="captureButton" class="btn btn-primary btn-sm" disabled>Registrar asistencia</button>
</form>
    
<br>
<canvas id="canvas" style="display: none;"></canvas>
<!-- <img id="foto" src="" alt="Foto tomada" width="380" height="280"> -->
<script>
    // Obtener elementos del DOM
    const videoElement = document.getElementById("video");
    const captureButton = document.getElementById("captureButton");
    const canvas = document.getElementById("canvas");
    let flag_pause_reloj = false;
    // const fotoElement = document.getElementById("foto");

    let mediaStream = null;

    // Función para iniciar la cámara
    async function startCamera() {
        try {
            // Solicitar acceso a la cámara
            mediaStream = await navigator.mediaDevices.getUserMedia({ video: true });
            // Conectar el flujo de medios al elemento de video
            videoElement.srcObject = mediaStream;
            // Habilitar el botón de tomar foto
            captureButton.disabled = false;
        } catch (err) {
            console.error("Error al acceder a la cámara:", err);
        }
    }

    // Función para tomar una foto
    function takePhoto() {
        // Obtener el contexto 2D del canvas
        const context = canvas.getContext("2d");
        // Establecer el tamaño del canvas igual al del video
        canvas.width = videoElement.videoWidth;
        canvas.height = videoElement.videoHeight;
        // Dibujar el frame actual del video en el canvas
        context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
        // Obtener la imagen del canvas como una URL en formato Data URL
        const imageUrl = canvas.toDataURL("image/png");
        // Establecer la imagen en el elemento <img>
        // fotoElement.src = imageUrl;

        //Pausar video
        videoElement.pause();
        $("#txt_foto").val(imageUrl);
        flag_pause_reloj = true;
    }

    // Asociar eventos a los botones
    startCamera();
    captureButton.addEventListener("click", takePhoto);
</script>

<script>
    // Función para actualizar el reloj
    function actualizarReloj() {
        if(flag_pause_reloj){
            return true;
        }
        const ahora = new Date();
        let horas = ahora.getHours();
        let minutos = ahora.getMinutes();
        let segundos = ahora.getSeconds();

        // Agregar un 0 antes de los números de un solo dígito
        horas = horas < 10 ? "0" + horas : horas;
        minutos = minutos < 10 ? "0" + minutos : minutos;
        segundos = segundos < 10 ? "0" + segundos : segundos;

        // Mostrar la hora en formato HH:MM:SS
        const tiempo = horas + ":" + minutos + ":" + segundos;
        document.getElementById("reloj").innerText = tiempo;
        $("#txt_hora").val(tiempo);
    }

    // Actualizar el reloj cada segundo
    setInterval(actualizarReloj, 1000);

    // Llamar a la función una vez al cargar la página para mostrar la hora inmediatamente
    actualizarReloj();
</script>


