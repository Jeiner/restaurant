
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#ventas_mensual">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Gráfico de ventas mensual
            </a>
        </li>
        <li>
            <a  href="<?= base_url('reportes/admin/reporte_ventas_diarias') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Gráfico de ventas diario
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="ventas_mensual" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-12" id="tabla_cierres">
                    <div id="container_ventas_mensuales" style="min-width: 310px; height: 400px; margin: 0 auto">

					</div>
                </div>
            </div> 
        </div>
        <br>
    </div>
</div>


<script type="text/javascript">
	crear_grafico_mensual();

	function crear_grafico_mensual(){
	    // var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Septiembre','Octubre', 'Noviembre', 'Diciembre']
	    var array_meses = ['Ene','Feb', 'Mar', 'Abr','May','Jun','Jul', 'Ago', 'Sep','Oct', 'Nov', 'Dic']
	    var fecha = new Date();
	    var anioActual = fecha.getFullYear();
	    
	    var array_ventas = new Array(12);
	    var array_productos = new Array(12);

	   
	    for (var i = 0; i < 12; i++) {
	        array_ventas[i] = 0;
	    }
	    for (var i = 0; i < 12; i++) {
	        array_productos[i] = 0;
	    }
	    $.ajax({
	        type: 'POST',
	        data: "anio="+anioActual,
	        url: '<?= base_url().'reportes/admin/ventas_mensuales' ?>',
	        success: function(response){
	            array_count = JSON.parse(response);
	            array_count.forEach(function(o){
	               array_ventas[o.mes-1] = parseFloat(o.importe);
	               array_productos[o.mes-1] = parseFloat(o.productos);
	            });
	            dibujar_grafico_mensual(anioActual,array_meses,array_ventas,array_productos);
	        },
	        error: function(response){
	            var msj = JSON.stringify(response);
	            alert("Error en la operación.\n\n"+msj);
	        }
	    }); 
	}
	function dibujar_grafico_mensual(anioActual,array_meses,array_ventas,array_productos){
	    Highcharts.chart('container_ventas_mensuales', {
	        chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Ventas mensuales del año '+anioActual
	        },
	        subtitle: {
	            text: ''
	        },
	        xAxis: {
	            categories: array_meses,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: ''
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series:[{
	            name: 'Monto de ventas (S/.)',
	            data: array_ventas,

	        },{
	            name: 'N° de productos (Unidades)',
	            data: array_productos,
	            visible:false
	        }]
	    }); 
	}
</script>