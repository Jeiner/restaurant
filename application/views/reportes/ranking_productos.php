
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#ranking_productos">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Ranking de productos
            </a>
        </li>
        <li>
            <a href="<?= base_url('reportes/admin/historico_por_producto') ?>">
                <i class="ace-icon fa fa-line-chart bigger-90" aria-hidden="true"></i>
                Evolución histórica por producto
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="ranking_productos" class="tab-pane in active">
            <div class="alert alert-info">
                A continuación se muestra el ranking de los <strong style="font-size: 15px">10 productos más vendidos</strong>. <br>
                Para visualizar el ranking seleccione una fecha de inicio, fin y pulse el boton "Filtrar".
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha inicio </label>
                        <input type="date" class="form-control input-sm " name="start_date" id="start_date"  value="<?=  date('Y-m-d') ?>" onchange="verify_start_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha fin </label>
                        <input type="date" class="form-control input-sm " name="end_date" id="end_date"  value="<?=  date('Y-m-d') ?>" onchange="verify_end_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" style="margin-top: 23px;">
                        <button class="btn btn-primary btn-xs btn-block" id="filtrar" onclick="ver_ranking();">
                            Ver ranking
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <div class="row" >
                <div class="col-sm-8">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="center"> <strong>N° </strong> </th>
                                <th class="center"> <strong>Producto   </strong> </th>
                                <th class="center"> <strong>Ventas </strong> </th>
                                <th class="center"> <strong>Importe (S/.)  </strong> </th>
                                <th class="center"> <strong>% Ventas   </strong> </th>
                            </tr>
                        </thead>
                        <tbody id="contenedor_ranking_productos">
                            
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
        <br>
    </div>
</div>

<script type="text/javascript">
    ver_ranking();

    function date_diff(start_date_AMD, end_date_AMD,tipe){
        var start_date = new Date(start_date_AMD).getTime();
        var end_date    = new Date(end_date_AMD).getTime();
        var diff = end_date - start_date;
        return diff/(1000*60*60*24);
    }
    function verify_end_date(){
        // alert();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date === undefined || start_date == ""){
            $('#end_date').parent(".form-group").addClass("has-error");            
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            $('#end_date').parent(".form-group").addClass("has-error");
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
    }
    function verify_start_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(end_date === undefined || end_date == ""){
            return true;
        }else{
            array_start_date = start_date.split("/");
            array_end_date = end_date.split("/");
            start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
            end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
            start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
            var dias = date_diff(start_date, end_date, 'days');
            if(dias < 0 ){
                $('#end_date').parent(".form-group").addClass("has-error");
                return false;
            }else{
                $('#end_date').parent(".form-group").removeClass("has-error");
            }
        }
    }
    function ver_ranking(){
        $('#contenedor_ranking_productos').html('<span class="blue bolder" style="margin: 15px;">Cargando ranking...</span>');
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        // abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('reportes/admin/ranking_productos_by_fechas')?>",
            data: {"start_date":start_date, "end_date":end_date},
            success: function(rpta){
                lista_productos = JSON.parse(rpta);
                // alert(lista_productos.length);
                dibujar_tabla(lista_productos);
            },
            error: function(rpta){
                alert("Error en la operación");
                // cerrarCargando();
            }
        });
    }
    function dibujar_tabla(lista_productos){
        var tabla_html = "";
        var cont = 0;
        var total = 0;
        lista_productos.forEach(function(o){
            total += parseFloat(o.importe);
        });
        lista_productos.forEach(function(o){
            cont++;
            if (cont > 10){
                return false;
            }else{
                tabla_html += "<tr>\
                                    <td class='center'>" + cont + "</td>\
                                    <td class='left'>" + o.producto + "</td>\
                                    <td class='center'>" + o.cantidad + "</td>\
                                    <td class='dinero'> S/. " + o.importe + "</td>\
                                    <td class='dinero'>" + parseFloat((o.importe*100)/total).toFixed(2) + " % </td>\
                                </tr>\
                            ";
            }
                
        });
        $('#contenedor_ranking_productos').html(tabla_html);
    }
</script>