
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#mozos_mensual">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Ranking de mozos mensual
            </a>
        </li>
        <li>
            <a  href="<?= base_url('reportes/admin/reporte_ventas_diarias') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Ranking de mozos diario
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="mozos_mensual" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-2 col-sm-offset-5 center">
                    <select class="form-control input-sm">
                        <option><?= date('Y') ?></option>
                    </select>
                </div>
            </div>
            <br>
            <div class="row" id="contenedor_tablas">
                
            </div> 
        </div>
        <br>
    </div>
</div>
<script type="text/javascript">
    var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Setiembre','Octubre', 'Noviembre', 'Diciembre']
    var fecha_actual = new Date();
    mes = fecha_actual.getMonth()+1; //0,1,...
    dia = fecha_actual.getDate();
    anio = fecha_actual.getFullYear();
    cargar_reporte(anio);
    function cargar_reporte(anio){
        $.ajax({
            type: 'POST',
            data: "anio="+anio,
            url: '<?= base_url().'reportes/admin/ventas_mozos_mensual' ?>',
            success: function(rpta){
                lista_mozos = JSON.parse(rpta);
                dibujar_tabla(mes, lista_mozos);
            },
            error: function(rpta){
                var msj = JSON.stringify(rpta);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }
    function dibujar_tabla(mes, lista_mozos){
        for (var i = 1; i <= mes ; i++) {
            tabla_html = "<div class='col-sm-4'>\
                            <table class='table table-bordered'>\
                                <thead>\
                                    <tr class='center' style='background: #7DB4D8'; color:'#FFF'; font-weight: bold>\
                                        <th colspan='3' class='center'><strong>" + array_meses[i-1] + "</strong></th>\
                                    </tr>\
                                    <tr>\
                                        <th> Mozo</th>\
                                        <th> Total vendido</th>\
                                        <th> N° comandas</th>\
                                    </tr>\
                                </thead>\
                                <tbody>";
            lista_mozos.forEach(function(o){
                if (o.mes == i) {
                    tabla_html += "<tr>\
                                        <td>" + o.nombre_completo + "</td>\
                                        <td class='dinero'>" + o.importe + "</td>\
                                        <td class='center'>" + o.nro_comandas + "</td>\
                                    </tr>\
                                ";
                }
            });
            tabla_html = tabla_html + "\
                                </tbody>\
                            </table>\
                        </div>";
            $('#contenedor_tablas').append(tabla_html);
        }
    }
</script>
