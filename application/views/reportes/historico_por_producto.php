
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="">
            <a href="<?= base_url('reportes/admin/ranking_productos') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Ranking de productos
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#evolucion_historica" >
                <i class="ace-icon fa fa-line-chart bigger-90" aria-hidden="true"></i>
                Evolución histórica por producto
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="row">
            <div class="col-sm-4">
                <select class="form-control chosen-select input-sm" id="producto_id" name="producto_id" onchange="crear_grafico_mensual();">
                    <option value="0">-- Seleccionar producto --</option>
                    <?php foreach ($productos as $key => $producto): ?>
                        <option value="<?= $producto->producto_id ?>"><?= $producto->producto ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-sm-2">
                <select class="form-control chosen-select input-sm" id="anual" name="anual" onchange="crear_grafico_mensual();">
                    <option value="2018" selected="selected">2018</option>
                    <option value="2019">2019</option>
                </select>
            </div>
        </div>
        <div id="evolucion_historica" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-12" id="tabla_cierres">
                    <div id="container_grafico_historico" style="min-width: 310px; height: 400px; margin: 0 auto">

                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.chosen-select').chosen({allow_single_deselect:true}); 
</script>

<script type="text/javascript">
    crear_grafico_mensual();

    function crear_grafico_mensual(){
        var producto_id = $('#producto_id').val();
        var anual = $('#anual').val();
        if (producto_id == '0' || producto_id == ''){
            return false;
        }
        // var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Septiembre','Octubre', 'Noviembre', 'Diciembre']
        var array_meses = ['Ene','Feb', 'Mar', 'Abr','May','Jun','Jul', 'Ago', 'Sep','Oct', 'Nov', 'Dic']
        // var fecha = new Date();
        var anioActual = anual
        
        var array_ventas = new Array(12);
        var array_productos = new Array(12);

       
        for (var i = 0; i < 12; i++) {
            array_ventas[i] = 0;
        }
        for (var i = 0; i < 12; i++) {
            array_productos[i] = 0;
        }
        $.ajax({
            type: 'POST',
            data: {"producto_id" : producto_id, "anual" : anioActual },
            url: '<?= base_url().'reportes/admin/historico_producto_by_mes' ?>',
            success: function(response){
                array_count = JSON.parse(response);
                array_count.forEach(function(o){
                   array_ventas[o.mes-1] = parseFloat(o.importe);
                   array_productos[o.mes-1] = parseFloat(o.cantidad);
                });
                dibujar_grafico_mensual(anioActual,array_meses,array_ventas,array_productos);
            },
            error: function(response){
                var msj = JSON.stringify(response);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }
    function dibujar_grafico_mensual(anioActual,array_meses,array_ventas,array_productos){
        Highcharts.chart('container_grafico_historico', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Histórico para el año '+anioActual
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: array_meses,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series:[{
                name: 'Monto de ventas (S/.)',
                data: array_ventas,

            },{
                name: 'N° de productos (Unidades)',
                data: array_productos,
                visible:false
            }]
        }); 
    }
</script>