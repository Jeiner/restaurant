<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('reportes/admin/reporte_ventas_mensual') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Gráfico de ventas mensual
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#ventas_diario">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Gráfico de ventas diario
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="ventas_diario" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-12" id="tabla_cierres">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">
    
					</div>		
                </div>
            </div> 
        </div>
        <br>
    </div>
</div>


<script type="text/javascript">
	crear_grafico_diario();
    function cant_ds(mes,ano){ 
        di=28;
        f = new Date(ano,mes-1,di); 
        while(f.getMonth() == mes-1){ 
            di++;
            f = new Date(ano,mes-1,di); 
        } 
        return di-1; 
    } 
    function crear_grafico_diario(){
        var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Setiembre','Octubre', 'Noviembre', 'Diciembre']
        var fecha = new Date();
        var anioActual = fecha.getFullYear();
        var mesActual = fecha.getMonth()+1;
        // alert(mesActual);
        var cant_dias = cant_ds(mesActual,anioActual);
        var array_dias = new Array(cant_dias);
        var array_ventas = new Array(cant_dias);
        var array_productos = new Array(cant_dias);

        for (var i = 0; i < cant_dias; i++) {
            array_dias[i] = i+1;
        }
        for (var i = 0; i < cant_dias; i++) {
            array_ventas[i] = 0;
        }
        for (var i = 0; i < cant_dias; i++) {
            array_productos[i] = 0;
        }
        $.ajax({
            type: 'POST',
            data: 'mes='+mesActual+"&anio="+anioActual,
            url: '<?= base_url().'reportes/admin/ventas_diarias' ?>',
            success: function(response){
                array_count = JSON.parse(response);
                array_count.forEach(function(o){
                   array_ventas[o.dia-1] = parseFloat(o.importe);
                   array_productos[o.dia-1] = parseFloat(o.productos);
                });
                dibujar_grafico_diario(array_meses[mesActual-1],array_dias,array_ventas,array_productos);
            },
            error: function(response){
                var msj = JSON.stringify(response);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }
    function dibujar_grafico_diario(mes,array_dias,array_ventas,array_productos){
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Ventas diarias del mes de '+mes
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: array_dias,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series:[{
                name: 'Monto de ventas (S/.)',
                data: array_ventas,

            },{
                name: 'N° de productos (Unidades)',
                data: array_productos,
                visible:false
            }]
        }); 
    }
</script>