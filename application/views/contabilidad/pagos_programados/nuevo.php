
<?php $meses = array('enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre'); ?>
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('contabilidad/pagos_programados') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Pagos programados
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#nuevo_cronograma">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nueva programación de pagos
            </a>
        </li>
    </ul>
    <div class="tab-content">
    	<form method="Post" action="<?= base_url('contabilidad/pagos_programados/guardar') ?>" class="form-horizontal"  id="form_programacion">
	        <div id="nuevo_cronograma" class="tab-pane active">
	            <div class="row">
					<div class="col-sm-5">
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Año <label class="red">*</label> </label>
							<div class="col-sm-4 col-xs-6">
								<input type="text" name="anual" id="anual" class="form-control input-sm" value="<?= date('Y') ?>" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Sede <label class="red">*</label> </label>
							<div class="col-sm-8 col-xs-12">
								<select class="form-control chosen-select" id="sede_id" name="sede_id">
									<option value=""> Seleccionar...</option>
									<?php foreach ($sedes as $key => $sed): ?>
										<option value="<?= $sed->sede_id ?>"><?= $sed->sede ?></option>						
									<?php endforeach ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Proveedor <label class="red">*</label> </label>
							<div class="col-sm-8 col-xs-12">
								<select class="form-control chosen-select" id="proveedor_id" name="proveedor_id">
									<option value=""> Seleccionar...</option>
									<?php foreach ($proveedores as $key => $prov): ?>
										<option value="<?= $prov->proveedor_id ?>"><?= $prov->razon_social ?> - <?= $prov->RUC ?></option>						
									<?php endforeach ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Servicio <label class="red">*</label></label>
							<div class="col-sm-8 col-xs-12">
								<textarea class="form-control" id="descripcion" name="descripcion"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Monto previsto <label class="red">*</label> </label>
							<div class="col-sm-4 col-xs-12">
								<input type="text" name="monto_previsto" id="monto_previsto" class="form-control input-sm" 
									onkeypress="return soloNumeroDecimal(event);">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Día alerta <label class="red">*</label></label>
							<div class="col-sm-4 col-xs-6">
								<input type="text" name="dia_alerta" id="dia_alerta" class="form-control input-sm" 
									onkeypress="return soloNumeroEntero(event);"
									onblur=" if(this.value > 28 ){ alert('Ingrese un valor menor a 28');this.value = ''" }
									>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 col-xs-12 control-label" for=""> Observaciones </label>
							<div class="col-sm-8 col-xs-12">
								<textarea class="form-control" id="observaciones" name="observaciones"></textarea>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-sm-offset-1">
						<div class="row ">
							<div class="col-sm-12">
								<span class="blue bolder "> Meses </span>
							</div>
							<hr>
						</div>
						<div class="row">
							<div class="col-sm-6 col-xs-6">
								<?php for ($i = 0; $i < sizeof($meses); $i++): ?>
									<?php if ($i <= 5): ?>
										<div class="checkbox">
											<label>
												<input value='<?= ($i+1) ?>' type='checkbox' class='ace' name="mes_id[]" >
												<span class="lbl"> <?= strtoupper($meses[$i]) ?> </span>
											</label>
										</div>
									<?php endif ?>
								<?php endfor ?>
							</div>
							<div class="col-sm-6 col-xs-6">
								<?php for ($i = 0; $i < sizeof($meses); $i++): ?>
									<?php if ($i >= 6): ?>
										<div class="checkbox">
											<label>
												<input value='<?= ($i+1) ?>' type='checkbox' class='ace' name="mes_id[]" >
												<span class="lbl"> <?= strtoupper($meses[$i]) ?> </span>
											</label>
										</div>
									<?php endif ?>
								<?php endfor ?>
							</div>
						</div>
					</div>
				</div>
				<br>
				<br>
				<div class="row">
					<div class="col-sm-2 col-sm-offset-7">
						<button class="btn btn-primary btn-sm btn-block" id="btn_guardar">
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Guardar cronograma
						</button>
					</div>
				</div>
	        </div>
        </form>
    </div>
</div>
<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true}); 
</script>

<script type="text/javascript">
	$("#form_programacion").submit(function() {
		var sede_id = $('#sede_id').val();
		var proveedor_id = $('#proveedor_id').val();
		if (sede_id == "") {
			alert("Debe seleccionar una sede");
			return false;
		}
		if (proveedor_id == "") {
			alert("Debe seleccionar un proveedor");
			return false;
		}
		if ($("#form_programacion").valid()) {
			$('#btn_guardar').attr('disabled', true);
		}else{
			$('#btn_guardar').attr('disabled', false);
		}

    });
	$("#form_programacion").validate({
        rules: {
            sede_id: "required",
            proveedor_id: "required",
            monto_previsto: "required",
            dia_alerta: "required"
        },
        messages:{
        	sede_id: "Seleccione una sede",
            proveedor_id: "Seleccione un proveedor",
            monto_previsto: "Ingrese un monto ",
            dia_alerta: "Ingrese dia de alerta"
        }
    });
</script>