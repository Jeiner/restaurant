<form method="Post" action="<?= base_url('contabilidad/pagos_programados/guardar_pago_de_programado') ?>" class="form-horizontal"  id="form_pago_de_programado">
    <div class="row">
    	<div class="col-sm-5">
    		<div class="panel panel-default">
                <div class="panel-header">
                    <div class="panel-title">
                       Pago programado
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Pago programado ID </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="pago_programado_id" id="pago_programado_id" value="<?= $oPago_programado->pago_programado_id ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Fecha prevista </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="fecha_prevista_pago" id="fecha_prevista_pago" value="<?= $oPago_programado->fecha_prevista_pago ?>">
                        </div>
                    </div>
                    <div class="form-group" hidden="hidden">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> sede_id </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="sede_id" id="sede_id" value="<?= $oPago_programado->sede_id ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Sede </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="sede" id="sede" value="<?= $oPago_programado->sede_desc ?>">
                        </div>
                    </div>
                    <div class="form-group" hidden="hidden">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> proveedor_id </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="proveedor_id" id="proveedor_id" value="<?= $oPago_programado->proveedor_id ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Proveedor </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="proveedor" id="proveedor" value="<?= $oPago_programado->razon_social ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Servicio </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="descripcion" id="descripcion" value="<?= $oPago_programado->descripcion ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Monto previsto </label>
                        <div class="col-sm-8 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="monto_previsto" id="monto_previsto" value="<?= $oPago_programado->monto_previsto ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Observaciones </label>
                        <div class="col-sm-8 col-xs-12">
                            <textarea readonly="readonly" name="observaciones" id="observaciones" class="form-control input-sm"><?= $oPago_programado->observaciones ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
    	</div>
        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="panel-header">
                    <div class="panel-title">
                       Datos del pago
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Fecha pago </label>
                        <div class="col-sm-4 col-xs-12">
                            <input readonly="readonly" type="text" class="form-control input-sm" name="fecha_pago" id="fecha_pago" value="<?= date('d-m-Y') ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Modo pago </label>
                        <div class="col-sm-8 col-xs-12">
                            <select class="form-control input-sm" id="modo_pago" name="modo_pago">
                                <?php foreach ($modos_pago as $key => $modo): ?>
                                    <option value="<?= $modo->valor ?>"> <?= $modo->descripcion ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Importe a pagar </label>
                        <div class="col-sm-4 col-xs-12">
                            <input  type="text" class="form-control input-sm" name="monto_pagado" id="monto_pagado" onkeypress="return soloNumeroDecimal(event)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Observaciones </label>
                        <div class="col-sm-8 col-xs-12">
                            <textarea class="form-control input-sm" name="observaciones" id="observaciones" ></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-7">
                            <button id="btn_guardar_pago" class="btn btn-primary btn-sm btn-block">
                                Guardar pago
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $("#form_pago_de_programado").submit(function() {
        
        if ($("#form_pago_de_programado").valid()) {
            $('#btn_guardar').attr('disabled', true);
        }else{
            $('#btn_guardar').attr('disabled', false);
        }

    });
    $("#form_pago_de_programado").validate({
        rules: {
            pago_programado_id: "required",
            monto_pagado: "required",
        },
        messages:{
            pago_programado_id: "Seleccione una sede",
            monto_pagado: "Ingrese un monto ",
        }
    });
</script>