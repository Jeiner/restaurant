<?php $meses = array('enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre'); ?>
<div class="modal fade" id="modal_ver_pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="exampleModalLabel"> <strong> Datos del pago programado </strong> </h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
        		<div class="row">
        			<div class="col-sm-6">
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Sede: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->sede_desc ?>
	                            </div>
	                        </div>
						</div>
						<div class="form-group">
	        				<div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Fecha prevista: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                            	<?php if ($oPago_programado->dia_alerta != ""): ?>
	                            		<?= $oPago_programado->dia_alerta ?> de
	                            	<?php endif ?>
	                                <?= strtoupper($meses[$oPago_programado->mes-1]) ?> del
	                                <?= $oPago_programado->anual ?>
	                            </div>
	                        </div>
						</div>
						<div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Proveedor: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->razon_social ?>
	                            </div>
	                        </div>
						</div>
						<div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Descripción: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->descripcion ?>
	                            </div>
	                        </div>
						</div>
        			</div>
        			<div class="col-sm-6">
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Monto previsto: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->monto_previsto ?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Monto pagado: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->monto_pagado ?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Estado: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->estado_desc ?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Observaciones: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oPago_programado->observaciones ?>
	                            </div>
	                        </div>
	                    </div>
        			</div>
        		</div>
      		</div>
      		<div class="modal-footer">
      			<div class="row">
      				<div class="col-sm-3">
      					<!-- data-dismiss="modal" -->
      					<div class="form-group">
      						<button type="button" class="btn btn-danger btn-sm btn-block" onclick="alert();">
      							Eliminar
      						</button>
      					</div>
      				</div>
      				<?php if ($oPago_programado->estado == "PEN"): ?>
      					<div class="col-sm-3">
	      					<div class="form-group">
	      						<a href="<?= base_url('contabilidad/pagos_programados/pagar_programado/').$oPago_programado->pago_programado_id ?>" class="btn btn-primary btn-sm btn-block">
	      							Realizar pago
	      						</a>
	      					</div>
	      				</div>
      				<?php endif ?>
      			</div>
      		</div>
    	</div>
  	</div>
</div>