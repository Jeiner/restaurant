<?php $meses = array('enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre'); ?>
<table class="table table-bordered table-striped" id="tabla-programados" width="100%">
	<thead>
		<tr>
			<th class="center">N° </th>
			<th class="center"> Año </th>
			<th class="center"> Mes </th>
			<th class="center"> Sede </th>
			<th class="center"> Proveedor </th>
			<th class="center"> Monto previsto </th>
			<th class="center"> Monto pagado </th>
			<th class="center"> Estado </th>
			<th class="center"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($pagos_programados as $key => $programado): ?>
            <tr>
				<td class="center"><?= ($key + 1)  ?>  </td>
				<td class="center"><?= $programado->anual  ?>  </td>
				<td class="center">
                    <span hidden >
                        <?php 
                            $string_fecha = $programado->anual.'-'."08".'-01';
                            $_fecha = date($string_fecha);
                            $_fecha = new DateTime($_fecha);
                            // echo $string_fecha;
                            echo $_fecha->format('Y-m')
                         ?>
                    </span>
                    <?= strtoupper($meses[$programado->mes - 1]) ?>  
                </td>
				<td><?= $programado->sede_desc  ?>  </td>
				<td class=""><?= $programado->razon_social  ?>  </td>
				<td class="dinero"><?= $programado->monto_previsto  ?>  </td>
				<td class="dinero"></td>
				<td class="center">
					<?php 
                        if($programado->estado == 'PEN') $est_desc = '<span class="label label-danger"> Pendiente </span>';
                        if($programado->estado == 'PAG') $est_desc = '<span class="label label-success"> Pagado </span>';
                        if($programado->estado == 'POS') $est_desc = '<span class="label label-default"> Postergado </span>';
                        if($programado->estado == 'X') $est_desc = '<span class="label label-inverse"> Anulado </span>';
                    ?>
                    <?= $est_desc  ?>
				</td>
				<td>
					<span class="" data-rel="tooltip" title="Ver pago programado" >
                        <a href="javascript:;" class="btn btn-primary btn-xs" onclick="ver_pago_programado(<?= $programado->pago_programado_id ?>);">
                            <i class="ace-icon fa fa-search bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
				</td>
			</tr>	
        <?php endforeach ?>
	</tbody>
</table>
<div id="para_modal">
    
</div>

<script type="text/javascript">
    function ver_pago_programado(pago_programado_id){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('contabilidad/pagos_programados/ajax_ver_pago_programado')?>",
            data: {"pago_programado_id":pago_programado_id},
            success: function(rpta){
                $('#para_modal').html(rpta);
                $('#modal_ver_pago').modal('show');
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>

<script type="text/javascript">
	$('#tabla-programados').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },     //Código
          { "targets": [ 1 ],"width": "7%", },   //Año
          { "targets": [ 2 ],"width": "8%", },   //Mes
          { "targets": [ 3 ],"width": "20%", },   //Sede
          { "targets": [ 4 ],"width": "20%", },   //Proveedor
          { "targets": [ 5 ],"width": "10%", },   //Monto previsto
          { "targets": [ 6 ],"width": "10%", },   //Monto pagado
          { "targets": [ 7 ],"width": "10%", },   //Estado 
          { "targets": [ 8 ],"width": "10%", },   // 
        ],
        "order": [[ 0, "asc" ]] ,
  	});
    $('#tabla-programados tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>