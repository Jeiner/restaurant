<?php $meses = array('enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre'); ?>
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#pagos_programados">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Pagos programados
            </a>
        </li>
        <li class="">
        	<a  href="<?= base_url('contabilidad/pagos_programados/nuevo') ?>">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nueva programación de pagos
            </a>
        </li>
    </ul>
    <div class="tab-content">
    	<div class="row">
    		<div class="col-sm-2 col-xs-6">
    			<div class="form-group">
    				<select class="form-control input-sm" id="cbo_anual" name="cbo_anual" onchange="filtrar_programados()">
	    				<option value="2018" <?php if(date('Y') == 2018) echo "selected" ?> > 2018</option>
	    				<option value="2019" <?php if(date('Y') == 2019) echo "selected" ?> > 2019</option>
	    				<option value="2020" <?php if(date('Y') == 2020) echo "selected" ?> > 2020</option>
	    			</select>
    			</div>
    		</div>
    		<div class="col-sm-2 col-xs-6">
				<div class="form-group">
					<select class="form-control input-sm" id="cbo_mes" name="cbo_mes" onchange="filtrar_programados()">
                        <option value="-1"> Todos los meses </option>
						<?php for ($i = 0; $i < sizeof($meses); $i++): ?>
							<option value="<?= $i ?>" <?php if(date('m') == ($i+1)) echo "selected" ?> > <?= strtoupper($meses[$i]) ?> </option>
						<?php endfor ?>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<select class="form-control input-sm" id="sede_id" name="sede_id" onchange="filtrar_programados()">
						<option value=""> Todas las sedes  </option>
						<?php foreach ($sedes as $key => $sede): ?>
							<option value="<?= $sede->sede_id ?>"> <?= $sede->sede ?> </option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<select class="form-control input-sm" id="estado" name="estado" onchange="filtrar_programados()">
						<option value=""> Todos los estados  </option>
						<?php foreach ($estados as $key => $est): ?>
							<option value="<?= $est->valor ?>"> <?= $est->descripcion ?> </option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<button class="btn btn-primary btn-sm" onclick="filtrar_programados();"> Filtrar </button>
				</div>
			</div>
    	</div>
    	<br>
    	<div id="tabla_programados">
    		<!-- Tabla -->
    	</div>
    </div>
</div>
<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true}); 

	filtrar_programados();
    function filtrar_programados(){
    	$('#tabla_programados').html('<span class="blue bolder" style="margin: 15px;">Cargando pagos programados...</span>');
    	var anual = $('#cbo_anual').val();
    	var mes_id = $('#cbo_mes').val();
    	var sede_id = $('#sede_id').val();
    	var estado = $('#estado').val();
    	mes_id = parseInt(mes_id) + 1;
    	$.ajax({
            type: 'POST',
            url: "<?=base_url('contabilidad/pagos_programados/ajax_cargar_tabla_programados')?>",
            data: {"anual":anual, "mes_id":mes_id, "sede_id":sede_id, "estado":estado},
            success: function(rpta){
                $('#tabla_programados').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });

    }
</script>