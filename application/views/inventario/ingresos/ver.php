<?php 
    $importe_total = 0.00;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                    Nuevo ingreso
                    <div class="pull-right">
                        <button class="btn btn-inverse btn-minier" onclick="window.history.back();">
                            <i class="ace-icon fa fa-arrow-left bigger-90" aria-hidden="true"></i>
                            Regresar
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 hidden">
                        <div class="form-group ">
                            <label class="control-label" for=""> Ingreso_id </label>
                            <input type="text" name="ingreso_id" id="ingreso_id" class="form-control input-sm" value="<?= $oIngreso->ingreso_id ?>">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for=""> Fecha ingreso </label><label class="red">*</label> 
                            <input type="" name="fecha_ingreso" id="fecha_ingreso" class="form-control input-sm" value="<?= $oIngreso->fecha_ingreso?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for=""> Motivo  </label><label class="red">*</label>
                            <input type="" name="" class="form-control input-sm" value="<?= $oIngreso->motivo_desc ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label" for=""> Proveedor </label>
                            <input type="" name="" class="form-control input-sm" value="<?= $oIngreso->proveedor ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for=""> Tipo de comprobante  </label>
                            <input type="" name="" class="form-control input-sm" value="<?= $oIngreso->tipo_comprobante_desc ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label" for=""> Serie - número  </label>
                            <input type="text" name="" id="" class="form-control input-sm center" value="<?= $oIngreso->nro_comprobante ?>" readonly>
                        </div>
                    </div>
                </div> <!-- row -->
                <br>
                <div class="row">
                    <div class="col-sm-10" id="productos_seleccionados">
                        <table class="table table-bordered " width="100%">
                            <thead>
                                <tr>
                                    <th class="center" style="width: 5%;"> N° </th>
                                    <th class="center" style="width: 40%;"> Producto / insumo</th>
                                    <th class="center" style="width: 10%;"> Unidad </th>
                                    <th class="center" style="width: 14%;"> P. costo </th>
                                    <th class="center" style="width: 10%;"> Cantidad </th>
                                    <th class="center" style="width: 15%;"> Importe </th>
                                    <th class="center" style="width: 10%;"> Vencimiento </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($oIngreso->items as $key => $item): ?>
                                    <?php
                                        $importe_total +=  $item->precio_costo * $item->cantidad;
                                    ?>
                                    <tr>
                                        <td class="center"> <?= $key+1 ?> </td>
                                        <td class="left">
                                            <input type="" name="insumo_id[]" id="insumo_id_<?= $key ?>" value="<?= $item->insumo_id ?>"  class="hidden">
                                            <input type="" name="producto_id[]" id="producto_id_<?= $key ?>" value="<?= $item->producto_id ?>" class="hidden">
                                            <?= $item->producto_insumo ?>
                                        </td>
                                        <td class="" >
                                            <input type="text" name="unidad"  value="<?= $item->unidad_desc ?>" class="form-control input-sm " readonly  style="font-size: 11px!important;">
                                        </td>
                                        <td class="" >
                                            <input type="text" name="precio_costo[]" id="precio_costo_<?= $key ?>" value="<?= $item->precio_costo ?>" class="form-control input-sm dinero" onchange="calcular_importe(<?= $key ?>);" onkeypress="return soloNumeroDecimal(event);" readonly>
                                        </td>
                                        <td class="">
                                            <input type="text" name="cantidad[]" id="cantidad_<?= $key ?>" value="<?= $item->cantidad ?>" class="form-control input-sm center" onchange="calcular_importe(<?= $key ?>);" onkeypress="return soloNumeroEntero(event);" readonly>
                                        </td>
                                        <td class="">
                                            <input type="text" name="importe[]" id="importe_<?= $key ?>" value="<?= $item->importe ?>" class="form-control input-sm dinero-total" readonly>
                                        </td>
                                        <td class="">
                                            <input type="date" name="fecha_vencimiento[]" id="fecha_vencimiento_<?= $key ?>" value="<?= $item->fecha_vencimiento ?>" class="form-control input-sm " onchange="calcular_importe(<?= $key ?>);" readonly>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                        $total_IGV = $importe_total/1.18;
                        $importe_sin_IGV = $importe_total - $total_IGV;
                        // Damos formato 
                        $importe_total = number_format(round($importe_total,2), 2, '.', ' ');
                        $total_IGV = number_format(round($total_IGV,2), 2, '.', ' ');
                        $importe_sin_IGV = number_format(round($importe_sin_IGV,2), 2, '.', ' ');
                    ?>
                    <div class="col-sm-2" style="text-align: right;">
                        <div class="form-group">
                            <label>Total IGV (18%)</label>
                            <input type="" name="total_IGV" class="form-control input-sm dinero" readonly="readonly" value="<?= $total_IGV ?>">
                        </div>
                        <div class="form-group">
                            <label>Total sin IGV</label>
                            <input type="" name="importe_sin_IGV" id="importe_sin_IGV" class="form-control input-sm dinero" readonly="readonly" value="<?= $importe_sin_IGV ?>">
                        </div>
                        <div class="form-group">
                            <label><strong>Importe total</strong></label>
                            <input type="text" name="importe_total" class="form-control input-sm dinero-total" readonly="readonly" value="<?= $importe_total ?>" style="font-size: 18px!important;">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row ">
                    <div class="col-sm-2 col-sm-offset-8">
                        <a href="<?= base_url('inventario/ingresos/imprimir') ?>" class="btn btn-primary btn-block btn-sm ">
                            <i class="ace-icon fa fa-print bigger-90" aria-hidden="true"></i>
                            Imprimir 
                        </a>
                    </div>
                    <div class="col-sm-2 col-md-2 " > 
                        <button class="btn btn-primary btn-block btn-sm">
                            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                            Editar ingreso 
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    function agregar_item(){
        var producto_id = $( "#cbo_productos option:selected" ).attr('producto_id');
        var insumo_id = $( "#cbo_productos option:selected" ).attr('insumo_id');
        if (producto_id == "" && insumo_id == "") {
            return false;
        }
        // alert(producto_id);
        // alert(insumo_id);
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('inventario/ingresos/agregar_item_ajax')?>",
            data: { "producto_id": producto_id, "insumo_id": insumo_id },
            success: function(rpta){
                // Si empieza por corchete es un objeto
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#productos_seleccionados').html(rpta);
                }
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error de conexión.");
                cerrarCargando();
            }
        });
    }
    function guardar_ingreso(){
        $('#btn_guardar_ingreso').attr('disabled', true);
        $('#form_ingreso').submit();
    }
</script>