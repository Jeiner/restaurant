<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                    Nuevo ingreso
                </div>
            </div>
            <div class="panel-body">
                <?php $this->load->view('inventario/ingresos/_form_ingreso'); ?>
            </div>
        </div>
    </div>
</div>