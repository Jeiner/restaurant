<form method="Post" action="<?= base_url('inventario/ingresos/guardar') ?>" class="form-vertical" id="form_ingreso">
	<div class="row">
		<div class="col-sm-4 hidden">
			<div class="form-group ">
				<label class="control-label" for=""> Ingreso_id </label>
				<input type="text" name="ingreso_id" id="ingreso_id" class="form-control input-sm" value="<?= $oIngreso->ingreso_id ?>">
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label class="control-label" for=""> Fecha ingreso </label><label class="red">*</label> 
				<input type="date" name="fecha_ingreso" id="fecha_ingreso" class="form-control input-sm" value="<?= date('Y-m-d') ?>">
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label class="control-label" for=""> Motivo  </label><label class="red">*</label>
				<select class="form-control input-sm chosen-select" id="motivo" name="motivo">
					<option value="">Seleccionar motivo...</option>
					<?php foreach ($motivos_ingreso as $key => $motivo): ?>
						<option value="<?= $motivo->valor ?>"><?= $motivo->descripcion ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label class="control-label" for=""> Proveedor </label>
				<select class="form-control input-sm  chosen-select" id="proveedor_id" name="proveedor_id">
					<option value="">Seleccionar proveedor...</option>
					<?php foreach ($proveedores as $key => $prov): ?>
						<?php 
							$prov_selec = "";
							if ($prov->proveedor_id == $oIngreso->proveedor_id) {
								$prov_selec = "selected";
							}
						?>
						<option <?= $prov_selec ?> value="<?= $prov->proveedor_id ?>"><?= $prov->RUC ?> - <?= $prov->razon_social ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label class="control-label" for=""> Tipo de comprobante  </label>
				<select class="form-control input-sm chosen-select" id="tipo_comprobante" name="tipo_comprobante">
					<option value="">Tipo comprobante ...</option>
					<?php foreach ($tipos_comprobante as $key => $tipos_com): ?>
						<option value="<?= $tipos_com->valor ?>" > <?= $tipos_com->descripcion ?> </option>	
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="form-group">
				<label class="control-label" for=""> Serie - número  </label>
				<input type="text" name="nro_comprobante" id="nro_comprobante" class="form-control input-sm center" value="<?= $oIngreso->nro_comprobante ?>">
			</div>
		</div>
	</div> <!-- row -->
	<br>
	<div class="row">
		<div class="col-sm-11">
			<select class="form-control chosen-select" onchange="agregar_item(this.value);" id="cbo_productos" width="100%">
				<option value="" producto_id="" insumo_id="">-- Seleccionar producto o insumo --</option>
				<?php foreach ($productos as $key => $prod): ?>
					<option value="<?= $key ?>" producto_id="<?= $prod->producto_id ?>" insumo_id="<?= $prod->insumo_id ?>" >
						<?= ($prod->tipo) ?> - <?= $prod->producto_insumo ?>
					</option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
	<br>	
	<div class="row">
		<div class="col-sm-10" id="productos_seleccionados">
			<table class="table table-bordered " width="100%">
				<thead>
					<tr>
						<th class="center" style="width: 5%;"> N° </th>
						<th class="center" style="width: 40%;"> Producto / insumo</th>
						<th class="center" style="width: 10%;"> Unidad </th>
						<th class="center" style="width: 14%;"> P. costo </th>
						<th class="center" style="width: 10%;"> Cantidad </th>
						<th class="center" style="width: 15%;"> Importe </th>
						<th class="center" style="width: 10%;"> Vencimiento </th>
						<th class="center" style="width: 5%;"> X </th>
					</tr>
				</thead>
				<tbody>
					
						
					
				</tbody>
			</table>
		</div>
		<div class="col-sm-2" style="text-align: right;">
			<div class="form-group">
				<label>Total IGV (18%)</label>
				<input type="" name="total_IGV" id="total_IGV" class="form-control input-sm dinero" readonly="readonly" value="">
			</div>
			<div class="form-group">
				<label>Total sin IGV</label>
				<input type="" name="importe_sin_IGV" id="importe_sin_IGV" class="form-control input-sm dinero" readonly="readonly" value="">
			</div>
			<div class="form-group">
				<label><strong>Importe total</strong></label>
				<input type="text" name="importe_total" id="importe_total" class="form-control input-sm dinero-total" readonly="readonly"  style="font-size: 18px!important;">
			</div>
		</div>
	</div>
	<br>
	<div class="row form-group">
		<div class="col-sm-3 col-sm-offset-6">
			<a href="javascript:;" class="btn btn-primary btn-sm btn-block" id="btn_reiniciar_form">
				Reiniciar formulario
			</a>
		</div>
		<div class="col-sm-3">
			<button type="button" class="btn btn-primary btn-block btn-sm" id="btn_guardar_ingreso" onclick="guardar_ingreso();" > Guardar ingreso </button>	
		</div>
	</div>
</form>

<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true});
	var gb_productos = '<?= json_encode($productos) ?>'; //Variable global  de produtcos
	var gb_array_productos = JSON.parse(gb_productos); //Pasamos los productos a una variable de javascript array

	function guardar_ingreso(){
		var motivo_ingreso = $('#motivo').val();
		var list_items = (localStorage.getItem('list_items') === null) ? []: JSON.parse(localStorage.getItem('list_items'));
		var nro_items = (localStorage.getItem('list_items') === null) ? 0 : list_items.length;
		// Verificar que haya por lo menos un producto para ingresar
		if(nro_items <= 0){
			alertify.error("Debe ingresar por lo menos un producto");
			return false;
		}
		// Verificar que no haya montos vacíos
		var existe_vacio = false;
		$.each(list_items, function () {
			if (this.precio_costo == "" || this.cantidad == "") {
				existe_vacio = true;
				return;
			}
		});
		if(existe_vacio){
			alertify.error("Hay productos con valores vacíos.");
			return false;
		}
		// Motivo de ingreso obligatorio
		if(motivo_ingreso == ""){
			alertify.error("Debe selecionar un motivo");
			return false;
		}
		$('#btn_guardar_ingreso').attr('disabled', true);
		$('#form_ingreso').submit();
	}
	function load_items(){ //Carga los items a la tabla 
		// Obtiene todos los productos
		var list_items = (localStorage.getItem('list_items') === null) ? []: JSON.parse(localStorage.getItem('list_items'));
		var row_no =  0;
		$("#productos_seleccionados tbody").empty();
		var new_data = "";
		var importe_total = 0;
		$.each(list_items, function () {
		 	var item = this;
		 	new_data += "<tr>\
							<td class='center'> " + (row_no + 1) + " </td>\
							<td class='left'>\
								<input type='' name='insumo_id[]' id='insumo_id_"+ row_no +"' value='"+this.insumo_id+"'  class='hidden'>\
								<input type='' name='producto_id[]' id='producto_id_"+ row_no +"' value='"+this.producto_id+"' class='hidden'>\
								<input type='' name='producto_insumo[]' id='producto_insumo"+ row_no +"' value='"+this.producto_insumo+"' class='hidden'>\
								"+ this.producto_insumo +"\
							</td>\
							<td class='' >\
								<input type='text' name='unidad_desc[]'  value='"+ this.unidad_desc +"' class='form-control input-sm ' readonly  style='font-size: 11px!important;'>\
							</td>\
							<td class='' >\
								<input type='text' name='precio_costo[]' id='precio_costo_"+ row_no +"' value='" + this.precio_costo + "' class='form-control input-sm dinero' onchange='calcular_importe(this,"+ row_no +");' onkeypress='return soloNumeroDecimal(event);'>\
							</td>\
							<td class=''>\
								<input type='text' name='cantidad[]' id='cantidad_"+ row_no +"' value='"+this.cantidad+"' class='form-control input-sm center' onchange='calcular_importe(this,"+ row_no +");' onkeypress='return soloNumeroEntero(event);'>\
							</td>\
							<td class=''>\
								<input type='text' name='importe[]' id='importe_"+ row_no +"' value='"+ this.importe +"' class='form-control input-sm dinero-total' readonly>\
							</td>\
							<td class=''>\
								<input type='date' name='fecha_vencimiento[]' id='fecha_vencimiento_"+ row_no +"' value='"+this.fecha_vencimiento+"' onchange='set_fecha_venci(this, "+row_no+")' class='form-control input-sm' >\
							</td>\
							<td class='center'>\
								<span class=' data-rel='tooltip' title='Quitar producto' >\
	                            <button type='button' class='btn btn-danger btn-xs' onclick='quitar_item("+ row_no +");'>\
	                                X\
	                            </button>\
	                            </span>\
							</td>\
						</tr>";
			row_no++;
			importe_total += (item.precio_costo * item.cantidad);
	 	});
		$("#productos_seleccionados tbody").html(new_data);
		var total_IGV = (importe_total/1.18);
		var importe_sin_IGV = importe_total - total_IGV;
		
		$('#total_IGV').val(parseFloat(total_IGV).toFixed(2));
		$('#importe_sin_IGV').val(parseFloat(importe_sin_IGV).toFixed(2));
		$('#importe_total').val(parseFloat(importe_total).toFixed(2));
	}
	function agregar_item(producto_key){
		var producto_id = $( "#cbo_productos option:selected" ).attr('producto_id');
		var insumo_id = $( "#cbo_productos option:selected" ).attr('insumo_id');		
		var new_item = gb_array_productos[producto_key]; 	//Obtenemos el producto o insumo
		// si es nulo le asignamos vacío
		new_item.producto_id = (new_item.producto_id === null) ? "" : new_item.producto_id;
		new_item.insumo_id = (new_item.insumo_id === null) ? "" : new_item.insumo_id;
		// Ottros valores del new_item
		new_item.precio_costo = (new_item.precio_costo === null) ? "" : new_item.precio_costo; 
		new_item.cantidad = 1;
		new_item.importe  = new_item.precio_costo;
		new_item.fecha_vencimiento  = "";
		//Obtenemos la lista guardada y actualizamos
		var list_items = (localStorage.getItem('list_items') === null) ? []: JSON.parse(localStorage.getItem('list_items'));
		var nro_items = (localStorage.getItem('list_items') === null) ? 0 : list_items.length;
		// Verificamos que el producto no exista
		existe = false;
		$.each(list_items, function () {
			if (this.producto_id == producto_id && this.insumo_id == insumo_id) {
				existe = true;
			}
		});
		if(existe){
			alertify.error("El producto ya está seleccionado.");
			return false;
		}
		list_items[nro_items] = new_item;
		localStorage.setItem('list_items', JSON.stringify(list_items));
		load_items();
	}
	function calcular_importe(input,key){
		var text_precio_costo = '#precio_costo_'+key;
		var text_cantidad = '#cantidad_'+key;		

		var precio_costo = ($(text_precio_costo).val() == "") ? "0.00" : parseFloat($(text_precio_costo).val()).toFixed(2);
		var cantidad = $(text_cantidad).val();
		var importe = parseFloat(precio_costo * cantidad).toFixed(2);
		// Actualizamos la lista de items
		var list_items = (localStorage.getItem('list_items') === null) ? []: JSON.parse(localStorage.getItem('list_items'));
		var nro_items = (localStorage.getItem('list_items') === null) ? 0 : list_items.length;

		// alert(key);
		item = list_items[key];
		item.precio_costo = precio_costo;
		item.cantidad = cantidad;
		item.importe = importe;

		list_items[key] = item;
		localStorage.setItem('list_items', JSON.stringify(list_items));
		load_items();
	}
	function set_fecha_venci(input, key){  //Guardar la fecha de vencimiento de algun item
		var list_items = (localStorage.getItem('list_items') === null) ? []: JSON.parse(localStorage.getItem('list_items'));
		list_items[key].fecha_vencimiento = $(input).val();
		localStorage.setItem('list_items', JSON.stringify(list_items));
	}
	function quitar_item(key){
		var list_items = (localStorage.getItem('list_items') === null) ? []: JSON.parse(localStorage.getItem('list_items'));
		delete list_items[key];
		list_items = list_items.filter(function() { return true; });
		localStorage.setItem('list_items', JSON.stringify(list_items));
        load_items();
	}
</script>



<!-- LOCAL STORAGE -->
<script type="text/javascript">
	
	<?php if ($this->session->userdata('remove_local_ingreso')): ?>
		remove_local_ingreso();
		<?php $this->session->unset_userdata('remove_local_ingreso'); ?>
	<?php endif ?>
	
	$('#btn_reiniciar_form').click(function() {
	    swal({
			title: "¿Esta seguro de reiniciar?",
			text: "Se perderán los datos ingresados",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "SI, Reiniciar",
			closeOnConfirm: false
		},
		function(){
		  	remove_local_ingreso();
			location.reload();
		});
  	});
  	function remove_local_ingreso(){  //Elikminar datos del localstorage
  		localStorage.removeItem('motivo');
  		localStorage.removeItem('proveedor_id');
  		localStorage.removeItem('tipo_comprobante');
  		localStorage.removeItem('nro_comprobante');
  		localStorage.removeItem('list_items');
  	}
	function cargar_local_storage(){
		$('#motivo').val(localStorage.getItem("motivo"));
		$('#proveedor_id').val(localStorage.getItem("proveedor_id"));
		$('#tipo_comprobante').val(localStorage.getItem("tipo_comprobante"));
		$('#nro_comprobante').val(localStorage.getItem("nro_comprobante"));
		$('select').trigger("chosen:updated");
		load_items();
	}
	cargar_local_storage();

	$('#motivo').change(function (e) {
        localStorage.setItem('motivo', $(this).val());
    });
    $('#proveedor_id').change(function (e) {
        localStorage.setItem('proveedor_id', $(this).val());
    });
    $('#tipo_comprobante').change(function (e) {
        localStorage.setItem('tipo_comprobante', $(this).val());
    });
    $('#nro_comprobante').change(function (e) {
        localStorage.setItem('nro_comprobante', $(this).val());
    });
</script>