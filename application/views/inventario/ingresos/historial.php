<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="ingresos" href="#pagos">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Historial de Ingresos
            </a>
        </li>
        <li>
            <a  href="<?= base_url('inventario/ingresos/historial_detalle') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Historial detalle de ingresos 
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="ingresos" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha inicio </label>
                        <input type="date" class="form-control input-sm " name="start_date" id="start_date"  value="<?=  date('Y-m-01') ?>" onchange="verify_start_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha fin </label>
                        <input type="date" class="form-control input-sm " name="end_date" id="end_date"  value="<?=  date('Y-m-d') ?>" onchange="verify_end_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" style="margin-top: 23px;">
                        <button class="btn btn-primary btn-xs btn-block" id="btn_filtrar" onclick="filtrar_ingresos();">
                            Buscar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" id="tabla_ingresos">
                    <!-- Lista de ingresos  -->
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-3 col-sm-offset-9">
                    <a href="javascript:;" class="btn btn-primary btn-xs btn-block" onclick="exportar_ingresos();">
                        <i class="ace-icon fa fa-file-excel-o bigger-90"></i>
                        Exportar a excel
                    </a>
                </div>
            </div> 
        </div>
    </div>
</div>
<script type="text/javascript">
    filtrar_ingresos();

    function date_diff(start_date_AMD, end_date_AMD,tipe){
        var start_date = new Date(start_date_AMD).getTime();
        var end_date    = new Date(end_date_AMD).getTime();
        var diff = end_date - start_date;
        return diff/(1000*60*60*24);
    }
    function verify_end_date(){
        // alert();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date === undefined || start_date == ""){
            $('#end_date').parent(".form-group").addClass("has-error");            
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            $('#end_date').parent(".form-group").addClass("has-error");
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
    }
    function verify_start_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(end_date === undefined || end_date == ""){
            return true;
        }else{
            array_start_date = start_date.split("/");
            array_end_date = end_date.split("/");
            start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
            end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
            start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
            var dias = date_diff(start_date, end_date, 'days');
            if(dias < 0 ){
                $('#end_date').parent(".form-group").addClass("has-error");
                return false;
            }else{
                $('#end_date').parent(".form-group").removeClass("has-error");
            }
        }
    }
    function filtrar_ingresos(){
        $('#btn_filtrar').attr('disabled', true);
        $('#tabla_ingresos').html('<span class="blue bolder" style="margin: 15px;">Cargando ingresos...</span>');
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        // abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('inventario/ingresos/cargar_tabla_ingresos_by_fechas')?>",
            data: {"start_date":start_date, "end_date":end_date},
            success: function(rpta){
                $('#btn_filtrar').attr('disabled', false);
                $('#tabla_ingresos').html(rpta);
                // cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
                // cerrarCargando();
            }
        });
    }
    function exportar_ingresos(){
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();

        var url = '<?=base_url('inventario/ingresos/exportar_ingresos')?>'+'?start_date='+start_date+'&end_date='+end_date;
        window.location = url;
    }
</script>