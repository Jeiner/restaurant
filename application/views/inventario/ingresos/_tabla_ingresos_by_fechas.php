<?php 
    $total = 0;
?>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped" id="tabla-ingresos" width="100%">
            <thead>
                <tr>
                    <th style="text-align: center;" > Cód. </th>
                    <th style="text-align: center;" > Fecha </th>
                    <th style="text-align: center;" > Motivo. </th>
                    <th style="text-align: center;" > Usuario </th>
                    <th style="text-align: center;" > Proveedor </th>
                    <th style="text-align: center;" > Total </th>
                    <th style="text-align: center;" > Tipo comprobante  </th>
                    <th style="text-align: center;" > Ver </th>
                </tr>
            </thead>
            <tbody id="cuerpo_tabla_ingresos">
                <?php foreach ($ingresos as $key => $ingreso): ?>
                    <?php $total += $ingreso->importe_total ?>
                    <tr>
                        <td style="text-align: center"> <?= $ingreso->ingreso_id  ?> </td>
                        <td style="text-align: center"> <?= $ingreso->fecha_ingreso ?> </td>
                        <td style="text-align: center"> <?= $ingreso->motivo_desc ?> </td>
                        <td style="text-align: center"> <?= $ingreso->usuario ?> </td>
                        <td style="text-align: center"> <?= ($ingreso->proveedor != '') ? substr($ingreso->proveedor, 0, 14).'...' : ''  ?> </td>
                        <td style="text-align: center" class="dinero"> <?= $ingreso->importe_total ?> </td>
                        <td style="text-align: center"> <?= $ingreso->tipo_comprobante_desc ?></td>
                        <td style="text-align: center">
                            <a href="<?= base_url('inventario/ingresos/ver/'); ?><?= $ingreso->ingreso_id ?>">
                                Ver
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-8 col-lg-3 col-lg-offset-9">
        <div class="form-group">
            <label class="col-sm-4" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                <strong>Total: </strong>
            </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="" id="" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= number_format(round($total,2), 2, '.', ' ') ?>" readonly>
            </div>
        </div>
    </div>
</div>
<br>

<script type="text/javascript">
    $('#tabla-ingresos').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },   //Cod
          { "targets": [ 1 ],"width": "15%", },   //Fecha
          { "targets": [ 2 ],"width": "10%", },   //motivo
          { "targets": [ 3 ],"width": "10%", },   //usuario
          { "targets": [ 4 ],"width": "15%", },   //proveed
          { "targets": [ 5 ],"width": "10%", },   //importe
          { "targets": [ 6 ],"width": "10%", },   //tipo
          { "targets": [ 7 ],"width": "10%", },   //tipo
        ],
        "order": [[ 1, "desc" ]] ,
    });
    $('#tabla-ingresos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>
