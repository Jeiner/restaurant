<table class="table table-bordered table-striped" id="tabla_data_detalle" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;" > Cód. </th>
            <th style="text-align: center;" > Fecha </th>            
            <th style="text-align: center;" > Producto/Insumo </th>
            <th style="text-align: center;" > Precio unit. </th>
            <th style="text-align: center;" > Cantidad. </th>
            <th style="text-align: center;" > Importe </th>
            <th style="text-align: center;" > Fecha venci. </th>
            <th style="text-align: center;" > Estado </th>
        </tr>
    </thead>
    <tbody id="cuerpo_tabla_comandas">
    	<?php foreach ($detalle as $key => $item): ?>
    		<tr>
				<td style="text-align: center"> <?= $item->ingreso_detalle_id  ?> </td>
				<td style="text-align: center"> <?= $item->fecha_registro ?> </td>
				<td style="text-align: left"> <?= $item->producto_insumo  ?> </td>
                <td class="dinero">   <?= $item->precio_costo ?> </td>
                <td style="text-align: center">  <?= $item->cantidad ?> </td>
                <td class="dinero"> <?= $item->importe ?> </td>
                <td style="text-align: center"> <?= ($item->fecha_vencimiento == '0000-00-00') ? '' : $item->fecha_vencimiento  ?> </td>
                <td style="text-align: center"> <?= $item->estado?> </td>
			</tr>
    	<?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">

    $('#tabla_data_detalle').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "6%", },   //Cod 
          { "targets": [ 1 ],"width": "15%", },   //fecha
          { "targets": [ 2 ],"width": "25%", },   //produco
          { "targets": [ 3 ],"width": "10%", },   //costo
          { "targets": [ 4 ],"width": "10%", },   //cantidad
          { "targets": [ 5 ],"width": "10%", },   //importe
          { "targets": [ 6 ],"width": "10%", },   //Venci
          { "targets": [ 7 ],"width": "10%", },   //Estado
        ],
        "order": [[ 1, "desc" ]] ,
    });
    
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>