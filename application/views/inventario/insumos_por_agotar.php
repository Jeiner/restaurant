<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('inventario/inventario/productos_por_agotar') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Productos por agotar
            </a>
        </li>
        <li  class="active">
            <a data-toggle="tab" href="#insumos_por_agotar">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Insumos por agotar
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="insumos_por_agotar" class="tab-pane active">
            <table class="table table-bordered table-striped" id="tabla_insumos">
                <thead>
                    <tr>
                        <th class="center">Cód.</th>
                        <th class="left">Insumo</th>
                        <th class="center">Unidad</th>
                        <th class="center">Stock actual</th>
                        <th class="center">Cantidad mínima</th>
                        <th class="center">Precio costo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($insumos_por_agotar as $key => $produ): ?>
                        <tr>
                            <td class="center"><?= $produ->insumo_id ?> </td>
                            <td class="left"><?= $produ->insumo ?> </td>
                            <td class="center"><?= $produ->unidad_desc ?> </td>
                            <td class="center"><?= $produ->stock ?> </td>
                            <td class="center"><?= $produ->stock_minimo ?> </td>
                            <td class="center"><?= $produ->precio_costo ?> </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#tabla_insumos').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },     //Código
          { "targets": [ 1 ],"width": "30%", },   //Producto
          { "targets": [ 2 ],"width": "20%", },   //Unidad
          { "targets": [ 3 ],"width": "10%", },   //stock
          { "targets": [ 4 ],"width": "10%", },   //minim
          { "targets": [ 5 ],"width": "10%", },   //costo
        ],
        "order": [[ 1, "asc" ]] ,
    });
    $('#tabla_insumos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>