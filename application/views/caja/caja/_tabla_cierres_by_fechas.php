<table class="table table-bordered table-striped" width="100%">
    <thead>
        <tr>
            <th style="text-align: center; width: 5%"> Cód. </th>
            <th style="text-align: center; width: 15%"> Aperturada en </th>
            <th style="text-align: center; width: 12%"> Aperturada por </th>
            <th style="text-align: center; width: 10%"> Monto inicial </th>
            <th style="text-align: center; width: 10%"> Saldo </th>
            <th style="text-align: center; width: 15%"> cerrado_en </th>
            <th style="text-align: center; width: 12%"> cerrado_por </th>
            <th style="text-align: center; width: 15%"> Estado </th>
            <th></th>
        </tr>
    </thead>
    <tbody id="">
    	<?php foreach ($cierres as $key => $caja): ?>
    		<tr>
				<td style="text-align: center"> <?= $caja->caja_id  ?> </td>
				<td style="text-align: center"> <?= $caja->aperturado_en ?> </td>
				<td style="text-align: center"> <?= strtoupper($caja->aperturado_por) ?> </td>
				<td class="dinero">
                    <?= number_format(round($caja->monto_inicial,2), 2, '.', ' ') ?>
                </td>
                <td class="dinero"> 
                    <?= number_format(round($caja->saldo_caja,2), 2, '.', ' ') ?>
                </td>
				<td style="text-align: center"> <?= $caja->cerrado_en ?> </td>
                <td style="text-align: center"> <?= strtoupper($caja->cerrado_por) ?> </td>
				<td style="text-align: center">
                    <?php if ($caja->estado == 'A'): ?>
                        <span class="label label-primary"> Aperturada </span>
                    <?php endif ?>
                    <?php if ($caja->estado == 'C'): ?>
                        <span class="label label-danger"> Cerrada </span>
                    <?php endif ?>            
                </td>
				<td style="text-align: center">
                    <?php if ($caja->estado == 'C'): ?>
                        <a href="<?= base_url('caja/caja/detalle/');?><?= $caja->caja_id ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-search"></i>
                        </a>
                    <?php endif ?>
                </td>
			</tr>
    	<?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
    
</script>
