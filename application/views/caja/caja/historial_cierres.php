<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-tasks bigger-90" aria-hidden="true"></i>
                    Historial de cierres de caja
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class=" control-label" style=""> Fecha inicio </label>
                            <input type="date" class="form-control input-sm " name="start_date" id="start_date"  value="<?=  date('Y-m-01') ?>" onchange="verify_start_date();">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class=" control-label" style=""> Fecha fin </label>
                            <input type="date" class="form-control input-sm " name="end_date" id="end_date"  value="<?=  date('Y-m-d') ?>" onchange="verify_end_date();">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" style="margin-top: 23px;">
                            <button class="btn btn-primary btn-xs btn-block" id="filtrar" onclick="filtrar_cierres();">
                                Buscar
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12" id="tabla_cierres">
                        <!-- Lista de comandas  -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-9">
                        <a href="javascript:;" class="btn btn-primary btn-xs btn-block" onclick="exportar_cierres();">
                            <i class="ace-icon fa fa-file-excel-o bigger-90"></i>
                            Exportar a excel
                        </a>
                    </div>
                </div>  
            </div>
        </div>
	</div>
</div>

<script type="text/javascript">
    filtrar_cierres();

    function date_diff(start_date_AMD, end_date_AMD,tipe){
        var start_date = new Date(start_date_AMD).getTime();
        var end_date    = new Date(end_date_AMD).getTime();
        var diff = end_date - start_date;
        return diff/(1000*60*60*24);
    }
    function verify_end_date(){
        // alert();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date === undefined || start_date == ""){
            $('#end_date').parent(".form-group").addClass("has-error");            
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            $('#end_date').parent(".form-group").addClass("has-error");
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
    }
    function verify_start_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(end_date === undefined || end_date == ""){
            return true;
        }else{
            array_start_date = start_date.split("/");
            array_end_date = end_date.split("/");
            start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
            end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
            start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
            var dias = date_diff(start_date, end_date, 'days');
            if(dias < 0 ){
                $('#end_date').parent(".form-group").addClass("has-error");
                return false;
            }else{
                $('#end_date').parent(".form-group").removeClass("has-error");
            }
        }
    }
    function filtrar_cierres(){
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        $('#tabla_cierres').html('<span class="blue bolder" style="margin: 15px;">Cargando cierres de caja...</span>');
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('caja/caja/cargar_tabla_cierres_by_fechas')?>",
            data: {"start_date":start_date, "end_date":end_date},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#tabla_cierres').html(rpta);
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function exportar_cierres(){
        // alert("");return;
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();

        var url = '<?=base_url('caja/caja/exportar_cierres')?>'+'?start_date='+start_date+'&end_date='+end_date;
        window.location = url;
       
    }
</script>