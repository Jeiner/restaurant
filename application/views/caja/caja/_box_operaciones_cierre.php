<div class="panel panel-default">
	<div class="panel-header">
		<div class="panel-title">
            <i class="ace-icon fa fa-lock bigger-90" aria-hidden="true"></i>
            Operaciones de cierre
        </div>
    </div>
    <div class="panel-body">
    	<?php

    		$checked_asistencias = ($oCaja->fecha_sync_marcaciones == null) ? "": "checked";
    		$checked_comandas = ($oCaja->fecha_sync_comandas == null) ? "": "checked";
    		$checked_procesar_cpbe = ($oCaja->fecha_procesar_cpbe == null) ? "": "checked";
    		
    	?>
    	<table class="" width="100%">
    		<tr>
    			<td>
					<div class="modulo-titulo">
        				<div class="">
        					<label>
								<input <?= $checked_asistencias ?> type="checkbox" class="ace" onclick="return false;" />
								<span class="lbl" style="color: #000;">
									Sincronizar marcaciones
								</span>
								<span class="text-muted" style="font-size:12px">
									<?php if ($oCaja->fecha_sync_marcaciones != null): ?>
										(<?= $oCaja->fecha_sync_marcaciones ?>)
									<?php endif ?>
								</span>
							</label>
        				</div>
        			</div>
    			</td>
    			<td style="text-align: right;">
    				<?php
    					$url_enviar_marcaciones = "nube/nube/enviar_marcaciones_nube?caja_id=".$oCaja->caja_id;
    				?>
    				<a class="btn-sin-disenio btn-xs" href='<?= base_url($url_enviar_marcaciones) ?>'>
    					Sincronizar
    				</a>
    			</td>
    		</tr>

    		<tr>
    			<td>
					<div class="modulo-titulo">
        				<div class="">
        					<label>
								<input <?= $checked_comandas ?> type="checkbox" class="ace" onclick="return false;" />
								<span class="lbl" style="color: #000;">
									Sincronizar comandas
								</span>
								<span class="text-muted" style="font-size:12px">
									<?php if ($oCaja->fecha_sync_comandas != null): ?>
										(<?= $oCaja->fecha_sync_comandas ?>)
									<?php endif ?>
								</span>
							</label>
        				</div>
        			</div>
    			</td>
    			<td style="text-align: right;">
    				<a class="btn-sin-disenio btn-xs">
    					Sincronizar
    				</a>
    			</td>
    		</tr>

    		<tr>
    			<td>
					<div class="modulo-titulo">
        				<div class="">
        					<label>
								<input <?= $checked_procesar_cpbe ?> type="checkbox" class="ace" onclick="return false;" />
								<span class="lbl" style="color: #000;">
									Procesar comprobantes electrónicos
								</span>
							</label>
        				</div>
        			</div>
    			</td>
    			<td style="text-align: right;">
    				<?php
    					$url_procesar_cpbe = "nube/facsu/procesar_cpbes_facsu?caja_id=".$oCaja->caja_id;
    				?>
    				<a class="btn-sin-disenio btn-xs" href='<?= base_url($url_procesar_cpbe) ?>'>
    					Procesar
    				</a>
    			</td>
    		</tr>

    	</table>
    </div>
</div>