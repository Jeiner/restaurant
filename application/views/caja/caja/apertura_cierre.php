
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-info">
			<i class="ace-icon fa fa-info"></i>
			Esta sección permite aperturar y cerrar caja, asi mismo visualizar las ventas y gastos de la caja aperturada en el momento.
			<br>
			**Recuerde que no podrá hacer ni ventas ni gastos si la caja no está aperturada.
			<button class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
		</div>
	</div>
</div>

<?php if (!$oCaja): ?>

	<!-- Cuando la caja esté cerrada -->
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="panel panel-default">
	            <div class="panel-header">
	                <div class="panel-title">
                        <i class="ace-icon fa fa-cubes bigger-90" aria-hidden="true"></i>
                        Apertura / Cierre de caja
	                </div>
	            </div>
	            <div class="panel-body">
					
					<button class=" btn btn-primary" onclick="cargar_modal_apertura();">
						Aperturar caja
					</button>
	            </div>
	        </div>
		</div>
		<div  id="div_modal_caja">
			<!-- Cargará el modal para la apertura de caja -->
		</div>
	</div>
	<?php $this->load->view('caja/caja/_modal_apertura'); ?>

<?php else: ?>
	<?php 
		$total_ventas = 0;
		$total_gastos = 0;
		$total_ventas_efectivo = 0;
		$total_ventas_tarjeta = 0;
		$total_gastos_efectivo = 0;
		$total_gastos_tarjeta = 0;
		$saldo_caja = $oCaja->monto_inicial;
	?>
	<div class="row">
		<div class="col-sm-8 col-xs-12">
			<div class="panel panel-default">
	            <div class="panel-header">
	                <div class="panel-title">
                        <i class="ace-icon fa fa-cubes bigger-90" aria-hidden="true"></i>
                        Caja aperturada - <strong>Movimientos en caja (Efectivo)</strong>
	                </div>
	            </div>
	            <div class="panel-body">
	            	<div class="row">
						<div class="col-sm-12 col-xs-12">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="text-align: center"> Fecha/Hora </th>
										<th style="text-align: center"> Usuario </th>
										<th style="text-align: center"> Operación </th>
										<th style="text-align: center"> Monto </th>
										<th style="text-align: center"> Estado </th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($movimientos_caja as $key => $mov): ?>
										<tr>
											<td style="text-align: center"> <?= $mov->fecha ?> </td>
											<td style="text-align: center"> <?= strtoupper($mov->usuario) ?> </td>
											<td style="text-align: center"> <?= $mov->operacion ?> </td>
											<td class="dinero" >
												<?= number_format(round($mov->monto,2), 2, '.', ' ') ?>
											</td>
											<td style="text-align: center">
												<?php if ($mov->estado == 'A'): ?>
							                        <span class="label label-success"> Activo </span>
							                    <?php else: ?>
							                        <span class="label label-inverse"> Anulado </span>
							                    <?php endif ?>
											</td>
										</tr>
										<?php 
											if ($mov->operacion == "Venta") {
												$total_ventas_efectivo += $mov->monto;
											}
											if ($mov->operacion == "Gasto") {
												$total_gastos_efectivo += $mov->monto;
											}
										?>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-left">
								Total ventas efectivo: S/. 
								<span>
									<?= number_format(round($total_ventas_efectivo,2), 2, '.', ' ') ?>
								</span>
							</div>
							<div class="pull-right">
								Total gastos efectivo: S/. 
								<span>
									<?= number_format(round($total_gastos_efectivo,2), 2, '.', ' ') ?>
								</span>
							</div>
						</div>
					</div>
	            </div>
	            <!-- panel boyd -->
	        </div>
	        <br>
	        <div class="panel panel-default">
	            <div class="panel-header">
	                <div class="panel-title">
                        <i class="ace-icon fa fa-cubes bigger-90" aria-hidden="true"></i>
                        Caja aperturada - <strong> Movimiento en tarjeta</strong>
	                </div>
	            </div>
	            <div class="panel-body">
	            	<div class="row">
						<div class="col-sm-12 col-xs-12">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="text-align: center"> Fecha/Hora </th>
										<th style="text-align: center"> Usuario </th>
										<th style="text-align: center"> Operación </th>
										<th style="text-align: center"> Monto </th>
										<th style="text-align: center"> Estado </th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($movimientos_tarjeta as $key => $mov): ?>
										<tr>
											<td style="text-align: center"> <?= $mov->fecha ?> </td>
											<td style="text-align: center"> <?= $mov->usuario ?> </td>
											<td style="text-align: center"> <?= $mov->operacion ?> </td>
											<td class="dinero" > <?= $mov->monto ?> </td>
											<td style="text-align: center">
												<?php if ($mov->estado == 'A'): ?>
							                        <span class="label label-success"> Activo </span>
							                    <?php else: ?>
							                        <span class="label label-inverse"> Anulado </span>
							                    <?php endif ?>
											</td>
										</tr>
										<?php 
											if ($mov->operacion == "Venta") {
												$total_ventas_tarjeta += $mov->monto;
											}
											if ($mov->operacion == "Gasto") {
												$total_gastos_tarjeta += $mov->monto;
											}
										?>
									<?php endforeach ?>
									<?php
										// $total_ventas_tarjeta = number_format(round($total_ventas_tarjeta,2), 2, '.', ' ');
										// $total_gastos_tarjeta = number_format(round($total_gastos_tarjeta,2), 2, '.', ' ');
									?>
									<?php
										$total_ventas = $total_ventas_efectivo + $total_ventas_tarjeta;
										$total_gastos = $total_gastos_efectivo + $total_gastos_tarjeta;
										
										$saldo_caja = $oCaja->monto_inicial + $total_ventas - $total_gastos;

										$total_ventas = number_format(round($total_ventas,2), 2, '.', ' ');
										$total_gastos = number_format(round($total_gastos,2), 2, '.', ' ');
										$saldo_caja = number_format(round($saldo_caja,2), 2, '.', ' ');
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-left">
								Total ventas tarjeta: S/. 
								<span>
									<?= number_format(round($total_ventas_tarjeta,2), 2, '.', ' ') ?>
								</span>
							</div>
							<div class="pull-right">
								Total gastos tarjeta: S/. 
								<span>
									<?= number_format(round($total_gastos_tarjeta,2), 2, '.', ' ') ?>
								</span>
							</div>
						</div>
					</div>
	            </div>
	            <!-- panel boyd -->
	        </div>
		</div>
		<div class="col-sm-4 col-xs-12">
			
			<?php $this->load->view('caja/caja/_box_operaciones_cierre'); ?>

			<div class="panel panel-default">
	            <div class="panel-header">
	                <div class="panel-title">
                        <i class="ace-icon fa fa-lock bigger-90" aria-hidden="true"></i>
                        Cerrar caja
	                </div>
	            </div>
	            <?php $ruta_cerrar = base_url('caja/caja/cerrar/').$oCaja->caja_id ?>
	            <?= form_open($ruta_cerrar, 'class="form-horizontal" id="myform"'); ?>
	            <div class="panel-body">
	            	<div class="row  form-horizontal" >
						<div class="col-sm-12 ">
							<div class="form-group hidden">
								<label class="control-label" for=""> ID </label>
								<input type="text" name="caja_id" id="caja_id" class="form-control input-sm" value="<?= $oCaja->caja_id ?>" readonly>
							</div>
							<div class="form-group">
	                            <label class="col-sm-5 control-label" style=""> Fecha de apetura </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm center" name="aperturado_en" id="aperturado_en"  value="<?= $oCaja->aperturado_en ?>" readonly >
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-sm-5 control-label" style=""> Aperturado por </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm center" name="aperturado_por" id="aperturado_por"  value="<?= strtoupper($oCaja->aperturado_por) ?>" readonly >
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-sm-5 control-label" style=""> Monto inicial </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm dinero " name="monto_inicial" id="monto_inicial"  value="<?= $oCaja->monto_inicial ?>" readonly >
	                            </div>
	                        </div>
	                        <div class="form-group hidden">
	                            <label class="col-sm-5 control-label" style=""> Total ventas efectivo </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm dinero" name="total_ventas_efectivo" id="total_ventas_efectivo"  value="<?= $total_ventas_efectivo ?>" readonly >
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-sm-5 control-label" style=""> Total ventas  </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm dinero" name="total_ventas" id="total_ventas"  value="<?= $total_ventas ?>" readonly >
	                            </div>
	                        </div>
	                        <div class="form-group hidden">
	                            <label class="col-sm-5 control-label" style=""> Total gastos efectivo </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm dinero" name="total_gastos_efectivo" id="total_gastos_efectivo"  value="<?= $total_gastos_efectivo ?>" readonly >
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-sm-5 control-label" style=""> Total gastos </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm dinero" name="total_gastos" id="total_gastos"  value="<?= $total_gastos ?>" readonly >
	                            </div>
	                        </div>
	                        <div id="tarjeta" class="hidden">
		                        <div class="form-group">
		                            <label class="col-sm-5 control-label" style=""> Total ventas tarjeta </label>
		                            <div class="col-sm-7">
		                                <input type="text" class="form-control input-sm dinero" name="total_ventas_tarjeta" id="total_ventas_tarjeta"  value="<?= $total_ventas_tarjeta ?>" readonly >
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="col-sm-5 control-label" style=""> Total gastos tarjeta </label>
		                            <div class="col-sm-7">
		                                <input type="text" class="form-control input-sm dinero" name="total_gastos_tarjeta" id="total_gastos_tarjeta"  value="<?= $total_gastos_tarjeta ?>" readonly >
		                            </div>
		                        </div>
	                        </div>
	                        <div class="form-group">
	                            <label class="col-sm-5 control-label" style=""> Saldo </label>
	                            <div class="col-sm-7">
	                                <input type="text" class="form-control input-sm dinero-total" name="saldo_caja" id="saldo_caja"  value="<?= $saldo_caja ?>" readonly >
	                            </div>
	                        </div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="">
								<!-- <label class="control-label" for=""> Ober </label> -->
								<textarea name="observaciones" id="observaciones" class="form-control input-sm" placeholder="Ingresar alguna obervacion"><?= $oCaja->observaciones ?></textarea>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-12 ">
							<?=  form_submit('cerrar_caja', 'Cerrar caja', 'class="btn btn-danger btn-block"'); ?>
						</div>
					</div>
	            </div>
	            <!-- panel boyd -->
	            <?= form_close(); ?>
	        </div>
		</div>
	</div>
<?php endif ?>

<script type="text/javascript">

	function cargar_modal_apertura(){
		$.ajax({
	        type: 'POST',
	        url: "<?=base_url('caja/caja/cargar_modal_apertura')?>",
	        data: "",
	        success: function(rpta){
	        	if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    if(rpta == '504'){
	                    alertify.error("El insumo no existe en la lista.");
	                    return false;
	                }else{
			        	$('#div_modal_caja').html(rpta);
			        	$('#modal_apertura').modal({
						    show: 'false'
						}); 
	                }
                }
	        },
	        error: function(rpta){
	            alert("Error en la operación");
	        }
	    });
	}
</script>