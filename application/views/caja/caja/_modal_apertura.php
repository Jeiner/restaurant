<div id="modal_apertura" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> <strong>Aperturar caja</strong> </h3>
            </div>
            <?= form_open(base_url('caja/caja/aperturar'), 'class="form-horizontal" id="myform"'); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group ">
                            <label class="col-sm-3 col-xs-12 control-label" for=""> Fecha apertura </label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="text" name="aperturado_en" id="aperturado_en" class="form-control input-sm" value="<?= $aperturado_en ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-3 col-xs-12 control-label" for=""> Aperturado por </label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="text" name="aperturado_por" id="aperturado_por" class="form-control input-sm" value="<?= $this->session->userdata('session_usuario'); ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-xs-12 control-label" for=""> Monto inicial </label>
                            <div class="col-sm-7 col-xs-12">
                                <input type="text" name="monto_inicial" id="monto_inicial" class="form-control input-sm" placeholder="Monto inicial (0.00)" onkeypress="return soloNumeroDecimal(event)">
                            </div>
                        </div>
                    </div>
                </div> <!-- row -->
            </div>
            <div class="modal-footer">
                <?=  form_submit('aperturar_caja', 'Aperturar caja', 'class="btn btn-primary"'); ?>
            </div>
             <?= form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
