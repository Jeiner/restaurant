<?php 
    $total_ventas = 0;
    $total_gastos = 0;
    $total_ventas_efectivo = 0;
    $total_ventas_tarjeta = 0;
    $total_gastos_efectivo = 0;
    $total_gastos_tarjeta = 0;
    $saldo_caja = $oCaja->monto_inicial;
?>
<div class="row">
    <div class="col-sm-3 col-sm-offset-9 col-xs-6 col-xs-offset-6">
        <div class="form-group">
            <a href="javascript:window.history.back();" class="btn btn-inverse btn-sm btn-block">
                <i class="ace-icon fa fa-arrow-left bigger-90" aria-hidden="true"></i>
                Regresar
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <strong>Movimientos en caja</strong>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 ">
                        <table class="table table-bordered" id="tabla_movimientos_caja" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center"> Fecha/Hora </th>
                                    <th style="text-align: center"> Usuario </th>
                                    <th style="text-align: center"> Operación </th>
                                    <th style="text-align: center"> Monto </th>
                                    <th style="text-align: center"> Estado </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($movimientos_caja as $key => $mov): ?>
                                    <tr>
                                        <td style="text-align: center"> <?= $mov->fecha ?> </td>
                                        <td style="text-align: center"> <?= strtoupper($mov->usuario) ?> </td>
                                        <td style="text-align: center"> <?= $mov->operacion ?> </td>
                                        <td class="dinero"> <?= $mov->monto ?> </td>
                                        <td style="text-align: center">
                                            <?php if ($mov->estado == 'A'): ?>
                                                <span class="label label-success"> Activo </span>
                                            <?php else: ?>
                                                <span class="label label-inverse"> Anulado </span>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <?php 
                                        if ($mov->operacion == "Venta") {
                                            $total_ventas_efectivo += $mov->monto;
                                        }
                                        if ($mov->operacion == "Gasto") {
                                            $total_gastos_efectivo += $mov->monto;
                                        }
                                    ?>
                                <?php endforeach ?>
                                <?php
                                    $total_ventas_efectivo = number_format(round($total_ventas_efectivo,2), 2, '.', ' ');
                                    $total_gastos_efectivo = number_format(round($total_gastos_efectivo,2), 2, '.', ' ');
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-left">
                            Total ventas efectivo: S/. <span><?= $total_ventas_efectivo ?></span>
                        </div>
                        <div class="pull-right">
                            Total gastos efectivo: S/. <span><?= $total_gastos_efectivo ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- panel boyd -->
        </div>
        <br>
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <strong>Movimientos en tarjeta</strong>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 ">
                        <table class="table table-bordered" id="tabla_movimientos_tarjeta" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center"> Fecha/Hora </th>
                                    <th style="text-align: center"> Usuario </th>
                                    <th style="text-align: center"> Operación </th>
                                    <th style="text-align: center"> Monto </th>
                                    <th style="text-align: center"> Estado </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($movimientos_tarjeta as $key => $mov): ?>
                                    <tr>
                                        <td style="text-align: center"> <?= $mov->fecha ?> </td>
                                        <td style="text-align: center"> <?= strtoupper($mov->usuario) ?> </td>
                                        <td style="text-align: center"> <?= $mov->operacion ?> </td>
                                        <td class="dinero"> <?= $mov->monto ?> </td>
                                        <td style="text-align: center">
                                            <?php if ($mov->estado == 'A'): ?>
                                                <span class="label label-success"> Activo </span>
                                            <?php else: ?>
                                                <span class="label label-inverse"> Anulado </span>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <?php 
                                        if ($mov->operacion == "Venta") {
                                            $total_ventas_tarjeta += $mov->monto;
                                        }
                                        if ($mov->operacion == "Gasto") {
                                            $total_gastos_tarjeta += $mov->monto;
                                        }
                                    ?>
                                <?php endforeach ?>
                                <?php
                                    $total_ventas_tarjeta = number_format(round($total_ventas_tarjeta,2), 2, '.', ' ');
                                    $total_gastos_tarjeta = number_format(round($total_gastos_tarjeta,2), 2, '.', ' ');
                                ?>
                                <?php
                                    $total_ventas = $total_ventas_efectivo + $total_ventas_tarjeta;
                                    $total_gastos = $total_gastos_efectivo + $total_gastos_tarjeta;
                                    
                                    $saldo_caja = $oCaja->monto_inicial + $total_ventas - $total_gastos;

                                    $total_ventas = number_format(round($total_ventas,2), 2, '.', ' ');
                                    $total_gastos = number_format(round($total_gastos,2), 2, '.', ' ');
                                    $saldo_caja = number_format(round($saldo_caja,2), 2, '.', ' ');
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-left">
                            Total ventas tarjeta: S/. <span><?= $total_ventas_tarjeta ?></span>
                        </div>
                        <div class="pull-right">
                            Total gastos tarjeta: S/. <span><?= $total_gastos_tarjeta ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- panel boyd -->
        </div>
        
    </div>
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-lock bigger-90" aria-hidden="true"></i>
                    Datos de la caja (Código: <?= $oCaja->caja_id ?>)
                </div>
            </div>
            <div class="panel-body">
                <div class="row  form-horizontal" >
                    <div class="col-sm-12 ">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Fecha de apetura </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm center" name="aperturado_en" id="aperturado_en"  value="<?= $oCaja->aperturado_en ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Aperturado por </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm center" name="aperturado_por" id="aperturado_por"  value="<?= strtoupper($oCaja->aperturado_por) ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Monto inicial </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm dinero " name="monto_inicial" id="monto_inicial"  value="<?= $oCaja->monto_inicial ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <label class="col-sm-5 control-label" style=""> Total ventas efectivo </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm dinero" name="total_ventas_efectivo" id="total_ventas_efectivo"  value="<?= $total_ventas_efectivo ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Total ventas </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm dinero" name="total_ventas" id="total_ventas"  value="<?= $total_ventas ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <label class="col-sm-5 control-label" style=""> Total gastos efectivo </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm dinero" name="total_gastos_efectivo" id="total_gastos_efectivo"  value="<?= $total_gastos_efectivo ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Total gastos </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm dinero" name="total_gastos" id="total_gastos"  value="<?= $total_gastos ?>" readonly >
                            </div>
                        </div>
                        <div id="tarjeta" class="hidden">
                            <div class="form-group">
                                <label class="col-sm-5 control-label" style=""> Total ventas tarjeta </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control input-sm dinero" name="total_ventas_tarjeta" id="total_ventas_tarjeta"  value="<?= $total_ventas_tarjeta ?>" readonly >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-5 control-label" style=""> Total gastos tarjeta </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control input-sm dinero" name="total_gastos_tarjeta" id="total_gastos_tarjeta"  value="<?= $total_gastos_tarjeta ?>" readonly >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Saldo </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm dinero-total" name="saldo_caja" id="saldo_caja"  value="<?= $saldo_caja ?>" readonly >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="">
                            <!-- <label class="control-label" for=""> Ober </label> -->
                            <textarea name="observaciones" id="observaciones" class="form-control input-sm" placeholder="Ingresar alguna obervacion" readonly><?= $oCaja->observaciones ?></textarea>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Fecha de cierre </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm center" name="cerrado_en" id="cerrado_en"  value="<?= $oCaja->cerrado_en ?>" readonly >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-5 control-label" style=""> Cerrado por </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm center" name="cerrado_por" id="cerrado_por"  value="<?= strtoupper($oCaja->cerrado_por) ?>" readonly >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- panel boyd -->
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#tabla_movimientos_caja').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "25%", },   //Hora
          { "targets": [ 1 ],"width": "20%", },   //Usuario
          { "targets": [ 2 ],"width": "15%", },   //Opera
          { "targets": [ 3 ],"width": "15%", },   //Monto
          { "targets": [ 4 ],"width": "15%", },   //Estado
        ],
        "order": [[ 0, "desc" ]] ,
    });

    $('#tabla_movimientos_tarjeta').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "25%", },   //Hora
          { "targets": [ 1 ],"width": "20%", },   //Usuario
          { "targets": [ 2 ],"width": "15%", },   //Opera
          { "targets": [ 3 ],"width": "15%", },   //Monto
          { "targets": [ 4 ],"width": "15%", },   //Estado
        ],
        "order": [[ 0, "desc" ]] ,
    });
    
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>