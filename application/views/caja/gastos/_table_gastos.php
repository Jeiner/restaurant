
<table class="table table-bordered" width="100%" id="tabla-gastos">
	<thead>
        <tr>
            <th style="text-align: center" class="hidden-xs"> Cód </th>
            <th style="text-align: center"> Fecha </th>
            <th style="text-align: center"> Usuario </th>
            <th style="text-align: center" class="hidden-xs"> Tipo </th>
            <th style="text-align: center"> Monto </th>
            <th style="text-align: left" class="hidden-xs"> Descripción </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center">  </th>
            <th style="text-align: center"> Anular. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($gastos as $key => $gasto): ?>
            <tr>
                <td style="text-align: center" class="hidden-xs"> <?= $gasto->gasto_id ?> </td>
                <td style="text-align: center"> <?= $gasto->fecha ?> </td>
                <td style="text-align: center"> <?= strtoupper($gasto->usuario) ?> </td>
                <td style="text-align: center" class="hidden-xs"> <?= $gasto->tipo_desc ?> </td>
                <td class="dinero" > <?= $gasto->monto ?> </td>
                <td style="text-align: left" class="hidden-xs"> <?= $gasto->descripcion ?> </td>
                <td style="text-align: center" >
                    <?php if ($gasto->estado == 'A'): ?>
                        <span class="label label-success"> Activo </span>
                    <?php else: ?>
                        <span class="label label-inverse"> Anulado </span>
                    <?php endif ?>
                </td>
                <td style="text-align: center">
                    <span class="" data-rel="tooltip" title="Ver detalle" >
                        <a href="<?= base_url('caja/gastos/ver/').$gasto->gasto_id ?>" class="btn btn-primary btn-minier">
                             <i class="ace-icon fa fa-search bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_anular = base_url('caja/gastos/anular/').$gasto->gasto_id ?>
                    <?php $mensaje = "¿Está seguro de anular la gasto de ".$gasto->monto." soles ?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para anular" >
                        <button href="<?= $ruta_anular ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar gasto" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-gastos').DataTable({
        "columnDefs": [
            { "targets": [ 0 ],"width": "5%", },   //id
            { "targets": [ 1 ],"width": "15%", },   //Fecha
            { "targets": [ 2 ],"width": "10%", },   //Usuario
            { "targets": [ 3 ],"width": "10%", },   //Tipo
            { "targets": [ 4 ],"width": "10%", },   //Monto
            { "targets": [ 5 ],"width": "20%", },   //Descriopcion   **
            { "targets": [ 6 ],"width": "10%", },   //estado
            { "targets": [ 7 ],"width": "10%", },   //estado
            { "targets": [ 8 ],"width": "10%", },   //Anular
        ],
        "order": [[ 1, "desc" ]] ,
  	});
    $('#tabla-gastos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>