<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#gastos">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de gastos
            </a>
        </li>
        <li>
            <a  href="<?= base_url('caja/gastos/nuevo') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo gasto
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="gastos" class="tab-pane in active">
            <?php $this->load->view('caja/gastos/_table_gastos'); ?>
        </div>
    </div>
</div>