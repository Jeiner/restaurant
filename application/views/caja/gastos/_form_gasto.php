<form method="Post" action="<?= base_url('caja/gastos/guardar') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Gasto_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" class="form-control input-sm" 
						name="gasto_id" id="gasto_id" 
						value="<?= $oGasto->gasto_id ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Fecha <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" class="form-control input-sm" 
						name="fecha" id="fecha" 
						value="<?= $oGasto->fecha ?>" 
						readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Usuario <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" class="form-control input-sm" 
						name="usuario" id="usuario" 
						value="<?= strtoupper($oGasto->usuario) ?>" 
						readonly>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Tipo <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<select class="form-control input-sm" id="tipo" name="tipo" >
						<option value="">Seleccionar...</option>
						<?php foreach ($tipos as $key => $tipo): ?>
							<?php
								$selected = "";
								if($tipo->valor == $oGasto->tipo){
									$selected = "selected";
								}
							?>
							<option value="<?= $tipo->valor ?>" <?= $selected ?>> <?= $tipo->descripcion ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Monto <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" class="form-control input-sm"
						name="monto" id="monto"  
						value="<?= $oGasto->monto ?>" 
						placeholder="Monto gastado" 
						onchange="change_monto()" 
						onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Descripción <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<textarea class="form-control input-sm" name="descripcion" id="descripcion"><?= $oGasto->descripcion ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<?php if ($oGasto->estado == 'X'): ?>
					<label class="col-sm-3 col-xs-12 control-label">
					</label>
					<label class="col-sm-12 col-xs-12 control-label pull-rigth" >
						<span class="label label-inverse" > Anulado </span>
					</label>
				<?php endif ?>
			</div>
		</div> <!-- col -->
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oGasto->gasto_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-3 col-md-2 col-sm-offset-2 col-md-offset-3">
				<button type=submit"" class="btn btn-primary btn-block btn-sm"> Guardar gasto </button>	
			</div>
		</div>
	<?php endif ?>
</form>
<script type="text/javascript">
	<?php if ($oGasto->gasto_id != 0): ?>
		$('#descripcion').attr('readonly', true);
		$('#monto').attr('readonly', true);
		$('#tipo').attr('disabled', true);
	<?php endif ?>
	function change_monto(){
		var monto = $('#monto').val();
		monto = parseFloat(monto).toFixed(2);
		if(isNaN(monto)){
			alertify.error("Valor no válido");
			$('#monto').val("0.00");
			return false;
		}
		$('#monto').val(monto);
	}
</script>








