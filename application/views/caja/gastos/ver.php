<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('caja/gastos') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de gastos
            </a>
        </li>
        <li>
            <a href="<?= base_url('caja/gastos/nuevo') ?>">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo gasto
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#editar_gasto">
            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                Detalle de gasto
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="editar_gasto" class="tab-pane active">
            <?php $this->load->view('caja/gastos/_form_gasto'); ?>
        </div>
    </div>
</div>