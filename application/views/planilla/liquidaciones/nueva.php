<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('planilla/liquidaciones') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de liquidaciones
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#nueva_liquidacion">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nueva liquidación
            </a>
        </li>
        <li>
            <a  href="<?= base_url('planilla/liquidaciones/reporte') ?>">
                <i class="ace-icon fa fa-line-chart bigger-90" aria-hidden="true"></i>
                Reporte 
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="nueva_liquidacion" class="tab-pane active">
            <?php $this->load->view('planilla/liquidaciones/_form_liquidacion' ); ?>
        </div>
    </div>
</div>