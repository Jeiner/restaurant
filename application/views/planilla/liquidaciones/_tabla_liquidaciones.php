<table class="table table-bordered" width="100%" id="tabla-liquidaciones">
	<thead>
        <tr>
            <th style="text-align: center"> Fecha pago </th>
            <th style="text-align: center"> Apellidos y nombres </th>
            <th style="text-align: center" class="hidden-xs"> Sede </th>
            <th style="text-align: center" class="hidden-xs"> motivo </th>
            <th style="text-align: center"> Monto </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($liquidaciones as $key => $liqui): ?>
            <tr class="liquidacion_inicio">
                <td style="text-align: center">
                    <span class="hidden"><?= $this->Site->rellenar_izquierda($key,0,2)  ?></span>
                    <?= substr($liqui->fecha_pago, 0,10) ?> 
                </td>
                <td style="text-align: left"> <?= $liqui->nombre_completo ?>  </td>
                <td style="text-align: left;" class="hidden-xs"> <?= $liqui->sede_desc ?> </td>
                <td style="text-align: center" class="hidden-xs"> <?= substr($liqui->motivo_desc, 0,10)  ?> </td>
                <td style="text-align: center" > <?= $liqui->total_a_pagar ?> </td>
                <td style="text-align: center">
                    <?php 
                        if($liqui->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($liqui->estado == 'X') $est_desc = '<span class="label label-inverse"> Anulado </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center" >
                    <span class="" data-rel="tooltip" title="Detalle" >
                        <a href="javascript:;" class="btn btn-primary btn-xs" onclick="ver_detalle_liquidacion(<?= $liqui->liquidacion_id ?>);">
                            <i class="ace-icon fa fa-search bigger-120" aria-hidden="true"></i>
                        </a>
                        <div style="margin-bottom: 10px" class="hidden-sm hidden-md hidden-lg"></div>
                    </span>
                    <span class="" data-rel="tooltip" title="Anular" >
                        <span style="margin-left: 15px" class="hidden-xs"></span>
                        <a href="javascript:;" class="btn btn-danger btn-xs" onclick="anular_liquidación(<?= $liqui->liquidacion_id ?>);">
                            <i class="ace-icon fa fa-trash bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>

<div id="para_modal">
    
</div>
<script type="text/javascript">
    var glob_html_detalle = ""; //Para mostrar el modal del detalle en una función ASÍNCRONA
    function anular_liquidación(liquidacion_id){
        swal({
            title: "¿Está seguro de anular la liquidación?",
            text: "Recuerde que la operación es irreversible.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, anular!",
            closeOnConfirm: false
        },
        function () {
            var url_anular = "<?= base_url('planilla/liquidaciones/anular/'); ?>" + liquidacion_id;
            window.location.href = url_anular
        });
    }
    function ver_detalle_liquidacion(liquidacion_id ){
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('planilla/liquidaciones/ajax_cargar_detalle')?>",
            data: {"liquidacion_id":liquidacion_id},
            success: function(rpta){
                glob_html_detalle = rpta;
                cerrarCargando();
                window.setTimeout("asinc_didujar_detalle()",400, rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function asinc_didujar_detalle(){
        $('#para_modal').html(glob_html_detalle);
        $('#modal_detalle').modal('show');
    }
	$('#tabla-liquidaciones').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },   //tipo doc
          { "targets": [ 1 ],"width": "25%", },   //num doc
          { "targets": [ 2 ],"width": "20%", },   //Nombrs
          { "targets": [ 3 ],"width": "10%", },   //Cago
          { "targets": [ 4 ],"width": "10%", },   //Telefono
          { "targets": [ 5 ],"width": "10%", },   //Estado
          { "targets": [ 6 ],"width": "15%", },   //Bloquear
        ],
        "order": [[0, "asc" ]] ,
  	});
    $('#tabla-liquidaciones tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>