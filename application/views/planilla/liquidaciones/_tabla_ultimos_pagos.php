<?php if (count($oPagos) <= 0): ?>
	<span style="margin-left: 10px"> No se encontraron pagos </span>
<?php else: ?>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th class="center">Fecha</th>
				<th class="center ">Importe</th>
				<th class="center">Estado</th>
				<th class="center">Ver</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($oPagos as $key => $pago): ?>
				<tr>
					<td class="center"> 
						<?php 
	                        $_fecha = date($pago->fecha_pago);
	                        $_fecha = new DateTime($_fecha);
						?>
	                    <?= $_fecha->format('d') ?> 
	                    de <?= $this->Site->meses[$_fecha->format('F')] ?> 
	                    del <?= $_fecha->format('Y') ?> 
					</td>
					<td class="dinero"> <?= $pago->total_a_pagar ?> </td>
					<td class="center">
						<?php 
	                        if($pago->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
	                        if($pago->estado == 'X') $est_desc = '<span class="label label-inverse"> Anulado </span>';
	                    ?>
	                    <?= $est_desc  ?>
					</td>
					<td class="center">
						<span class="" data-rel="tooltip" title="Ver liquidaciòn" style="margin-left: 15px">
	                        <a href="javascript:;" class="btn btn-primary btn-xs" onclick="ver_detalle_liquidacion(<?= $pago->liquidacion_id ?>);">
	                            <i class="ace-icon fa fa-search bigger-120" aria-hidden="true"></i>
	                        </a>
	                    </span>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>
<div id="para_modal">
    
</div>
<script type="text/javascript">
	var glob_html_detalle = ""; //Para mostrar el modal del detalle en una función ASÍNCRONA
	function ver_detalle_liquidacion(liquidacion_id ){
		abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('planilla/liquidaciones/ajax_cargar_detalle')?>",
            data: {"liquidacion_id":liquidacion_id},
            success: function(rpta){
            	glob_html_detalle = rpta;
            	cerrarCargando();
            	window.setTimeout('asinc_datos_detalle()',400);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function asinc_datos_detalle(){
    	$('#para_modal').html(glob_html_detalle);
        $('#modal_detalle').modal('show');
    }
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>
	