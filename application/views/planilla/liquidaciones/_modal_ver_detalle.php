<div class="modal fade" id="modal_detalle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="exampleModalLabel"> <strong> Boleta de pago </strong> </h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
        		<div class="row">
        			<div class="col-sm-6">
        				<div class="form-group">
	        				<div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Empleado: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oLiquidacion->nombre_completo ?>
	                            </div>
	                        </div>
						</div>
						<div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Cargo: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oLiquidacion->cargo_desc ?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Sede: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oLiquidacion->sede_desc ?>
	                            </div>
	                        </div>
						</div>
        			</div>
        			<div class="col-sm-6">
        				<div class="form-group">
	        				<div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Fecha pago: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oLiquidacion->fecha_pago ?>
	                            </div>
	                        </div>
						</div>
						<div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Motivo: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <?= $oLiquidacion->motivo_desc ?>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="row">
	                            <div class="col-sm-4">
	                                <strong style="color:#000;"> Total pagado: </strong>
	                            </div>
	                            <div class="col-sm-8">
	                                <strong><?= $oLiquidacion->total_a_pagar ?></strong>
	                            </div>
	                        </div>
	                    </div>
        			</div>
        		</div>
        		<hr>
        		<div class="row">
        			<div class="col-sm-6">
        				<table class="table table-bordered table-striped">
        					<thead>
        						<tr>
        							<th colspan="3"> <strong>REMUNERACIONES</strong> </th>
        						</tr>
        						<tr>
        							<th>N°</th>
        							<th>Concepto</th>
        							<th>Importe</th>
        						</tr>
        					</thead>
        					<tbody>
        						<?php $cont = 1; ?>
    							<?php foreach ($oLiquidacion->items as $key => $item): ?>
		        					<?php if ($item->tipo_concepto == "REM"): ?>
		        						<tr>
		        							<td><?= $cont++ ?></td>
		        							<td><?= $item->concepto ?></td>
		        							<td><?= $item->importe ?></td>
		        						</tr>
		        					<?php endif ?>
		        				<?php endforeach ?>
		        				<tr>
	    							<td colspan="2"><strong>TOTAL: </strong></td>
	    							<td><strong><?= $oLiquidacion->total_remuneraciones ?></strong></td>
	    						</tr>
        					</tbody>
        				</table>
        			</div>
        			<div class="col-sm-6">
        				<table class="table table-bordered table-striped">
        					<thead>
        						<tr>
        							<th colspan="3"> <strong>DESCUENTOS</strong> </th>
        						</tr>
        						<tr>
        							<th>N°</th>
        							<th>Concepto</th>
        							<th>Importe</th>
        						</tr>
        					</thead>
        					<tbody>
        						<?php $cont = 1; ?>
        						<?php foreach ($oLiquidacion->items as $key2 => $item): ?>
		        					<?php if ($item->tipo_concepto == "DES"): ?>
		        						<tr>
		        							<td><?= $cont++ ?></td>
		        							<td><?= $item->concepto ?></td>
		        							<td><?= $item->importe ?></td>
		        						</tr>
		        					<?php endif ?>
		        				<?php endforeach ?>
		        				<tr>
	    							<td colspan="2"><strong>TOTAL: </strong></td>
	    							<td><strong><?= $oLiquidacion->total_descuentos ?></strong></td>
	    						</tr>
        					</tbody>
        				</table>
        			</div>
        		</div>
        		
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      		</div>
    	</div>
  	</div>
</div>