<?php $meses = array('enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre'); ?>
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#liquidaciones">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de liquidaciones
            </a>
        </li>
        <li>
            <a  href="<?= base_url('planilla/liquidaciones/nueva') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nueva liquidación
            </a>
        </li>
        <li>
            <a  href="<?= base_url('planilla/liquidaciones/reporte') ?>">
                <i class="ace-icon fa fa-line-chart bigger-90" aria-hidden="true"></i>
                Reporte 
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="liquidaciones" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-2 col-xs-6">
                    <div class="form-group">
                        <select class="form-control input-sm" id="cbo_anual" name="cbo_anual">
                            <option value="2018" <?php if(date('Y') == 2018) echo "selected" ?> > 2018</option>
                            <option value="2019" <?php if(date('Y') == 2019) echo "selected" ?> > 2019</option>
                            <option value="2020" <?php if(date('Y') == 2020) echo "selected" ?> > 2020</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="form-group">
                        <select class="form-control input-sm" id="cbo_mes" name="cbo_mes">
                            <option value="-1"> Todos los meses </option>
                            <?php for ($i = 0; $i < sizeof($meses); $i++): ?>
                                <option value="<?= $i ?>" <?php if(date('m') == ($i+1)) echo "selected" ?> > <?= strtoupper($meses[$i]) ?> </option>
                            <?php endfor ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm" onclick="cargar_liquidaciones();"> Filtrar </button>
                    </div>
                </div>
            </div>
            <br>
            <div id="tabla_liquidaciones">
                <!--  -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
    cargar_liquidaciones();
    function cargar_liquidaciones(){
        $('#tabla_liquidaciones').html('<span class="blue bolder" style="margin: 15px;">Cargando liquidaciones...</span>');
        var anual = $('#cbo_anual').val();
        var mes_id = $('#cbo_mes').val();
        mes_id = parseInt(mes_id) + 1;
        $.ajax({
            type: 'POST',
            url: "<?=base_url('planilla/liquidaciones/ajax_tabla_liquidaciones')?>",
            data: {"anual":anual, "mes_id":mes_id},
            success: function(rpta){
                $('#tabla_liquidaciones').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>