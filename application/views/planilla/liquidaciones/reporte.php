<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('planilla/liquidaciones') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de liquidaciones
            </a>
        </li>
        <li>
            <a href="<?= base_url('planilla/liquidaciones/nueva') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nueva liquidación
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#reporte">
                <i class="ace-icon fa fa-line-chart bigger-90" aria-hidden="true"></i>
                Reporte
            </a>
        </li>
    </ul>
    <div class="tab-content" style="min-height: 700px">
        <div id="reporte" class="tab-pane active">
            <div class="row">
                <div class="col-sm-12" id="">
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">
                        <div class="row">
                            <div class="col-sm-2 col-xs-6">
                                <div class="form-group">
                                    <select class="form-control input-sm" id="cbo_anual" name="cbo_anual" onchange="crear_grafico_mensual()">
                                        <option value="2018" <?php if(date('Y') == 2018) echo "selected" ?> > 2018</option>
                                        <option value="2019" <?php if(date('Y') == 2019) echo "selected" ?> > 2019</option>
                                        <option value="2020" <?php if(date('Y') == 2020) echo "selected" ?> > 2020</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control input-sm chosen-select" id="cbo_empleado" name="cbo_empleado" onchange="crear_grafico_mensual()">
                                        <option value="0">Todos el personal...</option>
                                        <?php foreach ($empleados as $key => $empl): ?>
                                            <option value="<?= $empl->empleado_id ?>"><?= $empl->nombre_completo ?></option>
                                        <?php endforeach ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select class="form-control input-sm chosen-select" id="cbo_sede" name="cbo_sede" onchange="crear_grafico_mensual()">
                                        <option value="0">Todas las sedes...</option>
                                        <?php foreach ($sedes as $key => $sed): ?>
                                            <option value="<?= $sed->sede_id ?>"><?= $sed->sede ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm" onclick="crear_grafico_mensual();"> Filtrar </button>
                                </div>
                            </div>
                        </div>
                        <div id="container_liquidaciones">
                            
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    crear_grafico_mensual();
    function crear_grafico_mensual(){
        var array_meses = ['Ene','Feb', 'Mar', 'Abr','May','Jun','Jul', 'Ago', 'Sep','Oct', 'Nov', 'Dic']
        var array_liquidaciones = new Array(12);
        for (var i = 0; i < 12; i++) {
            array_liquidaciones[i] = 0;
        }
        anual = $('#cbo_anual').val();
        empleado_id = $('#cbo_empleado').val();
        sede_id = $('#cbo_sede').val();
        $.ajax({
            type: 'POST',
            data: {"anual": anual, "empleado_id":empleado_id, "sede_id": sede_id},
            url: '<?= base_url().'planilla/liquidaciones/ajax_liquidaciones_mensual' ?>',
            success: function(response){
                array_count = JSON.parse(response);
                array_count.forEach(function(o){
                   array_liquidaciones[o.mes-1] = parseFloat(o.importe);
                });
                dibujar_grafico_mensual(anual,array_meses,array_liquidaciones);
            },
            error: function(response){
                var msj = JSON.stringify(response);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }
    function dibujar_grafico_mensual(anual,array_meses,array_liquidaciones){
        Highcharts.chart('container_liquidaciones', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Liquidaciones mensuales del año '+anual
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: array_meses,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series:[{
                name: 'Monto de ventas (S/.)',
                data: array_liquidaciones,

            }]
        }); 
    }
</script>