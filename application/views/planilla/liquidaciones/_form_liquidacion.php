<form method="Post" action="<?= base_url('planilla/liquidaciones/guardar') ?>" class="form-vertical" id="form_liquidacion">
	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-6" hidden="hidden">
					<div class="form-group">
						<label class="" for=""> Liquidacion ID</label> 
						<input type="text" name="liquidacion_id" id="liquidacion_id" class="form-control input-sm" disabled="disabled" value="<?= $oLiquidacion->liquidacion_id ?>">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="" for=""> Fecha pago</label> 
						<input type="date" name="fecha_pago" id="fecha_pago" class="form-control input-sm" disabled="disabled" value="<?= $oLiquidacion->fecha_pago ?>">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group ">
						<label class="" for=""> Motivo </label> 
						<select class="form-control input-sm chosen-select" id="motivo" name="motivo" onchange="localStorage.setItem('motivo', this.value);">
							<option value="">Seleccionar...</option>
							<?php foreach ($motivos as $key => $mm): ?>
								<option value="<?= $mm->valor ?>" <?php if($oLiquidacion->motivo  == $mm->valor) echo 'selected' ?> >
									<?= strtoupper($mm->descripcion) ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group ">
						<label class="" for=""> Empleado  </label> 
						<select class="form-control input-sm chosen-select" id="empleado_id" name="empleado_id" onchange="seleccionar_empleado(this.value);">
							<option value="">Seleccionar...</option>
							<?php foreach ($empleados as $key => $e): ?>
								<option value="<?= $e->empleado_id ?>" <?php if($oLiquidacion->empleado_id == $e->empleado_id) echo 'selected' ?> >
									<?= strtoupper($e->nombre_completo) ?> - <?= $e->nro_documento ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="" for=""> Cargo - sede</label> 
						<input type="text" name="cargo_sede_desc" id="cargo_sede_desc" class="form-control input-sm" disabled="disabled" value="">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label class="" for=""> Base mensual</label> 
						<input type="text" name="base_mensual" id="base_mensual" class="form-control input-sm" disabled="disabled" value="">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label class="" for=""> Base semanal </label> 
						<input type="text" name="base_semanal" id="base_semanal" class="form-control input-sm" disabled="disabled" value="">
					</div>
				</div>
			</div>	
			<br>
			<div class="row">
				<div class="col-sm-12" style="">
					<span class="blue bolder" style=""> Últimos pagos</span>
					<hr style="	margin-top: 10px">
				</div>
				<div class="col-sm-12" id="ultimos_pagos">
					<!-- tabla ultimos pagos -->
				</div>
			</div>			
		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-12">
					<table class="table table-bordered" id="tabla_remuneraciones">
						<thead>
							<tr >
								<th colspan="3" > <span class="blue bolder "> Remuneraciones (+) </span> </th>
							</tr>
							<tr>
								<th width="60%"><strong>Concepto</strong></th>
								<th width="30%" class="center"><strong>Importe</strong></th>
								<th width="10%"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($conceptos as $key => $con): ?>
								<?php if ($con->tipo_concepto == "REM"): ?>
									<tr>
										<td> 
											<input type="" name="concepto_rem_id[]" value="<?= $con->concepto_id ?>" hidden>
											<?= $con->concepto ?> 
										</td>
										<td>
											<input type="text" class="form-control input-sm dinero" 
												id="concepto_<?= $con->concepto_id ?>"
												name="conceptos_rem[]"
												onkeypress="return soloNumeroDecimal(event);"
												onchange="agregar_rem(<?= $con->concepto_id ?>,this.value);"
												onclick="if(this.value == '0.00') this.value = ''" 
												onblur="if(this.value == '') this.value = ''"
												>
										</td>
										<td></td>
									</tr>
								<?php endif ?>
							<?php endforeach ?>
							<tr>
								<th width="60%" class="center" ><strong>Total Remuneraciones</strong> </th>
								<th width="30%">
									<input type="text" class="form-control input-sm dinero" name="total_remuneraciones" id="total_remuneraciones"  value="" readonly="">
								</th>
								<th width="10%"></th>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-12" id="btn_descuentos">
					<a href="javascript:;" class="pull-right" onclick="habilitar_descuentos();">
						Aplicar descuentos
					</a>
				</div>
				<div class="col-sm-12" style="display: none" id="div_descuentos">
					<table class="table table-bordered" id="tabla_descuentos">
						<thead>
							<tr >
								<th colspan="3" > <span class="blue bolder "> Descuentos (-) </span> </th>
							</tr>
							<tr>
								<th width="60%"><strong>Concepto</strong></th>
								<th width="30%" class="center"><strong>Importe</strong></th>
								<th width="10%"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($conceptos as $key => $con): ?>
								<?php if ($con->tipo_concepto == "DES"): ?>
									<tr>
										<td> 
											<input type="" name="concepto_des_id[]" value="<?= $con->concepto_id ?>" hidden>
											<?= $con->concepto ?> 
										</td>
										<td>
											<input type="text" class="form-control input-sm dinero" 
												id="concepto_<?= $con->concepto_id ?>"
												name="conceptos_des[]"
												onkeypress="return soloNumeroDecimal(event);"
												onchange="agregar_des(<?= $con->concepto_id ?>,this.value);"
												onclick="if(this.value == '0.00') this.value = ''" 
												onblur="if(this.value == '') this.value = ''"
												>
										</td>
										<td></td>
									</tr>
								<?php endif ?>
							<?php endforeach ?>
							<tr>
								<th width="60%" class="center" ><strong>Total descuentos</strong> </th>
								<th width="30%">
									<input type="text" class="form-control input-sm dinero" name="total_descuentos" id="total_descuentos"  value="" readonly="">
								</th>
								<th width="10%"></th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-sm-4 col-sm-offset-8">
					<div class="form-group">
		                <label class="control-label" style=""> <strong>Total a pagar</strong> </label>
		                <div class="">
		                    <input type="text" class="form-control input-sm dinero-total" name="importe_total" id="importe_total" value="26.00" style="font-size: 18px!important;text-align:center;font-weight: bold;" readonly="">
		                </div>
		            </div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label class="" for=""> Observaciones </label> 
						<textarea class="form-control" id="observaciones" name="observaciones" placeholder="Ingrese algun comentario"onchange="localStorage.setItem('observaciones', this.value);"></textarea>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<button type="button" class="btn btn-primary btn-sm btn-block" onclick="reiniciar_formulario();">
							<i class="ace-icon fa fa-refresh bigger-90" aria-hidden="true"></i>
							Reiniciar Formulario
						</button>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-sm btn-block" id="btn_guardar_liquidacion">
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Guardar pago
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<!-- Variables globales -->
<script type="text/javascript">
	var gb_conceptos = '<?= json_encode($conceptos) ?>'; //Variable global  de conceptos
	var gb_empleados = '<?= json_encode($empleados) ?>'; //Variable global  de conceptos
	var gb_array_conceptos = JSON.parse(gb_conceptos); //Pasamos los conceptos a una variable de javascript array
	var gb_array_empleados = JSON.parse(gb_empleados); //Pasamos los conceptos a una variable de javascript array
</script>
<!-- Verificar almacen local -->
<script type="text/javascript">
	$("#form_liquidacion").submit(function() {
		empleado_id = $('#empleado_id').val();
		if (empleado_id == "") {
			alert("Seleccione el empleado.");
			$('#btn_guardar_liquidacion').attr('disabled', false);   // disables button
			return false;
		}else{
			$('#btn_guardar_liquidacion').attr('disabled', true);        // enables button
		}
    });
	<?php if ($this->session->userdata('remove_local_liquidacion')): ?>
        remove_local_liquidacion();
        <?php $this->session->unset_userdata('remove_local_liquidacion'); ?>
    <?php endif ?>
    function remove_local_liquidacion(){
        localStorage.removeItem('motivo');
        localStorage.removeItem('empleado_id');
        localStorage.removeItem('cargo_sede_desc');
        localStorage.removeItem('sueldo_base_mensual');
        localStorage.removeItem('sueldo_base_semanal');
        localStorage.removeItem('observaciones');
        localStorage.removeItem('lista_rem');
        localStorage.removeItem('lista_des');
    }
	function reiniciar_formulario(){
		swal({
            title: "¿Está seguro de reiniciar el formulario?",
            text: "Perderá los datos actuales.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, reiniciar!",
            closeOnConfirm: false
        },
        function () {
			remove_local_liquidacion();
			location.reload();
        });
	}
	function habilitar_descuentos(){
		$('#div_descuentos').show('fast');
		$('#btn_descuentos').hide('fast');
	}
</script>
<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true});
    cargar_local_storage();

    function cargar_local_storage(){
     	$('#empleado_id').val(localStorage.getItem("empleado_id"));
        $('#empleado_id').trigger('chosen:updated');
        if (localStorage.getItem("empleado_id") !== null) {
        	cargar_ultimos_pagos(localStorage.getItem("empleado_id"));
        }
        var cargo_sede_desc = (localStorage.getItem("cargo_sede_desc") === null) ? '' : localStorage.getItem("cargo_sede_desc");
        var base_mensual = (localStorage.getItem("base_mensual") === null) ? '' : localStorage.getItem("base_mensual");
        var base_semanal = (localStorage.getItem("base_semanal") === null) ? '' : localStorage.getItem("base_semanal");
        var motivo = (localStorage.getItem("motivo") === null) ? 'SEM' : localStorage.getItem("motivo");
        var observaciones = (localStorage.getItem("motivo") === null) ? '' : localStorage.getItem("observaciones");
        $('#cargo_sede_desc').val(cargo_sede_desc);
        $('#base_mensual').val(base_mensual);
        $('#base_semanal').val(base_semanal);
        $('#observaciones').val(observaciones);
        $('#motivo').val(motivo);
        $('#motivo').trigger('chosen:updated');
    	load_items();
    }
    function load_items(){
    	var lista_rem = (localStorage.getItem('lista_rem') === null) ? []: JSON.parse(localStorage.getItem('lista_rem'));
    	var lista_des = (localStorage.getItem('lista_des') === null) ? []: JSON.parse(localStorage.getItem('lista_des'));
    	var importe_total_rem = 0;
    	var importe_total_des = 0;
    	var importe_total = 0;
    	// Remuneraciones
     	$.each(lista_rem, function () {
     		if (this.signo == 1) {
     			var input_concepto_id = "#concepto_"+this.concepto_id;
	     		if(this.importe == '' || this.importe === null){
	     			$(input_concepto_id).val();
	     		}else{
	     			$(input_concepto_id).val(parseFloat(this.importe).toFixed(2));	
	     			importe_total_rem += parseFloat(this.importe);
	     		}
     		}
        });
        $('#total_remuneraciones').val(parseFloat(importe_total_rem).toFixed(2));
        // Descuentos
        $.each(lista_des, function () {
     		if (this.signo == -1) {
     			var input_concepto_id = "#concepto_"+this.concepto_id;
	     		if(this.importe == '' || this.importe === null){
	     			$(input_concepto_id).val("");
	     		}else{
	     			$(input_concepto_id).val(parseFloat(this.importe).toFixed(2));	
	     			importe_total_des += parseFloat(this.importe);
	     		}
     		}
        });
        $('#total_descuentos').val(parseFloat(importe_total_des).toFixed(2));
        importe_total = importe_total_rem - importe_total_des;
        $('#importe_total').val(parseFloat(importe_total).toFixed(2));
    }
    function agregar_rem(concepto_id, importe){
    	var lista_rem = (localStorage.getItem('lista_rem') === null) ? []: JSON.parse(localStorage.getItem('lista_rem'));
        var cant_rem = (localStorage.getItem('lista_rem') === null) ? 0 : lista_rem.length;

        // Buscamos al concepto
        var concepto = null;
        for (var i = 0 ; i < gb_array_conceptos.length; i++) {
            if (gb_array_conceptos[i].concepto_id == concepto_id){
                concepto = gb_array_conceptos[i];
            }
        }
        if (concepto === null) {
            alertify.error("Concepto no encontrado.");
            return false;
        }
        // Verificamos si esta seleccionado
        var existe = false; var item = null;
        $.each(lista_rem, function () {
            if (this.concepto_id == concepto_id) {
                existe = true;
                item = this;
                return ;
            }
        });
        if (existe){
            // Si esta seleccionado
            item.importe = parseFloat(importe);
        }else{
            // Si no esta en la lista
            new_item = concepto;
            new_item.importe = parseFloat(importe);
            lista_rem[cant_rem] = new_item;
        }
        localStorage.setItem('lista_rem', JSON.stringify(lista_rem));
        load_items();
    }
    function agregar_des(concepto_id, importe){
    	var lista_des = (localStorage.getItem('lista_des') === null) ? []: JSON.parse(localStorage.getItem('lista_des'));
        var cant_des = (localStorage.getItem('lista_des') === null) ? 0 : lista_des.length;
        // Buscamos al concepto
        var concepto = null;
        for (var i = 0 ; i < gb_array_conceptos.length; i++) {
            if (gb_array_conceptos[i].concepto_id == concepto_id){
                concepto = gb_array_conceptos[i];
            }
        }
        if (concepto === null) {
            alertify.error("Concepto no encontrado.");
            return false;
        }
        // Verificamos si esta seleccionado
        var existe = false; var item = null;
        $.each(lista_des, function () {
            if (this.concepto_id == concepto_id) {
                existe = true;
                item = this;
                return;
            }
        });
        if (existe){
            // Si esta seleccionado
            item.importe = parseFloat(importe);
        }else{
            // Si no esta en la lista
            new_item = concepto;
            new_item.importe = parseFloat(importe);
            lista_des[cant_des] = new_item;
        }
        localStorage.setItem('lista_des', JSON.stringify(lista_des));
        load_items();
    }
    function seleccionar_empleado(empleado_id){
    	var cargo_sede_desc = "";
        var sueldo_base_mensual = "";
        var sueldo_base_semanal = "";
    	if(empleado_id != ''){
    		var empleados = null;
    		for (var i = 0 ; i < gb_array_empleados.length; i++) {
	            if (gb_array_empleados[i].empleado_id == empleado_id){
	                empleado = gb_array_empleados[i];
	            }
	        }
	        sede_desc = empleado.sede_desc;
	        cargo_desc = empleado.cargo_desc;
	        cargo_sede_desc = cargo_desc + " // " + sede_desc;
	        sueldo_base_mensual = empleado.sueldo_base_mensual;
	        sueldo_base_semanal = empleado.sueldo_base_semanal;
    	}

        $('#cargo_sede_desc').val(cargo_sede_desc);
        $('#base_mensual').val(sueldo_base_mensual);
        $('#base_semanal').val(sueldo_base_semanal);

        localStorage.setItem('empleado_id', empleado_id);
        localStorage.setItem('cargo_sede_desc', cargo_sede_desc);
        localStorage.setItem('sueldo_base_mensual', sueldo_base_mensual);
        localStorage.setItem('sueldo_base_semanal', sueldo_base_semanal);
        cargar_ultimos_pagos(empleado_id);
    }
    function cargar_ultimos_pagos(empleado_id){
    	$('#ultimos_pagos').html('<span class="blue bolder" style="margin: 15px;">Cargando pagos...</span>');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('planilla/liquidaciones/ajax_ultimos_pagos')?>",
            data: {"empleado_id":empleado_id},
            success: function(rpta){
                $('#ultimos_pagos').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>