<form method="Post" action="<?= base_url('planilla/Empleados_crud/guardar') ?>" class="form-horizontal" id="form_empleado">
	<div class="row">
		<div class="col-sm-4">
			<div class="row">
				<div class="col-sm-12">
					<h3>Datos personales</h3>
					<hr>
				</div>
			</div>
			<div class="form-group hidden">
				<div class="col-sm-8 col-xs-12">
					<label>empleado_id</label>
					<input type="text" name="empleado_id" id="empleado_id" class="form-control input-sm" value="<?= $oEmpleado->empleado_id ?>" placeholder="Nombres">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Nro. documento <label class="red">*</label></label> 
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-12 col-xs-12">
							<input type="text" name="nro_documento" id="nro_documento" class="form-control input-sm" value="<?= $oEmpleado->nro_documento ?>" placeholder="00000000" maxlength="15" onkeypress="return soloNumeroEntero(event)" >
						</div>
					</div>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Nombres <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="nombres" id="nombres" class="form-control input-sm" value="<?= $oEmpleado->nombres ?>" style="text-transform:uppercase">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Ape. paterno <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="ape_paterno" id="ape_paterno" class="form-control input-sm" value="<?= $oEmpleado->ape_paterno ?>" style="text-transform:uppercase">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Ape. materno <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="ape_materno" id="ape_materno" class="form-control input-sm" value="<?= $oEmpleado->ape_materno ?>" style="text-transform:uppercase">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> F. nacimiento <label class="red">*</label> </label> 
				<div class="col-sm-4 col-xs-12">
					<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control input-sm" value="<?= $oEmpleado->fecha_nacimiento ?>" onblur="cambiar_fecha_nac()">
				</div>
				<div class="col-sm-4 col-xs-12">
					<input type="text" name="edad" id="edad" class="form-control input-sm" readonly="readonly">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Género </label> 
				<div class="col-sm-6 col-xs-12">
					<select class="form-control input-sm chosen-select" id="sexo" name="sexo">
						<option value="">Seleccionar...</option>
						<option value="M" <?php if(strtoupper($oEmpleado->sexo) == 'M') echo 'selected' ?> > MASCULINO </option>
						<option value="F" <?php if(strtoupper($oEmpleado->sexo) == 'F') echo 'selected' ?> > FEMENINO </option>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Telf./Cel. </label> 
				<div class="col-sm-6 col-xs-12">
					<input type="text" name="telefono_1" id="telefono_1" class="form-control input-sm" value="<?= $oEmpleado->telefono_1 ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Correo </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="correo" id="correo" class="form-control input-sm" value="<?= $oEmpleado->correo ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Dirección </label> 
				<div class="col-sm-8 col-xs-12">
					<textarea class="form-control input-sm" id="direccion" name="direccion" rows="3"><?= $oEmpleado->direccion ?></textarea>
				</div>
			</div>
		</div>

		<div class="col-sm-5">
			<div class="row">
				<div class="col-sm-12">
					<h3>Datos laborales</h3>
					<hr>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Sucursal <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm chosen-select" id="sucursal_codigo" name="sucursal_codigo">
						<option value="" >Seleccionar...</option>
						<?php foreach ($lstSucursales as $key => $oSucursal): ?>
							<?php
								$optSucView = $oSucursal->sucursal_codigo." | ".strtoupper($oSucursal->empresa_nombre)." - ".strtoupper($oSucursal->sucursal_nombre);
								$optSucSelect = ($oEmpleado->sucursal_codigo == $oSucursal->sucursal_codigo)? "selected" : "";
							?>
							<option value="<?= $oSucursal->sucursal_codigo ?>" <?= $optSucSelect ?>><?= $optSucView ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Cargo <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm chosen-select" id="cargo_id" name="cargo_id">
						<option value="">Seleccionar...</option>
						<?php foreach ($cargos as $key => $oCargo): ?>
							<?php
								$optCargoView = $oCargo->cargo_id." | ".strtoupper($oCargo->cargo);
								$optCargoSelect = ($oEmpleado->cargo_id == $oCargo->cargo_id)? "selected" : "";
							?>
							<option value="<?= $oCargo->cargo_id ?>" <?= $optCargoSelect ?> > <?= $optCargoView ?> </option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Sueldo base mensual <label class="red">*</label></label> 
				<div class="col-sm-4 col-xs-12">
					<input type="text" name="sueldo_base_mensual" id="sueldo_base_mensual" class="form-control input-sm" value="<?= $oEmpleado->sueldo_base_mensual ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Sueldo base semanal</label> 
				<div class="col-sm-4 col-xs-12">
					<input type="text" name="sueldo_base_semanal" id="sueldo_base_semanal" class="form-control input-sm" value="<?= $oEmpleado->sueldo_base_semanal ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Día de pago</label> 
				<div class="col-sm-4 col-xs-12">
					<input type="number" name="dia_pago" id="dia_pago" class="form-control input-sm" value="<?= $oEmpleado->dia_pago ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Inicio - Termino </label> 
				<div class="col-sm-4 col-xs-12">
					<input type="date" name="fecha_ingreso" id="fecha_ingreso" class="form-control input-sm" value="<?= $oEmpleado->fecha_ingreso ?>">
				</div>
				<div class="col-sm-4 col-xs-12">
					<input type="date" name="fecha_salida" id="fecha_salida" class="form-control input-sm" value="<?= $oEmpleado->fecha_salida ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Tipo contrato </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm chosen-select" id="tipo_contrato" name="tipo_contrato">
						<option value="">Seleccionar...</option>
						<?php foreach ($tipos_contratos as $key => $tipo_c): ?>
							<option 
								value="<?= $tipo_c->valor ?>" 
								<?php if($oEmpleado->tipo_contrato == $tipo_c->valor) echo 'selected' ?> >
								<?= strtoupper($tipo_c->descripcion) ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Cuenta bancaria </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm chosen-select" id="cuenta_bancaria" name="cuenta_bancaria">
						<option value="">Seleccionar...</option>
						<?php foreach ($bancos as $key => $banco): ?>
							<option 
								value="<?= $banco->valor ?>"
								<?php if($oEmpleado->cuenta_bancaria == $banco->valor) echo 'selected' ?> >
								<?= strtoupper($banco->descripcion)  ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Nro cuenta </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="nro_cuenta" id="nro_cuenta" class="form-control input-sm" value="<?= $oEmpleado->nro_cuenta ?>">
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Estado <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="estado" name="estado">
						<option value="">Seleccionar...</option>
						<option value="A" <?php if(strtoupper($oEmpleado->estado) == 'A') echo 'selected' ?> > ACTIVO </option>
						<option value="I" <?php if(strtoupper($oEmpleado->estado) == 'I') echo 'selected' ?> > INACTIVO </option>
						<option value="X" <?php if(strtoupper($oEmpleado->estado) == 'X') echo 'selected' ?> > BLOQUEADO </option>
						<option value="L" <?php if(strtoupper($oEmpleado->estado) == 'L') echo 'selected' ?> > PEND. DATOS LABORALES </option>
					</select>
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Observaciones </label> 
				<div class="col-sm-8 col-xs-12">
					<textarea class="form-control input-sm" id="observaciones" name="observaciones" rows=""><?= $oEmpleado->observaciones ?></textarea>
				</div>
			</div>

		</div>

		<div class="col-sm-3">
			<div class="row">
				<div class="col-sm-12">
					<h3>Datos de usuario</h3>
					<hr>
				</div>
			</div>
			<div class="form-group ">
			 	<div>
			 		<?php
			 			$crearUsuInactivo = "disabled";
			 			if ($oEmpleado->empleado_id == 0 || $oEmpleado->empleado_id == "") {
			 				$crearUsuInactivo = "";
			 			}
			 		?>
	                <label  class="col-sm-12 col-xs-12 control-label" for="crear_usuario">
	                	<a href="javascript:;" style="text-decoration: underline;" onclick="ajax_generar_nombre_usuario(<?= $oEmpleado->empleado_id ?>);">
	                		Generar nombre usuario
	                	</a>
	            	</label>
	            </div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Usuario <label class="red">*</label></label> 
				<div class="col-sm-8 col-xs-12">
					<?php
						$nombre_usuario = "";
						if(isset($oEmpleado->oUsuario) && $oEmpleado->oUsuario != null){
							$nombre_usuario = $oEmpleado->oUsuario->usuario;
						}
					?>
					<input type="text" name="flag_usuario" id="flag_usuario" class="form-control input-sm" value="<?= $nombre_usuario ?>" readonly>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Rol usuario<label class="red">*</label></label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="flag_usuario_rol" name="flag_usuario_rol" <?= $crearUsuInactivo ?>>
						<option value="">Seleccionar..</option>
						<?php foreach ($lstRoles as $key => $oRol): ?>
							<?php
								$rol_selected = "";
								if(isset($oEmpleado->oUsuario) && $oEmpleado->oUsuario != null){
									$rol_selected = ($oEmpleado->oUsuario->rol_id == $oRol->rol_id) ? "selected" : "";
								}
							?>
							<option value="<?= $oRol->rol_id ?>"<?= $rol_selected ?> > <?= $oRol->rol_id ?> | <?= $oRol->rol ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Estado <label class="red">*</label></label> 
				<div class="col-sm-8 col-xs-12">
					<?php
						$estado_usuario = "ACTIVO";
						if(isset($oEmpleado->oUsuario) && $oEmpleado->oUsuario != null){
							$estado_usuario = ($oEmpleado->oUsuario->estado == "A") ? "ACTIVO" : $estado_usuario;
							$estado_usuario = ($oEmpleado->oUsuario->estado == "X") ? "BLOQUEADO" : $estado_usuario;
							
						}
					?>
					<input type="text" name="flag_usuario_estado" id="flag_usuario_estado" class="form-control input-sm" value="<?= $estado_usuario ?>" readonly>
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oEmpleado->empleado_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-3 col-md-2 col-sm-offset-9 col-md-offset-10">
				<button class="btn btn-primary btn-block btn-sm" id="btn_guardar_empleado"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar empleado 
				</button>	
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-3 col-md-2 col-xs-12">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="javascript:;" class="btn btn-danger btn-block btn-sm " onclick="eliminar_empleado(<?= $oEmpleado->empleado_id ?>);">
							<i class="ace-icon fa fa-trash bigger-90" aria-hidden="true"></i>
							Dar de baja empleado 
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-2 col-xs-12 col-sm-offset-4 col-md-offset-6">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="<?= base_url('planilla/empleados') ?>" class="btn btn-inverse btn-block btn-sm ">
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Cancelar edición 
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-2 col-xs-12 " >
				<div class="form-group">
					<div class="col-xs-12">
						<button class="btn btn-primary btn-block btn-sm">
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Actualizar empleado 
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
</form>
<script type="text/javascript">

	$("#form_empleado").submit(function() {
	 	if ($('#form_empleado').valid()) {
	 		if(validar_campos_adicionales()){
	 			$('#btn_guardar_empleado').attr('disabled', true);
	 			return true;
	 		}else{
	 			$('#btn_guardar_empleado').attr('disabled', false);
	 			return false;
	 		}
        } else {
            $('#btn_guardar_empleado').attr('disabled', false);
            return false;
        }
    });

	$("#form_empleado").validate({
        rules: {
        	nro_documento: "required",
            nombres: "required",
            ape_paterno: "required",
            ape_materno: "required",
            fecha_nacimiento: "required",
            sucursal: "required",
            cargo: "required",
            sueldo_base_mensual	: "required",
            estado: "required",
            flag_usuario: "required",
            flag_usuario_rol: "required",
        },
        messages:{
        	nro_documento: "Ingresar documento",
            nombres: "Ingresar nombre",
            ape_paterno: "Ingresar apellido paterno",
            ape_materno: "Ingresar apellido materno",
            fecha_nacimiento: "Ingresar fecha nacimiento",
            sucursal: "Seleccionar la sucursal",
            cargo: "Seleccionar el cargo",
            sueldo_base_mensual: "Ingresar sueldo mensual",
            estado: "Seleccionar estado",
            flag_usuario: "Ingresar usuario",
            flag_usuario_rol: "Seleccionar Rol de usuario",
        }
    });

    function validar_campos_adicionales(){
    	let cargo_id = $('#cargo_id').val();
    	
    	if(cargo_id == 0 || cargo_id == ""){
    		alert("Seleccionar cargo.");
    		return false;
    	}

    	return true;
    }

    function eliminar_empleado(empleado_id){
    	swal({
            title: "¿Seguro de eliminar al empleado?",
            text: "Recuerde que la operación es irreversible",
            type: "warning",
            // inputType: "password",
            showCancelButton: true,
            closeOnConfirm: false,
            // inputPlaceholder: "Código de seguridad"
        }, function () {
        	var url_eliminar = "<?= base_url('planilla/empleados/inactivar/'); ?>" + empleado_id;
    		window.location.href = url_eliminar

        });
    }
</script>


<!--  -->
<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true}); 
	function cambiar_fecha_nac(){
		var fecha = $('#fecha_nacimiento').val();
		var edad = calcularEdad(fecha);
		$('#edad').val(edad);
	}
	function consultar_doc(){
		return alert("No permitido.");
        var documento = $('#nro_documento').val();
        if(documento.length != 8 ){
        	alertify.error("DNI no válido.");
        	return false;
        }
        url = '<?= base_url('pide/pide/reniec/')?>' + documento;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(objeto.status != 1){
                    alertify.error(objeto.message);
                    limpiar_datos();
                    cerrarCargando();
                    return false;
                }else{
                	nombres = objeto.resultado.NOMBRES;
                    ape_paterno = objeto.resultado.APPAT;
                    ape_materno = objeto.resultado.APMAT;
                    direccion = objeto.resultado.DIRECCION;
                    ubigeo = objeto.resultado.UBIGEO;
                    foto = objeto.resultado.FOTObase64;

                    $('#nombres').val(nombres);
                    $('#ape_paterno').val(ape_paterno);
                    $('#ape_materno').val(ape_materno);
                    $('#direccion').val(ubigeo+" - "+direccion);
                    // $('#foto_empleado').attr('src',foto);
                    // $('#foto').val(foto);
	                
	                $('#btn_consultar').hide();
		        	$('#btn_editar').show();
		        	$('#nro_documento').attr('readonly', true);
		        	$('#nombres').attr('readonly', true);
		        	$('#ape_paterno').attr('readonly', true);
		        	$('#ape_materno').attr('readonly', true);
                }
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function limpiar_datos(){
    	// $('#foto').val(foto);
    	// $('#foto_empleado').attr('src','<?= base_url('assets/img/user.png'); ?>');
    	$('#nro_documento').val("");
        $('#nombres').val("");
        $('#ape_paterno').val("");
        $('#ape_materno').val("");
        $('#btn_consultar').show();
        $('#btn_editar').hide();

        $('#nombres').attr('readonly', false);
        $('#ape_paterno').attr('readonly', false);
        $('#ape_materno').attr('readonly', false);
        $('#nro_documento').attr('readonly', false);
    }

    function ajax_generar_nombre_usuario(empleado_id){
    	if(empleado_id != 0){
    		alertify.error("Opción valida para nuevos empleados.");
    		return false;
    	}
    	let nombres = $("#nombres").val();
    	let ape_paterno = $("#ape_paterno").val();
    	let ape_materno =  $("#ape_materno").val();

    	if(nombres == "" || ape_paterno == "" || ape_materno == ""){
    		alertify.error("Debe ingresar datos personales.");
    		return false;
    	}

    	$.ajax({
            type: 'POST',
            url: "<?= base_url('configuracion/usuarios_crud/ajax_generar_nombre_usuario') ?>",
            data: { 
        		"nombres": nombres, 
        		"ape_paterno": ape_paterno, 
        		"ape_materno": ape_materno 
            },
            success: function(rpta){
            	objeto = JSON.parse(rpta);
                if(objeto.success){
                    $("#flag_usuario").val(objeto.data);
                    alertify.success("Usuario generado correctamente.");
                }else{
                	alertify.error(objeto.message);
                }
            },
            error: function(rpta){
                alert("Error de conexión.");
            }
        });
    }

</script>



<!-- Al finalizar de cargar todos los datos. -->
<script type="text/javascript">
	<?php if ($oEmpleado->empleado_id >0 ): ?>
		cambiar_fecha_nac();
	<?php endif ?>
</script>