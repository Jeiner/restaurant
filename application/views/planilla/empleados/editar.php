<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('planilla/empleados') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de empleados
            </a>
        </li>
        <li>
            <a href="<?= base_url('planilla/empleados/nuevo') ?>">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo empleado
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#editar_empleado">
            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                Editar empleado
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="editar_empleado" class="tab-pane active">
            <?php $this->load->view('planilla/empleados/_form_empleado'); ?>
        </div>
    </div>
</div>