
<?php if ($nroEmpleadosPendientes > 0): ?>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-12" >
            <span class="label label-warning">
                <?= $nroEmpleadosPendientes ?> Pendientes para completar Datos laborales.
            </span>
        </div>
    </div>
<?php endif ?>
<?php if ($nroEmpleadosSinUsuario > 0): ?>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-sm-12" >
            <span class="label label-warning">
                <?= $nroEmpleadosSinUsuario ?> Pendientes para crear usuario.
            </span>
        </div>
    </div>
<?php endif ?>

<table class="table table-bordered" width="100%" id="tabla-empleados">
	<thead>
        <tr>
            <th style="text-align: center"> Cód </th>
            <th style="text-align: center" class="hidden-xs"> Nro doc </th>
            <th style="text-align: center"> Apellidos y nombres </th>
            <th style="text-align: center" class="hidden-xs"> Sucursal </th>
            <th style="text-align: left" class="hidden-xs"> Cargo </th>
            <th style="text-align: left"> Usuario </th>
            <th style="text-align: center"> Teléfono </th>
            <th style="text-align: center" > Estado </th>
            <th style="text-align: center"> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lstEmpleados as $key => $oEmpleado): ?>
            <tr>
                <td style="text-align: center"> <?= $oEmpleado->empleado_id ?> </td>
                <td style="text-align: center" class="hidden-xs"> <?= $oEmpleado->nro_documento ?> </td>
                <td style="text-align: left"> <?= $oEmpleado->nombre_completo ?>  </td>
                <td style="text-align: left;" class="hidden-xs">
                    <?php if ($oEmpleado->oSucursal != null): ?>
                        <?= $oEmpleado->oSucursal->sucursal_codigo ?> | <?= $oEmpleado->oSucursal->empresa_nombre ?> - <?= $oEmpleado->oSucursal->sucursal_nombre ?>
                    <?php endif ?>
                </td>
                <td style="text-align: left"class="hidden-xs">
                    <?php if ($oEmpleado->oCargo != null): ?>
                        <?= $oEmpleado->oCargo->cargo_id ?> | <?= $oEmpleado->oCargo->cargo ?>
                    <?php endif ?>
                </td>
                <td style="text-align: left">
                    <?php if ($oEmpleado->oUsuario != null): ?>
                        <?= $oEmpleado->oUsuario->usuario ?>
                    <?php else: ?>
                        <?php 
                            $urlCrearUsuarioo = base_url('configuracion/usuarios/nuevo')."?empleado_id=".$oEmpleado->empleado_id;
                        ?>
                        <a href="<?= $urlCrearUsuarioo ?>">
                            Crear usuario
                        </a>
                    <?php endif ?>
                </td>
                <td style="text-align: center" > <?= $oEmpleado->telefono_1 ?> </td>
                <td style="text-align: center">
                    <?php 
                        if($oEmpleado->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($oEmpleado->estado == 'X') $est_desc = '<span class="label label-danger"> Bloqueado </span>';
                        if($oEmpleado->estado == 'I') $est_desc = '<span class="label label-danger"> Inactivo </span>';
                        if($oEmpleado->estado == 'L') $est_desc = '<span class="label label-warning"> Pend. Datos laborales  </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('planilla/empleados/editar/').$oEmpleado->empleado_id ?>
                     <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-empleados').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },   //Cod
          { "targets": [ 1 ],"width": "10%", },   //tipo doc
          { "targets": [ 2 ],"width": "30%", },   //num doc
          { "targets": [ 3 ],"width": "20%", },   //Nombrs
          { "targets": [ 4 ],"width": "10%", },   //Cago
          { "targets": [ 5 ],"width": "10%", },   //Telefono
          { "targets": [ 6 ],"width": "10%", },   //Estado
          { "targets": [ 7 ],"width": "5%", },   //Bloquear
        ],
        "order": [[ 0, "desc" ]] ,
  	});
    $('#tabla-empleados tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>