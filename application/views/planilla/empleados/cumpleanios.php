<?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>
<div class="row">
	<div class="col-sm-5">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                	<i class="ace-icon fa fa-birthday-cake bigger-90" aria-hidden="true"></i>
                   	Próximos cumpleaños
                </div>
            </div>
            <div class="panel-body">
            	<div id="profile-feed-1" class="profile-feed ace-scroll" style="position: relative;">
            		<?php foreach ($proximos as $key => $prox): ?>
            				<div class="profile-activity clearfix">
								<div>
									<img class="pull-left" alt="" src="<?php if( $prox->foto == '') echo base_url('assets/img/user.png'); else echo $prox->foto  ?>">
									<a class="name" href="#"><strong> <?= $prox->nombre_completo ?> </strong> </a>
									<div class="time" style="color: #000">
										<i class="ace-icon fa fa-calendar bigger-110"></i>
										<?php 
					                      $anual = substr($prox->fecha_nacimiento,0, 4);
					                      $mes = substr($prox->fecha_nacimiento,5, 2);
					                      $dia = substr($prox->fecha_nacimiento,8, 2);
					                      // echo $prox->fecha_nacimiento;
					                      $fecha_cumple = date('Y').'-'.$mes.'-'.$dia;
					                      $fecha_cumple = new DateTime($fecha_cumple);
					            				?>
					        				    <?= strtoupper($this->Site->dias[$fecha_cumple->format('l')]) ?> 
					                            <?= $fecha_cumple->format('d') ?> 
					                            de <strong><?= strtoupper($this->Site->meses[$fecha_cumple->format('F')]) ?> </strong>
					                            del <?= $fecha_cumple->format('Y') ?> 
									</div>
								</div>
							</div>
            		<?php endforeach ?>
            	</div>
            </div>
        </div>
	</div>
	<div class="col-sm-7">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                    Todos los cumpleaños
                </div>
            </div>
            <div class="panel-body">
        		<div class="widget-main padding-8">
					<div id="profile-feed-1" class="profile-feed" style="position: relative;">
						<?php foreach ($empleados as $key => $empleado): ?>
							<div class="profile-activity clearfix">
								<div>
									<img class="pull-left" alt="" src="<?php if( $empleado->foto == '') echo base_url('assets/img/user.png'); else echo $empleado->foto  ?>" class="img-responisve">
									<a class="name" href="#"><strong> <?= $empleado->nombre_completo ?> </strong> </a>
									<div class="time" style="color: #000">
										<i class="ace-icon fa fa-calendar bigger-110"></i>
										<?php 
											$_fecha = date($empleado->fecha_nacimiento);
				                            $_fecha = new DateTime($_fecha);
										?>
										<?= strtoupper($this->Site->dias[$_fecha->format('l')]) ?> 
					                    <?= $_fecha->format('d') ?> 
					                    de <strong><?= strtoupper($this->Site->meses[$_fecha->format('F')]) ?> </strong>
					                    del <?= $_fecha->format('Y') ?> 
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</div>
            </div>
        </div>
	</div>
</div>