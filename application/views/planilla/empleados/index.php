<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#empleados">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de empleados
            </a>
        </li>
        <li>
            <a  href="<?= base_url('planilla/empleados/nuevo') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo empleado
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="empleados" class="tab-pane in active">
            <?php $this->load->view('planilla/empleados/_tabla_empleados'); ?>
        </div>
    </div>
</div>