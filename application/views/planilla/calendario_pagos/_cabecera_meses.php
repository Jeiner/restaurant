<style type="text/css">
    .no-skin .sidebar-shortcuts{
        background-color: #FFF;
    }
    .btn-mes{
        color: #868383;
        background-color: #FFF;
        border: 1px solid #FFF;
        border-bottom: 4px solid #FFF;
        font-weight: 600;
    }
    .btn-mes-active {
        color: #d02d32;
        border-bottom: 4px #d02d32 solid;
    }
</style>
<?php 
    $days_dias = array(
    'Monday'=>'LU',
    'Tuesday'=>'MA',
    'Wednesday'=>'MI',
    'Thursday'=>'JU',
    'Friday'=>'VI',
    'Saturday'=>'SA',
    'Sunday'=>'DO'
    );

    $array_meses = array(
    'January'=>'ENE',
    'February'=>'FEB',
    'March'=>'MAR',
    'April'=>'ABR',
    'May'=>'MAY',
    'June'=>'JUN',
    'July'=>'JUL',
    'August'=>'AGO',
    'September'=>'SET',
    'October'=>'OCT',
    'November'=>'NOV',
    'December'=>'DIC'
    );
?>

<div class="row">
    <div class="col-sm-12 text-center" style="margin-bottom: 10px">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <?php
                $fecha_inicio = $oCalendarioGeneral->dFechaInicio;
            ?>
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">

                <button class="btn-mes" onclick="cambiar_dia('<?= date('m', strtotime($fecha_inicio . ' -2 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' -2 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' -2 month'))]; ?> <br>
                    <?= date('Y', strtotime($fecha_inicio . ' -2 month')); ?>
                </button>

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' -1 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' -1 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' -1 month'))]; ?> <br>
                    <?= date('Y', strtotime($fecha_inicio . ' -1 month')); ?> 
                </button>

                <button class="btn-mes btn-mes-active">
                    <?= $array_meses[date('F', strtotime($fecha_inicio))]; ?> <br>
                    <?= date('Y', strtotime($fecha_inicio)); ?>
                </button>

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' +1 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' +1 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' +1 month'))]; ?>  <br>
                    <?= date('Y', strtotime($fecha_inicio . ' +1 month')); ?> 
                </button>

                <button class="btn-mes" onclick="cambiar_mes('<?= date('m', strtotime($fecha_inicio . ' +2 month')) ?>', '<?= date('Y', strtotime($fecha_inicio . ' +2 month')) ?>');">
                    <?= $array_meses[date('F', strtotime($fecha_inicio . ' +2 month'))]; ?>  <br>
                    <?= date('Y', strtotime($fecha_inicio . ' +2 month')); ?> 
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function cambiar_mes(mes, anio){
        //url
        let url = "<?= base_url() ?>" + "planilla/calendario_pagos?"+ "mes="+ mes + "&anio="+ anio;
        window.location = url;
    }
</script>