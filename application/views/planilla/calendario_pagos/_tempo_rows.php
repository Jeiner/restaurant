<div id="rows" class="tbody">

  <?php foreach ($lstEmpleados as $key => $oEmpleado): ?>
    <!-- Ini -  Item -->
    <div data-index="0" class="sc-fRkCxR TGriw">
      <!-- Ini -  Item Izquierda fija -->
      <div class="sc-iIzCuM gXGvrj">
        <span class="td" data-index="0" style="min-width: 250px; max-width: 250px;">
          <div class="sc-buwyrR xKKRw">
            <a title="<?= $oEmpleado->empleado_id?> | <?= $oEmpleado->nombre_completo ?>" class="sc-kuiBGP crKZxX titleColumnAvatarLink" data-testid="" 
            href="javascript:;" onclick="alert('Ver datos del usuario')">
              <div title="<?= $oEmpleado->empleado_id?> | <?= $oEmpleado->nombre_completo ?>" class="sc-cDIiIq fHhmxa"><img alt="<?= $oEmpleado->nombre_completo ?>"
                  src="https://secure.gravatar.com/avatar/c5fdf4079bd5cd330cc376120cff01b9?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FES-4.png"
                  class="sc-jJfNYf hGDBYt">
              </div>
              <div class="sc-krqHfh kVwHpR">
                  <?= $oEmpleado->nombre_completo ?>
              </div>
            </a>
          </div>
        </span>
        <span class="td" data-index="1" style="min-width: 75px; max-width: 75px;">
          <div class="sc-buwyrR xKKRw">
            <span class="sc-hYkDQb hEBDWR">
              <?= $oEmpleado->dia_pago ?>
            </span>
          </div>
        </span>
        <span class="td" data-index="2" style="min-width: 90px; max-width: 90px;">
          <div class="sc-buwyrR isVhhb">
            <div class="sc-jOuVGU czBxrt">
              <span title="<?= $oEmpleado->sueldo_base_mensual ?>" class="sc-eTArtr fzpUhk">
                <?= $oEmpleado->sueldo_base_mensual ?>
              </span>
            </div>
          </div>
        </span>
      </div>
      <!-- Fin -  Item Izquierda fija -->




      <!-- Ini -  Item Días Calendario anterior derecha movibles -->
      <div class="sc-hMumEG kICUIr calendario-anterior3" >
         <?php foreach ($oEmpleado->oCalendarioAnterior->lstDias as $key => $oDia): ?>
            <span class="td" data-index="12" style="min-width: 54px; max-width: 54px;">
              <div class="sc-cLsBkd nrDOj" onclick="abrir_modal_registrar_prepago('<?= $oEmpleado->empleado_id ?>', '<?= $oDia->dFecha ?>');">
                <div class="sc-cAYQHL leHzlL ">
                  <span class="sc-idzLyQ lpiUGb">


                    <!-- Pagos -->
                    <?php if ($oEmpleado->oCalendarioAnterior->oPeriodoPago->cDiaPago == $oDia->cDia): ?>
                      <span class="item-pago" onclick="abrir_modal_registrar_pago()">
                        <?= 
                          number_format($oEmpleado->oCalendarioAnterior->oPeriodoPago->nTotalPagar, 2);
                        ?>
                      </span>
                    <?php endif ?>


                    <!-- Prepagos -->
                    <?php foreach ($oEmpleado->oCalendarioAnterior->lstPrePagos as $key => $oPrePago): ?>

                      <!-- Adelanto -->
                      <?php if ($oPrePago->tipo_concepto == "ADE"): ?>
                        <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                          <span class="item-adelanto" title="<?= $oPrePago->comentario ?>">
                            <?= number_format($oPrePago->importe, '2') ?>
                          </span>
                        <?php endif ?>
                      <?php endif ?>

                        <!-- Tardanza -->
                        <?php if ($oPrePago->tipo_concepto == "TAR"): ?>
                          <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                            <span class="item-tardanza" title="<?= $oPrePago->comentario ?>">
                              <?= number_format($oPrePago->importe, '2') ?>
                            </span>
                          <?php endif ?>
                        <?php endif ?>

                        <!-- Falta -->
                        <?php if ($oPrePago->tipo_concepto == "FAL"): ?>
                          <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                            <span class="item-falta" title="<?= $oPrePago->comentario ?>">
                              <?= number_format($oPrePago->importe, '2') ?>
                            </span>
                          <?php endif ?>
                        <?php endif ?>

                    <?php endforeach ?>

                  </span>
                </div>
              </div>
            </span>
        <?php endforeach ?>
      </div>
      <!-- Fin -  Item Días Calendario anterior derecha movibles -->




      <!-- Ini -  Item Días derecha movibles -->
      <div class="sc-hMumEG kICUIr">
        <?php foreach ($oEmpleado->oCalendarioPrincipal->lstDias as $key => $oDia): ?>

          <?php
            $pintar_hoy = "";
            if ($oDia->dFecha == $oEmpleado->oCalendarioPrincipal->dFechaActual){
              $pintar_hoy = "td-hoy";
            }
          ?>

          <span class="td" data-index="12" style="min-width: 54px; max-width: 54px;">
            <div class="sc-cLsBkd nrDOj" onclick="abrir_modal_registrar_prepago('<?= $oEmpleado->empleado_id ?>', '<?= $oDia->dFecha ?>');">
              <div class="sc-cAYQHL leHzlL <?= $pintar_hoy ?>" >
                <span class="sc-idzLyQ lpiUGb">

                  <!-- Pagos -->
                  <?php if ($oEmpleado->oCalendarioPrincipal->oPeriodoPago->cDiaPago == $oDia->cDia): ?>
                    <span title="Registrar pago" class="item-pago" onclick="abrir_modal_registrar_pago()">
                      <?= 
                        number_format($oEmpleado->oCalendarioPrincipal->oPeriodoPago->nTotalPagar, 2);
                      ?>
                    </span>
                  <?php endif ?>
                
                  <!-- Prepagos -->
                  <?php foreach ($oEmpleado->oCalendarioPrincipal->lstPrePagos as $key => $oPrePago): ?>

                    <!-- Adelanto -->
                    <?php if ($oPrePago->tipo_concepto == "ADE"): ?>
                      <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                        <span class="item-adelanto" title="<?= $oPrePago->comentario ?>">
                          <?= number_format($oPrePago->importe, '2') ?>
                        </span>
                      <?php endif ?>
                    <?php endif ?>

                      <!-- Tardanza -->
                      <?php if ($oPrePago->tipo_concepto == "TAR"): ?>
                        <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                          <span class="item-tardanza" title="<?= $oPrePago->comentario ?>">
                            <?= number_format($oPrePago->importe, '2') ?>
                          </span>
                        <?php endif ?>
                      <?php endif ?>

                      <!-- Falta -->
                      <?php if ($oPrePago->tipo_concepto == "FAL"): ?>
                        <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                          <span class="item-falta" title="<?= $oPrePago->comentario ?>">
                            <?= number_format($oPrePago->importe, '2') ?>
                          </span>
                        <?php endif ?>
                      <?php endif ?>

                  <?php endforeach ?>

                </span>
              </div>
            </div>
          </span>
        <?php endforeach ?>
      </div>
      <!-- Fin -  Item Días derecha movibles -->

    </div>
    <!-- Fin -  Item -->
  <?php endforeach ?>


</div>

