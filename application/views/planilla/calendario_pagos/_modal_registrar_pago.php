
<div id="modal_registrar_prepago" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="padding: 10px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="smaller lighter blue" style=" margin-top: 8px">
                    <span style="font-size: 16; color: #0b8043; margin-left: 8px;">
                        <i class="menu-icon fa fa-certificate"></i>
                    </span>
                    <strong style="margin-left: 10px;">
                        Registrar pago
                    </strong> 
                </h4>
            </div>
            <div class="modal-body" >

                <form method="Post" id="form_reg_pago" action="<?= base_url("planilla/planilla/guardar_empleado_pago") ?>" class="" autocomplete="off">

                    <div class="row" style="display: none">
                        <div class="col-sm-12">
                            <input type="text" id="reg_pago_empleado_id" name="reg_pago_empleado_id" value="<?= $oEmpleado->empleado_id ?>">
                        </div>
                    </div>

                    <?php if ($oWarning != null): ?>
                        <div class="row">
                            <div class="col-sm-12" style="margin-left: 10px; margin-bottom: 10px">
                                <span class="red" >
                                    <i class="menu-icon fa fa-warning"></i>
                                    <span style="maring-left: 10px">
                                        <?= $oWarning->cMessage ?>
                                    </span>
                                </span>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-calendar"></i>
                                    </span>
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">
                                        Fecha de pago
                                        <?php if ($cFecha == $cFechaActual): ?>
                                            <span style="font-weight: 600;">
                                                - Hoy
                                            </span>
                                        <?php endif ?>
                                    </label>
                                    <input type="date" name="reg_pago_fecha" id="reg_pago_fecha" 
                                    class="form-control" 
                                    value="<?= $cFecha ?>" 
                                    placeholder=""
                                    required
                                    data-error="Debe ingresar la fecha.">
                                </div>
                            </div>
                        </div>

                        

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 6px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-user"></i>
                                    </span>
                                </div>
                                <div style="width: 60%">
                                    <input type="text" name="reg_pago_nombres"
                                    id="reg_pago_nombres"
                                    class="form-control"
                                    value="<?= strtoupper($oEmpleado->nombre_completo) ?> "
                                    placeholder="Apellidos y nombres" readonly>
                                </div>
                                <div style="width: 50%; margin-left: 10px">
                                    <input type="text" name="reg_pago_cargo_desc"
                                    id="reg_pago_cargo_desc"
                                    class="form-control"
                                    value="<?= $oEmpleado->cargo_desc ?>"
                                    readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-dollar"></i>
                                    </span>
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">Sueldo mensual</label>
                                    <input type="number" name="reg_pago_sueldo" 
                                    id="reg_pago_sueldo"
                                    class="form-control"
                                    value="<?= $oEmpleado->sueldo_base_mensual ?>"
                                    placeholder="0.00"
                                    readonly 
                                    data-error="">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div style="width: 40%; margin-left: 10px">
                                    <label class="control-label" style="margin-bottom: 0px">Sueldo diario (30 días)</label>
                                    <input type="number" name="reg_pago_sueldo_dia" 
                                    id="reg_pago_sueldo_dia"
                                    class="form-control"
                                    value="<?= $oEmpleado->sueldo_diario ?>"
                                    placeholder="0.00"
                                    readonly 
                                    data-error="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-calendar"></i>
                                    </span>
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">
                                        Periodo de pago
                                    </label>
                                    <input type="text" name="reg_pago_periodo" id="reg_pago_periodo"
                                    class="form-control" 
                                    value="<?= $fechaInicioPeriodo ?> - <?= $fechaFinPeriodo ?>" >
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12" >
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-list"></i>
                                    </span>
                                </div>
                                <label class="control-label" style="margin-bottom: 0px">
                                    Items:
                                </label>
                                <?php if (count($oEmpleado->lstPrePagosPeriodo) == 0): ?>
                                    <span style="margin-left: 10px;"> Sin items</span>
                                <?php endif ?>
                            </div>
                            <div style="padding: 0px 10px 0px 30px;">
                                <?php if (count($oEmpleado->lstPrePagosPeriodo) > 0): ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Item</th>
                                                <th>Fecha</th>
                                                <th>Importe</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $totalImportePrepago = 0;
                                            ?>
                                            <?php foreach ($oEmpleado->lstPrePagosPeriodo as $key => $oPrepago): ?>
                                                <?php $totalImportePrepago = $totalImportePrepago + $oPrepago->importe ?>
                                                <tr>
                                                    <td>
                                                        <input type="" name='reg_pago_array_prepagos[]' value="<?= $oPrepago->pre_pago_id ?>" style="display: none;" >
                                                        <?= $oPrepago->pre_pago_id ?>
                                                    </td>
                                                    <td>
                                                        <span style="background-color: <?= $oPrepago->color ?>">
                                                            <?= $oPrepago->tipo_concepto_desc ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <?= date('d-m-Y', strtotime($oPrepago->fecha_pago)); ?>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <?= $oPrepago->importe ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>

                                            <tr>
                                                <td>
                                                    <span style="font-weight: 600">
                                                        Total
                                                    </span>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td style="text-align: right; font-weight: 600;">
                                                    <?= number_format($totalImportePrepago, 2) ?>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                <?php endif ?>
                            </div>
                        </div>



                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 35px;">
                                    
                                </div>
                                <div>
                                    <input type="checkbox" id="reg_pago_bloquear" name="reg_pago_bloquear" checked="checked">
                                    <label for="reg_pago_bloquear"> Bloquear periodos anteriores </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex; justify-content: flex-end;">
                                <label class="control-label" style="margin-bottom: 0px; margin-top: 8px; margin-right: 10px;">
                                    Importe total a pagar
                                </label>
                                <div style="width: 30%">
                                    <input type="number" name="reg_pago_importe_total" 
                                    id="reg_pago_importe_total"
                                    class="form-control"
                                    value="<?= $nImporte ?>"
                                    placeholder="0.00"
                                    required 
                                    data-error="Debe ingresar el importe."
                                    style="font-weight: 600!important; font-size: 16px !important; text-align: right;">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row" style="text-align: right;">
                        <div class="col-sm-12">
                            <button class="btn btn-primary btn-sm" type="submit" id="btn_registrar_pago">
                                Registrar pago
                            </button>
                            <a href="javascript:;" class="btn-cancelar"  class="" data-dismiss="modal" >
                                Cancelar
                            </a>
                        </div>
                    </div>
                    
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $('#modal_registrar_prepago').modal({
        show: 'false'
    });

    $("#form_reg_pago").validator().on("submit", function (event) {
        bloquearFormPago(true);
        if (event.isDefaultPrevented()) {
            bloquearFormPago(false);
            alert("Por favor verifique los datos ingresados.");
            return false;
        }else{
            return true;
        }
    });

    function bloquearFormPago(modo){
        if(modo){
            $("#btn_registrar_pago").attr("disabled", "disabled");
        }else{
            $("#btn_registrar_pago").removeAttr("disabled");
        }
    }


</script>
