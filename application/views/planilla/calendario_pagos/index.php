<?php 
    $days_dias = array(
    'Monday'=>'LU',
    'Tuesday'=>'MA',
    'Wednesday'=>'MI',
    'Thursday'=>'JU',
    'Friday'=>'VI',
    'Saturday'=>'SA',
    'Sunday'=>'DO'
    );
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <!-- CSRF Token -->

    <meta name="Santorini" content="Santorini &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <link rel="shortcut icon" href="http://localhost:50/sanbrasa/assets/img/security.png" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css')?> " />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css')?> " />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css')?> " class="ace-main-stylesheet" id="main-ace-style" />

    <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css')?> " />

    <link rel="stylesheet" href="<?= base_url('assets/img/splashy/splashy.css') ?> " />

    <!-- ace settings handler -->
    <script src="<?= base_url('assets/js/ace-extra.min.js')?> "></script>

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/jqeasyui/themes/icon.css') ?> ">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/update_template.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/library/alertify/alertify.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/library/alertify/default.rtl.css')?> " />

    <script src="<?= base_url('assets/js/jquery-2.1.4.min.js')?> "></script>
    <script src="<?= base_url('assets/js/jquery.validate.min.js')?> "></script>
    
    <script type="text/javascript" src="<?= base_url('assets/library/alertify/alertify.min.js') ?> "></script>

    <script type="text/javascript" src="<?= base_url('assets/library/sweetalert/sweetalert.min.js') ?> "></script>
    <link rel="stylesheet" href="<?= base_url('assets/library/sweetalert/sweetalert.css')?> " />

    <!-- REPORTES -->
    <script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
    <script src="<?= base_url() ?>assets/highcharts/code/modules/series-label.js"></script>
    <script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script>
    <script src=" <?= base_url() ?>assets/js/form-validator.min.js"></script>
</head>
<style type="text/css">
    .no-skin .sidebar-shortcuts{
        background-color: #FFF;
    }
    .btn-dia{
        color: #555353;
        background-color: #FFF;
        border: 1px solid #FFF;
        border-bottom: 4px solid #FFF;
        font-weight: 600;
    }

    .btn-dia-active{
        color: #d02d32;
        border-bottom: 4px #d02d32 solid;
    }

    .btn-dia: hover, .btn-dia: focus, .btn-dia: before{
        color: #555353;
        background-color: red!important;
        border: 4px solid red!important;
    }

    /*Preloader*/
    .imagen_cargando{
        display: none;
    }
    .container_loader .transparente{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;

        opacity: 0.3;
        background-color: #797575;
        width:100%;
        height:100%;
        position: absolute;
    }
    .container_loader .imagen_cargando {
        display: block;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity: 1;
        /*float: left;*/
        margin-top:10%;
        margin-left: 25%;
        width:50%;
        position: absolute;
    }
    .container_loader .imagen_cargando img{
        width: 300px;
    }

</style>

<body class="no-skin">


    <div id="navbar" class="navbar navbar-default">
        <script type="text/javascript">
            try{ace.settings.check('navbar' , 'fixed')}catch(e){}
        </script>
        <div class="navbar-container" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left display" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try{ace.settings.loadState('main-container')}catch(e){}
        </script>
            

        <script src="<?= base_url('assets/js/bootstrap.min.js') ?> "></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/bootstrap-datepicker.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/jquery.jqGrid.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/grid.locale-en.js') ?> "></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/ace-elements.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/ace.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/script.js') ?> "></script>

        <!-- inline scripts related to this page -->

        <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/dataTables.tableTools.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/dataTables.colVis.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/pluginModalPDF.js') ?> "></script>

        <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?> "></script>
        <!-- Para graficos de estadisticas -->
        <script src="<?= base_url('assets/js/jquery.easypiechart.min.js') ?> "></script>


            <div id="sidebar" class="sidebar responsive ace-save-state compact " data-sidebar="false" data-sidebar-hover="false" >
                <script type="text/javascript">
                    try{ace.settings.loadState('sidebar')}catch(e){}
                </script>
                <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                        <button class="btn btn-success">
                            <i class="ace-icon fa fa-signal"></i>
                        </button>
                        <button class="btn btn-info">
                            <i class="ace-icon fa fa-pencil"></i>
                        </button>
                        <button class="btn btn-warning">
                            <i class="ace-icon fa fa-users"></i>
                        </button>
                        <button class="btn btn-danger">
                            <i class="ace-icon fa fa-cogs"></i>
                        </button>
                    </div>
                    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>

                        <span class="btn btn-info"></span>

                        <span class="btn btn-warning"></span>

                        <span class="btn btn-danger"></span>
                    </div>
                </div><!-- /.sidebar-shortcuts -->

                    
                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i id="sidebar-toggle-icon" class="ace-save-state ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>
            </div>
            <div class="main-content">
                <div class="main-content-inner">
                        <div class="page-content">
                            <!-- Mostrar mensajes como respuesta de una acción anterior (Registro, actualización, etc.) -->




<link rel="stylesheet" href="<?= base_url('assets/css/planilla_tempo.css')?> " />
<link rel="stylesheet" href="<?= base_url('assets/css/planilla_tempo_v2.css')?> " />

<?php $this->load->view('planilla/calendario_pagos/_cabecera_meses'); ?>

<?php $this->load->view('general/message'); ?>

<!-- Ini Planilla tempo -->
    <?php $this->load->view('planilla/calendario_pagos/_tempo'); ?>
<!-- Fin Planilla tempo -->

<hr>
<div class="row">
    <div class="col-sm-2">
        <div class="panel-body">
            <div class="form-group" style="display: flex; margin-bottom: 5px!important;">
                <div class="bg-leyenda-pago" style="width: 35px;height: 23px">
                </div>
                <span class="black bolder" style="margin-left: 5px;">Día a pagar</span>
            </div>
            <div class="form-group" style="display: flex; margin-bottom: 5px!important;">
                <div class="bg-leyenda-pago-registrado" style="width: 35px;height: 23px">
                </div>
                <span class="black bolder" style="margin-left: 5px;">Pago registrado</span>
            </div>
            <div class="form-group" style="display: flex; margin-bottom: 5px!important;">
                <div class="bg-leyenda-adelanto" style="width: 35px;height: 23px">
                </div>
                <span class="black bolder" style="margin-left: 5px;">Adelanto</span>
            </div>
            <div class="form-group" style="display: flex; margin-bottom: 5px!important;">
                <div class="bg-leyenda-tardanza" style="width: 35px;height: 23px">
                </div>
                <span class="black bolder" style="margin-left: 5px;">Tardanza</span>
            </div>
            <div class="form-group" style="display: flex; margin-bottom: 5px!important;">
                <div class="bg-leyenda-falta" style="width: 35px;height: 23px">
                </div>
                <span class="black bolder" style="margin-left: 5px;">Falta</span>
            </div>
            <div class="form-group" style="display: flex; margin-bottom: 5px!important;">
                <div class="bg-leyenda-descuento" style="width: 35px;height: 23px">
                </div>
                <span class="black bolder" style="margin-left: 5px;">Descuento</span>
            </div>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="panel-body">
            <div>
                <input type="checkbox" id="ver_adelantos" name="ver_adelantos" checked />
                <label for="ver_adelantos"> Ver adelantos </label>
            </div>
            <div>
                <input type="checkbox" id="ver_faltas" name="ver_faltas" checked />
                <label for="ver_faltas"> Ver faltas y pagos </label>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    function ver_mes_anterior(modo){
        if(modo){
            $("#head-mes-anterior-nombre").fadeIn('slow');
            $("#head-mes-anterior-dias").fadeIn('slow');
            $(".rows-mes-anterior").fadeIn('slow');
        }else{
            $("#head-mes-anterior-nombre").fadeOut('slow');
            $("#head-mes-anterior-dias").fadeOut('slow');
            $(".rows-mes-anterior").fadeOut('slow');
        }
    }

    function ver_adelantos(modo){
        if(modo){
            $(".bg-item-adelanto").fadeIn('slow');
        }else{
            $(".bg-item-adelanto").fadeOut('slow');
        }
    }
    
    function ver_faltas(modo){
        if(modo){
            $(".bg-item-falta").fadeIn('slow');
            $(".bg-item-tardanza").fadeIn('slow');
        }else{
            $(".bg-item-falta").fadeOut('slow');
            $(".bg-item-tardanza").fadeOut('slow');
        }
    }
</script>

<script type="text/javascript">

    const check_ver_mes_ant = document.querySelector("#ver_mes_ant");
    check_ver_mes_ant.addEventListener("change", () => {
        ver_mes_anterior(check_ver_mes_ant.checked);
    });

    const check_ver_adelantos = document.querySelector("#ver_adelantos");
    check_ver_adelantos.addEventListener("change", () => {
        ver_adelantos(check_ver_adelantos.checked);
    });

    const check_ver_faltas = document.querySelector("#ver_faltas");
    check_ver_faltas.addEventListener("change", () => {
        ver_faltas(check_ver_faltas.checked);
    });

</script>

<script type="text/javascript">
    $('.gXGvrj').on('click',  function () {
        seleccionarFila(this);
    });

    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        empleado_id = $(fila).attr("row_empleado_id")
        seleccionar = ".seleccionar_row_" + empleado_id;
        $(seleccionar).addClass("success");
    }
</script>












                        </div>
                </div>
            </div><!-- /.main-content -->
    
    <div class="footer">
        <div class="footer-inner">
            <div class="footer-content">
                <span class="bigger-120">
                    <span class="blue bolder">SANTORINI</span>
                    Pizzas y comida italiana © 2018
                </span>

                &nbsp; &nbsp;
                <span class="action-buttons">
                    <!-- <a href="#">
                        <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                    </a> -->

                    <a href="https://www.facebook.com/Pizzeria-Santorini-1500364683510878/" target="blanck">
                        <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                    </a>

                    <!-- <a href="#">
                        <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                    </a> -->
                </span>
            </div>
        </div>
    </div>
        <span class="ir-arriba" style="display: none;">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </span>
        <span class="ir-abajo" style="display: none;">
            <i class="ace-icon fa fa-angle-double-down icon-only bigger-110"></i>
        </span>
    </div><!-- /.main-container -->
    


</body>

</html>
