
<div id="modal_registrar_prepago" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="padding: 10px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="smaller lighter blue" style=" margin-top: 8px">
                    <span style="font-size: 16; color: #0b8043; margin-left: 8px;">
                        <i class="menu-icon fa fa-certificate"></i>
                    </span>
                    <strong style="margin-left: 10px;">
                        Registrar item
                    </strong> 
                </h4>
            </div>
            <div class="modal-body" >

                <form method="Post" id="form_reg_prepag" action="<?= base_url("planilla/planilla/guardar_pre_pago") ?>" class="" autocomplete="off">

                    <div class="row" style="display: none">
                        <div class="col-sm-12">
                            <input type="text" id="reg_prepag_empleado_id" name="reg_prepag_empleado_id" value="<?= $oEmpleado->empleado_id ?>">
                        </div>
                    </div>

                    <?php if ($oWarning != null): ?>
                        <div class="row">
                            <div class="col-sm-12" style="margin-left: 10px; margin-bottom: 10px">
                                <span class="red" >
                                    <i class="menu-icon fa fa-warning"></i>
                                    <span style="maring-left: 10px">
                                        <?= $oWarning->cMessage ?>
                                    </span>
                                </span>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-calendar"></i>
                                    </span>
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">
                                        Fecha
                                        <?php if ($cFecha == $cFechaActual): ?>
                                            <span style="font-weight: 600;">
                                                - Hoy
                                            </span>
                                        <?php endif ?>
                                    </label>
                                    <input type="date" name="reg_prepag_fecha" id="reg_prepag_fecha" 
                                    class="form-control" 
                                    value="<?= $cFecha ?>" 
                                    placeholder=""
                                    required 
                                    data-error="Debe ingresar la fecha.">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 6px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-user"></i>
                                    </span>
                                </div>
                                <div style="width: 60%">
                                    <input type="text" name="reg_prepag_nombres"
                                    id="reg_prepag_nombres"
                                    class="form-control"
                                    value="<?= strtoupper($oEmpleado->nombre_completo) ?> "
                                    readonly>
                                </div>
                                <div style="width: 50%; margin-left: 10px">
                                    <input type="text" name="reg_pago_cargo_desc"
                                    id="reg_pago_cargo_desc"
                                    class="form-control"
                                    value="<?= $oEmpleado->cargo_desc ?>"
                                    readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-dollar"></i>
                                    </span>
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">Sueldo mensual</label>
                                    <input type="number" name="reg_prepag_sueldo" 
                                    id="reg_prepag_sueldo"
                                    class="form-control"
                                    value="<?= $oEmpleado->sueldo_base_mensual ?>"
                                    placeholder="0.00"
                                    readonly 
                                    data-error="">
                                </div>
                                <div style="width: 40%; margin-left: 10px">
                                    <label class="control-label" style="margin-bottom: 0px">Sueldo diario (30 días)</label>
                                    <input type="number" name="reg_prepag_sueldo" 
                                    id="reg_prepag_sueldo"
                                    class="form-control"
                                    value="<?= $oEmpleado->sueldo_diario ?>"
                                    placeholder="0.00"
                                    readonly 
                                    data-error="">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        
                                    </span>
                                </div>
                                <div style="width: 100%">
                                    <label class="control-label" style="margin-bottom: 0px">Tipo de item</label>
                                    <select class="form-control input-sm" 
                                    id="reg_prepag_tipo_item" 
                                    name="reg_prepag_tipo_item"
                                    required 
                                    data-error="Debe ingresar el tipo." onchange="mxCambiarTipoItem(this.value, <?=  $oEmpleado->sueldo_diario ?>);">
                                        <option value="">Seleccionar</option>
                                        <?php foreach ($lstTiposItem as $key => $oTipo): ?>
                                            <option value="<?= $oTipo->valor ?>"><?= $oTipo->valor ?> | <?= $oTipo->descripcion ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12" id="div_min_tardanza" style="display: none">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">Minutos tardanza</label>
                                    <input type="decimal" name="reg_prepag_min_tardanza" 
                                    onchange="mxAsignarDctoPorTardanza(this.value, <?=  $oEmpleado->sueldo_diario ?>);"
                                    id="reg_prepag_min_tardanza"
                                    class="form-control"
                                    value=""
                                    style="font-size: 15px !important;">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-dollar"></i>
                                    </span>
                                </div>
                                <div style="width: 40%">
                                    <label class="control-label" style="margin-bottom: 0px">Importe</label>
                                    <input type="decimal" name="reg_prepag_importe" 
                                    id="reg_prepag_importe"
                                    class="form-control"
                                    value=""
                                    placeholder="0.00"
                                    required 
                                    data-error="Debe ingresar el importe."
                                    style="font-weight: 600!important; font-size: 16px !important;">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group" style="display: flex;">
                                <div style="width: 30px; text-align: center; padding-top: 26px; margin-right: 5px;">
                                    <span style="font-size: 15; color: rgb(0, 71, 136);">
                                        <i class="menu-icon fa fa-align-left"></i>
                                    </span>
                                </div>
                                <div style="width: 100%">
                                    <label class="control-label" style="margin-bottom: 0px"> Motivo </label>
                                    <input type="text" name="reg_prepag_motivo" id="reg_prepag_motivo" class="form-control" value="" placeholder="Motivo ">
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row" style="text-align: right;">
                        <div class="col-sm-12">
                            <button class="btn btn-primary btn-sm" type="submit" id="btn_registrar_item">
                                Registrar item
                            </button>
                            <a href="javascript:;" class="btn-cancelar"  class="" data-dismiss="modal" >
                                Cancelar
                            </a>
                        </div>
                    </div>
                    
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $('#modal_registrar_prepago').modal({
        show: 'false'
    });

    $("#form_reg_prepag").validator().on("submit", function (event) {
        bloquearFormInscripcion(true);
        if (event.isDefaultPrevented()) {
            bloquearFormInscripcion(false);
            alert("Por favor verifique los datos ingresados.");
            return false;
        }else{
            return true;
        }
    });

    function bloquearFormInscripcion(modo){
        if(modo){
            $("#btn_registrar_item").attr("disabled", "disabled");
        }else{
            $("#btn_registrar_item").removeAttr("disabled");
        }
    }

    function mostrarMensajeErrorGeneral(valid, msg){
        if(valid){
            var msgClasses = "msjCorrecto text-center tada animated text-success";
        } else {
            var msgClasses = "h3 text-center text-danger";
        }
        $("#msjErrorGeneral").removeClass().addClass(msgClasses).text(msg);
    }

    function mxCambiarTipoItem(valor, sueldo_diario){
        $("#reg_prepag_importe").removeClass("bg-item-adelanto");
        $("#reg_prepag_importe").removeClass("bg-item-tardanza");
        $("#reg_prepag_importe").removeClass("bg-item-falta");
        $("#reg_prepag_importe").removeClass("bg-item-descuento");

        $("#div_min_tardanza").fadeOut();
        $("#reg_prepag_min_tardanza").val("");

        //Agregar clase
        if(valor == "ADE"){
            $("#reg_prepag_importe").addClass("bg-item-adelanto");
        }
        if(valor == "TAR"){
            $("#reg_prepag_importe").addClass("bg-item-tardanza");
            $("#div_min_tardanza").fadeIn();
        }
        if(valor == "FAL"){
            $("#reg_prepag_importe").addClass("bg-item-falta");
            mxAsignarDctoPorFalta(sueldo_diario);
        }
        if(valor == "DES"){
            $("#reg_prepag_importe").addClass("bg-item-descuento");
        }
    }

    function mxAsignarDctoPorTardanza(minutos, sueldo_diario){
        let descuento = (sueldo_diario/(9*60)) * minutos;
        $("#reg_prepag_importe").val(descuento.toFixed(2));
    }

    function mxAsignarDctoPorFalta(sueldo_diario){
        $("#reg_prepag_importe").val(sueldo_diario.toFixed(2));
    }

</script>
