<!-- INI - ROW 1  -->
<div class="sc-gCnqgB kTdcnN">
    <div class="sc-gCnqgB sc-iLrwWu kTdcnN iJNdJS">
      <div class="sc-fUcwZz fRnrBw th" data-testid="TITLE_COLUMN-header">
        <div title="Ordenar" class="sc-cyJusY jluBao"><span class="sc-hYkDQb hEBDWR">
          
        </span>
        </div>
      </div>
      <div class="sc-fUcwZz fGlQhw th" data-testid="KEY_COLUMN-header">
        <div title="Día de pago" class="sc-cyJusY jluBao"><span class="sc-hYkDQb hEBDWR">
        </span></div>
      </div>
      <div class="sc-fUcwZz jtWdys th" data-testid="TOTAL_TIME_COLUMN-header">
        <div title="Sueldo mensual" class="sc-cyJusY hyfWkB">
          <span class="sc-hYkDQb gZwIU">
          </span>
        </div>
      </div>
    </div>


    <div style="display: flex;" class="calendario-anterior1">
      <span class="head-nombre-mes-anterior">
        <?= $oCalendarioAnterior->cMesDesc; ?> <?= $oCalendarioAnterior->cAnio; ?>
      </span>
      <?php foreach ($oCalendarioAnterior->lstDias as $key => $oDia): ?>
        <div class="sc-fUcwZz juAMsA th" data-testid="columnAccessor-0-header">
          <div data-testid="dateContainer" class="sc-hTgaWy OoikG tempo-day-header OoikG-head-anterior" >
            <div title="01" class="sc-yvFfD jBgGIG">
              
            </div>
            <div class="sc-dXqVGa ySfMd">
              
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>


    <div style="display: flex;">
      <span class="head-nombre-mes-principal">
        <?= $oCalendarioGeneral->cMesDesc; ?> <?= $oCalendarioGeneral->cAnio; ?>
      </span>
      <?php foreach ($oCalendarioGeneral->lstDias as $key => $oDia): ?>
        <div class="sc-fUcwZz juAMsA th" data-testid="columnAccessor-0-header">
          <div data-testid="dateContainer" class="sc-hTgaWy OoikG tempo-day-header OoikG-head-principal" >
            <div title="01" class="sc-yvFfD jBgGIG">
              
            </div>
            <div class="sc-dXqVGa ySfMd">
              
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
</div>
<!-- FIN - ROW 1  -->


<!-- INI - ROW 2  -->
<div class="sc-gCnqgB kTdcnN">

    <div class="sc-gCnqgB sc-iLrwWu kTdcnN iJNdJS">
      <div class="sc-fUcwZz fRnrBw th" data-testid="TITLE_COLUMN-header">
        <div title="Ordenar" class="sc-cyJusY jluBao">
          <span class="sc-hYkDQb hEBDWR" onclick="ver_mes_anterior();">
            Usuario
          </span>
        </div>
      </div>
      <div class="sc-fUcwZz fGlQhw th" data-testid="KEY_COLUMN-header">
        <div title="Día de pago" class="sc-cyJusY jluBao"><span class="sc-hYkDQb hEBDWR">Día de pago</span></div>
      </div>
      <div class="sc-fUcwZz jtWdys th" data-testid="TOTAL_TIME_COLUMN-header">
        <div title="Sueldo mensual" class="sc-cyJusY hyfWkB"><span
            class="sc-hYkDQb gZwIU">Sueldo</span></div>
      </div>
    </div>


    <div style="display: flex;" class="calendario-anterior2">
      <?php foreach ($oCalendarioAnterior->lstDias as $key => $oDia): ?>
        <div class="sc-fUcwZz juAMsA th" data-testid="columnAccessor-0-header">
          <div data-testid="dateContainer" class="sc-hTgaWy OoikG tempo-day-header">
            <div title="01" class="sc-yvFfD jBgGIG">
              <?= $oDia->cDia ?>
            </div>
            <div class="sc-dXqVGa ySfMd">
              <?= $oDia->cDiaDesc ?>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>


    <div style="display: flex;">
      <?php foreach ($oCalendarioGeneral->lstDias as $key => $oDia): ?>

        <?php
          $pintar_hoy = "";
          if ($oDia->dFecha == $oCalendarioGeneral->dFechaActual){
            $pintar_hoy = "td-hoy";
          }
        ?>

        <div class="sc-fUcwZz juAMsA th" data-testid="columnAccessor-0-header">
          <div data-testid="dateContainer" class="sc-hTgaWy OoikG tempo-day-header <?= $pintar_hoy ?>">
            <div title="01" class="sc-yvFfD jBgGIG">
              <?= $oDia->cDia ?>
            </div>
            <div class="sc-dXqVGa ySfMd">
              <?= $oDia->cDiaDesc ?>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
</div>
<!-- FIN - ROW 2  -->

