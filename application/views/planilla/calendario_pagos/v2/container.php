<div class="row">
    <div class="col-sm-12">
        <div>
            <input type="checkbox" id="ver_mes_ant" name="ver_mes_ant"  />
            <label for="ver_mes_ant"> Ver días de mes anterior </label>
        </div>
    </div>
</div>
<div class="" style="display: flex;">
    <div class="container-left" style="">
        <?php $this->load->view('planilla/calendario_pagos/v2/_left_header'); ?>

        <?php $this->load->view('planilla/calendario_pagos/v2/_left_rows'); ?>
    </div>

    <div id="container-right" style="overflow-x: scroll;">
        <?php $this->load->view('planilla/calendario_pagos/v2/_right_header'); ?>

        <?php $this->load->view('planilla/calendario_pagos/v2/_right_rows'); ?>
    </div>
</div>
