<div id="rows" class="tbody">

  <?php foreach ($lstEmpleados as $key => $oEmpleado): ?>
    <!-- Ini -  Item -->
    <div data-index="0" class="sc-fRkCxR TGriw">
    
      <?php 
        $cEmpleadoId = $oEmpleado->empleado_id;
      ?>

      <!-- Ini -  Item Días Calendario anterior derecha movibles -->
      <span class="rows-mes-anterior" style="display: none">
          <div class="sc-hMumEG kICUIr calendario-anterior3 seleccionar_row_<?= $cEmpleadoId ?>" >
             <?php foreach ($oEmpleado->oCalendarioAnterior->lstDias as $key => $oDia): ?>
                <span class="td" data-index="12" style="min-width: 54px; max-width: 54px;">
                  <div class="sc-cLsBkd nrDOj" onclick="abrir_modal_registrar_prepago('<?= $cEmpleadoId ?>', '<?= $oDia->dFecha ?>');">
                    <div class="sc-cAYQHL leHzlL ">
                      <span class="sc-idzLyQ lpiUGb">

                        <?php 
                          $nImportePrePago = $oEmpleado->oCalendarioAnterior->oPeriodoPago->nTotalPagar;
                        ?>

                        <!-- Pagos por realizar -->
                        <?php if ($oEmpleado->oCalendarioAnterior->oPeriodoPago->cDiaPago == $oDia->cDia): ?>
                          <span class="item-pago" onclick="abrir_modal_registrar_pago('<?= $cEmpleadoId ?>', '<?= $oDia->dFecha ?>', <?= $nImportePrePago ?>)">
                            <?= 
                              number_format($oEmpleado->oCalendarioAnterior->oPeriodoPago->nTotalPagar, 2);
                            ?>
                          </span>
                        <?php endif ?>


                        <!-- Prepagos -->
                        <?php foreach ($oEmpleado->oCalendarioAnterior->lstPrePagos as $key => $oPrePago): ?>

                          <!-- Adelanto -->
                          <?php if ($oPrePago->tipo_concepto == "ADE"): ?>
                            <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                              <span class="item-adelanto" title="<?= $oPrePago->comentario ?>">
                                <?= number_format($oPrePago->importe, '2') ?>
                              </span>
                            <?php endif ?>
                          <?php endif ?>

                            <!-- Tardanza -->
                            <?php if ($oPrePago->tipo_concepto == "TAR"): ?>
                              <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                                <span class="item-tardanza" title="<?= $oPrePago->comentario ?>">
                                  <?= number_format($oPrePago->importe, '2') ?>
                                </span>
                              <?php endif ?>
                            <?php endif ?>

                            <!-- Falta -->
                            <?php if ($oPrePago->tipo_concepto == "FAL"): ?>
                              <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                                <span class="item-falta" title="<?= $oPrePago->comentario ?>">
                                  <?= number_format($oPrePago->importe, '2') ?>
                                </span>
                              <?php endif ?>
                            <?php endif ?>

                        <?php endforeach ?>

                      </span>
                    </div>
                  </div>
                </span>
            <?php endforeach ?>
          </div>
      </span>
      <!-- Fin -  Item Días Calendario anterior derecha movibles -->




      <!-- Ini -  Item Días derecha movibles -->
      <div class="sc-hMumEG kICUIr seleccionar_row_<?= $cEmpleadoId ?>" >
        <?php foreach ($oEmpleado->oCalendarioPrincipal->lstDias as $key => $oDia): ?>

          <?php
            $pintar_hoy = "";
            if ($oDia->dFecha == $oEmpleado->oCalendarioPrincipal->dFechaActual){
              $pintar_hoy = "td-hoy";
            }
          ?>

          <span class="td" data-index="12" style="min-width: 54px; max-width: 54px;">
            <div class="sc-cLsBkd nrDOj" onclick="abrir_modal_registrar_prepago('<?= $cEmpleadoId ?>', '<?= $oDia->dFecha ?>');">
              <div class="sc-cAYQHL leHzlL <?= $pintar_hoy ?>" >
                <span class="sc-idzLyQ lpiUGb">

                  <?php 
                    $nImportePrePago = $oEmpleado->oCalendarioPrincipal->oPeriodoPago->nTotalPagar;
                  ?>
                  <!-- Pagos por realizar -->
                  <?php if ($oEmpleado->oCalendarioPrincipal->oPeriodoPago->cDiaPago == $oDia->cDia): ?>
                    <span title="Registrar pago"
                        class="item-pago"
                        onclick="abrir_modal_registrar_pago('<?= $cEmpleadoId ?>', '<?= $oDia->dFecha ?>', <?= $nImportePrePago ?>, '<?= $oEmpleado->oCalendarioPrincipal->oPeriodoPago->dFechaInicio ?>' , '<?= $oEmpleado->oCalendarioPrincipal->oPeriodoPago->dFechaFin ?>' )">
                      <?= 
                        number_format($nImportePrePago, 2);
                      ?>
                    </span>
                  <?php endif ?>
                
                  <!-- Prepagos -->
                  <?php foreach ($oEmpleado->oCalendarioPrincipal->lstPrePagos as $key => $oPrePago): ?>
                      <?php if (date('Y-m-d', strtotime($oPrePago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                        <span class="item-prepago <?= $oPrePago->color ?>" title="<?= $oPrePago->tipo_concepto_desc ?>" >
                          <?= number_format($oPrePago->importe, '2') ?>
                        </span>
                      <?php endif ?>
                  <?php endforeach ?>


                  <!-- Pagos realizados-->
                  <?php foreach ($oEmpleado->oCalendarioPrincipal->lstPagos as $key => $oPago): ?>
                    <?php if (date('Y-m-d', strtotime($oPago->fecha_pago)) == date('Y-m-d', strtotime($oDia->dFecha))): ?>
                      <span class="item-pagado">
                        <?= number_format($oPago->importe_total, '2') ?>
                      </span>
                    <?php endif ?>
                  <?php endforeach ?>

                </span>
              </div>
            </div>
          </span>
        <?php endforeach ?>
      </div>
      <!-- Fin -  Item Días derecha movibles -->

    </div>
    <!-- Fin -  Item -->
  <?php endforeach ?>


</div>
