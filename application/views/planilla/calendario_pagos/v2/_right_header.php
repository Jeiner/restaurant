<!-- INI - ROW 1  -->
<div class="sc-gCnqgB kTdcnN" style="position: inherit;">

    <div style="display: flex;" class="calendario-anterior1">
       <?php 
          $widthAnterior = count($oCalendarioAnterior->lstDias) * 54;
        ?>
      <div id="head-mes-anterior-nombre" style="width: <?= $widthAnterior ?>px; display: none;">
          <?= $oCalendarioAnterior->cMesDesc; ?> <?= $oCalendarioAnterior->cAnio; ?>
      </div>
    </div>

    <div style="display: flex;">
       <?php 
          $width = count($oCalendarioGeneral->lstDias) * 54;
        ?>
      <div id="head-mes-principal-nombre" style="width: <?= $width ?>px; ">
          <?= $oCalendarioGeneral->cMesDesc; ?> <?= $oCalendarioGeneral->cAnio; ?>
      </div>
    </div>

</div>
<!-- FIN - ROW 1  -->


<!-- INI - ROW 2  -->
<div class="sc-gCnqgB kTdcnN" style="position: inherit;">

    <span id="head-mes-anterior-dias" style="display: none">
      <div style="display: flex;" class="calendario-anterior2">
          <?php foreach ($oCalendarioAnterior->lstDias as $key => $oDia): ?>
            <div class="sc-fUcwZz juAMsA th" data-testid="columnAccessor-0-header">
              <div data-testid="dateContainer" class="sc-hTgaWy OoikG tempo-day-header">
                <div title="01" class="sc-yvFfD jBgGIG">
                  <?= $oDia->cDia ?>
                </div>
                <div class="sc-dXqVGa ySfMd">
                  <?= $oDia->cDiaDesc ?>
                </div>
              </div>
            </div>
          <?php endforeach ?>
      </div>
    </span>



    <div style="display: flex;">
      <?php foreach ($oCalendarioGeneral->lstDias as $key => $oDia): ?>

        <?php
          $pintar_hoy = "";
          if ($oDia->dFecha == $oCalendarioGeneral->dFechaActual){
            $pintar_hoy = "td-hoy";
          }
        ?>

        <div class="sc-fUcwZz juAMsA th" data-testid="columnAccessor-0-header">
          <div data-testid="dateContainer" class="sc-hTgaWy OoikG tempo-day-header <?= $pintar_hoy ?>">
            <div title="01" class="sc-yvFfD jBgGIG">
              <?= $oDia->cDia ?>
            </div>
            <div class="sc-dXqVGa ySfMd">
              <?= $oDia->cDiaDesc ?>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>

</div>
<!-- FIN - ROW 2  -->


<script type="text/javascript">

  const containerRight = document.getElementById("container-right");
  let scrollLeft = 54 * (<?= $oCalendarioGeneral->nNumeroDiaActual ?> - 2);
  containerRight.scrollLeft = scrollLeft;

</script>



