<div id="rows" class="tbody">

  <?php foreach ($lstEmpleados as $key => $oEmpleado): ?>
    <!-- Ini -  Item -->
    <div data-index="0" class="sc-fRkCxR TGriw">
      <!-- Ini -  Item Izquierda fija -->
      <div class="sc-iIzCuM gXGvrj seleccionar_row_<?= $oEmpleado->empleado_id ?>" row_empleado_id="<?= $oEmpleado->empleado_id  ?>">

        <span class="td td_row_0" data-index="0" >
          <div class="sc-buwyrR xKKRw" style="padding: 0px!important">
            <a title="" class="sc-kuiBGP crKZxX titleColumnAvatarLink" data-testid="" href="javascript:;">
              <div class="sc-krqHfh kVwHpR_v2" onclick="alert('Ver datos del usuario')">
                    <?= $oEmpleado->nombre_completo ?>
              </div>
              <div title="<?= $oEmpleado->empleado_id?> | <?= $oEmpleado->nombre_completo ?>" 
                class="sc-cDIiIq fHhmxa" style="color: #109810;" onClick="alert('Ver alertas');">
                <?php if ($oEmpleado->empleado_id > 3): ?>
                  <i class="menu-icon fa fa-bell"></i>
                <?php endif ?>
              </div>
            </a>
          </div>
        </span>

        <span class="td td_row_1" >
          <div class="sc-buwyrR xKKRw">
            <span class="sc-hYkDQb hEBDWR">
              <?= $oEmpleado->dia_pago ?>
            </span>
          </div>
        </span>

        <span class="td td_row_2" >
          <div class="sc-buwyrR isVhhb">
            <div class="sc-jOuVGU czBxrt">
              <span title="<?= $oEmpleado->sueldo_base_mensual ?>" class="sc-eTArtr fzpUhk">
                <?= $oEmpleado->sueldo_base_mensual ?>
              </span>
            </div>
          </div>
        </span>

      </div>
      <!-- Fin -  Item Izquierda fija -->

    </div>
    <!-- Fin -  Item -->
  <?php endforeach ?>


</div>
