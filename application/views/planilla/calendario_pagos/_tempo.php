<div class="sc-lgHmWD fwvRNl">
  <div class="sc-bPWcZb iKeoNy">

    <div data-testid="report-grid" class="sc-hswXQg bddnIl">
      
      <?php $this->load->view('planilla/calendario_pagos/v2/container'); ?>

    </div>

    <hr>

    <div id="div_modal_registrar_prepago">
      
    </div>

    <div id="div_modal_registrar_pago">
      
    </div>
    
  </div>
</div>


<script type="text/javascript">
  let flag_abrir_modal_prepago = true;

  function abrir_modal_registrar_pago(empleado_id, fecha, importe, fechaInicioPeriodo, fechaFinPeriodo){
    flag_abrir_modal_prepago = false;
    $.ajax({
        type: 'POST',
        url: "<?=base_url('planilla/planilla/ajax_abrir_modal_registrar_pago')?>",
        data: {"empleado_id": empleado_id, "fecha": fecha, "importe": importe,"fechaInicioPeriodo": fechaInicioPeriodo,"fechaFinPeriodo": fechaFinPeriodo},
        success: function(rpta){
            $('#div_modal_registrar_pago').html(rpta);
        },
        error: function(rpta){
            alert("Error en la operación");
        }
    });
  }

  function abrir_modal_registrar_prepago(empleado_id, fecha){
    if(!flag_abrir_modal_prepago){
      flag_abrir_modal_prepago = true;
      return;
    }

    $.ajax({
        type: 'POST',
        url: "<?=base_url('planilla/planilla/ajax_abrir_modal_registrar_prepago')?>",
        data: {"empleado_id": empleado_id, "fecha": fecha},
        success: function(rpta){
            $('#div_modal_registrar_prepago').html(rpta);
            // cerrarCargandoFull();
        },
        error: function(rpta){
          // cerrarCargandoFull();
            alert("Error en la operación");
        }
    });
  }

</script>