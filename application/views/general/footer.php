			</div>
		</div>
	</div><!-- /.main-content -->
	<div id="para_modal_cumpleanios">
		<!-- modal_cumpleanios -->
	</div>
	<div class="footer">
		<div class="footer-inner">
			<div class="footer-content">
				<span class="bigger-120">
					<span class="blue bolder"><?= strtoupper($oGeneralidades->nombre_comercial) ?></span>
					<?= $oGeneralidades->actividad ?> &copy; 2018
				</span>

				&nbsp; &nbsp;
				<span class="action-buttons">
					<!-- <a href="#">
						<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
					</a> -->

					<a href="<?= $lstVariables['url_facebook_fan_page'] ?>" target="blanck">
						<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
					</a>

					<!-- <a href="#">
						<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
					</a> -->
				</span>
			</div>
		</div>
	</div>
		<span class="ir-arriba">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</span>
		<span class="ir-abajo">
			<i class="ace-icon fa fa-angle-double-down icon-only bigger-110"></i>
		</span>
	</div><!-- /.main-container -->
	<?php $this->load->view('general/message_to_delete'); ?>
</body>

<?php 
	// $this->load->view('general/scripts'); 
?>
</html>
<style type="text/css">
	.ir-arriba,.ir-abajo {
		/*display:none;*/
		padding:10px 20px;
		opacity: 0.5;
		background:#888888;
		font-size:20px;
		color:#fff;
		cursor:pointer;
		position: fixed;
		
	}
	.ir-arriba {
		bottom:70px;
		right:10px;
		/*display:none;*/
	}
	.ir-abajo{
		bottom:10px;
		right:10px;
	}
	.ir-arriba:hover,.ir-abajo:hover {
		opacity: 1.0;
	}
</style>
<script type="text/javascript">
	$('[data-rel=tooltip]').tooltip();
	$('[data-rel=popover]').popover({html:true});

	function modal_proximos_cumples(){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('planilla/empleados/ajax_proximos_cumples')?>",
            data: {},
            success: function(rpta){
            	$('#para_modal_cumpleanios').html(rpta);
                $('#modal_cumpleanios').modal("show");
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }

    /* scripts para desplazar hacia arriba o abajo  la pantalla*/
	if ($(document).height() <= $(window).height()) {
	    $(".ir-arriba").hide();
	    $(".ir-abajo").hide();
	}
	$('.ir-arriba').click(function(){
		var posAct = $(window).scrollTop();
		var posNue = posAct - 350;
    	$("html, body").animate({ scrollTop: posNue }, 600);
            return false;
    });
    $('.ir-abajo').click(function(){
    	var posAct = $(window).scrollTop();
		var posNue = posAct + 350;
    	$("html, body").animate({ scrollTop: posNue }, 600);
            return false;
    });

</script>