<style type="text/css">
  /* Estilos nuevos */
.wrapper {
  position: relative;
  width: 100%;
  height: 100%;
}
.verOpcionesCelda {
  position:absolute;
  height:100%;
  left: 100%;
  top:0;
}
.verOpcionesCelda .cellOptionsBtn {
  border-top-right-radius:5px;
  border-bottom-right-radius:5px;
  position:absolute;
  top: 0;
  left: 100%;
  height:100%;
  width:22px;
  padding: 2px;
  background-color:#1c84c6;
  color:#FFF;
  text-align: center;
  cursor: pointer;
}
.verOpcionesCelda .cellOptionsMsg {
  position:relative;
  height:100%;
  width: 0;
  overflow: hidden;
  background-color: rgb(237, 85, 101);
  background-color: rgba(237, 85, 101,0.7);
}

.verOpcionesCelda.open .cellOptionsBtn {
  background-color: rgb(237, 85, 101);
}

/* Este estilo es solo para dar espacio a los costados*/
.row {
  margin: 0 15px !important;
}
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row">
  <div class="col-md-12">
    <table class="table">
      <caption>Optional table caption.</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="row">
            <div class="wrapper">
              1
              <div class="verOpcionesCelda">
                <div class="cellOptionsMsg">
                  Opciones de la celda
                </div>
                <i class="cellOptionsBtn fa fa-eye"></i>
              </div>
            </div>
          </td>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <td scope="row">
            <div class="wrapper">
              2
              <div class="verOpcionesCelda">
                <div class="cellOptionsMsg">
                  Opciones de la celda
                </div>
                <i class="cellOptionsBtn fa fa-eye"></i>
              </div>
            </div></td>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
  $(function() {
  
  $('.cellOptionsBtn').on('click', function() {
    alert();
    var $btn = $(this),
        $container = $btn.parent(),
        $td = $container.closest('td'),
        // alert($td);
        $tr = $container.closest('tr'),
        $msg = $container.find('.cellOptionsMsg');
    
    $container.toggleClass('open');
    if ($container.hasClass('open')) {
      $btn.addClass('fa-times')
        .removeClass('fa-eye');
      
      // Agrandamos al $msg tanto como el ancho del $tr 
      // menos el ancho del $td menos ancho del $btn
      $msg.animate({width: $tr.outerWidth() - $td.outerWidth() - $btn.outerWidth()}, 500);
    }
    else {
      $btn.removeClass('fa-times')
        .addClass('fa-eye');
      
      // Achicamos $msg a 0
      $msg.animate({width: 0}, 500);
    }
  });
});
</script>