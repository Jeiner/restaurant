<div class="modal fade" id="modal_cumpleanios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
      		<div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> <strong> Próximos cumpleaños  </strong> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -30px">
          			<span aria-hidden="true">&times;</span>
                </button>
      		</div>
      		<div class="modal-body">
				<div id="profile-feed-1" class="profile-feed ace-scroll" style="position: relative;">
            		<?php foreach ($proximos as $key => $prox): ?>
            				<div class="profile-activity clearfix">
								<div>
									<img class="pull-left" alt="" src="<?= base_url('assets/img/user.png'); ?>">
									<a class="name" href="#"><strong> <?= $prox->nombre_completo ?> </strong> </a>
									<div class="time" style="color: #000">
										<i class="ace-icon fa fa-calendar bigger-110"></i>
										<?php 
                      $anual = substr($prox->fecha_nacimiento,0, 4);
                      $mes = substr($prox->fecha_nacimiento,5, 2);
                      $dia = substr($prox->fecha_nacimiento,8, 2);
                      // echo $prox->fecha_nacimiento;
                      $fecha_cumple = date('Y').'-'.$mes.'-'.$dia;
                      $fecha_cumple = new DateTime($fecha_cumple);
            				?>
        				    <?= strtoupper($this->Site->dias[$fecha_cumple->format('l')]) ?> 
                            <?= $fecha_cumple->format('d') ?> 
                            de <strong><?= strtoupper($this->Site->meses[$fecha_cumple->format('F')]) ?> </strong>
                            del <?= $fecha_cumple->format('Y') ?> 
									</div>
								</div>
							</div>
            		<?php endforeach ?>
            	</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      		</div>
    	</div>
  	</div>
</div>