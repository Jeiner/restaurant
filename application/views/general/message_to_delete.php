
<div class="divMessageBox hidden" id="msgEliminar">
    <div class="MessageBoxContainer ">
        <div class="row">
            <div class="col-sm-9 col-sm-offset-3">
                <div class="MsgTitle">
                    <i class="fa fa-times" style="font-size: 30px;"></i>
                    <span class="titleMsg">Eliminar la entidad</span>
                </div>    
            </div>        
        </div>
        <div class="row">
             <div class="col-sm-9 col-sm-offset-3">
                <div class="MsgText">
                    ¿Está seguro de eliminar la entidad, recuerde que esta operación es irreversible?
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-8">
                <div class="MsgOptions ">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <button class="btn btn-primary btn-xs btn-block" id="closeMsg" rel="divMessageBox" > NO </button>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <a href="" class="btn btn-danger btn-xs btn-block" id="siEliminar"> SI </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function alertToDelete(btn){
        $("#msgEliminar").fadeIn("slow");
        $("#msgEliminar").removeClass('hidden');
        var msg = $(btn).attr('msg');
        var href = $(btn).attr('href');
        var titleMsg = $(btn).attr('titleMsg');
        
        if(msg != ''){
            $(".divMessageBox .MsgText").html(msg);
        }
        if(titleMsg != ''){
            $(".divMessageBox .titleMsg").html(titleMsg);
        }
        $(".divMessageBox #siEliminar").attr('href',href);
        return false;
    }
    // $(".btn-delete").click(function(){
    //     alert("msg");
    //     $("#msgEliminar").fadeIn("slow");
    //     $("#msgEliminar").removeClass('hidden');
    //     var msg = $(this).attr('msg');
    //     var href = $(this).attr('href');
    //     var titleMsg = $(this).attr('titleMsg');
        
    //     if(msg != ''){
    //         $(".divMessageBox .MsgText").html(msg);
    //     }
    //     if(titleMsg != ''){
    //         $(".divMessageBox .titleMsg").html(titleMsg);
    //     }
    //     $(".divMessageBox #siEliminar").attr('href',href);
    //     return false;
    // });
    $("#siEliminar").click(function(){
        $("#msgEliminar").fadeOut("fast");
    });
    $('#closeMsg').click(function(){
        var rel = '.'+$(this).attr('rel');
        $(rel).fadeOut("slow");
    });
</script>
