<!-- menu_lateral_compact -->
<ul class="nav nav-list" style="margin-top: 20px">
	<li class="hover <?php if($modulo == 'perfil') echo 'open' ?> ">
		<a href="javascript:;" class="dropdown-toggle">
			<i class="menu-icon fa fa-user"></i>
			<span class="menu-text">
				<?= substr($session_nombre_completo, 0, 10) ?>
			</span>
			<b class="arrow fa fa-angle-down"></b>
		</a>
		<ul class="submenu can-scroll">
			<li class="hover <?php if($item == 'mis_datos') echo 'active' ?>">
				<a href="<?= base_url("perfil/datos") ?>">
					<i class="menu-icon fa fa-caret-right"></i>
					Mis datos
				</a>
				<b class="arrow"></b>
			</li>
			<li class="hover <?php if($item == 'mis_marcaciones') echo 'active' ?>">
				<a href="<?= base_url("perfil/marcaciones/listar") ?>">
					<i class="menu-icon fa fa-caret-right"></i>
					Mis marcaciones
				</a>
				<b class="arrow"></b>
			</li>
			<li class="hover <?php if($item == 'nueva_marcacion') echo 'active' ?>">
				<a href="<?= base_url("perfil/marcaciones/nueva") ?>">
					<i class="menu-icon fa fa-caret-right"></i>
					Nueva marcacion
				</a>
				<b class="arrow"></b>
			</li>
		</ul>
	</li>
</ul>

<ul class="nav nav-list" style="top: 0px;">
	<?php foreach ($modulos as $key => $mod): ?>
		<?php if ($this->session->userdata('session_tipo_sistema') ==  $mod->tipo_sistema || $mod->tipo_sistema == '' ): ?>
			<li class="hover <?php if($modulo == $mod->para_seleccionar) echo 'open' ?> " >
				<a href="#" class="dropdown-toggle">
					<?= $mod->icono ?>
					<span class="menu-text">
						<?= $mod->modulo ?>
					</span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>
				<ul class="submenu can-scroll">
					<?php foreach ($opciones as $key => $opc): ?>
						<?php if ($opc->modulo_id == $mod->modulo_id && ($this->session->userdata('session_tipo_sistema') == $opc->tipo_sistema || $opc->tipo_sistema == '') ): ?>
							<?php
								$link = base_url($mod->para_seleccionar.'/'.$opc->controlador.'/'.$opc->accion)
							?>
							<li class="hover <?php if($item == $opc->para_seleccionar) echo 'active' ?>" >
								<a href="<?= $link ?>">
									<i class="menu-icon fa fa-caret-right"></i>
									<?= $opc->opcion ?>
								</a>
								<b class="arrow"></b>
							</li>
						<?php endif ?>
					<?php endforeach ?>
				</ul>
			</li>
		<?php endif ?>
	<?php endforeach ?>
</ul>