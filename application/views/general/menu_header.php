<!-- menu_header -->
<div id="navbar" class="navbar navbar-default" style="top:0; left: 0; right: 0; z-index: 1033; position: fixed;">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>
			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left hidden" style="color: #FFFFFF; font-size: 17px;" onclick="ir_atras();" id="ir_atras">
					<i class="ace-icon fa fa-arrow-left bigger-90" aria-hidden="true"></i>
				</button>
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-left">
					<a href="<?= base_url(); ?>" class="navbar-brand" title="Inicio">
						<div>
							<!-- SANTORINI -  -->
							<?= 
								$oGeneralidades->nombre_comercial." - ".$oGeneralidades->local 
							?>
						</div>
					</a>
				</div>
				<?php if ($this->session->userdata('session_tipo_sistema') == 'PROD'): ?>
					<div class="hidden-xs">
						<div id="menu_opciones" style="float: left; position: relative; left: 20%"> 
							<div id="contenedor opciones" style="float: left;position: relative;left: -30%;">

								<?php if ($oGeneralidades->tiene_delivery_online): ?>
								<a href="<?= $lstVariables['url_ver_pedidos_online'] ?>" class="btn btn-warning" target="blanck" aria-expanded="false" id="verNroPedidosOnline" style="display:none">
									<span data-placement="bottom" data-rel="tooltip" title="Pedidos online" style="font-size: 16px; font-weight: 500" id="nroPedidosOnline" >
										<!-- Cantidad de pedidos online (5) -->
									</span>
								</a>
								<?php endif ?>


								<a href="<?= base_url('salon/salon'); ?>" class="btn btn-success" href="javascript:;" aria-expanded="false">
									<span data-placement="bottom" data-rel="tooltip" title="Salón" >
										<i class="ace-icon fa fa-th bigger-160"></i>
									</span>
								</a>
								<a href="<?= base_url('salon/salon/ultimas_comandas'); ?>" class="btn btn-primary" href="javascript:;" aria-expanded="false">
									<span data-placement="bottom" data-rel="tooltip" title="Últimas comandas" >
										<i class="ace-icon fa fa-list-ul bigger-160"></i>
									</span>
								</a>
							</div>
						</div>
					</div>
				<?php endif ?>


				<div class="navbar-buttons navbar-header pull-right hidden-xs" role="navigation">
					<ul class="nav ace-nav">
						<?php if (count($proximos_cumples) > 0): ?>
							<li class="blue">
								<a href="javascript:;" onclick="modal_proximos_cumples();">
									<i class="ace-icon fa fa-birthday-cake"></i>
									<span class="badge badge-danger">
										<?= count($proximos_cumples) ?>
									</span>
								</a>
							</li>
						<?php endif ?>
							
						<li class="grey">
							<?php $total_alertas = $total_insumos_por_agotar + $total_productos_por_agotar; ?>
							<a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
								<i class="ace-icon fa fa-bell"></i>
								<span class="badge badge-danger">
									<?= $total_alertas ?>
								</span>
							</a>
							<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close" style="">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-check"></i>
									Alertas administrativas
								</li>
								<li class="dropdown-content ace-scroll" style="position: relative;"><div class="scroll-track" style="display: none;"><div class="scroll-bar"></div></div><div class="scroll-content" style="">
									<ul class="dropdown-menu dropdown-navbar">
										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-primary fa fa-calendar"></i>
														Productos por vencer
													</span>
													<span class="pull-right badge badge-danger">
														
													</span>
												</div>
											</a>
										</li>
										<?php if ($total_productos_por_agotar > 0): ?>
											<li>
												<a href="<?= base_url('inventario/inventario/productos_por_agotar'); ?>">
													<div class="clearfix">
														<span class="pull-left">
															<i class="btn btn-xs no-hover btn-primary fa fa-comment"></i>
															Productos por agotarse
														</span>
														<span class="pull-right badge badge-danger">
															<?= $total_productos_por_agotar ?>
														</span>
													</div>
												</a>
											</li>
										<?php endif ?>
										<?php if ($total_insumos_por_agotar > 0): ?>
											<li>
												<a href="<?= base_url('inventario/inventario/insumos_por_agotar'); ?>">
													<div class="clearfix">
														<span class="pull-left">
															<i class="btn btn-xs no-hover btn-primary fa fa-comment"></i>
															Insumos por agotarse
														</span>
														<span class="pull-right badge badge-danger">
															<?= $total_insumos_por_agotar ?>
														</span>
													</div>
												</a>
											</li>
										<?php endif ?>
									</ul>
								</div></li>
							</ul>
						</li>
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?= base_url('assets/img/user.png') ?>" alt="" />
								<span class="user-info" title="<?= $session_nombre_completo ?>">
									<small>Bienvenido,</small>
									<?= $session_nombre_completo ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<!-- <li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										Settings
									</a>
								</li> -->
								<li>
									<a href="#">
										<i class="ace-icon fa fa-user"></i>
										Perfil
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="<?= base_url('seguridad/acceso/cerrar_sesion')?>">
										<i class="ace-icon fa fa-power-off"></i>
										Cerrar sesión
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>


			</div><!-- /.navbar-container -->
		</div>

<script type="text/javascript">
	function modal_proximos_cumples(){
		alert();
	}
</script>
<script type="text/javascript">
	obtenerPedidosOnline();
	function obtenerPedidosOnline(){
		$.ajax({
	        type: 'POST',
	        url: "<?=base_url('servicios/pedidos_online/ajax_obtenerPedidosOnline')?>",
	        data: "",
	        success: function(rpta){
	        	objeto = JSON.parse(rpta);
	        	if(objeto.success){
	        		if(objeto.nroPedidosPendientes < 0){
	        			$("#verNroPedidosOnline").show('fast');
	        			$("#nroPedidosOnline").append("Error")
	        		}

	        		if(objeto.nroPedidosPendientes > 0){
	        			$("#verNroPedidosOnline").show('fast');
	        			$("#nroPedidosOnline").append(objeto.nroPedidosPendientes)
	        		}

	        		if(objeto.nroPedidosPendientes == 0){
	        			$("#verNroPedidosOnline").hide('fast');
	        		}
	        	}else{
        			$("#verNroPedidosOnline").show('fast');
        			$("#nroPedidosOnline").append("Error")
	        	}
	        },
	        error: function(rpta){
	            alertify.error(rpta);
	            location.reload();
	        }
	    });
	}
</script>
<script type="text/javascript">
	function ir_atras(){
		javascript:window.history.back();
	}
</script>