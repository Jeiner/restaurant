<!-- menu_lateral -->
<ul class="nav nav-list">
	<?php foreach ($modulos as $key => $mod): ?>
		<li <?php if($modulo == $mod->para_seleccionar) echo 'class="open"' ?> >
			<a href="#" class="dropdown-toggle">
				<?= $mod->icono ?>
				<span class="menu-text">
					<?= $mod->modulo ?>
				</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<?php foreach ($opciones as $key => $opc): ?>
					<?php if ($opc->modulo_id == $mod->modulo_id): ?>
						<?php
							$link = base_url($mod->para_seleccionar.'/'.$opc->controlador.'/'.$opc->accion)
						?>
						<li <?php if($item == $opc->para_seleccionar) echo 'class="active"' ?>>
							<a href="<?= $link ?>">
								<i class="menu-icon fa fa-caret-right"></i>
								<?= $opc->opcion ?>
							</a>
							<b class="arrow"></b>
						</li>
					<?php endif ?>
				<?php endforeach ?>
			</ul>
		</li>
	<?php endforeach ?>
</ul>
