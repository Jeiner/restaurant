<?php 
	$oGeneralidades = $this->session->userdata('session_generalidades');
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<!-- CSRF Token -->
    <title><?= $page_title ?> - <?= $oGeneralidades->titulo_sistema  ?></title>

	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<link rel="shortcut icon" href="<?= base_url($oGeneralidades->url_icon)?> " />
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto|Titillium+Web" rel="stylesheet"> -->
	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?> " />
	<link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css')?> " />

	<!-- text fonts -->
	<link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css')?> " />

	<!-- ace styles -->
	<link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css')?> " class="ace-main-stylesheet" id="main-ace-style" />

	<link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css')?> " />
	<link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css')?> " />

	<link rel="stylesheet" href="<?= base_url('assets/img/splashy/splashy.css') ?> " />

	<!-- ace settings handler -->
	<script src="<?= base_url('assets/js/ace-extra.min.js')?> "></script>

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/jqeasyui/themes/icon.css') ?> ">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/update_template.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/library/alertify/alertify.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/library/alertify/default.rtl.css')?> " />

    <script src="<?= base_url('assets/js/jquery-2.1.4.min.js')?> "></script>
    <script src="<?= base_url('assets/js/jquery.validate.min.js')?> "></script>
    
	<script type="text/javascript" src="<?= base_url('assets/library/alertify/alertify.min.js') ?> "></script>

	<script type="text/javascript" src="<?= base_url('assets/library/sweetalert/sweetalert.min.js') ?> "></script>
	<link rel="stylesheet" href="<?= base_url('assets/library/sweetalert/sweetalert.css')?> " />
	
<!-- REPORTES -->
	<script src="<?= base_url() ?>assets/highcharts/code/highcharts.js"></script>
	<script src="<?= base_url() ?>assets/highcharts/code/modules/series-label.js"></script>
	<script src="<?= base_url() ?>assets/highcharts/code/modules/exporting.js"></script> 
</head>
<style type="text/css">
	body{
		/*font-family: 'Roboto', sans-serif!important;*/
		font-family: 'Titillium Web', sans-serif!important;
	}
</style>
<body class="no-skin">
		<?php $this->load->view('general/menu_header'); ?>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			<?php 
				$this->load->view('general/scripts'); 
			?>
			<div id="sidebar" class="sidebar responsive ace-save-state compact">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>


				<?php $this->load->view('general/menu_lateral_compact'); ?>

				<hr>
				<ul class="nav nav-list">
					<li class="hover-show hover-shown">
						<a href="<?= base_url('seguridad/acceso/cerrar_sesion')?>" >
							<i class="menu-icon fa fa-power-off"></i>
							<span class="menu-text">
								Cerrar sesión
							</span>
						</a>
					</li>
				</ul>

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

			</div>
			<div class="main-content" style="margin-top: 45px">
				<div class="main-content-inner">
					<?php $this->load->view('general/direccion'); ?>
					<div class="page-content">
					<?php $this->load->view('general/message'); ?>
					<?php $this->load->view('general/preloader'); ?>
					<?php $this->load->view('general/acciones_sesion'); ?>
					<?php $this->load->view('general/acciones_para_nube'); ?>
			
