<div class="modal fade" id="modalCargando" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="">
	<div class="modal-dialog" align="center">
	  <div class="modal-content" align="center" style="margin-top:40%;width:50%;">
	    <img src="<?=base_url('assets/img/preloader_comida.gif')?>" class="img-responsive" style="padding:10%">
	  </div>
	  <!-- /.modal-content -->
	</div>
	<!-- /.modal-Dialog -->
</div>
<!-- /.modalCargando -->

<script type="text/javascript">
	var numeroCargas = 0;
	function cerrarModal(modal){
		$(modal).modal('hide');
	}
	function abrirModal(modal){
		$(modal).modal({
			show:true,
			backdrop:'static',
		});
	}
	function abrirCargando(){
		numeroCargas++;
		if(numeroCargas == 1){
			abrirModal('#modalCargando');	
		}
	}
	function cerrarCargando(){
		numeroCargas--;
		if(numeroCargas <= 1){
			cerrarModal('#modalCargando');
			numeroCargas = 0;
		}
	}
	
</script>

