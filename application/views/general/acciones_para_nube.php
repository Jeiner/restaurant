

<script type="text/javascript">
    function enviar_comnada_nube(comanda_id){
        console.log("Inicio de proceso: Enviar comanda a la nube Santorini.");
        var url = '<?= base_url("nube/nube/ajax_popup_enviar_comanda_nube"); ?>' + "?comanda_id=" + comanda_id;
        $.ajax({
            type: 'GET',
            url: url,
            data: {},
            success: function(rpta){
                console.log(rpta);
                console.log("Fin de proceso enviar comanda.");
            },
            error: function(rpta){
                console.log("Error al enviar la comanda.");
            }
        });
        return false;
    }

    function enviar_comprobante_nube(cabecera_id){
        console.log("Inicio de proceso: Enviar comprobante a la nube Santorini.");
        var url = '<?= base_url("nube/nube/ajax_popup_enviar_comprobante_nube"); ?>' + "?cabecera_id=" + cabecera_id;
        $.ajax({
            type: 'GET',
            url: url,
            data: {},
            success: function(rpta){
                console.log(rpta);
                console.log("Fin de proceso enviar comprobante.");
            },
            error: function(rpta){
                console.log("Error al enviar la comprobante.");
            }
        });
        return false;
    }

    function enviar_pago_nube(pago_id){
        console.log("Inicio de proceso: Enviar pago a la nube Santorini.");
        var url = '<?= base_url("nube/nube/ajax_popup_enviar_pago_nube"); ?>' + "?pago_id=" + pago_id;
        $.ajax({
            type: 'GET',
            url: url,
            data: {},
            success: function(rpta){
                console.log(rpta);
                console.log("Fin de proceso enviar pago.");
            },
            error: function(rpta){
                console.log("Error al enviar el pago.");
            }
        });
        return false;
    }

    function enviar_caja_nube(caja_id){
        console.log("Inicio de proceso: Enviar caja a la nube Santorini.");
        var url = '<?= base_url("nube/nube/ajax_popup_enviar_caja_nube"); ?>' + "?caja_id=" + caja_id;
        $.ajax({
            type: 'GET',
            url: url,
            data: {},
            success: function(rpta){
                console.log(rpta);
                console.log("Fin de proceso enviar caja a nube.");
            },
            error: function(rpta){
                console.log("Error al enviar caja a la nube.");
            }
        });
        return false;
    }

</script>

<script type="text/javascript">
    //Enviar comanda a nube
    <?php if ($this->session->userdata('ses_enviar_comanda_a_nube')): ?>
        var comanda_id = "<?= $this->session->userdata('ses_enviar_comanda_a_nube_comanda_id') ?>";
        enviar_comnada_nube(comanda_id);
        <?php 
            $this->session->unset_userdata('ses_enviar_comanda_a_nube'); 
            $this->session->unset_userdata('ses_enviar_comanda_a_nube_comanda_id'); 
        ?>
    <?php endif ?>

    //Enviar comprobante a nube
    <?php if ($this->session->userdata('ses_enviar_comprobante_a_nube')): ?>
        var comprobante_id = "<?= $this->session->userdata('ses_enviar_comprobante_a_nube_cabecera_id') ?>";
        enviar_comprobante_nube(comprobante_id);
        <?php 
            $this->session->unset_userdata('ses_enviar_comprobante_a_nube'); 
            $this->session->unset_userdata('ses_enviar_comprobante_a_nube_cabecera_id'); 
        ?>
    <?php endif ?>

    //Enviar pago a nube
    <?php if ($this->session->userdata('ses_enviar_pago_a_nube')): ?>
        var pago_id = "<?= $this->session->userdata('ses_enviar_pago_a_nube_pago_id') ?>";
        enviar_pago_nube(pago_id);
        <?php 
            $this->session->unset_userdata('ses_enviar_pago_a_nube'); 
            $this->session->unset_userdata('ses_enviar_pago_a_nube_pago_id'); 
        ?>
    <?php endif ?>

    //Enviar caja a nube
    <?php if ($this->session->userdata('ses_enviar_caja_a_nube')): ?>
        var caja_id = "<?= $this->session->userdata('ses_enviar_caja_a_nube_caja_id') ?>";
        enviar_caja_nube(caja_id);
        <?php 
            $this->session->unset_userdata('ses_enviar_caja_a_nube'); 
            $this->session->unset_userdata('ses_enviar_caja_a_nube_caja_id'); 
        ?>
    <?php endif ?>
</script>