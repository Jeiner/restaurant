<?php if ($oCaja != null): ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <button class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <?php 
                    setlocale(LC_ALL,"es_ES");
                    $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

                    $aperturado_en = date($oCaja->aperturado_en);
                    $inicio = new DateTime($oCaja->aperturado_en);
                    $fin = new DateTime($oCaja->cerrado_en);
                ?>
                Información mostrada desde el 
                <strong>
                    <?= $this->Site->dias[$inicio->format('l')] ?> 
                    <?= $inicio->format('d') ?> 
                    de <?= $this->Site->meses[$inicio->format('F')] ?> 
                    del <?= $inicio->format('Y') ?> 
                    a las <?= $inicio->format('h:i:s A') ?>
                </strong> 
                hasta
                <strong>
                    <?php if ($oCaja->cerrado_en != null): ?>
                        <strong>
                            <?= $this->Site->dias[$fin->format('l')] ?>
                            <?= $fin->format('d') ?>
                            de <?= $this->Site->meses[$fin->format('F')] ?>
                            del <?= $fin->format('Y') ?>
                            a las <?= $fin->format('h:i:s A') ?></strong>
                    <?php else: ?>
                        -
                    <?php endif ?>
                </strong>
                <span class="pull-right" style="padding-right: 20px;">
                    Caja: 
                    <?php if ($oCaja->estado == 'A'): ?>
                        <strong>Abierta</strong>
                    <?php else: ?>
                        <strong>Cerrada</strong>
                    <?php endif ?>
                </span>
            </div>
        </div> 
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                Ho hay datos para mostrar estadísticas.
            </div>
        </div> 
    </div>
<?php endif ?>

<div class="row">
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
		<div class="info-box">
            <div class="info-box-content">
            	<div class="info-box-title">
            		<span class="info-box-text">
	              		Ventas
	              		<!-- <div class="pull-right">HOLA</div> -->
	              	</span>	
            	</div>
            	<div class="info-box-body">
            		<span class="info-box-number" id="">
            			S/. <text id="total_ventas"><small class="cargando">Cargando...</small> </text>
            		</span>
	              	<div class="pull-left">Total de ventas</div>
	              	<div class="pull-right red2">
	              		<strong>
	              			Descuento: S/. <text id="total_descuentos"><small class="red2">...</small></text>
              			</strong> 
              		</div>
            	</div>
        	</div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
	</div>
    <!-- col -->

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="info-box">
            <div class="info-box-content">
                <div class="info-box-title">
                    <span class="info-box-text">
                        Pagos - por tipo
                    </span> 
                </div>
                <div class="info-box-body">
                    <span class="info-box-number" id="" style="font-size: 14px">
                        Efectivo: S/.
                        <text id="pago_efectivo"><small class="cargando">Cargando...</small></text>
                        <br>
                        Tarjeta: S/.
                        <text id="pago_tarjeta"><small class="cargando">Cargando...</small></text> 
                        <div class="pull-right green">
                            Total: S/.
                            <small id="total_pagado">
                                <text class="cargando" >Cargando...</text>
                            </small> 
                        </div>
                    </span>
                    
                </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
    </div>
    <!-- col -->
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="info-box">
            <div class="info-box-content">
                <div class="info-box-title">
                    <span class="info-box-text">
                        Atenciones
                    </span> 
                </div>
                <div class="info-box-body">
                    <span class="info-box-number" id="">
                        <text id="atenciones_pre"><small class="cargando">Cargando...</small></text>
                        <small style="font-weight: none!important">Presenciales</small>
                    </span>
                    <div class="pull-left">Delivery: <span id="atenciones_del">...</span></div>
                    <div class="pull-right">Para llevar: <span id="atenciones_lle">...</span></div>
                </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
    </div>
    <!-- col -->
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="info-box">
            <div class="info-box-content">
                <div class="info-box-title">
                    <span class="info-box-text">
                        Prom. de comsumo por mesa
                    </span> 
                </div>
                <div class="info-box-body">
                    <span class="info-box-number" id="">
                        S/. <text id="promedio_x_mesa"><small class="cargando">Cargando...</small> </text>
                    </span>
                    <div class="pull-left">En <span id="nro_comandas_x_mesa"></span> ventas presenciales</div>
                </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
    </div>
    <!-- col -->
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="info-box">
            <div class="info-box-content">
                <div class="info-box-title">
                    <span class="info-box-text">
                        Mozo del dia
                    </span> 
                </div>
                <div class="info-box-body" style="overflow-x: hidden">
                    <span class="info-box-number" id="">
                        <text id="mozo_del_dia"><small class="cargando">Cargando...</small> </text>
                    </span>
                    <div class="pull-left"><span id="nro_comandas_de_mozo"></span> Comandas </div>
                </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
    </div>
    <!-- col -->
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="info-box">
            <div class="info-box-content">
                <div class="info-box-title">
                    <span class="info-box-text">
                        Tiempo de atención
                    </span> 
                </div>
                <div class="info-box-body">
                    <span class="info-box-number" id="">
                        <text id="tiempo_min_atencion"><small class="cargando">...</small></text>
                        ' Mín -
                        <text id="tiempo_max_atencion"><small class="cargando">...</small></text> 
                        ' Máx
                    </span>
                    <div class="pull-left"><span id="nro_comandas_de_mozo"></span> Comandas </div>
                </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
    </div>
    <!-- col -->
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="info-box">
            <div class="info-box-content">
                <div class="info-box-title">
                    <span class="info-box-text red">
                        Comandas anuladas
                    </span> 
                </div>
                <div class="info-box-body">
                    <span class="info-box-number" id="">
                        <text id="atenciones_anu"><small class="cargando">Cargando...</small></text>
                    </span>
                    <div class="pull-left"><span id="nro_comandas_de_mozo"></span> Comandas </div>
                </div>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- info box -->
    </div>
    <!-- col -->
</div>
<br>
<div class="row">
    <div class="col-sm-6">
        <div class="tabbable">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="tabla_ventas">
                <li class="active">
                    <a data-toggle="tab" href="#ventas_diarias">
                        <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                        Gráfico de ventas diarias
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#ventas_mensuales">
                        <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                        Gráfico de ventas mensual
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="ventas_diarias" class="tab-pane in active">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <select class="form-control" id="cbo_mes_vd" name="cbo_mes_vd" onchange="crear_grafico_diario(null, this.value);" > <!-- Combo mes ventas diarias -->
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" id="tabla_cierres">
                            <div id="container_ventas_diarias" style="min-width: 310px; height: 400px; margin: 0 auto">
                                
                            </div>
                        </div>
                    </div> 
                </div>
                <div id="ventas_mensuales" class="tab-pane in">
                    <div class="row">
                        <div class="col-sm-12" id="tabla_cierres">
                            <div id="container_ventas_mensuales" style="min-width: 310px; height: 400px; margin: 0 auto">
                                
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	obtener_estadisticas();
	function obtener_estadisticas(){
		$.ajax({
            type: 'POST',
            dataType: "json",
            url: "<?=base_url('main/obtener_estadisticas')?>",
            data: {  },
            success: function(rpta){
                if(rpta.success){
                    $('#total_ventas').html(rpta.total_ventas);
                    $('#total_descuentos').html(rpta.total_descuentos);
                    $('#promedio_x_mesa').html(rpta.promedio_x_mesa);
                    $('#nro_comandas_x_mesa').html(rpta.nro_comandas_x_mesa);
                    $('#mozo_del_dia').html(rpta.mozo_del_dia);
                    $('#nro_comandas_de_mozo').html(rpta.nro_comandas_de_mozo);
                    $('#tiempo_min_atencion').html(rpta.tiempo_min_atencion);
                    $('#tiempo_max_atencion').html(rpta.tiempo_max_atencion);
                    $('#pago_efectivo').html(rpta.pago_efectivo);
                    $('#pago_tarjeta').html(rpta.pago_tarjeta);
                    $('#total_pagado').html(rpta.total_pagado);
                    $('#atenciones_pre').html(rpta.atenciones_pre);
                    $('#atenciones_del').html(rpta.atenciones_del);
                    $('#atenciones_lle').html(rpta.atenciones_lle);
                    $('#atenciones_anu').html(rpta.atenciones_anu);
                    
                }else{
                    alertify.error(rpta.mensaje);
                    $('#total_ventas').html("");
                    $('#total_descuentos').html("");
                    $('#promedio_x_mesa').html("");
                    $('#nro_comandas_x_mesa').html("");
                    $('#mozo_del_dia').html("");
                    $('#nro_comandas_de_mozo').html("");
                    $('#tiempo_min_atencion').html("");
                    $('#tiempo_max_atencion').html("");
                    $('#pago_efectivo').html("");
                    $('#pago_tarjeta').html("");
                    $('#total_pagado').html("");
                    $('#atenciones_pre').html("");
                    $('#atenciones_del').html("");
                    $('#atenciones_lle').html("");
                    $('#atenciones_anu').html("");
                }
            	
            	
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
	}
</script>

<!-- Para grafico diario -->
<script type="text/javascript">
    // Datos iniciales
    var gb_fecha = new Date();  //Variables globales
    var gb_anioActual = gb_fecha.getFullYear();
    var gb_mesActual = gb_fecha.getMonth()+1;
    // Asignacion inicial de inputs
    $("#cbo_mes_vd").val(gb_mesActual);
    // Funciones iniciales
    crear_grafico_diario(gb_anioActual, gb_mesActual);

    // 
    function cant_ds(mes,ano){ 
        di=28;
        f = new Date(ano,mes-1,di); 
        while(f.getMonth() == mes-1){ 
            di++;
            f = new Date(ano,mes-1,di); 
        } 
        return di-1; 
    } 
    function crear_grafico_diario(anio = '', mes){
        var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Setiembre','Octubre', 'Noviembre', 'Diciembre'];
        // var fecha = new Date();
        var anioActual = (anio == '' || anio == null) ? gb_anioActual : anio;
        var mesActual = mes;
        
        var cant_dias = cant_ds(mesActual,anioActual);
        var array_dias = new Array(cant_dias);
        // var array_nombre_dias = new Array(cant_dias);
        var array_ventas = new Array(cant_dias);
        var array_productos = new Array(cant_dias);

        for (var i = 0; i < cant_dias; i++) {
            array_dias[i] = i+1;
        }
        for (var i = 0; i < cant_dias; i++) {
            array_ventas[i] = 0;
        }
        for (var i = 0; i < cant_dias; i++) {
            array_productos[i] = 0;
        }
        $.ajax({
            type: 'POST',
            data: 'mes='+mesActual+"&anio="+anioActual,
            url: '<?= base_url().'reportes/admin/ventas_diarias' ?>',
            success: function(response){
                array_count = JSON.parse(response);
                array_count.forEach(function(o){
                   array_ventas[o.dia-1] = parseFloat(o.importe);
                   array_productos[o.dia-1] = parseFloat(o.productos);
                });
                dibujar_grafico_diario(array_meses[mesActual-1],array_dias,array_ventas,array_productos);
            },
            error: function(response){
                var msj = JSON.stringify(response);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }
    function dibujar_grafico_diario(mes,array_dias,array_ventas,array_productos){
        Highcharts.chart('container_ventas_diarias', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Ventas diarias del mes de '+mes
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: array_dias,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series:[{
                name: 'Monto de ventas (S/.)',
                data: array_ventas,

            },{
                name: 'N° de productos (Unidades)',
                data: array_productos,
                visible:false
            }]
        }); 
    }
</script>

<!-- Para grafico mensual -->
<script type="text/javascript">
    crear_grafico_mensual();

    function crear_grafico_mensual(){
        // var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Septiembre','Octubre', 'Noviembre', 'Diciembre']
        var array_meses = ['Ene','Feb', 'Mar', 'Abr','May','Jun','Jul', 'Ago', 'Sep','Oct', 'Nov', 'Dic'];
        var fecha = new Date();
        var anioActual = fecha.getFullYear();
        
        var array_ventas = new Array(12);
        var array_productos = new Array(12);

       
        for (var i = 0; i < 12; i++) {
            array_ventas[i] = 0;
        }
        for (var i = 0; i < 12; i++) {
            array_productos[i] = 0;
        }
        $.ajax({
            type: 'POST',
            data: "anio="+anioActual,
            url: '<?= base_url().'reportes/admin/ventas_mensuales' ?>',
            success: function(response){
                array_count = JSON.parse(response);
                array_count.forEach(function(o){
                   array_ventas[o.mes-1] = parseFloat(o.importe);
                   array_productos[o.mes-1] = parseFloat(o.productos);
                });
                dibujar_grafico_mensual(anioActual,array_meses,array_ventas,array_productos);
            },
            error: function(response){
                var msj = JSON.stringify(response);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }
    function dibujar_grafico_mensual(anioActual,array_meses,array_ventas,array_productos){
        Highcharts.chart('container_ventas_mensuales', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Ventas mensuales del año '+anioActual
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: array_meses,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series:[{
                name: 'Monto de ventas (S/.)',
                data: array_ventas,

            },{
                name: 'N° de productos (Unidades)',
                data: array_productos,
                visible:false
            }]
        }); 
    }
</script>