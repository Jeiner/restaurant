<div class="breadcrumbs" id="breadcrumbs">
	<ul class="breadcrumb">
		<?php foreach ($bc as $key => $item): ?>
			<li class=""><a href="<?= $item['link'] ?>"><?= $item['page'] ?></a></li>
		<?php endforeach ?>
	</ul><!-- /.breadcrumb -->
	<div class="nav-search" id="nav-search">
		<i class="ace-icon fa fa-calendar"></i>
		<span class="input-icon">
			<?= date("d") . " del " . date("m") . " de " . date("Y"); ?>
		</span>
	</div><!-- /.nav-search -->
</div>
