<!--  
	Acciones que se realizan dependiente las variables de session
	Ejem: Enviar documento electrònico a FACSU
-->
<script type="text/javascript">
	var ruta_ticket = "<?= $this->Site->get_ruta_proy_ticket() ?>";
    var url_facsu_generar_archivos = "<?= $this->Site->obtener_variable('facsu_generar_xml') ?>";
    var url_facsu_procesar_cpbes = "<?= $this->Site->obtener_variable('facsu_procesar_cpbes') ?>";

	//Enviar comprobante electrònico a FACSU
	<?php if ($this->session->userdata('ses_enviar_cbe_a_FACSU')): ?>
		var ruta = '<?= base_url("facturacion/documento_electronico/popup_enviar_cbe_a_facsu"); ?>' + "/" + "<?= $this->session->userdata('ses_enviar_cbe_a_FACSU_comanda_id') ?>";
        window.open(ruta,'_blank', 'width=1000,height=700');
        <?php 
            $this->session->unset_userdata('ses_enviar_cbe_a_FACSU'); 
            $this->session->unset_userdata('ses_enviar_cbe_a_FACSU_comanda_id'); 
        ?>
    <?php endif ?>
    // IMPRIMIR COMPROBANTE ELECTRÓNICO
    <?php if ($this->session->userdata('enviar_comprobante_e')): ?>
		url_cpbt = ruta_ticket + "imprimir_comprobante_e.php?cabecera_id=" + "<?= $this->session->userdata('cabecera_id_para_comprobante') ?>";
        window.open(url_cpbt,'_blank', 'width=1000,height=700');
        <?php 
            $this->session->unset_userdata('enviar_comprobante_e'); 
            $this->session->unset_userdata('cabecera_id_para_comprobante'); 
        ?>
    <?php endif ?>
    // ENVIAR TICKET A COCINA
	<?php if ($this->session->userdata('enviar_ticket_cocina')): ?>
		url_ticket = ruta_ticket + "imprimir_para_cocina.php?comanda_id=" + "<?= $this->session->userdata('comanda_id_para_ticket') ?>";
        window.open(url_ticket,'_blank', 'width=1000,height=700');
        <?php 
            $this->session->unset_userdata('enviar_ticket_cocina'); 
            $this->session->unset_userdata('comanda_id_para_ticket'); 
        ?>
    <?php endif ?>
    // IMPRIMIR PRODUCTO ADICIONAL
    <?php if ($this->session->userdata('enviar_adicional_cocina')): ?>
        url_adicional = "http://localhost:50/ticket/imprimir_adicional.php?comanda_item_id=" + "<?= $this->session->userdata('comanda_item_id') ?>";
        window.open(url_adicional,'_blank', 'width=500,height=300');
        <?php 
            $this->session->unset_userdata('enviar_adicional_cocina'); 
            $this->session->unset_userdata('comanda_item_id'); 
        ?>
    <?php endif ?>
    // IMPRIMIR CUENTA
    <?php if ($this->session->userdata('imprimir_comprobante')): ?>
        url_ticket = "http://localhost:50/ticket/imprimir_comprobante_local.php?comanda_id=" + "<?= $this->session->userdata('comanda_id_para_comprob') ?>";
        window.open(url_ticket,'_blank', 'width=1000,height=700');
        <?php 
            $this->session->unset_userdata('imprimir_comprobante'); 
            $this->session->unset_userdata('comanda_id_para_comprob'); 
        ?>
    <?php endif ?>

    // Generar archivos XML y procesar comporbantes electrónicos
    <?php if ($this->session->userdata('procesar_cpbes_facsu')): ?>
        window.open(url_facsu_generar_archivos,'_blank', 'width=1000,height=700');
        window.open(url_facsu_procesar_cpbes,'_blank', 'width=400,height=300');
        <?php 
            $this->session->unset_userdata('procesar_cpbes_facsu'); 
        ?>
    <?php endif ?>
</script>