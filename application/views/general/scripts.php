



<script src="<?= base_url('assets/js/bootstrap.min.js') ?> "></script>

<!-- page specific plugin scripts -->
<script src="<?= base_url('assets/js/bootstrap-datepicker.min.js') ?> "></script>
<script src="<?= base_url('assets/js/jquery.jqGrid.min.js') ?> "></script>
<script src="<?= base_url('assets/js/grid.locale-en.js') ?> "></script>

<!-- ace scripts -->
<script src="<?= base_url('assets/js/ace-elements.min.js') ?> "></script>
<script src="<?= base_url('assets/js/ace.min.js') ?> "></script>
<script src="<?= base_url('assets/js/script.js') ?> "></script>

<!-- inline scripts related to this page -->

<script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?> "></script>
<script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?> "></script>
<script src="<?= base_url('assets/js/dataTables.tableTools.min.js') ?> "></script>
<script src="<?= base_url('assets/js/dataTables.colVis.min.js') ?> "></script>
<script src="<?= base_url('assets/js/pluginModalPDF.js') ?> "></script>

<script src="<?= base_url('assets/js/chosen.jquery.min.js') ?> "></script>
<!-- Para graficos de estadisticas -->
<script src="<?= base_url('assets/js/jquery.easypiechart.min.js') ?> "></script>

<script src=" <?= base_url() ?>assets/js/form-validator.min.js"></script>