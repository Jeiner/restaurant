<!-- Mostrar mensajes como respuesta de una acción anterior (Registro, actualización, etc.) -->
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<?php if ($success !== null): ?>
			<?php if ($success): ?>
				<div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?= $message ?>
                </div>
			<?php else: ?>
				<div class="alert alert-block alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <?= $message ?>
                </div>
			<?php endif ?>
		<?php endif ?>
	</div>
</div>
