<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#insumos">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de insumos
            </a>
        </li>
        <li>
            <a  href="<?= base_url('administracion/insumos/nuevo') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo insumo
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="insumos" class="tab-pane in active">
            <?php $this->load->view('administracion/insumos/_table_insumos'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <i class="ace-icon fa fa-info"></i>
            <strong>Recuerde:</strong> Se debe considerar como unidades, la unidad minima que se utiliza para la preparación de platos.
            <br>
        </div>
    </div>
</div>