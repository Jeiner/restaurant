<form method="Post" action="<?= base_url('administracion/insumos/guardar') ?>" class="form-horizontal" id="form_insumo">
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Insumo_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="insumo_id" id="insumo_id" class="form-control input-sm" value="<?= $oInsumo->insumo_id ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Insumo <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="insumo" id="insumo" class="form-control input-sm" value="<?= $oInsumo->insumo ?>" placeholder="Nombre del insumo">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Unidad <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<select class="form-control input-sm chosen-select" id="unidad" name="unidad">
						<option value="">Seleccionar..</option>
						<?php foreach ($unidades as $key => $unidad): ?>
							<?php 
								$und_selected = "";
								if($oInsumo->unidad == $unidad->valor){
									$und_selected = "selected";
								}
							?>
							<option value="<?= $unidad->valor ?>"<?= $und_selected ?> ><?= $unidad->descripcion ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Stock actual <label class="red">*</label> </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="stock" id="stock" class="form-control input-sm" value="<?= $oInsumo->stock ?>" placeholder="Stock actual" onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Precio costo </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="precio_costo" id="precio_costo" class="form-control input-sm" value="<?= $oInsumo->precio_costo ?>" placeholder="Costo unitario (0.00)" onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Stock mínimo </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="stock_minimo" id="stock_minimo" class="form-control input-sm" value="<?= $oInsumo->stock_minimo ?>" onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Stock máximo </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="stock_maximo" id="stock_maximo" class="form-control input-sm" value="<?= $oInsumo->stock_maximo ?>" onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Estado  </label>
				<div class="col-sm-5 col-xs-12">
					<select class="form-control input-sm" id="estado" name="estado">
						<option> Seleccionar... </option>
						<option value="A" <?php if($oInsumo->estado == 'A') echo "selected" ?>> Activo </option>
						<option value="X" <?php if($oInsumo->estado == 'X') echo "selected" ?>> Inactivo </option>
					</select>
				</div>
			</div>
		</div> <!-- col -->
		<div class="col-sm-5 col-sm-offset-1">
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Descripcion </label>
				<div class="col-sm-9 col-xs-12">
					<textarea class="form-control input-sm" name="descripcion" id="descripcion"><?= $oInsumo->descripcion ?></textarea>
				</div>
			</div>
		</div> <!-- col -->
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oInsumo->insumo_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9">
				<button class="btn btn-primary btn-block btn-sm" id="btn_guardar" disabled="disabled"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar insumo 
				</button>
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-4 col-md-3  col-sm-offset-4 col-md-offset-6">
				<a href="<?= base_url('administracion/insumos') ?>" class="btn btn-inverse btn-block btn-sm "> 
					<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
					Cancelar edición 
				</a>
			</div>
			<div class="col-sm-4 col-md-3" > 
				<button class="btn btn-primary btn-block btn-sm"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Actualizar insumo 
				</button>
			</div>
		</div>
	<?php endif ?>
</form>


<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true}); 

	$("#form_insumo").submit(function() {
        $('#btn_guardar').attr('disabled', true);
    });
	$("#form_insumo").validate({
        rules: {
            insumo: "required",
            unidad: "required",
            stock: "required"
        },
        messages:{
            insumo: "Ingrese nombre del insumo",
            unidad: "Seleccione la unidad",
            stock: "Ingrese stock actual"

        }
    });
    $('#form_insumo').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#form_insumo').valid()) {                   // checks form for validity
            $('#btn_guardar').attr('disabled', false);        // enables button
        } else {
            $('#btn_guardar').attr('disabled', true);   // disables button
        }
    });
</script>
