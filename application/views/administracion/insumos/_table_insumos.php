
<table class="table table-bordered" width="100%" id="tabla-insumos">
	<thead>
        <tr>
            <th style="text-align: center"> Cód. </th>
            <th style="text-align: left"> Insumo </th>
            <!-- <th style="text-align: left"> Familia </th> -->
            <th style="text-align: center"> Precio costo </th>
            <th style="text-align: center"> Unidades </th>
            <th style="text-align: center"> Stock </th>
            <th style="text-align: center"> Stock Mín </th>
            <!-- <th style="text-align: center"> Cocina </th> -->
            <!-- <th style="text-align: center"> KIT </th> -->
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> Edit. </th>
            <th style="text-align: center"> Elim. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($insumos as $key => $insumo): ?>
            <tr>
                <td style="text-align: center"> <?= $insumo->insumo_id ?> </td>
                <td style="text-align: left"> <?= strtoupper($insumo->insumo) ?> </td>
                <td style="text-align: right;padding-right: 15px!important;"> <?= $insumo->precio_costo ?> </td>
                <td style="text-align: center;padding-right: 15px!important;"> <?= $insumo->unidad_desc ?> </td>
                <td style="text-align: center"> <?= $insumo->stock ?> </td>
                <td style="text-align: center"> <?= $insumo->stock_minimo ?> </td>
                <td style="text-align: center">
                    <?php 
                        if($insumo->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($insumo->estado == 'X') $est_desc = '<span class="label label-danger"> Inactivo </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('administracion/insumos/editar/').$insumo->insumo_id ?>
                    <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_eliminar = base_url('administracion/insumos/eliminar/').$insumo->insumo_id ?>
                    <?php $mensaje = "¿Está seguro de eliminar el insumo: ".strtoupper($insumo->insumo)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar insumo" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>


<script type="text/javascript">
	$('#tabla-insumos').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },     //Código
          { "targets": [ 1 ],"width": "20%", },   //insumo
          { "targets": [ 2 ],"width": "10%", },   //P Costo
          { "targets": [ 3 ],"width": "10%", },   //P und
          { "targets": [ 4 ],"width": "10%", },   //Stock
          { "targets": [ 5 ],"width": "10%", },   //Stock_minimo
          { "targets": [ 6 ],"width": "10%", },   //Estado
          { "targets": [ 7 ],"width": "10%", },   //Editar
          { "targets": [ 8 ],"width": "10%", },   //Eliminar
        ],
        "order": [[ 1, "asc" ]] ,
  	});
    $('#tabla-insumos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>