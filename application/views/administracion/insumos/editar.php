<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('administracion/insumos') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de insumos
            </a>
        </li>
        <li>
            <a href="<?= base_url('administracion/insumos/nuevo') ?>">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo insumo
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#editar_insumo">
            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                Editar insumo
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="editar_insumo" class="tab-pane active">
            <?php $this->load->view('administracion/insumos/_form_insumo'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <i class="ace-icon fa fa-info"></i>
            <strong>Recuerde:</strong> Para los insumos que irán a cocina es necesario indicar el KIT de preparación.
            <br>
            Por cada insumo en la comanda, que va a cocina, se verificará que haya insumos suficientes.
        </div>
    </div>
</div>