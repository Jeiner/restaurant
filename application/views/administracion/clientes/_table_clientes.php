
<table class="table table-bordered" width="100%" id="tabla-clientes">
	<thead>
        <tr>
            <th style="text-align: center"> Tipo </th>
            <th style="text-align: center"> DNI/RUC </th>
            <th style="text-align: left"> Nombre completo </th>
            <th style="text-align: center"> Teléfono </th>
            <th style="text-align: center"> Edit. </th>
            <th style="text-align: center"> Elim. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($clientes as $key => $cliente): ?>
            <tr>
                <td style="text-align: center">
                    <?= $cliente->tipo_documento_desc ?>
                </td>
                <td style="text-align: center"> <?= $cliente->documento ?> </td>
                <td style="text-align: left"> <?= $cliente->nombre_completo ?> </td>
               
                <td style="text-align: center" > <?= $cliente->telefono ?> </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('administracion/clientes/editar/').$cliente->cliente_id ?>
                     <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_eliminar = base_url('administracion/clientes/eliminar/').$cliente->cliente_id ?>
                    <?php $mensaje = "¿Está seguro de eliminar la cliente: ".strtoupper($cliente->nombre_completo)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar cliente" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-clientes').DataTable({
        "columnDefs": [
            { "targets": [ 0 ],"width": "10%", },   //Tipo
            { "targets": [ 1 ],"width": "10%", },   //DNI
            { "targets": [ 2 ],"width": "25%", },   //NOmbres
            { "targets": [ 3 ],"width": "15%", },   //Tele
            { "targets": [ 4 ],"width": "8%", },   //Editar
            { "targets": [ 5 ],"width": "7%", },   //Eliminar
        ],
        "order": [[ 2, "asc" ]] ,
  	});
    $('#tabla-clientes tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>