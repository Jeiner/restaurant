<!-- 
    Recibe:
    - oCliente
-->
<div class="widget-box widget-color-granate"> <!-- ui-sortable-handle -->
    <div class="widget-header">
        <h6 class="widget-title">
            <i class="ace-icon fa fa-user bigger-90" aria-hidden="true"></i>
            Datos del cliente
        </h6>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body" style="display: block;">
        <div class="widget-main padding-4" style="position: relative; padding: 15px">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Código de cliente</label>
                        <input type="text" name="" id="" class="form-control input-sm" value="<?= $oCliente->cliente_id ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Tipo documento</label>
                        <select class="form-control input-sm" id="comanda_cliente_tipo_documento" name="cliente_tipo_documento" style="width: 100%" disabled="disabled">
                            <option value="">Seleccionar...</option>
                            <?php foreach ($tipos_documento_identidad as $key => $tipoDoc): ?>
                                <?php 
                                    $tipoDoc_selec = "";
                                    if($tipoDoc->valor == $oCliente->sunat_tipo_documento_id) 
                                        $tipoDoc_selec = "selected";
                                ?>
                                <option value="<?= $tipoDoc->valor ?>" <?= $tipoDoc_selec ?>><?= $tipoDoc->descripcion ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Nro. Documento</label>
                        <input type="text" name="" id="comanda_cliente_documento" class="form-control input-sm" value="<?= $oCliente->documento ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label>Nombres completos</label>
                        <input type="text" name="" id="comanda_cliente_nombre" class="form-control input-sm" value="<?= $oCliente->nombre_completo ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" name="" id="comanda_cliente_telefono" class="form-control input-sm" value="<?= $oCliente->telefono ?>" readonly >
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Dirección</label>
                        <input type="text" name="" id="comanda_cliente_direccion" class="form-control input-sm" value="<?= $oCliente->direccion ?>" readonly >
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <label>Correo electrónico</label>
                        <input type="text" name="" id="comanda_cliente_correo" class="form-control input-sm" value="<?= $oCliente->correo ?>" readonly >
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-6">
                    <a class="btn btn-primary btn-sm btn-block" href="javascript:;" onclick="abrirModalEditarCliente();">
                        <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                        Editar datos del cliente
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="modal_edtar_datos_cliente" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> <strong> Editar datos del cliente </strong> </h3>
            </div>
            <div class="modal-body" >

                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                  <div class="form-group">
                                        <label>Tipo Documento</label>
                                        <select class="form-control input-sm" id="modal_cliente_tipo_documento" name="" style="width: 100%">
                                            <option value="">Seleccionar...</option>
                                            <?php foreach ($tipos_documento_identidad as $key => $tipoDoc): ?>
                                                <option value="<?= $tipoDoc->valor ?>" ><?= $tipoDoc->descripcion ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Documento</label>
                                        <input type="number" class="form-control input-sm" name="" id="modal_cliente_documento" value="" maxlength="11" placeholder="DNI o RUC" onkeypress="return soloNumeroEntero(event)">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-8 col-xs-12">
                                    <div class="form-group">
                                        <label >Cliente <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control input-sm" name="" id="modal_cliente_nombre" value="" placeholder="Nombre o Razón social">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="number" class="form-control input-sm" name="" id="modal_cliente_telefono" value="" maxlength="9" onkeypress="return soloNumeroEntero(event)">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" class="form-control input-sm" name="" id="modal_cliente_direccion" value="" >
                                    </div>
                                </div>
                                <div class="col-sm-8 col-xs-12">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="text" class="form-control input-sm" name="" id="modal_cliente_correo" value="" >
                                    </div>
                                </div>
                            </div>


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm" id="btn_unir_mesas" onclick="actualizarDatosCliente();">
                    <i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
                    Guardar
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    function abrirModalEditarCliente(){
        $('#modal_edtar_datos_cliente').modal({
            show: 'false'
        });

        let comanda_id = <?= $oComanda->comanda_id ?>;
        let cliente_tipo_documento = $("#comanda_cliente_tipo_documento").val();
        let cliente_documento = $("#comanda_cliente_documento").val();
        let cliente_nombre = $("#comanda_cliente_nombre").val();
        let cliente_direccion = $("#comanda_cliente_direccion").val();
        let cliente_telefono = $("#comanda_cliente_telefono").val();
        let cliente_correo = $("#comanda_cliente_correo").val();


        $("#modal_cliente_tipo_documento").val(cliente_tipo_documento);
        $("#modal_cliente_documento").val(cliente_documento);
        $("#modal_cliente_nombre").val(cliente_nombre);
        $("#modal_cliente_direccion").val(cliente_direccion);
        $("#modal_cliente_telefono").val(cliente_telefono);
        $("#modal_cliente_correo").val(cliente_correo);

    }

    function actualizarDatosCliente(){
        let comanda_id = <?= $oComanda->comanda_id ?>;
        let cliente_tipo_documento = $("#modal_cliente_tipo_documento").val();
        let cliente_documento = $("#modal_cliente_documento").val();
        let cliente_nombre = $("#modal_cliente_nombre").val();
        let cliente_direccion = $("#modal_cliente_direccion").val();
        let cliente_telefono = $("#modal_cliente_telefono").val();
        let cliente_correo = $("#modal_cliente_correo").val();

        if(comanda_id == 0 || comanda_id == ""){
            alert("Comanda no especificada.");
            return false;
        }
        if(cliente_nombre.trim() == ""){
            alert("Nombre no válido.");
            return false;
        }

        $.ajax({
            type: 'POST',
            url: "<?=base_url('salon/comanda/actualizar_comanda_client')?>",
            data: {'comanda_id': comanda_id, 'cliente_tipo_documento': cliente_tipo_documento, 'cliente_documento': cliente_documento, 'cliente_nombre': cliente_nombre,'cliente_nombre': cliente_nombre,'cliente_direccion': cliente_direccion,'cliente_telefono': cliente_telefono,'cliente_correo': cliente_correo },
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(objeto['success']){
                    alertify.success("Datos actualizados correctamente.");
                    $('#modal_edtar_datos_cliente').modal('hide');

                    setTimeout(function(){
                        location.reload();
                    },300);
                }else{
                    alertify.error(objeto['msg']);
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }


</script>