<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#clientes">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de clientes
            </a>
        </li>
        <li>
            <a  href="<?= base_url('administracion/clientes/nuevo') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo cliente
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="clientes" class="tab-pane in active">
            <?php $this->load->view('administracion/clientes/_table_clientes'); ?>
        </div>
    </div>
</div>