<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('administracion/clientes') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de clientes
            </a>
        </li>
        <li>
            <a href="<?= base_url('administracion/clientes/nuevo') ?>">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo cliente
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#editar_cliente">
            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                Editar cliente
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="editar_cliente" class="tab-pane active">
            <?php $this->load->view('administracion/clientes/_form_cliente'); ?>
        </div>
    </div>
</div>