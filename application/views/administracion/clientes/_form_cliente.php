<?php
	$editar = false;
	if($oCliente->cliente_id != 0){
		$editar = true;
	}
?>
<form method="Post" action="<?= base_url('administracion/clientes/guardar') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Cliente_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="cliente_id" id="cliente_id" class="form-control input-sm" value="<?= $oCliente->cliente_id ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Tipo documento <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<select class="form-control input-sm" name="sunat_tipo_documento_id" id="sunat_tipo_documento_id" onchange="cambiar_tipo_documento(this.value)" <?php if($editar) echo "disabled" ?> >  
                        <option value="">-- Seleccionar --</option>
                        <?php foreach ($documento_identidad as $key => $tipo_doc): ?>
                            <?php 
                                $selected = "";
                                if ($oCliente->sunat_tipo_documento_id == $tipo_doc->valor) {
                                    $selected = "selected";
                                }
                            ?>
                            <option value="<?= $tipo_doc->valor ?>" <?= $selected ?> > 
                                <?= $tipo_doc->descripcion ?>
                            </option>
                        <?php endforeach ?>
                    </select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Documento <label class="red">*</label></label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="documento" id="documento" class="form-control input-sm" value="<?= $oCliente->documento ?>" placeholder="N° Documento" maxlength="11" onkeypress="return soloNumeroEntero(event)" <?php if($editar) echo "disabled" ?> >
				</div>
				<div class="col-sm-4">
                    <div class="form-group" style="" id="btn_consultar">
                        <button type="button" class="btn btn-primary btn-xs btn-block" onclick="consultar_doc();" >
                            <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
                            Consultar
                        </button>
                    </div>
                    <div class="form-group" style="display: none" id="btn_editar" >
                        <button type="button" class="btn btn-primary btn-xs btn-block" onclick="limpiar_datos();" >
                            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                            Editar 
                        </button>
                    </div>
                </div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Cliente <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="nombre_completo" id="nombre_completo" class="form-control input-sm" value="<?= $oCliente->nombre_completo ?>" placeholder="Nombres o razón social">
				</div>
			</div>
			<div id="datos_persona_natural" <?php if($oCliente->sunat_tipo_documento_id == '6' ) echo "style='display:none'" ?>>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Ap.Paterno </label>
					<div class="col-sm-9 col-xs-12">
						<input type="text" name="ape_paterno" id="ape_paterno" class="form-control input-sm" value="<?= $oCliente->ape_paterno ?>" placeholder="Apellido paterno">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Ap. Materno </label>
					<div class="col-sm-9 col-xs-12">
						<input type="text" name="ape_materno" id="ape_materno" class="form-control input-sm" value="<?= $oCliente->ape_materno ?>" placeholder="Apellido materno">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Nombres </label>
					<div class="col-sm-9 col-xs-12">
						<input type="text" name="nombres" id="nombres" class="form-control input-sm" value="<?= $oCliente->nombres ?>" placeholder="Nombres">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Fecha Nac. </label>
					<div class="col-sm-4">
						<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control input-sm" value="<?= $oCliente->fecha_nacimiento ?>" onblur="cambiar_fecha_nac()">
					</div>
					<div class="col-sm-5">
						<input type="text" name="edad" id="edad" class="form-control input-sm"  readonly>
					</div>
				</div>
				<div class="form-group ">
						<label class="col-sm-3 col-xs-12 control-label" for=""> Sexo </label>
						<?php 
							if($oCliente->sexo == 'M'){
								$masculino = "checked";
								$femenino = "";
							}else{
								$masculino = "";
								$femenino = "checked";
							}
						?>
						<div class="col-sm-4 col-xs-5" align="center">
							<div class="radio">
								<label>
									<input name="sexo" value="M" type="radio" class="ace" <?= $masculino ?>  />
									<span class="lbl"> Masculino </span>
								</label>
							</div>
						</div>
						<div class="col-sm-4 col-xs-5" align="center">
							<div class="radio">
								<label>
									<input name="sexo" value="F" type="radio" class="ace" <?= $femenino ?> />
									<span class="lbl"> Femenino </span>
								</label>
							</div>
						</div>
				</div>
			</div>
		</div> <!-- col -->
		<div class="col-sm-5">
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Contacto </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="contacto" id="contacto" class="form-control input-sm" value="<?= $oCliente->contacto ?>" placeholder="nombres de algún contacto" maxlength="150">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Teléfono </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="telefono" id="telefono" class="form-control input-sm" value="<?= $oCliente->telefono ?>" placeholder="N° de celular o teéfono fijo" onkeypress="return soloTelefono(event)" maxlength="30">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Correo </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="correo" id="correo" class="form-control input-sm" value="<?= $oCliente->correo ?>" placeholder="Correo electrónico">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Dirección </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="direccion" id="direccion" class="form-control input-sm" value="<?= $oCliente->direccion ?>" placeholder="Dirección">
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oCliente->cliente_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9">
				<button class="btn btn-primary btn-block btn-sm"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar cliente 
				</button>
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-4 col-md-3 col-sm-offset-4 col-md-offset-6">
				<a href="<?= base_url('administracion/clientes') ?>" class="btn btn-inverse btn-block btn-sm "> 
					<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
					Cancelar edición 
				</a>
			</div>
			<div class="col-sm-4 col-md-3 " > 
				<button class="btn btn-primary btn-block btn-sm"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Actualizar cliente 
				</button>
			</div>
		</div>
	<?php endif ?>
</form>
<script type="text/javascript">
	function cambiar_fecha_nac(){
		var fecha = $('#fecha_nacimiento').val();
		var edad = calcularEdad(fecha);
		$('#edad').val(edad);
	}
	function cambiar_tipo_documento(tipo_documento_id){
		//Solo RUC hace referencia a empresa		
		if(tipo_documento_id == '6'){
			$('#datos_persona_natural').hide('slow');
			return;
		}
		$('#datos_persona_natural').show('slow');
	}
	function consultar_doc(){
        var tipo_documento_id = $('#sunat_tipo_documento_id').val();
        var documento = $('#documento').val();
        documento = documento.trim();
        if(tipo_documento_id == 1 && documento.length != 8){
        	alertify.error("Documento no válido.");
        	return false;
        }
        if(tipo_documento_id == 6 && documento.length != 11){
        	alertify.error("Documento no válido.");
        	return false;
        }
        if(documento.length < 5){
            alertify.error("Debe ingresar un número de documento válido.");
            return false;
        }
        var url = '<?= base_url('administracion/clientes/json_buscar_por_documento/')?>' + documento;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(objeto['success']){
                	if(objeto['data'].cliente_id != 0){
                		alertify.error("El cliente ya esta registrado.");
                		cerrarCargando();
                		return false;
                	}
                    var sunat_tipo_documento_id = objeto['data'].sunat_tipo_documento_id;
                    var cliente_id  = objeto['data'].cliente_id;
                    var documento   = objeto['data'].documento;
                    var cliente     = objeto['data'].cliente;
                    var nombres     = objeto['data'].nombres;
                    var ape_paterno = objeto['data'].ape_paterno;
                    var ape_materno = objeto['data'].ape_materno;
                    var telefono    = objeto['data'].telefono;
                    var correo      = objeto['data'].correo;
                    var direccion   = objeto['data'].direccion;
                    var ubigeo      = objeto['data'].ubigeo;
                    var direccion_ubigeo = (ubigeo == "") ? direccion : direccion + " - " + ubigeo;

                    $('#cliente_id').val(cliente_id);
                    $('#nombre_completo').val(cliente);
                    $('#ape_paterno').val(ape_paterno);
	                $('#ape_materno').val(ape_materno);
	                $('#nombres').val(nombres);
                    $('#telefono').val(telefono);
                    $('#direccion').val(direccion_ubigeo);

                    $('#btn_consultar').hide();
                    $('#btn_editar').show();
                    $('#documento').attr('readonly', true);
                    $('#nombre_completo').attr('readonly', true);
                }else{
                    alertify.error(objeto['msg']);
                }
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function limpiar_datos(){
    	// $('#documento').val("");
    	$('#nombres').val("");
    	$('#ape_paterno').val("");
    	$('#ape_materno').val("");
    	$('#nombre_completo').val("");
    	$('#telefono').val("");
    	$('#direccion').val("");
        $('#btn_editar').hide();

        $('#documento').attr('readonly', false);
        $('#nombres').attr('readonly', false);
        $('#ape_paterno').attr('readonly', false);
        $('#ape_materno').attr('readonly', false);
        $('#nombre_completo').attr('readonly', false);


        $('#btn_consultar').show();
        
    }
</script>
</script>