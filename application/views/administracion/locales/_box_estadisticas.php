
<div class="col-sm-4" id="" style="border: 1px solid #AF3A3E;">
    <div id="container_pie">
        
    </div>
</div>

<div class="col-sm-4" id="" style="border: 1px solid #AF3A3E;">
    <div id="container_pie_agrupa_modalidad">
        
    </div>
</div>

 <div class="col-sm-4" id="" style="border: 1px solid #AF3A3E;">
    <div id="container_ventas_diarias">
        
    </div>
</div>


<script type="text/javascript">
	// Datos iniciales
    var gb_fecha = new Date();  //Variables globales
    var gb_anioActual = gb_fecha.getFullYear();
    var gb_mesActual = gb_fecha.getMonth()+1;
    // Asignacion inicial de inputs
    $("#cbo_mes_vd").val(gb_mesActual);

    //Fecha
    let fecha_ini = $("#start_date").val();
    let fecha_fin = $("#end_date").val();

    // Funciones iniciales
    obtener_datos_para_pie();
    obtener_datos_para_pie_agrupa_modalidad();
    //crear_grafico_diario(gb_anioActual, gb_mesActual);

    function obtener_datos_para_pie(){
        $.ajax({
            type: 'POST',
            data: 'fecha_ini='+fecha_ini,
            url: '<?= base_url().'administracion/locales/ajax_obtener_array_para_pie_mensual' ?>',
            success: function(response){
                array_pie = JSON.parse(response);
                crear_grafico_pie(fecha_ini, array_pie);
            },
            error: function(response){
                // var msj = JSON.stringify(response);
                alert("Se generó un error al obtener los datos para grafico Pie mensual.");
            }
        }); 
    }

    function crear_grafico_pie(fecha_ini, array){
        let titulo = "Porcentaje de ventas diarias";
        let subtitulo = fecha_ini;

    	Highcharts.chart('container_pie', {
		    chart: {
		        type: 'pie'
		    },
		    title: {
		        text: titulo,
		        align: 'center'
		    },
		    subtitle: {
		        text: subtitulo,
		        align: 'center'
		    },

		    accessibility: {
		        announceNewData: {
		            enabled: true
		        },
		        point: {
		            valueSuffix: '%'
		        }
		    },

		    plotOptions: {
		        series: {
		            dataLabels: {
		                enabled: true,
		                format: '{point.name}: {point.y:.1f}%'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		    },

		    series: [
		        {
		            name: 'Locales',
		            colorByPoint: true,
		            data: array
		        }
		    ]
		});
              
    }

    function obtener_datos_para_pie_agrupa_modalidad(){
        $.ajax({
            type: 'POST',
            data: 'fecha_ini='+fecha_ini,
            url: '<?= base_url().'administracion/locales/ajax_obtener_array_para_pie_mensual_agrupa_modalidad' ?>',
            success: function(response){
                array_pie = JSON.parse(response);
                crear_grafico_pie_agrupa_modalidad(fecha_ini, array_pie);
            },
            error: function(response){
                // var msj = JSON.stringify(response);
                alert("Se generó un error al obtener los datos para grafico Pie mensual.");
            }
        }); 
    }

    function crear_grafico_pie_agrupa_modalidad(fecha_ini, array){
        let titulo = "Modalidad de venta por día";
        let subtitulo = fecha_ini;

        Highcharts.chart('container_pie_agrupa_modalidad', {
            chart: {
                type: 'pie'
            },
            title: {
                text: titulo,
                align: 'center'
            },
            subtitle: {
                text: subtitulo,
                align: 'center'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.1f}%'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            series: [
                {
                    name: 'Modalidad',
                    colorByPoint: true,
                    data: array
                }
            ]
        });
    }


    //Otros


	function cant_ds(mes,ano){ 
        di=28;
        f = new Date(ano,mes-1,di); 
        while(f.getMonth() == mes-1){ 
            di++;
            f = new Date(ano,mes-1,di); 
        } 
        return di-1; 
    } 

    function crear_grafico_diario(anio = '', mes){
        var array_meses = ['Enero','Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio', 'Agosto', 'Setiembre','Octubre', 'Noviembre', 'Diciembre'];
        // var fecha = new Date();
        var anioActual = (anio == '' || anio == null) ? gb_anioActual : anio;
        var mesActual = mes;
        
        var cant_dias = cant_ds(mesActual,anioActual);
        var array_dias = new Array(cant_dias);
        // var array_nombre_dias = new Array(cant_dias);
        var array_ventas = new Array(cant_dias);
        var array_productos = new Array(cant_dias);

        for (var i = 0; i < cant_dias; i++) {
            array_dias[i] = i+1;
        }
        for (var i = 0; i < cant_dias; i++) {
            array_ventas[i] = 0;
        }
        for (var i = 0; i < cant_dias; i++) {
            array_productos[i] = 0;
        }
        $.ajax({
            type: 'POST',
            data: 'mes='+mesActual+"&anio="+anioActual,
            url: '<?= base_url().'nube/reportes/nube_ventas_diarias' ?>',
            success: function(response){
                array_count = JSON.parse(response);
                array_count.forEach(function(o){
                   array_ventas[o.dia-1] = parseFloat(o.importe);
                   array_productos[o.dia-1] = parseFloat(o.productos);
                });
                dibujar_grafico_diario(array_meses[mesActual-1],anioActual,array_dias,array_ventas,array_productos);
            },
            error: function(response){
                var msj = JSON.stringify(response);
                alert("Error en la operación.\n\n"+msj);
            }
        }); 
    }


    function dibujar_grafico_diario(mes,anioActual,array_dias,array_ventas,array_productos){
        Highcharts.chart('container_ventas_diarias', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Ventas diarias. '+mes + ", " +anioActual
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: array_dias,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"> <b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series:[{
                name: 'Monto de ventas (S/.)',
                data: array_ventas,

            },{
                name: 'N° de productos (Unidades)',
                data: array_productos,
                visible:false
            }]
        }); 
    }
</script>