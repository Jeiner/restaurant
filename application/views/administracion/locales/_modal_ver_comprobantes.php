
<?php  
    $days_dias = array(
    'Monday'=>'LUNES',
    'Tuesday'=>'MARTES',
    'Wednesday'=>'MIERCOLES',
    'Thursday'=>'JUEVES',
    'Friday'=>'VIERNES',
    'Saturday'=>'SABADO',
    'Sunday'=>'DOMINGO'
    );
?>
<div id="modal_ver_comprobantes" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong> 
                        <span> <?= $days_dias[date('l', strtotime($fecha_ini))]; ?>  </span>
                        <?= date('d-m-Y', strtotime($fecha_ini))  ?>
                    </strong> 
                </h3>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" width="100%" id="tabla_comandas_">
                    <thead>
                        <tr>
                            <th class="text-center"><strong>Comanda</strong></th>
                            <th class="text-center"><strong>Fecha emisión</strong></th>
                            <th class="text-center"><strong>Tipo</strong></th>
                            <th class="text-center"><strong>Serie / Correlativo</strong></th>
                            <th class="text-center"><strong>Importe</strong></th>
                            <th class="text-center"><strong>Cliente</strong></th>
                            <th class="text-center"><strong>Estado</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($lstComprobantes as $key => $oComprobantes): ?>
                            <tr>
                                <td class="text-center">
                                    <?= $oComprobantes->comanda_id ?>
                                </td>
                                <td class="text-center">
                                    <?= $oComprobantes->fecEmision ?>
                                </td>
                                <td class="text-center">
                                    <?php if ($oComprobantes->tipoComprobante == "03"): ?>
                                        BOL
                                    <?php endif ?>
                                    <?php if ($oComprobantes->tipoComprobante == "01"): ?>
                                        FAC
                                    <?php endif ?>
                                </td>
                                <td class="text-right">
                                    <?= $oComprobantes->serie ?> <br>
                                    <?= $oComprobantes->correlativo ?>
                                </td>
                                <td class="text-center">
                                    <?= $oComprobantes->sumImpVenta ?>
                                </td>
                                <td class="text-center">
                                    <?= $oComprobantes->rznSocialUsuario ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    //Abrir modal
    $('#modal_ver_comprobantes').modal({
        show: 'false'
    });
</script>

<script type="text/javascript">

    $('#tabla_comandas').DataTable({
        "orderable": false,
        "columnDefs": [
          { "targets": [ 0 ],"width": "20%", },   //Producto
          { "targets": [ 1 ],"width": "20%", },   //Familia
          { "targets": [ 2 ],"width": "20%", },   //P unit
          { "targets": [ 3 ],"width": "20%", },   //Stock
          { "targets": [ 4 ],"width": "20%", },   //Stock
        ],
        "order": [0]
    });

    $('#tabla_comandas tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });

    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }


</script>

