<style type="text/css">
	.infobox-santorini{
		width: 100%!important;
		border: none;
		text-align: center;
	}

	.infobox-yellow>.infobox-icon>.ace-icon{
		background-color: #e59729;
	}

	.infobox-blue-sky>.infobox-icon>.ace-icon{
		background-color: #22ABBC;
	}

	.row_select{
		background-color: #f3f1f1;
	}

    .badge-error{
    	padding-top: 3px!important;
    	padding-left: 4px ;
    	padding-right: 4px ;
        background-color: red;
        margin-left: 5px;
        margin-top: -5px;
    }
	
	table{
		margin-bottom: 10px;
	}

	table tbody tr{
		border-top: 1px solid #ccc!important;
		border-bottom: 1px solid #ccc!important;
	}
	table tbody tr td{
		padding-top: 10px!important;
		padding-bottom: 10px!important;
	}
	table tbody tr td a{
		color: #000;
		/*text-decoration: underline;*/
	}

	.light-green{
		color:  #3bef06!important
	}

	.light-red{
		color:  #f00!important
	}
</style>

	<?php foreach ($lstSucursales as $key => $oSucursal): ?>
		<div class="col-sm-4 col-xs-12">
			<div class="widget-box widget-color-granate">
				<div class="widget-header widget-header-flat widget-header-small" style="padding-top: 2px; padding-bottom: 2px; font-size: 14px">
					 <h5 class="widget-title" style="text-transform: uppercase;">
						<i class="ace-icon fa fa-signal"></i>
						 <?= $oSucursal->empresa_nombre ?> :: <?= $oSucursal->sucursal_nombre ?>
					</h5>

					<div class="widget-toolbar no-border">
						<div class="inline dropdown-hover">
							<?php if ($oSucursal->caja_open): ?>
								<i class="ace-icon fa fa-circle light-green"></i>
							<?php else: ?>
								<i class="ace-icon fa fa-circle light-red"></i>
							<?php endif ?>
						</div>
					</div>
				</div>
				<div class="widget-body" >
					<div class="widget-main">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div style="text-align: right;">
									<?php if ($oSucursal->caja_open): ?>
										<span class="text-muted">
											<?= date('d-m-Y H:i:s', strtotime($oSucursal->caja_aperturado_en))  ?>
										</span>
									<?php endif ?>
								</div>
								<table class="" width="100%" id="tabla_box">
									<tbody>
										<tr onclick="ver_comandas('<?= $oSucursal->empresa_codigo ?>', '<?= $oSucursal->sucursal_codigo ?>')">
											<td class="text-left" width="50%">
												<strong>
													<a href="javascript:;">
														<span>Comandas</span>
														<?php if ($oSucursal->comandas_anuladas != null && $oSucursal->comandas_anuladas > 0): ?>
															<span class="badge badge-error">
			                                                    <?= $oSucursal->comandas_anuladas ?>
			                                                </span>
														<?php endif ?>
														<?php if ($oSucursal->comandas_items_anuladas != null && $oSucursal->comandas_items_anuladas > 0): ?>
															<span class="badge badge-error">
			                                                    <?= $oSucursal->comandas_items_anuladas ?>
			                                                </span>
														<?php endif ?>
													</a>
												</strong>
											</td>
											<td class="text-center" width="20%">
												<strong><span><?= $oSucursal->comandas_resumen->cantidad ?></span></strong>
											</td>
											<td class="text-right" width="30%">
												<strong><?= $oSucursal->comandas_resumen->importe_total ?></strong>
											</td>
										</tr>
										<tr onclick="ver_pagos('<?= $oSucursal->empresa_codigo ?>', '<?= $oSucursal->sucursal_codigo ?>')">
											<td class="text-left" width="50%">
												<strong>
													<a href="javascript:;">Pagos</a>
												</strong>
											</td>
											<td class="text-center" width="20%">
												<strong><span><?= $oSucursal->pagos_resumen->cantidad ?></span></strong>
											</td>
											<td class="text-right" width="30%">
												<strong><span><?= $oSucursal->pagos_resumen->importe_total ?></span></strong>
											</td>
										</tr>
										<tr onclick="ver_comprobantes('<?= $oSucursal->empresa_codigo ?>', '<?= $oSucursal->sucursal_codigo ?>')">
											<td class="text-left" width="50%">
												<strong>
													<a href="javascript:;">Comprobantes</a>
												</strong>
											</td>
											<td class="text-center" width="20%">
												<strong><?= $oSucursal->comprobantes_resumen->cantidad ?></strong>
											</td>
											<td class="text-right" width="30%">
												<strong><?= $oSucursal->comprobantes_resumen->importe_total ?></strong>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-12">
										<a href="<?= $oSucursal->ip_local ?>" target="_blank" class="btn btn-sm btn-primary">Ir a Local</a>
										<a href="<?= base_url("administracion/nube/asistencia") ?>" class="btn btn-sm btn-primary">Ver asistencia</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach ?>
	
<div id="div_modal_comandas">
	
</div>

<div id="div_modal_ver_pagos">
	
</div>

<div id="div_modal_ver_comprobantes">
	
</div>

<script type="text/javascript">
	function ver_comandas(empresa_codigo, sucursal_codigo){
		abrirCargandoFull();
		let fecha_ini = $("#start_date").val();
		let fecha_fin = $("#end_date").val();

        // Mensaje de cargando
        $('#div_modal_comandas').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/locales/ajax_modal_ver_comandas')?>",
            data: { "fecha_ini": fecha_ini, "fecha_fin" : fecha_fin, "empresa_codigo": empresa_codigo, "sucursal_codigo": sucursal_codigo },
            success: function(rpta){
                $('#div_modal_comandas').html(rpta);
                cerrarCargandoFull();
            },
            error: function(rpta){
            	cerrarCargandoFull();
                alert("Error en la operación");
            }
        });
	}


	function ver_pagos(empresa_codigo, sucursal_codigo){
		abrirCargandoFull();
		let fecha_ini = $("#start_date").val();
		let fecha_fin = $("#end_date").val();

        // Mensaje de cargando
        $('#div_modal_ver_pagos').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/locales/ajax_modal_ver_pagos')?>",
            data: { "fecha_ini": fecha_ini, "fecha_fin" : fecha_fin, "empresa_codigo": empresa_codigo, "sucursal_codigo": sucursal_codigo },
            success: function(rpta){
                $('#div_modal_ver_pagos').html(rpta);
                cerrarCargandoFull();
            },
            error: function(rpta){
                alert("Error en la operación");
                cerrarCargandoFull();
            }
        });
	}

	function ver_comprobantes(empresa_codigo, sucursal_codigo){
		abrirCargandoFull();
		let fecha_ini = $("#start_date").val();
		let fecha_fin = $("#end_date").val();

        // Mensaje de cargando
        $('#div_modal_ver_comprobantes').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/locales/ajax_modal_ver_comprobantes')?>",
            data: { "fecha_ini": fecha_ini, "fecha_fin" : fecha_fin, "empresa_codigo": empresa_codigo, "sucursal_codigo": sucursal_codigo },
            success: function(rpta){
                $('#div_modal_ver_comprobantes').html(rpta);
                cerrarCargandoFull();
            },
            error: function(rpta){
                alert("Error en la operación");
                cerrarCargandoFull();
            }
        });
	}

	$('#tabla_box tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFilaBox(this);
        }
    });

    function seleccionarFilaBox(fila) {
        $(".row_select").removeClass("row_select");
        $(fila).addClass("row_select");
    }
</script>