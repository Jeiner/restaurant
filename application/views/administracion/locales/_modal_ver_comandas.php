
<?php  
    $days_dias = array(
    'Monday'=>'LUNES',
    'Tuesday'=>'MARTES',
    'Wednesday'=>'MIERCOLES',
    'Thursday'=>'JUEVES',
    'Friday'=>'VIERNES',
    'Saturday'=>'SABADO',
    'Sunday'=>'DOMINGO'
    );
?>

<style type="text/css">
    .widget-color-blanco{
        margin-bottom: 10px;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 0.25rem;
        border-top: 3px solid #913032;
        box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%);
    }

    .widget-color-blanco>.widget-header{
        background: #FFF!important;
        padding: 6px;
        height: 75px;
    }

    .widget-color-blanco .widget-header .widget-title{
        color: #000;
        float: left;
    }

    .widget-color-blanco .widget-header>.widget-title{
        line-height: inherit;
    }

    .widget-color-blanco>.widget-header>.widget-toolbar>[data-action=collapse] {
        color: #8b8b8b;
    }

    .widget-color-blanco>.widget-header>.widget-toolbar{
        line-height: inherit;
    }

    .badge-error{
        background-color: red;
        margin-left: 5px;
    }

    /*tabs detalle comanda*/
    .bg-gray{
        background-color: #ebebeb !important;
        height: 35px;
        line-height: 35px;
        text-align: center;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        border-top: 3px solid #AF3A3E!important;
        border-color: #f7f7f7 #f7f7f7 transparent;
        color: #AF3A3E;
    }
    .nav-tabs>li>a, .nav-tabs>li>a:focus{
        color: #000;
    }
</style>
<div id="modal_ver_comandas" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong> 
                        <span> <?= $days_dias[date('l', strtotime($fecha_ini))]; ?>  </span>
                        <?= date('d-m-Y', strtotime($fecha_ini))  ?>
                    </strong> 
                </h3>
            </div>
            <div class="modal-body">

                <?php foreach ($lstComandas as $key => $oComanda): ?>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            
                            <div class="widget-box collapsed widget-color-blanco">
                                <div class="widget-header">
                                    <h6 class="widget-title">
                                        <span class="text-muted">Comanda:</span> <strong>
                                            <?= $oComanda->comanda_id ?>
                                            <?php if ($oComanda->detalle_anulados > 0): ?>
                                                <span class="badge badge-error"> 
                                                    <?= $oComanda->detalle_anulados ?>
                                                </span>
                                            <?php endif ?>
                                        </strong><br>
                                        <span class="text-muted">Fecha:</span> <strong><?= $oComanda->fecha_comanda ?></strong><br>
                                        <span class="text-muted">Total:</span> <strong> S/.<?= $oComanda->importe_total ?></strong><br>
                                        <span class="text-muted">Modalidad:</span> <strong>
                                            <?php if ($oComanda->modalidad == "PRE"): ?>
                                                <?= $oComanda->mesa ?> 
                                            <?php else: ?>
                                                <?= $oComanda->modalidad ?>
                                           <?php endif ?>
                                        </strong><br>
                                    </h6>
                                    <div class="widget-toolbar">
                                        <span style="width: 100px">
                                             <?php if ($oComanda->estado_atencion == 'E'): ?>
                                                <span style="width: 70px" class="label label-warning"> En espera </span>
                                            <?php endif ?>
                                            <?php if ($oComanda->estado_atencion == 'A'): ?>
                                                <span style="width: 70px" class="label label-primary"> Atendido </span>
                                            <?php endif ?>
                                            <?php if ($oComanda->estado_atencion == 'F'): ?>
                                                <span style="width: 70px" class="label label-success"> Finalizado </span>
                                            <?php endif ?>
                                            <?php if ($oComanda->estado_atencion == 'X'): ?>
                                                <span style="width: 70px" class="label label-inverse"> Anulado </span>
                                            <?php endif ?> 
                                            <br>
                                            <?php if ($oComanda->estado_pago == 'P'): ?>
                                                <span style="width: 70px" class="label label-danger"> Pendiente </span>
                                            <?php endif ?>
                                            <?php if ($oComanda->estado_pago == 'C'): ?>
                                                <span style="width: 70px" class="label label-success"> Pagado </span>
                                            <?php endif ?>
                                            <?php if ($oComanda->estado_pago == 'X'): ?>
                                                <span style="width: 70px" class="label label-inverse"> Anulado </span>
                                            <?php endif ?>
                                        </span>
                                        <a href="#" data-action="collapse">
                                            <i class="ace-icon fa fa-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body" style="display: none">
                                    <div class="widget-main padding-4">
                                        <div class="row">

                                            <div class="col-md-12 col-xs-12">
                                                <div class="nav-tabs-custom">
                                                    <?php
                                                        $nom_tab_1 = "tab_".$oComanda->comanda_codigo."_1";
                                                        $nom_tab_2 = "tab_".$oComanda->comanda_codigo."_2";
                                                        $nom_tab_3 = "tab_".$oComanda->comanda_codigo."_3";
                                                        $nom_tab_4 = "tab_".$oComanda->comanda_codigo."_4";
                                                    ?>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a href="#<?= $nom_tab_1 ?>" data-toggle="tab" aria-expanded="true">Resumen</a></li>
                                                        <li class=""><a href="#<?= $nom_tab_2 ?>" data-toggle="tab" aria-expanded="false">Pagos</a></li>
                                                        <li class=""><a href="#<?= $nom_tab_3 ?>" data-toggle="tab" aria-expanded="false">Cliente</a></li>
                                                        <li class=""><a href="#<?= $nom_tab_4 ?>" data-toggle="tab" aria-expanded="false">Comprobantes</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="<?= $nom_tab_1 ?>">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <table width="100%" style="border: 0px!important">
                                                                        <tbody>
                                                                            <tr style="border: 0px!important">
                                                                                <td style="padding: 0px!important;">Código: </td>
                                                                                <td style="padding: 0px!important;" class="text-right"><?= $oComanda->comanda_codigo ?></td>
                                                                            </tr>
                                                                            <tr style="border: 0px!important">
                                                                                <td style="padding: 0px!important;">Registrado por: </td>
                                                                                <td style="padding: 0px!important;" class="text-right"><?= $oComanda->registrado_por ?></td>
                                                                            </tr>
                                                                            <tr style="border: 0px!important">
                                                                                <td style="padding: 0px!important;">Fecha registro: </td>
                                                                                <td style="padding: 0px!important;" class="text-right"><?= $oComanda->fecha_registro ?></td>
                                                                            </tr>
                                                                            <tr style="border: 0px!important">
                                                                                <td style="padding: 0px!important;">Fecha Finalizado: </td>
                                                                                <td style="padding: 0px!important;" class="text-right"><?= $oComanda->finalizada_en ?></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="bg-gray disabled color-palette"><span><strong>Productos</strong></span></div>

                                                            <table class="" width="100%">
                                                                <tbody>
                                                                   <?php foreach ($oComanda->detalle as $key => $oDetalle): ?>
                                                                        <tr>
                                                                            <td class="text-left" width="65%">
                                                                                <strong>
                                                                                    <span><?= $oDetalle->producto ?></span>
                                                                                </strong><br>
                                                                                <?php if ($oDetalle->comentario != null && $oDetalle->comentario != ""): ?>
                                                                                    <span class="text-muted">
                                                                                        <?= $oDetalle->comentario ?>
                                                                                    </span><br>
                                                                                <?php endif ?>
                                                                                    
                                                                                <span class="text-muted" style="font-size: 12px">
                                                                                    <span><?= $oDetalle->producto_id ?></span> | 
                                                                                    <span>PU: <?= $oDetalle->precio_unitario ?></span>
                                                                                </span><br>
                                                                                <?php if ($oDetalle->estado_atencion == "X"): ?>
                                                                                    <span><span class="label label-danger"> Anulado </span></span>
                                                                                <?php endif ?>
                                                                            </td>
                                                                            <td class="text-center" width="15%">
                                                                                <?= $oDetalle->cantidad ?>
                                                                            </td>
                                                                            <td class="text-right" width="20%">
                                                                                <?= $oDetalle->importe ?>
                                                                            </td>
                                                                        </tr>
                                                                   <?php endforeach ?>
                                                                   <tr>
                                                                       <td width="65%" class="text-right"><strong>TOTALES</strong></td>
                                                                       <td width="15%" class="text-right"></td>
                                                                       <td width="25%" class="text-right"><strong><?= $oComanda->importe_total ?></strong></td>
                                                                   </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="tab-pane " id="<?= $nom_tab_2 ?>">
                                                            The European languages are members of the same family. Their separate existence is a myth.
                                                            For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                                                            in their grammar, their pronunciation and their most common words. Everyone realizes why a
                                                            new common language would be desirable: one could refuse to pay expensive translators. To
                                                            achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                                                            words. If several languages coalesce, the grammar of the resulting language is more simple
                                                            and regular than that of the individual languages.
                                                        </div>

                                                        <div class="tab-pane" id="<?= $nom_tab_3 ?>">
                                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                            when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                                            It has survived not only five centuries, but also the leap into electronic typesetting,
                                                            remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                                                            sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
                                                            like Aldus PageMaker including versions of Lorem Ipsum.
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                    </div>
                <?php endforeach ?>
                    


            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    //Abrir modal
    $('#modal_ver_comandas').modal({
        show: 'false'
    });
</script>


