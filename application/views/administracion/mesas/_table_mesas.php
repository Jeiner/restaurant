
<table class="table table-bordered" width="100%" id="tabla-mesas">
	<thead>
        <tr>
            <th style="text-align: center"> Cód. </th>
            <th style="text-align: left"> Mesa </th>
            <th style="text-align: center"> Capac. </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> Edit. </th>
            <th style="text-align: center"> Elim. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($mesas as $key => $mesa): ?>
            <tr>
                <td style="text-align: center"> <?= $mesa->mesa_id ?> </td>
                <td style="text-align: center"> <?= $mesa->mesa ?> </td>
                <td style="text-align: center"> <?= $mesa->capacidad ?> </td>
                <td style="text-align: center">
                    <?php 
                        if($mesa->estado == 'L') $est_desc = '<span class="label label-success">Libre</span>';
                        if($mesa->estado == 'O') $est_desc = '<span class="label label-danger">Ocupado</span>';
                        if($mesa->estado == 'A') $est_desc = '<span class="label label-primary">Atendida</span>';
                        if($mesa->estado == 'P') $est_desc = '<span class="label label-warning">Por salir</span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('administracion/mesas/editar/').$mesa->mesa_id ?>
                     <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_eliminar = base_url('administracion/mesas/eliminar/').$mesa->mesa_id ?>
                    <?php $mensaje = "¿Está seguro de eliminar la mesa: ".strtoupper($mesa->mesa)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar mesa" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-mesas').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },     //Codigo
          { "targets": [ 1 ],"width": "35%", },   //Mesa
          { "targets": [ 2 ],"width": "15%", },   //Capacidad
          { "targets": [ 3 ],"width": "20%", },   //Estado
          { "targets": [ 4 ],"width": "10%", },   //Editar
          { "targets": [ 5 ],"width": "10%", },   //Eliminar
        ],
        "order": [[ 1, "asc" ]] ,
  	});
    $('#tabla-mesas tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>