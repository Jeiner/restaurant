<div class="row">
	<div class="col-sm-5">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <?php if ($oMesa->mesa_id == 0): ?>
                        <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                        Nueva mesa
                    <?php else: ?>
                        <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                        Editar mesa
                    <?php endif ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $this->load->view('administracion/mesas/_form_mesa'); ?>
            </div>
        </div>
	</div>
	<div class="col-sm-7">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                    Listado de mesas
                </div>
            </div>
            <div class="panel-body">
                <?php $this->load->view('administracion/mesas/_table_mesas'); ?>
            </div>
        </div>
	</div>
</div>