
<form method="Post" action="<?= base_url('administracion/mesas/guardar') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Mesa_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm" value="<?= $oMesa->mesa_id ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Mesa <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="mesa" id="mesa" class="form-control input-sm" value="<?= $oMesa->mesa ?>" placeholder="Nombre de la mesa">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Capacidad <label class="red">*</label> </label>
				<div class="col-sm-4 col-xs-6">
					<input type="text" name="capacidad" id="capacidad" class="form-control input-sm" value="<?= $oMesa->capacidad ?>" placeholder="Nro de comensales">
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oMesa->mesa_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-7 col-md-6 col-sm-offset-5 col-md-offset-6">
				<button class="btn btn-primary btn-block btn-sm">
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar mesa 
				</button>	
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-6 col-md-6">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="<?= base_url('administracion/mesas') ?>" class="btn btn-inverse btn-block btn-sm "> 
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Cancelar edición 
						</a>
					</div>		
				</div>
			</div>
			<div class="col-sm-6 col-md-6" > 
				<div class="form-group">
					<div class="col-xs-12" > 
						<button class="btn btn-primary btn-block btn-sm">
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Actualizar mesa 
						</button>
					</div>	
				</div>
			</div>
		</div>
	<?php endif ?>
	
</form>
