<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#lista_proveedores">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de proveedores
            </a>
        </li>
        <li>
            <a  href="<?= base_url('administracion/proveedores/nuevo') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo proveedor
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="lista_proveedores" class="tab-pane in active">
            <?php $this->load->view('administracion/proveedores/_table_proveedores'); ?>
        </div>
    </div>
</div>