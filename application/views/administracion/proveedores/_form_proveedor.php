<form method="Post" action="<?= base_url('administracion/proveedores/guardar') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Proveedor_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="proveedor_id" id="proveedor_id" class="form-control input-sm" value="<?= $oProveedor->proveedor_id ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> RUC </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="RUC" id="RUC" class="form-control input-sm" value="<?= $oProveedor->RUC ?>" placeholder="RUC del proveedor" maxlength="11" onkeypress="return soloNumeroEntero(event);">
				</div>
				<?php if ($oProveedor->proveedor_id == 0): ?>
					<div class="col-sm-4">
	                    <div class="form-group" style="" id="btn_consultar">
	                        <button type="button" class="btn btn-primary btn-xs btn-block" onclick="consultar_doc();" >
	                            <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
	                            Consultar
	                        </button>
	                    </div>
	                    <div class="form-group" style="display: none" id="btn_editar" >
	                        <button type="button" class="btn btn-primary btn-xs btn-block" onclick="limpiar_datos();" >
	                            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
	                            Editar 
	                        </button>
	                    </div>
	                </div>
				<?php endif ?>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Razón social </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="razon_social" id="razon_social" class="form-control input-sm" value="<?= $oProveedor->razon_social ?>" placeholder="Razón social del proveedor">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Dirección </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="direccion" id="direccion" class="form-control input-sm" value="<?= $oProveedor->direccion ?>" placeholder="Dirección del proveedor">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Contacto </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="contacto" id="contacto" class="form-control input-sm" value="<?= $oProveedor->contacto ?>" placeholder="Nombres y apellidos del contacto">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Teléfono </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="telefono" id="telefono" class="form-control input-sm" value="<?= $oProveedor->telefono ?>" placeholder="Teléfono del proveedor" onkeypress="return soloTelefono(event)" maxlength="30">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Correo </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="correo" id="correo" class="form-control input-sm" value="<?= $oProveedor->correo ?>" placeholder="Correo del proveedor">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Productos y/o servicios  </label>
				<div class="col-sm-9 col-xs-12">
					<textarea class="form-control input-sm" name="productos" id="productos"><?= $oProveedor->productos ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Estado  </label>
				<div class="col-sm-5 col-xs-12">
					<select class="form-control input-sm" id="estado" name="estado">
						<option> Seleccionar... </option>
						<option value="A" <?php if($oProveedor->estado == 'A') echo "selected" ?> > Activo </option>
						<option value="X" <?php if($oProveedor->estado == 'X') echo "selected" ?>> Inactivo </option>
					</select>
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oProveedor->proveedor_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-4 col-md-3 col-sm-offset-2 col-md-offset-3">
				<button class="btn btn-primary btn-block btn-sm"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar proveedor 
				</button>	
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-4 col-md-3">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="<?= base_url('administracion/proveedores') ?>" class="btn btn-inverse btn-block btn-sm "> 
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Cancelar edición 
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-3">
				<div class="form-group">
					<div class="col-xs-12">
						<button class="btn btn-primary btn-block btn-sm"> 
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Actualizar proveedor 
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
</form>


<script type="text/javascript">
	function consultar_doc(){
        var RUC = $('#RUC').val();
        if(RUC.length < 11){
        	alertify.error("RUC no válido");
        	return false;
        }
        url = '<?= base_url('pide/pide/sunat/')?>' + RUC;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);

            	if(!objeto.success){
                    alertify.error(objeto.error);
                    limpiar_datos();
                    cerrarCargando();
                    return false;
                }else{
                	nombre_completo = objeto.data.RazonSocial;
                    telefono = objeto.data.Telefono;
                    direccion = objeto.data.Direccion;
	                $('#razon_social').val(nombre_completo);
	                $('#telefono').val(telefono);
	                $('#direccion').val(direccion);

	                $('#btn_consultar').hide();
		        	$('#btn_editar').show();
                }
              	$('#RUC').attr('readonly', true);
              	$('#razon_social').attr('readonly', true);
		        // $('#direccion').attr('readonly', true);
		        cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function limpiar_datos(){
    	$('#razon_social').val("");
    	$('#telefono').val("");
    	$('#direccion').val("");

        $('#razon_social').attr('readonly', false);
        $('#documento').attr('readonly', false);


        $('#btn_consultar').show();
        $('#btn_editar').hide();
    }
</script>