
<table class="table table-bordered" width="100%" id="tabla-proveedores">
	<thead>
        <tr>
            <th style="text-align: center"> Cód. </th>
            <th style="text-align: center"> RUC </th>
            <th style="text-align: left"> Razón social </th>
            <th style="text-align: left"> Contacto </th>
            <th style="text-align: center"> Teléfono </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> Edit. </th>
            <th style="text-align: center"> Elim. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($proveedores as $key => $proveedor): ?>
            <tr>
                <td style="text-align: center"> <?= $proveedor->proveedor_id ?> </td>
                <td style="text-align: center"> <?= $proveedor->RUC ?> </td>
                <td style="text-align: left"> <?= strtoupper($proveedor->razon_social) ?> </td>
                <td style="text-align: left"> <?= $proveedor->contacto ?> </td>
                <td style="text-align: center"> <?= $proveedor->telefono ?> </td>
                <td style="text-align: center">
                    <?php 
                        if($proveedor->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($proveedor->estado == 'X') $est_desc = '<span class="label label-danger"> Inactivo </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('administracion/proveedores/editar/').$proveedor->proveedor_id ?>
                     <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_eliminar = base_url('administracion/proveedores/eliminar/').$proveedor->proveedor_id ?>
                    <?php $mensaje = "¿Está seguro de eliminar la proveedor: ".strtoupper($proveedor->razon_social)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar proveedor" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-proveedores').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },     //Codigo
          { "targets": [ 1 ],"width": "15%", },   //RUC
          { "targets": [ 2 ],"width": "25%", },   //Razon social
          { "targets": [ 3 ],"width": "15%", },   //Contacto
          { "targets": [ 4 ],"width": "10%", },   //Telefono
          { "targets": [ 5 ],"width": "10%", },   //Estado
          { "targets": [ 6 ],"width": "10%", },   //Editar
          { "targets": [ 7 ],"width": "10%", },   //Eliminar
        ],
        "order": [[ 2, "asc" ]] ,
  	});
    $('#tabla-proveedores tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>