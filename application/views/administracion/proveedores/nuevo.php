<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('administracion/proveedores') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de proveedores
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#nuevo_proveedor">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo proveedor
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="nuevo_proveedor" class="tab-pane active">
            <?php $this->load->view('administracion/proveedores/_form_proveedor'); ?>
        </div>
    </div>
</div>