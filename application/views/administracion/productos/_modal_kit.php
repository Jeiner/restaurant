<div id="modal_kit" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> <strong> Kit de preparación </strong> </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Insumo</th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($insumos as $key => $insumo): ?>
                                    <tr>
                                        <td><?= $insumo->insumo ?></td>
                                        <td><?= $insumo->cantidad ?></td>
                                        <td><?= $insumo->unidad_desc ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div> <!-- row -->
            </div>
            <div class="modal-footer">
                <a href="<?= base_url('administracion/productos/editar/').$producto_id ?>" class="btn btn-primary btn-sm">
                    Editar
                </a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>