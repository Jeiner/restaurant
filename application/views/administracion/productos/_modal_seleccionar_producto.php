<div id="modal_seleccionar_producto" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> <strong> Seleccionar producto adicional </strong> </h3>
            </div>
            <div class="modal-body">


<table class="table table-bordered" width="100%" id="tabla-productos">
    <thead>
        <tr>
            <th style="text-align: left"> Producto </th>
            <th style="text-align: left"> Familia </th>
            <th style="text-align: left"> Prec. unit. </th>
            <th style="text-align: center"> Stock </th>
            <th style="text-align: center"> Selec </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($productos as $key => $producto): ?>
            <tr>
                <td style="text-align: left">
                    <?= $producto->producto ?> 
                </td>
                <td style="text-align: left">
                    <span class="hidden-xs">
                        <?= $producto->familia ?> 
                    </span>
                    <span class="hidden-sm hidden-lg">
                        <?= substr($producto->familia, 0,4) ?> 
                    </span>
                </td>
                <td class="dinero" > 
                    <?= $producto->precio_unitario ?> 
                </td>
                <td style="text-align: center"> 
                    <?= $producto->stock ?> 
                </td>
                <td>
                    <span class=""  >
                    <a href="javascript:;" class="btn btn-primary btn-sm btn-block btn_selec_producto" producto_id="<?= $producto->producto_id ?>" producto="<?= $producto->producto ?>" precio_unitario="<?= $producto->precio_unitario ?>" 
                        onclick="agregar_producto_adic(this,<?= $producto->producto_id ?>, '<?= $producto->producto ?>', <?= $producto->precio_unitario ?>, 1);" >
                        Selecc.
                    </a>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
        
    </tbody>
</table>



            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<script type="text/javascript">

    $('#tabla-productos').DataTable({
        "orderable": false,
        "columnDefs": [
          { "targets": [ 0 ],"width": "50%", },   //Producto
          { "targets": [ 1 ],"width": "15%", },   //Familia
          { "targets": [ 2 ],"width": "15%", },   //P unit
          { "targets": [ 3 ],"width": "10%", },   //Stock
          { "targets": [ 4 ],"width": "10%", },   //Stock
        ],
        "order": []
    });

    $('#tabla-productos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });

    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }

    function agrandar_buscador(){
        var div = document.getElementById("tabla-productos_filter");
        $(div).find('input').each(function() {
            if ($(this).attr('type') == 'search') {
                $(this).attr('id','txt_search')
                $(this).css("width",'95%');
                $(this).css("height",'25px');
                $(this).removeClass("input-sm");
                $(this).attr("placeholder",'Ingrese texto a buscar');
                var row = $(this).parents(".row");
                $(row).children('.col-xs-6').first().remove();
                $(row).children('.col-xs-6').removeClass('col-xs-6');
                $('#tabla-productos_filter').children('label').css("width",'100%');
            }
        });
        $( "#txt_search").click(function() {
            $(this).val("");
        });        
    }

    //
    $('#tabla-productos_length').addClass('hidden');
    agrandar_buscador();

</script>

<script type="text/javascript">
    function agregar_producto_adic(btn, producto_id, producto, precio_unitario, cantidad){
        $('#add_producto').val(producto);
        $('#add_precio_unitario').val(precio_unitario);
        $("#add_cantidad" ).val("1");
        $("#add_importe" ).val(precio_unitario);
        $("#add_producto_id" ).val(producto_id);
        $("#add_cantidad" ).focus();

        $('#modal_seleccionar_producto').modal('hide')
    }
</script>
