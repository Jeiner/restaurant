<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#productos">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de productos
            </a>
        </li>
        <li>
            <a  href="<?= base_url('administracion/productos/nuevo') ?>">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo producto
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="productos" class="tab-pane in active">
            <?php $this->load->view('administracion/productos/_table_productos'); ?>
        </div>
        <br>    
        <div class="row">
            <div class="col-sm-3 col-sm-offset-9">
                <a href="<?= base_url('administracion/productos/exportar_excel') ?>" class="btn btn-primary btn-sm btn-block">
                    <i class="ace-icon fa fa-file-excel-o bigger-90"></i>
                    Exportar a excel
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <i class="ace-icon fa fa-info"></i>
            <strong>Recuerde:</strong> 
            <br>
            <strong>Enviar a cocina: </strong>Al agregar a una comanda, el producto será enviado a cocina para su preparación. Así mismo es necesario indicar su <strong>KIT de preparación</strong> para verificar que haya stock de insumos disponibles.
            <br>
            <strong>Manejar stock: </strong>Por cada venta que se haga, se verificará si hay stock suficiente.
        </div>
    </div>
</div>