
<table class="table table-bordered" width="100%" id="tabla-productos">
    <thead>
        <tr>
            <th style="text-align: center"> Cód. </th>
            <th style="text-align: left"> Producto </th>
            <th style="text-align: left"> Familia </th>
            <th style="text-align: left"> Precio unit. </th>
            <th style="text-align: center"> Stock </th>
            <th style="text-align: center"> Cocina </th>
            <th style="text-align: center"> Estado </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($productos as $key => $producto): ?>
            <tr>
                <td style="text-align: center"> <?= $producto->producto_id ?> </td>
                <td style="text-align: left"> <?= strtoupper($producto->producto) ?> </td>
                <td style="text-align: left"> <?= $producto->familia ?> </td>
                <td style="text-align: left"> <?= $producto->precio_unitario ?> </td>
                <td style="text-align: center"> <?= $producto->stock ?> </td>
                <td style="text-align: center">
                    <?php if ($producto->para_cocina == 1): ?>
                        SI
                    <?php else: ?>
                        NO
                    <?php endif ?>
                </td>
                <td style="text-align: center">
                    <?php 
                        if($producto->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($producto->estado == 'X') $est_desc = '<span class="label label-danger"> Inactivo </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
    $('#tabla-productos').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },     //Código
          { "targets": [ 1 ],"width": "20%", },   //Producto
          { "targets": [ 2 ],"width": "20%", },   //Familia
          { "targets": [ 3 ],"width": "10%", },   //P unit
          { "targets": [ 4 ],"width": "10%", },   //Stock
          { "targets": [ 5 ],"width": "10%", },   //cocina
          { "targets": [ 6 ],"width": "10%", },   //Estado
        ],
        "order": [[ 2, "asc" ]] ,
    });
    $('#tabla-productos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>