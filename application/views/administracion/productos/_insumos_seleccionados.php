
<div class="row" style="margin-top: 10px;">
    <div class="col-sm-12">
        <table class="table table-bordered" width="100%">
            <thead>
                <tr>
                    <th style="width: 45%">Insumo</th>
                    <th style="width: 20%">Unidad</th>
                    <th style="width: 15%">Cantidad</th>
                    <th style="width: 5%">X</th>
                </tr>
            </thead>
            <tbody >
                <?php foreach ($oProducto->insumos as $key => $insumo): ?>
                    <tr>
                        <td>
                            <?= $insumo->insumo ?>
                        </td>
                        <td>
                            <?= $insumo->unidad_desc ?>
                        </td>
                        <td >
                            <input type="text"  class="form-control input-sm"  name="cantidad[]" value="<?= $insumo->cantidad ?>" style="text-align: center;" onchange="agregar_insumo(<?= $insumo->insumo_id ?>, this.value);"  >
                        </td>
                        <td style="vertical-align: middle;">
                            <span class="" data-rel="tooltip" title="Quitar insumo" >
                            <button type="button" class="btn btn-danger btn-xs" onclick="quitar_insumo(<?= $insumo->insumo_id ?>);">
                                X
                            </button>
                            </span>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
