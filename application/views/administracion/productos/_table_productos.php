
<table class="table table-bordered" width="100%" id="tabla-productos">
	<thead>
        <tr>
            <th style="text-align: center"> Cód. </th>
            <th style="text-align: left"> Producto </th>
            <th style="text-align: left"> Familia </th>
            <th style="text-align: left"> Ubicación </th>
            <th style="text-align: left"> Precio unit. </th>
            <th style="text-align: center"> Stock </th>
            <th style="text-align: center"> Cocina </th>
            <th style="text-align: center"> KIT </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> Edit. </th>
            <th style="text-align: center"> Elim. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($productos as $key => $producto): ?>
            <tr>
                <td style="text-align: center"> <?= $producto->producto_id ?> </td>
                <td style="text-align: left"> <?= strtoupper($producto->producto) ?> </td>
                <td style="text-align: left"> <?= $producto->familia ?> </td>
                <td style="text-align: left"> <?= $producto->ubi_carta ?> </td>
                <td style="text-align: right;padding-right: 15px!important;"> <?= $producto->precio_unitario ?> </td>
                <td style="text-align: center"> <?= $producto->stock ?> </td>
                <td style="text-align: center">
                    <?php if ($producto->para_cocina == 1): ?>
                        SI
                    <?php else: ?>
                        NO
                    <?php endif ?>
                </td>
                <td style="text-align: center">
                    <?php if ($producto->para_cocina == 1): ?>
                        <span class="" data-rel="tooltip" title="Ver KIT  de preparación" >
                            <a href="#" class="btn btn-primary btn-minier" onclick="cargar_modal_kit(<?= $producto->producto_id ?>)">
                                <i class="ace-icon fa fa-list bigger-120" aria-hidden="true"></i>
                            </a>
                        </span>
                    <?php endif ?>
                </td>
                <td style="text-align: center">
                    <?php 
                        if($producto->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($producto->estado == 'X') $est_desc = '<span class="label label-danger"> Inactivo </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('administracion/productos/editar/').$producto->producto_id ?>
                    <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_eliminar = base_url('administracion/productos/eliminar/').$producto->producto_id ?>
                    <?php $mensaje = "¿Está seguro de eliminar la producto: ".strtoupper($producto->producto)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar producto" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<div id="div_modal_kit">
    
</div>

<script type="text/javascript">
    function abrir_modal_kit(){
        $('#modal_kit').modal({
            show: 'false'
        }); 
    }
	$('#tabla-productos').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },     //Código
          { "targets": [ 1 ],"width": "20%", },   //Producto
          { "targets": [ 2 ],"width": "15%", },   //Familia
          { "targets": [ 3 ],"width": "10%", },   //Ubicación
          { "targets": [ 4 ],"width": "10%", },   //P unit
          { "targets": [ 5 ],"width": "6%", },   //Stock
          { "targets": [ 6 ],"width": "8%", },   //cocina
          { "targets": [ 7 ],"width": "6%", },   //KIT
          { "targets": [ 8 ],"width": "7%", },   //Estado
          { "targets": [ 9 ],"width": "7%", },   //Editar
          { "targets": [ 10 ],"width": "7%", },   //Eliminar
        ],
        "order": [[ 3, "asc" ]] ,
  	});
    $('#tabla-productos tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
    function cargar_modal_kit(producto_id){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/productos/cargar_modal_kit')?>",
            data: {"producto_id":producto_id},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#div_modal_kit').html(rpta);
                    $('#modal_kit').modal({
                        show: 'false'
                    });  
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>