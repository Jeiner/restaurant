<form method="Post" action="<?= base_url('administracion/productos/guardar') ?>" class="form-horizontal" id="form_producto">
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Producto_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="producto_id" id="producto_id" class="form-control input-sm" value="<?= $oProducto->producto_id ?>">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Familia <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<select class="form-control input-sm chosen-select" id="familia_id" name="familia_id">
						<option value="" >Seleccionar..</option>
						<?php foreach ($familias as $key => $cat): ?>
							<?php 
								$cat_selected = "";
								if($oProducto->familia_id == $cat->familia_id){
									$cat_selected = "selected";
								}
							?>
							<option value="<?= $cat->familia_id ?>"<?= $cat_selected ?> ><?= $cat->familia ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Producto <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="producto" id="producto" class="form-control input-sm" value="<?= $oProducto->producto ?>" placeholder="Nombre del producto">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Unidad <label class="red">*</label>  </label>
				<div class="col-sm-9 col-xs-12">
					<select class="form-control input-sm chosen-select" id="unidad" name="unidad">
						<option value="">Seleccionar..</option>
						<?php foreach ($unidades as $key => $unidad): ?>
							<?php 
								$und_selected = "";
								if($oProducto->unidad == $unidad->valor){
									$und_selected = "selected";
								}
							?>
							<option value="<?= $unidad->valor ?>"<?= $und_selected ?> ><?= $unidad->descripcion ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Precio venta <label class="red">*</label> </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="precio_unitario" id="precio_unitario" class="form-control input-sm" value="<?= $oProducto->precio_unitario ?>" placeholder="P. venta unitario (0.00)" onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Precio costo </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="precio_costo" id="precio_costo" class="form-control input-sm" value="<?= $oProducto->precio_costo ?>" placeholder="Costo unitario (0.00)" onkeypress="return soloNumeroDecimal(event)">
				</div>
			</div>
			<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Enviar a cocina </label>
					<?php 
						if($oProducto->para_cocina == 1){
							$checked_cocina_si = "checked";
							$checked_cocina_no = "";
						}else{
							$checked_cocina_si = "";
							$checked_cocina_no = "checked";
						}
					?>
					<div class="col-sm-2 col-xs-3" align="center">
						<div class="radio">
							<label>
								<input name="para_cocina" value="1" type="radio" class="ace" <?= $checked_cocina_si ?> onchange="ver_kit(this.value);"/>
								<span class="lbl"> SI </span>
							</label>
						</div>
					</div>
					<div class="col-sm-2 col-xs-3" align="center">
						<div class="radio">
							<label>
								<input name="para_cocina" value="0" type="radio" class="ace" <?= $checked_cocina_no ?> onchange="ver_kit(this.value);"/>
								<span class="lbl"> NO </span>
							</label>
						</div>
					</div>
			</div>
			<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Manejar stock </label>
					<?php 
						if($oProducto->manejar_stock == 1){
							$checked_stock_si = "checked";
							$checked_stock_no = "";
						}else{
							$checked_stock_si = "";
							$checked_stock_no = "checked";
						}
					?>
					<div class="col-sm-2 col-xs-3" align="center">
						<div class="radio">
							<label>
								<input name="manejar_stock" value="1" type="radio" class="ace" <?= $checked_stock_si ?> onchange="ver_manejo_stock(this.value)"  />
								<span class="lbl"> SI </span>
							</label>
						</div>
					</div>
					<div class="col-sm-2 col-xs-3" align="center">
						<div class="radio">
							<label>
								<input name="manejar_stock" value="0" type="radio" class="ace" <?= $checked_stock_no ?> onchange="ver_manejo_stock(this.value);"/>
								<span class="lbl"> NO </span>
							</label>
						</div>
					</div>
			</div>
			<div id="grupo_stock" <?php if($oProducto->manejar_stock == 0) echo "style='display: none'" ?>>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Stock actual </label>
					<div class="col-sm-5 col-xs-12">
						<input type="text" name="stock" id="stock" class="form-control input-sm" value="<?= $oProducto->stock ?>" placeholder="Stock actual" onkeypress="return soloNumeroEntero(event)">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Stock mínimo </label>
					<div class="col-sm-5 col-xs-12">
						<input type="text" name="stock_minimo" id="stock_minimo" class="form-control input-sm" value="<?= $oProducto->stock_minimo ?>" onkeypress="return soloNumeroEntero(event)">
					</div>
				</div>
				<div class="form-group ">
					<label class="col-sm-3 col-xs-12 control-label" for=""> Stock máximo </label>
					<div class="col-sm-5 col-xs-12">
						<input type="text" name="stock_maximo" id="stock_maximo" class="form-control input-sm" value="<?= $oProducto->stock_maximo ?>" onkeypress="return soloNumeroEntero(event)">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Estado  </label>
				<div class="col-sm-5 col-xs-12">
					<select class="form-control input-sm" id="estado" name="estado">
						<option> Seleccionar... </option>
						<option value="A" <?php if($oProducto->estado == 'A') echo "selected" ?>> Activo </option>
						<option value="X" <?php if($oProducto->estado == 'X') echo "selected" ?>> Inactivo </option>
					</select>
				</div>
			</div>
		</div> <!-- col -->
		<div class="col-sm-5 col-sm-offset-1">
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Ubicación </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="ubi_carta" id="ubi_carta" class="form-control input-sm" value="<?= $oProducto->ubi_carta ?>" placeholder="Ubicación en carta (00000000)" maxlength="8">
					<label>XX: Nro hoja en carta</label><br>
					<label>XX: Grupo</label><br>
					<label>XX: Fila</label><br>
					<label>XX: Columna</label><br>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Descripcion </label>
				<div class="col-sm-9 col-xs-12">
					<textarea class="form-control input-sm" name="descripcion" id="descripcion"><?= $oProducto->descripcion ?></textarea>
				</div>
			</div>
			<div id="grupo_kit" style="margin-top: 25px;">
				<div class="form-group center">
					<label> <strong>KIT  de preparación</strong> </label>
				</div>
				<div class="form-group">
					<select class="form-control input-sm chosen-select" id="kit" name="kit" onchange="agregar_insumo(this.value,0)">
						<option value="">Seleccionar insumo..</option>
						<?php foreach ($insumos as $key => $insumo): ?>
							<option value="<?= $insumo->insumo_id ?>"><?= $insumo->insumo ?></option>
						<?php endforeach ?>
					</select>
					<br>
					<div id="insumos_seleccionados">
						<?php $this->load->view('administracion/productos/_insumos_seleccionados'); ?>
					</div>
				</div>
			</div>
		</div> <!-- col -->
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oProducto->producto_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-4 col-md-3 col-sm-offset-8 col-md-offset-9">
				<button class="btn btn-primary btn-block btn-sm" id="btn_guardar" disabled="disabled"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar producto 
				</button>	
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-4 col-md-3  col-sm-offset-4 col-md-offset-6">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="<?= base_url('administracion/productos') ?>" class="btn btn-inverse btn-block btn-sm ">
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Cancelar edición 
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-3" > 
				<div class="form-group">
					<div class="col-xs-12">
						<button class="btn btn-primary btn-block btn-sm"> 
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Actualizar producto 
						</button>
					</div>
				</div>						
			</div>
		</div>
	<?php endif ?>
</form>



<script type="text/javascript">
	$('.chosen-select').chosen({allow_single_deselect:true}); 

	// Validaciones
	$("#form_producto").submit(function() {
        $('#btn_guardar').attr('disabled', true);
    });
	$("#form_producto").validate({
        rules: {
            familia_id: "required",
            producto: "required",
            unidad: "required",
            precio_unitario: "required"
        },
        messages:{
        	familia_id: "Seleccione una familia",
            producto: "Ingrese nombre del producto",
            unidad: "Seleccione la unidad",
            precio_unitario: "Ingrese precio de venta"

        }
    });
    $('#form_producto').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#form_producto').valid()) {                   // checks form for validity
            $('#btn_guardar').attr('disabled', false);        // enables button
        } else {
            $('#btn_guardar').attr('disabled', true);   // disables button
        }
    });
	function ver_manejo_stock(valor){
		if(valor == 1){
			$('#grupo_stock').show('slow');
		}else{
			$('#grupo_stock').hide('slow');
		}
	}
	function ver_kit(valor){
		if(valor == 1){
			$('#grupo_kit').show('slow');
		}else{
			$('#grupo_kit').hide('slow');
		}
	}
	function agregar_insumo(insumo_id, cantidad){
		if (insumo_id == '') {
			return false;
		}
		$.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/productos/agregar_insumo')?>",
            data: { "insumo_id": insumo_id, "cantidad": cantidad },
            success: function(rpta){
            	if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    if(rpta == 'existe'){
	                    alertify.error("El insumo ya está seleccionado.");
	                }
	                else{
	                    $('#insumos_seleccionados').html(rpta);
	                    if(cantidad == 0){
	                        alertify.success("Insumo agregado correctamente.");
	                    }else{
	                        alertify.success("Cantidad actualizada correctamente.");
	                    }
	                } 
                }
            },
            error: function(rpta){
                alert("Error de conexión.");
            }
        });
	}
	function quitar_insumo(insumo_id ){
		$.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/productos/quitar_insumo')?>",
            data: { "insumo_id": insumo_id },
            success: function(rpta){
            	if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    if(rpta == '504'){
	                    alertify.error("El insumo no existe en la lista.");
	                    return false;
	                }else{
	                    $('#insumos_seleccionados').html(rpta);
	                    alertify.success("Insumo quitado correctamente.");
	                }
                }
            },
            error: function(rpta){
                alert("Error de conexión.");
            }
        });
	}
</script>
