
<?php  
    $days_dias = array(
    'Monday'=>'LUNES',
    'Tuesday'=>'MARTES',
    'Wednesday'=>'MIERCOLES',
    'Thursday'=>'JUEVES',
    'Friday'=>'VIERNES',
    'Saturday'=>'SABADO',
    'Sunday'=>'DOMINGO'
    );
?>
<style type="text/css">
    .badge-error{
        /*color: #fff!important;*/
        margin-top:  0px!important;
        font-size: 13px!important;
        padding: 4px 5px!important;
        /*background-color: #cd9223!important;*/
    }
</style>
<div id="modal_ver_comprobantes" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong>
                        <?= $mesNombre ?> <?= $anio ?> <br>
                        <?= $oEmpleado->emp_code.' - '.$oEmpleado->first_name ?>
                    </strong>
                </h3>
            </div>
            <div class="modal-body">

                <div class="row">
                    <?php foreach ($oEmpleado->lstAsistencia as $key => $oAsistencias): ?>

                        <?php 
                            if($fechaActual == $oAsistencias['fecha']){
                                $dia_activo = "badge badge-error";
                            }else{
                                $dia_activo = "";
                            }
                        ?>
                        <div class="col-sm-2 col-xs-2"
                            style="height: 75px;border: 1px solid #CCC; text-align: center; padding: 5px; margin-top: 10px;">
                            <div style="font-size: 14px;">
                                <strong class="<?= $dia_activo ?>">
                                    <?= $oAsistencias['dia'] ?>
                                </strong>
                            </div>
                            <div style="font-size: 12px; padding-top: 5px;">

                                <?php if ($oAsistencias['estado_entrada'] == -1): ?>
                                    <label style="font-size: 12px; min-height:10px" class="label label-danger">
                                        <?= $oAsistencias['entrada'] ?>
                                    </label>
                                <?php else: ?>
                                    <label style="font-size: 12px; min-height:10px">
                                        <?= $oAsistencias['entrada'] ?>
                                    </label>
                                <?php endif ?>
                                
                            </div>
                            <div style="font-size: 12px;">
                                <?= $oAsistencias['salida'] ?>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>

            </div>
            <div class="modal-footer">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    //Abrir modal
    $('#modal_ver_comprobantes').modal({
        show: 'false'
    });
</script>

<script type="text/javascript">

    $('#tabla_comandas').DataTable({
        "orderable": false,
        "columnDefs": [
          { "targets": [ 0 ],"width": "20%", },   //Producto
          { "targets": [ 1 ],"width": "20%", },   //Familia
          { "targets": [ 2 ],"width": "20%", },   //P unit
          { "targets": [ 3 ],"width": "20%", },   //Stock
          { "targets": [ 4 ],"width": "20%", },   //Stock
        ],
        "order": [0]
    });

    $('#tabla_comandas tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });

    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }


</script>

