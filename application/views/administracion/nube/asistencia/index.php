<?php 
    $days_dias = array(
    'Monday'=>'LU',
    'Tuesday'=>'MA',
    'Wednesday'=>'MI',
    'Thursday'=>'JU',
    'Friday'=>'VI',
    'Saturday'=>'SA',
    'Sunday'=>'DO'
    );

    $lstMeses = array(
        '1'=>'ENE',
        '2'=>'FEB',
        '3'=>'MAR',
        '4'=>'ABR',
        '5'=>'MAYO',
        '6'=>'JUN',
        '7'=>'JUL',
        '8'=>'AGO',
        '9'=>'SET',
        '10'=>'OCT',
        '11'=>'NOV',
        '12'=>'DIC'
    );

    $data['lstMeses'] = $lstMeses;
?>
<html>
<head>
    <meta charset="utf-8">
    <title></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <!-- CSRF Token -->

    <meta name="Santorini" content="Santorini &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <link rel="shortcut icon" href="http://localhost:50/sanbrasa/assets/img/security.png" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css')?> " />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css')?> " />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css')?> " class="ace-main-stylesheet" id="main-ace-style" />

    <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css')?> " />

    <link rel="stylesheet" href="<?= base_url('assets/img/splashy/splashy.css') ?> " />

    <!-- ace settings handler -->
    <script src="<?= base_url('assets/js/ace-extra.min.js')?> "></script>

    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/jqeasyui/themes/icon.css') ?> ">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/update_template.css') ?> ">
    <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/library/alertify/alertify.css')?> " />
    <link rel="stylesheet" href="<?= base_url('assets/library/alertify/default.rtl.css')?> " />

    <script src="<?= base_url('assets/js/jquery-2.1.4.min.js')?> "></script>
    <script src="<?= base_url('assets/js/jquery.validate.min.js')?> "></script>
    
    <script type="text/javascript" src="<?= base_url('assets/library/alertify/alertify.min.js') ?> "></script>

    <script type="text/javascript" src="<?= base_url('assets/library/sweetalert/sweetalert.min.js') ?> "></script>
    <link rel="stylesheet" href="<?= base_url('assets/library/sweetalert/sweetalert.css')?> " />
</head>
<style type="text/css">
    .no-skin .sidebar-shortcuts{
        background-color: #FFF;
    }
    .btn-dia{
        color: #555353;
        background-color: #FFF;
        border: 1px solid #FFF;
        border-bottom: 4px solid #FFF;
        font-weight: 600;
    }

    .btn-dia-active{
        color: #d02d32;
        border-bottom: 4px #d02d32 solid;
    }

    .btn-dia: hover, .btn-dia: focus, .btn-dia: before{
        color: #555353;
        background-color: red!important;
        border: 4px solid red!important;
    }

    /*Preloader*/
    .imagen_cargando{
        display: none;
    }
    .container_loader .transparente{
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;

        opacity: 0.3;
        background-color: #797575;
        width:100%;
        height:100%;
        position: absolute;
    }
    .container_loader .imagen_cargando {
        display: block;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        opacity: 1;
        /*float: left;*/
        margin-top:10%;
        margin-left: 25%;
        width:50%;
        position: absolute;
    }
    .container_loader .imagen_cargando img{
        width: 300px;
    }

</style>

<body class="no-skin">


    <div id="navbar" class="navbar navbar-default">
        <script type="text/javascript">
            try{ace.settings.check('navbar' , 'fixed')}catch(e){}
        </script>
        <div class="navbar-container" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left display" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try{ace.settings.loadState('main-container')}catch(e){}
        </script>
            

        <script src="<?= base_url('assets/js/bootstrap.min.js') ?> "></script>

        <!-- page specific plugin scripts -->
        <script src="<?= base_url('assets/js/bootstrap-datepicker.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/jquery.jqGrid.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/grid.locale-en.js') ?> "></script>

        <!-- ace scripts -->
        <script src="<?= base_url('assets/js/ace-elements.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/ace.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/script.js') ?> "></script>

        <!-- inline scripts related to this page -->

        <script src="<?= base_url('assets/js/jquery.dataTables.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/jquery.dataTables.bootstrap.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/dataTables.tableTools.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/dataTables.colVis.min.js') ?> "></script>
        <script src="<?= base_url('assets/js/pluginModalPDF.js') ?> "></script>

        <script src="<?= base_url('assets/js/chosen.jquery.min.js') ?> "></script>
        <!-- Para graficos de estadisticas -->
        <script src="<?= base_url('assets/js/jquery.easypiechart.min.js') ?> "></script>


            <div id="sidebar" class="sidebar responsive ace-save-state compact " data-sidebar="false" data-sidebar-hover="false" >
                <script type="text/javascript">
                    try{ace.settings.loadState('sidebar')}catch(e){}
                </script>
                <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                        <button class="btn btn-success">
                            <i class="ace-icon fa fa-signal"></i>
                        </button>
                        <button class="btn btn-info">
                            <i class="ace-icon fa fa-pencil"></i>
                        </button>
                        <button class="btn btn-warning">
                            <i class="ace-icon fa fa-users"></i>
                        </button>
                        <button class="btn btn-danger">
                            <i class="ace-icon fa fa-cogs"></i>
                        </button>
                    </div>
                    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>

                        <span class="btn btn-info"></span>

                        <span class="btn btn-warning"></span>

                        <span class="btn btn-danger"></span>
                    </div>
                </div><!-- /.sidebar-shortcuts -->

                    
                <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                    <i id="sidebar-toggle-icon" class="ace-save-state ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
                </div>
            </div>
            <div class="main-content">
                <div class="main-content-inner">
                        <div class="page-content">
                            <!-- Mostrar mensajes como respuesta de una acción anterior (Registro, actualización, etc.) -->

                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                                        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">

                                            <?php foreach ($lstMesesMostrar as $key => $oMesMostrar): ?>
                                                <?php
                                                    $active = ($oMesMostrar['mes'] == $mes)? "btn-dia-active" : "";
                                                ?>
                                            
                                                <button class="btn-dia <?= $active ?>" 
                                                        onclick="cambiar_mes('<?= $oMesMostrar['anio'] ?>','<?= $oMesMostrar['mes'] ?>')">
                                                 <?= $oMesMostrar['mesAbrev'] ?>
                                                 <br>
                                                 <?= $oMesMostrar['anio'] ?>
                                                </button>
                                            <?php endforeach ?>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="border: 1px solid #eee; margin-bottom: 20px;">
                                
                            </div>

                            <div class="row">
                                    <?php $this->view('administracion/nube/asistencia/box_tabla_asistencia', $data); ?>
                            </div>

                            <div class="" id="preloader">
                                <div class="transparente">
                                    
                                </div>
                                <div class="imagen_cargando" align="center" style="">
                                    <img src="<?=base_url('assets/img/preloader_comida.gif')?>" class="img-responsive">
                                </div>
                            </div> 



                        </div>
                </div>
            </div><!-- /.main-content -->
    
    <div class="footer">
        <div class="footer-inner">
            <div class="footer-content">
                <span class="bigger-120">
                    <span class="blue bolder">SANTORINI</span>
                    Pizzas y comida italiana © 2018
                </span>

                &nbsp; &nbsp;
                <span class="action-buttons">
                    <!-- <a href="#">
                        <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                    </a> -->

                    <a href="https://www.facebook.com/Pizzeria-Santorini-1500364683510878/" target="blanck">
                        <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                    </a>

                    <!-- <a href="#">
                        <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                    </a> -->
                </span>
            </div>
        </div>
    </div>
        <span class="ir-arriba" style="display: none;">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </span>
        <span class="ir-abajo" style="display: none;">
            <i class="ace-icon fa fa-angle-double-down icon-only bigger-110"></i>
        </span>
    </div><!-- /.main-container -->
    



<style type="text/css">
    .ir-arriba,.ir-abajo {
        /*display:none;*/
        padding:10px 20px;
        opacity: 0.5;
        background:#888888;
        font-size:20px;
        color:#fff;
        cursor:pointer;
        position: fixed;
        
    }
    .ir-arriba {
        bottom:70px;
        right:10px;
        /*display:none;*/
    }
    .ir-abajo{
        bottom:10px;
        right:10px;
    }
    .ir-arriba:hover,.ir-abajo:hover {
        opacity: 1.0;
    }
</style>

</body>

</html>

<script type="text/javascript">

    function date_diff(start_date_AMD, end_date_AMD,tipe){
        var start_date = new Date(start_date_AMD).getTime();
        var end_date    = new Date(end_date_AMD).getTime();
        var diff = end_date - start_date;
        return diff/(1000*60*60*24);
    }
    

    

    function cambiar_mes(anio, mes){
        abrirCargandoFull();
        let url = "<?= base_url() ?>" + "administracion/nube/asistencia"+ "?anio="+ anio + "&&mes="+ mes
        window.location = url;
    }

    function abrirCargandoFull(){
        $("#preloader").addClass("container_loader");
    }

    function cerrarCargandoFull(){
        $("#preloader").removeClass("container_loader");
    }
</script>