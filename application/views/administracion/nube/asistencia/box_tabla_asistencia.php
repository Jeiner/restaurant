<style type="text/css">
	.infobox-santorini{
		width: 100%!important;
		border: none;
		text-align: center;
	}

	.infobox-yellow>.infobox-icon>.ace-icon{
		background-color: #e59729;
	}

	.infobox-blue-sky>.infobox-icon>.ace-icon{
		background-color: #22ABBC;
	}

	.row_select{
		background-color: #f3f1f1;
	}

    .badge-error{
    	padding-top: 3px!important;
    	padding-left: 4px ;
    	padding-right: 4px ;
        background-color: red;
        margin-left: 5px;
        margin-top: -5px;
    }
	
	table{
		margin-bottom: 10px;
	}

	table tbody tr{
		border-top: 1px solid #ccc!important;
		border-bottom: 1px solid #ccc!important;
	}
	table tbody tr td{
		padding-top: 10px!important;
		padding-bottom: 10px!important;
	}
	table tbody tr td a{
		color: #000;
		/*text-decoration: underline;*/
	}
</style>
	<div class="col-sm-3">
		<table>
			<thead>
				
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<div class="col-sm-6 col-xs-12">
		<label>
			Última actualización: <?= $ultimaActualizacion ?>
		</label>

		<div class="widget-box widget-color-granate">
			<div class="widget-header widget-header-flat widget-header-small" style="padding-top: 2px; padding-bottom: 2px; font-size: 14px">
				 <h5 class="widget-title" style="text-transform: uppercase;">
					<i class="ace-icon fa fa-signal"></i>
					 SANTORINI - Ver asistencias
				</h5>

				<div class="widget-toolbar no-border">
					<div class="inline dropdown-hover">

					</div>
				</div>
			</div>
			<div class="widget-body" >
				<div class="widget-main">
					<div class="row">
						<div class="col-sm-12" style="margin-bottom: 10px;">
							<a href="<?= base_url("administracion/locales") ?>" class="btn btn-sm btn-primary">Regresar a locales</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<table class="table" width="100%" id="tabla_box">
								<thead>
									<tr>
										<th>
											N°
										</th>
										<th>
											Nombre
										</th>
										<th>
											Mes
										</th>
										<th>
											Observ
										</th>
									</tr>
								</thead>
								<tbody>

									<?php foreach ($lstEmpleados as $key => $oEmpleado): ?>
									<tr  onclick="ver_detalle_por_usuario('<?= $oEmpleado->emp_code ?>', '<?= $anio ?>', '<?= $mes ?>');">
										<td class="text-center">
											<?= $key + 1 ?>
										</td>
										<td>
											<?= $oEmpleado->emp_code?>
											<br>
											<?= $oEmpleado->first_name?>
											<br>
											<?php if ($oEmpleado != null): ?>
												<span class="text-muted" style="font-size: 13;">
													Entrada: <?= $oEmpleado->hora_entrada ?>
												</span>
											<?php endif ?>
										</td>
										<td>
											<?= $lstMeses[intval($mes)] ?> - <?= $anio ?>
										</td>
										<td>
											<div>
												<!-- Tardanzas -->
												<?php if ($oEmpleado->resumen['contadorTardanzas'] > 0): ?>
												<label class="label label-danger" style="font-size: 13;">
													Tardanzas: <?= $oEmpleado->resumen['contadorTardanzas'] ?>
												</label>
												<?php else: ?>
												<label class="text-muted" style="font-size: 13;">
													Tardanzas: <?= $oEmpleado->resumen['contadorTardanzas'] ?>
												</label>
												<?php endif ?>

											</div>
											
											<div>

												<?php if ($oEmpleado->resumen['contadorFaltas'] > 4): ?>
												<label class="label label-warning" style="font-size: 13;">
													Faltas: <?= $oEmpleado->resumen['contadorFaltas'] ?>
												</label>
												<?php else: ?>
													<label class="text-muted" style="font-size: 13;">
														Faltas: <?= $oEmpleado->resumen['contadorFaltas'] ?>
													</label>
												<?php endif ?>

											</div>
										</td>
									</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		

<div id="div_modal_detalle_asistencia">
	
</div>

<script type="text/javascript">
	
	function ver_detalle_por_usuario(emp_code, anio, mes){
        $('#div_modal_detalle_asistencia').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/nube/asistencia/ajax_detalle_por_usuario')?>",
            data: { "mes": mes, "anio" : anio, "emp_code": emp_code },
            success: function(rpta){
                $('#div_modal_detalle_asistencia').html(rpta);
                cerrarCargandoFull();
            },
            error: function(rpta){
                alert("Error en la operación");
                cerrarCargandoFull();
            }
        });
	}

	$('#tabla_box').DataTable({
		"pageLength": 50,
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", "orderable": true, },   //Cod
          { "targets": [ 1 ],"width": "30%", "orderable": true, },   //MEsa
          { "targets": [ 2 ],"width": "30%", "orderable": true, },   //Mozo
          { "targets": [ 3 ],"width": "30%", "orderable": true, },   //Mozo

        ],
        "order": [[ 0, "asc" ]] ,
    });
	$('#tabla_box tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFilaBox(this);
        }
    });

    function seleccionarFilaBox(fila) {
        $(".row_select").removeClass("row_select");
        $(fila).addClass("row_select");
    }
</script>