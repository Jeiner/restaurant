<style type="text/css">
	tbody tr td{
		/*font-weight: bold!important;*/
		font-size: 14px!important;
	}
</style>
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box widget-color-red2">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title smaller"> Comandas por pagar </h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<?php $this->load->view('facturacion/pagos/_tabla_por_pagar'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box widget-color-green">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title smaller"> Comandas pagadas </h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<?php $this->load->view('facturacion/pagos/_tabla_pagadas'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box widget-color-grey">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title smaller"> Comandas anuladas </h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<?php $this->load->view('facturacion/pagos/_tabla_anuladas'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
    setTimeout('document.location.reload()',5000);
</script>