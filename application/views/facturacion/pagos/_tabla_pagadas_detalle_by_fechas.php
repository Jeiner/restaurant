<table class="table table-bordered table-striped" id="tabla_data_detalle" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;" title="comanda - comanda_item" > Item </th>
            <th style="text-align: center;" > Fecha pago</th>
            <th style="text-align: center;" class="hidden-xs"> Cajero </th>
            <th style="text-align: center;" > Producto </th>
            <th style="text-align: center;" > Precio uni. </th>
            <th style="text-align: center;" > Cant. </th>
            <th style="text-align: center;" > importe </th>
            <th style="text-align: center;" class="hidden-xs" > Espera </th>
            <th style="text-align: center;" > Estado </th>
        </tr>
    </thead>
    <tbody id="cuerpo_tabla_comandas">
    	<?php foreach ($venta_detalle as $key => $item): ?>
    		<tr>
				<td style="text-align: center" title="comanda - comanda_item" > 
                    <?= $item->comanda_id  ?> - <?= $item->comanda_item_id  ?> 
                </td>
				<td style="text-align: center"> <?= $item->fecha_pago ?> </td>
				<td style="text-align: center" class="hidden-xs"> <?= $item->cajero  ?> </td>
                <td style="text-align: left"> <?= $item->producto ?> </td>
                <td style="text-align: center" class="dinero"> <?= $item->precio_unitario ?> </td>
                <td style="text-align: center"> <?= $item->cantidad ?> </td>
                <td style="text-align: center" class="dinero"> <?= $item->importe ?> </td>
                <td style="text-align: center" class="hidden-xs"> <?= $item->tiempo_espera.' min' ?> </td>
                <td style="text-align: center">
                    <?php if ($item->estado_atencion == 'E'): ?>
                        <span class="label label-warning"> En espera </span>
                    <?php endif ?>
                    <?php if ($item->estado_atencion == 'P'): ?>
                        <span class="label label-primary"> En preparación </span>
                    <?php endif ?>
                    <?php if ($item->estado_atencion == 'D'): ?>
                        <span class="label label-success"> Despachado </span>
                    <?php endif ?>
                    <?php if ($item->estado_atencion == 'X'): ?>
                        <span class="label label-inverse"> Anulado </span>
                    <?php endif ?>
                </td>
			</tr>
    	<?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">

    $('#tabla_data_detalle').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "9%", },   //Cod item
          { "targets": [ 1 ],"width": "15%", },   //Fecha
          { "targets": [ 2 ],"width": "9%", },   //Cajero
          { "targets": [ 3 ],"width": "25%", },   //Producto
          { "targets": [ 4 ],"width": "8%", },   //Precio unit.
          { "targets": [ 5 ],"width": "6%", },   //Cantidad
          { "targets": [ 6 ],"width": "8%", },   //Imporrte
          { "targets": [ 7 ],"width": "10%", },   //Espera
          { "targets": [ 8 ],"width": "10%", },   //Estad
        ],
        "order": [[ 1, "desc" ]] ,
    });
    
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>