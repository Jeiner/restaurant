<!-- 
    Recibe: 
        - oComanda
        - oPago
 -->
<div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
    <div class="widget-header">
        <h6 class="widget-title">
            <i class="ace-icon fa fa-money bigger-90" aria-hidden="true"></i>
            Datos del pago
        </h6>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-down"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main padding-4" style="position: relative; padding: 15px">
            <?php if ($oPago != null): ?>
                <div class="row">
                    <div class="col-sm-12 form-horizontal">
                        <div class="form-group hidden">
                            <div class="col-sm-3 ">
                                <div class="form-group">
                                    <label>Comanda id</label>
                                    <input type="text" style="text-align:center" name="comanda_id" id="comanda_id" class="form-control input-sm" value="<?= $oComanda->comanda_id ?>" >
                                </div>
                            </div>
                            <div class="col-sm-3 ">
                                <div class="form-group">
                                    <label>pago id</label>
                                    <input type="text" style="text-align:center" name="pago_id" id="pago_id" class="form-control input-sm" value="<?= $oPago->pago_id ?>" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"> Fecha</label>
                            <div class="col-sm-3">
                                <input type="" name="" class="input-sm form-control" value="<?= $oPago->fecha_pago ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"> Comprobante </label>
                            <div class="col-sm-3">
                                <input type="" name="" class="input-sm form-control" value="<?= $oPago->tipo_comprobante ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"> Serie </label>
                            <div class="col-sm-3">
                                <input type="" name="" class="input-sm form-control" value="<?= $oPago->serie ?> - <?= $oPago->correlativo ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"> Cliente </label>
                            <div class="col-sm-6">
                                <input type="" name="" class="input-sm form-control" value="<?= $oPago->cliente_nombre ?>" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group" id="input_descuento" style="<?php if($oPago->descuento == "0.00") echo "display: none" ?>">
                            <label class="col-sm-3" style=""> <strong>Descuento </strong></label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="descuento" id="descuento" value="<?= $oPago->descuento ?>"  onchange="aplicar_descuento();" onkeypress="return soloNumeroDecimal(event)" readonly>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3" style=""> <strong>Total a pagar</strong> </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="total_a_pagar" id="total_a_pagar"  value="<?= $oPago->total_a_pagar ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <label class="col-sm-3" style=""> <strong>Pago en efectivo</strong> </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="pago_efectivo" id="pago_efectivo"  value="<?= $oPago->pago_efectivo ?>" onkeypress="return soloNumeroDecimal(event)" onchange="ingresar_pago(this);" readonly>
                            </div>
                        </div>
                        <div class="form-group" id="input_tarjeta" style="<?php if($oPago->pago_tarjeta == "0.00") echo "display: none" ?>">
                            <label class="col-sm-3" style=""> <strong>Tajerta</strong> </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="pago_tarjeta" id="pago_tarjeta"  value="<?= $oPago->pago_tarjeta ?>" onkeypress="return soloNumeroDecimal(event)" onchange="ingresar_pago(this);" readonly>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-3" style=""> <strong>Efectivo</strong> </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="efectivo" id="efectivo"  value="<?= $oPago->efectivo ?>" onkeypress="return soloNumeroDecimal(event)" onchange="ingresar_pago(this);" readonly>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-3" style=""> <strong>Vuelto</strong> </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="vuelto" id="vuelto"  value="<?= $oPago->vuelto ?>" onkeypress="return soloNumeroDecimal(event)" onchange="ingresar_pago(this);" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style=""> <strong>Saldo</strong> </label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control input-sm dinero-total" name="saldo" id="saldo"  value="<?= $oPago->saldo ?>" onkeypress="return soloNumeroDecimal(event)" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3"> Observaciones </label>
                            <div class="col-sm-6">
                                <textarea readonly="readonly" class="input-sm form-control"><?= $oPago->observaciones ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <a class="btn btn-primary btn-sm btn-block" href="javascript:;" onclick="imprimir_ticket(<?= $oPago->pago_id ?>)">
                            <i class="ace-icon fa fa-print bigger-90" aria-hidden="true"></i>
                            Imprimir ticket
                        </a>
                    </div>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-sm-12">
                        El pedido aún no tiene pagos.
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
 
<script type="text/javascript">
    function imprimir_ticket(pago_id){
        url = '<?= base_url("facturacion/pagos/imprimir_ticket/") ?>' + '<?php if($oPago != null){ echo $oPago->pago_id;} ?>';
        $.ajax({
            type: 'POST',
            url: url,
            data: "pago_id= " + pago_id,
            success: function(rpta){
                var pdf_link = "<?=base_url()?>reportes_pdf/comprobante.pdf";
                var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="100%">No Support</object>';
                $.createModal({
                    title: "",
                    message: iframe,
                    closeButton:true,
                    scrollable:false
                });
            },
            error: function(rpta){
                alert("Error al cargar la lista "+rpta);
            }
        });
        return false;
    }
</script>