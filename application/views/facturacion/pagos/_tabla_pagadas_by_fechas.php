<?php
    $total_pagos = 0;
    $descuentos = 0;
?>
<!-- Tabla comandas facturadas -->
<table class="table table-bordered table-striped" id="tabla_facturadas" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;" title="Comanda" > Cód. </th>
            <th style="text-align: center;" > Fecha pago </th>
            <th style="text-align: center;" class="hidden-xs" > Mesa </th>
            <th style="text-align: center;" class="hidden-xs"> Cajero </th>
            <th style="text-align: center;" class="hidden-xs" > Doc. interno  </th>
            <th style="text-align: center;" class="hidden-xs" > cpbe </th>
            <th style="text-align: center;" > Total </th>
            <th style="text-align: center;" > Desc. </th>
            <th style="text-align: center;" > Pagado </th>
            <th style="text-align: center;" > Estado </th>
            <th style="text-align: center;" >  </th>
        </tr>
    </thead>
    <tbody id="cuerpo_tabla_comandas">
    	<?php foreach ($pagos as $key => $pago): ?>
            <?php 
                $total_pagos += $pago->importe_total; 
                $descuentos += $pago->descuento; 
            ?>
    		<tr>
				<td style="text-align: center" title="Comanda" > 
                    <?= $pago->comanda_id  ?> 
                </td>
				<td style="text-align: center"> 
                    <?= $pago->fecha_pago ?> 
                </td>
                <td style="text-align: center" class="hidden-xs">
                    <?php if ($pago->modalidad != 'PRE'): ?>
                        <?= ($pago->modalidad == 'DEL') ? 'Delivery': 'Para llevar' ?> 
                    <?php else: ?>
                        <?= $pago->mesa ?> 
                    <?php endif ?>
                </td>
				<td style="text-align: center" class="hidden-xs"> 
                    <?= strtoupper($pago->cajero) ?> 
                </td>
                <td style="text-align: center" class="hidden-xs" title="<?= $pago->tipo_comprobante_desc ?>"> 
                    <?= $pago->serie ?> - <?= $pago->correlativo ?> 
                </td>
                <td style="text-align: center" class="hidden-xs"> 
                    <?php if ($pago->con_comprobante_e == 'C'): ?>
                        Completo
                    <?php endif ?>
                    <?php if ($pago->con_comprobante_e == 'R'): ?>
                        Parcial
                    <?php endif ?>
                </td>
				<td style="text-align: center" class="dinero"> <?= $pago->importe_total ?> </td>
                <td style="text-align: center" class="dinero"> <?= $pago->descuento ?> </td>
                <td style="text-align: center" class="dinero"> <?= $pago->total_pagado ?> </td>
                <td style="text-align: center"> 
                    <?php if ( $pago->estado == 'A'): ?>
                        <span class="label label-success"> Activa </span>
                    <?php else: ?>
                        <span class="label label-inverse"> Anulado </span>
                    <?php endif ?>
                </td>
				<td style="text-align: center">
                    <div class="btn-group ">
                        <button data-toggle="dropdown" class="btn dropdown-toggle btn-inverse btn-sm" aria-expanded="true">
                            <span class="ace-icon fa fa-caret-down icon-only"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-grey dropdown-menu-right">
                            <li>
                                <a href="<?= base_url('salon/comanda/ver/') ?><?= $pago->comanda_id ?>">
                                    Ver detalle
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
			</tr>
    	<?php endforeach ?>
    </tbody>
</table>
<br>
<?php 
    $total = $total_pagos - $descuentos;
 ?>
<div class="row">
    <div class="col-sm-4 col-sm-offset-8 col-lg-3 col-lg-offset-9">
        <div class="form-group">
            <label class="col-sm-4" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                <strong>Total: </strong>
            </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="" id="" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= number_format(round($total,2), 2, '.', ' ') ?>" readonly>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('#tabla_facturadas').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },   //Comanda id
          { "targets": [ 1 ],"width": "15%", },   //Fecha pago

          { "targets": [ 2 ],"width": "10%", },   //Mesa
          { "targets": [ 3 ],"width": "10%", },   //Cajero
          { "targets": [ 4 ],"width": "10%", },   //Documento
          { "targets": [ 5 ],"width": "10%", },   //Seire

          { "targets": [ 6 ],"width": "8%", },   //Total
          { "targets": [ 7 ],"width": "8%", },   //Desc
          { "targets": [ 8 ],"width": "7%", },   //Pagado

          { "targets": [ 9 ],"width": "10%", },   //Estado
          { "targets": [ 10 ],"width": "5%", },   //Opciones
        ],
        "order": [[ 1, "desc" ]] ,
    });
    
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>