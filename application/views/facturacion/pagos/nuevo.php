<?php  
    $importe_total = 0;
    $porcentaje_igv = $igv * 100;
?>

<!-- Titulo -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">            
            <h1 style="font-weight: 500">
                <i class="ace-icon fa fa-money bigger-90" aria-hidden="true"></i>
                Facturar comanda
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Código comanda: <?= $oComanda->comanda_id ?>
                </small>
            </h1>
        </div>
    </div>
</div>

<?= form_open(base_url('facturacion/pagos/guardar_pago'), '" id="form_pago"'); ?>
<div class="row">
    <div class="col-sm-7">

        <div class="widget-box widget-color-granate"> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-shopping-cart bigger-90" aria-hidden="true"></i>
                    Detalle de la comanda
                </h6>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                    <!-- Datos generales de comanda -->
                    <div class="row" id="Datos de la comanda">
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Fecha y hora</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->fecha_comanda ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Modalidad</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->modalidad_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Nro Comensales.</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->nro_comensales ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Canal</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->canal_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Mesa</label>
                                <input type="text" name="mesa" id="mesa" class="form-control input-sm center" value="<?= $oComanda->mesa ?>" readonly >
                                <input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm hidden" value="<?= $oComanda->mesa_id ?>" readonly >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Mozo</label>
                                <input type="text" name="mozo" id="mozo" class="form-control input-sm center" value="<?= strtoupper($oComanda->mozo) ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Atención</label><br>
                                <?php if ($oComanda->estado_atencion == 'E'): ?>
                                    <span class="label label-warning btn-block"> En espera </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'A'): ?>
                                    <span class="label label-primary btn-block"> Atendido </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'F'): ?>
                                    <span class="label label-success btn-block"> Finalizado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Pago</label><br>
                                <?php if ($oComanda->estado_pago == 'P'): ?>
                                    <span class="label label-danger btn-block"> Pendiente </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'C'): ?>
                                    <span class="label label-success btn-block"> Pagado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Items detalle -->
                    <div id="productos_seleccionados" style="overflow-x: scroll;">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center; width: 5%"> Cód. </th>
                                            <th style="text-align: center; width: 30%"> Producto </th>
                                            <!-- <th style="text-align: center; width: 10%"> Hora </th> -->
                                            <th style="text-align: center; width: 10%"> Espera </th>
                                            <th style="text-align: center; width: 12%"> P. Unit. </th>
                                            <th style="text-align: center; width: 8%"> Cantidad </th>
                                            <th style="text-align: center; width: 10%"> Importe </th>
                                            <th style="text-align: center; width: 15%"> Estado </th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php foreach ($oComanda->items as $key => $item): ?>
                                            <?php 
                                                $importe_total += $item->precio_unitario * $item->cantidad;
                                                $importe = number_format(round($item->precio_unitario * $item->cantidad,2), 2, '.', ' '); 
                                            ?>
                                            <tr>
                                                <td style="text-align: center" title="Código del detalle">
                                                    <?= $item->comanda_item_id ?> 
                                                </td>
                                                <td style="text-align: left"> 
                                                    <?= strtoupper($item->producto) ?> 
                                                    <?php if ($item->comentario != ""): ?>
                                                        <div class="label-warning comentario_pedido">
                                                            <?= strtoupper($item->comentario) ?> 
                                                        </div>
                                                    <?php endif ?>
                                                </td>
                                                <td style="text-align: center">
                                                    <!-- Obtenemos el tiempo de espera o lo calculamos -->
                                                    <?php if ($item->tiempo_espera): ?>
                                                        <?php $minutos = $item->tiempo_espera ?>
                                                    <?php else: ?>
                                                        <?php $minutos = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") ); ?>
                                                    <?php endif ?>
                                                    <!-- Mostrarmos los minutos calculados  -->
                                                    <?php if ($minutos < 15): ?>
                                                        <?= $minutos.' min' ?>
                                                    <?php endif ?>
                                                    <?php if ($minutos >= 15 && $minutos < 30): ?>
                                                        <span class="label label-warning"> <?= $minutos.' min' ?> </span>
                                                    <?php endif ?>
                                                    <?php if ($minutos >= 30): ?>
                                                        <span class="label label-danger"> <?= $minutos.' min' ?> </span>
                                                    <?php endif ?>
                                                </td>
                                                <td style="text-align: right;padding-right: 15px!important;vertical-align: middle;">
                                                    <?= number_format(round($item->precio_unitario,2), 2, '.', ' ');  ?>
                                                </td>
                                                <td style="text-align: center"> 
                                                    <?= $item->cantidad ?> 
                                                </td>
                                                <td style="text-align: right;padding-right: 15px!important;vertical-align: middle;">
                                                   <?= $importe ?>
                                                </td>
                                                <td style="vertical-align: middle;text-align: center">
                                                    <?php if ($item->estado_atencion == 'E'): ?>
                                                        <span class="label label-warning"> En espera </span>
                                                    <?php endif ?>
                                                    <?php if ($item->estado_atencion == 'P'): ?>
                                                        <span class="label label-primary"> En preparación </span>
                                                    <?php endif ?>
                                                    <?php if ($item->estado_atencion == 'D'): ?>
                                                        <span class="label label-success"> Despachado </span>
                                                    <?php endif ?>
                                                    <?php if ($item->estado_atencion == 'X'): ?>
                                                        <span class="label label-inverse"> Anulado </span>
                                                    <?php endif ?>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-3">
                            
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-9">
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Importe Sin IGV
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="importe_sin_IGV" id="importe_sin_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->importe_sin_IGV ?>" readonly>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Total IGV (<?= $porcentaje_igv ?>%)
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="total_IGV" id="total_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->total_IGV ?>" readonly>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 16px; color: #000; ">
                                        <strong>Total</strong>
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="importe_total" id="importe_total" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->importe_total ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($oPago->pago_id != 0): ?>
                        <br>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-8">
                                <a class="btn btn-primary btn-sm btn-block" href="javascript:;" onclick="imprimir_ticket(<?= $oPago->pago_id ?>)" >
                                    <i class="ace-icon fa fa-print bigger-90" aria-hidden="true"></i>
                                    Imprimir ticket
                                </a>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Nuevo pago -->
    <div class="col-sm-5 form-horizontal">

        <div class="widget-box widget-color-granate"> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-money bigger-90" aria-hidden="true"></i>
                    Detalle del pago
                </h6>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                     <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group hidden">
                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label>Comanda id</label>
                                        <input type="text" style="text-align:center" name="comanda_id" id="comanda_id" class="form-control input-sm" value="<?= $oComanda->comanda_id ?>" >
                                    </div>
                                </div>
                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label>pago id</label>
                                        <input type="text" style="text-align:center" name="pago_id" id="pago_id" class="form-control input-sm" value="<?= $oPago->pago_id ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-3 control-label" style=""> Fecha - Cajero </label>
                                <div class="col-sm-9">
                                    <input type="text" name="" id="" class="form-control input-sm" value="<?= $this->Site->fecha_yyyymmdd_a_ddmmyyyy_hms($oPago->fecha_pago) ?> - <?= strtoupper($oPago->cajero) ?> " readonly >
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-3 control-label" style=""> Comprobante </label>
                                <div class="col-sm-9">
                                    <select class="form-control input-sm" name="tipo_comprobante" id="tipo_comprobante" onchange="ver_serie(this.value);">  
                                            <option value="TCK" selected="">TICKET</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"> Serie </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control input-sm " name="serie" id="serie" value="<?= $oPago->serie.' - '.$oPago->correlativo ?>" readonly>
                                </div>
                            </div>
                            <div class="row " id="datos_cliente">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Documento cliente </label>
                                        <div class="col-sm-5">
                                            <input type="number" class="form-control input-sm" name="cliente_documento" id="cliente_documento" value="<?= $oPago->cliente_documento ?>"  maxlength="11" placeholder="DNI o RUC">
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="" style="" id="btn_consultar">
                                                <button type="button" class="btn btn-primary btn-xs btn-block" onclick="consultar_doc();" id="btn_btn_consultar">
                                                    <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
                                                    Consultar
                                                </button>
                                            </div>
                                            <div class="" style="display: none" id="btn_editar" >
                                                <button type="button" class="btn btn-primary btn-xs btn-block" onclick="limpiar_datos_consulta();" >
                                                    <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                                                    Editar 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="text-decoration: underline"> 
                                            <span class="" data-rel="tooltip" data-placement="top" title="Datos del cliente" >
                                                <a href="Javascript:;">Cliente</a> 
                                            </span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" name="cliente_nombre" id="cliente_nombre" value="<?= $oPago->cliente_nombre ?>" placeholder="Nombres o Razón social">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Teléfono </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" name="cliente_telefono" id="cliente_telefono" value="<?= $oPago->cliente_telefono ?>" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Dirección </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" name="cliente_direccion" id="cliente_direccion" value="<?= $oPago->cliente_direccion ?>" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="input_descuento" style="<?php if($oPago->descuento == "0.00") echo "display: none" ?>">
                                <label class="col-sm-3 control-label" style=""> <strong>Descuento</strong> </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm dinero-total" name="descuento" id="descuento" 
                                        value="<?= $oPago->descuento ?>"
                                        onkeypress="return soloNumeroDecimal(event)"
                                        onchange="aplicar_descuento();" 
                                        onclick="if(this.value == '0.00') this.value = ''" 
                                        onblur="if(this.value == '') this.value = '0.00'"
                                        style="font-size: 18px!important;text-align:center;font-weight: bold;"
                                        >
                                </div>
                            </div>
                            <div class="form-group" id="opcion_descuento" style="<?php if($oPago->descuento != "0.00") echo "display: none" ?>">
                                <div class="pull-right" style="padding-right: 25px;">
                                    <a href="javascript:;" onclick="habilitar_descuento();">Aplicar descuento</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style=""> <strong>Total a pagar</strong> </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm dinero-total" name="total_a_pagar" id="total_a_pagar"  value="<?= $oPago->total_a_pagar ?>" style="font-size: 18px!important;text-align:center;font-weight: bold;" readonly>
                                </div>
                            </div>
                            <div class="form-group" id="input_tarjeta" style="<?php if($oPago->pago_tarjeta == "0.00") echo "display: none" ?>">
                                <label class="col-sm-3 control-label" style=""> <strong>Tarjeta</strong> </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm dinero-total" name="pago_tarjeta" id="pago_tarjeta" 
                                        value="<?= $oPago->pago_tarjeta ?>" 
                                        onkeypress="return soloNumeroDecimal(event)" 
                                        onchange="ingresar_pago_tarjeta();"
                                        onclick="if(this.value == '0.00') this.value = ''" 
                                        onblur="if(this.value == '') this.value = '0.00'"
                                        style="font-size: 18px!important;text-align:center;font-weight: bold;">
                                </div>
                            </div>
                            <div class="form-group pull-right" id="opcion_tarjeta" style="<?php if($oPago->pago_tarjeta != "0.00") echo "display: none" ?>">
                                <div class="pull-right" style="padding-right: 25px;">
                                    <a href="javascript:;" onclick="habilitar_tarjeta();">Pagar con tarjeta</a>    
                                </div>
                            </div>
                            <!-- EFECTIVO -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style=""> <strong>EFECTIVO</strong> </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm dinero-total" name="efectivo" id="efectivo"  
                                        value="<?= $oPago->efectivo ?>"
                                        onkeypress="return soloNumeroDecimal(event)" 
                                        onchange="calcular_vuelto();" 
                                        onclick="if(this.value == '0.00') this.value = ''" 
                                        onblur="if(this.value == '') this.value = '0.00'"
                                        style="font-size: 18px!important;text-align:center;font-weight: bold;"
                                        >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style=""> <strong>VUELTO</strong> </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm dinero-total" name="vuelto" id="vuelto" value="<?= $oPago->vuelto ?>" style="font-size: 18px!important;text-align:center;font-weight: bold;" readonly>
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <label class="col-sm-3 control-label" style=""> <strong>Saldo</strong> </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control input-sm dinero-total" name="saldo" id="saldo"  value="<?= $oPago->saldo ?>" onkeypress="return soloNumeroDecimal(event)" style="font-size: 18px!important;text-align:center;font-weight: bold;" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <textarea class="form-control input-sm" id="observaciones" name="observaciones" placeholder="Ingresar alguna observación o comentario." rows="2" ><?= $oPago->observaciones ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($oComanda->estado_pago == 'P'): ?>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-8 right" style="text-align: right;">
                                <button class="btn btn-app btn-primary btn-md" id="btn_guardar_pago" >
                                    <i class="ace-icon fa fa-save bigger-160"></i>
                                    Pagar
                                </button>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>    
</div>
<?= form_close(); ?>

<script type="text/javascript">
    var gb_numeraciones = '<?= json_encode($numeraciones) ?>'; //Variable global  de produtcos
    var gb_array_numeraciones = JSON.parse(gb_numeraciones); //Pasamos los productos a una variable de javascript array
</script>
<script type="text/javascript">
    // mostramos el numero de serie y correlativo
    <?php if ($oPago->pago_id == 0): ?>
        ver_serie('<?= $oPago->tipo_comprobante ?>');
    <?php else: ?>
        $('#tipo_comprobante').attr('disabled',true);
        $('#cliente_nombre').attr('disabled',true);
        $('#cliente_documento').attr('disabled',true);
        $('#cliente_telefono').attr('disabled',true);
        $('#cliente_direccion').attr('disabled',true);
        $('#efectivo').attr('disabled',true);
        $('#observaciones').attr('disabled',true);
        $('#btn_btn_consultar').attr('disabled',true);

        $('#descuento').attr('disabled',true);
        $('#pago_tarjeta').attr('disabled',true);
    <?php endif ?>
    function imprimir_ticket( pago_id){
        url = '<?= base_url("impresion/ticketera/imprimir_ticket_pago/") ?><?= $oPago->pago_id ?>';
        $.ajax({
            type: 'POST',
            url: url,
            data: "pago_id= " + pago_id,
            success: function(rpta){
                var pdf_link = "<?=base_url()?>reportes_pdf/comprobante.pdf";
                var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="100%">No Support</object>';
                $.createModal({
                    title: "",
                    message: iframe,
                    closeButton:true,
                    scrollable:false
                });
            },
            error: function(rpta){
                alert("Error al cargar la lista "+rpta);
            }
        });
        return false;
    }
    function ver_serie(tipo_comprobante){
        if (tipo_comprobante == "O") {
            $('#serie').val("");
            return false;
        }
        $.each(gb_array_numeraciones, function () {
            if (this.tipo_comprobante == tipo_comprobante ) {
                serie = "" + this.serie + ' - ' + this.correlativo;
                $('#serie').val(serie);
            }
        });
    }
    // al presionar el boton guardar validamos que el saldo esté en "0"
    $("#form_pago").submit(function() {
        $('#btn_guardar_pago').attr('disabled', true);
        var saldo = $('#saldo').val();
        var importe_total = $('#importe_total').val();
        if(importe_total == 0){
            alertify.error("La comanda no tiene un monto a pagar.");
            $('#btn_guardar_pago').attr('disabled', false);
            return false;
        }
        //Verifica si se paga la totalidad
        if(saldo == 0){
            $('#btn_guardar_pago').attr('disabled', true);
            return true;
        }else{
            alertify.error("Debe pagar la totalidad.");
            $('#btn_guardar_pago').attr('disabled', false);
            return false;
        }
    });
    function desplegar_datos_cliente(){
        if($('#datos_cliente').hasClass('hidden')){
            $('#datos_cliente').removeClass('hidden');
        }else{
            $('#datos_cliente').addClass('hidden');
        }
    }
    function habilitar_descuento(){
        swal({
            title: "¿Seguro de anular la comanda?",
            text: "Si está seguro, ingrese el código de seguridad",
            type: "input",
            inputType: "password",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Código de seguridad"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Es obligatorio ingresar el código!");
                return false
            }
            verificar_codigo_de_seguridad(inputValue);
        });
    }
    function verificar_codigo_de_seguridad(codigo){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/generalidades/verificar_codigo_de_seguridad')?>",
            data: {'codigo': codigo},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    if (rpta == 1) {
                        $('#input_descuento').show('slow');
                        $('#opcion_descuento').hide('slow');
                        swal.close()
                    }else{
                         alertify.error("El código no es válido");
                    }
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function habilitar_tarjeta(){
        $('#input_tarjeta').show('slow');
        $('#opcion_tarjeta').hide('slow');
    }
    // 
    function calcular_saldos(){
        // Calcula los campos totales como importe total, saldo
        var importe_total = parseFloat('<?= $importe_total ?>').toFixed(2);
        var descuento = parseFloat($('#descuento').val()).toFixed(2);
        var total_a_pagar = parseFloat(importe_total - descuento).toFixed(2);
        var tarjeta = parseFloat($('#pago_tarjeta').val()).toFixed(2);
        var efectivo = parseFloat($('#efectivo').val()).toFixed(2);
        var saldo = parseFloat(total_a_pagar - tarjeta - efectivo ).toFixed(2);
        if(saldo < 0 ){
            //HAY VUELTO, lo mas probable.
            saldo = "0.00";
        }
        $('#saldo').val(saldo);
    }
    function aplicar_descuento(){
        var importe_total = parseFloat('<?= $importe_total ?>').toFixed(2);
        var descuento = parseFloat($('#descuento').val()).toFixed(2);

        if(isNaN(descuento)){
            alertify.error("Debe ingresar un número válido.");
            $('#descuento').val("0.00");
        }else{
            $('#descuento').val(descuento);
            var total_a_pagar = parseFloat(importe_total - descuento).toFixed(2);
            if(total_a_pagar < 0){
                // Se descontó más del total a pagar.
                alertify.error("Descuento no válido.");
                $('#descuento').val("0.00");
                $('#total_a_pagar').val(importe_total);
            }else{
                $('#total_a_pagar').val(total_a_pagar);
            }
        }
        calcular_saldos();
    }
    function calcular_vuelto(){ //ingreso pago en efectivo
        var importe_total = parseFloat('<?= $importe_total ?>').toFixed(2);
        var descuento = parseFloat($('#descuento').val()).toFixed(2);
        var tarjeta = parseFloat($('#pago_tarjeta').val()).toFixed(2);
        var efectivo = parseFloat($('#efectivo').val()).toFixed(2);
        var total_a_pagar_efectivo = parseFloat(importe_total - descuento - tarjeta).toFixed(2);
        
        var vuelto = parseFloat(efectivo - total_a_pagar_efectivo).toFixed(2);
        if(isNaN(vuelto)){
            alertify.error('Valores no válidos, verifique los valores ingresados.');
            return false;
        }
        if(vuelto < 0){
            alertify.error("Debe pagar la totalidad.");            
        }else{
            if (vuelto > 0){ //indicar que debe dar vuelto
                $('#vuelto').parent().addClass("has-warning");
            }
            saldo = "0.00";
        }
        $('#efectivo').val(efectivo); // con el formato decimal
        $('#vuelto').val(vuelto);
        calcular_saldos();
    }
    function ingresar_pago_tarjeta(){
        var importe_total = parseFloat('<?= $importe_total ?>').toFixed(2);
        var descuento = parseFloat($('#descuento').val()).toFixed(2);
        var efectivo = parseFloat($('#efectivo').val()).toFixed(2);
        var tarjeta = parseFloat($('#pago_tarjeta').val()).toFixed(2);
        var total_a_pagar = parseFloat(importe_total - descuento).toFixed(2);

        if(isNaN(tarjeta)){
            alertify.error("Debe ingresar un número válido.");
            $('#pago_tarjeta').val("0.00");
        }else{
            $('#pago_tarjeta').val(tarjeta);
          
        }
        calcular_vuelto();
    }
    
    // 
    function consultar_doc(){
        var documento = $('#cliente_documento').val();
        documento = documento.trim();
        if(documento.length < 5){
            alertify.error("Debe ingresar un número de documento válido.");
            return false;
        }
        var url = '<?= base_url('administracion/clientes/json_buscar_por_documento/')?>' + documento;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(objeto['success']){
                    var sunat_tipo_documento_id = objeto['data'].sunat_tipo_documento_id;
                    var cliente_id  = objeto['data'].cliente_id;
                    var documento   = objeto['data'].documento;
                    var cliente     = objeto['data'].cliente;
                    var nombres     = objeto['data'].nombres;
                    var ape_paterno = objeto['data'].ape_paterno;
                    var ape_materno = objeto['data'].ape_materno;
                    var telefono    = objeto['data'].telefono;
                    var correo      = objeto['data'].correo;
                    var direccion   = objeto['data'].direccion;
                    var ubigeo      = objeto['data'].ubigeo;
                    var direccion_ubigeo = (ubigeo == "") ? direccion : direccion + " - " + ubigeo;

                    $('#cliente_id').val(cliente_id);
                    $('#cliente_nombre').val(cliente);
                    $('#cliente_telefono').val(telefono);
                    $('#cliente_direccion').val(direccion_ubigeo);

                    $('#btn_consultar').hide();
                    $('#btn_editar').show();
                    $('#cliente_documento').attr('readonly', true);
                    $('#cliente_nombre').attr('readonly', true);
                }else{
                    alertify.error(objeto['msg']);
                }
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function limpiar_datos_consulta(){
        // $('#documento').val("");
        $('#cliente_id').val("");
        $('#cliente_documento').val("");
        $('#cliente_nombre').val("");
        $('#cliente_telefono').val("");
        $('#cliente_direccion').val("");

        $('#cliente_documento').attr('readonly', false);
        $('#cliente_nombre').attr('readonly', false);
        $('#cliente_telefono').attr('readonly', false);
        $('#cliente_direccion').attr('readonly', false);

        $('#btn_consultar').show();
        $('#btn_editar').hide();
    }
    // $('#descuento').mask('000,000,000.00', { reverse: true });
</script>
<script type="text/javascript">
    $("#ir_atras").removeClass("hidden");
</script>
