<!-- Tabla de comandas por pagar -->
<table class="table table-bordered table-striped" width="100%" id="tabla-pendientes">
    <thead>
        <tr>
            <th style="text-align: center; width: 5%"> Cód .</th>
            <th style="text-align: center; width: 10%"> Mesa </th>
            <th style="text-align: center; width: 10%"> Mozo </th>
            <th style="text-align: center; width: 10%"> Tiempo </th>
            <th style="text-align: center; width: 25%"> Cliente </th>
            <th style="text-align: center; width: 10%"> Importe </th>
            <th style="text-align: center; width: 10%"> Atencion </th>
            <th style="text-align: center; width: 10%"> Pago </th>
            <th style="text-align: center; width: 10%">  </th>
        </tr>
    </thead>
    <tbody>
        <?php $cant_por_pagar = 0; ?>
        <?php foreach ($comandas as $key => $comanda): ?>
            <!-- Todas las que están pendientes -->
            <?php if ($comanda->estado_atencion == 'X' || $comanda->estado_pago == 'X' ): ?>
                <?php $cant_por_pagar++; ?>
                <tr>
                    <td style="text-align: center"> <?= $comanda->comanda_id ?> </td>
                    <td style="text-align: center;"> 
                        <?php if ($comanda->modalidad == "PRE"): ?>
                            <?= $comanda->mesa ?> 
                        <?php endif ?>
                        <?php if ($comanda->modalidad == "DEL"): ?>
                            DELIVERY
                        <?php endif ?>
                        <?php if ($comanda->modalidad == "LLE"): ?>
                            PARA LLEVAR
                        <?php endif ?>
                    </td>
                    <td style="text-align: center"> <?= $comanda->mozo ?> </td>
                    <td style="text-align: center">
                        <?php if ($comanda->estado_atencion == 'F'): ?>
                            <?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda,$comanda->finalizada_en ); ?>
                        <?php else: ?>
                            <?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda,date("Y-m-d H:i:s") ); ?>
                        <?php endif ?>
                         <?= $minutos.' min' ?>
                    </td>
                    <td style="text-align: center"> <?= $comanda->cliente_nombre ?></td>
                    <td style="text-align: right;" class="dinero"> <?= $comanda->importe_total ?></td>
                    <td style="text-align: center">
                        <?php if ($comanda->estado_atencion == 'E'): ?>
                            <span class="label label-warning"> En espera </span>
                        <?php endif ?>
                        <?php if ($comanda->estado_atencion == 'A'): ?>
                            <span class="label label-primary"> Atendido </span>
                        <?php endif ?>
                        <?php if ($comanda->estado_atencion == 'F'): ?>
                            <span class="label label-success"> Finalizado </span>
                        <?php endif ?>
                        <?php if ($comanda->estado_atencion == 'X'): ?>
                            <span class="label label-inverse"> Anulado </span>
                        <?php endif ?>
                    </td>
                    <td style="text-align: center">
                        <?php if ($comanda->estado_pago == 'P'): ?>
                            <span class="label label-danger"> Pendiente </span>
                        <?php endif ?>
                        <?php if ($comanda->estado_pago == 'C'): ?>
                            <span class="label label-success"> Pagado </span>
                        <?php endif ?>
                        <?php if ($comanda->estado_pago == 'X'): ?>
                            <span class="label label-inverse"> Anulado </span>
                        <?php endif ?>
                    </td>
                    <td style="text-align: center">
                        <?php $url_pagar = base_url('salon/comanda/ver/').$comanda->comanda_id ?>
                        <a  href="<?= $url_pagar ?>" class="btn btn-xs btn-primary btn-block">
                            <i class="ace-icon fa fa-dollar-sign bigger-90" aria-hidden="true"></i>
                            Ver detalle
                        </a>
                    </td>
                </tr>
            <?php endif ?>
        <?php endforeach ?>
    </tbody>
</table>
<div>
    Nro de pedidos pendientes: <?= $cant_por_pagar ?>
</div>
