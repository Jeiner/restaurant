<?php 
	$docs_pendientes = 0;
	$docs_correctos = 0;
	$docs_errores = 0;

	$problemas = 0;
?>
<?php foreach ($documentos_sunat as $key => $doc): ?>
	<?php
		switch ($doc->IND_SITU) {
			case '01':
			case '02':
			case '07':
			case '08':
			case '09':
				$docs_pendientes++;
				break;
			case '03':
			case '04':
			case '11':
			case '12':
				$docs_correctos++;
				break;
			case '05':
			case '06':
			case '10':
				$docs_errores++;
				break;
			default:
				$problemas++;
				break;
		}
	?>	
<?php endforeach ?>
<?php if ($problemas > 0): ?>
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-danger" style="font-size:13px;padding: 5px">
            <strong>
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                Se encontraron <?= $problemas ?> errores, porfavor comunicarse con el Administrador del Sistema.
            </strong>
            <br>
        </div>
	</div>
</div>
<?php endif ?>
<div class="row">
	<div class="col-sm-4">
		<div class="alert alert-danger" style="font-size:13px;padding: 5px">
            <strong>
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                <?= $docs_errores ?>
                documentos con ERRORES
            </strong>
            <br>
        </div>
	</div>
	<div class="col-sm-4">
		<div class="alert alert-warning" style="font-size:13px;padding: 5px">
            <strong>
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                <?= $docs_pendientes ?>
                documentos pendientes
            </strong>
            <br>
        </div>
	</div>
</div>
<div class="row">
	<iframe width="100%" height="900" src="http://localhost:9000/#" frameborder="0" allowfullscreen></iframe>
</div>
