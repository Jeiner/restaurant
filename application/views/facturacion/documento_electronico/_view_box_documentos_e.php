<?php  
    $porcentaje_igv = $igv * 100;
?>
<div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
    <div class="widget-header">
        <h6 class="widget-title">
            <i class="ace-icon fa fa-file bigger-90" aria-hidden="true"></i>
            Comprobantes electrónicos (<?= count($oCpbeCabecera) ?> ítems)
        </h6>
        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="ace-icon fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body" style="display: block;">
        <div class="widget-main padding-4" style="position: relative; padding: 15px">
            <?php if (count($oCpbeCabecera) > 0): ?>
                <?php foreach ($oCpbeCabecera as $key => $cab): ?>
                    <?php if ($key != 0): ?>
                        <hr><br>
                    <?php endif ?>
                    <div class="row" >
                        <div class="col-sm-12 center">
                            <strong><?= $cab->sunat_tipo_doc_e ?></strong>
                            <br>
                            <?= $cab->serie ?> - <?= $cab->correlativo ?>
                            <br>
                        </div>
                        <div class="col-sm-12 left">                                
                            <strong>Fecha Emisión:</strong> <?= $cab->fecEmision.' '.$cab->horEmision ?>
                            <br>
                            <strong>Cliente:</strong> <?= $cab->rznSocialUsuario ?>
                            <br>
                            <strong>Documento:</strong> <?= $cab->numDocUsuario ?>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 left">
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width: 55%">Producto</th>
                                        <th style="width: 15%">Cantidad</th>
                                        <th style="width: 15%">P. Unit</th>
                                        <th style="width: 15%">Importe</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($oCpbeDetalle as $key => $det): ?>
                                        <?php if ($det->cabecera_id == $cab->cabecera_id): ?>
                                            <tr>
                                                <td class="left"> <?= $det->desItem ?></td>
                                                <td class="center"> <?= $det->ctdUnidadItem ?></td>
                                                <td style="text-align: right">
                                                    <?php 
                                                        $precioUnitario = ($det->importe / $det->ctdUnidadItem);
                                                    ?>
                                                    <?= number_format($precioUnitario, 2) ?>
                                                </td>
                                                <td style="text-align: right"> <?= $det->importe ?></td>
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Total IGV (<?= $porcentaje_igv ?>%)
                                    </label>
                                    <input type="text" class="form-control" name="" id="" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $cab->sumTotTributos ?>" readonly>
                                </div>
                                <div class="col-sm-4">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Importe Sin IGV
                                    </label>
                                    <input type="text" class="form-control" name="" id="" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $cab->sumTotValVenta ?>" readonly>
                                </div>
                                <div class="col-sm-4">
                                    <label class="" style="font-size: 16px; color: #000; ">
                                        <strong>Total</strong>
                                    </label>
                                    <input type="text" class="form-control" name="" id="" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $cab->sumPrecioVenta ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                           <button type="button" class="btn btn-granate btn-sm btn-block" onclick="imprimir_comprobante_e('<?= $cab->cabecera_id ?>');">
                                <i class="ace-icon fa fa-print bigger-90" aria-hidden="true"></i>
                                Imprimir comprobante
                            </button>
                        </div>
                        <div class="col-sm-6">
                           <button type="button"  id="generar_pdf" class="btn btn-granate btn-sm btn-block" onclick="generar_comprobante_pdf('<?= $cab->cabecera_id ?>', '<?= $cab->serie ?>', '<?= $cab->correlativo ?>');">
                                <i class="ace-icon fa fa-file-pdf-o bigger-90" aria-hidden="true"></i>
                                Ver en PDF
                            </button>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-sm-6">
                           <button type="button"
                                    id="generar_pdf"
                                    class="btn btn-danger btn-sm btn-block" 
                                    onclick="abrir_modal_anular_cpbe('<?= $cab->cabecera_id ?>', '<?= $cab->serie ?>', '<?= $cab->correlativo ?>');"
                                    >
                                <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                                Anular comprobante
                            </button>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php else: ?>
                <div class="row">
                    <div class="col-sm-12">
                        El pedido aún no tiene comprobantes electrónicos.
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<!-- Modales anular cpbe -->
<div id="div_modal_anular_cpbe">
    <?= form_open(base_url('facturacion/documento_electronico/anular_cpbe'), 'class="form-horizontal" id="form_anular_cpbe"'); ?>
        <div id="modal_anular_cpbe" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="smaller lighter blue no-margin center">
                            <strong>
                                Anular comprobante electrónico
                            <br>
                            <span id="ver_anular_cpbe_serie"> </span> - <span id="ver_anular_cpbe_correlativo"> </span>
                            </strong>
                        </h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">

                                <div class="form-group hidden" >
                                    <label>cabecera_id</label>
                                    <input type="text" name="anular_cpbe_cabecera_id" id="anular_cpbe_cabecera_id">
                                </div>
                                <div class="form-group hidden" >
                                    <label>serie</label>
                                    <input type="text" name="anular_cpbe_serie" id="anular_cpbe_serie">
                                </div>
                                <div class="form-group hidden" >
                                    <label>correlativo</label>
                                    <input type="text" name="anular_cpbe_correlativo" id="anular_cpbe_correlativo">
                                </div>
                                <div class="form-group">
                                    <label> Motivo / Descripción <label class="red">*</label> </label>
                                    <textarea class="form-control input-sm" rows="4" name="anular_cpbe_motivo" id="anular_cpbe_motivo" placeholder="Ingrese una descripción indicando el motivo por el cual se anulará el comprobante."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <em>
                                    Para poder anular el comprobante electrónico, debe tener máximo 3 días de generado.
                                </em>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-sm" id="btn_anular_comprobante" >
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                            Anular
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript">
    function imprimir_comprobante_e(cabecera_id){
        var ruta_ticket = "<?= $this->Site->get_ruta_proy_ticket() ?>";
        url_ticket = ruta_ticket + "imprimir_comprobante_e.php?cabecera_id=" + cabecera_id + "";
        window.open(url_ticket,'_blank', 'width=1000,height=700');
    }

    function generar_comprobante_pdf(cabecera_id,serie,correlativo){
        $("#generar_pdf").prop( "disabled", true);
        //Generar PDF
        var url = "<?=base_url('facturacion/documento_electronico/ajax_generar_cbe_pdf/')?>" + cabecera_id;
        $.ajax({
            type: 'GET',
            url: url,
            data: {},
            success: function(rpta){
                ver_comprobante_pdf(cabecera_id,serie,correlativo);
                $("#generar_pdf").prop( "disabled", false);
            },
            error: function(rpta){
                alert("Error en la operación");
                $("#generar_pdf").prop( "disabled", false);
            }
        });
        $("#generar_pdf").prop( "disabled", false);
    }

    function ver_comprobante_pdf(cabecera_id,serie,correlativo){
        var pdf_link = "<?=base_url()?>" + "reportes_pdf/" + "<?= NOMBRE_SISTEMA ?>" +"_CPBE_" + serie + "-" + correlativo + ".pdf";
        var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="100%">No Support</object>';
        $.createModal({
            title: "",
            message: iframe,
            closeButton: true,
            scrollable: false
            //fichaLabID: fichaLabID
        });
    }

    function abrir_modal_anular_cpbe(cabecera_id,serie,correlativo){
        
        $("#ver_anular_cpbe_serie").html(serie);
        $("#ver_anular_cpbe_correlativo").html(correlativo);

        $("#anular_cpbe_cabecera_id").val(cabecera_id);
        $("#anular_cpbe_serie").val(serie);
        $("#anular_cpbe_correlativo").val(correlativo);

        swal({
            title: "¿Seguro de anular el comprobante electrónico?",
            text: "Si está seguro, ingrese el código de seguridad",
            type: "input",
            inputType: "password",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Código de seguridad"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Es obligatorio ingresar el código!");
                return false
            }
            verificar_codigo_de_seguridad_para_anular_cpbe(inputValue);
        });
    }
    function verificar_codigo_de_seguridad_para_anular_cpbe(codigo){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/generalidades/verificar_codigo_de_seguridad')?>",
            data: {'codigo': codigo},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    if (rpta == 1) {
                        $('#modal_anular_cpbe').modal({
                            show: 'false'
                        }); 
                        swal.close()
                    }else{
                         alertify.error("El código de seguridad no es válido");
                    }
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>