<?php  
    $importe_total = 0;
    $dec_importe_total = 0;
    $porcentaje_igv = $igv * 100;
?>

<!-- Titulo -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-6">
                    <h1 style="font-weight: 500">
                        <i class="ace-icon fa fa-file bigger-90" aria-hidden="true"></i>
                        Generar comprobante electrónico
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Código comanda: <?= $oComanda->comanda_id ?>
                        </small>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">

<?= form_open(base_url('facturacion/documento_electronico/generar_documento'), '" id="form_generar_comprobante_e"'); ?>
    <!-- Datos de la comanda y el pago-->
    <div class="col-sm-7">
        <div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-shopping-cart bigger-90" aria-hidden="true"></i>
                    Detalle de la comanda
                </h6>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                    <!-- Datos generales de comanda -->
                    <div class="row" id="Datos de la comanda">
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Fecha y hora</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->fecha_comanda ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Modalidad</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->modalidad_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Nro Comensales.</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->nro_comensales ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Canal</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->canal_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Mesa</label>
                                <input type="text" name="mesa" id="mesa" class="form-control input-sm center" value="<?= $oComanda->mesa ?>" readonly >
                                <input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm hidden" value="<?= $oComanda->mesa_id ?>" readonly >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Mozo</label>
                                <input type="text" name="mozo" id="mozo" class="form-control input-sm center" value="<?= strtoupper($oComanda->mozo) ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Atención</label><br>
                                <?php if ($oComanda->estado_atencion == 'E'): ?>
                                    <span class="label label-warning btn-block"> En espera </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'A'): ?>
                                    <span class="label label-primary btn-block"> Atendido </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'F'): ?>
                                    <span class="label label-success btn-block"> Finalizado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Pago</label><br>
                                <?php if ($oComanda->estado_pago == 'P'): ?>
                                    <span class="label label-danger btn-block"> Pendiente </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'C'): ?>
                                    <span class="label label-success btn-block"> Pagado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Items detalle -->
                    <div id="productos_seleccionados" style="overflow-x: scroll;">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center; width: 5%"> Cód. </th>
                                            <th style="text-align: center; width: 30%"> Producto </th>
                                            <th style="text-align: center; width: 12%"> P. unit. </th>
                                            <th style="text-align: center; width: 8%"> Cantidad </th>
                                            <th style="text-align: center; width: 10%"> Importe </th>
                                            <th style="text-align: center; width: 15%"> Estado </th>
                                            <th style="text-align: center; width: 10%"> Declarar </th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php foreach ($oComanda->items as $key => $item): ?>
                                            <?php 
                                                $importe_total += $item->precio_unitario * $item->cantidad;
                                                $importe = number_format(round($item->precio_unitario * $item->cantidad,2), 2, '.', ' '); 
                                            ?>
                                            <tr>
                                                <td style="text-align: center"> <?= $item->comanda_item_id ?> </td>
                                                <td style="text-align: left"> 
                                                    <?= $item->producto ?>
                                                </td>
                                                <td style="text-align: right;padding-right: 15px!important;vertical-align: middle;">
                                                    <?= number_format(round($item->precio_unitario,2), 2, '.', ' ');  ?>
                                                </td>
                                                <td style="text-align: center"> <?= $item->cantidad ?> </td>
                                                <td style="text-align: right;padding-right: 15px!important;vertical-align: middle;">
                                                   <?= $importe ?>
                                                </td>
                                                <td style="vertical-align: middle;text-align: center">
                                                    <?php if ($item->estado_atencion == 'E'): ?>
                                                        <span class="label label-warning"> En espera </span>
                                                    <?php endif ?>
                                                    <?php if ($item->estado_atencion == 'P'): ?>
                                                        <span class="label label-primary"> En preparación </span>
                                                    <?php endif ?>
                                                    <?php if ($item->estado_atencion == 'D'): ?>
                                                        <span class="label label-success"> Despachado </span>
                                                    <?php endif ?>
                                                    <?php if ($item->estado_atencion == 'X'): ?>
                                                        <span class="label label-inverse"> Anulado </span>
                                                    <?php endif ?>
                                                </td>
                                                <td style="text-align: center">
                                                    <?php if ($item->estado_atencion != 'X'): ?>
                                                        <?php $declarado = false; ?>
                                                        <?php foreach ($oCpbeDetalle as $key => $cpbeDet): ?>
                                                            <?php if ($cpbeDet->comanda_item_id == $item->comanda_item_id): ?>
                                                                <?php $declarado = true; ?>
                                                            <?php endif ?>
                                                        <?php endforeach ?>
                                                        <?php if (!$declarado): ?>
                                                            <?php $dec_importe_total +=  $item->importe ?>
                                                        <?php endif ?>
                                                        <label class="pos-rel">
                                                                <input type="checkbox" class="ace" 
                                                                        readonly="readonly"
                                                                        <?php if ($declarado): ?>
                                                                            disabled="disabled"
                                                                        <?php endif ?>
                                                                        name="dec_seleccionados[]" 
                                                                        checked="checked" 
                                                                        value="<?= $item->comanda_item_id ?>" 
                                                                        onclick="dec_nuevo_total(this.checked, <?= $item->importe ?>);">
                                                                <span class="lbl"></span>
                                                        </label>
                                                    <?php endif ?>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php 
                            $importe_total = number_format(round($importe_total,2), 2, '.', ' ');
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-3">
                            
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-9">
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Importe Sin IGV
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="importe_sin_IGV" id="importe_sin_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->importe_sin_IGV ?>" readonly>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Total IGV (<?= $porcentaje_igv ?>%)
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="total_IGV" id="total_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->total_IGV ?>" readonly>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 16px; color: #000; ">
                                        <strong>Total</strong>
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="importe_total" id="importe_total" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->importe_total ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <textarea class="form-control input-sm" placeholder="" id="cmd_comentario" name="cmd_comentario" disabled="true"><?= $oComanda->cmd_comentario ?></textarea>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>

    <!-- Generar nuevo comprobante -->
    <div class="col-sm-5 form-horizontal">
        <!-- Emitir nuevo documento electronico -->
            <?php if ($oComanda->con_comprobante_e != "C"): ?>
            <div class="col-sm-12">
                <div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
                    <div class="widget-header">
                        <h6 class="widget-title">
                            <i class="ace-icon fa fa-file bigger-90" aria-hidden="true"></i>
                            Emitir nuevo Comprobante Electrónico
                        </h6>
                        <div class="widget-toolbar">
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body" style="display: block;">
                        <div class="widget-main padding-4" style="position: relative; padding: 15px">
                            <div class="row hidden">
                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label>Comanda id</label>
                                        <input type="text" style="text-align:center" name="comanda_id" id="comanda_id" class="form-control input-sm" value="<?= $oComanda->comanda_id ?>" >
                                    </div>
                                </div>
                                <div class="col-sm-3 ">
                                    <div class="form-group">
                                        <label>pago id</label>
                                        <input type="text" style="text-align:center" name="pago_id" id="pago_id" class="form-control input-sm" value="<?= $oPago->pago_id ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="col-sm-3 col-xs-4" align="center">
                                            <div class="radio">
                                                <label>
                                                    <input name="sunat_tipo_comprobante_id" id="check_boleta" value="03" type="radio" class="ace imput_cpbte"  onclick="ver_serie(this.value) ;cambiar_comprobante(this.value);" checked="" />
                                                    <span class="lbl"> BOLETA </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-xs-4" align="center">
                                            <div class="radio">
                                                <label>
                                                    <input name="sunat_tipo_comprobante_id" id="check_factura" value="01" type="radio" class="ace imput_cpbte" onclick="ver_serie(this.value); cambiar_comprobante(this.value);" />
                                                    <span class="lbl"> FACTURA </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label" style="text-align: left!important; margin-left: 30px; cursor: pointer;"> 
                                             <label class="pos-rel">
                                                    <input type="checkbox" class="ace"
                                                            id="por_consumo"
                                                            name="por_consumo"
                                                            value="1">
                                                    <span class="lbl"></span>
                                            </label>
                                            Emitir comprobante <strong>POR CONSUMO</strong>
                                        </label>
                                        <div class="col-sm-4">
                                           
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Serie - Número </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control input-sm " name="sunat_serie_numero" id="sunat_serie_numero" value="" readonly>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Tipo Doc. </label>
                                        <div class="col-sm-9">
                                            <select class="form-control input-sm" name="cliente_tipo_documento_id" id="cliente_tipo_documento_id">  
                                                <option value="">-- Seleccionar --</option>
                                                <?php foreach ($documento_identidad as $key => $tipo_doc): ?>
                                                    <?php 
                                                        $selected = "";
                                                        if ($oPago->tipo_comprobante == $tipo_doc->valor) {
                                                            $selected = "selected";
                                                        }
                                                    ?>
                                                    <option value="<?= $tipo_doc->valor ?>" <?= $selected ?> > 
                                                        <?= $tipo_doc->descripcion ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 hidden">
                                            <input type="text" class="" name="cliente_id" id="cliente_id" value="<?= $oPago->cliente_id ?>" >
                                        </div>
                                        <label class="col-sm-3 control-label"> N° Documento </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control input-sm" name="cliente_documento" id="cliente_documento" value="<?= $oComanda->cliente_documento ?>"  maxlength="11" placeholder="DNI o RUC" >
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="" style="" id="btn_consultar">
                                                <button type="button" class="btn btn-primary btn-xs btn-block" onclick="consultar_doc();" id="btn_btn_consultar">
                                                    <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
                                                    Consultar
                                                </button>
                                            </div>
                                            <div class="" style="display: none" id="btn_editar" >
                                                <button type="button" class="btn btn-primary btn-xs btn-block" onclick="limpiar_datos_consulta();" >
                                                    <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                                                    Editar 
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> 
                                            <span class="" data-rel="tooltip">
                                                <a href="Javascript:;">Cliente</a> 
                                            </span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" 
                                                    class="form-control input-sm" 
                                                    name="cliente_nombre_completo" 
                                                    id="cliente_nombre_completo" 
                                                    value="<?= $oComanda->cliente_nombre ?>" 
                                                    placeholder="Nombres o Razón social" >
                                        </div>
                                        <div id="datos_cliente" class="hidden">
                                            <input type="text" class="" name="cliente_nombres" id="cliente_nombres" value="">
                                            <input type="text" class="" name="cliente_ape_paterno" id="cliente_ape_paterno" value="">
                                            <input type="text" class="" name="cliente_ape_materno" id="cliente_ape_materno" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Teléfono </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" name="cliente_telefono" id="cliente_telefono" value="<?= $oComanda->cliente_telefono ?>"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Dirección </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control input-sm" name="cliente_direccion" id="cliente_direccion" ><?= $oComanda->cliente_direccion ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"> Correo </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-sm" name="cliente_correo" id="cliente_correo" value=""  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>



                            <div class="row">
                                <?php
                                            $parametro_sin_igv = floatval(1 + floatval($igv));
                                            $dec_importe_sin_IGV = round($dec_importe_total/$parametro_sin_igv,2);
                                            $dec_total_IGV = round(($dec_importe_total - $dec_importe_sin_IGV),2);
                                            $dec_importe_total = number_format(round($dec_importe_total,2), 2, '.', ' ');
                                        ?>
                                <div class="col-sm-6 col-md-6 col-xs-3">
                                    
                                </div>
                                <div class="col-sm-6 col-md-6 col-xs-9">
                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-sm-6 col-xs-6" style="">
                                            <label class="" style="font-size: 14px; color: #000; ">
                                                Importe Sin IGV
                                            </label>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" name="dec_importe_sin_IGV" id="dec_importe_sin_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $dec_importe_sin_IGV ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-sm-6 col-xs-6" style="">
                                            <label class="" style="font-size: 14px; color: #000; ">
                                                Total IGV (<?= $porcentaje_igv ?>%)
                                            </label>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" name="dec_total_IGV" id="dec_total_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $dec_total_IGV ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 5px">
                                        <div class="col-sm-6 col-xs-6" style="">
                                            <label class="" style="font-size: 16px; color: #000; ">
                                                <strong>Total</strong>
                                            </label>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" name="dec_importe_total" id="dec_importe_total" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $dec_importe_total ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-12" >
                                    <div class="form-group"style="padding-left: 10px;padding-right: 10px;">
                                        <?=  form_submit('btn_generar_comprobante_e', 'Generar Comprobante Electrónico', 'class="btn btn-primary btn-sm btn-block"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif ?>
    </div>

<?= form_close(); ?>

    <!-- Comprobantes generados -->
    <div class="col-sm-5 form-horizontal">
        <div class="row">
            <!-- Documentos electrónicos emitidos  -->
            <?php if (count($oCpbeCabecera) > 0): ?>
            <div class="col-sm-12 form-horizontal">
                <?php $this->view('facturacion/documento_electronico/_view_box_documentos_e'); ?>
            </div>
            <?php endif ?>
        </div>

        <div class="row">
            <!-- Documentos electrónicos BAJAS  -->
            <?php if (count($lstCpbeBajas) > 0): ?>
            <div class="col-sm-12 form-horizontal">
                <?php $this->view('facturacion/documento_electronico/_view_box_documentos_e_bajas'); ?>
            </div>
            <?php endif ?>
        </div>
    </div>

</div>


<div class="row">
    
</div>

<div class="row">
    <div class="col-sm-6">
        
        <!-- Datos del pago -->
        <?php $this->view('facturacion/pagos/_view_caja_datos_pago'); ?>
    </div>
</div>
<script type="text/javascript">
    var ruta_ticket = "<?= $this->Site->get_ruta_proy_ticket() ?>";
    var gb_numeraciones = '<?= json_encode($numeraciones) ?>'; //Variable global  de produtcos
    var gb_array_numeraciones = JSON.parse(gb_numeraciones); //Pasamos los productos a una variable de javascript array
    var gb_IGV = '<?= json_encode($igv) ?>';

    // PARA IMPRIMIR COMPROBANTE ELECTRÓNICO
    <?php if ($this->session->userdata('enviar_comprobante_e')): ?>
        url_cpbt = ruta_ticket + "imprimir_comprobante_e.php?cabecera_id=" + "<?= $this->session->userdata('cabecera_id_para_comprobante') ?>";
        window.open(url_cpbt,'_blank', 'width=1000,height=700');
        <?php 
            $this->session->unset_userdata('enviar_comprobante_e'); 
            $this->session->unset_userdata('cabecera_id_para_comprobante'); 
        ?>
    <?php endif ?>

</script>
<script type="text/javascript">
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //Valores por defecto 
    ver_serie("03");
    cambiar_comprobante("03");
    function ver_serie(valor_tipo_comp){
        $('#sunat_serie_numero').val("");
        if (valor_tipo_comp == "O") {
            $('#sunat_serie_numero').val("");
            return false;
        }
        $.each(gb_array_numeraciones, function () {
            if (this.tipo_comprobante == valor_tipo_comp ) {
                serie = "" + this.serie + ' - ' + this.correlativo;
                $('#sunat_serie_numero').val(serie);
            }
        });
    }
    function cambiar_comprobante(valor){
        $('#cliente_tipo_documento_id').val("");
        if(valor == '03') $('#cliente_tipo_documento_id').val("1"); //DNI
        if(valor == '01') $('#cliente_tipo_documento_id').val("6");  //RUC
    }
    function consultar_doc(){
        var tipo_documento_id = $('#cliente_tipo_documento_id').val();
        var documento = $('#cliente_documento').val();
        documento = documento.trim();
        if(tipo_documento_id == 6 && documento.length != 11){
            alertify.error("Documento no válido.");
            return false;
        }
        if(tipo_documento_id == 1 && documento.length == 11){
            alertify.error("Documento no válido.");
            return false;
        }
        if(documento.length < 5){
            alertify.error("Debe ingresar un número de documento válido.");
            return false;
        }
        var url = '<?= base_url('administracion/clientes/json_buscar_por_documento/')?>' + documento;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(objeto['success']){
                    var sunat_tipo_documento_id = objeto['data'].sunat_tipo_documento_id;
                    var cliente_id  = objeto['data'].cliente_id;
                    var documento   = objeto['data'].documento;
                    var cliente     = objeto['data'].cliente;
                    var nombres     = objeto['data'].nombres;
                    var ape_paterno = objeto['data'].ape_paterno;
                    var ape_materno = objeto['data'].ape_materno;
                    var telefono    = objeto['data'].telefono;
                    var correo      = objeto['data'].correo;
                    var direccion   = objeto['data'].direccion;
                    var ubigeo      = objeto['data'].ubigeo;
                    var direccion_ubigeo = (ubigeo == "") ? direccion : direccion + " - " + ubigeo;

                    $('#cliente_id').val(cliente_id);
                    $('#cliente_nombre_completo').val(cliente);
                    $('#cliente_nombres').val(nombres);
                    $('#cliente_ape_paterno').val(ape_paterno);
                    $('#cliente_ape_materno').val(ape_materno);
                    $('#cliente_telefono').val(telefono);
                    $('#cliente_correo').val(correo);
                    $('#cliente_direccion').val(direccion_ubigeo);

                    $('#btn_consultar').hide();
                    $('#btn_editar').show();

                    $('#cliente_documento').attr('readonly', true);
                    $('#cliente_nombre_completo').attr('readonly', true);

                }else{
                    alertify.error(objeto['msg']);
                }
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function limpiar_datos_consulta(){
        // $('#documento').val("");
        $('#cliente_id').val("");
        $('#cliente_documento').val("");
        $('#cliente_nombre_completo').val("");
        $('#cliente_nombres').val("");
        $('#cliente_ape_paterno').val("");
        $('#cliente_ape_materno').val("");
        $('#cliente_telefono').val("");
        $('#cliente_direccion').val("");

        $('#cliente_documento').attr('readonly', false);
        $('#cliente_nombre_completo').attr('readonly', false);
        $('#cliente_nombres').attr('readonly', false);
        $('#cliente_ape_paterno').attr('readonly', false);
        $('#cliente_ape_materno').attr('readonly', false);
        $('#cliente_telefono').attr('readonly', false);
        $('#cliente_direccion').attr('readonly', false);

        $('#btn_consultar').show();
        $('#btn_editar').hide();
    }
    $("#form_generar_comprobante_e").submit(function() {
        abrirCargando();
        $('#btn_generar_comprobante_e').attr('disabled', true);
        check_boleta =  document.getElementById("check_boleta");
        check_factura = document.getElementById("check_factura");
        sunat_tipo_comprobante_id = check_boleta.checked ? "03" : "01";
        //Verificar datos del cliente
        if($('#cliente_documento').val().trim() == "" ||
            $('#cliente_nombre_completo').val().trim() == ""){
                alertify.error("Cliente no válido.");
                $('#btn_generar_comprobante_e').attr('disabled', false);
                cerrarCargando();
                return false;
        }
        //Verificar que coincida Comrpobnate con Tipo de Documento
        if(sunat_tipo_comprobante_id == "01"){
            if($('#cliente_tipo_documento_id').val().trim() != "6"){
                alertify.error("Tipo de comprobante no coincide con Tipo de documento.");
                $('#btn_generar_comprobante_e').attr('disabled', false);
                cerrarCargando();
                return false;
            }
        }
        //Verificar si es factura
        if($('#cliente_tipo_documento_id').val().trim() == "6"){
            if($('#cliente_documento').val().trim().length != 11){
                alertify.error("Número de RUC no válido.");
                $('#btn_generar_comprobante_e').attr('disabled', false);
                cerrarCargando();
                return false;
            }
        }
        //Verificar si es boleta
        if($('#cliente_tipo_documento_id').val().trim() == "1"){
            if($('#cliente_documento').val().trim().length != 8){
                alertify.error("Número de DNI no válido.");
                $('#btn_generar_comprobante_e').attr('disabled', false);
                cerrarCargando();
                return false;
            }
        }
    });

    //Para documentos electronicos multiples
    function dec_nuevo_total(checked, importe){
        var IGV = 0;
        var importe_sin_IGV = 0;
        var importe_total = $('#dec_importe_total').val();

        if(checked){
            importe_total = parseFloat(importe_total) + parseFloat(importe);
        }else{
            importe_total = parseFloat(importe_total) - parseFloat(importe);
        }
        var parametro_sin_igv = parseFloat(1 + parseFloat(gb_IGV));
        importe_sin_IGV = parseFloat(importe_total/parametro_sin_igv);
        IGV = importe_total - importe_sin_IGV;

        $('#dec_total_IGV').val(parseFloat(IGV).toFixed(2));
        $('#dec_importe_sin_IGV').val(parseFloat(importe_sin_IGV).toFixed(2));
        $('#dec_importe_total').val(parseFloat(importe_total).toFixed(2));
    }
</script>
<script type="text/javascript">
    $("#ir_atras").removeClass("hidden");
</script>
