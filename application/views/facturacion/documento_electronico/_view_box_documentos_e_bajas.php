
<?php if (isset($lstCpbeBajas) && count($lstCpbeBajas) > 0 ): ?>

    <div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
        <div class="widget-header">
            <h6 class="widget-title">
                <i class="ace-icon fa fa-file bigger-90" aria-hidden="true"></i>
                Comprobantes  electrónicos anulados (<?= count($lstCpbeBajas) ?> ítems)
            </h6>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="ace-icon fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="widget-body" style="display: block;">
            <div class="widget-main padding-4" style="position: relative; padding: 15px">
                <?php if (count($lstCpbeBajas) > 0): ?>
                    <?php foreach ($lstCpbeBajas as $key => $oBaja): ?>
                        <?php if ($key != 0): ?>
                            <hr><br>
                        <?php endif ?>
                        <div class="row" >
                            <div class="col-sm-12 center">
                                <strong> COMUNICACIÓN DE BAJA </strong>
                                <br>
                                <?= $oBaja->numDocBaja ?>
                                <br>
                            </div>
                            <div class="col-sm-12 left">
                                <strong>Fecha Emisión:</strong> <?= $oBaja->fecGeneracion ?>
                                <br>
                                <strong>Fecha Baja:</strong> <?= $oBaja->fecComunicacion ?>
                                <br>
                                <strong>Cliente:</strong> 
                            </div>
                        </div>
                    <?php endforeach ?>
                <?php else: ?>
                    <div class="row">
                        <div class="col-sm-12">
                            El pedido aún no tiene comprobantes electrónicos.
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>

<?php endif ?>
    