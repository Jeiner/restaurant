<div id="modal_comprobante_e" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong> Detalle del comprobante electrónico </strong> 
                </h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <strong>Cliente:</strong> <?= strtoupper($comprobante_e->rznSocialUsuario) ?>
                        <br>
                        <strong>Documento:</strong> <?= $comprobante_e->numDocUsuario ?>
                        <br>
                        <strong>Dirección:</strong><?= ($oCliente == null)? " -": strtoupper($oCliente->direccion)?>
                        <br>
                        <strong>Correo:</strong><?= ($oCliente == null)? " -": strtoupper($comprobante_e->correo)?>
                        <br>
                        <strong>Moneda:</strong> <?= $comprobante_e->tipMoneda ?>
                    </div>
                    <div class="col-sm-4 center">
                        <strong><?= $comprobante_e->sunat_tipo_doc_e ?></strong><br>
                        <?= $comprobante_e->serie ?> - <?= $comprobante_e->correlativo ?><br>
                        <strong>RUC:</strong> <?= $generalidades->RUC ?> <br>
                        <strong>Fecha emisión:</strong> <?= $this->Site->fecha_yyyymmdd_a_ddmmyyyy($comprobante_e->fecEmision).' '.$comprobante_e->horEmision ?>
                    </div>
                </div>
                <br>
                <table class="table table-bordered" id="tabla-comprob" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center"> Cód. </th>
                            <th style="text-align: center"> Producto </th>
                            <th style="text-align: center"> Cantidad</th>
                            <th style="text-align: center"> P. unit. </th>
                            <th style="text-align: center"> Importe </th>
                            <th style="text-align: center"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle as $key => $item): ?>
                            <tr>
                                <td style="text-align: center"> <?= $item->comanda_id ?> </td>
                                <td style="text-align: left"> <?= strtoupper($item->desItem) ?> </td>
                                <td style="text-align: left;" class="center"> <?= $item->ctdUnidadItem ?></td>
                                <?php $precioUnit = $item->mtoValorUnitario + ($item->mtoValorUnitario * 0.18)  ?> 
                                <td style="text-align: center" class="dinero"> <?= number_format(round($precioUnit,2), 2, '.', ' ')  ?> </td>
                                <td style="text-align: center" class="dinero"> <?= $item->mtoPrecioVentaUnitario ?> </td>
                                <td style="text-align: center"> 
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
               </table>               
               <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <div class="form-group">
                        <label class="col-sm-6" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                            Total venta:
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control dinero" name="" id="" style="font-size: 18px!important;font-weight: bold;" value="<?= $comprobante_e->sumPrecioVenta ?>" readonly="">
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    Pedido: <?= $comprobante_e->comanda_id ?>, Oden de pago: <?= $comprobante_e->pago_id ?>
                </div>
            </div>
            </div>
            <br>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="imprimir_comprobante_e('<?= $comprobante_e->cabecera_id ?>');">
                            <i class="ace-icon fa fa-print bigger-90" aria-hidden="true"></i>
                            Imprimir comprobante
                        </button>
                    </div>
                    <div class="col-sm-4">
                        <?php
                            $url_facsu = base_url('facturacion/documento_electronico/popup_enviar_cbe_a_facsu/').$comprobante_e->cabecera_id;
                        ?>
                        <a href="<?= $url_facsu ?>" target="_blank" class="btn btn-primary btn-sm btn-block" >
                            Enviar a facturaciòn
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    var ruta_ticket = "<?= $this->Site->get_ruta_proy_ticket() ?>";
    function imprimir_comprobante_e(cabecera_id){
        url_ticket = ruta_ticket + "imprimir_comprobante_e.php?cabecera_id=" + cabecera_id + "";
        window.open(url_ticket,'_blank', 'width=1000,height=700');
    }
    $('#tabla-comprob').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },    //Código
          { "targets": [ 1 ],"width": "15%", },   //Docum
          { "targets": [ 2 ],"width": "20%", },   //Nombre
          { "targets": [ 3 ],"width": "15%", },   //Tipo
          { "targets": [ 4 ],"width": "15%", },   //Telefono
          { "targets": [ 5 ],"width": "15%", },   //Sele
        ],
        "order": [[ 1, "asc" ]] ,
    });
    $('#tabla-comprob tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>