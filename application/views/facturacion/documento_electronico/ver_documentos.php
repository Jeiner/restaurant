<div class="row">
    <div class="col-sm-2">
        <div class="alert alert-danger center" style="font-size:13px;padding: 5px">
            <strong>
                ERRORES<br>
            </strong>
            <?= $docs_errores ?>
            <br>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="alert alert-warning center" style="font-size:13px; padding: 5px">
            <strong>
                PENDIENTES
                <br>
            </strong>
            <?= $docs_pendientes ?>
            <br>
        </div>
    </div>
    <div class="col-sm-3 col-sm-offset-2">
        <a href="<?= base_url('facturacion/documento_electronico/enviar_lote_cbe_a_facsu') ?>" class="btn btn-primary btn-xs btn-block" id="filtrar" style="font-size:13px;" target="_blank" >
            Enviar Cpbe en lote
        </a>
    </div>
    <div class="col-sm-3">
        <a href="<?= base_url('facturacion/documento_electronico/procesar_lote_cpbe') ?>" class="btn btn-success btn-xs btn-block" id="filtrar" style="font-size:13px;" target="_blank">
            Procesar
        </a>
    </div>
</div>
<!-- Tabla con documentos electrónicos  -->
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#comrpobantes_e">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Comprobantes electrónicos
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="comrpobantes_e" class="tab-pane fade in active">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha inicio </label>
                        <input type="date" class="form-control input-sm " name="start_date" id="start_date"  value="<?=  date('Y-m-01') ?>" onchange="verify_start_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha fin </label>
                        <input type="date" class="form-control input-sm " name="end_date" id="end_date"  value="<?=  date('Y-m-d') ?>" onchange="verify_end_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" style="margin-top: 23px;">
                        <button class="btn btn-primary btn-xs btn-block" id="filtrar" onclick="filtrar_cpbtes();">
                            Buscar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" id="tabla_cpbtes">
                    <!-- Lista de comprobantes electrónicos  -->
                </div>
            </div>
        </div>
    </div>
</div>

<div id="div_modal_comprobante_e">
        
</div>

<script type="text/javascript">
    filtrar_cpbtes();

    function date_diff(start_date_AMD, end_date_AMD,tipe){
        var start_date = new Date(start_date_AMD).getTime();
        var end_date    = new Date(end_date_AMD).getTime();
        var diff = end_date - start_date;
        return diff/(1000*60*60*24);
    }
    function verify_end_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date === undefined || start_date == ""){
            $('#end_date').parent(".form-group").addClass("has-error");            
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            $('#end_date').parent(".form-group").addClass("has-error");
            return false;
        }else{
            $('#end_date').parent(".form-group").removeClass("has-error");
        }
    }
    function verify_start_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(end_date === undefined || end_date == ""){
            return true;
        }else{
            array_start_date = start_date.split("/");
            array_end_date = end_date.split("/");
            start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
            end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
            start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
            var dias = date_diff(start_date, end_date, 'days');
            if(dias < 0 ){
                $('#end_date').parent(".form-group").addClass("has-error");
                return false;
            }else{
                $('#end_date').parent(".form-group").removeClass("has-error");
            }
        }
    }
    function filtrar_cpbtes(){
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        $('#tabla_cpbtes').html('<span class="blue bolder" style="margin: 15px;">Cargando comprobantes...</span>');
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('facturacion/documento_electronico/ajax_cargar_tabla_documentos')?>",
            data: {"start_date":start_date, "end_date":end_date},
            success: function(rpta){
                $('#tabla_cpbtes').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>