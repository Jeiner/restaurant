<?php
    $total_valor = 0;
    $total_tributos = 0;
    $total_venta = 0;    
?>
<!-- Tabla comandas facturadas -->
<table class="table table-bordered table-striped" id="tabla_cpbts" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;" title="cabecera_id" > Cód. </th>
            <th style="text-align: center;" > Fecha </th>
            <th style="text-align: center;" class="hidden-xs"> Tipo Doc. Elec. </th>
            <th style="text-align: center;" > Serie-Número </th>
            <th style="text-align: center;" > Cliente </th>
            <th style="text-align: center;" > Valor venta </th>
            <th style="text-align: center;" > IGV </th>
            <th style="text-align: center;" > Total</th>
            <th style="text-align: center;" > SUNAT </th>
        </tr>
    </thead>
    <tbody id="cuerpo_tabla_cpbts">
    	<?php foreach ($comprobantes_e as $key => $cpbte): ?>
            <?php 
                $total_valor += $cpbte->sumTotValVenta; 
                $total_tributos += $cpbte->sumTotTributos; 
                $total_venta += $cpbte->sumPrecioVenta; 
            ?>
    		<tr>
                <td style="text-align: center" title="cabecera_id">
                    <?= $cpbte->cabecera_id?> 
                </td>
				<td style="text-align: center">
                    <?= $cpbte->fecEmision.' '.$cpbte->horEmision?> 
                </td>
                <td style="text-align: center" class="hidden-xs"> <?= $cpbte->sunat_tipo_doc_e ?> </td>
				<td style="text-align: center">
                    <a href="javascript:;" onclick="abrir_modal_comprobante_e(<?= $cpbte->cabecera_id ?>)"><?= $cpbte->serie ?> - <?= $cpbte->correlativo  ?></a> 
                </td>
                <td style="text-align: left"> 
                    <?= substr($cpbte->rznSocialUsuario, 0,30) ?>
                </td>
                <td style="text-align: center" class="dinero"> <?= $cpbte->sumTotValVenta ?> </td>
                <td style="text-align: center" class="dinero"> <?= $cpbte->sumTotTributos ?> </td>
				<td style="text-align: center" class="dinero"> <?= $cpbte->sumPrecioVenta ?> </td>
                <td style="text-align: center"> <?= $cpbte->sunat_estado ?> </td>
			</tr>
    	<?php endforeach ?>
    </tbody>
</table>
<br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-6 col-lg-4 col-lg-offset-8">
        <div class="form-group">
            <label class="col-sm-6" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                Total valor:
            </label>
            <div class="col-sm-6">
                <input type="text" class="form-control dinero" name="" id="" style="font-size: 18px!important;font-weight: bold;" value="<?= number_format(round($total_valor,2), 2, '.', ' ') ?>" readonly>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-6 col-lg-4 col-lg-offset-8">
        <div class="form-group">
            <label class="col-sm-6" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                Total tributos:
            </label>
            <div class="col-sm-6">
                <input type="text" class="form-control dinero" name="" id="" style="font-size: 18px!important;font-weight: bold;" value="<?= number_format(round($total_tributos,2), 2, '.', ' ') ?>" readonly>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-6 col-lg-4 col-lg-offset-8">
        <div class="form-group">
            <label class="col-sm-6" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                Total venta:
            </label>
            <div class="col-sm-6">
                <input type="text" class="form-control dinero" name="" id="" style="font-size: 18px!important;font-weight: bold;" value="<?= number_format(round($total_venta,2), 2, '.', ' ') ?>" readonly>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#tabla_cpbts').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "7%", },   //Cabecera_id
          { "targets": [ 1 ],"width": "10%", },   //Fecha
          { "targets": [ 2 ],"width": "13%", },   //Tipo com
          { "targets": [ 3 ],"width": "13%", },   //Serie
          { "targets": [ 4 ],"width": "20%", },   //Cliente
          { "targets": [ 5 ],"width": "8%", },   //Valor
          { "targets": [ 6 ],"width": "8%", },   //Tributo
          { "targets": [ 7 ],"width": "8%", },   //Total
          { "targets": [ 8 ],"width": "10%", },   //Sunat          
        ],
        "order": [[ 0, "desc" ]] ,
    });
    
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
    function abrir_modal_comprobante_e(cabecera_ia){
        var ruta = "<?=base_url('facturacion/documento_electronico/ajax_cargar_modal_documento')?>" + "/" +  cabecera_ia;
        $.ajax({
            type: 'POST',
            url: ruta,
            data: {},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#div_modal_comprobante_e').html(rpta);
                    $('#modal_comprobante_e').modal({
                        show: 'false'
                    }); 
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>