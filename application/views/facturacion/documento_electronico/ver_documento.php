<?php  
    $importe_total = 0;
?>
<?= form_open(base_url('facturacion/documento_electronico/generar_documento'), '" id="form_generar_comprobante_e"'); ?>
<div class="row">
    
    <div class="col-sm-7">
        <!-- Datos de la comanda  -->
        <div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-shopping-cart bigger-90" aria-hidden="true"></i>
                    Detalle de la comanda
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Fecha y hora</label>
                            <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->fecha_comanda ?>" readonly style="text-align: center" >
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Modalidad</label>
                            <?php
                                if ($oComanda->modalidad == "PRE") $modalidad_desc= "PRESENCIAL";
                                if ($oComanda->modalidad == "DEL") $modalidad_desc= "DELIVERY";
                                if ($oComanda->modalidad == "LLE") $modalidad_desc= "PARA LLEVAR";
                            ?>
                            <input type="text" name="" id="" class="form-control input-sm" value="<?= $modalidad_desc ?>" readonly style="text-align: center" >
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Mesa</label>
                            <input type="text" name="mesa" id="mesa" class="form-control input-sm center" value="<?= $oComanda->mesa ?>" readonly >
                            <input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm hidden" value="<?= $oComanda->mesa_id ?>" readonly >
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Mozo</label>
                            <input type="text" name="mozo" id="mozo" class="form-control input-sm center" value="<?= strtoupper($oComanda->mozo) ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Atención</label><br>
                            <?php if ($oComanda->estado_atencion == 'E'): ?>
                                <span class="label label-warning btn-block"> En espera </span>
                            <?php endif ?>
                            <?php if ($oComanda->estado_atencion == 'A'): ?>
                                <span class="label label-primary btn-block"> Atendido </span>
                            <?php endif ?>
                            <?php if ($oComanda->estado_atencion == 'F'): ?>
                                <span class="label label-success btn-block"> Finalizado </span>
                            <?php endif ?>
                            <?php if ($oComanda->estado_atencion == 'X'): ?>
                                <span class="label label-inverse btn-block"> Anulado </span>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Pago</label><br>
                            <?php if ($oComanda->estado_pago == 'P'): ?>
                                <span class="label label-danger btn-block"> Pendiente </span>
                            <?php endif ?>
                            <?php if ($oComanda->estado_pago == 'C'): ?>
                                <span class="label label-success btn-block"> Pagado </span>
                            <?php endif ?>
                            <?php if ($oComanda->estado_pago == 'X'): ?>
                                <span class="label label-inverse btn-block"> Anulado </span>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div id="productos_seleccionados">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th style="text-align: center; width: 5%"> Cód. </th>
                                        <th style="text-align: center; width: 30%"> Producto </th>
                                        <!-- <th style="text-align: center; width: 10%"> Hora </th> -->
                                        <th style="text-align: center; width: 10%"> Espera </th>
                                        <th style="text-align: center; width: 8%"> Cantidad </th>
                                        <th style="text-align: center; width: 12%"> Prec. unit. </th>
                                        <th style="text-align: center; width: 10%"> Importe </th>
                                        <th style="text-align: center; width: 15%"> Estado </th>
                                        <th style="text-align: center; width: 10%"> Declarar </th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php foreach ($oComanda->items as $key => $item): ?>
                                        <?php 
                                            $importe_total += $item->precio_unitario * $item->cantidad;
                                            $importe = number_format(round($item->precio_unitario * $item->cantidad,2), 2, '.', ' '); 
                                        ?>
                                        <tr>
                                            <td style="text-align: center"> <?= $item->comanda_item_id ?> </td>
                                            <td style="text-align: left"> <?= $item->producto ?> </td>
                                            <td style="text-align: center">
                                                <!-- Obtenemos el tiempo de espera o lo calculamos -->
                                                <?php if ($item->tiempo_espera): ?>
                                                    <?php $minutos = $item->tiempo_espera ?>
                                                <?php else: ?>
                                                    <?php $minutos = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") ); ?>
                                                <?php endif ?>
                                                <!-- Mostrarmos los minutos calculados  -->
                                                <?php if ($minutos < 15): ?>
                                                    <?= $minutos.' min' ?>
                                                <?php endif ?>
                                                <?php if ($minutos >= 15 && $minutos < 30): ?>
                                                    <span class="label label-warning"> <?= $minutos.' min' ?> </span>
                                                <?php endif ?>
                                                <?php if ($minutos >= 30): ?>
                                                    <span class="label label-danger"> <?= $minutos.' min' ?> </span>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center"> <?= $item->cantidad ?> </td>
                                            <td style="text-align: right;padding-right: 15px!important;vertical-align: middle;">
                                                <?= number_format(round($item->precio_unitario,2), 2, '.', ' ');  ?>
                                            </td>
                                            <td style="text-align: right;padding-right: 15px!important;vertical-align: middle;">
                                               <?= $importe ?>
                                            </td>
                                            <td style="vertical-align: middle;text-align: center">
                                                <?php if ($item->estado_atencion == 'E'): ?>
                                                    <span class="label label-warning"> En espera </span>
                                                <?php endif ?>
                                                <?php if ($item->estado_atencion == 'P'): ?>
                                                    <span class="label label-primary"> En preparación </span>
                                                <?php endif ?>
                                                <?php if ($item->estado_atencion == 'D'): ?>
                                                    <span class="label label-success"> Despachado </span>
                                                <?php endif ?>
                                                <?php if ($item->estado_atencion == 'X'): ?>
                                                    <span class="label label-inverse"> Anulado </span>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center">
                                                <?php if ($item->estado_atencion != 'X'): ?>
                                                    <?php $declarado = false; ?>
                                                    <?php foreach ($oCpbeDetalle as $key => $cpbeDet): ?>
                                                        <?php if ($cpbeDet->comanda_item_id == $item->comanda_item_id): ?>
                                                            <?php $declarado = true; ?>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                    <label class="pos-rel">
                                                            <input type="checkbox" class="ace" 
                                                                    readonly="readonly"
                                                                    <?php if ($declarado): ?>
                                                                        disabled="disabled"
                                                                    <?php endif ?>
                                                                    name="dec_seleccionados[]" 
                                                                    checked="checked" 
                                                                    value="<?= $item->comanda_item_id ?>" 
                                                                    onclick="dec_nuevo_total(this.checked, <?= $item->importe ?>);">
                                                            <span class="lbl"></span>
                                                    </label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php 
                        $importe_total = number_format(round($importe_total,2), 2, '.', ' ');
                    ?>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label class="" style="font-size: 14px; color: #000; ">
                                    Total IGV (18%)
                                </label>
                                <input type="text" class="form-control" name="total_IGV" id="total_IGV" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $oComanda->total_IGV ?>" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label class="" style="font-size: 14px; color: #000; ">
                                    Importe Sin IGV
                                </label>
                                <input type="text" class="form-control" name="importe_sin_IGV" id="importe_sin_IGV" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $oComanda->importe_sin_IGV ?>" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label class="" style="font-size: 16px; color: #000; ">
                                    <strong>Total</strong>
                                </label>
                                <input type="text" class="form-control" name="importe_total" id="importe_total" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $oComanda->importe_total ?>" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Datos de la PAGO  -->
        <?php $this->view('facturacion/pagos/_view_caja_datos_pago'); ?>
    </div>
    <!-- Documentos electronicos -->
    <?php if (count($oCpbeCabecera) > 0): ?>
        <div class="col-sm-5 form-horizontal">
            <!-- Datos de la PAGO  -->
            <?php $this->view('facturacion/documento_electronico/_view_box_documentos_e'); ?>
        </div>
    <?php endif ?>
        
</div>
<!-- Detalle del pago -->
<div class="row">
    <div class="col-sm-7">
        
    </div>
</div>

<?= form_close(); ?>
<script type="text/javascript">
    $('.chosen-select').chosen({allow_single_deselect:true}); 
</script>