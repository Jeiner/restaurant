<!-- Tabla comandas facturadas -->
<table class="table table-bordered table-striped" id="tabla_resumenes" width="100%">
    <thead>
        <tr>
            <th style="text-align: center; width: 5%" title="id" > Id </th>
            <th style="text-align: center; width: 10%" > Fecha emisión </th>
            <th style="text-align: center; width: 10%" > Fecha resumen </th>
            <th style="text-align: center; width: 10%" > Boletas </th>
            <th style="text-align: center; width: 10%" > Valor venta </th>
            <th style="text-align: center; width: 10%" > IGV </th>
            <th style="text-align: center; width: 10%" > Total</th>
            <th style="text-align: center; width: 10%" > Ticket </th>
            <th style="text-align: center; width: 10%" > Estado </th>
            <th style="text-align: center; width: 10%" > Enviar </th>
        </tr>
    </thead>
    <tbody id="cuerpo_tabla_resumenes">
        <?php foreach ($resumenes as $key => $res): ?>
            <tr>
                <td style="text-align: center"> <?= $res->resumen_id ?> </td>
                <td style="text-align: center"> <?= $res->fecEmision ?> </td>
                <td style="text-align: center"> <?= $res->fecResumen ?> </td>
                <td style="text-align: center"> <?= $res->nroBoletas ?> </td>
                <td style="text-align: right"> <?= $res->sumValorVenta ?> </td>
                <td style="text-align: right"> <?= $res->sumTributos ?> </td>
                <td style="text-align: right"> <?= $res->sumTotalVenta ?> </td>
                <td style="text-align: center"> <?= $res->ticket ?> </td>
                <td style="text-align: center"> <?= $res->estado_desc ?> </td>
                <td>
                    <a href="javascript:;" onclick="abrir_modal_resumen(<?= $res->resumen_id ?>)">
                        Ver documentos
                    </a> 
                    <?php if ($res->estado == "G"): ?>
                        <?php
                            $url_facsu = base_url('facturacion/resumen_diario/popup_enviar_resumen_a_facsu/').$res->resumen_id;
                        ?>
                    <?php endif ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>

<div id="div_modal_resumen">
        
</div>

<script type="text/javascript">
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }

    function abrir_modal_resumen(resumen_id){
        var ruta = "<?=base_url('facturacion/resumen_diario/ajax_cargar_modal_resumen')?>" + "/" +  resumen_id;
        $.ajax({
            type: 'POST',
            url: ruta,
            data: {},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#div_modal_resumen').html(rpta);
                    $('#modal_resumen').modal({
                        show: 'false'
                    }); 
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>