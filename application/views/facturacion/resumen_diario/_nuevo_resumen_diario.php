<div class="row">
	<div class="col-sm-2">
		<div class="form-group">
            <label>Fecha del resumen:</label>
            <input type="date" class="form-control input-sm " name="fecha_nuevo_resumen" id="fecha_nuevo_resumen"  value="<?=  date('Y-m-d') ?>">
        </div>
	</div>
    <div class="col-sm-2">
    	<div class="form-group">
    		<br>
	 		<button class="btn btn-primary btn-xs btn-block" id="filtrar" onclick="obtener_cpbtes_porfecha();">
            	Buscar
        	</button>
        </div>
    </div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12" id="tabla_cpbtes_para_resumen">
        <!-- Lista de comprobantes electrónicos para generar nuevo resumen  -->
    </div>
</div>
<hr>
<div class="row">
	<div class="col-sm-3 col-sm-offset-9">
    	<div class="form-group">
	 		<button class="btn btn-success btn-sm btn-block" id="btn_generar_resumen" onclick="iniciar_resumen_diario();" disabled="disabled">
            	Generar resumen diario
        	</button>
        </div>
    </div>
</div>

<script type="text/javascript">
	function obtener_cpbtes_porfecha(){
		//Valida fecha
        var fecha_nuevo_resumen = $("#fecha_nuevo_resumen").val();
        if(fecha_nuevo_resumen === undefined || fecha_nuevo_resumen == ""){
            alertify.error("Fecha no válida");
            return false;
        }

        $('#tabla_cpbtes_para_resumen').html('<span class="blue bolder" style="margin: 15px;">Cargando comprobantes...</span>');

        $.ajax({
            type: 'POST',
            url: "<?=base_url('facturacion/resumen_diario/ajax_cargar_tabla_boletas_por_fecha')?>",
            data: {"date":fecha_nuevo_resumen},
            success: function(rpta){
                $('#tabla_cpbtes_para_resumen').html(rpta);
                $("#btn_generar_resumen").prop( "disabled", false);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }

    function iniciar_resumen_diario(){
    	$("#btn_generar_resumen").prop( "disabled", true);

    	//Valida fecha
    	var fecha_nuevo_resumen = $("#fecha_nuevo_resumen").val();
    	if(fecha_nuevo_resumen === undefined || fecha_nuevo_resumen == ""){
            alertify.error("Fecha no válida");
            return false;
        }
        
    	$.ajax({
            type: 'POST',
            url: "<?=base_url('facturacion/resumen_diario/ajax_cantidad_boletas_por_fecha')?>",
            data: {"date":fecha_nuevo_resumen},
            success: function(rpta){
            	let nro_boletas = 0;
            	objeto = JSON.parse(rpta);
            	respuesta = objeto['success'];
            	mensaje = objeto['mensaje'];

            	//Validar respuesta y número de boletas.
                if(!respuesta){
                	alertify.error("Error al obtener las boletas electrónicas.");
                	return false;
                }
                nro_boletas = objeto['nro_boletas'];
                if(nro_boletas == 0){
                	alertify.error("No se encontraron boletas para generar resumen diario.");
                	return false;
                }

                abrir_modal_confirmar_resumen(nro_boletas, fecha_nuevo_resumen);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }

 	function abrir_modal_confirmar_resumen(nro_boletas, fecha_nuevo_resumen){
 		let mensaje = "" + nro_boletas + " boletas para el resumen diario."

 		swal({
				title: "¿Seguro de generar el resumen diario?",
				text: mensaje,
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-success",
				cancelButtonClass: "btn-danger",
				confirmButtonText: "Si, Generar!",
				cancelButtonText: "No, cancelar",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm) {
		  		if (isConfirm) {
		    		generar_resumen_diario(fecha_nuevo_resumen);
		    		
		  		} else {
		    		swal("Cancelado", "", "error");
		  		}
		  	}
		);
    }

    function generar_resumen_diario(fecha_nuevo_resumen){
    	$.ajax({
            type: 'POST',
            url: "<?=base_url('facturacion/resumen_diario/generar_resumen_diario')?>",
            data: {"date":fecha_nuevo_resumen},
            success: function(rpta){
            	let nro_boletas = 0;
            	objeto = JSON.parse(rpta);
            	respuesta = objeto['success'];
            	mensaje = objeto['mensaje'];

            	//Validar respuesta
                if(!respuesta){
                	alertify.error(mensaje);
                	return false;
                }
				swal("Generado!", "El resumen diario se generó correctamente.", "success");
				$("#btn_generar_resumen").prop( "disabled", true);
            },
            error: function(rpta){
                alert("Error en la operación,");
            }
        });
    }
</script>