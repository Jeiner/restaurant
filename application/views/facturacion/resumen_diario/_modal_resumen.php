<div id="modal_resumen" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong> Boletas electrónicas para el resumen diario </strong> 
                </h3>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="tabla-detalle" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center"> ID </th>
                            <th style="text-align: center"> Fecha </th>
                            <th style="text-align: center"> Tipo documento </th>
                            <th style="text-align: center"> Documento </th>
                            <th style="text-align: center"> Valor venta </th>
                            <th style="text-align: center"> IGV </th>
                            <th style="text-align: center"> Total </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($detalle as $key => $item): ?>
                            <tr>
                                <td style="text-align: center"> <?= $item->resumen_id ?> </td>
                                <td style="text-align: center"> <?= $item->fecResumen ?> </td>
                                <td style="text-align: center"> <?= $item->tipDocResumenDesc ?> </td>
                                <td style="text-align: center"> <?= $item->idDocResumen ?> </td>
                                <td style="text-align: right"> <?= $item->totValGrabado ?> </td>
                                <td style="text-align: right"> <?= $item->totImpCpe - $item->totValGrabado ?> </td>
                                <td style="text-align: right"> <?= $item->totImpCpe ?> </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
               </table>               
            </div>
            <br>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-4">
                        <?php
                            $url_facsu = base_url('facturacion/resumen_diario/popup_enviar_resumen_a_facsu/').$resumen->resumen_id;
                        ?>
                        <a href="<?= $url_facsu ?>" target="_blank" class="btn btn-primary btn-sm btn-block" >
                            Enviar a facturaciòn
                        </a>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $('#tabla-detalle').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },    //Código
          { "targets": [ 1 ],"width": "10%", },   //Docum
          { "targets": [ 2 ],"width": "10%", },   //Nombre
          { "targets": [ 3 ],"width": "10%", },   //Tipo
          { "targets": [ 4 ],"width": "10%", },   //Telefono
          { "targets": [ 5 ],"width": "10%", },   //Sele
          { "targets": [ 6 ],"width": "10%", },   //Sele
        ],
        "order": [[ 3, "asc" ]] ,
    });
    $('#tabla-detalle tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>