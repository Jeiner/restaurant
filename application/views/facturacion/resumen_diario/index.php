<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#resumen_diario">
                Resumen diario
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="resumen_diario" class="tab-pane fade in active">
            <div class="row">
				<div class="col-sm-12">
					<div id="accordion" class="accordion-style1 panel-group">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
										<i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										Listado de resúmenes generados
									</a>
								</h4>
							</div>

							<div class="panel-collapse collapse in" id="collapseOne">
								<div class="panel-body">
									<?php $this->load->view('facturacion/resumen_diario/_lista_resumenes'); ?>
								</div>
							</div>
						</div>
						<!-- panel - Lista de resumenes diarios-->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
										<i class="ace-icon fa fa-angle-right bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
										Generar nuevo resumen 
									</a>
								</h4>
							</div>

							<div class="panel-collapse collapse" id="collapseTwo">
								<div class="panel-body">
									<?php $this->load->view('facturacion/resumen_diario/_nuevo_resumen_diario'); ?>
								</div>
							</div>
						</div>
						<!-- Panel - Nuevo resumen -->
					</div>
					<!-- accordion -->
				</div>
			</div>
			<!-- row -->
        </div>
    </div>
</div>
