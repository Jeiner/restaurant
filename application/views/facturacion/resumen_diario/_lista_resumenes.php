<?php
    $anioSelec = "";
    $mesSelec = "";
    $anioActual = date('Y');
    $mesActual = date('m');
    $meses = array('enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre');
?>
<div class="row">
    <div class="col-sm-2">
        <div class="form-group">
            <label>Año:</label>
            <select class="form-control input-sm" id="cboAnio">
                <option value="0">--Seleccionar--</option>
                <?php for ($anio = 2018; $anio <= $anioActual ; $anio++): ?>
                    <?php if ($anio == $anioActual): ?>
                        <?php $anioSelec = "selected" ?>
                    <?php else: ?>
                        <?php $anioSelec = "" ?>
                    <?php endif ?>
                    <option value="<?= $anio ?>" <?= $anioSelec ?>><?= $anio ?></option>
                <?php endfor ?>
            </select>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label>Mes:</label>
            <select class="form-control input-sm" id="cboMes">
                <option value="0">--Seleccionar--</option>
                <?php foreach ($meses as $key => $mes): ?>
                    <?php if ($key + 1 == $mesActual): ?>
                        <?php $mesSelec = "selected" ?>
                    <?php else: ?>
                        <?php $mesSelec = "" ?>
                    <?php endif ?>
                    <option value="<?= $key ?>" <?= $mesSelec ?>><?= strtoupper($meses[$key]) ?></option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    <div class="col-sm-2">
    	<div class="form-group">
    		<br>
	 		<button class="btn btn-primary btn-xs btn-block" id="btn_obtener_resumenes" onclick="obtener_resumenes();">
            	Buscar resúmenes
        	</button>
        </div>
    </div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12" id="tabla_resumenes">
        <!-- Lista de resumenes -->
    </div>
</div>

<script type="text/javascript">

    obtener_resumenes();

    function obtener_resumenes(){
        $("#btn_obtener_resumenes").prop( "disabled", true);

        let anio = $("#cboAnio").val();
        let mes = $("#cboMes").val();
        mes = parseInt(mes) + 1;

        //Valida parámetros
        if(anio ==  0 || mes == 0){
            alertify.error("Datos no válidos.");
            $("#btn_obtener_resumenes").prop( "disabled", false);
            return
        }

        $('#tabla_resumenes').html('<span class="blue bolder" style="margin: 15px;">Cargando comprobantes...</span>');
        
        $.ajax({
            type: 'POST',
            url: "<?=base_url('facturacion/resumen_diario/ajax_tabla_obtener_resumenes')?>",
            data: {"anio":anio, "mes":mes},
            success: function(rpta){
                $('#tabla_resumenes').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });

        $("#btn_obtener_resumenes").prop( "disabled", false);
    }
</script>