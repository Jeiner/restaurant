<?php
    $nuevo_resumen_total_valor = 0;
    $nuevo_resumen_total_tributos = 0;
    $nuevo_resumen_total_venta = 0;    
?>
<!-- Tabla comandas facturadas -->
<table class="table table-bordered table-striped" id="tabla_boletas_para_resumen" width="100%">
    <thead>
        <tr>
            <th style="text-align: center;" > N°</th>
            <th style="text-align: center;" title="cabecera_id" > Cód. </th>
            <th style="text-align: center;" > Fecha </th>
            <th style="text-align: center;" class="hidden-xs"> Tipo Doc. Elec. </th>
            <th style="text-align: center;" > Serie-Número </th>
            <th style="text-align: center;" > Cliente </th>
            <th style="text-align: center;" > Valor venta </th>
            <th style="text-align: center;" > IGV </th>
            <th style="text-align: center;" > Total</th>
        </tr>
    </thead>
    <tbody id="cuerpo_tabla_boletas_para_resumen">
    	<?php foreach ($boletas_e as $key => $cpbte): ?>
            <?php 
                $nuevo_resumen_total_valor += $cpbte->sumTotValVenta; 
                $nuevo_resumen_total_tributos += $cpbte->sumTotTributos; 
                $nuevo_resumen_total_venta += $cpbte->sumPrecioVenta; 
            ?>
    		<tr>
                <td style="text-align: center" title="cabecera_id">
                    <?= $key+1  ?>
                </td>
                <td style="text-align: center" title="cabecera_id">
                    <?= $cpbte->cabecera_id?> 
                </td>
                <td style="text-align: center">
                    <?= $cpbte->fecEmision.' '.$cpbte->horEmision?> 
                </td>
                <td style="text-align: center" class="hidden-xs"> <?= $cpbte->sunat_tipo_doc_e ?> </td>
                <td style="text-align: center">
                    <a href="javascript:;" onclick="abrir_modal_comprobante_e(<?= $cpbte->cabecera_id ?>)"><?= $cpbte->serie ?> - <?= $cpbte->correlativo  ?></a> 
                </td>
                <td style="text-align: left"> 
                    <?= substr($cpbte->rznSocialUsuario, 0,30) ?>
                </td>
                <td style="text-align: center" class="dinero"> <?= $cpbte->sumTotValVenta ?> </td>
                <td style="text-align: center" class="dinero"> <?= $cpbte->sumTotTributos ?> </td>
                <td style="text-align: center" class="dinero"> <?= $cpbte->sumPrecioVenta ?> </td>
            </tr>
    	<?php endforeach ?>
    </tbody>
</table>
<div class="row">
    <div class="col-sm-12">
        <div style="text-align: right;">
            <label style="font-size: 16px; color: #000;">
                <span>Total valor: </span>
                <span>
                    <strong> S/.<?= number_format(round($nuevo_resumen_total_valor,2), 2, '.', ' ') ?></strong>
                </span>
            </label>
        </div>
        <div style="text-align: right;">
            <label style="font-size: 16px; color: #000; text-align: right;">
                <span>Total tributos: </span>
                <span>
                    <strong> S/.<?= number_format(round($nuevo_resumen_total_tributos,2), 2, '.', ' ') ?></strong>
                </span>
            </label>
        </div>
        <div style="text-align: right;">
            <label style="font-size: 16px; color: #000; text-align: right;">
                <span>Total venta: </span>
                <span>
                    <strong> S/.<?= number_format(round($nuevo_resumen_total_venta,2), 2, '.', ' ') ?></strong>
                </span>
            </label>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>