<?php $total = 0; ?>
<div class="row">
	<div class="col-sm-12 col-xs-12">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-tasks bigger-90" aria-hidden="true"></i>
                    Últimas comandas
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="<?= base_url('salon/salon/historial_comandas') ?>" class="btn btn-primary btn-sm btn-block ">
                            <i class="ace-icon fa fa-list-alt bigger-90" aria-hidden="true"></i>
                            Ver todas las comandas
                        </a>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped" id="tabla-comandas" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center;" title="comanda"> Cód. </th>
                                    <th style="text-align: center;" > Fecha </th>
                                    <th style="text-align: center;" > Mesa/Mod. </th>
                                    <th style="text-align: center;" > Mozo </th>
                                    <th style="text-align: center;" class="hidden-xs hidden-sm"> Tiempo </th>
                                    <th style="text-align: center;" class="hidden-xs hidden-sm" > Cliente </th>
                                    <th style="text-align: center;" > Importe </th>
                                    <th style="text-align: center;" > Atención </th>
                                    <th style="text-align: center;" > Pago </th>
                                    <th style="text-align: center;" > Detalle </th>
                                </tr>
                            </thead>
                            <tbody id="cuerpo_tabla_comandas">
                                <?php if (count($comandas) == 0): ?>
                                    <tr>
                                        <td colspan="10">No hay comandas para mostrar</td>
                                    </tr>
                                <?php else: ?>
                                    <?php foreach ($comandas as $key => $comanda): ?>
                                        <?php $total += $comanda->importe_total ?>
                                        <tr>
                                            <td style="text-align: center" title="comanda"> 
                                                <?= $comanda->comanda_id  ?> 
                                            </td>
                                            <td style="text-align: center"> <?= $comanda->fecha_comanda ?> </td>
                                            <td style="text-align: center"> <!-- Mesa  / modalida -->
                                                <?php if ($comanda->modalidad == "PRE"): ?>
                                                    <?= $comanda->mesa ?> 
                                                <?php else: ?>
                                                    <?= $comanda->modalidad_desc ?>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center"> <?= strtoupper($comanda->mozo) ?> </td>
                                            <td style="text-align: center" class="hidden-xs hidden-sm"> <!-- Tiempo -->
                                                <?php if ($comanda->estado_atencion == 'F'): ?>
                                                    <?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda,$comanda->finalizada_en ); ?>
                                                    <?= $minutos.' min' ?>
                                                <?php else: ?>
                                                    <?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda, date("Y/m/d H:i:s") ); ?>
                                                    <?= $minutos.' min' ?>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: left" class="hidden-xs hidden-sm">
                                                <span class="" title="<?= $comanda->cliente_nombre ?>" >
                                                    <?= ($comanda->cliente_nombre != "") ? substr($comanda->cliente_nombre, 0,11).'...' : "-" ?>
                                                </span>
                                            </td>
                                            <td style="text-align: center" class="dinero"> <?= $comanda->importe_total ?> </td>
                                            <td style="text-align: center">
                                                <?php if ($comanda->estado_atencion == 'E'): ?>
                                                    <span class="label label-warning"> En espera </span>
                                                <?php endif ?>
                                                <?php if ($comanda->estado_atencion == 'A'): ?>
                                                    <span class="label label-primary"> Atendido </span>
                                                <?php endif ?>
                                                <?php if ($comanda->estado_atencion == 'F'): ?>
                                                    <span class="label label-success"> Finalizado </span>
                                                <?php endif ?>
                                                <?php if ($comanda->estado_atencion == 'X'): ?>
                                                    <span class="label label-inverse"> Anulado </span>
                                                <?php endif ?> 
                                            </td>
                                            <td style="text-align: center">
                                                <?php if ($comanda->estado_pago == 'P'): ?>
                                                    <span class="label label-danger"> Pendiente </span>
                                                <?php endif ?>
                                                <?php if ($comanda->estado_pago == 'C'): ?>
                                                    <span class="label label-success"> Pagado </span>
                                                <?php endif ?>
                                                <?php if ($comanda->estado_pago == 'X'): ?>
                                                    <span class="label label-inverse"> Anulado </span>
                                                <?php endif ?> 
                                            </td>
                                            <td style="text-align: center">
                                                <a href="<?= base_url('salon/comanda/ver/') ?><?= $comanda->comanda_id ?>">
                                                    Ver detalle
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<script type="text/javascript">
    $('#tabla-comandas').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },   //Cod
          { "targets": [ 1 ],"width": "15%", },   //Fecha
          { "targets": [ 2 ],"width": "10%", },   //Mozo
          { "targets": [ 3 ],"width": "10%", },   //Mesa
          { "targets": [ 4 ],"width": "10%", },   //Tiempo
          { "targets": [ 5 ],"width": "10%", },   //Tiempo
          { "targets": [ 6 ],"width": "10%", },   //Importe
          { "targets": [ 7 ],"width": "10%", },   //Atencion
          { "targets": [ 8 ],"width": "10%", },   //Pago
          { "targets": [ 9 ],"width": "10%", },   //Detalle
        ],
        "order": [[ 1, "desc" ]] ,
    });
    $('#tabla-comandas tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>