<!-- 
	Recibe:
	- $oComanda
	- $listaMesas
-->
<?= form_open(base_url('salon/salon/editar_datos_generales'), 'class="form-horizontal" id="form_unir_mesas"'); ?>
    <div id="modal_editar_datos_generales" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="smaller lighter blue no-margin center"> <strong> Editar datos generales </strong> </h3>
                </div>
                <div class="modal-body" style="padding: 10px 30px 30px 30px" >
                    <!-- Listado de mesas -->
                    <input type="" name="gen_comanda_id"  value="<?= $oComanda->comanda_id ?>" class="hidden">

                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group">
                                <label>Modalidad</label>
                                <select class="form-control input-sm" id="gen_modalidad" name="gen_modalidad" style="width: 100%">
                                    <option value="">Seleccionar...</option>
                                    <?php foreach ($modalidades as $key => $mod): ?>
                                        <?php 
                                            $modalidad_selec = "";
                                            if($mod->valor == $oComanda->modalidad) $modalidad_selec = "selected";
                                        ?>
                                        <option value="<?= $mod->valor ?>" <?= $modalidad_selec ?>><?= $mod->descripcion ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-4">
                            <div class="form-group">
                                <label>Mesa</label>
                                <select class="form-control input-sm" id="gen_mesa_id" name="gen_mesa_id" style="width: 100%">
                                    <option value="">Seleccionar...</option>
                                    <?php foreach ($listaMesas as $key => $oMesa): ?>
                                        <?php 
                                            $mesa_selec = "";
                                            if($oMesa->mesa_id == $oComanda->mesa_id) $mesa_selec = "selected";
                                        ?>
                                        <option value="<?= $oMesa->mesa_id ?>" <?= $mesa_selec ?>><?= $oMesa->mesa ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group">
                                <label>Canal</label>
                                <select class="form-control input-sm" id="gen_canal" name="gen_canal" style="width: 100%">
                                    <option value="">Seleccionar...</option>
                                    <?php foreach ($canales as $key => $canal): ?>
                                        <?php 
                                            $canal_selec = "";
                                            if($canal->valor == $oComanda->canal) $canal_selec = "selected";
                                        ?>
                                        <option value="<?= $canal->valor ?>" <?= $canal_selec ?>><?= $canal->descripcion ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-4">
                            <div class="form-group">
                                <label>N° comens.</label>
                                <input type="text" style="text-align:center" name="gen_nro_comensales" id="gen_nro_comensales" class="form-control input-sm" value="<?= $oComanda->nro_comensales ?>" onkeypress="return soloNumeroEntero(event)">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm" id="btn_guardar_datos_generales">
                        <i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
                        Guardar datos
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?= form_close(); ?>

<script type="text/javascript">
	 $('#modal_editar_datos_generales').modal({
        show: 'false'
    }); 
</script>