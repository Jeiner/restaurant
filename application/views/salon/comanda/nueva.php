<?php
    $porcentaje_igv = $igv * 100;
?>
<!-- Titulo -->
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="page-header">            
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12">
                    <h1 style="font-weight: 500">
                        <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                        Nueva comanda
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            <?php if ($oComanda->mesa != ""): ?>
                                Mesa: <?= $oComanda->mesa ?>
                            <?php else: ?>
                                <?= ($oComanda->modalidad == 'DEL') ? 'Delivery': 'Para llevar' ?>
                            <?php endif ?>
                        </small>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="widget-box widget-color-granate"> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                    Buscar productos
                </h6>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label>Filtrar por familia: </label>
                                <select class="form-control input-sm" id="filtro_familia" name="filtro_familia"  onchange="cargar_tabla_productos_sm()">
                                    <option value="0" >Todas las familias</option>
                                    <?php foreach ($familias as $key => $fam): ?>
                                        <option value="<?= $fam->familia_id ?>"><?= $fam->familia ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label>Ordenar por: </label>
                                <select class="form-control input-sm" id="ordenar_por" name="ordenar_por" onchange="cargar_tabla_productos_sm()">
                                    <option value="ubicacion" > Ubicación en carta </option>
                                    <option value="producto" > Nombre de producto </option>
                                    <option value="precio" > Precio de producto </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="div_tabla_productos">
                        <!-- lista de productos para seleccionar -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <form method="Post" action="<?= base_url('salon/comanda/guardar') ?>" id="form_comanda" >
            <div class="widget-box widget-color-granate"> <!-- ui-sortable-handle -->
                <div class="widget-header">
                    <h6 class="widget-title">
                        <i class="ace-icon fa fa-shopping-cart bigger-90" aria-hidden="true"></i>
                        <?php if ($oComanda->mesa != ""): ?>
                            Mesa: <?= $oComanda->mesa ?> |
                        <?php else: ?>
                            <?= ($oComanda->modalidad == 'DEL') ? 'Delivery': 'Para llevar' ?> | 
                        <?php endif ?>
                        Usuario: <?= strtoupper($oComanda->mozo) ?>
                    </h6>
                </div>
                <div class="widget-body" style="display: block;">
                    <div class="widget-main padding-4" style="position: relative; padding: 15px">
                        <div class="row hidden" id="datos_ocultados">
                            <div class="col-sm-3 ">
                                <div class="form-group">
                                    <label>Comanda id</label>
                                    <input type="text" style="text-align:center" name="comanda_id" id="comanda_id" class="form-control input-sm" value="<?= $oComanda->comanda_id ?>" >
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6 ">
                                <div class="form-group">
                                    <label>Mesa</label>
                                    <input type="text" name="mesa" id="mesa" class="form-control input-sm" value="<?= $oComanda->mesa ?>" readonly >
                                    <input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm " value="<?= $oComanda->mesa_id ?>" readonly >
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6 ">
                                <div class="form-group">
                                    <label>Mozo</label>
                                    <input type="text" name="mozo" id="mozo" class="form-control input-sm" value="<?= $oComanda->mozo ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-xs-4">
                                <div class="form-group">
                                    <label>Modalidad</label>
                                    <select class="form-control input-sm" id="modalidad" name="modalidad" style="width: 100%">
                                        <option value="">Seleccionar...</option>
                                        <?php foreach ($modalidades as $key => $mod): ?>
                                            <?php 
                                                $modalidad_selec = "";
                                                if($mod->valor == $oComanda->modalidad) $modalidad_selec = "selected";
                                            ?>
                                            <option value="<?= $mod->valor ?>" <?= $modalidad_selec ?>><?= $mod->descripcion ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <div class="form-group">
                                    <label>N° comens.</label>
                                    <input type="text" style="text-align:center" name="nro_comensales" id="nro_comensales" class="form-control input-sm" value="<?= $oComanda->nro_comensales ?>" onkeypress="return soloNumeroEntero(event)">
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-4">
                                <div class="form-group">
                                    <label>Canal</label>
                                    <select class="form-control input-sm" id="canal" name="canal" style="width: 100%">
                                        <option value="">Seleccionar...</option>
                                        <?php foreach ($canales as $key => $canal): ?>
                                            <?php 
                                                $canal_selec = "";
                                                if($canal->valor == $oComanda->canal) $canal_selec = "selected";
                                            ?>
                                            <option value="<?= $canal->valor ?>" <?= $canal_selec ?>><?= $canal->descripcion ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>                         
                            <div class="col-sm-3">
                                <div class="spacio hidden-xs" style="margin-top: 30px;"> </div>
                                <div class="form-group pull-right" id="btn_opc_cliente">
                                    <a href="javascript:;" onclick="mostrar_opciones(true)" > Más opciones... </a>
                                </div>
                                <div class="form-group pull-right" style="display: none" id="btn_menos_opciones">
                                    <a href="javascript:;" onclick="mostrar_opciones(false)" > Menos opciones... </a>
                                </div>
                            </div>
                        </div>
                        <!-- style="display: none" -->
                        <div id="opc_cliente" <?php if($oComanda->cliente_id == 0 && $oComanda->modalidad == 'PRE') echo 'style="display: none" '  ?> >
                            <div class="row" hidden="">
                                <div class="col-sm-4 col-xs-6">
                                    <div class="spacio hidden-xs" style="margin-top: 23px;"> </div>
                                    <div class="form-group" >
                                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="cargar_modal_clientes();">
                                            Seleccionar cliente...
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="hidden">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Cliente_id</label>
                                            <input type="text" class="form-control input-sm" name="cliente_id" id="cliente_id" value="<?= $oComanda->cliente_id ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Tipo Documento</label>
                                        <select class="form-control input-sm" id="cliente_tipo_documento" name="cliente_tipo_documento" style="width: 100%">
                                            <option value="">Seleccionar...</option>
                                            <?php foreach ($tipos_documento_identidad as $key => $tipoDoc): ?>
                                                <?php 
                                                    $selected = "";
                                                    if($tipoDoc->valor == $oComanda->cliente_tipo_documento) 
                                                        $selected = "selected";
                                                ?>
                                                <option value="<?= $tipoDoc->valor ?>" <?= $selected ?>><?= $tipoDoc->descripcion ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <div class="form-group">
                                        <label>Documento</label>
                                        <input type="number" class="form-control input-sm" name="cliente_documento" id="cliente_documento" value="<?= $oComanda->cliente_documento ?>" maxlength="11" placeholder="DNI o RUC">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <div class="spacio" style="margin-top: 20px;"> </div>
                                    <div class="form-group" style="" id="btn_consultar">
                                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="consultar_doc();" >
                                            <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
                                            Consultar
                                        </button>
                                    </div>
                                    <div class="form-group" style="display: none" id="btn_editar" >
                                        <button type="button" class="btn btn-primary btn-sm btn-block" onclick="limpiar_datos_consulta();" >
                                            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                                            Editar 
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 col-xs-12">
                                    <div class="form-group">
                                        <label>Cliente</label>
                                        <input type="text" class="form-control input-sm" name="cliente_nombre" id="cliente_nombre" value="<?= $oComanda->cliente_nombre ?>" placeholder="Nombre o Razón social">
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="number" name="cliente_telefono" id="cliente_telefono" class="form-control input-sm" value="<?= $oComanda->cliente_telefono ?>" maxlength="9" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <input type="text" name="cliente_direccion" id="cliente_direccion" class="form-control input-sm" value="<?= $oComanda->cliente_direccion ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="text" name="cliente_correo" id="cliente_correo" class="form-control input-sm" value="<?= $oComanda->cliente_correo ?>" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <table id="productos_seleccionados" class="table table-bordered" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 45%">Producto</th>
                                            <th style="width: 20%">Cantidad</th>
                                            <th style="width: 15%">Prec. unit.</th>
                                            <th style="width: 15%">Importe</th>
                                            <th style="width: 5%">X</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-xs-3">
                                
                            </div>
                            <div class="col-sm-6 col-md-6 col-xs-9">
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-sm-6 col-xs-6" style="">
                                        <label class="" style="font-size: 14px; color: #000; ">
                                            Importe Sin IGV
                                        </label>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <input type="text" class="form-control" name="importe_sin_IGV" id="importe_sin_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="" readonly>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-sm-6 col-xs-6" style="">
                                        <label class="" style="font-size: 14px; color: #000; ">
                                            Total IGV (<?= $porcentaje_igv ?>%)
                                        </label>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <input type="text" class="form-control" name="total_IGV" id="total_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="" readonly>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 5px">
                                    <div class="col-sm-6 col-xs-6" style="">
                                        <label class="" style="font-size: 16px; color: #000; ">
                                            <strong>Total</strong>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <input type="text" class="form-control" name="importe_total" id="importe_total" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12" >
                                <textarea class="form-control input-sm" placeholder="Ingresar algún comentario" id="cmd_comentario" name="cmd_comentario"></textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-6 col-xs-8">
                                <button type="button" class="btn btn-app btn-danger btn-sm" id="btn_cancelar_cmd">
                                        <i class="ace-icon fa fa-arrow-left bigger-160"></i>
                                        Cancelar
                                </button>
                                <?php if ($oComanda->comanda_id == 0): ?>
                                    <button type="button" class="btn btn-app btn-primary btn-sm " id="btn_reiniciar_cmd">
                                        <i class="ace-icon fa fa-refresh bigger-160"></i>
                                        Reiniciar
                                    </button>
                                <?php endif ?>
                            </div>
                            <div class="col-sm-6 col-xs-4" style="text-align: right;">
                                <button type="submit" class="btn btn-app btn-success btn-sm" id="btn_guardar_comanda">
                                    <i class="ace-icon fa fa-save bigger-160"></i>
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <button class="close" data-dismiss="alert">
                <i class="ace-icon fa fa-times"></i>
            </button>
            <i class="ace-icon fa fa-info"></i>
            <strong>Recuerde:</strong> <br>
            - Si el producto tiene el atributo <strong>"Manejar stock"</strong>, el sistema validará que el stock actual sea mayor a lo que se quiere vender.
            <br>
            - Si el producto tiene un <strong>kit de insumos</strong>, el sistema verificará que haya stock suficiente de dichos insumos.
            <br>
            -Si la modalidad es <strong>"Delivery" o "Para llevar" </strong>la mesa seguirá con estado <strong>"Libre"</strong>.
        </div>
    </div>
</div>
<div id="div_modal_clientes">
    
</div>
<div id="div_modal_cliente">
    
</div>
<div id="div_modal_agregar_comentario">
    
</div>

<!-- Variables globales -->
<script type="text/javascript">
    var gb_familia_id = 0;
    var gb_ordenar_por = 'ubicacion';
    var gb_productos = '<?= json_encode($productos) ?>'; //Variable global  de produtcos
    var gb_array_productos = JSON.parse(gb_productos); //Pasamos los productos a una variable de javascript array
    var gb_kit = '<?= json_encode($kits) ?>'; //Insumos para cada productos
    var gb_array_kits = JSON.parse(gb_kit); //Pasamos a un array
    var gb_IGV = '<?= json_encode($igv) ?>';
    
</script>
<!-- LOCALSTORAGE -->
<script type="text/javascript">
    <?php if ($this->session->userdata('remove_local_comanda')): ?>
        remove_local_comanda();
        <?php $this->session->unset_userdata('remove_local_comanda'); ?>
    <?php endif ?>
    
    $("#form_comanda").submit(function() {
        $('#btn_guardar_comanda').attr('disabled', true);
        return true;
    });

    function remove_local_comanda(){  
        //Eliminar datos del localstorage
        //Se debe eliminar del index tambien
        localStorage.removeItem('nro_comensales');
        localStorage.removeItem('canal');
        localStorage.removeItem('cmd_comentario');
        
        localStorage.removeItem('cliente_tipo_documento');
        localStorage.removeItem('cliente_documento');
        localStorage.removeItem('cliente_nombre');
        localStorage.removeItem('cliente_direccion');
        localStorage.removeItem('cliente_telefono');
        localStorage.removeItem('cliente_correo');

        localStorage.removeItem('gb_familia_id');
        localStorage.removeItem('gb_ordenar_por');
        localStorage.removeItem('list_items_cmd');
    }
    function cargar_local_storage(){
        gb_familia_id = (localStorage.getItem("gb_familia_id") === null) ? 0 : localStorage.getItem("gb_familia_id") ;
        gb_ordenar_por = (localStorage.getItem("gb_ordenar_por") === null) ? 'ubicacion' : localStorage.getItem("gb_ordenar_por") ;
        
        $('#nro_comensales').val(localStorage.getItem("nro_comensales"));
        $('#canal').val(localStorage.getItem("canal"));
        $('#cmd_comentario').val(localStorage.getItem("cmd_comentario"));
        
        $('#cliente_tipo_documento').val(localStorage.getItem("cliente_tipo_documento"));
        $('#cliente_documento').val(localStorage.getItem("cliente_documento"));
        $('#cliente_nombre').val(localStorage.getItem("cliente_nombre"));
        $('#cliente_direccion').val(localStorage.getItem("cliente_direccion"));
        $('#cliente_telefono').val(localStorage.getItem("cliente_telefono"));
        $('#cliente_correo').val(localStorage.getItem("cliente_correo"));

        $('#filtro_familia').val(gb_familia_id);
        $('#ordenar_por').val(gb_ordenar_por);
        load_items();
    }
    cargar_local_storage();

    $('#nro_comensales').change(function (e) {
        localStorage.setItem('nro_comensales', $(this).val());
    });
    $('#canal').change(function (e) {
        localStorage.setItem('canal', $(this).val());
    });
    $('#cmd_comentario').change(function (e) {
        localStorage.setItem('cmd_comentario', $(this).val());
    });
    $('#cliente_tipo_documento').change(function (e) {
        localStorage.setItem('cliente_tipo_documento', $(this).val());
    });
    $('#cliente_documento').change(function (e) {
        localStorage.setItem('cliente_documento', $(this).val());
    });
    $('#cliente_nombre').change(function (e) {
        localStorage.setItem('cliente_nombre', $(this).val());
    });
    $('#cliente_direccion').change(function (e) {
        localStorage.setItem('cliente_direccion', $(this).val());
    });
    $('#cliente_telefono').change(function (e) {
        localStorage.setItem('cliente_telefono', $(this).val());
    });
    $('#cliente_correo').change(function (e) {
        localStorage.setItem('cliente_correo', $(this).val());
    });
    
    function load_items(){ //Carga los items a la tabla
        // Obtiene todos los productos
        var list_items_cmd = (localStorage.getItem('list_items_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_items_cmd'));
        var row_no =  0;
        $("#productos_seleccionados tbody").empty();
        var new_data = "";
        var importe_total = 0;
        $.each(list_items_cmd, function () {
            var item = this;
            this.tmp_otro_comentario = (this.tmp_otro_comentario == null) ? "" : this.tmp_otro_comentario;
            this.tmp_ids_comentarios = (this.tmp_ids_comentarios == null) ? "" : this.tmp_ids_comentarios;
            var html_coment = (this.comentario != '') ? "<div class='label-warning comentario_pedido'>"+ this.comentario +"</div>" : '';

            new_data += "<tr>\
                            <td class='left'>\
                                <input type='text' class='hidden' name='producto_id[]' value='" +this.producto_id+ "' readonly >\
                                <input type='text' class='hidden' name='comentario[]' value='" +this.comentario+ "' readonly >\
                                <a href='javascript:;' style='color:#000;' onclick='mostrar_caja_coment(this, " +this.producto_id+ ");' id='btn_abrir_modal_comentario_"+this.producto_id+"'>\
                                    <strong>"+ this.producto +"</strong>\
                                </a>\
                                "+ html_coment +"\
                                <div class='caja_comentario' style='display: none'>\
                                    <textarea class='form-control input-sm textarea_coment' placeholder='Agregar un comentario para el pedido.' onchange='set_comentario(this, "+ this.producto_id +");'>"+ this.comentario +"</textarea>\
                                    <input class='tmp_ids_comentarios' hidden value='"+this.tmp_ids_comentarios+"'>\
                                    <input class='tmp_otro_comentario' hidden value='"+this.tmp_otro_comentario+"'>\
                                </div>\
                            </td>\
                            <td style='padding-left: 0px!important;padding-right: 0px!important'>\
                                <div class='col-sm-4 col-xs-4' style='margin: 0px!important;padding: 0px!important'>\
                                    <button type='button' class='btn btn-xs btn-block btn-primary' onclick='agregar_producto(null,"+ this.producto_id +",-1)'>\
                                        <i class='icon-only  ace-icon fa fa-minus'></i>\
                                    </button>\
                                </div>\
                                <div class='col-sm-4 col-xs-4' style='margin: 0px!important;padding: 0px!important'>\
                                    <input type='text'  class='form-control input-sm'  name='cantidad[]' value='" +this.cantidad+ "' style='text-align: center;font-weight: bold' readonly >\
                                </div>\
                                <div class='col-sm-4 col-xs-4' style='margin: 0px!important;padding: 0px!important'>\
                                    <button type='button' class='btn btn-xs btn-primary btn-block' onclick='agregar_producto(null,"+ this.producto_id +",1)'>\
                                        <i class='icon-only  ace-icon fa fa-plus'></i>\
                                    </button>\
                                </div>\
                            </td>\
                            <td style='text-align: right;padding-right: 15px!important;vertical-align: middle;'>\
                                "+ this.precio_unitario +"\
                            </td>\
                            <td style='text-align: right;padding-right: 15px!important;vertical-align: middle;'>\
                                "+ this.importe.toFixed(2) +"\
                            </td>\
                            <td style='vertical-align: middle;'>\
                                <span class=' data-rel='tooltip' title='Quitar producto' >\
                                <button type='button' class='btn btn-danger btn-xs' onclick='quitar_producto("+ this.producto_id +");'>\
                                    X\
                                </button>\
                                </span>\
                            </td>\
                        </tr>";
            row_no++;
            importe_total += (item.precio_unitario * item.cantidad);
         });
        $("#productos_seleccionados tbody").html(new_data);
        var parametro_sin_igv = parseFloat(1 + parseFloat(gb_IGV));
        var importe_sin_IGV = (importe_total/parametro_sin_igv);
        var total_IGV  = importe_total - importe_sin_IGV;
        
        $('#total_IGV').val(parseFloat(total_IGV).toFixed(2));
        $('#importe_sin_IGV').val(parseFloat(importe_sin_IGV).toFixed(2));
        $('#importe_total').val(parseFloat(importe_total).toFixed(2));
    }
</script>


<script type="text/javascript">
    cargar_tabla_productos_sm(gb_familia_id);
    function cargar_tabla_productos_sm(){
        // Seleccionamos el boton de familia
        var familia_id = $('#filtro_familia').val();
        var ordenar_por = $('#ordenar_por').val();

        localStorage.setItem('gb_familia_id', familia_id);
        localStorage.setItem('gb_ordenar_por', ordenar_por);
        
        // Mensaje de cargando
        $('#div_tabla_productos').html('<span class="blue bolder" style="margin: 15px;">Cargando productos...</span>');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/productos/cargar_tabla_productos_sm')?>",
            data: { "familia_id": familia_id, "ordenar_por": ordenar_por },
            success: function(rpta){
                $('#div_tabla_productos').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function agregar_producto(btn, producto_id, cantidad){
        // Obtenemos la lista de items de la comanda
        var list_items_cmd = (localStorage.getItem('list_items_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_items_cmd'));
        var nro_items = (localStorage.getItem('list_items_cmd') === null) ? 0 : list_items_cmd.length;
        // Buscamos al producto
        var producto = null;
        for (var i = 0 ; i < gb_array_productos.length; i++) {
            if (gb_array_productos[i].producto_id == producto_id){
                producto = gb_array_productos[i];
            }
        }
        if (producto === null) {
            alertify.error("Producto no encontrado.");
            return false;
        }
        // Verificamos si esta seleccionado
        var existe = false; var item = null;
        $.each(list_items_cmd, function () {
            if (this.producto_id == producto_id) {
                existe = true;
                item = this;
                return;
            }
        });
        // Calculamos a nueva cantidad
        var new_cantidad = 1;
        if (existe){
            if (item.cantidad + cantidad <= 0){
                alertify.error("La cantidad de productos debe ser mayor a 0.");
                return false;
            }else{
                new_cantidad = item.cantidad + cantidad;    
            }
        }
        // Validamos si hay stock
        if (producto.manejar_stock == 1){
            if (producto.stock < new_cantidad) {
                alertify.error("No hay stock suficiente.");
                return false;
            }
        }else{
            // Verificamos stock de insumos
            var falta_stock = false;
            $.each(gb_array_kits, function () {
                if (this.producto_id == producto_id) {
                    var cantidad_insumo = new_cantidad * this.cantidad; //Lo que se gastará
                    if (this.stock < cantidad_insumo ) {
                        mensaje = "No hay stock suficiente del insumo: " + this.insumo;
                        alertify.error(mensaje);
                        falta_stock = true;
                        return;
                    }
                }
            });
            if (falta_stock) {
                return false;
            }
        }
        //Abrir modal de comentario


        // Ingresamos nuevos datos
        if (existe){
            // Si esta seleccionado
            item.cantidad += cantidad;
            item.importe += cantidad * parseFloat(item.precio_unitario);
        }else{
            // Si no esta en la lista
            new_item = producto;
            new_item.comentario = '';
            new_item.cantidad = 1;
            new_item.importe = parseFloat(new_item.precio_unitario);
            new_item.tmp_otro_comentario = "";
            new_item.tmp_ids_comentarios = "";
            list_items_cmd[nro_items] = new_item;
        }

        //Producto agregado correctamente
        localStorage.setItem('list_items_cmd', JSON.stringify(list_items_cmd));
        alertify.success("Producto agregado correctamente.");
        load_items();
    }
    function quitar_producto(producto_id){
        var list_items_cmd = (localStorage.getItem('list_items_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_items_cmd'));
        var existe = false; var index = -1;
        for (var i = 0 ; i < list_items_cmd.length; i++) {
            if (list_items_cmd[i].producto_id == producto_id){
                existe = true;
                index = i;
                break;
            }
        }
        if (!existe) {
            alertify.error("No se encontró el producto.");
            return false;
        }
        delete list_items_cmd[index];
        list_items_cmd = list_items_cmd.filter(function() { return true; });
        localStorage.setItem('list_items_cmd', JSON.stringify(list_items_cmd));
        load_items();
    }
    function mostrar_opciones(opcion){
        if(opcion){
            $('#opc_cliente').show('slow');
            $('#btn_opc_cliente').hide();
            $('#btn_menos_opciones').show();
            setTimeout(function(){ 
                $('.chosen-select').chosen({allow_single_deselect:true}); 
            }, 800);
        }else{
            $('#opc_cliente').hide('slow');
            $('#btn_opc_cliente').show();
            $('#btn_menos_opciones').hide();
        }
    }
    function cargar_modal_clientes(){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('salon/comanda/cargar_modal_clientes')?>",
            data: {},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#div_modal_clientes').html(rpta);
                    $('#modal_clientes_select').modal({
                        show: 'false'
                    }); 
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function abrir_modal_agregar_comentario(producto_id){
        localStorage.removeItem('list_comentario_cmd');
        localStorage.removeItem('otro_comentario');

        $.ajax({
            type: 'POST',
            url: "<?=base_url('salon/comanda/ajax_abrir_modal_agregar_comentario')?>",
            data: {'producto_id':producto_id},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    $('#div_modal_agregar_comentario').html(rpta);
                    $('#modal_agregar_comentario').modal({
                        show: 'false',
                        backdrop: 'static',
                        keyboard: false
                    });
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function mostrar_caja_coment(btn, producto_id){
        //abrir_modal_agregar_comentario(producto_id);
        //return;
        var td = $(btn).parent();
        var caja = $(td).children('.caja_comentario');
        var comentario = $(td).children('.comentario_pedido'); 
        if($(caja).css('display') == 'none'){
            // Abro la caja y cierro el comentario
            $(caja).show('fast');
            $(comentario).hide('fast');
        }else{
            // Cierro la caja y abro el comentario
            $(caja).hide('fast');
            $(comentario).show('fast');
        }
    }
    function set_comentario(input, producto_id, tmp_otro_comentario = "", tmp_ids_comentarios = ""){  //Guarda un comentario para un item
        var list_items_cmd = (localStorage.getItem('list_items_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_items_cmd'));
        var existe = false; var index = -1;
        for (var i = 0 ; i < list_items_cmd.length; i++) {
            if (list_items_cmd[i].producto_id == producto_id){
                existe = true;
                index = i;
                break;
            }
        }
        if (!existe) {
            alertify.error("No se encontró el producto.");
            return false;
        }
        list_items_cmd[index].comentario = $(input).val();
        list_items_cmd[index].tmp_otro_comentario = tmp_otro_comentario;
        list_items_cmd[index].tmp_ids_comentarios = tmp_ids_comentarios;
        localStorage.setItem('list_items_cmd', JSON.stringify(list_items_cmd));
        load_items();
    }
</script>
<script type="text/javascript">
    $('#btn_reiniciar_cmd').click(function() {
        swal({
            title: "¿Esta seguro de reiniciar?",
            text: "Se perderán los datos ingresados",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "SI, Reiniciar",
            closeOnConfirm: false
        },
        function(){
            remove_local_comanda();
            location.reload();
            return ;
            location.href = '<?= base_url('salon/comanda/reiniciar_comanda') ?>';
        });
    });
    $('#btn_cancelar_cmd').click(function() {
        swal({
            title: "¿Esta seguro de cancelar?",
            text: "Se perderán los datos ingresados",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, regresar a salón.",
            closeOnConfirm: false
        },
        function(){
            remove_local_comanda();
            location.href = '<?= base_url('salon/salon') ?>';
        });
    });
    function seleccionar_cliente(cliente_id){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('administracion/clientes/get_one_ajax')?>",
            data: {'cliente_id':cliente_id},
            success: function(rpta){
                $('#cliente_id').val("");
                $('#cliente_documento').val("");
                $('#cliente_nombre').val("");
                $('#cliente_telefono').val("");
                $('#cliente_direccion').val("");
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    objeto = JSON.parse(rpta);
                    var cliente_id = objeto[0].cliente_id;
                    var cliente_documento = objeto[0].documento;
                    var cliente_nombre = objeto[0].nombre_completo;
                    var cliente_telefono = objeto[0].telefono;
                    var cliente_direccion = objeto[0].direccion;
                    $('#cliente_id').val(cliente_id);
                    $('#cliente_documento').val(cliente_documento);
                    $('#cliente_nombre').val(cliente_nombre);
                    $('#cliente_telefono').val(cliente_telefono);
                    $('#cliente_direccion').val(cliente_direccion);
                }
                $('#modal_clientes_select').modal('hide');
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function limpiar_datos_consulta(){
        // $('#documento').val("");
        $('#cliente_id').val("");
        $('#cliente_documento').val("");
        $('#cliente_nombre').val("");
        $('#cliente_telefono').val("");
        $('#cliente_direccion').val("");

        $('#cliente_documento').attr('readonly', false);
        $('#cliente_nombre').attr('readonly', false);

        $('#btn_consultar').show();
        $('#btn_editar').hide();
    }
    function consultar_doc(){
        var documento = $('#cliente_documento').val();
        documento = documento.trim();
        if(documento.length < 5){
            alertify.error("Debe ingresar un número de documento válido.");
            return false;
        }
        var url = '<?= base_url('administracion/clientes/json_buscar_por_documento/')?>' + documento;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(objeto['success']){
                    var sunat_tipo_documento_id = objeto['data'].sunat_tipo_documento_id;
                    var cliente_id  = objeto['data'].cliente_id;
                    var documento   = objeto['data'].documento;
                    var cliente     = objeto['data'].cliente;
                    var nombres     = objeto['data'].nombres;
                    var ape_paterno = objeto['data'].ape_paterno;
                    var ape_materno = objeto['data'].ape_materno;
                    var telefono    = objeto['data'].telefono;
                    var correo      = objeto['data'].correo;
                    var direccion   = objeto['data'].direccion;
                    var ubigeo      = objeto['data'].ubigeo;
                    var direccion_ubigeo = (ubigeo == "") ? direccion : direccion + " - " + ubigeo;

                    $('#cliente_id').val(cliente_id);
                    $('#cliente_nombre').val(cliente);
                    $('#cliente_telefono').val(telefono);
                    $('#cliente_direccion').val(direccion_ubigeo);

                    $('#btn_consultar').hide();
                    $('#btn_editar').show();
                    $('#cliente_documento').attr('readonly', true);
                    $('#cliente_nombre').attr('readonly', true);
                }else{
                    alertify.error(objeto['msg']);
                }
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>

<script type="text/javascript">
    $("#ir_atras").removeClass("hidden");
</script>
