<!-- 
	Recibe:
	- $oComanda
	- $listaMesas
-->
<?= form_open(base_url('salon/salon/unir_mesas'), 'class="form-horizontal" id="form_unir_mesas"'); ?>
    <div id="modal_unir_mesas" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="smaller lighter blue no-margin center"> <strong> Unir <?= $oComanda->mesa ?> con: </strong> </h3>
                </div>
                <div class="modal-body" >
                    <!-- Listado de mesas -->
                    <input type="" name="comanda_id_para_unir"  value="<?= $oComanda->comanda_id ?>" class="hidden">
                    <div class="row" style="text-align: center">
						<?php foreach ($listaMesas as $key => $mesa): ?>
							<?php if ($mesa->estado == 'L'): ?>
								<div class="col-sm-4">
									<div class="radio">
										<label>
											<input name="mesas[]" value="<?= $mesa->mesa_id ?>" type="checkbox" class="ace"/>
											<span class="lbl"> <?= $mesa->mesa ?> </span>
										</label>
									</div>
								</div>
							<?php endif ?>
						<?php endforeach ?>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm" id="btn_unir_mesas">
                        <i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
                        Unir mesas
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?= form_close(); ?>

<script type="text/javascript">
	 $('#modal_unir_mesas').modal({
        show: 'false'
    }); 
</script>