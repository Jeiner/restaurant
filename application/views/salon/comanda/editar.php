<?php  
    $importe_total = 0;
    $porcentaje_igv = $igv * 100;
?>
<!-- Titulo -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">            
            <div class="row">
                <div class="col-sm-6">
                    <h1 style="font-weight: 500">
                        <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                        Editar comanda
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Código de comanda: <?= $oComanda->comanda_id ?>
                        </small>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Detalle de la comanda -->
<div class="row">
    <div class="col-sm-7">
        <div class="widget-box widget-color-granate"> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-shopping-cart bigger-90" aria-hidden="true"></i>
                    Detalle de la comanda
                    <div class="pull-right">
                        <div class="btn-group ">
                            <button data-toggle="dropdown" class="btn btn-warning btn-sm btn-sm-opciones"> Opciones 
                                <span class="ace-icon fa fa-caret-down icon-only"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-grey dropdown-menu-right">
                                <li>
                                    <a href="javascript:;" style="padding: 15px;" onclick="abrir_modal_editar_datos_generales_comanda('<?= $oComanda->comanda_id ?>');"  > 
                                        <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                                        EDITAR DATOS GENERALES COMANDA
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" style="padding: 15px;" onclick="abrir_modal_anular_comanda();"  > 
                                        <i class="ace-icon fa fa-times bigger-90" aria-hidden="true"></i>
                                        ANULAR COMANDA
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" style="padding: 15px;" onclick="abrir_modal_unir_mesas('<?= $oComanda->comanda_id ?>');"> 
                                        <i class="ace-icon fa fa-object-group bigger-90" aria-hidden="true"></i>
                                        UNIR MESAS
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;" style="padding: 15px;" onclick="abrir_modal_cambiar_mesa('<?= $oComanda->comanda_id ?>');"> 
                                        <i class="ace-icon fa fa-exchange bigger-90" aria-hidden="true"></i>
                                        CAMBIAR MESA(S)
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </h6>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                    <!-- Datos generales -->
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Fecha y hora</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->fecha_comanda ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Modalidad</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->modalidad_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Nro Comensales.</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->nro_comensales ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Canal</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->canal_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Mesa</label>
                                <input type="text" name="mesa" id="mesa" class="form-control input-sm center" value="<?= $oComanda->mesa ?>" readonly >
                                <input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm hidden" value="<?= $oComanda->mesa_id ?>" readonly >
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Mozo</label>
                                <input type="text" name="mozo" id="mozo" class="form-control input-sm center" value="<?= strtoupper($oComanda->mozo) ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Atención</label><br>
                                <?php if ($oComanda->estado_atencion == 'E'): ?>
                                    <span class="label label-warning btn-block"> En espera </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'A'): ?>
                                    <span class="label label-primary btn-block"> Atendido </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'F'): ?>
                                    <span class="label label-success btn-block"> Finalizado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label>Pago</label><br>
                                <?php if ($oComanda->estado_pago == 'P'): ?>
                                    <span class="label label-danger btn-block"> Pendiente </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'C'): ?>
                                    <span class="label label-success btn-block"> Pagado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Items-detalle -->
                    <div class="row" id="productos_seleccionados" style="overflow-x: scroll;">
                        <div class="col-sm-12">
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th style="text-align: center; width: 25%"> Producto </th>
                                        <th style="text-align: center; width: 10%"> Espera </th>
                                        <th style="text-align: center; width: 10%"> P. Unit. </th>
                                        <th style="text-align: center; width: 5%"> Cantidad </th>
                                        <th style="text-align: center; width: 10%"> Importe </th>
                                        <th style="text-align: center; width: 10%"> Estado </th>
                                        <th style="text-align: center; width: 10%" class="hidden-xs"> Despachar </th>
                                        <th style="text-align: center; width: 10%"> X </th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php foreach ($oComanda->items as $key => $item): ?>
                                        <?php 
                                            $importe_total += $item->precio_unitario * $item->cantidad;
                                            $importe = number_format(round($item->precio_unitario * $item->cantidad,2), 2, '.', ' '); 
                                        ?>
                                        <tr>
                                            <td style="text-align: left;vertical-align: middle;"> 
                                                <strong>
                                                    <?= strtoupper($item->producto) ?> 
                                                </strong>
                                                <?php if ($item->comentario != ""): ?>
                                                    <div class="label-warning comentario_pedido">
                                                        <?= strtoupper($item->comentario) ?> 
                                                    </div>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle; ">
                                                <!-- Obtenemos el tiempo de espera o lo calculamos -->
                                                <?php if ($item->tiempo_espera): ?>
                                                    <?php $minutos = $item->tiempo_espera ?>
                                                <?php else: ?>
                                                    <?php $minutos = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") ); ?>
                                                <?php endif ?>
                                                <!-- Mostrarmos los minutos calculados  -->
                                                <?php if ($minutos < 15): ?>
                                                    <?= $minutos.' min' ?>
                                                <?php endif ?>
                                                <?php if ($minutos >= 15 && $minutos < 30): ?>
                                                    <span class="label label-warning"> <?= $minutos.' min' ?> </span>
                                                <?php endif ?>
                                                <?php if ($minutos >= 30): ?>
                                                    <span class="label label-danger"> <?= $minutos.' min' ?> </span>
                                                <?php endif ?>
                                            </td>
                                            <td style="vertical-align: middle;" class="dinero">
                                                <?= number_format(round($item->precio_unitario,2), 2, '.', ' ');  ?>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;"> 
                                                <?= $item->cantidad ?> 
                                            </td>
                                            <td style="vertical-align: middle;" class="dinero">
                                               <?= $importe ?>
                                            </td>
                                            <td style="vertical-align: middle;text-align: center">
                                                <?php switch ($item->estado_atencion) {
                                                    case CG::M_39_ItemEnEspera:
                                                        ?> 
                                                            <span class="label label-warning" title="En espera"> Espera </span>
                                                        <?php
                                                        break;
                                                    case CG::M_39_ItemEnPreparacion:
                                                        ?> 
                                                            <span class="label label-primary" title="En preparación"> Prepar. </span>
                                                        <?php
                                                        break;
                                                    case CG::M_39_ItemDespachado:
                                                        ?> 
                                                            <span class="label label-success" title="Despachado"> Despac. </span>
                                                        <?php
                                                        break;
                                                    case CG::M_39_ItemAnulado:
                                                        ?> 
                                                            <span class="label label-inverse" title="Anulado"> Anulado </span>
                                                        <?php
                                                        break;
                                                    default:
                                                        ?> 
                                                            Sin etiqueta
                                                        <?php
                                                        break;
                                                } ?>
                                            </td>
                                            <td style="text-align: center" class="hidden-xs">
                                                <?php if ($item->estado_atencion == CG::M_39_ItemEnEspera || 
                                                        $item->estado_atencion == CG::M_39_ItemEnPreparacion): ?>
                                                    <a class="btn btn-success btn-sm" href="<?= base_url('cocina/cocina/despachar_pedido/');?><?= $item->comanda_item_id ?>">
                                                        Despachar
                                                    </a>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center">
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn dropdown-toggle btn-inverse btn-sm" aria-expanded="true">
                                                        <span class="ace-icon fa fa-caret-down icon-only"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-grey dropdown-menu-right">
                                                        <li>
                                                            <?php $url_anular = base_url('salon/comanda/anular_item/').$item->comanda_item_id ?>
                                                            <?php $mensaje = "¿Está seguro de anular el item: ".strtoupper($item->producto)."?" ?>
                                                            <span class="" data-rel="tooltip" title="Pulsa para anular" >
                                                                <button href="<?= $url_anular ?>" 
                                                                        onclick="alertToDelete(this)" 
                                                                        msg="<?= $mensaje ?>" 
                                                                        titleMsg="Anular item" 
                                                                        class="btn btn-link"
                                                                        style="margin: 5px;">
                                                                    <i class="ace-icon fa fa-times bigger-90" aria-hidden="true"></i>
                                                                    Anular item
                                                                </button>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <?php if ($item->estado_atencion == CG::M_39_ItemEnEspera || 
                                                                    $item->estado_atencion == CG::M_39_ItemEnPreparacion): ?>
                                                                <button href="<?= base_url('cocina/cocina/despachar_pedido/');?><?= $item->comanda_item_id ?>"
                                                                    class="btn btn-link"  
                                                                    style="margin: 5px;">
                                                                    <i class="ace-icon fa fa-check bigger-90" aria-hidden="true"></i>
                                                                    Despachar
                                                                </button>
                                                            <?php endif ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <?php 
                            $importe_total = number_format(round($importe_total,2), 2, '.', ' ');
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-3">
                            
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-9">
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Importe Sin IGV
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="importe_sin_IGV" id="importe_sin_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->importe_sin_IGV ?>" readonly>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Total IGV (<?= $porcentaje_igv ?>%)
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="total_IGV" id="total_IGV" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->total_IGV ?>" readonly>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px">
                                <div class="col-sm-6 col-xs-6" style="">
                                    <label class="" style="font-size: 16px; color: #000; ">
                                        <strong>Total</strong>
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <input type="text" class="form-control" name="importe_total" id="importe_total" style="font-size: 18px!important;text-align:right;font-weight: bold;" value="<?= $oComanda->importe_total ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <textarea class="form-control input-sm" placeholder="" id="cmd_comentario" name="cmd_comentario" disabled="true"><?= $oComanda->cmd_comentario ?></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-2 col-xs-3" title="Imprimir en cocina">
                            <div class="form-group">
                                <a href="<?= base_url('impresion/ticketera/imprimir_ticket_cocina/').$oComanda->comanda_id ?>" class="btn btn-app btn-primary btn-sm" target='_blanck'>
                                    <i class="ace-icon fa fa-print bigger-160"></i>
                                    Cocina
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-3  col-sm-offset-2">
                            <?php if ($oGeneralidades->emitir_cpbe): ?>
                                <?php 
                                    $hab_btn_cpbe = ($oComanda->estado_pago != 'P' && $oComanda->con_comprobante_e != 'C') ? "" : "disabled";
                                ?>
                                <div class="form-group" title="Generar comprobante electrónico">
                                    <button onclick="ir_a_generar_comprobante(<?= $oComanda->comanda_id ?>);" 
                                            <?= $hab_btn_cpbe ?>
                                            class="btn btn-app btn-pink btn-sm"
                                            style="background-color: #AF0C41 !important">
                                        <img src="<?= base_url('assets/img/sunat.png') ?>" height="34"><br>
                                        CPBe
                                    </button>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-2 col-xs-3">
                            <?php 
                                $hab_btn_pago = ($oComanda->estado_pago == CG::M_5_PagoPendiente) ? "" : "disabled";
                            ?>
                            <div class="form-group" title="Facturar comanda">
                                <button onclick="ir_a_pagar(<?= $oComanda->comanda_id ?>);" 
                                        <?= $hab_btn_pago ?>
                                        class="btn btn-app btn-primary btn-sm" >
                                    <i class="ace-icon fa fa-money bigger-160"></i>
                                    Facturar
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-3" title="Imprimir cuenta">
                            <a href="<?= base_url('impresion/ticketera/imprimir_cuenta_local/').$oComanda->comanda_id ?>" class="btn btn-app btn-primary btn-sm" target='_blanck'>
                                <i class="ace-icon fa fa-print bigger-160"></i>
                                Imprimir
                            </a>
                        </div>
                        <div class="col-sm-2 col-xs-3" title="Finalizar comanda">
                            <?php if ($oComanda->estado_atencion == CG::M_4_AtencionEnEspera || 
                                    $oComanda->estado_atencion == CG::M_4_AtencionAtendido): ?>
                                <div class="form-group">
                                    <?php $ruta_finalizar = base_url('salon/comanda/finalizar_comanda').'?comanda_id='.$oComanda->comanda_id  ?>
                                    <a href="<?= $ruta_finalizar ?>" class="btn btn-app btn-success btn-sm">
                                        <i class="ace-icon fa fa-check bigger-160"></i>
                                        Finalizar
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Agregar producto -->
    <div class="col-sm-5">
        <div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                    Productos adicionales
                </h6>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12" style="text-align: right;">
                            <button class="btn btn-primary btn-sm" onclick="abrir_modal_selec_prod_adicional()">
                                Seleccionar producto adicional
                            </button>
                        </div>
                    </div>

                    <?= form_open(base_url('salon/comanda/agregar_producto_adicional'), 'id="form_adicional"'); ?>
                        <div class="row" id="div_agregar_nuevo_producto">
                            
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label><strong>Producto</strong></label>
                                    <input type="text" name="add_producto" id="add_producto" class="form-control input-sm" readonly>
                                </div>
                            </div>

                             <div class="col-sm-12 col-xs-12 hidden">
                                <div class="form-group">
                                    <label><strong>Producto ID</strong></label>
                                    <input type="text" name="add_producto_id" id="add_producto_id" class="form-control input-sm" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group hidden">
                                    <label>comanda ID</label>
                                    <input type="text" name="comanda_id" id="comanda_id" class="form-control input-sm " value="<?= $oComanda->comanda_id ?>">
                                </div>
                                <div class="form-group">
                                    <label><strong>P. unitario</strong></label>
                                    <input type="text" name="add_precio_unitario" id="add_precio_unitario" class="form-control input-sm dinero-total" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label><strong>Cantidad</strong></label>
                                    <input type="number" name="add_cantidad" id="add_cantidad" class="form-control input-sm center" onkeypress="return soloNumeroEntero(event)" onchange="ingresar_cantidad();" style="font-weight: bold">
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <div class="form-group">
                                    <label><strong>Importe</strong></label>
                                    <input type="text" name="add_importe" id="add_importe" class="form-control input-sm dinero-total" readonly>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <textarea class="form-control input-sm" id="add_comentario" name="add_comentario" placeholder="Ingresar algún comentario o descripción."></textarea>
                            </div> 
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="pull-right">
                                    <button  class="btn btn-primary btn-sm" id="btn_guardar_adicional">
                                        <i class="ace-icon fa fa-plus bigger-90"></i>
                                        Agregar producto
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
        
        <!-- Datos del cliente -->
        <?php $this->view('administracion/clientes/_view_box_cliente'); ?>

        <!-- Pagos de la comanda -->
        <?php $this->view('facturacion/pagos/_view_caja_datos_pago'); ?>

        <!-- Documentos electrónicos -->
        <?php $this->view('facturacion/documento_electronico/_view_box_documentos_e'); ?>

        <!-- Documentos electrónicos -->
        <?php $this->view('facturacion/documento_electronico/_view_box_documentos_e_bajas'); ?>

        <!-- Panel -->
    </div>
</div>

<!-- Modales -->
<div id="div_modal_anular_comanda">
    <?= form_open(base_url('salon/comanda/anular_comanda'), 'class="form-horizontal" id="form_anular_comanda"'); ?>
        <div id="modal_anular_comanda" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="smaller lighter blue no-margin center"> <strong> Anular comanda </strong> </h3>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="form-group hidden">
                                    <label>Comanda id</label>
                                    <input type="text" name="anular_comanda_id" value="<?= $oComanda->comanda_id ?>">
                                </div>
                                <div class="form-group">
                                    <label> Motivo / Descripción <label class="red">*</label> </label>
                                    <textarea class="form-control input-sm" rows="4" name="anular_motivo" id="anular_motivo" placeholder="Ingrese una descripción indicando el motivo por el cual se anulará la comanda."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <em>
                                    Para poder anular una comanda, esta no debe estar pagada y todos sus pedidos deben estar pendientes de atención.
                                </em>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-sm" id="btn_anular_comanda" >
                            <i class="ace-icon fa fa-arrow-left bigger-90" aria-hidden="true"></i>
                            Anular
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    <?= form_close(); ?>
</div>


        <!-- Documentos electrónicos -->
        <?php $this->view('administracion/productos/_modal_seleccionar_producto'); ?>


<div id="div_modal_editar_datos_generales">
    <!-- Código del modal para editar datos genrales -->
</div>
<div id="div_modal_unir_mesas">
    <!-- Código del modal para unir mesas -->
</div>
<div id="div_modal_cambiar_mesa">
    <!-- Código del modal para cambiar mesas -->
</div>

<script type="text/javascript">

    $('.chosen-select').chosen({allow_single_deselect:true});
    
    $("#form_unir_mesas").submit(function() {
        $('#btn_unir_mesas').attr('disabled', true);
        abrirCargando();
        return true;
    });
    $("#form_adicional").submit(function() {
        $('#btn_guardar_adicional').attr('disabled', true);
        var cantidad = $('#add_cantidad').val();
        var precio_unitario = $('#add_precio_unitario').val();
        if (precio_unitario == "") {
            alertify.error("Seleccione un producto");
            $('#btn_guardar_adicional').attr('disabled', false);
            return false;
        }
        if (cantidad == "" || cantidad == 0) {
            alertify.error("Ingrese una cantidad");
            $('#btn_guardar_adicional').attr('disabled', false);
            return false;
        }
        abrirCargando();
        return true;
    });
    // Validar formulario de anular comanda
    $("#form_anular_comanda").submit(function() {
        $('#btn_anular_comanda').attr('disabled', true);
        abrirCargando();
        return true;
    });
    $("#form_anular_comanda").validate({
        rules: {
            anular_motivo: "required"
        },
        messages:{
            anular_motivo: "Debe ingresar un motivo"
        }
    });
    $('#form_anular_comanda').on('keyup blur', function () { // fires on every keyup & blur
        if ($('#form_anular_comanda').valid()) {                   // checks form for validity
            $('#btn_anular_comanda').attr('disabled', false);        // enables button
        } else {
            $('#btn_anular_comanda').attr('disabled', true);   // disables button
        }
    });
    function imprimir_cuenta(comanda_id){
        url = '<?= base_url("salon/comanda/imprimir_cuenta/") ?>' + comanda_id;        
        $.ajax({
            type: 'POST',
            url: url,
            data: "",
            success: function(rpta){
                var pdf_link = "<?=base_url()?>reportes_pdf/cuenta_comanda.pdf";
                var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="100%">No Support</object>';
                $.createModal({
                    title: "",
                    message: iframe,
                    closeButton:true,
                    scrollable:false
                });
            },
            error: function(rpta){
                alert("Error al cargar la lista "+rpta);
            }
        });
        return false;
    }
    function abrir_modal_anular_comanda(){
        swal({
            title: "¿Seguro de anular la comanda?",
            text: "Si está seguro, ingrese el código de seguridad",
            type: "input",
            inputType: "password",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Código de seguridad"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Es obligatorio ingresar el código!");
                return false
            }
            verificar_codigo_de_seguridad(inputValue);
        });
    }
    function verificar_codigo_de_seguridad(codigo){
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/generalidades/verificar_codigo_de_seguridad')?>",
            data: {'codigo': codigo},
            success: function(rpta){
                if(rpta.substring(0,1) == "{"){
                    objeto = JSON.parse(rpta);
                    if(!objeto.success){
                        alertify.error(objeto.mensaje);
                    }
                }else{
                    if (rpta == 1) {
                        $('#modal_anular_comanda').modal({
                            show: 'false'
                        }); 
                        swal.close()
                    }else{
                         alertify.error("El código no es válido");
                    }
                }
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
    function preselec_producto(combo){
        var precio_unitario = $( "#add_producto_id option:selected" ).attr('precio_unitario');
        $('#add_precio_unitario').val(precio_unitario);
        $("#add_cantidad" ).val("1");
        $("#add_importe" ).val(precio_unitario);
        $("#add_cantidad" ).focus();
    }
    function ingresar_cantidad(){
        var cantidad = parseFloat($('#add_cantidad').val());
        var precio_unitario = parseFloat($('#add_precio_unitario').val());
        $('#add_cantidad').val(cantidad);
        if(isNaN(cantidad)){
            alertify.error("Debe ingresar un número válido.");
            $('#add_cantidad').val("0");
        }else{
            importe = parseFloat(cantidad * precio_unitario).toFixed(2);
            $('#add_importe').val(importe);
        }
    }
    function abrir_modal_unir_mesas(comanda_id){
        $('#div_modal_unir_mesas').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('salon/salon/ajax_cargar_modal_unir_mesas')?>",
            data: {'comanda_id': comanda_id},
            success: function(rpta){
                $('#div_modal_unir_mesas').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }

    function abrir_modal_editar_datos_generales_comanda(comanda_id){
        $('#div_modal_editar_datos_generales').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('salon/salon/ajax_cargar_modal_editar_datos_generales_comanda')?>",
            data: {'comanda_id': comanda_id},
            success: function(rpta){
                $('#div_modal_editar_datos_generales').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }

    function abrir_modal_cambiar_mesa(comanda_id){
        $('#div_modal_cambiar_mesa').html("");
        $.ajax({
            type: 'POST',
            url: "<?=base_url('salon/salon/ajax_cargar_modal_cambiar_mesa')?>",
            data: {'comanda_id': comanda_id},
            success: function(rpta){
                cerrarCargando();
                $('#div_modal_cambiar_mesa').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }

    //Funciones para redirección
    function ir_a_pagar(comanda_id){
        var url = "<?=base_url('facturacion/pagos/nuevo/')?>" + comanda_id;
        window.location.href = url;
    }

    function ir_a_generar_comprobante(comanda_id){
        var url = "<?=base_url('facturacion/documento_electronico/nuevo_documento/')?>" + comanda_id;
        window.location.href = url;
    }
</script>
<script type="text/javascript">
    $("#ir_atras").removeClass("hidden");
</script>

<script type="text/javascript">
    function abrir_modal_selec_prod_adicional(){
        $('#modal_seleccionar_producto').modal({
            show: 'false'
        });
    }
</script>