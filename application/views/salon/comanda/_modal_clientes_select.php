<div id="modal_clientes_select" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> <strong> Seleccionar cliente </strong> </h3>
            </div>
            <div class="modal-body">
               <table class="table table-bordered" id="tabla-clientes" width="100%">
                    <thead>
                        <tr>
                            <th style="text-align: center" class="hidden-xs"> Cód </th>
                            <th style="text-align: center"> Documento </th>
                            <th style="text-align: center"> Nombres/Razón social</th>
                            <th style="text-align: center" class="hidden-xs"> Tipo </th>
                            <th style="text-align: center" class="hidden-xs"> Teléfono </th>
                            <th style="text-align: center"> Seleccionar </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($clientes as $key => $cliente): ?>
                            <tr>
                                <td style="text-align: center" class="hidden-xs"> <?= $cliente->cliente_id ?> </td>
                                <td style="text-align: left"> <?= $cliente->documento ?> </td>
                                <td style="text-align: left;"> <?= $cliente->nombre_completo ?></td>
                                <td style="text-align: center" class="hidden-xs"> 
                                    <?php if ($cliente->tipo_cliente == 'N'): ?>
                                        Natural
                                    <?php endif ?>
                                    <?php if ($cliente->tipo_cliente == 'J'): ?>
                                        Jurídico
                                    <?php endif ?>
                                </td>
                                <td style="text-align: center" class="hidden-xs"> <?= $cliente->telefono ?> </td>
                                <td style="text-align: center"> 
                                    <button type="button" class="btn btn-minier btn-primary" onclick="seleccionar_cliente(<?= $cliente->cliente_id ?>)">
                                        Seleccionar
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
               </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">
                    <i class="ace-icon fa fa-arrow-left bigger-90" aria-hidden="true"></i>
                    Cancelar
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    $('#tabla-clientes').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "5%", },    //Código
          { "targets": [ 1 ],"width": "15%", },   //Docum
          { "targets": [ 2 ],"width": "20%", },   //Nombre
          { "targets": [ 3 ],"width": "15%", },   //Tipo
          { "targets": [ 4 ],"width": "15%", },   //Telefono
          { "targets": [ 5 ],"width": "15%", },   //Sele
        ],
        "order": [[ 2, "asc" ]] ,
    });
    $('#tabla-clientes tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>