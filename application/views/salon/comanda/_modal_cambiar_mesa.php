<!-- 
    Recibe 
    $oComanda: Datos de la comanda
    $listaUniones: Listado de uniones de mesas
    $listaMesas: Listado de messas
-->
<?= form_open(base_url('salon/salon/cambiar_mesa'), 'class="form-horizontal" id="form_cambiar_mesa"'); ?>
    <div id="modal_cambiar_mesa" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="smaller lighter blue no-margin center"> <strong> Cambiar mesa </strong> </h3>
                </div>
                <div class="modal-body" >
                    <!-- Listado de mesas -->
                    <input type="" name="cambiar_mesa_comanda_id"  value="<?= $oComanda->comanda_id ?>" class="hidden">
                    <div class="row center">
                        <!-- Mesas actules  -->
                        <div class="col-sm-4 col-xs-6 col-sm-offset-2">
                            <h5 class="smaller lighter blue no-margin center"> <strong> Actuales </strong> </h5>
                            <br>
                            <?php if ($oComanda->union_mesas == 0): ?>
                                <label>
                                    <input name="cambiar_mesa_actual[]" value="<?= $oComanda->mesa_id ?>" type="radio" class="ace"/>
                                    <span class="lbl blue"> <?= $oComanda->mesa ?> </span>
                                </label>
                                <br>
                            <?php else: ?>
                                    <?php foreach ($listaMesas as $key => $mesa): ?>
                                        <?php foreach ($listaUniones as $key2 => $union): ?>
                                            <?php if ($key == 0 && $key2 == 0): ?>
                                                <label>
                                                    <input name="cambiar_mesa_actual[]" value="<?= $union->mesa_principal_id ?>" type="radio" class="ace"/>
                                                    <span class="lbl blue"> <?= $union->mesa_principal ?> </span>
                                                </label>
                                                <br>
                                            <?php endif ?>
                                            <?php if ($mesa->mesa_id == $union->mesa_unida_id): ?>
                                                <label>
                                                    <input name="cambiar_mesa_actual[]" value="<?= $mesa->mesa_id ?>" type="radio" class="ace"/>
                                                    <span class="lbl"> <?= $mesa->mesa ?> </span>
                                                </label>
                                                <br>
                                                <?php break; ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                            <?php endif ?>
                        </div>
                        <!-- Mesas Libres -->
                        <div class="col-sm-4 col-xs-6">
                            <h5 class="smaller lighter blue no-margin center"> <strong> Libres </strong> </h5>
                            <br>
                            <?php foreach ($listaMesas as $key => $libre): ?>
                                <?php if ($libre->estado == 'L'): ?>
                                     <label>
                                        <input name="cambiar_mesa_nueva[]" value="<?= $libre->mesa_id ?>" type="radio" class="ace"/>
                                        <span class="lbl"> <?= $libre->mesa ?> </span>
                                    </label>
                                    <br>
                                <?php endif ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm" id="btn_cambiar_mesa">
                        <i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
                        Actualizar mesas
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
<?= form_close(); ?>

<script type="text/javascript">
    $('#modal_cambiar_mesa').modal({
        show: 'false'
    }); 
</script>