<?php  
    $importe_total = 0;
    $porcentaje_igv = $igv * 100;
?>
<!-- Titulo -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header">            
            <div class="row">
                <div class="col-sm-6">
                    <h1 style="font-weight: 500">
                        <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
                        Ver comanda
                        <small>
                            <i class="ace-icon fa fa-angle-double-right"></i>
                            Código de comanda: <?= $oComanda->comanda_id ?>
                        </small>
                    </h1>
                </div>
                <div class="col-sm-6">
                     <div class="pull-right">
                        <a href="javascript:window.history.back();" class="">
                            <i class="ace-icon fa fa-arrow-left bigger-90" aria-hidden="true"></i>
                            Volver atrás
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Detalle de comanda -->
<div class="row">
    <!-- DATOS DE LA COMANDA -->
    <div class="col-sm-7">
        <!-- Datos de la COMANDA -->
        <div class="widget-box widget-color-granate "> <!-- ui-sortable-handle -->
            <div class="widget-header">
                <h6 class="widget-title">
                    <i class="ace-icon fa fa-shopping-cart bigger-90" aria-hidden="true"></i>
                    Detalle de la comanda
                </h6>
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main padding-4" style="position: relative; padding: 15px">
                    <!-- Datos generales -->
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Fecha y hora</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->fecha_comanda ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Modalidad</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->modalidad_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Nro Comensales.</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->nro_comensales ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Canal</label>
                                <input type="text" name="" id="" class="form-control input-sm" value="<?= $oComanda->canal_desc ?>" readonly style="text-align: center" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Mesa</label>
                                <input type="text" name="mesa" id="mesa" class="form-control input-sm center" value="<?= $oComanda->mesa ?>" readonly >
                                <input type="text" name="mesa_id" id="mesa_id" class="form-control input-sm hidden" value="<?= $oComanda->mesa_id ?>" readonly >
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Mozo</label>
                                <input type="text" name="mozo" id="mozo" class="form-control input-sm center" value="<?= strtoupper($oComanda->mozo) ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Atención</label><br>
                                <?php if ($oComanda->estado_atencion == 'E'): ?>
                                    <span class="label label-warning btn-block"> En espera </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'A'): ?>
                                    <span class="label label-primary btn-block"> Atendido </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'F'): ?>
                                    <span class="label label-success btn-block"> Finalizado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_atencion == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Pago</label><br>
                                <?php if ($oComanda->estado_pago == 'P'): ?>
                                    <span class="label label-danger btn-block"> Pendiente </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'C'): ?>
                                    <span class="label label-success btn-block"> Pagado </span>
                                <?php endif ?>
                                <?php if ($oComanda->estado_pago == 'X'): ?>
                                    <span class="label label-inverse btn-block"> Anulado </span>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <!-- Items-detalle -->
                    <div class="row" id="productos_seleccionados">
                        <div class="col-sm-12">
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th style="text-align: center; width: 25%"> Producto </th>
                                        <th style="text-align: center; width: 10%"> Espera </th>
                                        <th style="text-align: center; width: 10%"> P. Unit. </th>
                                        <th style="text-align: center; width: 5%"> Cantidad </th>
                                        <th style="text-align: center; width: 10%"> Importe </th>
                                        <th style="text-align: center; width: 10%"> Estado </th>
                                        <th style="text-align: center; width: 10%" class="hidden-xs"> Despachar </th>
                                        <th style="text-align: center; width: 10%"> X </th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php foreach ($oComanda->items as $key => $item): ?>
                                        <?php 
                                            $importe_total += $item->precio_unitario * $item->cantidad;
                                            $importe = number_format(round($item->precio_unitario * $item->cantidad,2), 2, '.', ' '); 
                                        ?>
                                        <tr>
                                            <td style="text-align: left;vertical-align: middle;"> 
                                                <strong>
                                                    <?= strtoupper($item->producto) ?> 
                                                </strong>
                                                <?php if ($item->comentario != ""): ?>
                                                    <div class="label-warning comentario_pedido">
                                                        <?= strtoupper($item->comentario) ?> 
                                                    </div>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle; ">
                                                <!-- Obtenemos el tiempo de espera o lo calculamos -->
                                                <?php if ($item->tiempo_espera): ?>
                                                    <?php $minutos = $item->tiempo_espera ?>
                                                <?php else: ?>
                                                    <?php $minutos = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") ); ?>
                                                <?php endif ?>
                                                <!-- Mostrarmos los minutos calculados  -->
                                                <?php if ($minutos < 15): ?>
                                                    <?= $minutos.' min' ?>
                                                <?php endif ?>
                                                <?php if ($minutos >= 15 && $minutos < 30): ?>
                                                    <span class="label label-warning"> <?= $minutos.' min' ?> </span>
                                                <?php endif ?>
                                                <?php if ($minutos >= 30): ?>
                                                    <span class="label label-danger"> <?= $minutos.' min' ?> </span>
                                                <?php endif ?>
                                            </td>
                                            <td style="vertical-align: middle;" class="dinero">
                                                <?= number_format(round($item->precio_unitario,2), 2, '.', ' ');  ?>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;"> 
                                                <?= $item->cantidad ?> 
                                            </td>
                                            <td style="vertical-align: middle;" class="dinero">
                                               <?= $importe ?>
                                            </td>
                                            <td style="vertical-align: middle;text-align: center">
                                                <?php switch ($item->estado_atencion) {
                                                    case CG::M_39_ItemEnEspera:
                                                        ?> 
                                                            <span class="label label-warning" title="En espera"> Espera </span>
                                                        <?php
                                                        break;
                                                    case CG::M_39_ItemEnPreparacion:
                                                        ?> 
                                                            <span class="label label-primary" title="En preparación"> Prepar. </span>
                                                        <?php
                                                        break;
                                                    case CG::M_39_ItemDespachado:
                                                        ?> 
                                                            <span class="label label-success" title="Despachado"> Despac. </span>
                                                        <?php
                                                        break;
                                                    case CG::M_39_ItemAnulado:
                                                        ?> 
                                                            <span class="label label-inverse" title="Anulado"> Anulado </span>
                                                        <?php
                                                        break;
                                                    default:
                                                        ?> 
                                                            Sin etiqueta
                                                        <?php
                                                        break;
                                                } ?>
                                            </td>
                                            <td style="text-align: center" class="hidden-xs">
                                                <?php if ($item->estado_atencion == CG::M_39_ItemEnEspera || 
                                                        $item->estado_atencion == CG::M_39_ItemEnPreparacion): ?>
                                                    <a class="btn btn-success btn-sm" href="<?= base_url('cocina/cocina/despachar_pedido/');?><?= $item->comanda_item_id ?>">
                                                        Despachar
                                                    </a>
                                                <?php endif ?>
                                            </td>
                                            <td style="text-align: center">
                                                <div class="btn-group">
                                                    <button data-toggle="dropdown" class="btn dropdown-toggle btn-inverse btn-sm" aria-expanded="true">
                                                        <span class="ace-icon fa fa-caret-down icon-only"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-grey dropdown-menu-right">
                                                        <li>
                                                            <?php $url_anular = base_url('salon/comanda/anular_item/').$item->comanda_item_id ?>
                                                            <?php $mensaje = "¿Está seguro de anular el item: ".strtoupper($item->producto)."?" ?>
                                                            <span class="" data-rel="tooltip" title="Pulsa para anular" >
                                                                <button href="<?= $url_anular ?>" 
                                                                        onclick="alertToDelete(this)" 
                                                                        msg="<?= $mensaje ?>" 
                                                                        titleMsg="Anular item" 
                                                                        class="btn btn-link"
                                                                        style="margin: 5px;">
                                                                    <i class="ace-icon fa fa-times bigger-90" aria-hidden="true"></i>
                                                                    Anular item
                                                                </button>
                                                            </span>
                                                        </li>
                                                        <li>
                                                            <?php if ($item->estado_atencion == CG::M_39_ItemEnEspera || 
                                                                    $item->estado_atencion == CG::M_39_ItemEnPreparacion): ?>
                                                                <button href="<?= base_url('cocina/cocina/despachar_pedido/');?><?= $item->comanda_item_id ?>"
                                                                    class="btn btn-link"  
                                                                    style="margin: 5px;">
                                                                    <i class="ace-icon fa fa-check bigger-90" aria-hidden="true"></i>
                                                                    Despachar
                                                                </button>
                                                            <?php endif ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <?php 
                            $importe_total = number_format(round($importe_total,2), 2, '.', ' ');
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-sm-4 col-xs-4">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Total IGV (<?= $porcentaje_igv ?>%)
                                    </label>
                                    <input type="text" class="form-control" name="total_IGV" id="total_IGV" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $oComanda->total_IGV ?>" readonly>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <label class="" style="font-size: 14px; color: #000; ">
                                        Importe Sin IGV
                                    </label>
                                    <input type="text" class="form-control" name="importe_sin_IGV" id="importe_sin_IGV" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $oComanda->importe_sin_IGV ?>" readonly>
                                </div>
                                <div class="col-sm-4 col-xs-4">
                                    <label class="" style="font-size: 16px; color: #000; ">
                                        <strong>Total</strong>
                                    </label>
                                    <input type="text" class="form-control" name="importe_total" id="importe_total" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= $oComanda->importe_total ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <textarea class="form-control input-sm" placeholder="" id="cmd_comentario" name="cmd_comentario" disabled="true"><?= $oComanda->cmd_comentario ?></textarea>
                            </div>
                        </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-2 col-xs-3  col-sm-offset-2">
                            <?php if ($oGeneralidades->emitir_cpbe): ?>
                                <?php 
                                    $hab_btn_cpbe = ($oComanda->estado_pago != 'P' && $oComanda->con_comprobante_e != 'C') ? "" : "";
                                ?>
                                <div class="form-group" title="Generar comprobante electrónico">
                                    <button onclick="ir_a_generar_comprobante(<?= $oComanda->comanda_id ?>);" 
                                            <?= $hab_btn_cpbe ?>
                                            class="btn btn-app btn-pink btn-sm"
                                            style="background-color: #AF0C41 !important">
                                        <img src="<?= base_url('assets/img/sunat.png') ?>" height="34"><br>
                                        CPBe
                                    </button>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-2 col-xs-3" title="Imprimir en cocina">
                            <div class="form-group">
                                <a href="<?= base_url('impresion/ticketera/imprimir_ticket_cocina/').$oComanda->comanda_id ?>" class="btn btn-app btn-primary btn-sm" target='_blanck'>
                                    <i class="ace-icon fa fa-print bigger-160"></i>
                                    Cocina
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-2 col-xs-3" title="Imprimir cuenta">
                            <a href="<?= base_url('impresion/ticketera/imprimir_cuenta_local/').$oComanda->comanda_id ?>" class="btn btn-app btn-primary btn-sm" target='_blanck'>
                                <i class="ace-icon fa fa-print bigger-160"></i>
                                Imprimir
                            </a>
                        </div>
                        <div class="col-sm-2 col-xs-3" title="Imprimir cuenta">
                            <a href="<?= base_url('salon/comanda/editar/').$oComanda->comanda_id ?>" class="btn btn-app btn-primary btn-sm">
                                <i class="ace-icon fa fa-pencil bigger-160"></i>
                                Editar
                            </a>
                        </div>
                    </div>
                    <hr>

                    <a href="javascript:;" onclick="enviar_comanda_nube(<?= $oComanda->comanda_id ?>)">
                        Enviar a la nube
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- DATOS DE COMPROBANTES ELECTRONOCIS -->
    <div class="col-sm-5">
        <!-- Datos del cliente -->
        <?php $this->view('administracion/clientes/_view_box_cliente'); ?>

        <!-- Pagos de la comanda -->
        <?php $this->view('facturacion/pagos/_view_caja_datos_pago'); ?>

        <!-- Documentos electrónicos -->
        <?php $this->view('facturacion/documento_electronico/_view_box_documentos_e'); ?>
        <!-- Panel -->
    </div>
</div>

<script type="text/javascript">
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    function desplegar_datos_cliente(){
        if($('#datos_cliente').hasClass('hidden')){
            $('#datos_cliente').removeClass('hidden');
        }else{
            $('#datos_cliente').addClass('hidden');
        }
    }
    function calcular_saldos(){
        // Calcula los campos totales como importe total, saldo
        var importe_total = parseFloat('<?= $importe_total ?>').toFixed(2);
        var descuento = parseFloat($('#descuento').val()).toFixed(2);
        var total_a_pagar = parseFloat(importe_total - descuento).toFixed(2);
        var efectivo = parseFloat($('#pago_efectivo').val()).toFixed(2);
        var tarjeta = parseFloat($('#pago_tarjeta').val()).toFixed(2);
        var saldo = parseFloat(total_a_pagar - efectivo - tarjeta ).toFixed(2);

        $('#saldo').val(saldo);
        $('#total_a_pagar').val(total_a_pagar);

        $('#descuento').val(descuento);
        $('#pago_efectivo').val(efectivo);
        $('#pago_tarjeta').val(tarjeta);
    }
    function aplicar_descuento(){
        var total = parseFloat('<?= $importe_total ?>').toFixed(2);
        var descuento = parseFloat($('#descuento').val());

        if(isNaN(descuento)){
            alertify.error("Debe ingresar un número válido.");
            $('#descuento').val("0.00");
        }else{
            var total_a_pagar = (total - descuento).toFixed(2);
            if(total_a_pagar < 0){
                alertify.error("Descuento no válido.");
                $('#descuento').val("0.00");
            }else{
                $('#total_a_pagar').val(total_a_pagar);
            }
        }
        calcular_saldos();
    }
    function habilitar_descuento(){
        $('#input_descuento').show('slow');
        $('#opcion_descuento').hide('slow');
    }
    function habilitar_tarjeta(){
        $('#input_tarjeta').show('slow');
        $('#opcion_tarjeta').hide('slow');
    }
    function ingresar_pago(input){
        var efectivo = parseFloat($('#pago_efectivo').val()).toFixed(2);
        var tarjeta = parseFloat($('#pago_tarjeta').val()).toFixed(2);
        var total_a_pagar = parseFloat($('#total_a_pagar').val()).toFixed(2);

        if(isNaN(parseFloat($(input).val()).toFixed(2))){
            alertify.error("Debe ingresar un número válido.");
            $(input).val("0.00");
        }else{
            saldo = total_a_pagar - efectivo - tarjeta;
            saldo = parseFloat(saldo).toFixed(2);
            if(saldo < 0){
                alertify.error("El pago no debe exceder al total a pagar.");
                $(input).val("0.00");
            }else{
                $('#saldo').val(saldo);    
            }
        }
        calcular_saldos();
    }
    // $('#descuento').mask('000,000,000.00', { reverse: true });
    function imprimir_en_cocina(comanda_id){
        var ruta_ticket = "<?= $this->Site->get_ruta_proy_ticket() ?>";
        // PARA IMPRIMIR TICKET
        var url_ticket = ruta_ticket + "imprimir_para_cocina.php?comanda_id=" + comanda_id + "";
        window.open(url_ticket,'_blank', 'width=1000,height=700');
    }
    
    function ir_a_generar_comprobante(comanda_id){
        var url = "<?=base_url('facturacion/documento_electronico/nuevo_documento/')?>" + comanda_id;
        window.location.href = url;
    }
</script>
<script type="text/javascript">
    function enviar_comanda_nube(comanda_id){
        var ruta = '<?= base_url("nube/nube/ajax_popup_enviar_comanda_nube"); ?>' + "?comanda_id=" + comanda_id;
        window.open(ruta,'_blank', 'width=500,height=500');
    }
</script>
<script type="text/javascript">
    $("#ir_atras").removeClass("hidden");
</script>
