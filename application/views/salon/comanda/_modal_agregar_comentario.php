<div id="modal_agregar_comentario" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong>
                        <?= $oProducto->producto ?> 
                        <br>
                        <span style="font-size:12px!important">(<?= $oFamilia->familia ?>)</span>
                    </strong> 
                </h3>
            </div>
            <div class="modal-body" >
                <div class="row">
                <?php foreach ($lstComentarios as $key => $coment): ?>
                    <div class="col-sm-4 form-group">
                        <bctton class="btn btn-sin-seleccion" type="button" style="min-width: 100px!important;" onClick="seleccionarComentario(<?= $coment->comentario_id ?>, '<?= $coment->comentario ?>');" id="btn-comentario-<?= $coment->comentario_id ?>">
                            <?= $coment->comentario ?>
                        </button>
                    </div>
                <?php endforeach ?>
                </div>
                <hr>
                <div>
                    <textarea id="txt_comentario" class="form-control" rows="3" readonly="readonly"></textarea>
                </div>
                <hr>
                <textarea id="txt_otro_comentario" class="form-control" rows="2" placeholder="agregar otro comentario (Opcional)"></textarea>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-6" style="text-align: left">
                        <button type="submit" class="btn btn-danger" id="" onclick="cancelar_cerrar()">
                            Cancelar y cerrar
                        </button>
                    </div>
                    <div class="col-sm-6" style="text-align: right;">
                        <button type="submit" class="btn btn-primary" id="btn_agregar_comentario" onclick="agregar_cerrar()">
                            <i class="ace-icon fa fa-save bigger-90" ></i>
                            Agregar comentario
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    var bg_comentario = '<?= json_encode($lstComentarios) ?>'; //Variable global  de comentarios
    var gb_array_comentarios = JSON.parse(bg_comentario); //Pasamos los comentarios a una variable de javascript array
    //mostrarComentarios();
    cargarComentarios();

    function cargarComentarios(){
        var list_comentario_cmd = [];
        var list_items_cmd = (localStorage.getItem('list_items_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_items_cmd'));
        var producto_id = '<?= $oProducto->producto_id ?>';

        var producto = null;
        $.each(list_items_cmd, function () {
            if(producto_id == this.producto_id){
                producto = this;
            }
        });

        $("#txt_otro_comentario").html(producto.tmp_otro_comentario);
        var array_ids_comentarios = producto.tmp_ids_comentarios.split(",")

        // Buscamos los comentarios
        var comentario = null;
        for (var i = 0 ; i < gb_array_comentarios.length; i++) {
            $.each(array_ids_comentarios, function () {
                if(gb_array_comentarios[i].comentario_id == this){
                    list_comentario_cmd[list_comentario_cmd.length] = gb_array_comentarios[i];
                }
            });
        }

        localStorage.setItem('list_comentario_cmd', JSON.stringify(list_comentario_cmd));
        mostrarComentarios();
    }

    function seleccionarComentario(comentario_id, comentario){
        // Obtenemos la lista de items de la comanda
        var list_comentario_cmd = (localStorage.getItem('list_comentario_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_comentario_cmd'));
        var nro_items = (localStorage.getItem('list_comentario_cmd') === null) ? 0 : list_comentario_cmd.length;

        // Buscamos el comentario
        var comentario = null;
        for (var i = 0 ; i < gb_array_comentarios.length; i++) {
            if (gb_array_comentarios[i].comentario_id == comentario_id){
                comentario = gb_array_comentarios[i];
            }
        }

        // Verificamos si esta seleccionado
        var existe = false; 
        var item = null;
        var index = 0;
        for (var i = 0 ; i < list_comentario_cmd.length; i++) {
            if (list_comentario_cmd[i].comentario_id == comentario_id){
                existe = true;
                item = list_comentario_cmd[i];
                index = i;
                break;
            }
        }

        // Ingresamos nuevos datos
        if (existe){
            // Si esta seleccionado
            delete list_comentario_cmd[index];
            list_comentario_cmd = list_comentario_cmd.filter(function() { return true; });
        }else{
            // Si no esta en la lista
            new_item = comentario;
            list_comentario_cmd[nro_items] = new_item;
        }

        localStorage.setItem('list_comentario_cmd', JSON.stringify(list_comentario_cmd));

        mostrarComentarios();
    }

    function mostrarComentarios(){
        var comentario = "";
        var contador = 0;
        var list_comentario_cmd = (localStorage.getItem('list_comentario_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_comentario_cmd'));

        $.each(list_comentario_cmd, function () {
            comentario += (contador > 0) ? ", " : "";
            comentario += this.comentario;
            contador++;
        });

        $("#txt_comentario").html(comentario);
        pintarComentarios();
    }

    function pintarComentarios(){
        $('.btn-seleccionado').addClass("btn-sin-seleccion");
        $('.btn-seleccionado').removeClass("btn-seleccionado");
        var list_comentario_cmd = (localStorage.getItem('list_comentario_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_comentario_cmd'));
        $.each(list_comentario_cmd, function () {
            var btnID = "#btn-comentario-" + this.comentario_id;
            $(btnID).removeClass("btn-sin-seleccion");
            $(btnID).addClass("btn-seleccionado");
        });
    }

    function agregar_cerrar(){
        var list_comentario_cmd = (localStorage.getItem('list_comentario_cmd') === null) ? []: JSON.parse(localStorage.getItem('list_comentario_cmd'));

        //Armar comentario final
        var comentario =  $("#txt_comentario").val();
        var otro_comentario_original =  $("#txt_otro_comentario").val();
        var otro_comentario =  $("#txt_otro_comentario").val();

        if(comentario != "" && otro_comentario != ""){
            otro_comentario = ", " + otro_comentario;
        }
        var comentario_final = "" + comentario.toString() + otro_comentario.toString();
        $(txt_comentario).html(comentario_final);

        //ids de comentarios a string
        var ids_string = "";
        $.each(list_comentario_cmd, function () {
            ids_string += this.comentario_id.toString() + ",";
        });

        //Cerrar modal
        $('#modal_agregar_comentario').modal('hide');

        //Abrir o cerrar caja comentario
        var producto_id = '<?= $oProducto->producto_id ?>';
        var btn_abrir_modal_comentario = "#btn_abrir_modal_comentario_" + producto_id;
        var td = $(btn_abrir_modal_comentario).parent();
        var caja = $(td).children('.caja_comentario');
        var comentario = $(td).children('.comentario_pedido');
        var textarea_comentario = $(caja).children('textarea');
        var div_otro_comentario = $(caja).children('.tmp_otro_comentario');

        if(comentario_final.trim() != ""){
            $(caja).show('fast');
            $(comentario).hide('fast');
        }else{
            $(caja).hide('fast');
            $(comentario).show('fast');
        }

        //Asignar comentario al item
        $(textarea_comentario).val(comentario_final);
        set_comentario(textarea_comentario, producto_id, otro_comentario_original, ids_string);
    }

    function cancelar_cerrar(){
        //Cerrar modal
        $('#modal_agregar_comentario').modal('hide');
    }


</script>