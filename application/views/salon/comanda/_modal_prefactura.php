<div id="modal_prefactura" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> <strong> Pre-Facturación </strong> </h3>
            </div>
            <?= form_open(base_url('salon/comanda/prefacturar'), 'class="form-horizontal" id="myform"'); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="form-group hidden">
                            <label class="control-label" for=""> Comanda_id </label>
                            <input type="text" name="comanda_id" id="comanda_id" class="form-control input-sm" value="<?= $comanda_id ?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for=""> Importe total </label>
                            <input type="text" name="importe_total" id="importe_total" class="form-control input-sm" placeholder="" onkeypress="return soloNumeroDecimal(event)" readonly="readonly" value="<?= $oComanda->importe_total ?>">
                        </div>
                        <div class="form-group ">
                            <label class="control-label" for=""> Comprobante </label>
                            <select class="form-control input-sm" name="tipo_comprobante" id="tipo_comprobante">
                                <option value="">Seleccionar...</option>
                                <?php 
                                    $boleta_selected = "";
                                    $factura_selected = "";
                                    if ($oComanda->tipo_comprobante == 'B') {
                                        $boleta_selected = "selected";
                                        $factura_selected = "";
                                    }
                                    if ($oComanda->tipo_comprobante == 'F') {
                                        $boleta_selected = "";
                                        $factura_selected = "selected";
                                    }
                                ?>
                                <option value="B" <?= $boleta_selected ?>> BOLETA </option>
                                <option value="F" <?= $factura_selected ?>> FACTURA </option>
                            </select>
                        </div>
                        <div class="row ">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label class="control-label" for=""> DNI o RUC </label>
                                    <input type="text" name="cliente_documento" id="cliente_documento" class="form-control input-sm" value="<?= $oComanda->cliente_documento ?>" onchange="verificar_documento();" onkeypress="return soloNumeroEntero(event)">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group" style="margin-top: 23px;margin-left: 10px;">
                                    <button type="button" class="btn btn-primary btn-sm btn-block" onclick="consultar_doc();">
                                        <i class="ace-icon fa fa-search bigger-90" aria-hidden="true"></i>
                                        Consultar 
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label" for=""> Nombres o Razón social</label>
                            <input type="text" name="cliente_nombre" id="cliente_nombre" class="form-control input-sm" value="<?= $oComanda->cliente_nombre ?>">
                        </div>
                        <div class="form-group ">
                            <label class="control-label" for=""> Telefono</label>
                            <input type="text" name="cliente_telefono" id="cliente_telefono" class="form-control input-sm" value="<?= $oComanda->cliente_telefono ?>">
                        </div>
                    </div>
                </div> <!-- row -->
            </div>
            <div class="modal-footer">
                <?=  form_submit('prefacturar', 'Prefacturar', 'class="btn btn-primary"'); ?>
            </div>
             <?= form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script type="text/javascript">
    function verificar_documento(){
        var tipo_doc = $('#tipo_comprobante').val();
        var documento = $('#cliente_documento').val();
        if(tipo_doc == 'B'){
            if(documento.length != 8){
                alertify.error("El documento debe tener 8 dígitos");
                $('#cliente_documento').parent().addClass('has-error');
                $('#cliente_documento').parent().removeClass('has-success');
            }else{
                $('#cliente_documento').parent().removeClass('has-error');
                $('#cliente_documento').parent().addClass('has-success');
            }
        }
        if(tipo_doc == 'F'){
            if(documento.length != 11){
                alertify.error("El documento debe tener 11 dígitos");
                $('#cliente_documento').parent().addClass('has-error');
                $('#cliente_documento').parent().removeClass('has-success');
            }else{
                $('#cliente_documento').parent().removeClass('has-error');
                $('#cliente_documento').parent().addClass('has-success');
            }
        }
        
    }
    function consultar_doc(){
        var tipo_doc = $('#tipo_comprobante').val();
        var documento = $('#cliente_documento').val();
        if(tipo_doc == ""){
            alertify.error("Debe seleccionar un tipo de comprobante.");
            return false;
        }
        if(tipo_doc == 'B'){
            url = '<?= base_url('pide/pide/reniec/')?>' + documento;
        }
        if(tipo_doc == 'F'){
            url = '<?= base_url('pide/pide/sunat/')?>' + documento;
        }
        // alert(url);
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(!objeto.success){
                    alertify.error(objeto.error);
                    $('#cliente_nombre').val("");
                    $('#cliente_telefono').val("");
                    return false;
                }                
                var cliente_nombre = "";
                var cliente_telefono = "";
                if(tipo_doc == 'B'){
                    nombres = objeto.data.Nombre;
                    ape_paterno = objeto.data.Paterno;
                    ape_materno = objeto.data.Materno;
                    cliente_nombre = "" + ape_paterno + " " + ape_materno + ", " + nombres;
                }
                if(tipo_doc == 'F'){
                    cliente_nombre = objeto.data.RazonSocial;
                    cliente_telefono = objeto.data.Telefono;
                }
                $('#cliente_nombre').val(cliente_nombre);
                $('#cliente_telefono').val(cliente_telefono);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>