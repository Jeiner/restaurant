<?php 
    $total = 0;
?>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped" id="tabla-comandas" width="100%">
            <thead>
                <tr>
                    <th style="text-align: center;" > Cód. </th>
                    <th style="text-align: center;" > Fecha </th>
                    <th style="text-align: center;" > Mesa/Mod. </th>
                    <th style="text-align: center;" > Canal </th>
                    <th style="text-align: center;" > Mozo </th>
                    <th style="text-align: center;" class="hidden-xs hidden-sm"> Tiempo </th>
                    <th style="text-align: center;" class="hidden-xs hidden-sm" > Cliente </th>
                    <th style="text-align: center;" > Importe </th>
                    <th style="text-align: center;" > Atención </th>
                    <th style="text-align: center;" > Pago </th>
                    <th style="text-align: center;" > Detalle </th>
                </tr>
            </thead>
            <tbody id="cuerpo_tabla_comandas">
                <?php foreach ($comandas as $key => $comanda): ?>
                    <?php $total += $comanda->importe_total ?>
                    <tr>
                        <td style="text-align: center"> <?= $comanda->comanda_id  ?> </td>
                        <td style="text-align: center"> <?= $comanda->fecha_comanda ?> </td>
                        <td style="text-align: center"> <!-- Mesa  / modalida -->
                            <?php if ($comanda->modalidad == "PRE"): ?>
                                <?= $comanda->mesa ?> 
                            <?php else: ?>
                                <?= $comanda->modalidad_desc ?>
                            <?php endif ?>
                        </td>
                        <td style="text-align: center"> <!-- Mesa  / modalida -->
                            <?= $comanda->canal_desc ?>
                        </td>
                        <td style="text-align: center"> <?= $comanda->mozo ?> </td>
                        <td style="text-align: center" class="hidden-xs hidden-sm"> <!-- Tiempo -->
                            <?php if ($comanda->estado_atencion == 'F'): ?>
                                <?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda,$comanda->finalizada_en ); ?>
                                <?= $minutos.' min' ?>
                            <?php else: ?>
                                <?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda, date("Y/m/d H:i:s") ); ?>
                                <?= $minutos.' min' ?>
                            <?php endif ?>
                        </td>
                        <td style="text-align: left" class="hidden-xs hidden-sm">  <!-- cliente -->
                            <span class="" title="<?= $comanda->cliente_nombre ?>" >
                                <?= ($comanda->cliente_nombre != "") ? substr($comanda->cliente_nombre, 0,11).'...' : "-" ?>
                            </span>
                        </td>
                        <td style="text-align: center" class="dinero"> <?= $comanda->importe_total ?> </td>
                        <td style="text-align: center">
                            <?php if ($comanda->estado_atencion == 'E'): ?>
                                <span class="label label-warning"> En espera </span>
                            <?php endif ?>
                            <?php if ($comanda->estado_atencion == 'A'): ?>
                                <span class="label label-primary"> Atendido </span>
                            <?php endif ?>
                            <?php if ($comanda->estado_atencion == 'F'): ?>
                                <span class="label label-success"> Finalizado </span>
                            <?php endif ?>
                            <?php if ($comanda->estado_atencion == 'X'): ?>
                                <span class="label label-inverse"> Anulado </span>
                            <?php endif ?> 
                        </td>
                        <td style="text-align: center">
                            <?php if ($comanda->estado_pago == 'P'): ?>
                                <span class="label label-danger"> Pendiente </span>
                            <?php endif ?>
                            <?php if ($comanda->estado_pago == 'C'): ?>
                                <span class="label label-success"> Pagado </span>
                            <?php endif ?>
                            <?php if ($comanda->estado_pago == 'X'): ?>
                                <span class="label label-inverse"> Anulado </span>
                            <?php endif ?> 
                        </td>
                        <td style="text-align: center">
                            <a href="<?= base_url('salon/comanda/ver/') ?><?= $comanda->comanda_id ?>">
                                Ver detalle
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-4 col-sm-offset-8 col-lg-3 col-lg-offset-9">
        <div class="form-group">
            <label class="col-sm-4" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                <strong>Total: </strong>
            </label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="" id="" style="font-size: 18px!important;text-align:center;font-weight: bold;" value="<?= number_format(round($total,2), 2, '.', ' ') ?>" readonly>
            </div>
        </div>
    </div>
</div>
<br>

<script type="text/javascript">
    $('#tabla-comandas').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "6%", },   //Cod
          { "targets": [ 1 ],"width": "8%", },   //Fecha
          { "targets": [ 2 ],"width": "10%", },   //Mozo
          { "targets": [ 3 ],"width": "10%", },   //Mesa
          { "targets": [ 4 ],"width": "8%", },   //Canal
          { "targets": [ 5 ],"width": "10%", },   //Mozo
          { "targets": [ 6 ],"width": "10%", },   //Tiempo
          { "targets": [ 7 ],"width": "9%", },   //Importe
          { "targets": [ 8 ],"width": "10%", },   //Atencion
          { "targets": [ 9 ],"width": "10%", },   //Pago
          { "targets": [ 10 ],"width": "8%", },   //Detalle
        ],
        "order": [[ 0, "desc" ]] ,
    });
    $('#tabla-comandas tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>
