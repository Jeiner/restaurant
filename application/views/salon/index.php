<!-- Listado de mesas para agregar una comanda -->
<div class="row">
	<div class="col-sm-9">
		<div class="row">
			<?php foreach ($mesas as $key => $mesa): ?>
				<div class="col-sm-4 col-md-3 col-xs-6" align="center">
					<div class="form-group">
						<?php 
							$boton = "";
							if($mesa->estado == 'L') $boton = ' btn-success ';
							if($mesa->estado == 'O') $boton = ' btn-danger ';
							if($mesa->estado == 'A') $boton = ' btn-primary ';
							if($mesa->estado == 'P') $boton = ' btn-warning ';
						?>
						<a href="<?= base_url('salon/comanda')?>?mesa_id=<?= $mesa->mesa_id ?>" class="btn btn-app no-radius <?= $boton ?>" style="padding-top: 15px;height: 100px;width: 130px">
							<div><?= $mesa->mesa ?></div>
							<?php if ($mesa->estado != 'L'): ?>
								<!-- Verifico si es una mesa que se unió a otra mesa principal -->
								<?php foreach ($uniones as $key => $union): ?>
									<?php if ($mesa->mesa_id == $union->mesa_unida_id && $union->estado != 'L'): ?>
										<div style="font-size: 12px">
											Unida a <?= $union->mesa_principal ?>
										</div>
									<?php endif ?>
								<?php endforeach ?>
								<!-- MESA OCUPADA, atendida o por Salir -->
								<?php foreach ($comandas as $key => $comanda): ?>
									<?php if ($comanda->mesa_id == $mesa->mesa_id && ($comanda->estado_atencion == "E" || $comanda->estado_atencion == "A" || $comanda->estado_atencion == "P")): ?>
										<!-- COMANDA  -->
										<div style="font-size: 13px;margin: 0px;">
											<i class="ace-icon fa fa-user bigger-90" aria-hidden="true"></i>
											<?= $comanda->mozo ?>
										</div>
										<!-- Tiempo de espera -->
										<div style="font-size: 13px;margin: 0px;">
											<i class="ace-icon fa fa-clock-o bigger-90" aria-hidden="true"></i>
											<?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda,date("Y-m-d H:i:s") ); ?>
											<?= $minutos.' min' ?>
										</div>
										<?php if ($comanda->estado_pago == 'C'): ?>
											<span class="badge badge-inverse badge-left">
												Pagado
											</span>
										<?php endif ?>
									<?php endif ?>
								<?php endforeach ?>
							<?php endif ?>
						</a>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-info bigger-90" aria-hidden="true"></i>
                    Leyenda
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                	<a href="javascript:;" class="btn btn-app no-radius btn-success" style="width: 35px;height: 23px">
					</a>
					<span class="black bolder">Libre</span>
                </div>
                <div class="form-group">
                	<a href="javascript:;" class="btn btn-app no-radius btn-danger" style="width: 35px;height: 23px">
					</a>
					<span class="black bolder">Ocupada</span>
                </div>
                <div class="form-group">
                	<a href="javascript:;" class="btn btn-app no-radius btn-primary" style="width: 35px;height: 23px">
					</a>
					<span class="black bolder">Atendida</span>
                </div>
                <div class="form-group">
                	<a href="javascript:;" class="btn btn-app no-radius btn-warning" style="width: 35px;height: 23px">
					</a>
					<span class="black bolder">Por salir</span>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
        	<div class="col-sm-6 col-xs-6 center" >
        		<a class="btn btn-app btn-primary btn-md" href="<?= base_url('salon/comanda/delivery'); ?>" >
					<i class="ace-icon fa fa-car bigger-160"></i>
					Delivery
				</a>
        	</div>
        	<div class="col-sm-6 col-xs-6 center">
        		<a class="btn btn-app btn-primary btn-md" href="<?= base_url('salon/comanda/para_llevar'); ?>" >
					<i class="ace-icon fa fa-archive bigger-160"></i>
					Para llevar
				</a>
        	</div>
        </div>
        <br>
	</div>
</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<div class="widget-box widget-color-red2">
					<div class="widget-header widget-header-small">
						<h5 class="widget-title smaller"> Comandas para llevar y Delivery (Pendientes) </h5>
					</div>
					<div class="widget-body" style="overflow-x: scroll;">
						<div class="widget-main">
							<table class="table table-bordered table-striped" id="tabla-delivery">
								<thead>
									<tr>
										<th style="text-align: center; width: 10%">
											<span class="" data-rel="tooltip" title="Código de comanda" >Cód.</span>
										</th>
										<th style="text-align: center; width: 10%">Nro.</th>
										<th style="text-align: center; width: 15%">Modalidad</th>
										<th style="text-align: center; width: 10%">Mozo</th>
										<th style="text-align: center; width: 10%">Tiempo</th>
										<th style="text-align: center; width: 10%">Importe</th>
										<th style="text-align: center; width: 10%">Atención</th>
										<th style="text-align: center; width: 10%">Pago</th>
										<th style="text-align: center; width: 10%">Detalle</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($comandas as $key => $comanda): ?>
										<?php if ($comanda->modalidad != "PRE"): ?>
											<?php if ($comanda->estado_atencion == "E" || $comanda->estado_atencion == "A"): ?>
												<tr>
													<td style="text-align: center"><?= $comanda->comanda_id ?></td>
													<td style="text-align: center">
														<?= $comanda->modalidad ?> - 
														<?php if ($comanda->correlativo < 10): ?>
															<?= '0'.$comanda->correlativo ?>
														<?php else: ?>
															<?= $comanda->correlativo ?>
														<?php endif ?>
													</td>
													<td style="text-align: center">
														<?= $comanda->modalidad_desc ?><br>
														<span style="font-size: 12px">
															<?php if ($comanda->cliente_direccion != ''): ?>
																<?= $comanda->cliente_direccion ?><br>	
															<?php endif ?>
															<?php if ($comanda->cliente_telefono != ''): ?>
																(<?= $comanda->cliente_telefono ?>)	
															<?php endif ?>
														</span>
													</td>
													<td style="text-align: center"><?= strtoupper($comanda->mozo) ?></td>
													<td style="text-align: center">
														<?php $minutos = $this->Site->minutos_transcurridos($comanda->fecha_comanda,date("Y-m-d H:i:s") ); ?>
														<?= $minutos.' min' ?>
													</td>
													<td style="text-align: center"><?= $comanda->importe_total ?></td>
													<td style="text-align: center">
														<?php if ($comanda->estado_atencion == 'E'): ?>
								        					<span class="label label-warning"> En espera </span>
								        				<?php endif ?>
								        				<?php if ($comanda->estado_atencion == 'A'): ?>
								        					<span class="label label-primary"> Atendido </span>
								        				<?php endif ?>
								        				<?php if ($comanda->estado_atencion == 'F'): ?>
								        					<span class="label label-success"> Finalizado </span>
								        				<?php endif ?>
								        				<?php if ($comanda->estado_atencion == 'X'): ?>
								        					<span class="label label-inverse"> Anulado </span>
								        				<?php endif ?>
													</td>
													<td style="text-align: center">
														<?php if ($comanda->estado_pago == 'P'): ?>
								        					<span class="label label-danger"> Pendiente </span>
								        				<?php endif ?>
								        				<?php if ($comanda->estado_pago == 'C'): ?>
								        					<span class="label label-success"> Pagado </span>
								        				<?php endif ?>
								        				<?php if ($comanda->estado_pago == 'X'): ?>
								        					<span class="label label-inverse"> Anulado </span>
								        				<?php endif ?>
													</td>
													<th style="text-align: center">
														<?php $ruta_ver =  base_url('salon/comanda/editar/').$comanda->comanda_id  ?>
														<a href="<?= $ruta_ver ?>" class="btn btn-primary btn-sm">
								                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
								                        </a>
													</th>
												</tr>
											<?php endif ?>
										<?php endif ?>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
<script type="text/javascript">
    //BORRAR DATOS DE COMANDA LOCAL
    <?php if ($this->session->userdata('remove_local_comanda')): ?>
        remove_local_comanda();
        <?php $this->session->unset_userdata('remove_local_comanda'); ?>
    <?php endif ?>
    function remove_local_comanda(){  //Eliminar datos del localstorage
        localStorage.removeItem('nro_comensales');
        localStorage.removeItem('canal');
        localStorage.removeItem('cmd_comentario');
        localStorage.removeItem('cliente_tipo_documento');
        localStorage.removeItem('cliente_documento');
        localStorage.removeItem('cliente_nombre');
        localStorage.removeItem('cliente_direccion');
        localStorage.removeItem('cliente_telefono');
        localStorage.removeItem('cliente_correo');

        localStorage.removeItem('gb_familia_id');
        localStorage.removeItem('gb_ordenar_por');
        localStorage.removeItem('list_items_cmd');
    }

	var nro_comandas = '<?= $nro_comandas ?>';
	var nro_finalizadas = '<?= $nro_finalizadas ?>';
	var nro_enespera = '<?= $nro_enespera ?>';
	var nro_atendidas = '<?= $nro_atendidas ?>';

	function traer_totales(){
		$.ajax({
	        type: 'POST',
	        url: "<?=base_url('salon/salon/consulta_totales')?>",
	        data: "",
	        success: function(rpta){
	        	objeto = JSON.parse(rpta);
				new_nro_comandas = objeto.nro_comandas;
				new_nro_finalizadas = objeto.nro_finalizadas;
				new_nro_enespera = objeto.nro_enespera;
				new_nro_atendidas = objeto.nro_atendidas;
				if(new_nro_comandas != nro_comandas) location.reload();
				if(new_nro_finalizadas != nro_finalizadas) location.reload();
				if(new_nro_atendidas != nro_atendidas) location.reload();
	        },
	        error: function(rpta){
	            alertify.error(rpta);
	            location.reload();
	        }
	    });
	}
	$('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });

    $('#tabla-delivery').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },   //Cod
          { "targets": [ 1 ],"width": "10%", },   //Nro
          { "targets": [ 2 ],"width": "20%", },   //Modalidad
          { "targets": [ 3 ],"width": "10%", },   //Mozo
          { "targets": [ 4 ],"width": "10%", },   //Tiempo
          { "targets": [ 5 ],"width": "10%", },   //Importe
          { "targets": [ 6 ],"width": "10%", },   //Atencion
          { "targets": [ 7 ],"width": "10%", },   //Pago
          { "targets": [ 8 ],"width": "10%", },   //Detalle
        ],
        "order": [[ 0, "asc" ]] ,
    });

    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
	setInterval('traer_totales()',50000); //50 segundos
</script>
