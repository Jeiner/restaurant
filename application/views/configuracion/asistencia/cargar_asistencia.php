<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="">
            <a href="<?= base_url('configuracion/asistencia/index') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Buscar asistencias
            </a>
        </li>
        <li>
            <a  href="<?= base_url('configuracion/asistencia/asignar_horario') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Asignar horario de entrada y salida
            </a>
        </li>
        <li  class="active">
            <a  data-toggle="tab">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Cargar asistencias 
            </a>
        </li>
        <li>
            <a  href="<?= base_url('configuracion/asistencia/marcaciones') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Marcaciones
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <form method="Post" action="<?= base_url('configuracion/asistencia/importar_asistencias') ?>" class="form-horizontal" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-4">
                    <div class="alert alert-info " style="font-size:13px;">
                        <strong>
                            Fecha última carga: 
                            <?php
                                if($oUltimaCarga != null){
                                    $fecha = strtotime($oUltimaCarga->fecha." ".$oUltimaCarga->hora);
                                }else{
                                    $fecha = strtotime('2020-01-01');
                                }
                             ?>
                            <?= date('d-m-Y  h:i:s', $fecha) ?>
                        </strong>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 style="font-weight: 500">
                        Importar asistencias
                    </h3>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="col-sm-3 col-xs-12 control-label" for=""> Seleccionar archivo excel </label>
                        <div class="col-sm-9 col-xs-12" style="padding-top: 15px">
                            <input type="file" name="archivo_excel" required="required">
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <button class="btn btn-sm btn-primary">
                        Importar asistencias
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
     $('#tabla-validaciones tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
     
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>