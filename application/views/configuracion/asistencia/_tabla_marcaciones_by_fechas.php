<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped" id="tabla-marcaciones" width="100%">
            <thead>
                <tr>
                    <th style="text-align: center;" > ID </th>
                    <th style="text-align: center;" > Nombres </th>
                    <th style="text-align: center;" > Fecha </th>
                    <th style="text-align: center;" > Hora </th>
                </tr>
            </thead>
            <tbody id="cuerpo_tabla_comandas">
                <?php foreach ($lstMarcaciones as $key => $oMarcacion): ?>
                    <tr>
                        <td>
                            <?= $oMarcacion->id_usuario_asistencia ?>
                        </td>
                        <td>
                            <?= $oMarcacion->nombre ?>
                        </td>
                        <td>
                            <span hidden=""><?= $oMarcacion->fecha ?></span>
                            <?php 
                                $dFecha = strtotime($oMarcacion->fecha);
                            ?>
                            <?= date('d-m-Y', $dFecha) ?>
                        </td>
                        <td>
                            <?= $oMarcacion->hora ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<br>

<script type="text/javascript">
    $('#tabla-marcaciones').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "25%", },   //Cod
          { "targets": [ 1 ],"width": "15%", },   //Fecha
          { "targets": [ 2 ],"width": "10%", },   //Mozo
          { "targets": [ 3 ],"width": "10%", },   //Mesa
        ],
        "order": [[ 2, "desc" ]] ,
        "pageLength": 25
    });
    $('#tabla-marcaciones tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>
