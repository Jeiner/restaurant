<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="">
            <a href="<?= base_url('configuracion/asistencia/index') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Buscar asistencias
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="<?= base_url('configuracion/asistencia/asignar_horario') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Asignar horario de entrada y salida
            </a>
        </li>
        <li  class="">
            <a  href="<?= base_url('configuracion/asistencia/cargar_asistencia') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Cargar asistencias 
            </a>
        </li>
        <li>
            <a  href="<?= base_url('configuracion/asistencia/marcaciones') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Marcaciones
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <form method="Post" action="<?= base_url('configuracion/asistencia/guardar_horario') ?>" id="form_horario" >
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Cargo</th>
                                <th>ID usuario asistencia</th>
                                <th>Hora de entrada</th>
                                <th>Hora de salida</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($lstUsuarios as $key => $oUsuario): ?>
                            <tr>
                                <td>
                                    <?= $oUsuario->nombre_completo ?> <br>
                                    (<?= $oUsuario->DNI ?>)
                                </td>
                                <td>
                                    <?= $oUsuario->rol ?>
                                </td>
                                <td>
                                    <input hidden="" type="" name="dni[]" value="<?= $oUsuario->DNI ?>">
                                    <input hidden="" type="" name="usuario_id[]" value="<?= $oUsuario->usuario_id ?>">

                                    <?php if (trim($oUsuario->id_usuario_asistencia) == ""): ?>
                                        <div class="form-group has-error">
                                            <input type="text" name="id_usuario_asistencia[]" value="<?= $oUsuario->id_usuario_asistencia ?>">
                                        </div>
                                    <?php else: ?> 
                                        <div class="form-group has-success">
                                            <input type="text" name="id_usuario_asistencia[]" value="<?= $oUsuario->id_usuario_asistencia ?>">
                                        </div>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <?php if (trim($oUsuario->hora_entrada) == ""): ?>
                                        <div class="form-group has-error">
                                            <input type="text" placeholder="hh:mm:ss" name="hora_entrada[]" value="<?= $oUsuario->hora_entrada ?>">
                                        </div>
                                    <?php else: ?> 
                                        <div class="form-group has-success">
                                            <input type="text" placeholder="hh:mm:ss" name="hora_entrada[]" value="<?= $oUsuario->hora_entrada ?>">
                                        </div>
                                    <?php endif ?>
                                </td>
                                <td>
                                    <?php if (trim($oUsuario->hora_salida) == ""): ?>
                                        <div class="form-group has-error">
                                            <input type="text" placeholder="hh:mm:ss" name="hora_salida[]" value="<?= $oUsuario->hora_salida ?>">
                                        </div>
                                    <?php else: ?> 
                                        <div class="form-group has-success">
                                            <input type="text" placeholder="hh:mm:ss" name="hora_salida[]" value="<?= $oUsuario->hora_salida ?>">
                                        </div>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <button class="btn btn-primary">Guardar horarios</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
     $('#tabla-validaciones tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
     
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>