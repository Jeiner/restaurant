<?php
    $usuario_id_defecto = 0;
    $fecha_ini_defecto = date('Y-m-01');
    $fecha_fin_defecto =  date('Y-m-d');

    if(isset($fecha_inicio) && $fecha_inicio != null){
        $fecha_ini_defecto = date($fecha_inicio);
    }
    if(isset($fecha_fin) && $fecha_fin != null){
        $fecha_fin_defecto = date($fecha_fin);
    }
    if($usuario_id != 0){
        $usuario_id_defecto = $usuario_id;
    }
?>
<div id="modal_ver_asistencia" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong>
                        Ver asistencia
                    </strong> 
                </h3>
            </div>
            <div class="modal-body" >

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha inicio </label>
                        <input type="date" class="form-control input-sm " name="start_date" id="start_date"  value="<?= $fecha_ini_defecto ?>" onchange="verify_start_date();">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha fin </label>
                        <input type="date" class="form-control input-sm " name="end_date" id="end_date"  value="<?= $fecha_fin_defecto ?>" onchange="verify_end_date();">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class=" control-label" style=""> Usuario </label>
                        <select class="form-control input-sm" id="filtro_usuario" name="filtro_usuario" style="width: 100%">
                            <option value=""> --Seleccionar-- </option>
                            <?php foreach ($lstUsuarios as $key => $oUsuario): ?>
                                <?php  
                                $selected = "";
                                if ($usuario_id_defecto == $oUsuario->usuario_id) {
                                    $selected = "selected";
                                }
                                ?>
                                <option value="<?= $oUsuario->usuario_id ?>" <?= $selected ?>><?= $oUsuario->nombre_completo ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" style="margin-top: 23px;">
                        <button class="btn btn-primary btn-xs btn-block" id="filtrar" onclick="buscar_asistencia();">
                            Buscar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="tabla_asistencias">
                        
                    </div>
                </div>
            </div>


            </div>
            <div class="modal-footer">
               
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $('#modal_ver_asistencia').modal({
        show: 'false'
    }); 

    <?php if ($usuario_id != 0): ?>
        buscar_asistencia();
    <?php endif ?>

    function buscar_asistencia(){

        var filtro_usuario = $("#filtro_usuario").val();
        if(filtro_usuario == "*" || filtro_usuario == ""){
            alertify.error("Debe seleccionar un usuario");
            return false;
        }
        
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();

        $('#tabla_asistencias').html('<span class="blue bolder" style="margin: 15px;">Cargando asistencias...</span>');
        // abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/asistencia/cargar_tabla_asistencias_by_fechas')?>",
            data: {"start_date":start_date,
                    "end_date":end_date,
                    "filtro_usuario":filtro_usuario,
                },
            success: function(rpta){
                $('#tabla_asistencias').html(rpta);
                // cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
                // cerrarCargando();
            }
        });
    }


    function verify_start_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(end_date === undefined || end_date == ""){
            return true;
        }else{
            array_start_date = start_date.split("/");
            array_end_date = end_date.split("/");
            start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
            end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
            start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
            var dias = date_diff(start_date, end_date, 'days');
            if(dias < 0 ){
                $('#end_date').parent(".form-group").addClass("has-error");
                return false;
            }else{
                $('#end_date').parent(".form-group").removeClass("has-error");
            }
        }
    }
</script>
