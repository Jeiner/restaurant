<?php
    $usuario_id_defecto = 0;
    $fecha_ini_defecto = date('Y-m-01');
    $fecha_fin_defecto =  date('Y-m-d');

    if($fecha_inicio != null){
        $fecha_ini_defecto = date($fecha_inicio);
    }
    if($fecha_fin != null){
        $fecha_fin_defecto = date($fecha_fin);
    }
    if($usuario_id != 0){
        $usuario_id_defecto = $usuario_id;
    }
?>
<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#generalidades">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Buscar asistencias
            </a>
        </li>
        <li>
            <a  href="<?= base_url('configuracion/asistencia/asignar_horario') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Asignar horario de entrada y salida
            </a>
        </li>
        <li  class="">
            <a  href="<?= base_url('configuracion/asistencia/cargar_asistencia') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Cargar asistencias
            </a>
        </li>
        <li>
            <a  href="<?= base_url('configuracion/asistencia/marcaciones') ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Marcaciones
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="generalidades" class="tab-pane in active">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha inicio </label>
                        <input type="date" class="form-control input-sm " name="start_date" id="start_date"  value="<?= $fecha_ini_defecto ?>" onchange="verify_start_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Fecha fin </label>
                        <input type="date" class="form-control input-sm " name="end_date" id="end_date"  value="<?= $fecha_fin_defecto ?>" onchange="verify_end_date();">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class=" control-label" style=""> Usuario </label>
                        <select class="form-control input-sm" id="filtro_usuario" name="filtro_usuario" style="width: 100%">
                            <option value=""> --Seleccionar-- </option>
                            <?php foreach ($lstUsuarios as $key => $oUsuario): ?>
                                <?php  
                                $selected = "";
                                if ($usuario_id_defecto == $oUsuario->usuario_id) {
                                    $selected = "selected";
                                }
                                ?>
                                <option value="<?= $oUsuario->usuario_id ?>" <?= $selected ?>><?= $oUsuario->nombre_completo ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group" style="margin-top: 23px;">
                        <button class="btn btn-primary btn-xs btn-block" id="filtrar" onclick="buscar_asistencia();">
                            Buscar
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div id="tabla_asistencias">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    <?php if ($usuario_id != 0): ?>
        buscar_asistencia();
    <?php endif ?>

    function buscar_asistencia(){

        var filtro_usuario = $("#filtro_usuario").val();
        if(filtro_usuario == "*" || filtro_usuario == ""){
            alertify.error("Debe seleccionar un usuario");
            return false;
        }
        
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        
        if(start_date === undefined || start_date == ""){
            alertify.error("Fecha inicial no válida");
            return false;
        }
        if(end_date === undefined || end_date == ""){
            alertify.error("Fecha final no válida");
            return false;
        }
        array_start_date = start_date.split("/");
        array_end_date = end_date.split("/");
        start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
        end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
        start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
        var dias = date_diff(start_date, end_date, 'days');
        if(dias < 0 ){
            alertify.error("Fechas no válidas");
            return false;
        }
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();

        $('#tabla_asistencias').html('<span class="blue bolder" style="margin: 15px;">Cargando asistencias...</span>');
        // abrirCargando();
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/asistencia/cargar_tabla_asistencias_by_fechas')?>",
            data: {"start_date":start_date,
                    "end_date":end_date,
                    "filtro_usuario":filtro_usuario,
                },
            success: function(rpta){
                $('#tabla_asistencias').html(rpta);
                // cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
                // cerrarCargando();
            }
        });
    }


    function verify_start_date(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(end_date === undefined || end_date == ""){
            return true;
        }else{
            array_start_date = start_date.split("/");
            array_end_date = end_date.split("/");
            start_date = "" + array_start_date[2] + "-" + array_start_date[1] + "-" + array_start_date[0];
            end_date = "" + array_end_date[2] + "-" + array_end_date[1] + "-" + array_end_date[0];
            start_date_temp = "" + array_start_date[0] + "/" + array_start_date[1] + "/" + array_start_date[2];
            var dias = date_diff(start_date, end_date, 'days');
            if(dias < 0 ){
                $('#end_date').parent(".form-group").addClass("has-error");
                return false;
            }else{
                $('#end_date').parent(".form-group").removeClass("has-error");
            }
        }
    }
</script>