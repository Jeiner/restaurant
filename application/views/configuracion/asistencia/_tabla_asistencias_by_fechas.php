<?php 
    $totalTardanza = 0;
?>
<div class="row">
    <div class="col-sm-12">
        <?php if ($oUsuario->hora_entrada == "" || $oUsuario->hora_salida == ""): ?>
            <div class="alert alert-block alert-danger">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                Hora de entrada o salida no configurada(s).
            </div> 
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php if ($lstValidaciones['min_tol_entrada'] == ""): ?>
            <div class="alert alert-block alert-danger">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                Minutos de tolerancia no configurao.
            </div> 
        <?php endif ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        <div class="form-group">
            <label class=" control-label" style=""> Hora entrada y salida</label>
            <input type="text" class="form-control input-sm " name="hora_entrada_salida" id="hora_entrada_salida" readonly="readonly" value="<?= $oUsuario->hora_entrada ?> - <?= $oUsuario->hora_salida ?>">
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label class=" control-label" style=""> Tolerancia entrada y salida (min) </label>
            <input type="text" class="form-control input-sm " name="hora_salida" id="hora_salida" readonly="readonly" value="<?= $lstValidaciones['min_tol_entrada'] ?> min - <?= $lstValidaciones['min_tol_salida'] ?> min">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-striped" id="tabla-asistencias" width="100%">
            <thead>
                <tr>
                    <th style="text-align: center;" > Nombres. </th>
                    <th style="text-align: center;" > Fecha </th>
                    <th style="text-align: center;" > Día </th>
                    <th style="text-align: center;" > Hora Entrada </th>
                    <th style="text-align: center;" > Hora Salida </th>
                    <th style="text-align: center;" > Estado entrada </th>
                    <th style="text-align: center;" > Marcaciones </th>
                </tr>
            </thead>
            <tbody id="cuerpo_tabla_comandas">
                <?php foreach ($lstAsistencias as $key => $oAsistencia): ?>
                    <?php if ($oAsistencia->dif_entrada > 0 && $oAsistencia->dif_entrada < 900): ?>
                        <?php
                            $totalTardanza += intval($oAsistencia->dif_entrada);
                        ?>
                    <?php endif ?>

                    <tr>
                        <td>
                            <?= $oUsuario->nombre_completo ?>
                        </td>
                        <td class="center">
                            <span hidden=""><?= $oAsistencia->fecha ?></span>
                            <?php 
                                $dFecha = strtotime($oAsistencia->fecha);
                            ?>
                            <?= date('d-m-Y', $dFecha) ?>
                        </td>
                        <td>
                            <?php
                                $label_success = "";
                                $es_hoy = false;

                            ?>
                            <label class="">
                                <?= $oAsistencia->nombre_dia ?> <br>
                                <?php if (date('Y-m-d') == $oAsistencia->fecha): ?>
                                    (Hoy)
                                <?php endif ?>
                            </label>
                        </td>
                        <td class="center">
                            <?php 
                                $color_entrada = "";
                                if($oAsistencia->estado_entrada == -1){
                                    $color_entrada = "label label-danger";
                                }
                            ?>
                            <?php if ($oAsistencia->hora_entrada != ""): ?>
                                <label class="<?= $color_entrada ?>">
                                    <?= $oAsistencia->hora_entrada ?>
                                </label>
                            <?php endif ?>
                            <br>
                            <?php if ($oAsistencia->dif_entrada > 0 && $oAsistencia->dif_entrada < 900): ?>
                                <span>T. <?= $oAsistencia->dif_entrada ?> min</span>
                            <?php endif ?>
                        </td>
                        <td class="center">
                            <?php 
                                $color_salida = "";
                                if($oAsistencia->estado_salida == -1){
                                    $color_salida = "label label-danger";
                                }
                            ?>
                            <?php if ($oAsistencia->hora_salida != ""): ?>
                                <label class="<?= $color_salida ?>">
                                    <?= $oAsistencia->hora_salida ?>
                                </label>
                            <?php endif ?>
                        </td>
                        <td class="center">
                            <?php if ($oAsistencia->estado_entrada != 1 || $oAsistencia->estado_salida != 1): ?>
                                <!-- <span class="label label-danger"> Con observaciones </span><br> -->
                            <?php endif ?>

                            <?php foreach ($oAsistencia->comentarios as $key => $oComentario): ?>
                                <label><?= $oComentario ?></label>
                            <?php endforeach ?>
                        </td>
                        <td class="center">
                            <?php foreach ($oAsistencia->marcaciones as $key => $oMarcacion): ?>
                                <label><?= $oMarcacion->hora ?></label><br>
                            <?php endforeach ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-8">
        
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="col-sm-6" style="font-size: 16px; color: #000; margin-top: 5px;text-align: right;">
                Total tardanzas:
            </label>
            <div class="col-sm-6">
                <input type="text" class="form-control dinero" name="" id="" style="font-size: 18px!important;font-weight: bold;" value="<?= $totalTardanza ?> min" readonly="">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#tabla-asistencias').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "25%", },   //Cod
          { "targets": [ 1 ],"width": "15%", },   //Fecha
          { "targets": [ 2 ],"width": "10%", },   //Mozo
          { "targets": [ 3 ],"width": "10%", },   //Mesa
          { "targets": [ 4 ],"width": "15%", },   //Canal
          { "targets": [ 5 ],"width": "15%", },   //Mozo
          { "targets": [ 6 ],"width": "10%", },   //Mozo
        ],
        "order": [[ 1, "desc" ]] ,
        "pageLength": 10
    });
    $('#tabla-asistencias tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>
