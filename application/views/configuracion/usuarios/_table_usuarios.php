
<table class="table table-bordered" width="100%" id="tabla-usuarios">
	<thead>
        <tr>
            <th style="text-align: center"> Cód </th>
            <th style="text-align: center"> Usuario </th>
            <th style="text-align: center"> Nombre completo </th>
            <th style="text-align: center"> Rol </th>
            <th style="text-align: center"> Teléfono </th>
            <th style="text-align: center"> Correo </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> Edit. </th>
            <th style="text-align: center"> Bloq. </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($usuarios as $key => $usuario): ?>
            <tr>
                <td style="text-align: center"> <?= $usuario->usuario_id ?> </td>
                <td style="text-align: center"> <?= $usuario->usuario ?> </td>
                <td style="text-align: left"> <?= $usuario->nombre_completo ?>  </td>
                <td style="text-align: center"> 
                    <?= $usuario->rol ?>
                </td>
                <td style="text-align: center" > <?= $usuario->telefono ?> </td>
                <td style="text-align: center">
                    <?= $usuario->correo ?>
                </td>
                <td style="text-align: center">
                    <?php 
                        if($usuario->estado == 'A') $est_desc = '<span class="label label-success"> ACTIVO </span>';
                        if($usuario->estado == 'X') $est_desc = '<span class="label label-danger"> BLOQUEADO </span>';
                        if($usuario->estado == 'I') $est_desc = '<span class="label label-danger"> INACTIVO </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('configuracion/usuarios/editar')."?usuario_id=".$usuario->usuario_id ?>
                    <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php if ($usuario->estado == 'A'): ?>
                        <?php $ruta_bloquear = base_url('configuracion/usuarios/bloquear/').$usuario->usuario_id ?>
                        <?php $mensaje = "¿Está seguro de bloquear al usuario: ".strtoupper($usuario->usuario)."?" ?>
                        <span class="" data-rel="tooltip" title="Pulsa para bloquear" >
                            <button href="<?= $ruta_bloquear ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Bloquear usuario" class="btn btn-danger btn-minier  btn-delete">
                                <i class="ace-icon fa fa-lock bigger-120" aria-hidden="true"></i>
                            </button>
                        </span>
                    <?php else: ?>
                        <?php $ruta_activar = base_url('configuracion/usuarios/activar/').$usuario->usuario_id ?>
                        <?php $mensaje = "¿Está seguro de activar al usuario: ".strtoupper($usuario->usuario)."?" ?>
                        <span class="" data-rel="tooltip" title="Pulsa para activar" >
                            <button href="<?= $ruta_activar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Activar usuario" class="btn btn-primary btn-minier  btn-delete">
                                <i class="ace-icon fa fa-unlock  bigger-120" aria-hidden="true"></i>
                            </button>
                        </span>
                    <?php endif ?>
                        
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-usuarios').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "6%", },   //Co
          { "targets": [ 1 ],"width": "10%", },   //usuario
          { "targets": [ 2 ],"width": "20%", },   //Nombres
          { "targets": [ 3 ],"width": "15%", },   //Rol
          { "targets": [ 4 ],"width": "10%", },   //Tele
          { "targets": [ 5 ],"width": "15%", },   //Correo
          { "targets": [ 6 ],"width": "10%", },   //Estado
          { "targets": [ 7 ],"width": "7%", },   //Editar
          { "targets": [ 8 ],"width": "7%", },   //Eliminr
        ],
        "order": [[ 0, "desc" ]] ,
  	});
    $('#tabla-usuarios tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>