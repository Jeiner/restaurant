<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li>
            <a  href="<?= base_url('configuracion/usuarios') ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de usuarios
            </a>
        </li>
        <li class="active">
            <a data-toggle="tab" href="#nuevo_usuario">
            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Nuevo usuario
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="nuevo_usuario" class="tab-pane active">
            <?php $this->load->view('configuracion/usuarios/_form_usuario'); ?>
        </div>
    </div>
</div>