<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#usuarios">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Listado de usuarios
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="usuarios" class="tab-pane in active">
            <?php $this->load->view('configuracion/usuarios/_table_usuarios'); ?>

            <br><br>
            <div class="row">
                <div class="col-sm-12 center">
                    <h1 class="green text-success">
                        Usuarios
                    </h1>
                    <br><br>
                </div>
                <?php foreach ($usuarios as $key => $oUsuario): ?>
                    <?php 
                        if ($oUsuario->estado != "A") {
                            continue;
                        }
                    ?>
                <div class="col-sm-3 center">
                    <span class="profile-picture" style="height: 110px; margin: 10px;">
                        <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="Usuario" style="height:100px" src="<?= base_url($oUsuario->ruta_foto) ?>" onerror="this.onerror=null;this.src='<?= base_url("assets/img/avatars/avatar2.png") ?>';"
                        >
                    </span>
                    <div class="width-80 label label-info label-xlg" style="height: 63px">
                        <div class="inline position-relative">
                            <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                <i class="ace-icon fa fa-circle light-green"></i>
                                <span class="white">
                                    <?= $oUsuario->nombre_completo ?><br>
                                    <?= $oUsuario->DNI ?><br>
                                    <?= $oUsuario->rol ?>
                                </span>
                            </a>
                            <ul class="align-left dropdown-menu dropdown-caret dropdown-lighter" style="z-index: 100;">
                                <li style="margin:10px">
                                    <?php $ruta_editar = base_url('configuracion/usuarios/editar/').$oUsuario->usuario_id ?>
                                    <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-block">
                                        Editar
                                    </a>
                                </li>
                                <li style="margin:10px">
                                    <a href="javascript:;" onclick="abrir_modal_ver_asistencia(<?= $oUsuario->usuario_id ?>);" class="btn btn-primary btn-block">
                                        Asistencia
                                    </a>
                                </li>
                                <li style="margin:10px">
                                    <?php $ruta_bloquear = base_url('configuracion/usuarios/bloquear/').$oUsuario->usuario_id ?>
                                    <?php $mensaje = "¿Está seguro de bloquear al usuario: ".strtoupper($oUsuario->usuario)."?" ?>
                                    <span class="" data-rel="tooltip" title="Pulsa para bloquear" >
                                        <button href="<?= $ruta_bloquear ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Bloquear usuario" class="btn btn-danger btn-block btn-delete">
                                            <i class="ace-icon fa fa-lock bigger-120" aria-hidden="true"></i>
                                            Bloquear
                                        </button>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br><br>
                </div>
                <?php endforeach ?>
            </div>
            <br><hr>
            <div class="row">
                <div class="col-sm-12 center">
                    <h1 class="red text-danger">
                        Usuarios inactivos
                    </h1>
                    <br><br>
                </div>
                <?php foreach ($usuarios as $key => $oUsuario): ?>
                    <?php 
                        if ($oUsuario->estado == "A") {
                            continue;
                        }
                    ?>
                <div class="col-sm-3 center">
                    <span class="profile-picture" style="height: 110px; margin: 10px;">
                        <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="Usuario" style="height:100px" src="<?= base_url($oUsuario->ruta_foto) ?>" onerror="this.onerror=null;this.src='<?= base_url("assets/img/avatars/avatar2.png") ?>';"
                        >
                    </span>
                    <div class="width-80 label label-info label-xlg">
                        <div class="inline position-relative">
                            <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                <i class="ace-icon fa fa-circle light-red"></i>
                                <span class="white">
                                    <?= $oUsuario->nombre_completo ?>
                                </span>
                            </a>
                            <ul class="align-left dropdown-menu dropdown-caret dropdown-lighter" style="z-index: 100;">
                                <li style="margin:10px">
                                    <?php $ruta_editar = base_url('configuracion/usuarios/editar/').$oUsuario->usuario_id ?>
                                    <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-block">
                                        Editar
                                    </a>
                                </li>
                                <li style="margin:10px">
                                    <a href="javascript:;" onclick="abrir_modal_ver_asistencia(<?= $oUsuario->usuario_id ?>);" class="btn btn-primary btn-block">
                                        Asistencia
                                    </a>
                                </li>
                                <li style="margin:10px">
                                    <?php $ruta_activar = base_url('configuracion/usuarios/activar/').$oUsuario->usuario_id ?>
                                    <?php $mensaje = "¿Está seguro de activar al usuario: ".strtoupper($oUsuario->usuario)."?" ?>
                                    <span class="" data-rel="tooltip" title="Pulsa para activar" >
                                        <button href="<?= $ruta_activar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Activar usuario" class="btn btn-success btn-block">
                                            <i class="ace-icon fa fa-unlock  bigger-120" aria-hidden="true"></i>
                                            Activar usuario
                                        </button>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <br><br>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>

<div id="div_modal_ver_asistencia">
    <!-- Código del modal para ver asistencia -->
</div>

<script type="text/javascript">
    function abrir_modal_ver_asistencia(usuario_id){
        $('#div_modal_ver_asistencia').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/asistencia/ajax_cargar_modal_asistencia')?>",
            data: {'usuario_id': usuario_id},
            success: function(rpta){
                $('#div_modal_ver_asistencia').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }
</script>