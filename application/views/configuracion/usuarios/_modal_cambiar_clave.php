<div id="modal_cambiar_clave" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="smaller lighter blue no-margin center"> 
                    <strong>
                        Modificar contraseña
                    </strong> 
                </h3>
            </div>
            <div class="modal-body" >

                <form method="Post" action="<?= base_url('configuracion/usuarios_crud/actualizar_clave') ?>" class="form-horizontal" autocomplete="off" id="form_cambiar_clave">

                    <div class="form-group ">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> ID Usuario </label> 
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" name="cambiar_clave_usuario_id" id="cambiar_clave_usuario_id" class="form-control input-sm" readonly value="<?= $oUsuario->usuario_id ?>">
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Usuario </label> 
                        <div class="col-sm-6 col-xs-12">
                            <input type="text" name="cambiar_clave_usuario" id="cambiar_clave_usuario" class="form-control input-sm" readonly value="<?= $oUsuario->usuario ?>">
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Nueva contraseña </label> 
                        <div class="col-sm-6 col-xs-12">
                            <input type="password" name="cambiar_clave_1" id="cambiar_clave_1" class="form-control input-sm" >
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-sm-4 col-xs-12 control-label" for=""> Repetir contraseña </label> 
                        <div class="col-sm-6 col-xs-12">
                            <input type="password" name="cambiar_clave_2" id="cambiar_clave_2" class="form-control input-sm" >
                        </div>
                    </div>

                    <br>
                    <hr>

                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-xs-6">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <a href="javascript:;" class="btn btn-inverse btn-block btn-sm ">
                                        <i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
                                        Cancelar 
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-xs-6 " >
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <button class="btn btn-primary btn-block btn-sm" id="btn_cambiar_clave">
                                        <i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
                                        Actualizar contraseña 
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    $("#form_cambiar_clave").validate({
        rules: {
            cambiar_clave_1: "required",
            cambiar_clave_2: "required",
        },
        messages:{
            cambiar_clave_1: "Ingresar contraseña",
            cambiar_clave_2: "Ingresar contraseña",
        }
    });

    $("#form_cambiar_clave").submit(function() {
        if ($('#form_cambiar_clave').valid()) {
            if(validar_campos_adic()){
                $('#btn_cambiar_clave').attr('disabled', true);
                return true;
            }else{
                $('#btn_cambiar_clave').attr('disabled', false);
                return false;
            }
        } else {
            $('#btn_cambiar_clave').attr('disabled', false);
            return false;
        }
    });

    function validar_campos_adic(){
        let clave_1 = $("#cambiar_clave_1").val();
        let clave_2 = $("#cambiar_clave_2").val();

        if(clave_1.length < 5){
            alertify.error("La contraseña debe tener almenos 5 caracteres.");
            return false;
        }
        if(clave_1 != clave_2){
            alertify.error("Contraseñas no coinciden.");
            return false;
        }

        return true;
    }
</script>