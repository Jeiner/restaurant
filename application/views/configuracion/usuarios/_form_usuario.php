<form method="Post" action="<?= base_url('configuracion/usuarios_crud/guardar') ?>" class="form-horizontal" autocomplete="off">
	<div class="row">
		<div class="col-sm-4">

			<div class="form-group">
				<div class="col-sm-12">
					<label class="primary" style="font-size: 18px"><strong>
						Datos personales
					</strong></label>
					<hr>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> ID usuario </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="usuario_id" id="usuario_id" class="form-control input-sm" value="<?= $oUsuario->usuario_id ?>" placeholder="Nombres" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> ID empleado</label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="empleado_id" id="empleado_id" class="form-control input-sm" value="<?= $oEmpleado->empleado_id ?>" placeholder="" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Nro. documento </label> 
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<input type="text" name="DNI" id="DNI" class="form-control input-sm" value="<?= $oEmpleado->nro_documento ?>" placeholder="Nro documento"onkeypress="return soloNumeroEntero(event)" readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Apellidos y nom.   </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="nombre_completo" id="nombre_completo" autocomplete="off" class="form-control input-sm" value="<?= $oEmpleado->nombre_completo ?>" placeholder="Apellidos y nombres"  readonly>
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Fecha Nac. </label>
				<div class="col-sm-5 col-xs-12">
					<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control input-sm" value="<?= $oEmpleado->fecha_nacimiento ?>" readonly>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Edad </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="edad" id="edad" class="form-control input-sm"  readonly>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Teléfono </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="telefono" id="telefono" class="form-control input-sm" value="<?= $oEmpleado->telefono_1 ?>" placeholder="N° de celular o teéfono fijo" onkeypress="return soloTelefono(event)" maxlength="30" readonly>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Correo </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="correo" id="correo" class="form-control input-sm" value="<?= $oEmpleado->correo ?>" placeholder="Correo electrónico" readonly>
				</div>
			</div>


		</div>

		<div class="col-sm-4">
			<div class="form-group">
				<div class="col-sm-12">
					<label class="primary" style="font-size: 18px"><strong>
						Datos laborales
					</strong></label>
					<hr>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Sucursal <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="sucursal_codigo" name="sucursal_codigo" disabled >
						<option value="" >Seleccionar...</option>
						<?php foreach ($lstSucursales as $key => $oSucursal): ?>
							<?php
								$optSucView = $oSucursal->sucursal_codigo." | ".strtoupper($oSucursal->empresa_nombre)." - ".strtoupper($oSucursal->sucursal_nombre);
								$optSucSelect = ($oEmpleado->sucursal_codigo == $oSucursal->sucursal_codigo)? "selected" : "";
							?>
							<option value="<?= $oSucursal->sucursal_codigo ?>" <?= $optSucSelect ?>><?= $optSucView ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Cargo <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="cargo_id" name="cargo_id" disabled>
						<option value="">Seleccionar...</option>
						<?php foreach ($cargos as $key => $oCargo): ?>
							<?php
								$optCargoView = $oCargo->cargo_id." | ".strtoupper($oCargo->cargo);
								$optCargoSelect = ($oEmpleado->cargo_id == $oCargo->cargo_id)? "selected" : "";
							?>
							<option value="<?= $oCargo->cargo_id ?>" <?= $optCargoSelect ?> > <?= $optCargoView ?> </option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Sueldo base mensual <label class="red">*</label></label> 
				<div class="col-sm-4 col-xs-12">
					<input type="text" name="sueldo_base_mensual" id="sueldo_base_mensual" class="form-control input-sm" value="<?= $oEmpleado->sueldo_base_mensual ?>" disabled>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Sueldo base semanal</label> 
				<div class="col-sm-4 col-xs-12">
					<input type="text" name="sueldo_base_semanal" id="sueldo_base_semanal" class="form-control input-sm" value="<?= $oEmpleado->sueldo_base_semanal ?>" disabled>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Día de pago</label> 
				<div class="col-sm-4 col-xs-12">
					<input type="number" name="dia_pago" id="dia_pago" class="form-control input-sm" value="<?= $oEmpleado->dia_pago ?>" disabled>
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Estado <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="estado" name="estado" disabled>
						<option value="">Seleccionar...</option>
						<option value="A" <?php if(strtoupper($oEmpleado->estado) == 'A') echo 'selected' ?> > ACTIVO </option>
						<option value="I" <?php if(strtoupper($oEmpleado->estado) == 'I') echo 'selected' ?> > INACTIVO </option>
						<option value="X" <?php if(strtoupper($oEmpleado->estado) == 'X') echo 'selected' ?> > BLOQUEADO </option>
						<option value="L" <?php if(strtoupper($oEmpleado->estado) == 'L') echo 'selected' ?> > PEND. DATOS LABORALES </option>
					</select>
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-4 col-xs-12 control-label" for=""> Observaciones </label> 
				<div class="col-sm-8 col-xs-12">
					<textarea disabled class="form-control input-sm" id="observaciones" name="observaciones" rows=""><?= $oEmpleado->observaciones ?></textarea>
				</div>
			</div>

		</div>



		<div class="col-sm-4">

			<div class="form-group">
				<div class="col-sm-12">
					<label class="primary" style="font-size: 18px"><strong>Datos de usuario</strong></label>
					<hr>
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Usuario <label class="red">*</label></label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="usuario" id="usuario" autocomplete="off" class="form-control input-sm" value="<?= $oUsuario->usuario ?>" placeholder="Nombre de usuario" <?php if($oUsuario->usuario_id != 0) echo "readonly"; ?> >
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Rol de usuario<label class="red">*</label></label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="rol_id" name="rol_id">
						<option value="">Seleccionar..</option>
						<?php foreach ($roles as $key => $rol): ?>
							<?php 
								$rol_selected = ($oUsuario->rol_id == $rol->rol_id) ? "selected" : "";
							?>
							<option value="<?= $rol->rol_id ?>"<?= $rol_selected ?> > <?= $rol->rol_id ?> | <?= $rol->rol ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Estado <label class="red">*</label></label> 
				<div class="col-sm-8 col-xs-12">
					<select class="form-control input-sm" id="estado" name="estado">
						<option value="">Seleccionar..</option>
						<option value="A" <?php if(strtoupper($oUsuario->estado) == 'A') echo 'selected' ?>>ACTIVO</option>
						<option value="X" <?php if(strtoupper($oUsuario->estado) == 'X') echo 'selected' ?>>BLOQUEADO</option>
						<option value="I" <?php if(strtoupper($oUsuario->estado) == 'I') echo 'selected' ?>>INACTIVO</option>
					</select>
				</div>
			</div>

			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> ID asistencia <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="id_usuario_asistencia" id="id_usuario_asistencia" class="form-control input-sm" value="<?= $oUsuario->id_usuario_asistencia ?>" placeholder="">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Ruta foto <label class="red">*</label> </label> 
				<div class="col-sm-8 col-xs-12">
					<input type="text" name="ruta_foto" id="ruta_foto" class="form-control input-sm" value="<?= $oUsuario->ruta_foto ?>" placeholder="">
				</div>
			</div>
		</div> <!-- col -->

	</div> <!-- row -->

	<div class="space"></div>
	<?php if ($oUsuario->usuario_id == 0): ?>
		<div class="row form-group">
			<div class="col-sm-3 col-md-2 col-sm-offset-9 col-md-offset-10">
				<button class="btn btn-primary btn-block btn-sm"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar usuario 
				</button>
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-3 col-md-2 col-xs-12 col-sm-offset-4 col-md-offset-6">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="<?= base_url('configuracion/usuarios') ?>" class="btn btn-inverse btn-block btn-sm ">
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Cancelar edición 
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-2 col-xs-12 " >
				<div class="form-group">
					<div class="col-xs-12">
						<a href="javascript:;" class="btn btn-primary btn-block btn-sm " onclick="abrir_modal_cambiar_clave();">
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Restablecer contraseña
						</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3 col-md-2 col-xs-12 " >
				<div class="form-group">
					<div class="col-xs-12">
						<button class="btn btn-primary btn-block btn-sm">
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Actualizar usuario 
						</button>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
</form>
<div id="div_modal_ver_asistencia">
    <!-- Código del modal para ver asistencia -->
</div>

<div id="div_modal_cambiar_clave">
    <?php $this->load->view('configuracion/usuarios/_modal_cambiar_clave'); ?>
</div>

<script type="text/javascript">

	function abrir_modal_cambiar_clave(){
		$('#modal_cambiar_clave').modal({
	        show: 'false'
	    });
	}

	cambiar_fecha_nac();

	function cambiar_fecha_nac(){
		var fecha = $('#fecha_nacimiento').val();
		var edad = calcularEdad(fecha);
		$('#edad').val(edad);
	}

    function abrir_modal_ver_asistencia(usuario_id){
        $('#div_modal_ver_asistencia').html('');
        $.ajax({
            type: 'POST',
            url: "<?=base_url('configuracion/asistencia/ajax_cargar_modal_asistencia')?>",
            data: {'usuario_id': usuario_id},
            success: function(rpta){
                $('#div_modal_ver_asistencia').html(rpta);
            },
            error: function(rpta){
                alert("Error en la operación");
            }
        });
    }


</script>

