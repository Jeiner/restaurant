<?php $this->load->view('configuracion//_tabbable.php'); ?>

    <div class="tab-content">
        <table class="table table-bordered" id="tabla-validaciones">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre variable</th>
                    <th>Variable</th>
                    <th>Valor</th>
                    <th>Estado</th>
                    <th>Comentario</th>
                    <th>Validar</th>
                    <th>Editar</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lstValidaciones as $key => $value): ?>
                <tr>
                    <td>
                        <?= $value->validacion_id ?>
                    </td>
                    <td>
                        <?= $value->nombre_variable ?>
                    </td>
                    <td>
                        <?= $value->validacion ?>
                    </td>
                    <td>
                        <?= $value->valor ?>
                        <br>
                        <?php 
                            switch (trim($value->tipo)) {
                                case "URL":
                                    echo "<a href='".$value->valor."' target='blanck'>Link</a>";
                                    break;
                                case 1:
                                    echo "i es igual a 1";
                                    break;
                                case 2:
                                    echo "i es igual a 2";
                                    break;
                            }
                        ?>
                    </td>
                    <td>
                        <?php if ($value->estado): ?>
                        <label class="label label-success">
                            OK
                        </label>
                        <?php else: ?>
                        <label class="label label-danger">
                            ERROR
                        </label>
                        <?php endif ?>
                    </td>
                    <td>
                        <?= $value->comentario ?>
                    </td>
                    <td>
                        <a href="<?= base_url('configuracion/validaciones/validarPorId/').$value->validacion_id ?>" class="btn btn-sm btn-primary">
                            Validar
                        </a>
                    </td>
                    <td>
                        <a href="<?= base_url('configuracion/validaciones/validarPorId/').$value->validacion_id ?>" class="btn btn-sm btn-primary">
                            Editar
                        </a>
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>

    
<script type="text/javascript">
     $('#tabla-validaciones tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
     
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>