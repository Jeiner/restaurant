<?php $this->load->view('configuracion//_tabbable.php'); ?>

<div class="tab-content">
    <div id="generalidades" class="tab-pane in active">
        <form method="Post" action="<?= base_url('configuracion/generalidades/actualizar_codigo_seguridad') ?>" class="form-horizontal">
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label class="col-sm-3 col-xs-12 control-label" for=""> Código de seguridad <label class="red">*</label> </label>
                        <div class="col-sm-9 col-xs-12">
                            <input type="password" name="codigo_seguridad" id="codigo_seguridad" class="form-control input-sm" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-xs-12 control-label" for=""> Repetir código <label class="red">*</label> </label>
                        <div class="col-sm-9 col-xs-12">
                            <input type="password" name="codigo_seguridad_2" id="codigo_seguridad_2" class="form-control input-sm" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-2 col-xs-12   col-xs-12 col-sm-offset-2 col-md-offset-5" >
                    <div class="form-group">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-block btn-sm">
                                <i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
                                Actualizar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>