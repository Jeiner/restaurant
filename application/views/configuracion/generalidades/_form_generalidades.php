<form method="Post" action="<?= base_url('configuracion/generalidades/actualizar') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> ID </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="generalidades_id" id="generalidades_id" class="form-control input-sm" value="<?= $oGeneralidades->generalidades_id ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> RUC <label class="red">*</label> </label>
				<div class="col-sm-9">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<input type="text" name="RUC" id="RUC" class="form-control input-sm" value="<?= $oGeneralidades->RUC ?>">
						</div>
						<div class="col-sm-6 col-xs-12">
							<div class="hidden-sm hidden-md hidden-lg" style="margin-top: 10px"></div>
		                    <div class="" style="" id="btn_consultar">
		                    </div>
		                </div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Razón social <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="razon_social" id="razon_social" class="form-control input-sm" value="<?= $oGeneralidades->razon_social ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Activ. económica <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="actividad" id="actividad" class="form-control input-sm" value="<?= $oGeneralidades->actividad ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Slogan <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="slogan" id="slogan" class="form-control input-sm" value="<?= $oGeneralidades->slogan ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Local  <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="local" id="local" class="form-control input-sm" value="<?= $oGeneralidades->local ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Nombre comercial  <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="nombre_comercial" id="nombre_comercial" class="form-control input-sm" value="<?= $oGeneralidades->nombre_comercial ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Dirección <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="direccion" id="direccion" class="form-control input-sm" value="<?= $oGeneralidades->direccion ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Ubigeo <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="ubigeo" id="ubigeo" class="form-control input-sm" value="<?= $oGeneralidades->ubigeo ?>">
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Teléfono 1  <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="telefono_1" id="telefono_1" class="form-control input-sm" value="<?= $oGeneralidades->telefono_1 ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Contacto</label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="contacto" id="contacto" class="form-control input-sm" value="<?= $oGeneralidades->contacto ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Teléfono 2</label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="telefono_2" id="telefono_2" class="form-control input-sm" value="<?= $oGeneralidades->telefono_2 ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Correo </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="correo" id="correo" class="form-control input-sm" value="<?= $oGeneralidades->correo ?>">
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<hr>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Tipo sistema <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="tipo_sistema" id="tipo_sistema" class="form-control input-sm" value="<?= $oGeneralidades->tipo_sistema ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Titulo sistema <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="titulo_sistema" id="titulo_sistema" class="form-control input-sm" value="<?= $oGeneralidades->titulo_sistema ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Icono del sistema <label class="red">*</label> </label>
				<div class="col-sm-5 col-xs-5">
					<input type="text" name="url_icon" id="url_icon" class="form-control input-sm" value="<?= $oGeneralidades->url_icon ?>">
				</div>
				<div class="col-sm-3">
					<img src="<?= base_url($oGeneralidades->url_icon) ?>" class="img-responsive" width="30">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Logo de login <label class="red">*</label> </label>
				<div class="col-sm-5 col-xs-12">
					<input type="text" name="url_logo_login" id="url_logo_login" class="form-control input-sm" value="<?= $oGeneralidades->url_logo_login ?>">
				</div>
				<div class="col-sm-3">
					<img src="<?= base_url($oGeneralidades->url_logo_login) ?>" class="img-responsive" width="50">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Página web <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="pagina_web" id="pagina_web" class="form-control input-sm" value="<?= $oGeneralidades->pagina_web ?>">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Emite Cpbe <label class="red">*</label> </label>
				<?php 
					if($oGeneralidades->emitir_cpbe){
						$Si_emite = "checked";
						$No_emite = "";
					}else{
						$Si_emite = "";
						$No_emite = "checked";
					}
				?>
				<div class="col-sm-3 col-xs-3" align="center">
					<div class="radio">
						<label>
							<input name="emitir_cpbe" value="1" type="radio" class="ace" <?= $Si_emite ?>  />
							<span class="lbl"> SI </span>
						</label>
					</div>
				</div>
				<div class="col-sm-3 col-xs-3" align="center">
					<div class="radio">
						<label>
							<input name="emitir_cpbe" value="0" type="radio" class="ace" <?= $No_emite ?> />
							<span class="lbl"> NO </span>
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Api de Cpbe<label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="api_cpbe_base" id="api_cpbe_base" class="form-control input-sm" value="<?= $oGeneralidades->api_cpbe_base ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Token para api Cpbe  <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="token_facturador" id="token_facturador" class="form-control input-sm" value="<?= $oGeneralidades->token_facturador ?>">
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Tiene delivery Online <label class="red">*</label> </label>
				<?php 
					if($oGeneralidades->tiene_delivery_online){
						$Si_tiene = "checked";
						$No_tiene = "";
					}else{
						$Si_tiene = "";
						$No_tiene = "checked";
					}
				?>
				<div class="col-sm-3 col-xs-3" align="center">
					<div class="radio">
						<label>
							<input name="tiene_delivery_online" value="1" type="radio" class="ace" <?= $Si_tiene ?>  />
							<span class="lbl"> SI </span>
						</label>
					</div>
				</div>
				<div class="col-sm-3 col-xs-3" align="center">
					<div class="radio">
						<label>
							<input name="tiene_delivery_online" value="0" type="radio" class="ace" <?= $No_tiene ?> />
							<span class="lbl"> NO </span>
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Api GET pedidos Online <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="api_get_pedidos_online" id="api_get_pedidos_online" class="form-control input-sm" value="<?= $oGeneralidades->api_get_pedidos_online ?>">
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-sm-3 col-md-2 col-xs-12   col-xs-12 col-sm-offset-4 col-md-offset-5" >
			<div class="form-group">
				<div class="col-xs-12">
					<button class="btn btn-primary btn-block btn-sm">
						<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
						Actualizar
					</button>
				</div>
			</div>
		</div>
	</div>
	
</form>
<script type="text/javascript">
	function consultar_doc(){
        var documento = $('#RUC').val();
        if(documento.length != 11 ){
        	alertify.error("RUC no válido.");
        	return false;
        }
        url = '<?= base_url('pide/pide/sunat/')?>' + documento;
        abrirCargando();
        $.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(rpta){
                objeto = JSON.parse(rpta);
                if(!objeto.success){
                    alertify.error(objeto.error);
                    limpiar_datos();
                    cerrarCargando();
                    return false;
                }
                nombre_completo = objeto.data.RazonSocial;
                telefono = objeto.data.Telefono;
                direccion = objeto.data.Direccion;
                $('#razon_social').val(nombre_completo);
                $('#telefono_1').val(telefono);
                $('#direccion').val(direccion);
                cerrarCargando();
            },
            error: function(rpta){
                alert("Error en la operación");
                cerrarCargando();
            }
        });
    }
</script>