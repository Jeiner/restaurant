<?php 
    $active_generalidades = "";
    $active_sucursales = "";
    $active_seguridad = "";
    $active_validaciones = "";

    $link_generalidades = base_url('configuracion/generalidades/index');
    $link_sucursales = base_url('configuracion/sucursales/index');
    $link_seguridad = base_url('configuracion/generalidades/seguridad');
    $link_validaciones = base_url('configuracion/validaciones/index');


    switch ($select_tab) {
        case 'generalidades':
            $active_generalidades = "active";
            $link_generalidades = "javascript:;";
            break;
        case 'sucursales':
            $active_sucursales = "active";
            $link_sucursales = "javascript:;";
            break;
        case 'seguridad':
            $active_seguridad = "active";
            $link_seguridad = "javascript:;";
            break;
        case 'validaciones':
            $active_validaciones = "active";
            $link_validaciones = "javascript:;";
            break;
        default:
            // code...
            break;
    }
?>

<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="<?= $active_generalidades ?>">
            <a  href="<?= $link_generalidades ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Datos generales
            </a>
        </li>
        <li class="<?= $active_sucursales ?>">
            <a  href="<?= $link_sucursales ?>">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Sucursales
            </a>
        </li>
        <li class="<?= $active_seguridad ?>">
            <a  href="<?= $link_seguridad ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Seguridad
            </a>
        </li>
        <li class="<?= $active_validaciones ?>">
            <a  href="<?= $link_validaciones ?>">
                <i class="ace-icon fa fa-shield bigger-90" aria-hidden="true"></i>
                Validaciones
            </a>
        </li>
    </ul>
</div>



