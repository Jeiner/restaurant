<table class="table table-bordered">
	<thead>
		<tr>
			<th style="text-align: center">Código</th>
			<th style="text-align: center">Ícono</th>
			<th style="text-align: center">Módulo</th>
			<th style="text-align: center">Prioridad</th>
			<th style="text-align: center">Estado</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($modulos as $key => $modulo): ?>
			<tr>
				<td style="text-align: center"> <?= $modulo->modulo_id ?> </td>
				<td style="text-align: center"> <?= $modulo->icono ?> </td>
				<td style="text-align: left"> <?= $modulo->modulo ?> </td>
				<td style="text-align: center"> <?= $modulo->prioridad ?> </td>
				<td style="text-align: center">
					<?php if ( $modulo->estado == 'A' ): ?>
						<span class="label label-success"> Activo </span>
					<?php else: ?>
						<span class="label label-danger"> Bloqueado </span>
					<?php endif ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>