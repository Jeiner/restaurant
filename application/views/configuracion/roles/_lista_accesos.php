
<div class="row">
		<div class="col-sm-3">
			<select class="form-control" name="rol_id" id="rol_id" onchange="cambiar_rol(this.value);">
				<option value="0"> Seleccionar rol... </option>
				<?php foreach ($roles as $rol): ?>
					<?php if ($rol_id == $rol->rol_id): ?>
						<?php $selected = "selected" ?>
					<?php else: ?>
						<?php $selected = "" ?>
					<?php endif ?>
					<option value="<?= $rol->rol_id ?>" <?= $selected ?> > <?= $rol->rol ?> </option>
				<?php endforeach ?>
			</select>
		</div>
	</div>
<hr>
<?php if ($rol_id == 0): ?>
	<div class="row">
		<div class="col-sm-12">
			Debe seleccionar un rol para asignar o quitar accesos a módulos e ítems del menú.
		</div>
	</div>
<?php else: ?>
	<div class="row">
		<div class="col-sm-12">
			<ul class="check-lista">
				<?php foreach ($modulos as $key => $modulo): ?>
					<li class="modulo">
						<div class="modulo-titulo">
							<?php
								$select_modulo = "";
								foreach ($accesos as $key => $acceso) {
									if ( $acceso->modulo_id == $modulo->modulo_id ) {
										$select_modulo = "checked";
										break;
									}
								}
							?>
							<div class="checkbox">
								<label>
									<input <?= $select_modulo ?> type="checkbox" class="ace" onchange="change_modulo(this, <?= $modulo->modulo_id ?>);" />
									<span class="lbl"> <?= $modulo->modulo ?> </span>
								</label>
							</div>
						</div>
						<ul class="check-lista">
							<?php foreach ($opciones as $key => $opcion): ?>
								<?php if ($opcion->modulo_id == $modulo->modulo_id): ?>
									<li class="opcion">
										<div class="opcion-titulo">
											<?php
												$select_opc = "";
												foreach ($accesos as $key => $acceso) {
													if ( $acceso->opcion_id == $opcion->opcion_id ) {
														$select_opc = "checked";
														break;
													}
												}
											?>
											<div class="checkbox">
												<label>
													<input <?= $select_opc ?> type="checkbox" class="ace" onchange="change_opcion(this, <?= $opcion->opcion_id ?>);" />
													<span class="lbl"> <?= $opcion->opcion ?> </span>
												</label>
											</div>
										</div>
									</li>
								<?php endif ?>
							<?php endforeach ?>
						</ul>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
	</div>
<?php endif ?>
<script type="text/javascript">
	function cambiar_rol(rol_id){
		var url = '<?= base_url("configuracion/roles/asignar_accesos/") ?>' + rol_id;
		window.location.href = url;
	}
    function change_modulo(input, modulo_id){ //rol_id,modulo_id, opcion_id 
    	if(input.checked){
    		agregar_acceso('<?= $rol_id ?>', modulo_id, null);
    	}else{
    		quitar_acceso('<?= $rol_id ?>', modulo_id, null);
    	}
    }
    function change_opcion(input, opcion_id){ //rol_id,modulo_id, opcion_id 
    	if(input.checked){
    		agregar_acceso('<?= $rol_id ?>', null, opcion_id);
    	}else{
    		quitar_acceso('<?= $rol_id ?>', null, opcion_id);
    	}
    }
    function agregar_acceso(rol_id, modulo_id, opcion_id){
    	$.ajax({
            type: 'POST',
            url: "<?= base_url('configuracion/roles/agregar_acceso') ?>",
            data: { 
        		"rol_id": rol_id, 
        		"modulo_id": modulo_id, 
        		"opcion_id": opcion_id 
            },
            success: function(rpta){
            	objeto = JSON.parse(rpta);
                if(objeto.success){
                    alertify.success(objeto.mensaje);
                }else{
                	alertify.error(objeto.mensaje);
                }
            },
            error: function(rpta){
                alert("Error de conexión.");
            }
        });
    }
    function quitar_acceso(rol_id, modulo_id, opcion_id){
    	$.ajax({
            type: 'POST',
            url: "<?= base_url('configuracion/roles/quitar_acceso') ?>",
            data: { 
        		"rol_id": rol_id, 
        		"modulo_id": modulo_id, 
        		"opcion_id": opcion_id 
            },
            success: function(rpta){
            	objeto = JSON.parse(rpta);
                if(objeto.success){
                    alertify.success(objeto.mensaje);
                }else{
                	alertify.error(objeto.mensaje);
                }
            },
            error: function(rpta){
                alert("Error de conexión.");
            }
        });
    }
</script>