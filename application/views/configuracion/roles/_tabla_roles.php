<table class="table table-bordered">
	<thead>
		<tr>
			<th style="text-align: center">Código</th>
			<th style="text-align: center">Rol</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($roles as $key => $rol): ?>
			<tr>
				<td style="text-align: center"><?= $rol->rol_id ?></td>
				<td style="text-align: left"><?= $rol->rol ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>