<div class="tabbable">
    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
        <li class="active">
            <a data-toggle="tab" href="#asignar_accesos">
                <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                Asignación de accesos
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#roles">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Gestionar roles
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#modulos">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Gestionar módulos
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#opciones">
                <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                Gestionar opciones
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="asignar_accesos" class="tab-pane in active">
	        <div class="alert alert-info" style="padding: 5px;">
	            Listado de módulos e items asignados a un rol específico.
	        </div>
            <?php $this->load->view('configuracion/roles/_lista_accesos'); ?>
        </div>
        <div id="roles" class="tab-pane in">
            <div class="row">
            	<div class="col-sm-4">
            		<?php $this->load->view('configuracion/roles/_tabla_roles'); ?>
            	</div>
            </div>
        </div>
        <div id="modulos" class="tab-pane in">
            <div class="row">
            	<div class="col-sm-9">
            		<?php $this->load->view('configuracion/roles/_tabla_modulos'); ?>
            	</div>
            </div>
        </div>
        <div id="opciones" class="tab-pane in">
            <div class="row">
            	<div class="col-sm-9">
            		<?php $this->load->view('configuracion/roles/_tabla_opciones'); ?>
            	</div>
            </div>
        </div>
    </div>
</div>