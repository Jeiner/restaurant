<table class="table table-bordered">
	<thead>
		<tr>
			<th style="text-align: center">Código</th>
			<th style="text-align: center">Módulo</th>
			<th style="text-align: center">Opcion</th>
			<th style="text-align: center">Controlador</th>
			<th style="text-align: center">Acción</th>
			<th style="text-align: center">Prioridad</th>
			<th style="text-align: center">Estado</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($opciones as $key => $opcion): ?>
			<tr>
				<td style="text-align: center"> <?= $opcion->opcion_id ?> </td>
				<td style="text-align: left"> <?= $opcion->modulo ?> </td>
				<td style="text-align: left"> <?= $opcion->opcion ?> </td>
				<td style="text-align: center"> <?= $opcion->controlador ?> </td>
				<td style="text-align: center"> <?= $opcion->accion ?> </td>
				<td style="text-align: center"> <?= $opcion->prioridad ?> </td>
				<td style="text-align: center">
					<?php if ( $opcion->estado == 'A' ): ?>
						<span class="label label-success"> Activo </span>
					<?php else: ?>
						<span class="label label-danger"> Bloqueado </span>
					<?php endif ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>