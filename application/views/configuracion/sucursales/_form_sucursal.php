
<form method="Post" action="<?= base_url('configuracion/Sucursales_crud/crud_guardar_sucursal') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-12">
			<?php if (isset($oSucursal->CRUD)): ?>
				<div class="form-group hidden">
					<label class="col-sm-3 col-xs-12 control-label" > CRUD </label>
					<div class="col-sm-9 col-xs-12">
						<input type="text"name="CRUD" id="CRUD" class="form-control input-sm" value="<?= $oSucursal->CRUD ?>" placeholder="000">
					</div>
				</div>
			<?php endif ?>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" > Empresa Código <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text"name="empresa_codigo" id="empresa_codigo" class="form-control input-sm" 
					value="<?= $oSucursal->empresa_codigo ?>" placeholder="000" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" > Empresa <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="empresa_nombre" id="empresa_nombre" class="form-control input-sm"
					value="<?= $oSucursal->empresa_nombre ?>" placeholder="Nombre de la empresa">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" > Sucursal código <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="sucursal_codigo" id="sucursal_codigo" class="form-control input-sm"
					value="<?= $oSucursal->sucursal_codigo ?>" placeholder="000000" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" > Sucursal <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="sucursal_nombre" id="sucursal_nombre" class="form-control input-sm" value="<?= $oSucursal->sucursal_nombre ?>" placeholder="Nombre de la sucursal">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" > Ip Local </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="ip_local" id="ip_local" class="form-control input-sm" value="<?= $oSucursal->ip_local ?>" placeholder="http://000.000.000.000:50/empresa/salon/salon">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" > Lugar </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="lugar" id="lugar" class="form-control input-sm" value="<?= $oSucursal->lugar ?>" placeholder="Lugar">
				</div>
			</div>
			<div class="form-group ">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Estado <label class="red">*</label> </label> 
				<div class="col-sm-6 col-xs-12">
					<select class="form-control input-sm chosen-select" id="estado" name="estado">
						<option value="">Seleccionar...</option>
						<option value="A" <?php if(strtoupper($oSucursal->estado) == 'A') echo 'selected' ?> > ACTIVO </option>
						<option value="I" <?php if(strtoupper($oSucursal->estado) == 'I') echo 'selected' ?> > INACTIVO </option>
					</select>
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<div class="space"></div>
	<?php if (isset($oSucursal->CRUD) && $oSucursal->CRUD == "C"): ?>
		<div class="row form-group">
			<div class="col-sm-7 col-md-6 col-sm-offset-5 col-md-offset-6">
				<button class="btn btn-primary btn-block btn-sm">
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar sucursal 
				</button>
			</div>
		</div>
	<?php else: ?>
		<div class="row form-group">
			<div class="col-sm-6 col-md-6">
				<a href="<?= base_url('configuracion/sucursales') ?>" class="btn btn-inverse btn-block btn-sm "> 
					<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
					Cancelar edición 
				</a>
			</div>
			<div class="col-sm-6 col-md-6" > 
				<button class="btn btn-primary btn-block btn-sm">
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Actualizar sucursal
				</button>
			</div>
		</div>
	<?php endif ?>
</form>
