<?php $this->load->view('configuracion/_tabbable.php'); ?>

<div class="tab-content">
    <div class="row">

        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="panel-header">
                    <div class="panel-title">
                        <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                        Listado de sucursales
                    </div>
                </div>
                <div class="panel-body">
                    <?php $this->load->view('configuracion/sucursales/_table_sucursales'); ?>
                </div>
            </div>
        </div>
        
        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="panel-header">
                    <div class="panel-title">
                        <?php if (isset($oSucursal->CRUD) && $oSucursal->CRUD == "C"): ?>
                            <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                            Crear Nueva Sucursal
                        <?php else: ?>
                            <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                            Editar sucursal
                        <?php endif ?>
                    </div>
                </div>
                <div class="panel-body">
                    <?php $this->load->view('configuracion/sucursales/_form_sucursal'); ?>
                </div>
            </div>
        </div>

    </div>
</div>