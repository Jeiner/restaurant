<table class="table table-bordered" width="100%" id="tabla-sucursales">
	<thead>
        <tr>
            <th style="text-align: left"> Cód. </th>
            <th style="text-align: left"> Empresa - Sucursal </th>
            <th style="text-align: left"> Lugar </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($lstSucursales as $key => $oSucu): ?>
            <tr>
                <td style="text-align: left"> <?= $oSucu->sucursal_codigo ?> </td>
                <td style="text-align: left"> <?= $oSucu->empresa_nombre ?> - <?= $oSucu->sucursal_nombre ?> </td>
                <td style="text-align: left"> <?= $oSucu->lugar ?> </td>
                <td style="text-align: center">
                    <?php 
                        $est_desc = "";
                        if($oSucu->estado == 'A') $est_desc = '<span class="label label-success">Activo</span>';
                        if($oSucu->estado == 'I') $est_desc = '<span class="label label-danger">Inactivo</span>';
                        if($oSucu->estado == '') $est_desc = '<span class="label label-warning">Otro</span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">

                    <?php 
                        $ruta_editar = base_url('configuracion/sucursales/editar/').$oSucu->empresa_codigo."/".$oSucu->sucursal_codigo;
                    ?>
                     <span class="" data-rel="tooltip" title="Pulsa para editar" style="margin-right: 15px">
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                    <?php
                        $nombre_sucursal_full = $oSucu->empresa_nombre." - ".$oSucu->sucursal_nombre;
                     ?>
                    <?php $ruta_eliminar = base_url('configuracion/sucursales/eliminar/').$oSucu->sucursal_codigo ?>
                    <?php $mensaje = "¿Está seguro de eliminar la sucursal: ".strtoupper($nombre_sucursal_full)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar sucursal" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-sucursales').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },     //Codigo
          { "targets": [ 1 ],"width": "35%", },   //Mesa
          { "targets": [ 2 ],"width": "15%", },   //Capacidad
          { "targets": [ 3 ],"width": "20%", },   //Estado
          { "targets": [ 4 ],"width": "10%", },   //Editar
        ],
        "order": [[ 0, "asc" ]] ,
  	});
    $('#tabla-sucursales tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>