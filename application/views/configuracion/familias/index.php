<div class="row">
	<div class="col-sm-5">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <?php if ($oFamilia->familia_id == 0): ?>
                        <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                        Nueva familia
                    <?php else: ?>
                        <i class="ace-icon fa fa-pencil bigger-90" aria-hidden="true"></i>
                        Editar familia
                    <?php endif ?>
                </div>
            </div>
            <div class="panel-body">
                <?php $this->load->view('configuracion/familias/_form_familia'); ?>
            </div>
        </div>
	</div>
	<div class="col-sm-7">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-list bigger-90" aria-hidden="true"></i>
                    Listado de familias
                </div>
            </div>
            <div class="panel-body">
                <?php $this->load->view('configuracion/familias/_table_familias'); ?>
            </div>
        </div>
	</div>
</div>