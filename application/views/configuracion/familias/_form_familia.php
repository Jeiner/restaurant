<form method="Post" action="<?= base_url('configuracion/familias/guardar') ?>" class="form-horizontal">
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group hidden">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Familia_id </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="familia_id" id="familia_id" class="form-control input-sm" value="<?= $oFamilia->familia_id ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Familia <label class="red">*</label> </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="familia" id="familia" class="form-control input-sm" value="<?= $oFamilia->familia ?>" placeholder="Descripción de familia">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 col-xs-12 control-label" for=""> Ubicación </label>
				<div class="col-sm-9 col-xs-12">
					<input type="text" name="ubi_carta" id="ubi_carta" class="form-control input-sm" value="<?= $oFamilia->ubi_carta ?>" maxlength="4" placeholder="Ubicación en carta (0000)" onkeypress="return soloNumeroEntero(event)">
					<label>XX: Nro hoja en carta</label><br>
					<label>XX: Grupo</label><br>
				</div>
			</div>
		</div>
	</div> <!-- row -->
	<div class="space"></div>
	<?php if ($oFamilia->familia_id == 0): ?>
		<div class="row">
			<div class="col-sm-7 col-md-6 col-sm-offset-5 col-md-offset-6">
				<button class="btn btn-primary btn-block btn-sm"> 
					<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
					Guardar familia 
				</button>	
			</div>
		</div>
	<?php else: ?>
		<div class="row ">
			<div class="col-sm-6 col-md-6">
				<div class="form-group">
					<div class="col-xs-12">
						<a href="<?= base_url('configuracion/familias') ?>" class="btn btn-inverse btn-block btn-sm "> 
							<i class="ace-icon fa fa-undo bigger-90" aria-hidden="true"></i>
							Cancelar edición 
						</a>
					</div>		
				</div>
			</div>
			<div class="col-sm-6 col-md-6" > 
				<div class="form-group">
					<div class="col-xs-12">
						<button class="btn btn-primary btn-block btn-sm"> 
							<i class="ace-icon fa fa-save bigger-90" aria-hidden="true"></i>
							Actualizar familia 
						</button>
					</div>		
				</div>
			</div>
		</div>
	<?php endif ?>
	
</form>
