<table class="table table-bordered" width="100%" id="tabla-familias">
	<thead>
        <tr>
            <th style="text-align: center"> Cód. </th>
            <th style="text-align: left"> Categoria </th>
            <th style="text-align: center"> Ubicación </th>
            <th style="text-align: center"> Estado </th>
            <th style="text-align: center"> Editar </th>
            <th style="text-align: center"> Eliminar </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($familias as $key => $familia): ?>
            <tr>
                <td style="text-align: center"> <?= $familia->familia_id ?> </td>
                <td style="text-align: left"> <?= $familia->familia ?> </td>
                <td style="text-align: center"> <?= $familia->ubi_carta ?> </td>
                <td style="text-align: center"> 
                     <?php 
                        if($familia->estado == 'A') $est_desc = '<span class="label label-success"> Activo </span>';
                        if($familia->estado == 'X') $est_desc = '<span class="label label-danger"> Inactivo </span>';
                    ?>
                    <?= $est_desc  ?>
                </td>
                <td style="text-align: center">
                    <?php $ruta_editar = base_url('configuracion/familias/editar/').$familia->familia_id ?>
                     <span class="" data-rel="tooltip" title="Pulsa para editar" >
                        <a href="<?= $ruta_editar ?>" class="btn btn-primary btn-minier">
                            <i class="ace-icon fa fa-pencil bigger-120" aria-hidden="true"></i>
                        </a>
                    </span>
                </td>
                <td style="text-align: center">
                    <?php $ruta_eliminar = base_url('configuracion/familias/eliminar/').$familia->familia_id ?>
                    <?php $mensaje = "¿Está seguro de eliminar la familia: ".strtoupper($familia->familia)."?" ?>
                    <span class="" data-rel="tooltip" title="Pulsa para eliminar" >
                        <button href="<?= $ruta_eliminar ?>" onclick="alertToDelete(this)" msg="<?= $mensaje ?>" titleMsg="Eliminar familia" class="btn btn-danger btn-minier  btn-delete">
                            <i class="ace-icon fa fa-trash-o bigger-120" aria-hidden="true"></i>
                        </button>
                    </span>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#tabla-familias').DataTable({
        "pageLength": 50,
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },     //Codigo
          { "targets": [ 1 ],"width": "30%", },   //Categoria
          { "targets": [ 2 ],"width": "15%", },   //Ubicación
          { "targets": [ 3 ],"width": "15%", },   //Estado
          { "targets": [ 4 ],"width": "15%", },   //Editar
          { "targets": [ 5 ],"width": "15%", },   //Eliminar
        ],
        "order": [[ 2, "asc" ]] ,
  	});
    $('#tabla-familias tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>