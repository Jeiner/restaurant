<style type="text/css">
	tbody tr td{
		/*font-weight: bold!important;*/
		font-size: 14px!important;
	}
</style>
<audio src="<?= base_url('assets/tonos/timbre_bell.mp3') ?>">
	<p>Tu navegador no implementa el elemento audio.</p>
</audio>

<div class="row"  id="mensaje_pedido_anulado" style="display: none">
	<div class="col-sm-12">
		<div class="alert alert-block alert-danger">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			Un pedido fue anulado desde el salón, por favor verificar en los pedidos del día.
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="widget-box widget-color-red2">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title smaller"> Pedidos por atender </h5>
			</div>
			<div class="widget-body">
				<div class="widget-main" id="tabla_pendientes">
					<?php $this->load->view('cocina/cocina/_tabla_pendientes'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="widget-box widget-color-grey">
			<div class="widget-header widget-header-small">
				<h5 class="widget-title smaller"> Pedidos en preparación </h5>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<?php $this->load->view('cocina/cocina/_tabla_preparacion'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var max_pendiente_cocina = '<?= $max_pendiente_cocina ?>';
	var nro_pedidos_anulados = '<?= $nro_pedidos_anulados ?>';
	// alert(nro_pedidos_anulados);
	var audio = document.getElementsByTagName("audio")[0];
	$('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
    // Verifica el ultimo item para cocina
    function consultas_valores_time(){
    	$.ajax({
	        type: 'POST',
	        url: "<?=base_url('cocina/cocina/consultas_valores_time')?>",
	        data: "",
	        success: function(rpta){
	        	objeto = JSON.parse(rpta);
	        	var nuevo_max_pendiente_cocina = objeto.max_pendiente_cocina;
	        	var nuevo_nro_pedidos_anulados = objeto.nro_pedidos_anulados;

	        	if(nuevo_max_pendiente_cocina > max_pendiente_cocina){
	        		max_pendiente_cocina = nuevo_max_pendiente_cocina;
	        		audio.play();
	        		cargar_tabla_pendientes_ajax();
	        	}
	        	if(nuevo_nro_pedidos_anulados > nro_pedidos_anulados){
	        		$('#mensaje_pedido_anulado').show('slow');
	        		cargar_tabla_pendientes_ajax();
	        	}
	        },
	        error: function(rpta){
	            alertify.error(rpta);
	            location.reload();
	        }
	    });
    }
    function cargar_tabla_pendientes_ajax(){
    	$.ajax({
	        type: 'POST',
	        url: "<?=base_url('cocina/cocina/cargar_tabla_pendientes_ajax')?>",
	        data: "",
	        success: function(rpta){
	        	$('#tabla_pendientes').html(rpta);
	        },
	        error: function(rpta){
	            alertify.error(rpta);
	            location.reload();
	        }
	    });
    }
    setInterval('consultas_valores_time()',2000);
</script>
