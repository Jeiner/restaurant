
<table class="table table-bordered table-striped" width="100%" id="tabla-pendientes">
	<thead>
		<tr>
            <!-- <th style="text-align: center; width: 8%"> Cód .</th> -->
            <th style="text-align: center; width: 20%"> Mozo/Mesa </th>
            <th style="text-align: center; width: 15%"> Espera </th>
            <th style="width: 35%"> Producto </th>
            <th style="text-align: center; width: 10%"> Cantidad </th>
            <th style="text-align: center; width: 10%" class="hidden"> Estado </th>
            <th style="text-align: center; width: 10%"> Preparar </th>
		</tr>
	</thead>
	<tbody>
		<?php $cant_pendientes = 0; $mesa_ant = "" ?>
		<?php foreach ($items_espera as $key => $item): ?>  <!-- EN ESPERA-->
			<?php $cant_pendientes++; ?>
            <?php if ($mesa_ant != $item->mesa): ?>
                <?php $mesa_ant = $item->mesa ?>
                <tr>
                    <td colspan="5" style="background-color: #B1B1B1"></td>
                </tr>
            <?php endif ?>
			<tr>
				<!-- <td style="text-align: center"> <?= $item->comanda_item_id ?> </td> -->
                <td style="text-align: left">
                    <?= strtoupper($item->mozo) ?>
                    <br>
                    (<?php if ($item->modalidad == "PRE"): ?>
                        <?= $item->mesa ?> 
                    <?php endif ?>
                    <?php if ($item->modalidad == "DEL"): ?>
                        DELIVERY
                    <?php endif ?>
                    <?php if ($item->modalidad == "LLE"): ?>
                        PARA LLEVAR
                    <?php endif ?> )
                </td>
                <td style="text-align: center">                        
                    <?php $minutos = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") ); ?>
                    <?php if ($minutos < 18): ?>
                        <?= $minutos.' min' ?>
                    <?php endif ?>
                    <?php if ($minutos >= 18 && $minutos < 30): ?>
                        <span class="label label-warning"> <?= $minutos.' min' ?> </span>
                    <?php endif ?>
                    <?php if ($minutos >= 30): ?>
                        <span class="label label-danger"> <?= $minutos.' min' ?> </span>
                    <?php endif ?>
                </td>
    			<td style="text-align: left">
                    <strong> <?= $item->producto ?> </strong>
                    <?php if ($item->comentario != ""): ?>
                        <div class="label-warning comentario_pedido"><?= $item->comentario ?> </div>
                    <?php endif ?>
                </td>
    			<td style="text-align: center"><strong> <?= $item->cantidad ?> </strong></td>
    			<td style="text-align: center" class="hidden">
    				<?php if ($item->estado_atencion == 'E'): ?>
    					<span class="label label-warning"> En espera </span>
    				<?php endif ?>
    				<?php if ($item->estado_atencion == 'P'): ?>
    					<span class="label label-primary"> En preparación </span>
    				<?php endif ?>
    				<?php if ($item->estado_atencion == 'D'): ?>
    					<span class="label label-success"> Despachado </span>
    				<?php endif ?>
    			</td>
    			<td style="text-align: center">
    				<?php if ($item->estado_atencion == 'E'): ?>
    					<a class="btn btn-primary btn-sm btn-block" href="<?= base_url('cocina/cocina/preparar_pedido/');?><?= $item->comanda_item_id ?>">
    						Preparar
    					</a>
    				<?php endif ?>
    			</td>
    		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<div>
	Nro de pedidos por atender: <?= $cant_pendientes ?>
</div>