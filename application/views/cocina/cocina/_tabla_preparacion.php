
<table class="table table-bordered table-striped" width="100%" id="tabla-pendientes">
	<thead>
		<tr>
			<!-- <th style="text-align: center; width: 8%"> Cód .</th> -->
            <th style="text-align: center; width: 20%"> Mesa/Mozo </th>
            <th style="text-align: center; width: 15%"> Espera </th>
			<th style="width: 35%"> Producto </th>
			<th style="text-align: center; width: 10%"> Cantidad </th>
			<th style="text-align: center; width: 10%" class="hidden"> Estado </th>
			<th style="text-align: center; width: 10%"> Despacho </th>
		</tr>
	</thead>
	<tbody>
		<?php $cant_prepa = 0; ?>
		<?php foreach ($items_prepa as $key => $item): ?> <!-- Items en PREPARACION -->
			<?php $cant_prepa++; ?>
			<tr>
				<!-- <td style="text-align: center"> <?= $item->comanda_item_id ?> </td> -->
                <td style="text-align: left">
                    <?= strtoupper($item->mozo) ?>
                    <br>
                    (<?php if ($item->modalidad == "PRE"): ?>
                        <?= $item->mesa ?> 
                    <?php endif ?>
                    <?php if ($item->modalidad == "DEL"): ?>
                        DELIVERY
                    <?php endif ?>
                    <?php if ($item->modalidad == "LLE"): ?>
                        PARA LLEVAR
                    <?php endif ?> )
                </td>
                <td style="text-align: center">                        
                    <?php $minutos = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") ); ?>
                    <?php if ($minutos < 18): ?>
                        <?= $minutos.' min' ?>
                    <?php endif ?>
                    <?php if ($minutos >= 18 && $minutos < 30): ?>
                        <span class="label label-warning"> <?= $minutos.' min' ?> </span>
                    <?php endif ?>
                    <?php if ($minutos >= 30): ?>
                        <span class="label label-danger"> <?= $minutos.' min' ?> </span>
                    <?php endif ?>
                </td>
    			<td style="text-align: left">
                    <a href="javascript:;" onclick="mostrar_opc(this);" style="font-weight: bold;color: #000">
                        <?= $item->producto ?> 
                    </a><br>
                    <span class="opc hidden">
                        <a href="<?= base_url('cocina/cocina/regresar_a_pendiente/')?><?= $item->comanda_item_id ?>">Regresar a pendiente</a>
                    </span>
                    <?php if ($item->comentario != ""): ?>
                        <div class="label-warning comentario_pedido"><?= $item->comentario ?> </div>
                    <?php endif ?>
                </td>
    			<td style="text-align: center"><strong> <?= $item->cantidad ?> </strong></td>
    			<td style="text-align: center" class="hidden">
    				<?php if ($item->estado_atencion == 'E'): ?>
    					<span class="label label-warning"> En espera </span>
    				<?php endif ?>
    				<?php if ($item->estado_atencion == 'P'): ?>
    					<span class="label label-primary"> En preparación </span>
    				<?php endif ?>
    				<?php if ($item->estado_atencion == 'D'): ?>
    					<span class="label label-success"> Despachado </span>
    				<?php endif ?>
    			</td>
    			<td style="text-align: center">
    				<?php if ($item->estado_atencion == 'E' || $item->estado_atencion == 'P'): ?>
    					<a class="btn btn-success btn-sm btn-block" href="<?= base_url('cocina/cocina/despachar_pedido/');?><?= $item->comanda_item_id ?>">
    						Despachar
    					</button>
    				<?php endif ?>
    			</td>
    		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<div>
	Nro de pedidos en preparación: <?= $cant_prepa ?>
</div>

<script type="text/javascript">
    function mostrar_opc(btn){
        var td = $(btn).parent();
        var opc = $(td).children('.opc');
        if($(opc).hasClass('hidden')){
            // Abro la opc y cierro el comentario
            $(opc).removeClass('hidden');
        }else{
            // Cierro la opc y abro el comentario
            $(opc).addClass('hidden');
        }
    }
</script>
