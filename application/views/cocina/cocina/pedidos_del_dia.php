<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
            <div class="panel-header">
                <div class="panel-title">
                    <i class="ace-icon fa fa-plus bigger-90" aria-hidden="true"></i>
                    Pedidos del día para cocina
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-striped" width="100%" id="tabla_pedidos_del_dia">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 5%"> Cód .</th>
                            <th style="text-align: center; width: 10%"> Mesa/Mod. </th>
                            <th style="text-align: center; width: 10%"> Mozo </th>
                            <th style="text-align: center; width: 10%"> Hora pedido </th>
                            <th style="text-align: center; width: 10%"> Espera </th>
                            <th style="width: 20%"> Producto </th>
                            <th style="text-align: center; width: 5%"> Cantidad </th>
                            <th style="text-align: center; width: 10%"> Estado </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($items as $key => $itemP): ?>
                            <tr>
                                <td style="text-align: center"> <?= $itemP->comanda_item_id ?> </td>
                                <td style="text-align: center">
                                    <?php if ($itemP->modalidad == "PRE"): ?>
                                        <?= $itemP->mesa ?> 
                                    <?php endif ?>
                                    <?php if ($itemP->modalidad == "DEL"): ?>
                                        DELIVERY
                                    <?php endif ?>
                                    <?php if ($itemP->modalidad == "LLE"): ?>
                                        PARA LLEVAR
                                    <?php endif ?>
                                </td>
                                <td style="text-align: center"> <?= $itemP->mozo ?></td>
                                 <td style="text-align: center">
                                    <?php $hora = date("H:i:s",strtotime($itemP->pedido_en)); echo $hora; ?>
                                </td>
                                <td style="text-align: center">
                                    <?php if ($itemP->estado_atencion == 'D' || $itemP->estado_atencion == 'X' ): ?>
                                        <?= $itemP->tiempo_espera.' min' ?>
                                    <?php else: ?>
                                        
                                    <?php endif ?>    
                                </td>
                                <td style="text-align: left">
                                    <?= $itemP->producto ?> 
                                    <?php if ($itemP->comentario != ""): ?>
                                        <span style="font-style: oblique">(<?= $itemP->comentario ?> )</span>    
                                    <?php endif ?>
                                </td>
                                <td style="text-align: center"> <?= $itemP->cantidad ?> </td>
                                <td style="text-align: center">
                                    <?php if ($itemP->estado_atencion == 'E'): ?>
                                        <span class="label label-warning"> En espera </span>
                                    <?php endif ?>
                                    <?php if ($itemP->estado_atencion == 'P'): ?>
                                        <span class="label label-primary"> En preparación </span>
                                    <?php endif ?>
                                    <?php if ($itemP->estado_atencion == 'D'): ?>
                                        <span class="label label-success"> Despachado </span>
                                    <?php endif ?>
                                    <?php if ($itemP->estado_atencion == 'X'): ?>
                                        <span class="label label-inverse"> Anulado </span>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <div>
                </div>
            </div>
        </div>
	</div>
</div>
<script type="text/javascript">
    $('#tabla_pedidos_del_dia').DataTable({
        "columnDefs": [
          { "targets": [ 0 ],"width": "10%", },   //Cod
          { "targets": [ 1 ],"width": "10%", },   //MEsa
          { "targets": [ 2 ],"width": "10%", },   //Mozo
          { "targets": [ 3 ],"width": "10%", },   //Hora
          { "targets": [ 4 ],"width": "10%", },   //Espera
          { "targets": [ 5 ],"width": "30%", },   //Produto
          { "targets": [ 6 ],"width": "10%", },   //Cant
          { "targets": [ 7 ],"width": "10%", },   //Estado
        ],
        "order": [[ 3, "desc" ]] ,
    });
    $('.table tbody').on('click', 'tr', function () {
        if ($(this).parents("table").find('tbody tr td').length == 1) { //Si no hay datos
            return false;
        }
        else {
            seleccionarFila(this);
        }
    });
    function seleccionarFila(fila) {
        $(".success").removeClass("success");
        $(fila).addClass("success");
    }
</script>

