<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <!-- CSRF Token -->
        <title><?= $page_title ?> - <?= $oGeneralidades->titulo_sistema; ?></title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <link rel="shortcut icon" href="<?= base_url($oGeneralidades->url_icon)?> " />
        <link href="https://fonts.googleapis.com/css?family=Roboto|Titillium+Web" rel="stylesheet">
        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?> " />
        <link rel="stylesheet" href="<?= base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css')?> " />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?= base_url('assets/css/fonts.googleapis.com.css')?> " />

        <!-- ace styles -->
        <link rel="stylesheet" href="<?= base_url('assets/css/ace.min.css')?> " class="ace-main-stylesheet" id="main-ace-style" />

        <link rel="stylesheet" href="<?= base_url('assets/css/ace-skins.min.css')?> " />
        <link rel="stylesheet" href="<?= base_url('assets/css/ace-rtl.min.css')?> " />

        <link rel="stylesheet" href="<?= base_url('assets/img/splashy/splashy.css') ?> " />

        <!-- ace settings handler -->
        <script src="<?= base_url('assets/js/ace-extra.min.js')?> "></script>

        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/js/jqeasyui/themes/icon.css') ?> ">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/update_template.css') ?> ">
        <link rel="stylesheet" href="<?= base_url('assets/css/chosen.min.css')?> " />
        <link rel="stylesheet" href="<?= base_url('assets/library/alertify/alertify.css')?> " />
        <link rel="stylesheet" href="<?= base_url('assets/library/alertify/default.rtl.css')?> " />

        <script src="<?= base_url('assets/js/jquery-2.1.4.min.js')?> "></script>

        <script type="text/javascript" src="<?= base_url('assets/library/alertify/alertify.min.js') ?> "></script>
    </head>
    <body class="login-layout light-login" style="background-color: #FFFEF6">
        <div class="main-container">
            <div class="main-content">
                <div class="class hidden xs">
                    <div style="margin-top: 40px"></div>
                </div>
                <div class="row" style="margin-top: 60px">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <img src="<?= base_url($oGeneralidades->url_logo_login) ?>" class="img-responsive">
                               <!-- <h1 style="font-size:28px!important;font-weight: bold">
                                    <span class="grey" id="id-text2" >
                                        Bienvenidos
                                    </span>
                                </h1> -->
                                <!-- <br>
                                <h4 class="blue" id="id-company-text" style="font-size:20px!important;">
                                    <i class="ace-icon fa fa-flask blue"></i>
                                    
                                </h4> -->
                                <br>
                            </div>

                            <div class="space-6"></div>
                            <div class="center">
                                <?php if (isset($success)): ?>
                                    <?php if (!$success): ?>
                                        <label style="background-color: red;color: #fff;padding:5px 10px;width: 100%;margin-bottom:0px">
                                            <?= $mensaje ?>
                                        </label>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <div class="position-relative" style="margin-top: 0px">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="grey center" id="id-company-text" style="font-size:18px!important;">
                                                Ingrese sus datos de acceso
                                            </h4> 
                                            <div class="space-6"></div>
                                            <form role="form" method="post" action="<?= base_url('seguridad/acceso/iniciar_sesion')?>">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" class="form-control" placeholder="Nombre de usuario" name="usuario" id="usuario"/>
                                                            <i class="ace-icon fa fa-user"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="password" class="form-control" placeholder="Contraseña" name="clave" id="clave"/>
                                                            <i class="ace-icon fa fa-lock"></i>
                                                        </span>
                                                    </label>

                                                    <div class="space"></div>

                                                    <div class="clearfix">

                                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                            <i class="ace-icon fa fa-key"></i>
                                                            <span class="bigger-110">Ingresar</span>
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /.widget-main -->
                                    </div><!-- /.widget-body -->
                                </div><!-- /.login-box -->
                            </div><!-- /.position-relative -->

                            <!-- <div class="navbar-fixed-top align-right">
                                <br />
                                &nbsp;
                                <a id="btn-login-dark" href="#">Dark</a>
                                &nbsp;
                                <span class="blue">/</span>
                                &nbsp;
                                <a id="btn-login-blur" href="#">Blur</a>
                                &nbsp;
                                <span class="blue">/</span>
                                &nbsp;
                                <a id="btn-login-light" href="#">Light</a>
                                &nbsp; &nbsp; &nbsp;
                            </div> -->
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.main-content -->
        </div><!-- /.main-container -->
        </script>
    </body>
</html>
