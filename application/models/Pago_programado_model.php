<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pago_programado_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $pago_programado_id = "0";
    public $sede_id = "0";
    public $anual = "";
    public $mes = "";
    public $dia_alerta = "";
    public $fecha_prevista_pago = "";
    public $proveedor_id = "0";
    public $descripcion = "";
    public $monto_previsto = "";
    public $estado = "A"; /*PEN:Pendiente, PAG:Pagado, POS:Postergado, X: Anulado*/
    public $observaciones = "";

    public $fecha_registro = "";
    public $registrado_por = "";
    public $fecha_modificacion = "";
    public $modificado_por = "";

    public function insert($data){
        return $this->db->insert('pago_programado', $data);
    }
    public function update($data){
        $this->db->where('pago_programado_id', $data["pago_programado_id"]);

        $this->db->set('sede_id', $data["sede_id"]);
        $this->db->set('anual', $data["anual"]);
        $this->db->set('mes', $data['mes']);
        $this->db->set('dia_alerta', $data['dia_alerta']);
        $this->db->set('fecha_prevista_pago', $data['fecha_prevista_pago']);
        $this->db->set('proveedor_id', $data['proveedor_id']);
        $this->db->set('descripcion', $data['descripcion']);
        $this->db->set('monto_previsto', $data['monto_previsto']);
        $this->db->set('estado', $data['estado']);
        $this->db->set('observaciones', $data['observaciones']);
        // $this->db->set('modificado_por', $data['modificado_por']);

        return $this->db->update('pago_programado');
    }
    public function get_all(){
        $this->db->select("
                        pp.*,
                        s.sede as 'sede_desc',
                        p.razon_social as 'razon_social',
                        me.descripcion as 'estado_desc'
                        ");
        $this->db->from('pago_programado pp');
        $this->db->join('sede s', 's.sede_id = pp.sede_id');
        $this->db->join('proveedor p', 'p.proveedor_id = pp.proveedor_id');
        $this->db->join('multitabla me', 'me.valor = pp.estado and me.tipo_multitabla_id = 14');
        $this->db->order_by('pp.anual', "asc");
        $this->db->order_by('pp.mes', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_con_filtros($anual = "0", $mes_id = "0", $sede_id = "0", $estado = "0"){
        $myQuery = "select 
                        pp.*,
                        s.sede as 'sede_desc', 
                        p.razon_social as 'razon_social', 
                        me.descripcion as 'estado_desc' 
                        FROM pago_programado pp 
                        JOIN sede s ON s.sede_id = pp.sede_id 
                        JOIN proveedor p ON p.proveedor_id = pp.proveedor_id 
                        JOIN multitabla me ON me.valor = pp.estado and me.tipo_multitabla_id = 14 
                        where  (pp.anual = ".$anual." or ".$anual." = 0)  and
                                (pp.mes = ".$mes_id." or ".$mes_id." = '0')  and
                                (pp.sede_id = ".$sede_id." or ".$sede_id." = '0')  and
                                (pp.estado = '".$estado."' or '".$estado."' = '0') 
                        ORDER BY pp.anual ASC, pp.mes ASC";

        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function get_one($pago_programado_id){
        $this->db->select("
                        pp.*,
                        s.sede as 'sede_desc',
                        p.razon_social as 'razon_social',
                        me.descripcion as 'estado_desc'
                        ");
        $this->db->from('pago_programado pp');
        $this->db->join('sede s', 's.sede_id = pp.sede_id');
        $this->db->join('proveedor p', 'p.proveedor_id = pp.proveedor_id');
        $this->db->join('multitabla me', 'me.valor = pp.estado and me.tipo_multitabla_id = 14');
        $this->db->where('pp.pago_programado_id', $pago_programado_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function anular($pago_programado_id){
        $this->db->where('pago_programado_id',$pago_programado_id);
        $this->db->set('estado', 'X');
        return $this->db->delete('pago_programado');
    }
    public function postergar($pago_programado_id){
        $this->db->where('pago_programado_id',$pago_programado_id);
        $this->db->set('estado', 'POS');
        return $this->db->delete('pago_programado');
    }
    public function cambiar_a_pagado($pago_programado_id, $monto_pagado){
        $this->db->where('pago_programado_id',$pago_programado_id);
        $this->db->set('estado', 'PAG');
        $this->db->set('monto_pagado', $monto_pagado);
        return $this->db->update('pago_programado');
    }
    public function eliminar_fisico($pago_programado_id){
        // Guardar en la papelera de reciclaje
        return false;
        $this->db->where('pago_programado_id',$pago_programado_id);
        return $this->db->delete('pago_programado');
    }
}

