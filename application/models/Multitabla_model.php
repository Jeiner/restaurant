<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Multitabla_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public $multitabla_id = "";
	public $tipo_multitabla_id = "";
	public $valor = "";
	public $descripcion = "";
	public $estado = "";

    public function get_all_by_tipo($tipo_multitabla_id){ //Obtener los parametros de un tipo especifico
        // echo $tipo_multitabla_id;exit();
        $this->db->select("*");
        $this->db->from('multitabla');
        $this->db->where('tipo_multitabla_id',$tipo_multitabla_id);
        $this->db->order_by('orden', "asc");
        $this->db->order_by('descripcion', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    // Obtener numero de ordenes referenciados a esta mesa
}

