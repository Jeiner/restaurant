<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Validaciones_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all(){
        $this->db->select("*");
        $this->db->from('validacion');
        $this->db->order_by('validacion_id', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_nombre_variable($nombre_variable){
        $this->db->select("*");
        $this->db->from('validacion');
        $this->db->where('nombre_variable',$nombre_variable);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function update($data){
        $this->db->where('validacion_id', $data["validacion_id"]);
        return $this->db->update('validacion', $data);
    }

    public function obtener_variables_array_nombre(){
        //Devulve un array con las variables, indexadas por el nombre
        $lstVariables = null;
        $obtenerVariables = $this->get_all();
        foreach ($obtenerVariables as $key => $value) {
            $lstVariables[$value->nombre_variable] = $value->valor;
        }
        return $lstVariables;
    }
}
