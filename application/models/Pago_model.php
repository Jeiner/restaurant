<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pago_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $pago_id = "0";
    public $comanda_id = "";
    public $importe_total = "";
    public $descuento = "0.00";
    public $total_a_pagar = "";
    public $pago_efectivo = "0.00";
    public $pago_tarjeta = "0.00";
    public $total_pagado = "";
    public $saldo = "";
    public $efectivo = "0.00";
    public $vuelto = "0.00";
    public $fecha_pago = "";
    public $cajero = "";
    public $tipo_comprobante = "";
    public $serie = "";
    public $correlativo = "";
    public $cliente_id = "";
    public $cliente_documento = "";
    public $cliente_nombre = "";
    public $cliente_direccion = "";
    public $cliente_telefono = "";
    public $caja_id = "";
    public $observaciones = "";
    public $estado = "A"; //'A:Activa, X:Anulada',

    public function insert($data){
        $this->db->insert('pago', $data);
        return  $this->db->insert_id();
    }
    public function get_all_by_fechas($start_date, $end_date){
        $this->db->select("
            p.*,
            c.fecha_comanda,
            c.modalidad,
            c.mozo,
            c.mesa,
            c.nro_comensales,
            c.nro_productos,
            c.estado_atencion,
            c.con_comprobante_e,
            mc.descripcion as 'tipo_comprobante_desc'
        ");
        $this->db->from('pago p');
        $this->db->join('comanda c','c.comanda_id = p.comanda_id');
        $this->db->join('multitabla mc','mc.valor = p.tipo_comprobante and mc.tipo_multitabla_id = 7'); //Tip comprob.
        $this->db->where('date(p.fecha_pago) >=',$start_date);
        $this->db->where('date(p.fecha_pago) <=',$end_date);
        $this->db->order_by('p.fecha_pago', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_by_caja($caja_id){
        // Obtener todos los pagos, para una determinada caja
        $this->db->select("*");
        $this->db->from('pago');
        $this->db->where('caja_id',$caja_id);
        $this->db->order_by('fecha_pago', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one_by_comanda($comanda_id){
        // Obtener el pago de una comanda
        $this->db->select("*");
        $this->db->from('pago');
        $this->db->where('comanda_id',$comanda_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one($pago_id){
        // Obtener un pago por su id
        $this->db->select("
                        p.*,
                        uc.nombre_completo as 'cajero_desc'

                        ");
        $this->db->from('pago p');
        $this->db->where('p.pago_id',$pago_id);
        $this->db->join('usuario uc','uc.usuario = p.cajero'); //usuario cajero
        $query = $this->db->get();
        return $query->result();
    }
    public function get_ventas_detalle_by_fechas($start_date, $end_date){
        $myQuery = "select 
                        ci.*,
                        p.pago_id,
                        p.fecha_pago,
                        p.cajero,

                        p.cliente_nombre,
                        p.cliente_documento,
                        p.cliente_telefono,
                        (select CASE WHEN sunat_tipo_documento_id = 1 THEN 'DNI' 
                                WHEN sunat_tipo_documento_id = 6 THEN 'RUC'
                                ELSE ''END sunat_tipo_documento_id from cliente where documento = c.cliente_documento ) as cliente_tipo ,
                        (select correo from cliente where documento = c.cliente_documento ) as cliente_correo ,
                        (select fecha_nacimiento from cliente where documento = c.cliente_documento ) as cliente_fecha_nacimiento

                        from comanda_item ci
                        inner join comanda c on c.comanda_id = ci.comanda_id
                        inner join pago p on p.comanda_id = c.comanda_id
                        where date(p.fecha_pago) >= '".$start_date."' and date(p.fecha_pago) <= '".$end_date."' 
                    order by p.fecha_pago desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
}

