<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cargo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $cargo_id = "0";
    public $cargo = "";
    public $estado = "A";  //'A:activo, X:Eliminado',

    public function get_all(){
        $this->db->select("*");
        $this->db->from('cargo');
        $this->db->order_by('cargo', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_one_or_null_by_codigo($cargo_id){
        $this->db->select("*");
        $this->db->from('cargo');
        $this->db->where('cargo_id', $cargo_id);
        return $this->db->get()->row();
    }

}

