<?php defined('BASEPATH') OR exit('No direct script access allowed');

class OnlinePedido_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function obtenerPedidosPendientes(){
        $nroPedidosPendientes = 0;
        $oGeneralidades = ($this->Site->get_generalidades())[0];
        $url_webservice = $oGeneralidades->api_get_pedidos_online;

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 1000);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        if(is_null($rptaWs)){
            return -1;
        }

        if(!isset($rptaWs->lSuccess)){
            return -1;
        }

        if(!$rptaWs->lSuccess){
            return -1;
        }

        $nroPedidosPendientes = $rptaWs->nroPedidosPendientes;

        return $nroPedidosPendientes;
    }
}

