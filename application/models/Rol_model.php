<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rol_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $rol_id = "0";
    public $rol = "";
    public $estado = "";  //'A:activo, X:Eliminado',

    public function get_all(){
        $this->db->select("*");
        $this->db->from('rol');
        $this->db->order_by('rol', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function agregar_acceso($data){
        return $this->db->insert('acceso', $data);
    }
    public function quitar_acceso($data){
        $this->db->where('rol_id',$data['rol_id']);
        $this->db->where('modulo_id',$data['modulo_id']);
        $this->db->where('opcion_id',$data['opcion_id']);
        return $this->db->delete('acceso');
    }
    public function get_accesos_by_rol($rol_id){
        $this->db->select("*");
        $this->db->from('acceso');
        $this->db->where('rol_id',$rol_id);
        $query = $this->db->get();
        return $query->result();
    }
}

