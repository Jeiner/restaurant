<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $usuario_id = "0";
    public $usuario = "";
    public $clave = "";
    public $rol_id = "";
    public $DNI = "";
    public $nombres = "";
    public $ape_paterno = "";
    public $ape_materno = "";
    public $nombre_completo = "";
    public $fecha_nacimiento = "";
    public $sexo = "M";
    public $telefono = "";
    public $correo = "";
    public $direccion = "";
    public $codigo_activacion = "";
    public $estado = "A"; // 'A:activo, X:Eliminado',
    public $id_usuario_asistencia = "";
    public $ruta_foto = "";

    public $empleado_id = "0";

    public function insert($data){
        return $this->db->insert('usuario', $data);
    }
    public function update($data){
        $this->db->where('usuario_id', $data["usuario_id"]);
        return $this->db->update('usuario', $data);
    }
    public function delete($usuario_id){
        $this->db->where('usuario_id',$usuario_id);
        return $this->db->delete('usuario');
    }
    public function get_one($usuario_id){
        $this->db->select("*");
        $this->db->from('usuario');
        $this->db->where('usuario_id',$usuario_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("u.*, r.rol");
        $this->db->from('usuario u');
        $this->db->join('rol r','r.rol_id = u.rol_id');
        $this->db->order_by('usuario', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_order_nombre(){
        $this->db->select("u.*, r.rol");
        $this->db->from('usuario u');
        $this->db->join('rol r','r.rol_id = u.rol_id');
        $this->db->order_by('nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_activos_order_nombre(){
        $this->db->select("u.*, r.rol");
        $this->db->from('usuario u');
        $this->db->where('estado','A');
        $this->db->join('rol r','r.rol_id = u.rol_id');
        $this->db->order_by('nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function bloquear($usuario_id){
        $this->db->where('usuario_id', $usuario_id);
        $this->db->set('estado', 'X');
        return $this->db->update('usuario');
    }
    public function activar($usuario_id){
        $this->db->where('usuario_id', $usuario_id);
        $this->db->set('estado', 'A');
        return $this->db->update('usuario');
    }

    public function update_asignar_horario($data){
        $this->db->where('usuario_id', $data["usuario_id"]);
        return $this->db->update('usuario', $data);
    }

    public function update_cambiar_clave($usuario_id, $data){
        $this->db->where('usuario_id', $usuario_id);
        return $this->db->update('usuario', $data);
    }

    // ROLES
    public function get_all_roles(){
        $this->db->select("*");
        $this->db->from('rol');
        $this->db->order_by('rol', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_empleado_id($empleado_id){
        $this->db->select("U.*");
        $this->db->from('usuario U');
        $this->db->where('empleado_id', $empleado_id);
        return $this->db->get()->row();
    }

    public function get_by_nombre_usuario($usuario){
        $this->db->select("U.*");
        $this->db->from('usuario U');
        $this->db->where('usuario', $usuario);
        return $this->db->get()->row();
    }

    public function get_by_usuario_id($usuario_id){
        $this->db->select("U.*");
        $this->db->from('usuario U');
        $this->db->where('usuario_id', $usuario_id);
        return $this->db->get()->row();
    }
}

