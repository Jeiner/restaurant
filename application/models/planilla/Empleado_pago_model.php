<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Empleado_pago_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_pagos_by_fechas($start_date, $end_date, $empleado_id){
        $myQuery = "select 
                        a.*
                    from empleado_pago a
                    where date(a.fecha_pago) >= '".$start_date."' 
                            and date(a.fecha_pago) <= '".$end_date."'
                            and (a.empleado_id = ".$empleado_id.")
                            and (a.estado = 'A')
                    order by a.fecha_pago desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function insert($data){
        $this->db->insert('empleado_pago', $data);
        return $this->db->insert_id();
    }

}

