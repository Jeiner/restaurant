<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generalidades_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_one(){
        $this->db->select("*");
        $this->db->from('generalidades');
        $this->db->where('estado',1);
        $query = $this->db->get();
        return $query->result();
    }
    public function update($data){
        $this->db->where('generalidades_id', $data["generalidades_id"]);
        return $this->db->update('generalidades', $data);
    }
    public function actualizar_codigo_seguridad($data){
        $this->db->where('estado', 1);
        $this->db->set('codigo_seguridad', md5($data['codigo_seguridad']));
        $this->db->set('fecha_modificacion_codigo', date("Y/m/d H:i:s"));
        
        return $this->db->update('generalidades');
    }
}

