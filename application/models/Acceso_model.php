<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Acceso_model extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }
    public function get_usuario($data){
        $array = array('usuario' => $data['usuario'], 'clave' => $data['clave']);

        $this->db->select('u.*, r.rol');
        $this->db->from('usuario u');
        $this->db->join('rol r','r.rol_id = u.rol_id');
        $this->db->where($array);
        
        $query = $this->db->get();
        return $query->result();
    }
}
