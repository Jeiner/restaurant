<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Insumo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $insumo_id = "0";
    public $insumo = "";
    public $unidad = "g";
    public $precio_costo = null; //Por unidad
    public $stock  = "";
    public $stock_minimo = "";
    public $stock_maximo = "";
    public $fecha_vencimiento = "";
    public $descripcion = "";
    public $estado = "A";  //'A:Activo, X:Inactivo',

    public function insert($data){
        return $this->db->insert('insumo', $data);
    }
    public function update($data){
        $this->db->where('insumo_id', $data["insumo_id"]);
        return $this->db->update('insumo', $data);
    }
    public function delete($insumo_id){
        $this->db->where('insumo_id',$insumo_id);
        return $this->db->delete('insumo');
    }
    public function get_one($insumo_id){
        $this->db->select("i.*,
                            u.descripcion as unidad_desc"
                        );
        $this->db->from('insumo i');
        $this->db->join('multitabla u','u.valor = i.unidad and u.tipo_multitabla_id = 1','left'); //unidad
        $this->db->where('i.insumo_id',$insumo_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("
                        i.*,
                        u.descripcion as unidad_desc");
        $this->db->from('insumo i');
        $this->db->join('multitabla u','u.valor = i.unidad and u.tipo_multitabla_id = 1','left'); //unidad
        $this->db->order_by('insumo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function actualiza_stock($insumo_id, $cantidad){
        //Suma la canditdad al stock actual(Dicha suma puede ser positiva o negativa)
        $this->db->where('insumo_id', $insumo_id);
        $this->db->set('stock', 'stock + '.$cantidad,FALSE);
        return $this->db->update('insumo');
    }
    // Obtener numero de ordenes referenciados a esta insumo

}

