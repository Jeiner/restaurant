<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comentario_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all(){
        $this->db->select("*");
        $this->db->from('comentario');
        $this->db->order_by('comentario', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all_by_familia($familia_id){
        $this->db->select("*");
        $this->db->from('comentario');
        $this->db->where('familia_id', $familia_id);
        $this->db->order_by('orden', 'asc');
        $query = $this->db->get();
        return $query->result();
    }
}

