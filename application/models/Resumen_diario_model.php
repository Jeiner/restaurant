<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resumen_diario_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // Insertar nuevo resumen
    public function insert($data, $items){
        if( $this->db->insert('cpbe_resumen', $data) ){
            $resumen_id = $this->db->insert_id();
            foreach ($items as $key => $item) {
                $item['resumen_id'] = $resumen_id;
                $this->db->insert('cpbe_resumen_detalle', $item);
            }
        }
        return $resumen_id;
    }

    //Verifica si una boleta ya pertenece a un resumen
    public function verificaExistenciaDeDocumentoEnResumen($idDocResumen){
        $this->db->select("*");
        $this->db->from('cpbe_resumen_detalle');
        $this->db->where('idDocResumen',$idDocResumen);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_resumenes_por_mes_anio($anio, $mes){
        $myQuery = "SELECT 
                        R.*,
                        mm.descripcion AS 'estado_desc'
                        FROM cpbe_resumen R
                        LEFT JOIN multitabla mm ON mm.valor = R.estado AND mm.tipo_multitabla_id = 37
                        WHERE YEAR(fecResumen) = ".$anio." and
                                MONTH(fecResumen) = ".$mes." 
                        ORDER BY fecResumen ASC";

        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_resumen_cab_por_id($resumen_id){
        $myQuery = "SELECT 
                        R.*,
                        mm.descripcion AS 'estado_desc'
                        FROM cpbe_resumen R
                        LEFT JOIN multitabla mm ON mm.valor = R.estado AND mm.tipo_multitabla_id = 37
                        WHERE resumen_id = ".$resumen_id." ";

        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_resumen_det_por_resumen($resumen_id){
        $myQuery = "SELECT 
                        D.*,
                        mm.descripcion AS 'tipDocResumenDesc'
                        FROM cpbe_resumen_detalle D
                        LEFT JOIN multitabla mm ON mm.valor = D.tipDocResumen AND mm.tipo_multitabla_id = 36
                        WHERE resumen_id = ".$resumen_id." 
                        ORDER BY D.idDocResumen
                        ";

        $q = $this->db->query($myQuery, false);
        return $q->result();
    }    
}

