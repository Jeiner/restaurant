<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Log_error_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $log_error_id = "";
    public $tipo_error = "";
    public $archivo = "";
    public $mensaje = "";

    public $fecha_registro = "";
    public $registrado_por = "";
    public $fecha_modificacion = "";
    public $modificado_por = "";

    public function insert($data){
        return $this->db->insert('log_error', $data);
    }
}

