<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Variables_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_by_nombre_variable($nombre_variable){
        $this->db->select("*");
        $this->db->from('variable');
        $this->db->where('nombre_variable',$nombre_variable);
        $query = $this->db->get();
        return $query->result();
    }
}

