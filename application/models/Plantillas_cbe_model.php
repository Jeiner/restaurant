<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plantillas_cbe_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("Pdf");
        $this->load->library("NumerosEnLetras");
        $this->load->model('Generalidades_model');
        $this->load->model('Cliente_model');
    }
    //Generar PDF
    public function generar_cbe_pdf($nCbeId){
        $oCabecera = $this->Documento_electronico_model->get_cpbe_cab($nCbeId);
        // echo json_encode($oCabecera);exit();
        if (count($oCabecera) == 0) {
            $mensaje = "No se puede generar el PDF ya que no se encontró el comprobante electrónico: ".$nCbeId;
            throw new Exception($mensaje);
        }
        $oCabecera = $oCabecera[0];
        $items = $this->Documento_electronico_model->get_cpbe_det_by_cab($nCbeId);
        
        if(count($items) == 0){
            $mensaje = "No se puede generar el PDF ya que no se encontró items para el comprobante electrónico: ".$nCbeId;
            throw new Exception($mensaje);
        }
        $oEmpresa = $this->Site->get_generalidades();
        if(count($oEmpresa) == 0){
            $mensaje = "No se puede generar el PDF ya que no se encontró la empresa relacionada al comprobante electrónico: ".$oCabecera->nCbeId;
            throw new Exception($mensaje);
        }
        $oEmpresa = $oEmpresa[0];
        $oCliente = $this->Cliente_model->get_by_doc($oCabecera->numDocUsuario);
        if(count($oCliente) > 0){
            $oCliente = $oCliente[0];
        }else{
            $oCliente = null;
        }
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('SANTORINI');
        $pdf->SetTitle('Comprobante electrónico');
        $pdf->SetSubject('Comprobante electrónico');
        $pdf->SetKeywords('Santorini, Pizzas, Comida italianda, Trujillo, Pastas','Las Quintanas');
        $pdf->SetY(-15);
        $pdf->SetX(-15);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setFontSubsetting(true);   
        $pdf->SetFont('dejavusans', '', 9, '', true);   
        $pdf->AddPage(); 
        $html = $this->armar_html_pdf_cbe($oEmpresa, $oCabecera, $items, $oCliente);

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);   
        //Generar Código de barras
        $style = array(
            'border' => 1,
            'vpadding' => '1',
            'hpadding' => '1',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );
        $height = $pdf->getY() + 5;
        //Datos Para el codigo QR
        $cTipUsu = "";
        if($oCabecera->tipDocUsuario== '1'){
            $cTipUsu = "DNI";
        }
        if($oCabecera->tipDocUsuario == '6'){
            $cTipUsu = "RUC";
        }
        $codeQR = $oEmpresa->RUC.' | '.
                  $oCabecera->sunat_tipo_doc_e.' | '.
                  $oCabecera->serie.' | '.
                  $oCabecera->correlativo.' | '.
                  $oCabecera->sumTotTributos.' | '.
                  $oCabecera->sumImpVenta.' | '.
                  $this->Site->fecha_yyyymmdd_a_ddmmyyyy($oCabecera->fecEmision).' | '.
                  $cTipUsu.' | '.
                  $oCabecera->numDocUsuario.' | ';
        $pdf->write2DBarcode($codeQR, 'QRCODE,H', 85, $height, 40, 40, $style, 'N');
        //Generar y Guardar el PDF
        $direc_comprobante_e = $this->Site->get_ruta_reportes_pdf().NOMBRE_SISTEMA.'_CPBE_'.$oCabecera->serie.'-'.$oCabecera->correlativo.'.pdf';
        $pdf->Output($direc_comprobante_e, 'F');
    }

    //Genera el HTMLpara el PDF del comprobante electronico
    private function armar_html_pdf_cbe($oEmpresa, $oCabecera, $items, $oCliente){
        //Obtener igv
        $oItem = $items[0];
        $porcentaje_igv = $oItem->porIgvItem;

        //
        $oNumLetras = new NumerosEnLetras();
        $html = "";
        $html .= '<table style="height: 127px; width: 100%;">
                        <tbody>
                            <tr style="height: 100%;">
                                <td style="width: 35%; text-align:center"><img src="https://santorini.com.pe/img/logo_santorini_xxs.png"><br><span><strong>'.$oEmpresa->razon_social.'<br/>'.$oEmpresa->actividad.'</strong></span> <span style="font-size:10px;color:#4D4D4D"> 
                                        <br/>'.$oEmpresa->direccion.'<br/>'.$oEmpresa->ubigeo.'<br/>Teléfono: '.$oEmpresa->telefono_1.'
                                    </span>
                                </td>
                                <td style="width: 35%"></td>
                                <td style="width: 30%">
                                    <div style="text-align:center; border:1px solid #C7A76A;">
                                        <span style="line-height: 2.1 ">RUC: '.$oEmpresa->RUC.'</span><br/>
                                        <span style="font-size:15px;color:#913032"> <strong>'.$oCabecera->sunat_tipo_doc_e.'</strong> </span><br/>
                                        <span style="line-height: 2.1 ">'.$oCabecera->serie.'-'.$oCabecera->correlativo.'</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                </table>
                <br/><br/>';
        //Datos del cliente
        $cTipUsu = $oCabecera->sunat_tipo_doc_i;
        
        $cTmpDir = "";
        if($oCabecera->tipDocUsuario== '1'){
            $cTmpDir = "";
        }
        if($oCabecera->tipDocUsuario == '6'){
            if($oCliente != null){
                $cTmpDir = $oCliente->direccion;
            }
        }

        $html .='<table cellspacing="0" cellpadding="1" style="width: 100%;border:1px solid #C7A76A;"  >
                    <tbody>
                        <tr><br/>
                            <td style="width: 3%"></td>
                            <td style="width: 67%;margin-left:10px"><b>CLIENTE</b><br/>'.$cTipUsu.': '.$oCabecera->numDocUsuario.'<br/>'.$oCabecera->rznSocialUsuario.'<br/>'.$cTmpDir.'
                                <br/>
                            </td>
                            <td style="width: 30%"><b>Fecha emisión: </b> '.$this->Site->fecha_yyyymmdd_a_ddmmyyyy($oCabecera->fecEmision).'<br/><b>Moneda:</b> SOLES<br/><b>IGV: </b> '.$porcentaje_igv.'%  
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p></p>';
        //Detalle
        $html .='<table cellspacing="0" cellpadding="1" style="width: 100%;border-top:1px solid #4D4D4D;border-bottom:1px solid #4D4D4D;"  >
                    <tbody>
                        <tr style="height: 20px; border:1px solid #000000;">
                            <td style="width: 15%; height: 20px; text-align:center"><b>Código</b></td>
                            <td style="width: 15%; height: 20px; text-align:center"><b>Cantidad</b></td>
                            <td style="width: 40%; height: 20px; text-align:center"><b>Descripción</b></td>
                            <td style="width: 15%; height: 20px; text-align:rigth"><b>P. Unit.</b></td>
                            <td style="width: 15%; height: 20px; text-align:rigth"><b>Importe</b></td>
                        </tr>
                    </tbody>
                </table>';
        $html_items = "";
        $html .='<table cellspacing="0" cellpadding="1" style="width: 100%;"  >
                    <tbody>';
                    foreach ($items as $key => $item) {
                        $html_items .= '<tr style="height: 20px;">
                                <td style="width: 15%; height: 20px; text-align:center">'.$this->Site->rellenar_izquierda($item->codProducto,'0',4).'</td>
                                <td style="width: 15%; height: 20px; text-align:center">'.$item->ctdUnidadItem.'</td>
                                <td style="width: 40%; height: 20px; text-align:left">'.strtoupper($item->desItem).'</td>
                                <td style="width: 15%; height: 20px; text-align:rigth">'.number_format($item->importe / $item->ctdUnidadItem,2, '.', ' ').'</td>
                                <td style="width: 15%; height: 20px; text-align:rigth">'.$item->importe.'</td>
                            </tr>';
                    }
        $html .= $html_items;
        $html .='</tbody>
                </table>
                <p></p>
                <table cellspacing="0" cellpadding="1" style="width: 100%;"  >
                    <tbody>
                        <tr>
                            <td style="width: 70%"></td>
                            <td style="width: 15%; text-align:rigth">Total Gravada<br/>Total IGV<br/><b>Total</b></td>
                            <td style="width: 15%; text-align:rigth">'.$oCabecera->sumTotValVenta.'<br/>'.$oCabecera->sumTotTributos.'<br/>'.$oCabecera->sumImpVenta.'</td>
                        </tr>
                        <tr><td style="width: 100%; text-align:left"><div><br><br>IMPORTE EN LETRAS: '.strtoupper($oNumLetras->convertir($oCabecera->sumImpVenta,'')).'SOLES </div></td>
                        </tr>
                    </tbody>
                </table>
                <table style="height: 37px;" width="100%">
                    <tbody>
                        <tr>
                            <td style="width: 10%;"></td>
                            <td style="width: 80%;text-align:center">
                                <p>Representación impresa del COMPROBANTE ELECTRÓNICO, para consultar el documento visita '.$oEmpresa->pagina_web.'</p>
                            </td>
                            <td style="width: 10%;"></td>
                        </tr>
                    </tbody>
                </table>
        ';
        return $html;
    }
}

