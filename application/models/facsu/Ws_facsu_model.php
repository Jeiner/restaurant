<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_facsu_model extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
    }
    public function enviar_comprobante_e($datos){
        $oGeneralidades = $datos['oGeneralidades'];
        $oCabecera = $datos['oCabecera'];
        $items = $datos['items'];
        $oCliente = $datos['oCliente'];
        $cabecera_id = $oCabecera->cabecera_id;

        //Verifica estado del servidor
        if (!$this->verificar_servidor()) {
            $urlServ = $oGeneralidades->api_cpbe_base;
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta del servidor FACSU '".$urlServ."'. NO se envió el comprobante electrónico: ".$cabecera_id;
            return $response;
        }

        //Obtener webservice
        $url_webservice = $oGeneralidades->api_cpbe_base."/webservice/facmcbe_api/insert_facmcbe";
        
        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'nEmpId' => "1",
            // 'nCodSuc'
            'cNumRuc' => $oGeneralidades->RUC,
            'cTipCbe' => $oCabecera->tipoComprobante,
            'cSerie' => $oCabecera->serie,
            'cCorrel' => $oCabecera->correlativo,
            'cNumDoc' => $oCabecera->serie.'-'.$oCabecera->correlativo,

            'comanda_id' => $oCabecera->comanda_id,
            'pago_id' => $oCabecera->pago_id,

            'cTipOpe' => $oCabecera->tipoOperacion,
            'cFecEmi' => $oCabecera->fecEmision,
            'cHorEmi' => $oCabecera->horEmision,
            'cFecVen' => $oCabecera->fecVencimiento,
            'cLocEmi' => $oCabecera->codLocalEmisor,
            'cTIpUsu' => $oCabecera->tipDocUsuario,
            'cDocUsu' => $oCabecera->numDocUsuario,
            'cNomUsu' => $oCabecera->rznSocialUsuario,
            'cTipMon' => $oCabecera->tipMoneda,

            'nTotTri' => $oCabecera->sumTotTributos,
            'nValVen' => $oCabecera->sumTotValVenta,
            'nPreVen' => $oCabecera->sumPrecioVenta,
            'nTotDes' => $oCabecera->sumDescTotal,
            'nOtrCar' => $oCabecera->sumOtrosCargos,
            'nTotAnt' => $oCabecera->sumTotalAnticipos,
            'nImpVen' => $oCabecera->sumImpVenta,

            'cUblId' => $oCabecera->ublVersionId,
            'cCusId' => $oCabecera->customizationId,

            'cCodHas' => "",
            'cIndSit' => "",
            'cEnvCoe' => 0,

            'cTmpDir' => $oCliente->direccion,
            'cTmpCoe' => $oCliente->correo,

            'tFecReg' => date("Y/m/d H:i:s"),

            'cToken' => $oGeneralidades->token_facturador,
            'aItems' => json_encode($items)
        ));
        $buffer = curl_exec($curl_handle);
        $buffer = trim($buffer, "\xEF\xBB\xBF"); //Para quitar el texto "ufeff"
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar el comprobante electrónico: ".$cabecera_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío el comprobante electrónico al sistema de facturación FACSU: ".$rptaWs->message;
            return $response;
        }
        $nCbeId = $rptaWs->id;
        $response['success'] = true;
        $response['message'] = "Comprobante electrónico enviado correctamente";
        return $response;
    }

    //Verifica el estado del api cpbe, True: activo, False:Sin conexión
    public function verificar_servidor(){
        $oGeneralidades = ($this->Site->get_generalidades())[0];
        $urlServ = $oGeneralidades->api_cpbe_base;
        $rptaServ = @get_headers($urlServ);

        if (!is_array($rptaServ)) {
            return false;
        }

        foreach ($rptaServ as $key => $value) {
            if(strpos(strtoupper($value), 'NOT FOUND')){
                return false;
            }
        }
        return true;
    }

    //Consulta estado del comprobante electrónico
    public function consultar_estado_cpbe($cNumDoc){
        $oGeneralidades = $this->session->userdata('session_generalidades');
        $url_webservice = $oGeneralidades->api_cpbe_base."/webservice/facmcbe_api/get_facmcbe"."/?cNumDoc=".$cNumDoc;

        //Verifica estado del servidor
        if (!$this->verificar_servidor()) {
            $urlServ = $oGeneralidades->api_cpbe_base;
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta del servidor FACSU '".$urlServ."'.";
            return $response;
        }
        //Llmar a Web service GET
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 1000);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar el comprobante electrónico: ".$cabecera_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = $rptaWs->message;
            return $response;
        }
        $response['success'] = true;
        $response['message'] = "Comprobante electrónico encontrado correctamente.";
        $response['data'] = $rptaWs->data;
        return $response;
    }

    public function enviar_resumen_diario($datos){
        $oGeneralidades = $datos['oGeneralidades'];
        $oResumen = $datos['oResumen'];
        $items = $datos['items'];
        $resumen_id = $oResumen->resumen_id;

        //Verifica estado del servidor
        if (!$this->verificar_servidor()) {
            $urlServ = $oGeneralidades->api_cpbe_base;
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta del servidor FACSU '".$urlServ."'. NO se envió el resumen diario: ".$resumen_id;
            return $response;
        }

        //Obtener url webservice
        $url_webservice = $this->Site->get_url_webservice_facsu_facmres("I");

        //Enviar a webservice el resumen
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'cNumRuc' => $oGeneralidades->RUC,
            'tFecEmi' => $oResumen->fecEmision,
            'tFecRes' => $oResumen->fecResumen,
            'nNumBol' => $oResumen->nroBoletas,
            'nTotVal' => $oResumen->sumValorVenta,
            'nTotTri' => $oResumen->sumTributos,
            'nTotVen' => $oResumen->sumTotalVenta,
            'cTicket' => $oResumen->ticket,
            'cIndSit' => $oResumen->estado,
            'tFecReg' => $oResumen->fecha_registro,
            'cUsuReg' => $oResumen->registrado_por,
            'tFecMod' => $oResumen->fecha_modificacion,
            'cUsuMod' => $oResumen->modificado_por,
            'lstDetalle' => json_encode($items)
        ));
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar el resumen diario: ".$resumen_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío el resumen diario al sistema de facturación FACSU: ".$rptaWs->message;
            return $response;
        }
        $resumen_id = $rptaWs->id;
        $response['success'] = $rptaWs->success;
        $response['message'] = $rptaWs->message;
        
        return $response;
    }

    //Consulta estado de comprobantes electrónicos
    public function consultar_estado_cpbes_batch($lstCpbes){
        $oGeneralidades = $this->session->userdata('session_generalidades');
        $url_webservice = $oGeneralidades->api_cpbe_base."/webservice/facmcbe_api/consulta_estado_batch";

        //Verifica estado del servidor
        if (!$this->verificar_servidor()) {
            $urlServ = $oGeneralidades->api_cpbe_base;
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta del servidor FACSU '".$urlServ."'.";
            return $response;
        }

        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 1000);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);

        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'lstCpbes' => json_encode($lstCpbes)
        ));

        $buffer = curl_exec($curl_handle);
        $buffer = trim($buffer, "\xEF\xBB\xBF"); //Para quitar el texto "ufeff"
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al consultar los comprobantes electrónicos.";
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = $rptaWs->message;
            return $response;
        }
        $response['success'] = true;
        $response['message'] = "Comprobantes electrónicos encontrados correctamente.";
        $response['data'] = $rptaWs->data;
        return $response;
    }
}

