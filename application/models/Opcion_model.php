<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Opcion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $opcion_id ="0";
    public $modulo_id ="0";
    public $opcion ="";
    public $icono ="";
    public $prioridad ="";
    public $controlador ="";
    public $accion ="";
    public $estado ="";//'A:activo, X:Eliminado',

    public function get_all(){
        $this->db->select("o.*, m.modulo");
        $this->db->from('opcion o');
        $this->db->join('modulo m', 'm.modulo_id = o.modulo_id');
        $this->db->order_by('m.prioridad', "desc");
        $this->db->order_by('o.prioridad', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_by_rol($rol_id = 0){
        $this->db->select(" o.* ");
        $this->db->from('opcion o');
        $this->db->join('acceso a','o.opcion_id = a.opcion_id');
        $this->db->where('a.rol_id',$rol_id);
        $this->db->where('o.estado','A');
        $this->db->order_by('o.prioridad', "desc");
        $query = $this->db->get();
        return $query->result();
    }
}

