<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Union_mesa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function insert($data){
        return $this->db->insert('union_mesa', $data);
    }
    public function get_all_by_comanda($comanda_id){
        $this->db->select("*");
        $this->db->from('union_mesa');
        $this->db->where('comanda_id', $comanda_id);  // comanda_ principal
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_by_caja($caja_id){
        $this->db->select("*");
        $this->db->from('union_mesa');
        $this->db->where('caja_id', $caja_id);  // comanda_ principal
        $query = $this->db->get();
        return $query->result();
    }
    //Metodo general para actualizar datos de una comanda
    public function update($union_id, $nueva_data){
        $this->db->where('union_id', $union_id);
        $this->db->set($nueva_data);
        return $this->db->update('union_mesa');
    }
}

