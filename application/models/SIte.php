<?php defined('BASEPATH') OR exit('No direct script access allowed');
// Modelo para funciones generales
class Site extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Log_error_model');
        $this->load->model('Variables_model');
        
    }
    public $dias = array(
    			'Monday' => 'Lunes',
    			'Tuesday' => 'Martes',
    			'Wednesday' => 'Miércoles',
    			'Thursday' => 'Jueves',
    			'Friday' => 'Viernes',
    			'Saturday' => 'Sábado',
    			'Sunday' => 'Domingo',
    		);
    public $meses = array(
    			'January' => 'Enero',
    			'February' => 'Febrero',
    			'March' => 'Marzo',
    			'April' => 'Abril',
    			'May' => 'Mayo',
    			'June' => 'Junio',
    			'July' => 'Julio',
    			'August' => 'Agosto',
    			'September' => 'Setiembre',
    			'October' => 'Octubre',
    			'November' => 'Noviembre',
    			'December' => 'Diciembre',

    		);
    public function get_generalidades(){
        $this->db->select("*");
        $this->db->from('generalidades');
        $this->db->where('estado',1);
        $query = $this->db->get();
        return $query->result();
    }
    //::::::::::::::::::::::::::::::::::::::::::::::.... FECHAS Y HORAS ....::::::::::::::::::::::::::::::::::::::::::::::
    public function fecha_yyyymmdd_a_ddmmyyyy($fecha_yyyy_mm_dd){
        $newDate = date("d-m-Y", strtotime($fecha_yyyy_mm_dd));
        return $newDate;
    }
    public function fecha_yyyymmdd_a_ddmmyyyy_hms($fecha_yyyy_mm_dd){
        $newDate = date("d-m-Y H:i:s", strtotime($fecha_yyyy_mm_dd));
        return $newDate;
    }
    public function minutos_transcurridos($fecha_i,$fecha_f){ //Y-m-d H:i:s
        $minutos = (strtotime($fecha_i)-strtotime($fecha_f))/60;
        $minutos = abs($minutos); 
        $minutos = floor($minutos);
        return $minutos;
    }
    public function minutos_transcurridos_con_signo($fecha_i,$fecha_f){ //Y-m-d H:i:s
        $minutos = (strtotime($fecha_i)-strtotime($fecha_f))/60;
        $minutos = $minutos;
        $minutos = floor($minutos);
        return $minutos;
    }
    // Dias transcurridos hasta hoy (Fecha en Y-m-d H:i:s)
    public function dias_transcurridos($fechaRegistro){
        $fechaActual = date("Y-m-d H:i:s");
        $segundosFechaActual = strtotime($fechaActual);
        $segundosFechaRegistro = strtotime($fechaRegistro);
        $segundosTranscurridos = $segundosFechaActual - $segundosFechaRegistro;
        $diasTranscurridos = floor($segundosTranscurridos/86400);
        return $diasTranscurridos;
    }
    public function horas_transcurridos($fechaRegistro){
        $fechaActual = date("Y-m-d H:i:s");
        $segundosFechaActual = strtotime($fechaActual);
        $segundosFechaRegistro = strtotime($fechaRegistro);
        $horasTranscurridas = floor(abs($segundosFechaActual - $segundosFechaRegistro)/3600);
        return $horasTranscurridas;
    }
    public function total_productos_por_agotar() {
        $this->db->where('stock < stock_minimo', NULL, FALSE)
                ->where('manejar_stock', 1);
        return $this->db->count_all_results('producto');
    }
    public function total_insumos_por_agotar() {
        $this->db->where('stock < stock_minimo', NULL, FALSE);
        return $this->db->count_all_results('insumo');
    }
    public function rellenar_izquierda($texto, $relleno, $longitud){
        if (strlen($texto) < $longitud) return $this->rellenar_izquierda($relleno.$texto,$relleno,$longitud);
        else return $texto;
    }
    public function proximos_cumples(){
        $myQuery = " SELECT * FROM empleado
                        WHERE DATE_ADD(fecha_nacimiento, INTERVAL YEAR(CURDATE()) - YEAR(fecha_nacimiento) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(fecha_nacimiento),1,0) YEAR) 
                        BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 5 DAY); 
                        ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    //Devuleve la ruta localhost del servidor
    public function get_ruta_servidor(){
        return "http://localhost:50/";
    }
    //Devulve la ruta del proyecto de imprésion de tickets
    public function get_ruta_proy_ticket(){
        return $this->get_ruta_servidor()."ticket/";
    }
    // Ruta de la aplicacion rstaurante
    public function get_ruta_restaurant(){
        return $this->get_ruta_servidor().NOMBRE_SISTEMA."/";
    }
    public function get_ruta_base_sunat(){
        return "C:/wamp64/www/SFS_v1.2/sunat_archivos/sfs/DATA/";
    }
    public function get_ruta_backups_documentos_sunat(){
        return "C:/wamp64/www/SFS_v1.2/backup_sunat/DATA/";
    }

    //Ruta para reportes pdf
    public function get_ruta_reportes_pdf(){
        return $_SERVER['DOCUMENT_ROOT'].NOMBRE_SISTEMA."/reportes_pdf/";
    }

    /* Considerar errores */
    public function registrar_log_error($tipo_error, $archivo, $mensaje, $usuario = null){
        $data['tipo_error'] = $tipo_error;
        $data['archivo'] = $archivo;
        $data['mensaje'] = $mensaje;
        $data['registrado_por'] = ($usuario == null)? "SIS":$usuario;
        $log_error_id = $this->Log_error_model->insert($data);
    }
    //::::::::::::::::::::::::::::::::::::::::::::::.... SERVIDORES O SERVICIOS ....::::::::::::::::::::::::::::::::::::::::::::::
    //Cabecera del comprobante electrónico
    public function get_url_webservice_facsu_facmcbe($tipo){
        //I: Insert,
        //G: Get
        //U: Update
        //D: Delete
        //GP: Get, con otros parametros
        $url = 'http://localhost:50/facsu/webservice/facmcbe_api/';
        switch ($tipo) {
            case "I": 
                $url.= "insert_facmcbe";
                break;
            case "G":
                $url.= "get_facmcbe";
                break;
        }
        return $url;
    }
    //Detalle del comprobante electrónico
    public function get_url_webservice_facsu_facdcbe($tipo){
        //I: Insert,
        //G: Get
        //U: Update
        //D: Delete
        //GP: Get, con otros parametros
        $url = 'http://localhost:50/facsu/webservice/facdcbe_api/';
        switch ($tipo) {
            case "I": 
                $url.= "insert_facdcbe";
                break;
        }
        return $url;
    }

    //Cabecera del comprobante electrónico
    public function get_url_webservice_facsu_facmres($tipo){
        //I: Insert,
        //G: Get
        //U: Update
        //D: Delete
        //GP: Get, con otros parametros
        $url = 'http://localhost:50/facsu/webservice/facmres_api/';
        switch ($tipo) {
            case "I": 
                $url.= "insert_facmres";
                break;
        }
        return $url;
    }
    /**/

    //El mismo método esta en Model y Controller
    public function obtener_variable($nombre_variable){
        $buscarVariable = $this->Variables_model->get_by_nombre_variable($nombre_variable);
        $oVariable = null;

        if(count($buscarVariable) >= 1){
            $oVariable = $buscarVariable[0];
        }

        if($oVariable == null){
            return "";
        }else{
            return $oVariable->valor_variable;
        }
    }
}















