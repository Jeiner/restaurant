<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Empleado_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $empleado_id = "0";

    public $tipo_documento = "DNI";
    public $nro_documento = "";
    public $nombres = "";
    public $ape_paterno = "";
    public $ape_materno = "";
    public $nombre_completo = "";
    public $datos_reniec = "";
    public $fecha_nacimiento = "";
    public $sexo = "";
    public $foto = "";
    public $telefono_1 = "";
    public $telefono_2 = "";
    public $correo = "";
    public $ubigeo = "";
    public $direccion = "";

    public $sede_id = "0";
    public $tipo_contrato = "";
    public $cargo_id = "0";
    public $fecha_ingreso = "";
    public $fecha_salida = "";
    public $sueldo_base_mensual = "";
    public $sueldo_base_semanal = "";
    public $cuenta_bancaria = "";
    public $nro_cuenta = "";
    public $estado = "A"; /*Activo, X:Eliminado*/
    public $observaciones = "";

    public $dia_pago = "";
    public $sucursal_codigo = "";


    public function insert($data){
        $this->db->insert('empleado', $data);
        return $this->db->insert_id();
    }
    public function update($data){
        $this->db->where('empleado_id', $data["empleado_id"]);

        $this->db->set('tipo_documento', $data["tipo_documento"]);
        $this->db->set('nro_documento', $data["nro_documento"]);
        $this->db->set('nombres', $data["nombres"]);
        $this->db->set('ape_paterno', $data["ape_paterno"]);
        $this->db->set('ape_materno', $data["ape_materno"]);
        $this->db->set('nombre_completo', $data["nombre_completo"]);
        // $this->db->set('datos_reniec', $data["datos_reniec"]);
        $this->db->set('fecha_nacimiento', $data["fecha_nacimiento"]);
        $this->db->set('sexo', $data["sexo"]);
        $this->db->set('foto', $data["foto"]);
        $this->db->set('telefono_1', $data["telefono_1"]);
        // $this->db->set('telefono_2', $data["telefono_2"]);
        $this->db->set('correo', $data["correo"]);
        // $this->db->set('ubigeo', $data["ubigeo"]);
        $this->db->set('direccion', $data["direccion"]);

        $this->db->set('sede_id', $data["sede_id"]);
        $this->db->set('tipo_contrato', $data["tipo_contrato"]);
        $this->db->set('cargo_id', $data["cargo_id"]);
        $this->db->set('fecha_ingreso', $data["fecha_ingreso"]);
        $this->db->set('fecha_salida', $data["fecha_salida"]);
        $this->db->set('sueldo_base_mensual', $data["sueldo_base_mensual"]);
        $this->db->set('sueldo_base_semanal', $data["sueldo_base_semanal"]);
        $this->db->set('cuenta_bancaria', $data["cuenta_bancaria"]);
        $this->db->set('nro_cuenta', $data["nro_cuenta"]);
        $this->db->set('estado', $data["estado"]);
        $this->db->set('observaciones', $data["observaciones"]);

        $this->db->set('dia_pago', $data["dia_pago"]);
        $this->db->set('sucursal_codigo', $data["sucursal_codigo"]);

        return $this->db->update('empleado');
    }
    
    public function get_all_by_filtros($filtro_estado = null, $filtro_sucursal = null, $filtro_cargo = null){
        $this->db->select("e.*");
        $this->db->from('empleado e');
        if($filtro_estado != null){
            $this->db->where('e.estado', $filtro_estado);
        }
        $this->db->order_by('e.nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all(){
        $this->db->select("
                        e.*,
                        c.cargo as 'cargo_desc'
                        ");
        $this->db->from('empleado e');
        $this->db->join('cargo c', 'c.cargo_id = e.cargo_id');
        // $this->db->join('sede s', 's.sede_id = e.sede_id');
        $this->db->order_by('e.nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all_order_dia_pago(){
        $this->db->select("
                        e.*,
                        c.cargo as 'cargo_desc',
                        s.sede as 'sede_desc'
                        ");
        $this->db->from('empleado e');
        $this->db->join('cargo c', 'c.cargo_id = e.cargo_id');
        $this->db->join('sede s', 's.sede_id = e.sede_id');
        // $this->db->order_by('e.nombre_completo', "asc");
        $this->db->where('e.estado', "A");
        $this->db->order_by('(e.dia_pago * 1)', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all_by_cumples(){ //Obtener empleados ordenados por cumpleaños 
        $this->db->select("
                        e.*,
                        c.cargo as 'cargo_desc',
                        s.sede as 'sede_desc'
                        ");
        $this->db->from('empleado e');
        $this->db->join('cargo c', 'c.cargo_id = e.cargo_id');
        $this->db->join('sede s', 's.sede_id = e.sede_id');
        $this->db->order_by('month(e.fecha_nacimiento)', "asc");
        $this->db->order_by('day(e.fecha_nacimiento)', "asc");
        $this->db->where('e.estado', "A");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one($empleado_id){
        $this->db->select("e.*,
                        c.cargo as 'cargo_desc',
                        '' as 'sede_desc'
                        ");
        $this->db->from('empleado e');
        $this->db->join('cargo c', 'c.cargo_id = e.cargo_id');
        // $this->db->join('sede s', 's.sede_id = e.sede_id');
        $this->db->where('e.empleado_id', $empleado_id);
        $this->db->order_by('e.nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one_by_documento($nro_documento){
        $this->db->select("e.*");
        $this->db->from('empleado e');
        $this->db->where('e.nro_documento', $nro_documento);
        $this->db->order_by('e.nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function pagos_by_empleado($empleado_id){
        $this->db->select("*");
        $this->db->from('liquidacion');
        $this->db->where('empleado_id', $empleado_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function eliminar_fisico($empleado_id){
        // Guardar en la papelera de reciclaje
        $this->db->where('empleado_id',$empleado_id);
        return $this->db->delete('empleado');
    }
    public function inactivar($empleado_id){
        $this->db->where('empleado_id', $empleado_id);
        $this->db->set('estado', "X");
        return $this->db->update('empleado');
    }
}

