<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function ventas_mensuales($data){
        $this->db->select("
            month(c.fecha_comanda) as 'mes',
            year(c.fecha_comanda) as 'anio',
            sum(importe_total) as 'importe',
            sum(nro_productos) as 'productos'
        ");
        $this->db->from('comanda c');
        $this->db->where('c.estado_atencion <>', "X");
        $this->db->where('year(c.fecha_comanda)', $data['anio']);
        $this->db->group_by('month(c.fecha_comanda), year(c.fecha_comanda) ');
        $query = $this->db->get();
        return $query->result();
    }
    public function ventas_diarias($data){
        $this->db->select("
            day(cj.aperturado_en) as 'dia',
            month(cj.aperturado_en) as 'mes',
            year(cj.aperturado_en) as 'anio',
            sum(importe_total) as 'importe',
            sum(nro_productos) as 'productos'
        ");
        $this->db->from('comanda c');
        $this->db->join('caja cj', 'cj.caja_id = c.caja_id');
        $this->db->where('c.estado_atencion <>', "X");
        $this->db->where('year(cj.aperturado_en)', $data['anio']);
        $this->db->where('month(cj.aperturado_en)', $data['mes']);
        $this->db->group_by('day(cj.aperturado_en), month(cj.aperturado_en), year(cj.aperturado_en) ');
        $query = $this->db->get();
        return $query->result();
    }
    public function ranking_mozos($anio = "0", $mes = "0", $dia = "0"){
        $myQuery = "select 
                        day(cj.aperturado_en) as 'dia',
                        month(cj.aperturado_en) as 'mes',
                        year(cj.aperturado_en) as 'anio',
                        c.mozo as 'usuario',
                        u.nombre_completo as 'nombre_completo',
                        count(*) as 'nro_comandas',
                        sum(c.importe_total) as 'importe'
                        from comanda c
                        inner join caja cj on cj.caja_id = c.caja_id
                        inner join usuario u on c.mozo = u.usuario
                        where  c.estado_atencion <> 'X' and
                                (day(cj.aperturado_en)  = ".$dia." or ".$dia." = 0)and
                                (month(cj.aperturado_en) =  ".$mes." or ".$mes." = 0) and 
                                (year(cj.aperturado_en) = ".$anio." or ".$anio." = 0)
                        group by c.mozo, u.nombre_completo, day(cj.aperturado_en), month(cj.aperturado_en),year(cj.aperturado_en)
                        order by mes asc, importe desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function ranking_mozos_mensual($anio = "0", $mes = "0"){
        $myQuery = "select 
                        month(cj.aperturado_en) as 'mes',
                        year(cj.aperturado_en) as 'anio',
                        c.mozo as 'usuario',
                        u.nombre_completo as 'nombre_completo',
                        count(*) as 'nro_comandas',
                        sum(c.importe_total) as 'importe'
                        from comanda c
                        inner join caja cj on cj.caja_id = c.caja_id
                        inner join usuario u on c.mozo = u.usuario
                        where  c.estado_atencion <> 'X' and
                                (month(cj.aperturado_en) =  ".$mes." or ".$mes." = 0) and 
                                (year(cj.aperturado_en) = ".$anio." or ".$anio." = 0)
                        group by c.mozo, u.nombre_completo, month(cj.aperturado_en),year(cj.aperturado_en)
                        order by mes asc, importe desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function ranking_productos_by_fechas($start_date, $end_date){
        $this->db->select("
                            trim(replace(replace(replace(replace(replace(ci.producto,'(Mediana)',''),'(Extra)',''),'(Familiar)',''),'(Personal)',''),'1/4','')) as 'producto',
                            sum(ci.cantidad) as 'cantidad', 
                            sum(ci.importe) as 'importe'
                        ");
        $this->db->from('comanda_item ci');
        $this->db->join('comanda co', 'co.comanda_id = ci.comanda_id' );
        $this->db->join('caja cj', 'cj.caja_id = co.caja_id' );
        $this->db->join('producto p', 'p.producto_id = ci.producto_id');
        $this->db->where('date(cj.aperturado_en) >=',$start_date);
        $this->db->where('date(cj.aperturado_en) <=',$end_date);
        $this->db->where('ci.estado_atencion <>','x');
        $this->db->group_by("p.familia_id, trim(replace(replace(replace(replace(replace(ci.producto,'(Mediana)',''),'(Extra)',''),'(Familiar)',''),'(Personal)',''),'1/4',''))");
        $this->db->order_by('importe','desc'); 
        $query = $this->db->get();
        return $query->result();
    }
    public function historico_producto_by_mes($producto_id, $anio){
        // Evolución histórica mensual de un determinado año
        $this->db->select("
                month(cj.aperturado_en) as 'mes',
                year(cj.aperturado_en) as 'anio',
                sum(ci.importe) as 'importe',
                sum(cantidad) as 'cantidad'
        ");
        $this->db->from('comanda_item ci');
        $this->db->join('comanda co', 'co.comanda_id = ci.comanda_id' );
        $this->db->join('caja cj', 'cj.caja_id = co.caja_id' );
        $this->db->where('ci.estado_atencion <>', "X");
        $this->db->where('ci.producto_id', $producto_id);
        $this->db->where('year(cj.aperturado_en)', $anio);
        $this->db->group_by('month(cj.aperturado_en), year(cj.aperturado_en) ');
        $query = $this->db->get();
        return $query->result();
    }
    public function liquidaciones_mensuales($anual = "0", $empleado_id = "0", $sede_id = "0"){
        $myQuery = "select 
                        month(l.fecha_pago) as 'mes',
                        year(l.fecha_pago) as 'anio',
                        sum(l.total_a_pagar) as 'importe'
                        from liquidacion l
                        where l.estado != 'X' and
                            (year(l.fecha_pago) = ".$anual." or ".$anual." = 0) and 
                            (l.empleado_id = ".$empleado_id." or ".$empleado_id." = 0) and
                            (l.sede_id = ".$sede_id." or ".$sede_id." = 0)
                        group by month(l.fecha_pago), year(l.fecha_pago) 
                       ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
}
