<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_comandas_by_caja($caja_id){
        $myQuery = "select * from comanda where caja_id = '".$caja_id."'";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function totales($caja_id){
        $myQuery = "select
                        sum(importe_total) as 'total_ventas',
                        sum(descuento) as 'total_descuentos',
                        sum(total_pagado) as 'total_pagado',
                        caja_id
                        from comanda 
                        where caja_id = '".$caja_id."' and estado_atencion <> 'X'";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function comsumo_promedio_x_mesa($caja_id){
        $myQuery = "select 
                        sum(importe_total) as 'importe_total',
                        count(*) as'nro_comandas',
                        sum(importe_total) / count(*) as 'promedio_x_mesa'
                        from comanda
                        where caja_id = '".$caja_id."' and mesa_id is not null and estado_atencion <> 'X'";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function mozo_del_dia($caja_id){
        $myQuery = "select 
                    c.mozo,
                    u.nombre_completo,
                    count(*) as 'nro_comandas',
                    sum(c.importe_total)
                    from comanda c
                    inner join usuario u on c.mozo = u.usuario
                    where c.caja_id = '".$caja_id."' and c.estado_atencion <> 'X'
                    group by c.mozo, u.nombre_completo
                    order by nro_comandas desc
                    limit 1";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function tiempo_promedio_atencion($caja_id){
        $myQuery = "select
                        min(TIMESTAMPDIFF(MINUTE, fecha_comanda, finalizada_en)) AS 'tiempo_min_atencion',
                        max(TIMESTAMPDIFF(MINUTE, fecha_comanda, finalizada_en)) AS 'tiempo_max_atencion'
                        from comanda
                        where caja_id = '".$caja_id."' and estado_atencion <> 'X'
                        ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function pagos_x_tipo($caja_id){
        $myQuery = "select 
                        sum(pago_efectivo) as pago_efectivo,
                        sum(pago_tarjeta) as pago_tarjeta,
                        sum(total_pagado) as total_pagado
                        from pago
                        where caja_id = '".$caja_id."' and estado = 'A'
                        ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function atenciones($caja_id){
        $myQuery = "select * from 
                        (select     
                            count(*) as 'atenciones_pre'
                            from comanda mpre
                            where mpre.modalidad = 'PRE' and caja_id = '".$caja_id."' and estado_atencion <> 'X') pre
                        join
                        (select     
                            count(*) as 'atenciones_del'
                            from comanda mpre
                            where mpre.modalidad   = 'DEL' and caja_id = '".$caja_id."' and estado_atencion <> 'X') del
                        join
                        (select     
                            count(*) as 'atenciones_lle'
                            from comanda mpre
                            where mpre.modalidad   = 'LLE' and caja_id = '".$caja_id."' and estado_atencion <> 'X') lle
                        join 
                        (select     
                            count(*) as 'atenciones_anu'
                            from comanda 
                            where caja_id = '".$caja_id."' and estado_atencion = 'X') anu
                        ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

}

