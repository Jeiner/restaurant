<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Concepto_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $concepto_id = "0";
    public $concepto = "";
    public $tipo_concepto = "";
    public $signo = "";
    public $estado = "A";  //'A:activo, X:Eliminado',

    public function get_all(){
        $this->db->select("
                        *
                        ");
        $this->db->from('concepto');
        $this->db->order_by('concepto', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one($concepto_id){
        $this->db->select("
                        *
                        ");
        $this->db->from('concepto');
        $this->db->where('concepto_id',$concepto_id);
        $this->db->order_by('concepto', "desc");
        $query = $this->db->get();
        return $query->result();
    }
}

