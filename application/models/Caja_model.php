<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Caja_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // if(count($this->get_aperturada()) == 0){
        //     echo "Para ingresar a este módulo debe aperturar la caja.";
        //     exit();
        // }
    }
	public $caja_id = "0";
	public $aperturado_en = "";
	public $aperturado_por = "";
	public $monto_inicial = "0.00";
	public $cerrado_en = "";
	public $cerrado_por = "";
	public $monto_final = "";
	public $monto_retirado = "";
	public $remanente = "";
	public $observaciones = "";
	public $estado = "A";

    public function aperturar($data){
        $this->db->insert('caja', $data);
        return $this->db->insert_id();
    }
    public function update_sync_marcaciones($caja_id){
        $this->db->where('caja_id', $caja_id);
        $this->db->set('fecha_sync_marcaciones', date('Y-m-d H:i:s'));
        return $this->db->update('caja');
    }
    public function update_sync_procesamiento_cpbe($caja_id){
        $this->db->where('caja_id', $caja_id);
        $this->db->set('fecha_procesar_cpbe', date('Y-m-d H:i:s'));
        return $this->db->update('caja');
    }
    public function cerrar($data){
        $this->db->where('caja_id', $data["caja_id"]);
        return $this->db->update('caja', $data);
    }
    public function verificar_caja_aperturada(){
        // Verifica que la caja este aperturada, sino es asi regresa al panel
        $this->db->select("*");
        $this->db->from('caja');
        $this->db->where('estado','A');
        $query = $this->db->get();
        $buscarCaja = $query->result();

        if($buscarCaja == null || count( $buscarCaja ) == 0){
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',"Para ingresar a esta opción es necario aperturar la caja.");
            redirect(base_url('main/panel'));
        }

        $oCaja = $buscarCaja[0];
        $horasTranscurridas = $this->Site->horas_transcurridos($oCaja->aperturado_en);
        $horasMaxPermitidas = intval($this->obtener_variable("max_hora_apertura_caja"));

        //Validar que la caja no tenga mas de 15 horas aperturada.
        if($horasTranscurridas > $horasMaxPermitidas){
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',"La caja excede las ".$horasMaxPermitidas." horas de apertura. Debe cerrar caja y abrir nuevamente.");
            redirect(base_url('main/panel'));
        }
    }

    public function get_aperturada(){
        // Obtiene la caja aperturada
        $this->db->select("*");
        $this->db->from('caja');
        $this->db->where('estado','A');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one($caja_id){
        // Obtiene la caja aperturada
        $this->db->select("*");
        $this->db->from('caja');
        $this->db->where('caja_id',$caja_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        // Obtiene todas  las cajas
        $this->db->select("*");
        $this->db->from('caja');
        $this->db->order_by('aperturado_en', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    function movimientos_caja($caja_id){
        //Obtiene los pagos y los gastos de una determinada caja
        // Por lo general de la activa
        $myQuery = "select * from (
                        select  
                            p.pago_id as 'codigo',
                            'Venta' as 'operacion',
                            p.pago_efectivo as 'monto',
                            p.fecha_pago as 'fecha',
                            p.cajero as 'usuario',
                            p.caja_id as 'caja_id',
                            p.estado as 'estado'
                            from pago p
                            where p.caja_id = ".$caja_id." and p.pago_efectivo <> 0 and p.estado = 'A'
                        union
                        select 
                            g.gasto_id as 'codigo',
                            'Gasto' as 'operacion',
                            g.monto as 'monto',
                            g.fecha as 'fecha',
                            g.usuario as 'usuario',
                            g.caja_id as 'caja_id',
                            g.estado as 'estado'
                            from gasto g
                            where g.caja_id = ".$caja_id." and g.monto <> 0 and g.estado = 'A' and g.tipo = 'EFEC'
                    ) as tb
                    order by tb.fecha desc";
        $query = $this->db->query($myQuery, false);
        return $query->result();
    }
    function movimientos_tarjeta($caja_id){
        //Obtiene los pagos y los gastos de una determinada caja
        // Por lo general de la activa
        $myQuery = "select * from (
                        select  
                            p.pago_id as 'codigo',
                            'Venta' as 'operacion',
                            p.pago_tarjeta as 'monto',
                            p.fecha_pago as 'fecha',
                            p.cajero as 'usuario',
                            p.caja_id as 'caja_id',
                            p.estado as 'estado'
                            from pago p
                            where p.caja_id = ".$caja_id." and p.pago_tarjeta <> 0 and p.estado = 'A'
                        union
                        select 
                            g.gasto_id as 'codigo',
                            'Gasto' as 'operacion',
                            g.monto as 'monto',
                            g.fecha as 'fecha',
                            g.usuario as 'usuario',
                            g.caja_id as 'caja_id',
                            g.estado as 'estado'
                            from gasto g
                            where g.caja_id = ".$caja_id." and g.monto <> 0 and g.estado = 'A' and g.tipo = 'TARJ'
                    ) as tb
                    order by tb.fecha desc";
        $query = $this->db->query($myQuery, false);
        return $query->result();
    }
    public function get_all_by_fechas($start_date, $end_date){
        $myQuery = "select * from caja 
                        where date(aperturado_en) >= '".$start_date."' 
                            and date(aperturado_en) <= '".$end_date."' 
                        order by aperturado_en desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function ultima_caja(){
        $myQuery = "select * from caja
                        order by caja_id desc
                        limit 1";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
}

