<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comanda_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	public $comanda_id = "0";
    public $modalidad = "PRE"; //'PRE:presencial, DEL:Delivery, LLE:Llevar,',
	public $fecha_comanda = "";
	public $mozo_id = "";
	public $mozo = "";
	public $mesa_id = "";
	public $mesa = "";
	public $nro_comensales = "";
	public $prioridad = "";
	public $nro_productos = "0";
	public $importe_total = "0";
	public $importe_pagado = "0";
	public $fecha_pago = "";
	public $tipo_comprobante = "";
	public $cliente_id = "";
    public $cliente_documento = "";
	public $cliente_nombre = "";
    public $cliente_direccion = "";
    public $cliente_telefono = "";
	public $caja_id = "";
	public $estado_atencion = "E"; //'E:En espera, A:Atendido, F:Finalizado',
	public $estado_pago = "P"; //'P:Pendiente, C:Cancelado'

    public $con_comprobante_e = "";
    public $sunat_tipo_comprobante_id = "";
    public $canal = "";
    public $cliente_correo = "";
    public $cliente_tipo_documento = "";

    public $items = array();

    // Insertar nueva comanda con sus items.
    public function insert($data, $items){
        if( $this->db->insert('comanda', $data) ){
            $comanda_id = $this->db->insert_id();
            foreach ($items as $key => $item) {
                $item['comanda_id'] = $comanda_id;
                $this->db->insert('comanda_item', $item);
            }
        }
        return $comanda_id;
    }
    // Insertar producto adicional
    public function agregar_producto_adicional($data){
        if ($this->db->insert('comanda_item', $data)) {
            return $this->db->insert_id();
        }
    }
    public function get_one($comanda_id){ 
        $this->db->select(" 
                            c.*,
                            moda.descripcion as 'modalidad_desc',
                            IFNULL(canal.descripcion, '-') as 'canal_desc'
                            ");
        $this->db->from('comanda c');        
        $this->db->where('c.comanda_id',$comanda_id);
        $this->db->join('multitabla moda','moda.valor = c.modalidad and moda.tipo_multitabla_id = 3','left');
        $this->db->join('multitabla canal','canal.valor = c.canal and canal.tipo_multitabla_id = 40','left');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one_por_mesa_actual($mesa_id){ 
        //Obetner la comanda de la mesa que esta actualmente ocupando
        $this->db->select("*");
        $this->db->from('comanda');
        $this->db->where('mesa_id',$mesa_id);
        $this->db->where('estado_atencion !=',"F");
        $this->db->where('estado_atencion !=',"X");
        $query = $this->db->get();
        return $query->result();
    }
    // todas las comandas entre un rango de fecha
    public function get_all_by_fechas($start_date, $end_date, $modalidad = '*', $canal = '*'){
        $myQuery = "select 
                        c.*,
                        moda.descripcion as 'modalidad_desc',
                        ea.descripcion as 'estado_atencion_desc',
                        ep.descripcion as 'estado_pago_desc',
                        canal.descripcion as 'canal_desc'
                    from comanda c

                    left join multitabla moda on moda.valor = c.modalidad and moda.tipo_multitabla_id = 3
                    LEFT JOIN multitabla ea on ea.valor = c.estado_atencion and ea.tipo_multitabla_id = 4
                    LEFT JOIN multitabla ep on ep.valor = c.estado_pago and ep.tipo_multitabla_id = 5
                    LEFT JOIN multitabla canal on canal.valor = c.canal and canal.tipo_multitabla_id = 40

                    where date(c.fecha_comanda) >= '".$start_date."' 
                            and date(c.fecha_comanda) <= '".$end_date."'
                            and (c.modalidad = '".$modalidad."' OR  '".$modalidad."' = '*')
                            and (c.canal = '".$canal."' OR  '".$canal."' = '*')
                    order by c.fecha_comanda desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    public function get_all_by_caja($caja_id){
        //Obtener todas las comandas de una caja especifica
        $this->db->select(" 
                            c.*,
                            moda.descripcion as 'modalidad_desc'");
        $this->db->from('comanda c');
        $this->db->where('c.caja_id',$caja_id);
        $this->db->join('multitabla moda','moda.valor = c.modalidad and moda.tipo_multitabla_id = 3','left');
        $this->db->order_by('comanda_id', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function prefacturar($data){
        $this->db->where('comanda_id', $data["comanda_id"]);
        return $this->db->update('comanda', $data);
    }
    public function comanda_atendida($comanda_id){
        // Si todos los productos de una comanda fueron despchados, la comanda pasará a atendida.
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set('estado_atencion', 'A');
        $this->db->set('atendida_en', date("Y/m/d H:i:s"));
        return $this->db->update('comanda');
    }   
    public function finalizar_comanda($comanda_id){
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set('estado_atencion', 'F');
        $this->db->set('finalizada_en', date("Y/m/d H:i:s"));
        $this->db->set('finalizada_por', $this->session->userdata('session_usuario'));
        return $this->db->update('comanda');
    }


    public function pagar_comanda($data){
        $this->db->where('comanda_id', $data["comanda_id"]);
        $this->db->set('descuento', $data['descuento']);
        $this->db->set('total_pagado', $data['total_pagado']);
        $this->db->set('fecha_pago', $data['fecha_pago']);
        $this->db->set('pagado_por', $data['cajero']);
        $this->db->set('pago_id', $data['pago_id']);
        $this->db->set('tipo_comprobante', $data['tipo_comprobante']);
        $this->db->set('cliente_id', $data['cliente_id']);
        $this->db->set('cliente_documento', $data['cliente_documento']);
        $this->db->set('cliente_nombre', $data['cliente_nombre']);
        $this->db->set('cliente_direccion', $data['cliente_direccion']);
        $this->db->set('cliente_telefono', $data['cliente_telefono']);
        $this->db->set('estado_pago','C');
        return $this->db->update('comanda');
        // return $this->db->update('comanda_item');
    }
    public function actualizar_importe_comanda($comanda_id, $importe_total, $nro_productos){
        $igv = $this->obtener_igv();
        $parametro_sin_igv = floatval(1 + floatval($igv));
        
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set('importe_total', $importe_total);
        $this->db->set('importe_sin_IGV', ($importe_total/$parametro_sin_igv));
        $this->db->set('total_IGV', ($importe_total-($importe_total/$parametro_sin_igv)));
        $this->db->set('nro_productos', $nro_productos);
        return $this->db->update('comanda');
    }
    public function anular_comanda($data){
        $this->db->where('comanda_id', $data["comanda_id"]);
        return $this->db->update('comanda', $data);
    }
    public function cambiar_estado_atencion($comanda_id, $nuevo_estado){
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set('estado_atencion', $nuevo_estado);
        return $this->db->update('comanda');
    }
    public function actualizar_por_union($comanda_id, $nuevas_mesas){
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set('mesa', $nuevas_mesas);
        $this->db->set('union_mesas', 1);
        return $this->db->update('comanda');
    }
    //Metodo general para actualiar una comanda
    public function actualizar_comanda($comanda_id,$nueva_data){
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set($nueva_data);
        return $this->db->update('comanda');
    }

    // FUNCIONALIDADES DE LA COMANDA ITEM

    public function get_items_by_comanda($comanda_id){
        //Obtener lista de items por comanda especifica
        $this->db->select("*");
        $this->db->from('comanda_item');
        $this->db->where('comanda_id',$comanda_id);
        $this->db->order_by('comanda_item_id', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_item_by_comanda($comanda_item_id){
        //Obtiene solo un item de una comanda especifica
        $this->db->select("*");
        $this->db->from('comanda_item');
        $this->db->where('comanda_item_id',$comanda_item_id);
        // $this->db->where('producto_id',$producto_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_items_para_cocina_by_caja($caja_id, $estado_atencion = ''){ 
        //Obtiene todos los items que irán a cocina durante la caja activa
        $myQuery = "select 
                        ci.*,
                        co.mesa,
                        co.mozo   

                        from comanda_item ci 
                        inner join comanda co on co.comanda_id = ci.comanda_id
                        where ci.para_cocina = 1 
                            and  co.caja_id = '".$caja_id."'
                            and (ci.estado_atencion = '".$estado_atencion."' || '".$estado_atencion."' = '')
                        order by ci.pedido_en desc, ci.comanda_item_id desc
                    ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
    // Para verificar cada dos segundos
    public function max_pendiente_cocina($caja_id){
        $this->db->select('max(comanda_item_id) as max_pendiente_cocina');
        $this->db->where('para_cocina', '1');
        $this->db->where('estado_atencion', 'E');
        $this->db->from('comanda_item');
        return $this->db->get()->row()->max_pendiente_cocina;
    }
    public function nro_pedidos_anulados($caja_id){
        $this->db->select('count(*) as nro_pedidos_anulados');
        $this->db->where('para_cocina', '1');
        $this->db->where('estado_atencion', 'X');
        $this->db->from('comanda_item');
        return $this->db->get()->row()->nro_pedidos_anulados;
    }



    public function preparar_pedido($comanda_item_id){
        $this->db->where('comanda_item_id', $comanda_item_id);
        $this->db->set('estado_atencion', 'P');
        return $this->db->update('comanda_item');
    }
    public function despachar_pedido($data){
        $this->db->where('comanda_item_id', $data["comanda_item_id"]);
        return $this->db->update('comanda_item', $data);
    }
    public function regresar_a_pendiente($comanda_item_id){
        $this->db->where('comanda_item_id', $comanda_item_id);
        $this->db->set('estado_atencion', 'E');
        return $this->db->update('comanda_item');
    }
    public function anular_item($comanda_item_id){
        $this->db->where('comanda_item_id', $comanda_item_id);
        $this->db->set('estado_atencion', 'X');
        $this->db->set('precio_unitario', 0);
        $this->db->set('importe', 0);
        $this->db->set('anulado_en', date("Y/m/d H:i:s"));
        $this->db->set('anulado_por', $this->session->userdata('session_usuario'));
        return $this->db->update('comanda_item');
    }

    public function actualizar_comanda_cliente($data){
        $this->db->where('comanda_id', $data["comanda_id"]);
        $this->db->set('cliente_documento',$data["cliente_documento"]);
        $this->db->set('cliente_nombre',$data["cliente_nombre"]);
        $this->db->set('cliente_direccion',$data["cliente_direccion"]);
        $this->db->set('cliente_telefono',$data["cliente_telefono"]);
        $this->db->set('cliente_tipo_documento',$data["cliente_tipo_documento"]);
        $this->db->set('cliente_correo',$data["cliente_correo"]);
        return $this->db->update('comanda');
    }

    // Obtener numero de ordenes referenciados a esta comanda






    //PARA SINCRONIZACIÓN

    //Obtiene comandas que no están sincronizadas
    public function sync_get_comandas_not_sync(){
        $this->db->select("*");
        $this->db->from('comanda c');
        $this->db->where('c.sync', 0);
        $this->db->limit(10);
        // $this->db->join('multitabla moda','moda.valor = c.modalidad and moda.tipo_multitabla_id = 3','left');
        $query = $this->db->get();
        return $query->result();
    }
    //Actualizar estado de sincronizaciòn  de la comanda
    public function comanda_esta_sincronizada($comanda_id){
        $this->db->where('comanda_id', $comanda_id);
        $this->db->set('sync', '1');
        $this->db->set('fecha_sync', date("Y/m/d H:i:s"));
        return $this->db->update('comanda');
    }

    //METODOS DEL SERVIDOR REMOTO
    //Envia comandas e items  al servidor remoto
    public function sync_enviar_comanda($data, $items,$pagos, $db_remoto){
        $db_remoto->trans_begin();

        if( $db_remoto->insert('comanda', $data) ){
            $comanda_id = $db_remoto->insert_id();
            foreach ($items as $key => $item) {
                $db_remoto->insert('comanda_item', $item);
            }
            foreach ($pagos as $key => $pago) {
                $db_remoto->insert('pago', $pago);
            }
        }

        if ($db_remoto->trans_status() === FALSE){
            $db_remoto->trans_rollback();
        }
        else{
            $db_remoto->trans_commit();
        }
        return $comanda_id;
    }


    //DOCUMENTO ELECTRONICO
     //Actualiza la comanda al EMITIR DOCUMENTO ELECTRONICO
    public function add_documento_e($data){
        $this->db->where('comanda_id', $data["comanda_id"]);
        $this->db->set('con_comprobante_e',$data["con_comprobante_e"]);
        return $this->db->update('comanda');
    }

    //Obtener correlativo por día 
    public function get_correlativo_por_dia($modalidad, $caja_id){
        $myQuery = "SELECT COUNT(1) + 1 AS correlativo
                        FROM comanda 
                        WHERE modalidad = '".$modalidad."'
                        AND caja_id = '".$caja_id."'
                        ORDER BY fecha_comanda DESC
                    ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

}

