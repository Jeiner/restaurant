<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sede_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $sede_id = "0";
    public $sede = "";
    public $ubigeo = "";
    public $direccion = "";
    public $telefono = "";
    public $estado = "A";  //'A:activo, X:Eliminado',

    public function get_all(){
        $this->db->select("*");
        $this->db->from('sede');
        $this->db->order_by('sede', "asc");
        $query = $this->db->get();
        return $query->result();
    }
}

