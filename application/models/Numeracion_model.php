<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Numeracion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $tipo_comprobante = "";
    public $serie = "";
    public $correlativo = "";
    public $correlativo_num = "";

    //Obtiene los correlativos de comprobantes de una determinada sede.
    public function get_numeracion_comprobante($sede, $tipo_comprobante = ""){
        $this->db->select("*");
        $this->db->from('numeracion');
        $this->db->where('sede', $sede);
        if ($tipo_comprobante != "") {
            $this->db->where('tipo_comprobante', $tipo_comprobante);
        }
        $query = $this->db->get();
        return $query->result();
    }
    public function update_numeracion($sede,$tipo_comprobante, $correlativo, $correlativo_num){
        $this->db->where('tipo_comprobante', $tipo_comprobante);
        $this->db->where('sede', $sede);
        $this->db->set('correlativo', $correlativo);
        $this->db->set('correlativo_num', $correlativo_num);
        return $this->db->update('numeracion');
    }
}