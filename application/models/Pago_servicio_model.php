<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pago_servicio_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    public $pago_servicio_id = "0";
    public $cronograma_pago_id = "";
    public $sede_id = "0";
    public $fecha_pago = "";
    public $proveedor_id = "0";
    public $descripcion = "";
    public $monto_pagado = "";
    public $modo_pago = "";
    public $observaciones = "";
    public $estado = "A"; /*A:Activo, X:Anulado*/

    public $fecha_registro = "";
    public $registrado_por = "";
    public $fecha_modificacion = "";
    public $modificado_por = "";

    public function insert($data){
        return $this->db->insert('pago_servicio', $data);
    }
    public function update($data){
        $this->db->where('pago_servicio_id', $data["pago_servicio_id"]);

        $this->db->set('pago_servicio_id', $data['pago_servicio_id']);
        $this->db->set('cronograma_pago_id', $data['cronograma_pago_id']);
        $this->db->set('sede_id', $data['sede_id']);
        $this->db->set('fecha_pago', $data['fecha_pago']);
        $this->db->set('proveedor_id', $data['proveedor_id']);
        $this->db->set('descripcion', $data['descripcion']);
        $this->db->set('monto_pagado', $data['monto_pagado']);
        $this->db->set('modo_pago', $data['modo_pago']);
        $this->db->set('observaciones', $data['observaciones']);
        $this->db->set('estado', $data['estado']);
        // $this->db->set('modificado_por', $data['modificado_por']);

        return $this->db->update('pago_servicio');
    }
    public function get_all(){
        $this->db->select("
                        ps.*,
                        s.sede as 'sede_desc',
                        p.razon_social as 'razon_social'
                        ");
        $this->db->from('pago_servicio ps');
        $this->db->join('sede s', 's.sede_id = ps.sede_id');
        $this->db->join('proveedor p', 'p.proveedor_id = ps.proveedor_id');
        $this->db->order_by('ps.pago_servicio_id', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one($pago_servicio_id){
        $this->db->select("
                        ps.*,
                        s.sede as 'sede_desc',
                        p.razon_social as 'razon_social'
                        ");
        $this->db->from('pago_servicio ps');
        $this->db->join('sede s', 's.sede_id = ps.sede_id');
        $this->db->join('proveedor p', 'p.proveedor_id = ps.proveedor_id');
        $this->db->where('ps.pago_servicio_id', $pago_servicio_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function anular($pago_servicio_id){
        $this->db->where('pago_servicio_id',$pago_servicio_id);
        $this->db->set('estado', 'X');
        return $this->db->delete('pago_servicio');
    }
   
    public function eliminar_fisico($pago_servicio_id){
        // Guardar en la papelera de reciclaje
        return false;
        $this->db->where('pago_servicio_id',$pago_servicio_id);
        return $this->db->delete('pago_servicio');
    }
}

