<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $cliente_id = "0";
    public $tipo_cliente = ""; //N:Natural, J: Jurídico
    public $documento = "";
    public $ape_paterno = "";
    public $ape_materno = "";
    public $nombres = "";
    public $nombre_completo = "";
    public $fecha_nacimiento = "";
    public $sexo = "M"; //'M:masculino, F:Femenino',
    public $contacto = "";
    public $telefono = "";
    public $correo = "";
    public $direccion = "";
    public $estado = "A";   //'A:activo, X:Inactivo'
    public $sunat_tipo_documento_id = "";

    public function insert($data){
        $this->db->insert('cliente', $data);
        return $this->db->insert_id();
    }
    public function update($data){
        $this->db->where('cliente_id', $data["cliente_id"]);
        return $this->db->update('cliente', $data);
    }
    public function delete($cliente_id){
        $this->db->where('cliente_id',$cliente_id);
        return $this->db->delete('cliente');
    }
    public function get_one($cliente_id){
        $this->db->select("*");
        $this->db->from('cliente');
        $this->db->where('cliente_id',$cliente_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("C.*,
                            TP.valor_constante as tipo_documento_desc
                            ");
        $this->db->from('cliente C');
        $this->db->join('multitabla TP','TP.valor = C.sunat_tipo_documento_id and TP.tipo_multitabla_id = 22','left');
        $this->db->order_by('nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_by_doc($documento){
        $this->db->select("*");
        $this->db->from('cliente');
        $this->db->where('documento',$documento);
        $this->db->order_by('nombre_completo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    // Obtener numero de ordenes referenciados a este cliente

}

