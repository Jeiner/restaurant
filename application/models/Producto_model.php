<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Producto_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $producto_id = "0";
    public $producto = "";
    public $unidad = "und";
    public $precio_costo = "";
    public $precio_unitario = "";
    public $para_cocina = "1";
    public $manejar_stock = "";
    public $stock = "";
    public $stock_minimo = "";
    public $stock_maximo = "";
    public $fecha_vencimiento = "";
    public $familia_id = "";
    public $foto = "";
    public $codigo_barras = "";
    public $descripcion = "";
    public $estado = "A";//'A:activo, X:Inactivo'
    public $ubi_carta = "";

    public $insumos = array();

    public function insert($data, $insumos){
        // return $this->db->insert('producto', $data);
        $this->db->trans_begin();
        if( $this->db->insert('producto', $data) ){
            $producto_id = $this->db->insert_id();
            foreach ($insumos as $key => $insumo) {
                // echo json_encode($insumo);exit();
                $insumo_data = array( 
                            'producto_id' => $producto_id,
                            'insumo_id' => $insumo->insumo_id,
                            'cantidad' => $insumo->cantidad,
                            'unidad' => $insumo->unidad,
                        );
                $this->db->insert('insumos', $insumo_data);
            }
        }
        if ($this->db->trans_status() === FALSE){
            //Hubo errores en la consulta, entonces se cancela la transacción.   
            $this->db->trans_rollback();      
            return false;    
        }else{      
            //Todas las consultas se hicieron correctamente.  
            $this->db->trans_commit();    
            return $producto_id;    
        }  
    }
    public function update($data, $insumos){
        $this->db->trans_begin();

        $this->db->where('producto_id', $data["producto_id"]);
        if($this->db->update('producto', $data)){
            $this->db->where('producto_id',$data["producto_id"]);
            if($this->db->delete('insumos')){
                foreach ($insumos as $key => $insumo) {
                    // echo json_encode($insumo);exit();
                    $insumo_data = array( 
                                'producto_id' => $data["producto_id"],
                                'insumo_id' => $insumo->insumo_id,
                                'cantidad' => $insumo->cantidad,
                                'unidad' => $insumo->unidad,
                            );
                    $this->db->insert('insumos', $insumo_data);
                }
            }
        }
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();      
            return false;    
        }else{      
            $this->db->trans_commit();    
            return true;    
        }  
    }
    public function delete($producto_id){
        $this->db->where('producto_id',$producto_id);
        return $this->db->delete('producto');
    }
    public function get_one($producto_id){
        $this->db->select("*");
        $this->db->from('producto');
        $this->db->where('producto_id',$producto_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("p.*,f.familia");
        $this->db->from('producto p');
        $this->db->join('familia f','f.familia_id = p.familia_id');
        $this->db->order_by('p.producto', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_agregados(){
        $this->db->select("p.*,f.familia");
        $this->db->from('producto p');
        $this->db->join('familia f','f.familia_id = p.familia_id');
        $this->db->where('p.familia_id', 3);
        $this->db->order_by('p.producto', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    //Productos para ventas (Solo activos)
    public function get_by_familia($familia_id = 0, $ordenar = 'ubicacion'){
        // Obtiene todos los productos de una determinada familia
        $this->db->select("p.*,f.familia");
        $this->db->from('producto p');
        $this->db->where('p.estado', 'A');
        $this->db->join('familia f','f.familia_id = p.familia_id');
        if($familia_id != 0){
            $this->db->where('p.familia_id',$familia_id);
        }

        //Ordenar
        switch ($ordenar) {
            case 'ubicacion':
                $this->db->order_by('p.ubi_carta', "asc");
                break;
            case 'producto':
                $this->db->order_by('p.producto', "asc");
                break;
            case 'precio':
                $this->db->order_by('p.precio_unitario', "asc");
                break;
            default:
                break;
        }
        
        //Devolver
        $query = $this->db->get();
        return $query->result();
    }
    public function productos_por_agotar(){
        $this->db->select("
                            p.*,
                            mu.descripcion as unidad_desc
                        ");
        $this->db->from('producto p');
        $this->db->join('multitabla mu','mu.valor = p.unidad and tipo_multitabla_id = 1', 'left');
        $this->db->where('p.stock_minimo >= p.stock');
        $this->db->order_by('p.producto', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function insumos_por_agotar(){
        $this->db->select("
                            i.*,
                            mu.descripcion as unidad_desc
                        ");
        $this->db->from('insumo i');
        $this->db->join('multitabla mu','mu.valor = i.unidad and tipo_multitabla_id = 1', 'left');
        $this->db->where('i.stock_minimo >= i.stock');
        $this->db->order_by('i.insumo', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    // Obtenemos los insumos de un producto
    public function get_insumos_by_producto($producto_id){
        $this->db->select("is.*, in.insumo, u.descripcion as unidad_desc");
        $this->db->from('insumos is');
        $this->db->where('is.producto_id',$producto_id);
        $this->db->join('insumo in','in.insumo_id = is.insumo_id', 'left');
        $this->db->join('multitabla u','u.valor = is.unidad and tipo_multitabla_id = 1', 'left');
        // $this->db->order_by('p.comanda', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_insumos_all(){ //Obtener todos los insumos que estan en los kits
        $this->db->select("is.*, 
                            in.insumo,
                            in.stock,
                            u.descripcion as unidad_desc
                            ");
        $this->db->from('insumos is');
        $this->db->join('insumo in','in.insumo_id = is.insumo_id', 'left');
        $this->db->join('multitabla u','u.valor = is.unidad and tipo_multitabla_id = 1', 'left');

        $query = $this->db->get();
        return $query->result();
    }
    public function actualiza_stock($producto_id, $cantidad){
        //Suma la canditdad al stock actual(Dicha suma puede ser positiva o negativa)
        $this->db->where('producto_id', $producto_id);
        $this->db->set('stock', 'stock+'.$cantidad,FALSE);
        return $this->db->update('producto');
    }
    // Obtener numero de ordenes referenciados a esta producto

}

