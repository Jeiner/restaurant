<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Familia_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $familia_id = 0;
    public $familia = "";
    public $nro_productos = 0;
    public $imagen = "";
    public $estado = "A";
    public $ubi_carta = "";

    public function insert($data){
        return $this->db->insert('familia', $data);
    }
    public function update($data){
        $this->db->where('familia_id', $data["familia_id"]);
        return $this->db->update('familia', $data);
    }
    public function delete($familia_id){
        $this->db->where('familia_id', $familia_id);
        $this->db->set('estado', 'X');
        return $this->db->update('familia');
    }
    public function get_one($familia_id){
        $this->db->select("*");
        $this->db->from('familia');
        $this->db->where('familia_id',$familia_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all($estado = ''){
        $this->db->select("*");
        $this->db->from('familia');
        $this->db->order_by('ubi_carta', "asc");

        //Filtro estado
        if($estado != ''){
            $this->db->where('estado',$estado);
        }

        //Devolver 
        $query = $this->db->get();
        return $query->result();
    }
    //Incrementar en uno cada ves que se agrega un producto de determinada familia
    public function add_product($familia_id){  
        $this->db->where('familia_id', $familia_id);
        $this->db->set('nro_productos', 'nro_productos + 1', FALSE);
        return $this->db->update('familia');
    }
}

