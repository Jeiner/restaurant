<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $ingreso_id = "0";
    public $fecha_ingreso = "";
    public $motivo = "COMP";
    public $usuario = "";
    public $nro_productos = "0";
    public $tasa_IGV = "0.18";
    public $importe_sin_IGV = "0.00";
    public $total_IGV = "0.00";
    public $importe_total = "0.00";

    public $proveedor_id = "";
    public $tipo_comprobante = "";
    public $nro_comprobante = "";
    public $estado_pago = "C"; //P:Pendiente,  C:Cancelado, X:Anulado

    public $observaciones = "";
    // public $anulado_en = "";
    // public $anulado_por = "";
    // public $anulado_motivo = "";
    public $items = array();

    public function insert($data, $items){
        if( $this->db->insert('ingreso', $data) ){
            $ingreso_id = $this->db->insert_id();
            foreach ($items as $key => $item) {
                $item['ingreso_id'] = $ingreso_id;
                $this->db->insert('ingreso_item', $item);
            }
            return $ingreso_id;
        }else{
            return false;
        }
    }
    public function get_all_by_fechas($start_date, $end_date){ //todos los ingresos por fechas
        $this->db->select("
                            i.*,
                            prov.razon_social as 'proveedor',
                            mmc.descripcion as 'motivo_desc',
                            mtc.descripcion as 'tipo_comprobante_desc'
                        ");
        $this->db->from('ingreso i');
        $this->db->join('multitabla mmc', 'mmc.valor = i.motivo and mmc.tipo_multitabla_id = 6');  //Motivo de compra
        $this->db->join('multitabla mtc', 'mtc.valor = i.tipo_comprobante and mtc.tipo_multitabla_id = 7','left');  //tipo comprobante
        $this->db->join('proveedor prov', 'prov.proveedor_id = i.proveedor_id','left');  //Motivo de compra
        $this->db->where('date(i.fecha_ingreso) >=',$start_date);
        $this->db->where('date(i.fecha_ingreso) <=',$end_date);
        $this->db->order_by('i.fecha_ingreso','desc'); 
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_items_by_fechas($start_date, $end_date){
        $this->db->select("
                            *
                        ");
        $this->db->from('ingreso_item');
        $this->db->where('date(fecha_registro) >=',$start_date);
        $this->db->where('date(fecha_registro) <=',$end_date);
        $this->db->order_by('fecha_registro','desc'); 
        $query = $this->db->get();
        return $query->result();
    }
    public function get_one($ingreso_id){   //Obtener un ingreso
        $this->db->select("
                            i.*,
                            prov.razon_social as 'proveedor',
                            mmc.descripcion as 'motivo_desc',
                            mtc.descripcion as 'tipo_comprobante_desc'
                        ");
        $this->db->from('ingreso i');        
        $this->db->where('i.ingreso_id',$ingreso_id);
        $this->db->join('multitabla mmc', 'mmc.valor = i.motivo and mmc.tipo_multitabla_id = 6');  //Motivo de compra
        $this->db->join('multitabla mtc', 'mtc.valor = i.tipo_comprobante and mtc.tipo_multitabla_id = 7','left');  //tipo comprobante
        $this->db->join('proveedor prov', 'prov.proveedor_id = i.proveedor_id','left');  //Motivo de compra
        
        $query = $this->db->get();
        return $query->result();
    }
    public function get_items_by_ingreso($ingreso_id){ 
        $this->db->select(" * ");
        $this->db->from('ingreso_item');
        $this->db->where('ingreso_id',$ingreso_id);
        $query = $this->db->get();
        return $query->result();
    }

    // left join multitabla m on m.valor = p.unidad and m.tipo_multitabla_id = 1





    // listadio de productos e  insumos   PARA EL REGISTRO DE UN INGRESO
    function get_view_productos_insumos(){
        $this->db->select(" *");
        $this->db->from('view_productos_insumos');        
        $query = $this->db->get();
        return $query->result();
    }
    function get_one_productos_insumos($insumo_id = null, $producto_id = null){
        $this->db->select(" *");
        $this->db->from('view_productos_insumos');
        if ($insumo_id != null) $this->db->where('insumo_id',$insumo_id);
        if ($producto_id != null) $this->db->where('producto_id',$producto_id);
        $query = $this->db->get();
        return $query->result();
    }
}

