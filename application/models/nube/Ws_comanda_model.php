<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_comanda_model extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
    }
    public function enviar_comanda($datos){

        $oGeneralidades = $datos['oGeneralidades'];
        $oComanda = $datos['oComanda'];
        $oCaja = $datos['oCaja'];

        //Obtener URL de la nube
        $buscarUrlNubeComanda = $this->Validaciones_model->get_by_nombre_variable(NOMBRE_VAR_URL_NUBE_COMANNDA);
        if(count($buscarUrlNubeComanda) <= 0){
            $response['success'] = FALSE;
            $response['message'] = "URL de nube no válido";
            return $response;
        }
        $url_webservice = $buscarUrlNubeComanda[0]->valor;

        //Verifica estado del servidor
        if (!$this->verificar_servidor($url_webservice)) {
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta de la NUBE '".$url_webservice."'";
            return $response;
        }

        //Obtener webservice
        $array_comanda = $this->armar_array_comanda($oComanda, $oGeneralidades, $oCaja);

        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $array_comanda);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        //echo json_encode($rptaWs);exit();

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar la Comanda: ".$oComanda->comanda_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío la Comanda a la nube: ".$rptaWs->message;
            return $response;
        }
        $nCbeId = $rptaWs->id;
        $response['success'] = true;
        $response['message'] = "Comanda enviado correctamente";
        return $response;
    }

    public function armar_array_comanda($oComanda, $oGeneralidades, $oCaja){
        //obtener datos de empres
        $empresa_codigo = $oGeneralidades->empresa_codigo;
        $sucursal_codigo = $oGeneralidades->sucursal_codigo;
        $comanda_codigo = $empresa_codigo."-".$sucursal_codigo."-".$oComanda->comanda_id;

        //Crear objeto 
        foreach ($oComanda->items as $key => $oItem) {
            $oItem->comanda_codigo = $comanda_codigo;
        }

        //Crear objeto comanda
        $array_comanda = array(
            'empresa_codigo' => $empresa_codigo,
            'sucursal_codigo' => $sucursal_codigo,
            'comanda_codigo' => $comanda_codigo,

            'comanda_id' => $oComanda->comanda_id,
            'modalidad' => $oComanda->modalidad,
            'fecha_comanda' => $oComanda->fecha_comanda,
            'mozo' => $oComanda->mozo,
            'mesa_id' => $oComanda->mesa_id,
            'mesa' => $oComanda->mesa,
            'nro_comensales' => $oComanda->nro_comensales,
            'prioridad' => $oComanda->prioridad,
            'nro_productos' => $oComanda->nro_productos,
            'pago_id' => $oComanda->pago_id,
            'tasa_IGV' => $oComanda->tasa_IGV,
            'total_IGV' => $oComanda->total_IGV,
            'importe_sin_IGV' => $oComanda->importe_sin_IGV,
            'importe_total' => $oComanda->importe_total,
            'descuento' => $oComanda->descuento,
            'total_pagado' => $oComanda->total_pagado,
            'fecha_pago' => $oComanda->fecha_pago,
            'pagado_por' => $oComanda->pagado_por,
            'tipo_comprobante' => $oComanda->tipo_comprobante,
            'cliente_id' => $oComanda->cliente_id,
            'cliente_documento' => $oComanda->cliente_documento,
            'cliente_nombre' => $oComanda->cliente_nombre,
            'cliente_direccion' => $oComanda->cliente_direccion,
            'cliente_telefono' => $oComanda->cliente_telefono,
            'union_mesas' => $oComanda->union_mesas,
            'caja_id' => $oComanda->caja_id,
            'estado_atencion' => $oComanda->estado_atencion,
            'estado_pago' => $oComanda->estado_pago,
            'atendida_en' => $oComanda->atendida_en,
            'prefacturada_en' => $oComanda->prefacturada_en,
            'prefacturada_por' => $oComanda->prefacturada_por,
            'finalizada_en' => $oComanda->finalizada_en,
            'finalizada_por' => $oComanda->finalizada_por,
            'anulado_en' => $oComanda->anulado_en,
            'anulado_por' => $oComanda->anulado_por,
            'anulado_motivo' => $oComanda->anulado_motivo,
            'fecha_registro' => $oComanda->fecha_registro,
            'registrado_por' => $oComanda->registrado_por,
            'fecha_modificacion' => $oComanda->fecha_modificacion,
            'modificado_por' => $oComanda->modificado_por,
            'comentario' => $oComanda->comentario,
            'cmd_comentario' => $oComanda->cmd_comentario,
            'sync' => $oComanda->sync,
            'fecha_sync' => $oComanda->fecha_sync,
            'con_comprobante_e' => $oComanda->con_comprobante_e,
            'correlativo' => $oComanda->correlativo,
            'cliente_tipo_documento' => $oComanda->cliente_tipo_documento,
            'cliente_correo' => $oComanda->cliente_correo,
            'canal' => $oComanda->canal,
            'fecha_caja_apertura' => $oCaja->aperturado_en,

            'aItems' => json_encode($oComanda->items)
        );

        return $array_comanda;
    }

    //Verifica el estado del api cpbe, True: activo, False:Sin conexión
    public function verificar_servidor($urlServ){
        $rptaServ = @get_headers($urlServ);

        if (!is_array($rptaServ)) {
            return false;
        }

        foreach ($rptaServ as $key => $value) {
            if(strpos(strtoupper($value), 'NOT FOUND')){
                return false;
            }
        }
        return true;
    }

}

