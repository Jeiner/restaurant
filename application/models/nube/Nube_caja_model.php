<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_caja_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data){
        $rpta = $this->db->insert('nube_caja', $data);
        return $rpta;
    }

    public function delete_by_codigo_caja($caja_codigo){
        $this->db->where('caja_codigo', $caja_codigo);
        $this->db->delete('nube_caja');
    }

    public function get_caja_abierta($empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT A.empresa_codigo, A.sucursal_codigo, A.aperturado_en, 
                    A.aperturado_por, A.monto_inicial, A.cerrado_en, A.cerrado_por
                    FROM nube_caja A 
                    WHERE A.estado = 'A' 
                    AND A.empresa_codigo = '".$empresa_codigo."'
                    AND A.sucursal_codigo = '".$sucursal_codigo."'
                    ORDER BY A.aperturado_en DESC
                    LIMIT 1 ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function closed_previous($empresa_codigo, $sucursal_codigo){
        $myQuery = "UPDATE nube_caja SET estado = 'C'
                    WHERE estado = 'A'
                    AND empresa_codigo = '".$empresa_codigo."'
                    AND sucursal_codigo = '".$sucursal_codigo."'";
        $rpta = $this->db->query($myQuery, false);
        return $rpta;
    }

}

