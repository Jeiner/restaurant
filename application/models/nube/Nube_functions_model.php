<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_functions_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nube/Nube_trama_model');
    }

    public function registrar_trama($objeto, $tipo){
        $data_trama['empresa_codigo'] = isset($objeto['empresa_codigo']) ? $objeto['empresa_codigo'] : "";
        $data_trama['sucursal_codigo'] = isset($objeto['sucursal_codigo']) ? $objeto['sucursal_codigo'] : "";
        $data_trama['fecha_registro_nube'] = date("Y/m/d H:i:s");
        $data_trama['tipo_trama'] = $tipo;
        $data_trama['trama'] = json_encode($objeto);
        $this->Nube_trama_model->insert($data_trama);
    }

}

