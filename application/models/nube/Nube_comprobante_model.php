<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_comprobante_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $items){
        $rpta = $this->db->insert('nube_cpbe_cabecera', $data);
        if($rpta){
            foreach ($items as $key => $item) {
                $this->db->insert('nube_cpbe_detalle', $item);
            }
        }
        return $rpta;
    }

    public function delete_by_codigo_comprobante($comprobante_codigo){
        $this->db->where('comprobante_codigo', $comprobante_codigo);
        $this->db->delete('nube_cpbe_detalle');

        $this->db->where('comprobante_codigo', $comprobante_codigo);
        $this->db->delete('nube_cpbe_cabecera');
    }

    public function get_totales($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT count(1) cantidad, sum(sumPrecioVenta) importe_total 
                    FROM nube_cpbe_cabecera 
                    WHERE DATE(nube_cpbe_cabecera.fecEmision) >= '".$start_date."' 
                        AND DATE(nube_cpbe_cabecera.fecEmision) <= '".$end_date."'
                        AND nube_cpbe_cabecera.empresa_codigo= '".$empresa_codigo."'
                        AND nube_cpbe_cabecera.sucursal_codigo= '".$sucursal_codigo."'
                    ORDER BY nube_cpbe_cabecera.fecEmision desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_comprobantes_by_fechas($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT *
                    FROM nube_cpbe_cabecera 
                    WHERE DATE(nube_cpbe_cabecera.fecEmision) >= '".$start_date."' 
                        AND DATE(nube_cpbe_cabecera.fecEmision) <= '".$end_date."'
                        AND nube_cpbe_cabecera.empresa_codigo= '".$empresa_codigo."'
                        AND nube_cpbe_cabecera.sucursal_codigo= '".$sucursal_codigo."'
                        ORDER BY nube_cpbe_cabecera.comanda_id desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
}

