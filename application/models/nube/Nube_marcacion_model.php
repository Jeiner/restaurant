<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_marcacion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data){
        $rpta = $this->db->insert('nube_marcacion', $data);
        $rpta;
    }

    public function get_one($transaction_id){ 
        $this->db->select("*");
        $this->db->from('nube_marcacion');
        $this->db->where('transaction_id',$transaction_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function delete_by_id($transaction_id){
        $this->db->where('transaction_id', $transaction_id);
        $this->db->delete('nube_marcacion');
    }

    public function get_marcaciones_por_anio_mes($anio, $mes){
        $myQuery = "SELECT  transaction_id, emp_id, emp_code, first_name, photo, department_id, create_time,
                          punch_time, punch_state, upload_time
                        FROM nube_marcacion
                        WHERE YEAR(punch_time) = '".$anio."' AND MONTH(punch_time) = '".$mes."'
                        ORDER BY transaction_id DESC";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_marcaciones_por_anio_mes_empleado($anio, $mes, $emp_code){
        $myQuery = "SELECT  transaction_id, emp_id, emp_code, first_name, photo, department_id, create_time,
                          punch_time, punch_state, upload_time
                        FROM nube_marcacion
                        WHERE YEAR(punch_time) = '".$anio."' AND MONTH(punch_time) = '".$mes."' AND emp_code = '".$emp_code."'
                        ORDER BY transaction_id DESC";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function obtenerUltimoRegistro(){
        $myQuery = "SELECT fecha_registro FROM nube_marcacion ORDER BY fecha_registro DESC LIMIT 1";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    } 
}


