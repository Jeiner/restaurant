<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_comanda_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $items){
        $rpta = $this->db->insert('nube_comanda', $data);
        if($rpta){
            foreach ($items as $key => $item) {
                $this->db->insert('nube_comanda_item', $item);
            }
        }
        return $rpta;
    }

    public function delete_by_codigo_comanda($comanda_codigo){
        $this->db->where('comanda_codigo', $comanda_codigo);
        $this->db->delete('nube_comanda_item');

        $this->db->where('comanda_codigo', $comanda_codigo);
        $this->db->delete('nube_comanda');
    }

    public function get_comandas_by_fechas($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT *
                    FROM nube_comanda 
                    WHERE DATE(nube_comanda.fecha_caja_apertura) >= '".$start_date."' 
                        AND DATE(nube_comanda.fecha_caja_apertura) <= '".$end_date."'
                        AND nube_comanda.empresa_codigo= '".$empresa_codigo."'
                        AND nube_comanda.sucursal_codigo= '".$sucursal_codigo."'
                        ORDER BY nube_comanda.comanda_id DESC";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_totales($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT count(1) cantidad, sum(importe_total) importe_total 
                    FROM nube_comanda 
                    WHERE DATE(nube_comanda.fecha_caja_apertura) >= '".$start_date."' 
                        AND DATE(nube_comanda.fecha_caja_apertura) <= '".$end_date."'
                        AND nube_comanda.estado_atencion <> 'X'
                        AND nube_comanda.empresa_codigo= '".$empresa_codigo."'
                        AND nube_comanda.sucursal_codigo= '".$sucursal_codigo."'";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_anuladas($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT count(1) cantidad
                    FROM nube_comanda 
                    WHERE DATE(nube_comanda.fecha_caja_apertura) >= '".$start_date."' 
                        AND DATE(nube_comanda.fecha_caja_apertura) <= '".$end_date."'
                        AND nube_comanda.estado_atencion = 'X'
                    AND nube_comanda.empresa_codigo= '".$empresa_codigo."'
                    AND nube_comanda.sucursal_codigo= '".$sucursal_codigo."'";
        $q = $this->db->query($myQuery, false);
        return $q->row()->cantidad;
    }

    public function get_anuladas_detalle($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT count(1) cantidad
                    FROM nube_comanda_item
                    inner join nube_comanda on nube_comanda_item.comanda_codigo = nube_comanda.comanda_codigo
                    where DATE(nube_comanda.fecha_caja_apertura) >= '".$start_date."' 
                        AND DATE(nube_comanda.fecha_caja_apertura) <= '".$end_date."'
                        and nube_comanda_item.estado_atencion = 'X'
                    AND nube_comanda.empresa_codigo= '".$empresa_codigo."'
                    AND nube_comanda.sucursal_codigo= '".$sucursal_codigo."'";
        $q = $this->db->query($myQuery, false);
        return $q->row()->cantidad;
    }

    public function get_detalle_por_comanda($comanda_codigo){
        $myQuery = "SELECT nube_comanda_item.*
                    FROM nube_comanda_item
                    inner join nube_comanda on nube_comanda_item.comanda_codigo = nube_comanda.comanda_codigo
                    where nube_comanda_item.comanda_codigo = '".$comanda_codigo."'";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_totales_por_dia_mes_anio($dia, $mes, $anio){
        $myQuery = "SELECT DAY(A.fecha_caja_apertura) AS dia, MONTH(A.fecha_caja_apertura) AS mes,
                    YEAR(A.fecha_caja_apertura) AS anio, 
                    A.empresa_codigo, A.sucursal_codigo, B.empresa_nombre, B.sucursal_nombre, 
                    SUM(A.importe_total) AS importe
                    FROM nube_comanda A
                    INNER JOIN nube_sucursal B ON A.empresa_codigo = B.empresa_codigo AND A.sucursal_codigo = B.sucursal_codigo
                    WHERE YEAR(A.fecha_caja_apertura) = '".$anio."' 
                    AND MONTH(A.fecha_caja_apertura) = '".$mes."' 
                    AND DAY(A.fecha_caja_apertura) = '".$dia."' 
                    AND A.estado_atencion <> 'X'
                    GROUP BY A.empresa_codigo, A.sucursal_codigo, B.empresa_nombre, B.sucursal_nombre
                    ORDER BY A.empresa_codigo, A.sucursal_codigo";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_totales_por_dia_mes_anio_agrupa_modalidad($dia, $mes, $anio){
        $myQuery = "SELECT A.modalidad, DAY(A.fecha_caja_apertura) AS dia, MONTH(A.fecha_caja_apertura) AS mes,
                    YEAR(A.fecha_caja_apertura) AS anio,
                    COUNT(1) AS cantidad,
                    SUM(A.importe_total) AS importe
                    FROM nube_comanda A
                    INNER JOIN nube_sucursal B ON A.empresa_codigo = B.empresa_codigo AND A.sucursal_codigo = B.sucursal_codigo
                    WHERE YEAR(A.fecha_caja_apertura) = '".$anio."' 
                    AND MONTH(A.fecha_caja_apertura) = '".$mes."' 
                    AND DAY(A.fecha_caja_apertura) = '".$dia."' 
                    AND A.estado_atencion <> 'X'
                    GROUP BY A.modalidad";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

}


