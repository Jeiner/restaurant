<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_reportes_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function ventas_diarias($data){
        $this->db->select("
            day(fecha_caja_apertura) as 'dia',
            month(fecha_caja_apertura) as 'mes',
            year(fecha_caja_apertura) as 'anio',
            sum(importe_total) as 'importe',
            sum(nro_productos) as 'productos'
        ");
        $this->db->from('nube_comanda c');
        $this->db->where('c.estado_atencion <>', "X");
        $this->db->where('year(fecha_caja_apertura)', $data['anio']);
        $this->db->where('month(fecha_caja_apertura)', $data['mes']);
        $this->db->group_by('day(fecha_caja_apertura), month(fecha_caja_apertura), year(fecha_caja_apertura) ');
        $query = $this->db->get();
        return $query->result();
    }
}


