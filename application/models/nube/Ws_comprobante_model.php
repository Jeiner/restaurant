<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_comprobante_model extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
    }
    public function enviar_comprobante($datos){

        $oGeneralidades = $datos['oGeneralidades'];
        $oComprobante = $datos['oComprobante'];

        //Obtener URL de la nube
        $buscarUrlNubeComprobante = $this->Validaciones_model->get_by_nombre_variable(NOMBRE_VAR_URL_NUBE_COMPROBANTE);
        if(count($buscarUrlNubeComprobante) <= 0){
            $response['success'] = FALSE;
            $response['message'] = "URL de nube no válido";
            return $response;
        }
        $url_webservice = $buscarUrlNubeComprobante[0]->valor;

        //Verifica estado del servidor
        if (!$this->verificar_servidor($url_webservice)) {
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta de la NUBE '".$url_webservice."'";
            return $response;
        }


        //Obtener webservice
        $array_comprobante = $this->armar_array_comprobante($oComprobante, $oGeneralidades);

        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $array_comprobante);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        //echo json_encode($rptaWs);exit();

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar la Comanda: ".$oComprobante->comanda_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío la Comanda a la nube: ".$rptaWs->message;
            return $response;
        }
        $nCbeId = $rptaWs->id;
        $response['success'] = true;
        $response['message'] = "Comanda enviado correctamente";
        return $response;
    }

    public function armar_array_comprobante($oComprobante, $oGeneralidades){
        //obtener datos de empres
        $empresa_codigo = $oGeneralidades->empresa_codigo;
        $sucursal_codigo = $oGeneralidades->sucursal_codigo;
        $comanda_codigo = $empresa_codigo."-".$sucursal_codigo."-".$oComprobante->comanda_id;
        $comprobante_codigo = $empresa_codigo."-".$sucursal_codigo."-".$oComprobante->serie."-".$oComprobante->correlativo;

        //Crear objeto 
        foreach ($oComprobante->items as $key => $oItem) {
            $oItem->comanda_codigo = $comanda_codigo;
            $oItem->comprobante_codigo = $comprobante_codigo;
        }

        //Crear objeto comanda
        $array_comprobante = array(
            'empresa_codigo' => $empresa_codigo,
            'sucursal_codigo' => $sucursal_codigo,
            'comanda_codigo' => $comanda_codigo,
            'comprobante_codigo' => $comprobante_codigo,

            'comanda_id' => $oComprobante->comanda_id,
            'pago_id' => $oComprobante->pago_id,
            'cabecera_id' => $oComprobante->cabecera_id,
            'tipoComprobante' => $oComprobante->tipoComprobante,
            'serie' => $oComprobante->serie,
            'correlativo' => $oComprobante->correlativo,
            'tipoOperacion' => $oComprobante->tipoOperacion,
            'fecEmision' => $oComprobante->fecEmision,
            'horEmision' => $oComprobante->horEmision,
            'fecVencimiento' => $oComprobante->fecVencimiento,
            'codLocalEmisor' => $oComprobante->codLocalEmisor,
            'tipDocUsuario' => $oComprobante->tipDocUsuario,
            'numDocUsuario' => $oComprobante->numDocUsuario,
            'rznSocialUsuario' => $oComprobante->rznSocialUsuario,
            'tipMoneda' => $oComprobante->tipMoneda,
            'sumTotTributos' => $oComprobante->sumTotTributos,
            'sumTotValVenta' => $oComprobante->sumTotValVenta,
            'sumPrecioVenta' => $oComprobante->sumPrecioVenta,
            'sumDescTotal' => $oComprobante->sumDescTotal,
            'sumOtrosCargos' => $oComprobante->sumOtrosCargos,
            'sumTotalAnticipos' => $oComprobante->sumTotalAnticipos,
            'sumImpVenta' => $oComprobante->sumImpVenta,
            'ublVersionId' => $oComprobante->ublVersionId,
            'customizationId' => $oComprobante->customizationId,
            'codHash' => $oComprobante->codHash,
            'estadoSunat' => $oComprobante->estadoSunat,

            'aItems' => json_encode($oComprobante->items)
        );

        return $array_comprobante;
    }

    //Verifica el estado del api cpbe, True: activo, False:Sin conexión
    public function verificar_servidor($urlServ){
        $rptaServ = @get_headers($urlServ);

        if (!is_array($rptaServ)) {
            return false;
        }

        foreach ($rptaServ as $key => $value) {
            if(strpos(strtoupper($value), 'NOT FOUND')){
                return false;
            }
        }
        return true;
    }

}

