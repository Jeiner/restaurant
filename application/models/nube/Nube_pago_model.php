<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_pago_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data){
        $rpta = $this->db->insert('nube_pago', $data);
        return $rpta;
    }

    public function delete_by_codigo_pago($pago_codigo){
        $this->db->where('pago_codigo', $pago_codigo);
        $this->db->delete('nube_pago');
    }

    public function get_totales($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT count(1) cantidad, sum(total_a_pagar) importe_total 
                    FROM nube_pago 
                    WHERE DATE(nube_pago.fecha_caja_apertura) >= '".$start_date."' 
                        AND DATE(nube_pago.fecha_caja_apertura) <= '".$end_date."'
                        AND nube_pago.estado <> 'X'
                        AND nube_pago.empresa_codigo= '".$empresa_codigo."'
                        AND nube_pago.sucursal_codigo= '".$sucursal_codigo."'
                    ORDER BY nube_pago.fecha_pago desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_pagos_by_fechas($start_date, $end_date, $empresa_codigo, $sucursal_codigo){
        $myQuery = "SELECT *
                    FROM nube_pago 
                    WHERE DATE(nube_pago.fecha_caja_apertura) >= '".$start_date."' 
                        AND DATE(nube_pago.fecha_caja_apertura) <= '".$end_date."'
                        AND nube_pago.empresa_codigo= '".$empresa_codigo."'
                        AND nube_pago.sucursal_codigo= '".$sucursal_codigo."'
                        ORDER BY nube_pago.comanda_id desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

}

