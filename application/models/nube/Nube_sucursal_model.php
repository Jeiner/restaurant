<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_sucursal_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all(){
        $this->db->select("*");
        $this->db->from('nube_sucursal');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all_activos(){
        $this->db->select("*");
        $this->db->from('nube_sucursal');
        $this->db->where('estado', "A");
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data){
        $rpta = $this->db->insert('nube_sucursal', $data);
        return $rpta;
    }

    public function update($data){
        $this->db->where('empresa_codigo', $data["empresa_codigo"]);
        $this->db->where('sucursal_codigo', $data["sucursal_codigo"]);
        return $this->db->update('nube_sucursal', $data);
    }

    public function get_by_codigo($sucursal_codigo){
        $this->db->select("*");
        $this->db->from('nube_sucursal');
        $this->db->where('sucursal_codigo', $sucursal_codigo);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_one_or_null_by_codigo($sucursal_codigo){
        $this->db->select("*");
        $this->db->from('nube_sucursal');
        $this->db->where('sucursal_codigo', $sucursal_codigo);
        return $this->db->get()->row();
    }

    public function get_one_by_nombre($sucursal_nombre){
        $this->db->select("*");
        $this->db->from('nube_sucursal');
        $this->db->where('sucursal_nombre', $sucursal_nombre);
        $query = $this->db->get();
        return $query->result();
    }

    // public function delete_by_codigo_pago($pago_codigo){
    //     $this->db->where('pago_codigo', $pago_codigo);
    //     $this->db->delete('nube_pago');
    // }

}

