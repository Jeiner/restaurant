<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_caja_model extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
    }
    public function enviar_caja($datos){

        $oGeneralidades = $datos['oGeneralidades'];
        $oCaja = $datos['oCaja'];

        //Obtener URL de la nube
        $buscarUrlNubeCaja = $this->Validaciones_model->get_by_nombre_variable(NOMBRE_VAR_URL_NUBE_CAJA);
        if(count($buscarUrlNubeCaja) <= 0){
            $response['success'] = FALSE;
            $response['message'] = "URL de nube no válido.";
            return $response;
        }
        $url_webservice = $buscarUrlNubeCaja[0]->valor;

        //Verifica estado del servidor
        if (!$this->verificar_servidor($url_webservice)) {
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta de la NUBE '".$url_webservice."'";
            return $response;
        }

        //Obtener webservice
        $array_caja = $this->armar_array_caja($oGeneralidades, $oCaja);

        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $array_caja);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);
        
        //echo json_encode($rptaWs);exit();

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar Caja: ".$oCaja->caja_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío Caja a la nube: ".$rptaWs->message;
            return $response;
        }
        $nCbeId = $rptaWs->id;
        $response['success'] = true;
        $response['message'] = "Caja enviada correctamente.";
        return $response;
    }

    public function armar_array_caja($oGeneralidades, $oCaja){
        //obtener datos de empres
        $empresa_codigo = $oGeneralidades->empresa_codigo;
        $sucursal_codigo = $oGeneralidades->sucursal_codigo;
        $caja_codigo = $empresa_codigo."-".$sucursal_codigo."-".$oCaja->caja_id;

        //Crear objeto Caja
        $array_caja = array(
            'empresa_codigo' => $empresa_codigo,
            'sucursal_codigo' => $sucursal_codigo,
            'caja_codigo' => $caja_codigo,

            'caja_id'     => $oCaja->caja_id,
            'aperturado_en'     => $oCaja->aperturado_en,
            'aperturado_por'    => $oCaja->aperturado_por,
            'monto_inicial'     => $oCaja->monto_inicial,
            'cerrado_en'        => $oCaja->cerrado_en,
            'cerrado_por'       => $oCaja->cerrado_por,
            'total_ventas_efectivo'  => $oCaja->total_ventas_efectivo,
            'total_gastos_efectivo'  => $oCaja->total_gastos_efectivo,
            'saldo_caja'            => $oCaja->saldo_caja,
            'total_ventas_tarjeta'  => $oCaja->total_ventas_tarjeta,
            'total_gastos_tarjeta'  => $oCaja->total_gastos_tarjeta,
            'monto_retirado'    => $oCaja->monto_retirado,
            'remanente'         => $oCaja->remanente,
            'observaciones'     => $oCaja->observaciones,
            'estado'            => $oCaja->estado,
            'fecha_registro'    => $oCaja->fecha_registro,
            'registrado_por'    => $oCaja->registrado_por,
            'fecha_modificacion' => $oCaja->fecha_modificacion,
            'modificado_por'        => $oCaja->modificado_por,
            'fecha_sync_marcaciones' => $oCaja->fecha_sync_marcaciones,
            'fecha_sync_comandas' => $oCaja->fecha_sync_comandas,
            'fecha_procesar_cpbe' => $oCaja->fecha_procesar_cpbe,
        );

        return $array_caja;
    }

    //Verifica el estado del api cpbe, True: activo, False:Sin conexión
    public function verificar_servidor($urlServ){
        $rptaServ = @get_headers($urlServ);

        if (!is_array($rptaServ)) {
            return false;
        }

        foreach ($rptaServ as $key => $value) {
            if(strpos(strtoupper($value), 'NOT FOUND')){
                return false;
            }
        }
        return true;
    }

}

