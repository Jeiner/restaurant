<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_marcacion_empleado_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data){
        $rpta = $this->db->insert('nube_marcacion_empleado', $data);
        $rpta;
    }

    public function get_empleados(){ 
        $this->db->select("emp_id, emp_code, first_name, hora_entrada, hora_salida, estado, fecha_ingreso");
        $this->db->from('nube_marcacion_empleado');
        $this->db->where('estado', 1);
        $this->db->order_by('emp_code');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_empleado_por_code($emp_code){ 
        $this->db->select("emp_id, emp_code, first_name, hora_entrada, hora_salida, estado, fecha_ingreso");
        $this->db->from('nube_marcacion_empleado');
        // $this->db->where('estado', 1);
        $this->db->where('emp_code', $emp_code);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all(){ 
        $this->db->select("emp_id, emp_code, first_name, hora_entrada, hora_salida, estado");
        $this->db->from('nube_marcacion_empleado');
        $this->db->where('estado', 1);
        $query = $this->db->get();
        return $query->result();
    }

}


