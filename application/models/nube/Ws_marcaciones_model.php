<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_marcaciones_model extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
    }

    public function enviar_marcaciones($datos){

        $lstMarcaciones = $datos['lstMarcaciones'];

        //Obtener URL de la nube
        $buscarUrlNubeComanda = $this->Validaciones_model->get_by_nombre_variable(NOMBRE_VAR_URL_NUBE_MARCACIONES);
        if(count($buscarUrlNubeComanda) <= 0){
            $response['success'] = FALSE;
            $response['message'] = "URL de nube no válido: ".NOMBRE_VAR_URL_NUBE_MARCACIONES.".";
            return $response;
        }
        $url_webservice = $buscarUrlNubeComanda[0]->valor;

        //Verifica estado del servidor
        if (!$this->verificar_servidor($url_webservice)) {
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta de la NUBE '".$url_webservice."'";
            return $response;
        }

        //Obtener webservice
        $lstMarcaciones = $this->armar_array_marcaciones($lstMarcaciones);

        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $lstMarcaciones);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        // echo json_encode($rptaWs);exit();

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar las marcaciones.";
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío las marcaciones".$rptaWs->message;
            return $response;
        }
        $nCbeId = $rptaWs->id;
        $response['success'] = true;
        $response['message'] = "Marcaciones enviadas correctamente";
        return $response;
    }

    public function armar_array_marcaciones($lstMarcaciones){
        //obtener datos de empres

        //Crear objeto comanda
        $array_marcacion = array(
            'lstMarcaciones' => json_encode($lstMarcaciones)
        );

        return $array_marcacion;
    }

    //Verifica el estado del api cpbe, True: activo, False:Sin conexión
    public function verificar_servidor($urlServ){
        $rptaServ = @get_headers($urlServ);

        if (!is_array($rptaServ)) {
            return false;
        }

        foreach ($rptaServ as $key => $value) {
            if(strpos(strtoupper($value), 'NOT FOUND')){
                return false;
            }
        }
        return true;
    }

}

