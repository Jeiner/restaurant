<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nube_trama_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data){
        $rpta = $this->db->insert('nube_trama', $data);
        return $rpta;
    }
}