<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_pago_model extends CI_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
    }
    public function enviar_pago($datos){

        $oGeneralidades = $datos['oGeneralidades'];
        $oPago = $datos['oPago'];
        $oCaja = $datos['oCaja'];

        //Obtener URL de la nube
        $buscarUrlNubePago = $this->Validaciones_model->get_by_nombre_variable(NOMBRE_VAR_URL_NUBE_PAGO);
        if(count($buscarUrlNubePago) <= 0){
            $response['success'] = FALSE;
            $response['message'] = "URL de nube no válido";
            return $response;
        }
        $url_webservice = $buscarUrlNubePago[0]->valor;

        //Verifica estado del servidor
        if (!$this->verificar_servidor($url_webservice)) {
            $response['success'] = FALSE;
            $response['message'] = "No hay respuesta de la NUBE '".$url_webservice."'";
            return $response;
        }


        //Obtener webservice
        $array_pago = $this->armar_array_pago($oPago, $oGeneralidades, $oCaja);

        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url_webservice);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $array_pago);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $rptaWs = json_decode($buffer);

        //echo json_encode($rptaWs);exit();

        if(is_null($rptaWs)){
            $response['success'] = FALSE;
            $response['message'] = "Problemas al enviar el Pago: ".$oPago->comanda_id;
            return $response;
        }
        if(!$rptaWs->success){
            $response['success'] = FALSE;
            $response['message'] = "No se envío el Pago a la nube: ".$rptaWs->message;
            return $response;
        }
        $nCbeId = $rptaWs->id;
        $response['success'] = true;
        $response['message'] = "Pago enviado correctamente";
        return $response;
    }

    public function armar_array_pago($oPago, $oGeneralidades, $oCaja){
        //obtener datos de empres
        $empresa_codigo = $oGeneralidades->empresa_codigo;
        $sucursal_codigo = $oGeneralidades->sucursal_codigo;
        $comanda_codigo = $empresa_codigo."-".$sucursal_codigo."-".$oPago->comanda_id;
        $pago_codigo = $empresa_codigo."-".$sucursal_codigo."-".$oPago->pago_id;

        //Crear objeto pago
        $array_pago = array(
            'empresa_codigo' => $empresa_codigo,
            'sucursal_codigo' => $sucursal_codigo,
            'comanda_codigo' => $comanda_codigo,
            'pago_codigo' => $pago_codigo,

            'pago_id' => $oPago->pago_id,
            'comanda_id' => $oPago->comanda_id,
            'importe_total' => $oPago->importe_total,
            'descuento' => $oPago->descuento,
            'total_a_pagar' => $oPago->total_a_pagar,
            'pago_efectivo' => $oPago->pago_efectivo,
            'pago_tarjeta' => $oPago->pago_tarjeta,
            'total_pagado' => $oPago->total_pagado,
            'saldo' => $oPago->saldo,
            'efectivo' => $oPago->efectivo,
            'vuelto' => $oPago->vuelto,
            'fecha_pago' => $oPago->fecha_pago,
            'cajero' => $oPago->cajero,
            'tipo_comprobante' => $oPago->tipo_comprobante,
            'serie' => $oPago->serie,
            'correlativo' => $oPago->correlativo,
            'cliente_id' => $oPago->cliente_id,
            'cliente_documento' => $oPago->cliente_documento,
            'cliente_nombre' => $oPago->cliente_nombre,
            'cliente_direccion' => $oPago->cliente_direccion,
            'cliente_telefono' => $oPago->cliente_telefono,
            'caja_id' => $oPago->caja_id,
            'observaciones' => $oPago->observaciones,
            'estado' => $oPago->estado,
            'anulado_en' => $oPago->anulado_en,
            'anulado_por' => $oPago->anulado_por,
            'fecha_registro' => $oPago->fecha_registro,
            'registrado_por' => $oPago->registrado_por,
            'fecha_modificacion' => $oPago->fecha_modificacion,
            'modificado_por' => $oPago->modificado_por,
            'modificado_por' => $oPago->modificado_por,
            'fecha_caja_apertura' => $oCaja->aperturado_en,
        );

        return $array_pago;
    }

    //Verifica el estado del api cpbe, True: activo, False:Sin conexión
    public function verificar_servidor($urlServ){
        $rptaServ = @get_headers($urlServ);

        if (!is_array($rptaServ)) {
            return false;
        }

        foreach ($rptaServ as $key => $value) {
            if(strpos(strtoupper($value), 'NOT FOUND')){
                return false;
            }
        }
        return true;
    }

}

