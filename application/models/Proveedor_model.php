<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $proveedor_id = "0"; 
    public $RUC = ""; 
    public $razon_social = ""; 
    public $direccion = ""; 
    public $contacto = ""; 
    public $telefono = ""; 
    public $correo = ""; 
    public $productos = ""; 
    public $estado = "A"; //'A:activo, X:Inactivo'

    public function insert($data){
        return $this->db->insert('proveedor', $data);
    }
    public function update($data){
        $this->db->where('proveedor_id', $data["proveedor_id"]);
        return $this->db->update('proveedor', $data);
    }
    public function delete($proveedor_id){
        $this->db->where('proveedor_id',$proveedor_id);
        return $this->db->delete('proveedor');
    }
    public function get_one($proveedor_id){
        $this->db->select("*");
        $this->db->from('proveedor');
        $this->db->where('proveedor_id',$proveedor_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("*");
        $this->db->from('proveedor');
        $this->db->order_by('razon_social', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    // Ver productos referenciados a este proveedor 
}

