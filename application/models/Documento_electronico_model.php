<?php defined('BASEPATH') OR exit('No direct script access allowed');
//Documentos electronicos enviados asunat
class Documento_electronico_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //insertar cabecera del comprobante electronico
    public function insert_cpbe_cab($data){
        $this->db->insert('cpbe_cabecera', $data);
        return  $this->db->insert_id();
    }
    //insertar detalle del comprobante electronico
    public function insert_cpbe_det($data){
        $this->db->insert('cpbe_detalle', $data);
        return  $this->db->insert_id();
    }
    public function get_cpbe_cab($cabecera_id){
        $this->db->select(" CAB.*,
                            TDS.descripcion as sunat_tipo_doc,
                            TO.descripcion as sunat_tipo_operacion,
                            TDI.valor_constante as sunat_tipo_doc_i,
                            EST.descripcion as sunat_estado,
                            TDE.descripcion as sunat_tipo_doc_e
                        ");
        $this->db->from('cpbe_cabecera CAB');
        $this->db->join('multitabla TDS','CAB.tipoComprobante = TDS.valor and TDS.tipo_multitabla_id = 17'); //Tip documento sunat.
        $this->db->join('multitabla TO','CAB.tipoOperacion = TO.valor and TO.tipo_multitabla_id = 32'); //Tip operacion
        $this->db->join('multitabla TDI','CAB.tipDocUsuario = TDI.valor and TDI.tipo_multitabla_id = 22'); //Tip documento identidad
        $this->db->join('multitabla EST','CAB.estadoSunat = EST.valor and EST.tipo_multitabla_id = 37', 'left'); //Estado
        $this->db->join('multitabla TDE','CAB.tipoComprobante = TDE.valor and TDE.tipo_multitabla_id = 36'); //Tip documento elect.
        
        $this->db->where('cabecera_id',$cabecera_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_cpbe_det_by_cab($cabecera_id){
        $this->db->select("*");
        $this->db->from('cpbe_detalle');
        $this->db->where('cabecera_id =',$cabecera_id);
        $query = $this->db->get();
        return $query->result();
    }
    //Obtenemos todoslos items declarados para una comanda
    public function get_cpbe_DET_by_comanda($comanda_id){
        $this->db->select("*");
        $this->db->from('cpbe_detalle');
        $this->db->where('comanda_id =',$comanda_id);
        $this->db->order_by('comanda_item_id');
        $query = $this->db->get();
        return $query->result();
    }
    //Obtener los datos de la cabecera de los documentos electronicos
    public function get_cpbe_CAB_by_fechas($start_date, $end_date){
        $this->db->select(" CAB.*,
                            TDS.descripcion as sunat_tipo_doc,
                            TO.descripcion as sunat_tipo_operacion,
                            TDI.valor_constante as sunat_tipo_doc_i,
                            EST.descripcion as sunat_estado,
                            TDE.descripcion as sunat_tipo_doc_e
                        ");
        $this->db->from('cpbe_cabecera CAB');
        $this->db->join('multitabla TDS','CAB.tipoComprobante = TDS.valor and TDS.tipo_multitabla_id = 17'); //Tip documento sunat.
        $this->db->join('multitabla TO','CAB.tipoOperacion = TO.valor and TO.tipo_multitabla_id = 32'); //Tip operacion
        $this->db->join('multitabla TDI','CAB.tipDocUsuario = TDI.valor and TDI.tipo_multitabla_id = 22'); //Tip documento identidad
        $this->db->join('multitabla EST','CAB.estadoSunat = EST.valor and EST.tipo_multitabla_id = 37', 'left'); //Estado
        $this->db->join('multitabla TDE','CAB.tipoComprobante = TDE.valor and TDE.tipo_multitabla_id = 36'); //Tip documento elect.

        $this->db->where('date(CAB.fecEmision) >=',$start_date);
        $this->db->where('date(CAB.fecEmision) <=',$end_date);
        $this->db->order_by('CAB.serie', "asc");
        $this->db->order_by('CAB.correlativo', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    //Obtener los datos de la cabecera de los documentos electronicos para una fecha determinada
    public function get_boletas_CAB_by_fecha($date){
        $this->db->select(" CAB.*,
                            TDS.descripcion as sunat_tipo_doc,
                            TO.descripcion as sunat_tipo_operacion,
                            TDI.valor_constante as sunat_tipo_doc_i,
                            EST.descripcion as sunat_estado,
                            TDE.descripcion as sunat_tipo_doc_e
                        ");
        $this->db->from('cpbe_cabecera CAB');
        $this->db->join('multitabla TDS','CAB.tipoComprobante = TDS.valor and TDS.tipo_multitabla_id = 17'); //Tip documento sunat.
        $this->db->join('multitabla TO','CAB.tipoOperacion = TO.valor and TO.tipo_multitabla_id = 32'); //Tip operacion
        $this->db->join('multitabla TDI','CAB.tipDocUsuario = TDI.valor and TDI.tipo_multitabla_id = 22'); //Tip documento identidad
        $this->db->join('multitabla EST','CAB.estadoSunat = EST.valor and EST.tipo_multitabla_id = 37', 'left'); //Estado
        $this->db->join('multitabla TDE','CAB.tipoComprobante = TDE.valor and TDE.tipo_multitabla_id = 36'); //Tip documento elect.

        $this->db->where('date(CAB.fecEmision) =',$date); //Fecha
        $this->db->where('CAB.tipoComprobante =','03'); //Boletas
        $this->db->order_by('CAB.serie', "asc");
        $this->db->order_by('CAB.correlativo', "asc");
        $query = $this->db->get();
        return $query->result();
    }

    //Se considera a las boletas del día anterior
    public function get_boletas_CAB_para_procesar(){
        $myQuery = "SELECT cabecera_id, serie, correlativo, fecEmision, sumImpVenta
                    FROM cpbe_cabecera 
                    WHERE cpbe_cabecera.fecEmision < (SELECT CAST(aperturado_en AS DATE) fecha FROM caja WHERE estado = 'A' LIMIT 1)
                    AND (cpbe_cabecera.estadoSunat IS NULL OR RTRIM(cpbe_cabecera.estadoSunat) = '')
                    ORDER BY cabecera_id
                    ";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    //Obtener los datos de las cabeceras de una comanda
    public function get_cpbe_CAB_by_comanda($comanda_id){
        $this->db->select(" CAB.*,
                            TDS.descripcion as sunat_tipo_doc,
                            TO.descripcion as sunat_tipo_operacion,
                            TDI.valor_constante as sunat_tipo_doc_i,
                            EST.descripcion as sunat_estado,
                            TDE.descripcion as sunat_tipo_doc_e
                        ");
        $this->db->from('cpbe_cabecera CAB');
        $this->db->join('multitabla TDS','CAB.tipoComprobante = TDS.valor and TDS.tipo_multitabla_id = 17'); //Tip documento sunat.
        $this->db->join('multitabla TO','CAB.tipoOperacion = TO.valor and TO.tipo_multitabla_id = 32'); //Tip operacion
        $this->db->join('multitabla TDI','CAB.tipDocUsuario = TDI.valor and TDI.tipo_multitabla_id = 22'); //Tip documento identidad
        $this->db->join('multitabla EST','CAB.estadoSunat = EST.valor and EST.tipo_multitabla_id = 37', 'left'); //Estado
        $this->db->join('multitabla TDE','CAB.tipoComprobante = TDE.valor and TDE.tipo_multitabla_id = 36'); //Tip documento elect.

        $this->db->where('CAB.comanda_id =',$comanda_id);
        $query = $this->db->get();
        return $query->result();
    }
    //Obtener los datos de la cabecera de UN SOLO documento electronico por su serie y correlativo
    public function get_cpbe_CAB_by_num_docu($serie, $correlativo){
        $this->db->select(" CAB.*,
                            TDS.descripcion as sunat_tipo_doc,
                            TO.descripcion as sunat_tipo_operacion,
                            TDI.valor_constante as sunat_tipo_doc_i,
                            EST.descripcion as sunat_estado,
                            TDE.descripcion as sunat_tipo_doc_e
                        ");
        $this->db->from('cpbe_cabecera CAB');
        $this->db->join('multitabla TDS','CAB.tipoComprobante = TDS.valor and TDS.tipo_multitabla_id = 17'); //Tip documento sunat.
        $this->db->join('multitabla TO','CAB.tipoOperacion = TO.valor and TO.tipo_multitabla_id = 32'); //Tip operacion
        $this->db->join('multitabla TDI','CAB.tipDocUsuario = TDI.valor and TDI.tipo_multitabla_id = 22'); //Tip documento identidad
        $this->db->join('multitabla EST','CAB.estadoSunat = EST.valor and EST.tipo_multitabla_id = 37', 'left'); //Estado
        $this->db->join('multitabla TDE','CAB.tipoComprobante = TDE.valor and TDE.tipo_multitabla_id = 36'); //Tip documento elect.

        $this->db->where('CAB.serie =',$serie);
        $this->db->where('CAB.correlativo =',$correlativo);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_cpbe_pendientes(){
        $this->db->select("*");
        $this->db->from('cpbe_cabecera CAB');
        $this->db->where('estadoSunat','');
        $this->db->or_where('estadoSunat',null);
        $this->db->order_by('cabecera_id','desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function get_cpbe_errores(){
        $this->db->select("*");
        $this->db->from('cpbe_cabecera CAB');
        
        $this->db->where('estadoSunat','05');
        $this->db->or_where('estadoSunat','06');
        $this->db->or_where('estadoSunat','10');
        $query = $this->db->get();
        return $query->result();
    }
    //actualiza un comprobante electronico enviado correctamente
    public function update_cpbe_cab($cabecera_id, $data){
        $this->db->where('cabecera_id', $cabecera_id);
        return $this->db->update('cpbe_cabecera', $data);
        // $this->db->set('estadoSunat', $estado);
        // return $this->db->update('cpbe_cabecera');
    }

    public function update_cpbe_cab_actualiza_estado($cabecera_id, $estadoSunat){
        $this->db->where('cabecera_id', $cabecera_id);
        $this->db->set('estadoSunat', $estadoSunat);
        return $this->db->update('cpbe_cabecera');
    }

    //Se convierte en negativo cuando anulamos el comprobante.
    public function update_cpbe_cab_negativo($cabecera_id){
        $this->db->where('cabecera_id', $cabecera_id);
        $this->db->set('comanda_id', 'comanda_id*-1',FALSE);
        return $this->db->update('cpbe_cabecera');
    }
    public function update_cpbe_det_negativo($cabecera_id){
        $this->db->where('cabecera_id', $cabecera_id);
        $this->db->set('comanda_item_id', 'comanda_item_id*-1',FALSE);
        $this->db->set('comanda_id', 'comanda_id*-1',FALSE);
        return $this->db->update('cpbe_detalle');
    }
}







