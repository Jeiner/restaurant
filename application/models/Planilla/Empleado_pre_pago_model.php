<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Empleado_pre_pago_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // todas las comandas entre un rango de fecha
    public function get_prepagos_by_fechas($start_date, $end_date, $empleado_id){
        $myQuery = "SELECT 
                        A.*,
                        B.descripcion AS 'tipo_concepto_desc',
                        B.columNa_aux_1 AS 'color'
                    FROM empleado_pre_pago A
                    INNER JOIN multitabla B ON B.tipo_multitabla_id = 45 AND A.tipo_concepto = B.valor 
                    WHERE DATE(A.fecha_pago) >= '".$start_date."' 
                            and DATE(A.fecha_pago) <= '".$end_date."'
                            and (A.empleado_id = ".$empleado_id.")
                            and (A.estado = 'A')
                    ORDER BY A.fecha_pago asc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function insert($data){
        $this->db->insert('empleado_pre_pago', $data);
        return $this->db->insert_id();
    }

}

