<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mesa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $mesa_id = "0";  
    public $mesa = "";  
    public $capacidad = "";
    public $estado = "L"; //'L:Libre, O:Ocupada, P: Por salir'

    public function insert($data){
        return $this->db->insert('mesa', $data);
    }
    public function update($data){
        $this->db->where('mesa_id', $data["mesa_id"]);
        return $this->db->update('mesa', $data);
    }
    public function delete($mesa_id){
        $this->db->where('mesa_id',$mesa_id);
        return $this->db->delete('mesa');
    }
    public function get_one($mesa_id){
        $this->db->select("*");
        $this->db->from('mesa');
        $this->db->where('mesa_id',$mesa_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("*");
        $this->db->from('mesa');
        $this->db->order_by('mesa', "asc");
        $query = $this->db->get();
        return $query->result();
    }
    public function cambiar_estado($mesa_id, $nuevo_estado){
        $this->db->where('mesa_id', $mesa_id);
        $this->db->set('estado', $nuevo_estado);
        $this->db->update('mesa');
        $this->cambiar_estado_unidas($mesa_id, $nuevo_estado);
        return true;
    }
    public function cambiar_estado_unidas($mesa_principal_id, $nuevo_estado){
        // Obtengo todas las uniones de dicha mesa que aun no hayan finalizado
        $myQuery = "select * from union_mesa where mesa_principal_id = '".$mesa_principal_id."' and estado <> 'L'";
        $q = $this->db->query($myQuery, false);
        $rows  = $q->result();
        // Si hay uniones las actualizo al nuevo estado
        if(count($rows) > 0){
            // a todas la uniones cambiamos de estado 
            $this->db->where('mesa_principal_id', $mesa_principal_id);
            $this->db->where('estado <>', 'L');
            $this->db->set('estado', $nuevo_estado);
            $this->db->update('union_mesa');
            // Cambiamos de estado a todas las mesas unidas.
            foreach ($rows as $key => $row) {
                $query = "UPDATE mesa SET estado = '".$nuevo_estado."' WHERE mesa_id = '".$row->mesa_unida_id."'";
                $this->db->query($query);
            }
        }
    }
    public function comandas_by_mesa($mesa_id){
        $this->db->select("*");
        $this->db->from('comanda');
        $this->db->where('mesa_id', $mesa_id);
        $query = $this->db->get();
        return $query->result();
    }
}

