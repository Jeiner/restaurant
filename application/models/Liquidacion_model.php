<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Liquidacion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public $liquidacion_id = "0";
    public $fecha_pago = "";
    public $motivo = "SEM";
    public $empleado_id = "0";
    public $total_remuneraciones = "0";
    public $total_descuentos = "0";
    public $total_a_pagar = "0";
    public $estado = "A";
    public $observaciones = "";


    public function insert($data,$items){
        // return $this->db->insert('liquidacion', $data);

        if( $this->db->insert('liquidacion', $data) ){
            $liquidacion_id = $this->db->insert_id();
            foreach ($items as $key => $item) {
                $item['liquidacion_id'] = $liquidacion_id;
                $this->db->insert('liquidacion_detalle', $item);
            }
        }
        return $liquidacion_id;  
    }
    public function update($data){
        $this->db->where('liquidacion_id', $data["liquidacion_id"]);

        $this->db->set('motivo', $data['motivo']);
        $this->db->set('empleado_id', $data['empleado_id']);
        $this->db->set('total_remuneraciones', $data['total_remuneraciones']);
        $this->db->set('total_descuentos', $data['total_descuentos']);
        $this->db->set('total_a_pagar', $data['total_a_pagar']);
        $this->db->set('estado', $data['estado']);
        $this->db->set('observaciones', $data['observaciones']);

        return $this->db->update('liquidacion');
    }
    public function get_all(){
        $this->db->select("
                        l.*,
                        e.nombre_completo,
                        s.sede as 'sede_desc',
                        mm.descripcion  as 'motivo_desc'
                        ");
        $this->db->from('liquidacion l');
        $this->db->join('empleado e', 'e.empleado_id = l.empleado_id');
        $this->db->join('sede s', 's.sede_id = l.sede_id');
        $this->db->join('multitabla mm', 'mm.valor = l.motivo and mm.tipo_multitabla_id = 13');
        $this->db->order_by('l.fecha_pago', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_with_filtros($anual = "0", $mes_id = "0"){
        $myQuery = "select 
                        l.*,
                        e.nombre_completo,
                        s.sede as 'sede_desc',
                        mm.descripcion  as 'motivo_desc'

                        FROM liquidacion l 
                        JOIN empleado e on e.empleado_id = l.empleado_id
                        JOIN sede s on s.sede_id = l.sede_id
                        JOIN multitabla mm ON mm.valor = l.motivo and mm.tipo_multitabla_id = 13
                        where   (year(l.fecha_pago) = ".$anual." or ".$anual." = 0) and
                                (month(l.fecha_pago) = ".$mes_id." or ".$mes_id." = 0)
                        ORDER BY l.fecha_pago desc";

        $q = $this->db->query($myQuery, false);
        return $q->result();

    }
    
    public function get_one($liquidacion_id){
        $this->db->select("
                        l.*,
                        e.nombre_completo,
                        s.sede as 'sede_desc',
                        c.cargo as 'cargo_desc',
                        mm.descripcion  as 'motivo_desc'
                        ");
        $this->db->from('liquidacion l');
        $this->db->join('empleado e', 'e.empleado_id = l.empleado_id');
        $this->db->join('cargo c', 'c.cargo_id = e.cargo_id');
        $this->db->join('sede s', 's.sede_id = l.sede_id');
        $this->db->join('multitabla mm', 'mm.valor = l.motivo and mm.tipo_multitabla_id = 13');
        $this->db->where('liquidacion_id', $liquidacion_id);
        $this->db->order_by('l.fecha_pago', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_by_empleado($empleado_id){
        $this->db->select("*");
        $this->db->from('liquidacion');
        $this->db->where('empleado_id', $empleado_id);
        $this->db->order_by('fecha_pago', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    
    public function eliminar_fisico($liquidacion_id){
        // Guardar en la papelera de reciclaje
        $this->db->where('liquidacion_id',$liquidacion_id);
        return $this->db->delete('liquidacion');
    }
    public function anular_liquidacion($liquidacion_id){
        // Guardar en la papelera de reciclaje
        $this->db->where('liquidacion_id',$liquidacion_id);
        // $this->db->set('total_remuneraciones', 0);
        // $this->db->set('total_descuentos', 0);
        // $this->db->set('total_a_pagar', 0);
        $this->db->set('estado', 'X');

        return $this->db->update('liquidacion');
    }
    public function anular_detalle($liquidacion_id){
        // Guardar en la papelera de reciclaje
        $this->db->where('liquidacion_id',$liquidacion_id);
        // $this->db->set('importe', 0);
        $this->db->set('estado', 'X');

        return $this->db->update('liquidacion_detalle');
    }
    public function ultimos_pagos($empleado_id){
        $this->db->select("
                        l.*,
                        e.nombre_completo,
                        s.sede as 'sede_desc',
                        c.cargo as 'cargo_desc',
                        mm.descripcion  as 'motivo_desc'
                        ");
        $this->db->from('liquidacion l');
        $this->db->join('empleado e', 'e.empleado_id = l.empleado_id');
        $this->db->join('cargo c', 'c.cargo_id = e.cargo_id');
        $this->db->join('sede s', 's.sede_id = l.sede_id');
        $this->db->join('multitabla mm', 'mm.valor = l.motivo and mm.tipo_multitabla_id = 13');
        $this->db->where('l.empleado_id', $empleado_id);
        $this->db->order_by('l.fecha_pago', "desc");
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }

    // Detalle

    public function get_detalle_by_liquidacion($liquidacion_id){
        $this->db->select("
                        l.*,
                        c.concepto,
                        c.tipo_concepto
                        ");
        $this->db->from('liquidacion_detalle l');
        $this->db->join('concepto c', 'c.concepto_id = l.concepto_id');
        $this->db->where('liquidacion_id',$liquidacion_id);
        $query = $this->db->get();
        return $query->result();
    }
}

