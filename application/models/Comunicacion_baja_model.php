<?php defined('BASEPATH') OR exit('No direct script access allowed');
//Documentos electronicos enviados asunat
class Comunicacion_baja_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //insertar comunicacion baja
    public function insert_baja_cab($data){
        $this->db->insert('cpbe_baja', $data);
        return  $this->db->insert_id();
    }
    public function get_bajas_by_comanda($comanda_id){
        $this->db->select("*");
        $this->db->from('cpbe_baja');
        $this->db->where('comanda_id =',$comanda_id);
        $query = $this->db->get();
        return $query->result();
    }
    //insertar detalle del comprobante electronico
    public function insert_baja_det($data){
        
    }
}







