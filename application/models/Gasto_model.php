<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gasto_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $gasto_id = "0";
    public $fecha = "";
    public $usuario = "";
    public $caja_id = "";
    public $monto = "";
    public $tipo = "EFEC";//'EFEC: efectivo, TAR: tarjeta',  mutitabla: 2
    public $descripcion = "";
    public $estado = ""; //'A:Activo, X:Eliminado',

    public function insert($data){
        return $this->db->insert('gasto', $data);
    }
    // public function update($data){
    //     $this->db->where('gasto_id', $data["gasto_id"]);
    //     return $this->db->update('gasto', $data);
    // }
    public function anular($data){
         $this->db->where('gasto_id', $data["gasto_id"]);
        return $this->db->update('gasto', $data);
    }
    public function get_one($gasto_id){
        $this->db->select("*");
        $this->db->from('gasto');
        $this->db->where('gasto_id',$gasto_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all(){
        $this->db->select("g.*,t.descripcion as 'tipo_desc' ");
        $this->db->from('gasto g');
        $this->db->join('multitabla t','t.valor = g.tipo and t.tipo_multitabla_id = 2');
        $this->db->order_by('fecha', "desc");
        $query = $this->db->get();
        return $query->result();
    }

    // Obtener numero de ordenes referenciados a esta gasto
}

