<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marcacion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data){
        return $this->db->insert('marcacion', $data);
    }

    public function get_all(){
        $this->db->select("*");
        $this->db->from('marcacion');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_ultima_fecha_carga(){
        $this->db->select("*");
        $this->db->from('marcacion');
        $this->db->order_by('fecha', 'desc');
        $this->db->order_by('hora', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function update($data){
        $this->db->where('marcacion_id', $data["marcacion_id"]);
        return $this->db->update('marcacion', $data);
    }

    public function get_all_by_fechas($start_date, $end_date, $id_usuario_asistencia){
        $myQuery = "select * from marcacion
                    where date(fecha) >= '".$start_date."' 
                            and date(fecha) <= '".$end_date."'
                            and (id_usuario_asistencia = '".$id_usuario_asistencia."' OR '".$id_usuario_asistencia."' = '*')
                    order by fecha desc, hora desc";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }

    public function get_by_usuario_fecha_hora($id_usuario_asistencia, $fecha, $hora){
        $this->db->select("*");
        $this->db->from('marcacion');
        $this->db->where('id_usuario_asistencia', $id_usuario_asistencia);
        $this->db->where('fecha', $fecha);
        $this->db->where('hora', $hora);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_marcaciones_mensuales_by_empleado_id($empleado_id, $anio, $mes){
        $myQuery = "SELECT * FROM marcacion 
                    WHERE empleado_id = '".$empleado_id."' 
                            and YEAR(fecha) = '".$anio."'
                            and MONTH(fecha) = '".$mes."'
                    ORDER BY marcacion_id DESC";
        $q = $this->db->query($myQuery, false);
        return $q->result();
    }
}

