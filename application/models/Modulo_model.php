<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public $modulo_id = "";
    public $modulo = "";
    public $icono = "";
    public $prioridad = "0";
    public $estado = "";  //'A:activo, X:Eliminado',

    public function get_all(){
        $this->db->select("*");
        $this->db->from('modulo');
        $this->db->order_by('prioridad', "desc");
        $query = $this->db->get();
        return $query->result();
    }
    public function get_all_by_rol($rol_id = 0){
        $this->db->select(" m.* ");
        $this->db->from('modulo m');
        $this->db->join('acceso a','a.modulo_id = m.modulo_id');
        $this->db->where('a.rol_id',$rol_id);
        $this->db->where('m.estado','A');
        $this->db->order_by('m.prioridad', "desc");
        $query = $this->db->get();
        return $query->result();
    }
}

