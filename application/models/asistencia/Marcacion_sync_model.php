<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marcacion_sync_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function insert_sync($data){
        $this->db  = $this->load->database('biotime', TRUE);
        return $this->db->insert('iclock_transaction_sync_nube', $data);
    }
}

