<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Marcacion_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function obtener_marcaciones_para_nube(){
        /*Marcaciones que no han sido migradas*/
        
        $this->db  = $this->load->database('biotime', TRUE);
        $myQuery = "SELECT A.id AS 'transaction_id', B.id AS 'emp_id', B.emp_code, B.first_name, B.photo, B.department_id, B.create_time,
                     A.emp_code, A.punch_time, A.punch_state, A.upload_time
                    FROM iclock_transaction A
                    INNER JOIN personnel_employee B ON A.emp_code = B.emp_code
                    LEFT JOIN iclock_transaction_sync_nube C ON C.transaction_id = A.id
                    WHERE C.transaction_id IS NULL
                    ORDER BY A.id DESC";
        $query = $this->db->query($myQuery, false);
        return $query->result();
    }
}

