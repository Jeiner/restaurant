<?php defined('BASEPATH') or exit('No direct script access allowed');

class Documento_electronico extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Caja_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Generalidades_model');
        $this->load->model('Comanda_model');
        $this->load->model('Cliente_model');
        $this->load->model('Pago_model');
        $this->load->model('Numeracion_model');
        $this->load->model('Documento_electronico_model');
        $this->load->model('facsu/Ws_facsu_model');
        $this->load->model('Plantillas_cbe_model');
        $this->load->model('Comunicacion_baja_model');
        $this->load->model('Comunicacion_baja_model');
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    /*Carga la vista de nuevo documento electronico*/
    function nuevo_documento($comanda_id){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        try {
            //Verificar si existe la comanda
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificada, por favor comuníquese con el área de sistemas.");
            }
            //Obtenemos el ID para saber en que local estamos.
            $generalidades_id = $this->session->userdata('session_generalidades_id');
            $data['generalidades'] = ($this->Generalidades_model->get_one())[0];
            $data['numeraciones'] = $this->Numeracion_model->get_numeracion_comprobante($generalidades_id);
            $data['comprobantes'] = $this->Multitabla_model->get_all_by_tipo(7);
            $data['documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);
            $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
            $data['oComanda']->items = $this->Comanda_model->get_items_by_comanda($comanda_id);
            //Verificar y obtener el pago de la comanda
            $oPago = $this->Pago_model->get_one_by_comanda($comanda_id);
            if(count($oPago) == 0){
                throw new Exception("Error, La comanda especificada aún no ha sido pagada.");
            }
            // Verificar los días transcurridos desde el día de la venta
            $nro_dias = $this->Site->dias_transcurridos($data['oComanda']->fecha_comanda);
            if($nro_dias > 3){
                throw new Exception("Error, No se puede emitir comprobante electrónico, debido a los días transcurridos.");
            }
            $data['oPago'] = $oPago[0];
            //Verificar que no haya sido declarada totalmente
            if($data['oComanda']->con_comprobante_e == 1){
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message',"La comanda ya fue declarada a SUNAT.");
                $url = base_url('facturacion/documento_electronico/ver_documento/').$data['oPago']->comanda_id;
                redirect($url);
            }
            //Obtenemos la cabecera del documento electronico
            $data['oCpbeCabecera'] = $this->Documento_electronico_model->get_cpbe_CAB_by_comanda($comanda_id);
            $data['oCpbeDetalle'] = $this->Documento_electronico_model->get_cpbe_DET_by_comanda($comanda_id);
            $data['lstCpbeBajas'] = $this->Comunicacion_baja_model->get_bajas_by_comanda($comanda_id);
            // echo json_encode($data['oCpbeCabecera']);exit()
            $bc = array(
                array('link' => base_url(), 'page' => 'Inicio'),
                array('link' => '#', 'page' => 'Facturación' ),
                array('link' => '#', 'page' => 'Emitir Comprobante Electrónico' ),
            ); //breadcrumbs 
            $meta = array( 'page_title' => 'Emitir Comprobante Electrónico', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'documentos_electronicos' );
            $this->page_construct('facturacion/documento_electronico/nuevo_documento', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    /*Método que permite generar solo un documento electronico para una comanda.*/
    function generar_documento(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        // echo json_encode($this->input->post());exit();
        try {
            $data = array(  
                    'pago_id' => $this->input->post('pago_id'),
                    'comanda_id' => $this->input->post('comanda_id'),
                    'cliente_id' => $this->input->post('cliente_id'),
                    'sunat_tipo_comprobante_id'=> $this->input->post('sunat_tipo_comprobante_id'),
                    'cliente_tipo_documento_id'=> $this->input->post('cliente_tipo_documento_id'),
                    'cliente_documento' => $this->input->post('cliente_documento'),
                    'cliente_nombre_completo' => $this->input->post('cliente_nombre_completo'),
                    'cliente_nombres' => $this->input->post('cliente_nombres'),
                    'cliente_ape_paterno' => $this->input->post('cliente_ape_paterno'),
                    'cliente_ape_materno' => $this->input->post('cliente_ape_materno'),
                    'cliente_direccion' => $this->input->post('cliente_direccion'),
                    'cliente_telefono' => $this->input->post('cliente_telefono'),
                    'cliente_correo' => $this->input->post('cliente_correo'),

                    'dec_seleccionados' => $this->input->post('dec_seleccionados'),
                    'dec_importe_total' => $this->input->post('dec_importe_total'),
                    'por_consumo' => $this->input->post('por_consumo'),
                );

            //Obtenemos la comanda y el pago
            $oComanda = $this->Comanda_model->get_one($data['comanda_id']);
            $oPago = $this->Pago_model->get_one_by_comanda($data['comanda_id']);
            if(count($oComanda) == 0){
                throw new Exception("Error, no se encontró la comanda, por favor comuníquese con el área de sistemas.");
            }
            if(count($oPago) == 0){
                throw new Exception("Error, no se encontró la comanda, por favor comuníquese con el área de sistemas.");
            }
            $oComanda = $oComanda[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $oPago = $oPago[0];

            //Validar que no haya sido declarado en su totalidad
            if($oComanda->con_comprobante_e == 'C'){
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message',"La comanda ya fue declarada a SUNAT.");
                $url = base_url('facturacion/documento_electronico/ver_documento/').$data['oPago']->comanda_id;
                redirect($url);
            }

            //Verificar si hay item seleccionados
            if(count($data['dec_seleccionados']) == 0){
                throw new Exception("Error, debe seleccionar productos a declarar.");
            }

            //Generemos una nueva comanda TOTAL O PARCIAL para emitir documento electronico
            $oNuevaComanda = $this->crear_nueva_comanda($oComanda, $data);

            //Obtene el cliente, registrar o actualizar
            $oCliente = $this->obtener_cliente($data);

            //Generalidades y numeración del comprobante
            $generalidades_id = $this->session->userdata('session_generalidades_id');
            $sunat_tipo_comprobante_id = $data['sunat_tipo_comprobante_id'];
            $oNumeracion = $this->Numeracion_model->get_numeracion_comprobante($generalidades_id,$sunat_tipo_comprobante_id);
            if(count($oNumeracion) == 0){
                throw new Exception("No se encontró correlativo válido para el comprobante.");
            }
            $oNumeracion = $oNumeracion[0];

            $this->form_validation->set_rules('cliente_documento', "Número de documento ", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

            if ($this->form_validation->run() == true) {
                //Insertar datos para documento electronico
                $cabecera_id = $this->insertar_datos_documento_electronico($oNuevaComanda, $oCliente, $oNumeracion, $data);

                //Actualizamos la comanda, las columnas de documento electronico
                $this->actualizar_comanda_por_cpbe($oNuevaComanda);

                //Actualizar el correlativo
                $correlativo_num = $oNumeracion->correlativo_num;
                $correlativo_num++;
                $new_correlativo = $this->Site->rellenar_izquierda($correlativo_num,'0',8);
                $this->Numeracion_model->update_numeracion($generalidades_id, $data['sunat_tipo_comprobante_id'],$new_correlativo,$correlativo_num);

                //Envia los datos a facturacion de sunat en nube.(FACSU - Amazon)
                $mensaje = "Comprobante Electrónico generado correctamente para comanda: ".$oComanda->comanda_id.".";
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', $mensaje);

                //Para imprimir comprobante electrònico
                $this->session->set_userdata('enviar_comprobante_e',true);
                $this->session->set_userdata('cabecera_id_para_comprobante',$cabecera_id);

                //Sesion para enviar comprobante electronico a FACSU
                $this->session->set_userdata('ses_enviar_cbe_a_FACSU',true); //Enviar comprobante electrònico a FACSU
                $this->session->set_userdata('ses_enviar_cbe_a_FACSU_comanda_id',$cabecera_id); //Cabecera_id
                //
                $url = base_url().'facturacion/documento_electronico/nuevo_documento/'.$oComanda->comanda_id;

                $this->set_session_enviar_comprobante_nube($cabecera_id);
                
                redirect($url);
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    /*Método que permite anular un documento electronico. */
    function anular_cpbe(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oCaja = ($this->Caja_model->get_aperturada())[0];

        try {
            $data = array(
                    'anular_cpbe_cabecera_id' => $this->input->post('anular_cpbe_cabecera_id'),
                    'anular_cpbe_serie' => $this->input->post('anular_cpbe_serie'),
                    'anular_cpbe_correlativo' => $this->input->post('anular_cpbe_correlativo'),
                    'anular_cpbe_motivo' => $this->input->post('anular_cpbe_motivo'),
                );

            //Obtener el comprobante electronico
            $buscarCpbe = $this->Documento_electronico_model->get_cpbe_cab($data['anular_cpbe_cabecera_id']);
            if(count($buscarCpbe) == 0){
                throw new Exception("Error, no se encontró el comprobante, por favor comuníquese con el área de sistemas.");
            }
            $oCabecera = $buscarCpbe[0];
            $comanda_id = $oCabecera->comanda_id;
            $pago_id = $oCabecera->pago_id;
            if($oCabecera->serie != $data['anular_cpbe_serie'] || $oCabecera->correlativo != $data['anular_cpbe_correlativo'] ){
                throw new Exception("Error, serie y correlativo del comprobante no coinciden, por favor comuníquese con el área de sistemas.");
            }

            //Obtenemos la comanda y el pago
            $oComanda = $this->Comanda_model->get_one($comanda_id);
            $oPago = $this->Pago_model->get_one($pago_id);
            if(count($oComanda) == 0){
                throw new Exception("Error, no se encontró la comanda, por favor comuníquese con el área de sistemas.");
            }
            if(count($oPago) == 0){
                throw new Exception("Error, no se encontró la comanda, por favor comuníquese con el área de sistemas.");
            }
            $oComanda = $oComanda[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $oPago = $oPago[0];

            //Registrar la comunicación de baja
            $o_caja_cabecera['comanda_id'] = $comanda_id;
            $o_caja_cabecera['fecGeneracion'] = $oCabecera->fecEmision;
            $o_caja_cabecera['fecComunicacion'] = date("Y-m-d");
            $o_caja_cabecera['tipDocBaja'] = $oCabecera->tipoComprobante;
            $o_caja_cabecera['numDocBaja'] = $oCabecera->serie."-".$oCabecera->correlativo;
            $o_caja_cabecera['desMotivoBaja'] = $data['anular_cpbe_motivo'];
            $o_caja_cabecera['estadoSunat'] = "";
            $o_caja_cabecera['estado'] = "A";

            $baja_id = $this->Comunicacion_baja_model->insert_baja_cab($o_caja_cabecera);
            if($baja_id <= 0){
                throw new Exception("Error al registrar la comunicación de baja.");
            }

            //Actualizar la cabecera y detalle
            $this->Documento_electronico_model->update_cpbe_cab_negativo($oCabecera->cabecera_id);
            $this->Documento_electronico_model->update_cpbe_det_negativo($oCabecera->cabecera_id);

            //Actualizar comanda
            $data_comanda_update['comanda_id'] = $oComanda->comanda_id;
            $data_comanda_update['con_comprobante_e'] = 'R'; //Parcial
            $this->Comanda_model->add_documento_e($data_comanda_update); //Update comanda

            //Enviar a facsu


            //Mostrar mensaje
            $mensaje = "Comprobante Electrónico anulado correctamente: ".$oCabecera->serie."-".$oCabecera->correlativo;
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    } 

    //Crea nueva comanda PARCIAL O TOTAL para generar documento electronico
    private function crear_nueva_comanda($oComanda, $data){
        $data['dec_importe_total'] = (float)(str_replace(' ', '', $data['dec_importe_total']));
        $nuevaComanda = new Comanda_model();

        //obtener igv
        $igv = $this->obtener_igv(); //0.18
        $parametro_sin_igv = floatval(1 + floatval($igv)); //1.18
        $porcentaje_igv = $igv * 100;

        //Para DOCUMENTO PARCIAL
        if($data['dec_importe_total'] < $oComanda->importe_total){

            //Seteamos datos a la nueva comanda
            $nuevaComanda->comanda_id   = $oComanda->comanda_id;
            $nuevaComanda->pago_id      = $oComanda->pago_id;

            //Montos parciales a la nueva comanda
            $nuevaComanda->importe_total = $data['dec_importe_total'];
            $nuevaComanda->importe_sin_IGV = round($data['dec_importe_total']/$parametro_sin_igv, 2);
            $nuevaComanda->total_IGV = $nuevaComanda->importe_total - $nuevaComanda->importe_sin_IGV;

            //Obtenemos solo los item seleccionados
            for ($i=0; $i < count($data['dec_seleccionados']); $i++) { 
                foreach ($oComanda->items as $key => $item) {
                    if($data['dec_seleccionados'][$i] == $item->comanda_item_id){
                        array_push($nuevaComanda->items, $item);
                        break;
                    }
                }
            }
        }

        //Para DOCUMENTO TOTAL
        if($data['dec_importe_total'] == $oComanda->importe_total){
            $nuevaComanda = $oComanda;
        }

        return $nuevaComanda;
    }
    private function obtener_cliente($data){
        $oCliente = $this->Cliente_model->get_by_doc($data['cliente_documento']);
        $clientesEncontrados = count($oCliente);

        //Inserta
        if($clientesEncontrados == 0){
            $data_cliente['documento'] = $data['cliente_documento'];
            $data_cliente['ape_paterno'] = $data['cliente_ape_paterno'];
            $data_cliente['ape_materno'] = $data['cliente_ape_materno'];
            $data_cliente['nombres'] = $data['cliente_nombres'];
            $data_cliente['nombre_completo'] = $data['cliente_nombre_completo'];
            $data_cliente['telefono'] = $data['cliente_telefono'];
            $data_cliente['correo'] = $data['cliente_correo'];
            $data_cliente['direccion'] = $data['cliente_direccion'];
            $data_cliente['sunat_tipo_documento_id'] = $data['cliente_tipo_documento_id'];

            $cliente_id = $this->Cliente_model->insert($data_cliente);

            if(!$cliente_id){
                throw new Exception("Error en el registro del Cliente.");
            }
            $oCliente = $this->Cliente_model->get_by_doc($data['cliente_documento']);
            return $oCliente[0];
        }

        //Actualiza
        if($clientesEncontrados == 1){
            $oCliente = $oCliente[0];
            $this->verificar_cambios_en_cliente($oCliente, $data);
            $oCliente = $this->Cliente_model->get_by_doc($data['cliente_documento']);
            return $oCliente[0];
        }

        if($clientesEncontrados > 1){
            throw new Exception("Se encontró más de un cliente con el mismo número de documento.");
        }
    }
    private function verificar_cambios_en_cliente($oCliente, $data){
        //Si es clientes Varios no actualizar
        if($oCliente->documento == "00000000"){
            return true;
        }

        //Si hay datos que se modificaron, actualizamos el cliente 
        $data_U_cliente = null;
        if($oCliente->ape_paterno != $data['cliente_ape_paterno'])
            $data_U_cliente['ape_paterno'] = $data['cliente_ape_paterno'];
        if($oCliente->ape_materno != $data['cliente_ape_materno'])
            $data_U_cliente['ape_materno'] = $data['cliente_ape_materno'];
        if($oCliente->nombres != $data['cliente_nombres'])
            $data_U_cliente['nombres'] = $data['cliente_nombres'];
        if($oCliente->nombre_completo != $data['cliente_nombre_completo']) 
            $data_U_cliente['nombre_completo'] = $data['cliente_nombre_completo'];
        if($oCliente->telefono != $data['cliente_telefono']) 
            $data_U_cliente['telefono'] = $data['cliente_telefono'];
        if($oCliente->correo != $data['cliente_correo']) 
            $data_U_cliente['correo'] = $data['cliente_correo'];
        if($oCliente->direccion != $data['cliente_direccion']) 
            $data_U_cliente['direccion'] = $data['cliente_direccion'];

        if($data_U_cliente == null){
            return true;
        }
        
        if( count($data_U_cliente) > 0 ){
            $data_U_cliente['cliente_id'] = $oCliente->cliente_id;
            $oCliente = $this->Cliente_model->update($data_U_cliente);
        }
    }
    private function actualizar_comanda_por_cpbe($oComanda){
        $con_comprobante_e = 'C'; //Completo
        $boolDeclarado = true;

        //Obtenemos todas las facturaciones de la comanda
        $oComandaOriginal = $this->Comanda_model->get_one($oComanda->comanda_id);
        $items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
        $itemsDeclarado = $this->Documento_electronico_model->get_cpbe_DET_by_comanda($oComanda->comanda_id);
        
        foreach ($items as $key => $item) { //Productos originales
            if($item->estado_atencion == 'X'){
                continue;
            }
            $boolDeclarado = false;
            foreach ($itemsDeclarado as $key => $declarado) { //Productos declarados
                if($item->comanda_item_id == $declarado->comanda_item_id){
                    $boolDeclarado = true;
                    break;
                }
            }
            if($boolDeclarado == false){
                $con_comprobante_e = 'R'; //Parcial
                break;
            }
        }
        $data_doc['comanda_id'] = $oComanda->comanda_id;
        $data_doc['con_comprobante_e'] = $con_comprobante_e;
        $this->Comanda_model->add_documento_e($data_doc); //Update comanda
    }
    /*Verifica si los items de una comanda estan pendientes por declarar, VERDADERO si estan pendientes*/
    private function verificar_items_pendientes($oComanda){
        $itemsDeclarado = $this->Documento_electronico_model->get_cpbe_DET_by_comanda($oComanda->comanda_id);
        foreach ($itemsDeclarado as $key => $declarado) {
            foreach ($oComanda->items as $key => $item) {
                if($item->comanda_item_id == $declarado->comanda_item_id){
                    return false;
                    break;
                }
            }
        }
        return true;
    }

    /*Inserta datos a la cabecera y detalle para ser utilizado en la generación de archivos para SUNAT*/
    function insertar_datos_documento_electronico($oComanda, $oCliente, $oNumeracion, $data){
        //Si alguno no esta pendiemte
        if(!$this->verificar_items_pendientes($oComanda)){
            throw new Exception("Los productos seleccionados ya han sido declarados a SUNAT.");
        }
        $o_cpbe_cabecera['comanda_id'] = $oComanda->comanda_id;
        $o_cpbe_cabecera['pago_id'] = $oComanda->pago_id;
        $o_cpbe_cabecera['tipoComprobante'] = $data['sunat_tipo_comprobante_id'];
        $o_cpbe_cabecera['serie'] = $oNumeracion->serie;
        $o_cpbe_cabecera['correlativo'] = $oNumeracion->correlativo;

        $o_cpbe_cabecera['tipoOperacion'] = "0101"; //Venta interna
        $o_cpbe_cabecera['fecEmision'] = date("Y-m-d");
        $o_cpbe_cabecera['horEmision'] = date("H:i:s");
        $o_cpbe_cabecera['fecVencimiento'] = '-';
        $o_cpbe_cabecera['codLocalEmisor'] = '0000';
        $o_cpbe_cabecera['tipDocUsuario'] = $oCliente->sunat_tipo_documento_id;
        $o_cpbe_cabecera['numDocUsuario'] = $oCliente->documento;
        $o_cpbe_cabecera['rznSocialUsuario'] = $oCliente->nombre_completo;
        $o_cpbe_cabecera['tipMoneda'] = "PEN";
        $o_cpbe_cabecera['sumTotTributos'] = $oComanda->total_IGV;
        $o_cpbe_cabecera['sumTotValVenta'] = $oComanda->importe_sin_IGV;
        $o_cpbe_cabecera['sumPrecioVenta'] = $oComanda->importe_total;
        $o_cpbe_cabecera['sumDescTotal'] = 0.00;
        $o_cpbe_cabecera['sumOtrosCargos'] = 0.00;
        $o_cpbe_cabecera['sumTotalAnticipos'] = 0.00;
        $o_cpbe_cabecera['sumImpVenta'] = $oComanda->importe_total;
        $o_cpbe_cabecera['ublVersionId'] = "2.1";
        $o_cpbe_cabecera['customizationId'] = "2.0";
        
        $cabecera_id = $this->Documento_electronico_model->insert_cpbe_cab($o_cpbe_cabecera);
        if($cabecera_id <= 0){
            throw new Exception("Error al registrar los datos generales del comprobante electrónico.");
        }

        //Registramos el detalle del documento electrónico
        if($data['por_consumo'] == "1"){
            $detalle['comanda_item_id'] = $oComanda->items[0]->comanda_item_id; //Se considera este campo por que es primary y obligatorio
            $detalle['comanda_id'] = $oComanda->comanda_id;
            $detalle['producto_id'] = 0;
            $detalle['producto'] = "POR CONSUMO";
            $detalle['cantidad'] = 1;
            $detalle['precio_unitario'] = $oComanda->importe_total;

            $this->insertar_detalle_documento_electronico($cabecera_id,$detalle);
        }else{
            foreach ($oComanda->items as $key => $item) {
                if($item->estado_atencion == 'X'){
                    continue;
                }
                $detalle['comanda_item_id'] = $item->comanda_item_id;
                $detalle['comanda_id'] = $item->comanda_id;
                $detalle['producto_id'] = $item->producto_id;
                $detalle['producto'] = $item->producto;
                $detalle['cantidad'] = $item->cantidad;
                $detalle['precio_unitario'] = $item->precio_unitario;

                $this->insertar_detalle_documento_electronico($cabecera_id,$detalle);
            }
        }
            
        return $cabecera_id;
    }

    private function insertar_detalle_documento_electronico($cabecera_id, $detalle){
        //obtener igv
        $igv = $this->obtener_igv(); //0.18
        $parametro_sin_igv = floatval(1 + floatval($igv)); //1.18
        $porcentaje_igv = $igv * 100;

        $data_det['comanda_id'] = $detalle['comanda_id'];
        $data_det['comanda_item_id'] = $detalle['comanda_item_id'];
        $data_det['cabecera_id'] = $cabecera_id;
        $data_det['importe'] = $detalle['cantidad'] * $detalle['precio_unitario'];

        $data_det['codUnidadMedida'] = 'NIU'; /*DET_01*/
        $data_det['ctdUnidadItem'] = $detalle['cantidad'];
        $data_det['codProducto'] = $detalle['producto_id'];
        $data_det['codProductoSUNAT'] = '-';
        $data_det['desItem'] = $detalle['producto'];
        $data_det['mtoValorUnitario'] = $detalle['precio_unitario']/$parametro_sin_igv;    /*DET_6*/
        $data_det['sumTotTributosItem'] = (($detalle['precio_unitario']/$parametro_sin_igv) * $detalle['cantidad']) * $igv;/*DET_7*/

        $data_det['codTriIGV'] = '1000';    /*DET_8*/
        $data_det['mtoIgvItem'] = (($detalle['precio_unitario']/$parametro_sin_igv) * $detalle['cantidad']) * $igv; /*DET_9*/
        $data_det['mtoBaseIgvItem'] = ($detalle['precio_unitario']/$parametro_sin_igv) * $detalle['cantidad']; /*DET_10*/
        $data_det['nomTributoIgvItem'] = 'IGV'; /*DET_11*/
        $data_det['codTipTributoIgvItem'] = 'VAT';
        $data_det['tipAfeIGV'] = '10';
        $data_det['porIgvItem'] = $porcentaje_igv;

        $data_det['codTriISC'] = '-';
        $data_det['mtoIscItem'] = '0.00';
        $data_det['mtoBaseIscItem'] = '0.00';
        $data_det['nomTributoIscItem'] = 'ISC';
        $data_det['codTipTributoIscItem'] = 'EXC';
        $data_det['tipSisISC'] = '01';
        $data_det['porIscItem'] = '0.00';

        $data_det['codTriOtroItem'] = '-';
        $data_det['mtoTriOtroItem'] = '0.00';
        $data_det['mtoBaseTriOtroItem'] = '0.00';
        $data_det['nomTributoIOtroItem'] = '';
        $data_det['codTipTributoIOtroItem'] = '';
        $data_det['porTriOtroItem'] = '0.00';   /*DET_27*/
        $data_det['mtoPrecioVentaUnitario'] = $detalle['cantidad'] * $detalle['precio_unitario']; /*DET_28*/
        $data_det['mtoValorVentaItem'] = ($detalle['precio_unitario']/$parametro_sin_igv) * $detalle['cantidad']; /*DET_29*/
        $data_det['mtoValorReferencialUnitario'] = '0.00';  /*DET_30*/

        $this->Documento_electronico_model->insert_cpbe_det($data_det);
    }

    //Una venta adicional para enviar el comprobante electrònico, se debe cerrar cuando termina de enviar
    public  function popup_enviar_cbe_a_facsu($cabecera_id){
        $oGeneralidades = $this->Site->get_generalidades();
        $oCabecera = $this->Documento_electronico_model->get_cpbe_cab($cabecera_id);
        $items = $this->Documento_electronico_model->get_cpbe_det_by_cab($cabecera_id);
        if(count($oGeneralidades) == 1 && count($oCabecera) == 1 && count($items) > 0){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oCabecera'] = $oCabecera[0];
            $datos['items'] = $items;
            $oCliente = $this->Cliente_model->get_by_doc($datos['oCabecera']->numDocUsuario);
            $datos['oCliente'] = $oCliente[0];
            $rpta = $this->Ws_facsu_model->enviar_comprobante_e($datos);
            if(!$rpta['success']){
                $this->Site->registrar_log_error('A','Controler-Documento_electronico',$rpta['message']);
                echo $rpta['message'];
            }else{
                echo "Documento electrònico enviado correctamente.";
                echo "<script type='text/javascript'>";
                echo "window.close();";
                echo "</script>";
            }
        }else{
            $log_mensaje = "No se envió el documento electrónico al sistema de facturación FACSU";
            $this->Site->registrar_log_error('A','Controler-Documento_electronico',$log_mensaje);
        }
    }
    //Genera los archivos cab y Det que lee el facturador de SUNAT
    function generar_archivo_sunat($cabecera_id){
        try {
            $this->Caja_model->verificar_caja_aperturada();
            $this->loaders->verifica_sesion();

            $oGeneralidades = ($this->Site->get_generalidades())[0];
            //Obtener cabecera
            $oCabecera = $this->Documento_electronico_model->get_cpbe_cab($cabecera_id);
            if(count($oCabecera) != 1){
                throw new Exception("Error al generar el archivo SUNAT, No se encontró el documento electrónico.");
            }
            $oCabecera = $oCabecera[0];
            //Obtener Detalle
            $o_cpbe_detalle = $this->Documento_electronico_model->get_cpbe_det_by_cab($cabecera_id);
            if(count($o_cpbe_detalle) == 0){
                throw new Exception("Error al generar el archivo SUNAT, No se encontró detalle para el documento electrónico");
            }
            //Ruta de archivos SUNAT DATA
            $ruta_base = $this->Site->get_ruta_base_sunat();
            //Archivo cabecera
            $archivo_CAB = $oGeneralidades->RUC."-".$oCabecera->tipoComprobante."-".$oCabecera->serie."-".$oCabecera->correlativo.".cab";
            $ruta_CAB = $ruta_base.$archivo_CAB;
            $contenido_CAB = $oCabecera->tipoOperacion."|".   /*CAB_1*/
                             $oCabecera->fecEmision."|".      /*CAB_2*/
                             $oCabecera->horEmision."|".      /*CAB_3*/
                             $oCabecera->fecVencimiento."|".  /*CAB_4*/
                             $oCabecera->codLocalEmisor."|".  /*CAB_5*/
                             $oCabecera->tipDocUsuario."|".   /*CAB_6*/
                             $oCabecera->numDocUsuario."|".   /*CAB_7*/
                             $oCabecera->rznSocialUsuario."|".    /*CAB_8*/
                             $oCabecera->tipMoneda."|".       /*CAB_9*/
                             $oCabecera->sumTotTributos."|".  /*CAB_10*/
                             $oCabecera->sumTotValVenta."|".  /*CAB_11*/
                             $oCabecera->sumPrecioVenta."|".  /*CAB_12*/
                             $oCabecera->sumDescTotal."|".    /*CAB_13*/
                             $oCabecera->sumOtrosCargos."|".  /*CAB_14*/
                             $oCabecera->sumTotalAnticipos."|".   /*CAB_15*/
                             $oCabecera->sumImpVenta."|".     /*CAB_16*/
                             $oCabecera->ublVersionId."|".    /*CAB_17*/
                             $oCabecera->customizationId."|"; /*CAB_18*/
            //Archivo Detalle 
            $archivo_DET = $oGeneralidades->RUC."-".$oCabecera->tipoComprobante."-".$oCabecera->serie."-".$oCabecera->correlativo.".det";
            $ruta_DET = $ruta_base.$archivo_DET;
            $contenido_DET = "";
            foreach ($o_cpbe_detalle as $key => $detalle) {
                $contenido_DET .= $detalle->codUnidadMedida."|".
                                 $detalle->ctdUnidadItem."|".
                                 $detalle->codProducto."|".
                                 $detalle->codProductoSUNAT."|".
                                 $detalle->desItem."|".
                                 $detalle->mtoValorUnitario."|".
                                 $detalle->sumTotTributosItem."|".
                                 $detalle->codTriIGV."|".
                                 $detalle->mtoIgvItem."|".
                                 $detalle->mtoBaseIgvItem."|".
                                 $detalle->nomTributoIgvItem."|".
                                 $detalle->codTipTributoIgvItem."|".
                                 $detalle->tipAfeIGV."|".
                                 $detalle->porIgvItem."|".
                                 $detalle->codTriISC."|".
                                 $detalle->mtoIscItem."|".
                                 $detalle->mtoBaseIscItem."|".
                                 $detalle->nomTributoIscItem."|".
                                 $detalle->codTipTributoIscItem."|".
                                 $detalle->tipSisISC."|".
                                 $detalle->porIscItem."|".
                                 $detalle->codTriOtroItem."|".
                                 $detalle->mtoTriOtroItem."|".
                                 $detalle->mtoBaseTriOtroItem."|".
                                 $detalle->nomTributoIOtroItem."|".
                                 $detalle->codTipTributoIOtroItem."|".
                                 $detalle->porTriOtroItem."|".
                                 $detalle->mtoPrecioVentaUnitario."|".
                                 $detalle->mtoValorVentaItem."|".
                                 $detalle->mtoValorReferencialUnitario."|\n";
            }
            //GUARDAR ARCHIVO CAB
            $archivo_CAB = fopen($ruta_CAB, "w");
            fwrite($archivo_CAB, "$contenido_CAB");
            fclose($archivo_CAB);
            //GUARDAR ARCHIVO DET
            $archivo_DET = fopen($ruta_DET, "w");
            fwrite($archivo_DET, "$contenido_DET");
            fclose($archivo_DET);

        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function ver_documento($comanda_id){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        try {
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificada, por favor comuníquese con el área de sistemas.");
            }
            $generalidades_id = $this->session->userdata('session_generalidades_id');
            // $data['comprobantes'] = $this->Multitabla_model->get_all_by_tipo(7);
            // $data['comprobante_sunat'] = $this->Multitabla_model->get_all_by_tipo(17);
            // $data['documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);
            $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
            $data['oComanda']->items = $this->Comanda_model->get_items_by_comanda($comanda_id);
            //Obtenemos el pago de la comanda
            $oPago = $this->Pago_model->get_one_by_comanda($comanda_id);
            if(count($oPago) == 0){
                throw new Exception("Error, La comanda especificada no cuenta con pago aún.");
            }
            $data['oPago'] = $oPago[0];
            //Obtenemos la cabecera del documento electronico
            $data['oCpbeCabecera'] = $this->Documento_electronico_model->get_cpbe_CAB_by_comanda($comanda_id);
            $data['oCpbeDetalle'] = $this->Documento_electronico_model->get_cpbe_DET_by_comanda($comanda_id);
            
            //Verificar que no haya sido declarada
            if($data['oComanda']->con_comprobante_e == ''){
                throw new Exception("La comanda aún no ha sido declarada a SUNAT.");
            }
            
            //Obtenemos el cliente
            // $data['oCliente'] = ($this->Cliente_model->get_one($data['oPago']->cliente_id))[0];

            $bc = array(
                array('link' => base_url(), 'page' => 'Inicio'),
                array('link' => '#', 'page' => 'Facturación' ),
                array('link' => '#', 'page' => 'Ver Comprobante Electrónico' ),
            ); //breadcrumbs 
            $meta = array( 'page_title' => 'Ver Comprobante Electrónico', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'comandas_facturadas' );
            $this->page_construct('facturacion/documento_electronico/ver_documento', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('facturacion/documento_electronico/comandas_facturadas'));
        }
    }
    //Para llamar a la vista de listado de comprobantes electronicos
    function ver_documentos_electronicos(){
        $data['docs_pendientes'] = count($this->Documento_electronico_model->get_cpbe_pendientes());
        $data['docs_errores'] = count($this->Documento_electronico_model->get_cpbe_errores());
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Facturación' ),
                    array('link' => '#', 'page' => 'Comprobantes Electrónicos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Comprobantes Electrónicos', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'documentos_electronicos' );
        $this->page_construct('facturacion/documento_electronico/ver_documentos', $meta, $data);
    }
    function ajax_cargar_tabla_documentos(){
        // Listado de comprobantes electrónicos desde una fecha de inicio hasta una fecha de fin
        $start_date = $this->input->post('start_date');//Año-Mes-Dia
        $end_date = $this->input->post('end_date');
        $data['comprobantes_e'] = $this->Documento_electronico_model->get_cpbe_CAB_by_fechas($start_date, $end_date);
        $this->partial_view('facturacion/documento_electronico/_tabla_documentos',$data);
    }
    function ajax_cargar_modal_documento($cabecera_id){
        $data['generalidades'] = ($this->Site->get_generalidades())[0];
        $data['comprobante_e'] = ($this->Documento_electronico_model->get_cpbe_cab($cabecera_id))[0];
        $tmp_comanda_id = $data['comprobante_e']->comanda_id;
        $tmp_comanda = ($this->Comanda_model->get_one($tmp_comanda_id))[0];
        $tmp_cliente = $this->Cliente_model->get_one($tmp_comanda->cliente_id);
        if(count($tmp_cliente) > 0){
            $data['oCliente'] = $tmp_cliente[0];
        }else{
            $data['oCliente'] = null;
        }
        
        $data['detalle'] = $this->Documento_electronico_model->get_cpbe_det_by_cab($cabecera_id);
        $this->partial_view('facturacion/documento_electronico/_modal_documento',$data);
    }
    //Wweservice para Obtener el documento electronico para imprimir
    function ws_get_documento_e($cabecera_id){
        try {
            //Obtener el documento electronico (CABECERA)
            $oCabecera = $this->Documento_electronico_model->get_cpbe_cab($cabecera_id);
            if(count($oCabecera) == 0){
                throw new Exception("Error, No se encontró el documento electónico.");
            }
            $oCabecera = $oCabecera[0];
            //Obtenemos el detalle del documento electróncio
            $oDetalle = $this->Documento_electronico_model->get_cpbe_det_by_cab($cabecera_id);
            //Obtener la comanda
            $oComanda = $this->Comanda_model->get_one($oCabecera->comanda_id);
            if(count($oComanda) == 0){
                throw new Exception("Error, no se encontró la comanda especificada, por favor comuníquese con el área de sistemas.");
            }
            $oComanda = $oComanda[0];

            $data['oGeneralidades'] = ($this->Generalidades_model->get_one())[0];
            $data['oCabecera'] = $oCabecera;
            $data['oDetalle'] = $oDetalle;
            $data['oComanda'] = $oComanda;
            echo json_encode($data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }
    //Enviar, en lote, comprobantes electronicos pendientes a FACSU
    public  function enviar_lote_cbe_a_facsu(){
        $lista_errores = array();
        $lista_enviados = array();
        //Verificar documentos electrónicos pendientes
        $docs_pendientes = $this->Documento_electronico_model->get_cpbe_pendientes();
        if(count($docs_pendientes) == 0){
            echo "No hay comprobantes electrónicos pendientes.";
            echo "<script type='text/javascript'>";
            echo "window.close();";
            echo "</script>";
            exit();
        }
        //Verificar estado del servidor
        if(!$this->Ws_facsu_model->verificar_servidor()){
            echo "No hay respuesta del servidor FACSU.";
            exit();
        }

        $oGeneralidades = $this->Site->get_generalidades();
        foreach ($docs_pendientes as $key => $doc){
            $items = $this->Documento_electronico_model->get_cpbe_det_by_cab($doc->cabecera_id);
            $oCliente = $this->Cliente_model->get_by_doc($doc->numDocUsuario);
            //Verificar cliente
            if(count($oCliente) == 0){
                $lista_errores[count($lista_errores)] = $doc->serie.'-'.$doc->correlativo.' => '.'Cliente de referencia no existe.';
                continue;
            }
            //Datos a enviar
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oCabecera'] = $doc;
            $datos['items'] = $items;
            $datos['oCliente'] = $oCliente[0];
            $rpta = $this->Ws_facsu_model->enviar_comprobante_e($datos);
            if(!$rpta['success']){
                $lista_errores[count($lista_errores)] = $doc->serie.'-'.$doc->correlativo.' => '.$rpta['message'];
            }else{
                $lista_enviados[count($lista_enviados)] = $doc->serie.'-'.$doc->correlativo.' => '.$rpta['message'];
            }
        }
        //Finalizar y mostrar comprobantes electrónicos
        echo '('.count($lista_enviados).') Documentos enviados correctamente: '.'<br>';
        for ($i=0; $i < count($lista_enviados); $i++) { 
            echo $lista_enviados[$i].'<br>';
        }
        echo '<br>';
        echo '('.count($lista_errores).") Documentos No enviados:".'<br>';
        for ($i=0; $i < count($lista_errores); $i++) { 
            echo $lista_errores[$i].'<br>';
        }
    }
    public function procesar_lote_cpbe(){
        $lista_errores = array();
        $lista_correctos = array();
        //Verificar comprobantes electrónicos pendientes
        $cpbe_pendientes = $this->Documento_electronico_model->get_cpbe_pendientes();
        if(count($cpbe_pendientes) == 0){
            echo "No hay comprobantes electrónicos pendientes.";
            echo "<script type='text/javascript'>";
            echo "window.close();";
            echo "</script>";
            exit();
        }
        //Verificar estado del servidor
        if(!$this->Ws_facsu_model->verificar_servidor()){
            echo "No hay respuesta del servidor FACSU.";
            exit();
        }
        //Consultar comprobantes e
        foreach ($cpbe_pendientes as $key => $cpbe) {
            if($key == 20){
                break;
            }
            $cNumDoc = $cpbe->serie.'-'.$cpbe->correlativo;
            $rpta = $this->Ws_facsu_model->consultar_estado_cpbe($cNumDoc);
            //Validar la respuesra
            if(!$rpta['success']){
                $lista_errores[count($lista_errores)] = $cNumDoc.' => '.$rpta['message'];
                continue;
            }
            $rptaCpbe = $rpta['data'];
            if($cNumDoc != $rptaCpbe->cNumDoc){
                $lista_errores[count($lista_errores)] = $cNumDoc.' => '."Número de documento distinto en base de datos";
                continue;
            }
            if($cpbe->sumPrecioVenta != $rptaCpbe->nPreVen){
                $lista_errores[count($lista_errores)] = $cNumDoc.' => '."Montos totales distintos.";
                continue;
            }
            //Preparar data para actualizar el comprobante
            $dataCpbe['cabecera_id'] = $cpbe->cabecera_id;
            $dataCpbe['estadoSunat'] = $rptaCpbe->cIndSit;
            $this->Documento_electronico_model->update_cpbe_cab($cpbe->cabecera_id, $dataCpbe);
            $lista_correctos[count($lista_correctos)] = $cNumDoc.' => '."Correcto";
        }
        //Finalizar y mostrar comprobantes electrónicos
        echo '('.count($lista_correctos).') Documentos procesados correctamente: '.'<br>';
        for ($i=0; $i < count($lista_correctos); $i++) { 
            echo $lista_correctos[$i].'<br>';
        }
        echo '<br>';
        echo '('.count($lista_errores).") Errores:".'<br>';
        for ($i=0; $i < count($lista_errores); $i++) { 
            echo $lista_errores[$i].'<br>';
        }
        // Si hay mas copmporbantes y no hay errores 
        $redirectUrl = base_url('facturacion/documento_electronico/procesar_lote_cpbe/');
        if(count($lista_errores) <= 1 && count($cpbe_pendientes) > 20){
            echo "<script languaje='javascript' type='text/javascript'>setTimeout('location.reload(true);',1000);</script>";
        }
    }
    public function ajax_generar_cbe_pdf($nCbeId){
        $this->Plantillas_cbe_model->generar_cbe_pdf($nCbeId);
    }
}

