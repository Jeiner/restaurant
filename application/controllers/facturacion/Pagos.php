<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pagos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Caja_model');
        $this->load->model('Comanda_model');
        $this->load->model('Cliente_model');
        $this->load->model('Pago_model');
        $this->load->model('Generalidades_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Numeracion_model'); //Para la numeracion de comprobantes de pago
        $this->load->model('Documento_electronico_model'); //Para la numeracion de comprobantes de pago
        
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
        // $this->Caja_model->verificar_caja_aperturada();
    }
    function preparar_obj_pago(){
        if($this->session->userdata('oPago')){
            return $this->session->userdata('oPago');
        }else{
            $oPago = new Pago_model();
            $this->session->set_userdata('oPago', $oPago);
            return $this->session->userdata('oPago');
        }
    }
    function set_datos_obj_pago($data){
        $oPago =  $this->preparar_obj_pago();

        $oPago->pago_id = $data['pago_id'];
        $oPago->comanda_id = $data['comanda_id'];
        $oPago->importe_total = $data['importe_total'];
        $oPago->descuento = $data['descuento'];
        $oPago->total_a_pagar = $data['total_a_pagar'];
        $oPago->pago_efectivo = $data['pago_efectivo'];
        $oPago->pago_tarjeta = $data['pago_tarjeta'];
        $oPago->saldo = $data['saldo'];
        $oPago->efectivo = $data['efectivo'];
        $oPago->vuelto = $data['vuelto'];
        $oPago->total_pagado = $data['total_pagado'];
        $oPago->fecha_pago = $data['fecha_pago'];
        $oPago->cajero = $data['cajero'];
        $oPago->tipo_comprobante = $data['tipo_comprobante'];
        // echo $oPago->tipo_comprobante;exit(); 
        $oPago->cliente_id = $data['cliente_id'];
        $oPago->cliente_documento = $data['cliente_documento'];
        $oPago->cliente_nombre = $data['cliente_nombre'];
        $oPago->cliente_direccion = $data['cliente_direccion'];
        $oPago->cliente_telefono = $data['cliente_telefono'];
        $oPago->caja_id = $data['caja_id'];
        $oPago->observaciones = $data['observaciones'];
        $oPago->estado = $data['estado'];

        $this->session->set_userdata('oPago', $oPago);
    }
    function comandas(){
        $this->Caja_model->verificar_caja_aperturada();
        // Listar comandas para ser pagadas.
        $this->session->unset_userdata('oPago');                
        // Obtenemos la caja activa y las comandas de dicha caja
        $data['oCaja'] = ($this->Caja_model->get_aperturada())[0];
        $data['comandas'] = $this->Comanda_model->get_all_by_caja($data['oCaja']->caja_id);
        // echo json_encode($data['comandas']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Facturación' ),
                    array('link' => '#', 'page' => 'Comandas por facturar' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Por facturar', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'facturar_comandas' );
        $this->page_construct('facturacion/pagos/comandas', $meta, $data);
    }
    function nuevo($comanda_id = 0){
        $this->Caja_model->verificar_caja_aperturada();
        try {
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda, por favor comuníquese con el área de soporte.");
            }
            $data['clientes'] = $this->Cliente_model->get_all();
            $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
            $data['oComanda']->items = $this->Comanda_model->get_items_by_comanda($comanda_id);
            // Obtenemos los datos de la comanda y lo pasamos a pago
            if(count( $this->Pago_model->get_one_by_comanda($comanda_id)) == 0){
                $data['oPago'] = $this->preparar_obj_pago();
                if($data['oPago']->comanda_id != $comanda_id){
                    $this->session->unset_userdata('oPago');
                }
                $data['oPago'] = $this->preparar_obj_pago();
                $data['oPago']->fecha_pago = date("Y/m/d H:i:s");
                // Cliente
                $data['oPago']->cliente_id = $data['oPago']->cliente_id;
                $data['oPago']->cliente_documento = $data['oPago']->cliente_documento;
                $data['oPago']->cliente_nombre = $data['oPago']->cliente_nombre;
                $data['oPago']->cliente_telefono = $data['oPago']->cliente_telefono;
                $data['oPago']->cliente_direccion = $data['oPago']->cliente_direccion;

                if($data['oPago']->importe_total == 0 || $data['oPago']->importe_total == ""){
                    // Si se crea recien un nuevo objeto de pago
                    $data['oPago']->cajero = $this->session->userdata('session_usuario');
                    $data['oPago']->importe_total = $data['oComanda']->importe_total;
                    $data['oPago']->tipo_comprobante = ($data['oComanda']->tipo_comprobante == null) ? 'TCK' : $data['oComanda']->tipo_comprobante;
                    // Cliente
                    $data['oPago']->cliente_id = $data['oComanda']->cliente_id;
                    $data['oPago']->cliente_documento = $data['oComanda']->cliente_documento;
                    $data['oPago']->cliente_nombre = $data['oComanda']->cliente_nombre;
                    $data['oPago']->cliente_telefono = $data['oComanda']->cliente_telefono;
                    $data['oPago']->cliente_direccion = $data['oComanda']->cliente_direccion;
                }
                if($data['oPago']->descuento == 0){
                    $data['oPago']->total_a_pagar = $data['oComanda']->importe_total;
                }
                if($data['oPago']->pago_efectivo == 0 && $data['oPago']->pago_tarjeta == 0 && $data['oPago']->descuento == 0){
                    $data['oPago']->saldo = $data['oComanda']->importe_total;
                }
            }else{
                $data['oPago'] = ($this->Pago_model->get_one_by_comanda($comanda_id))[0];
            }
            //Obtenemos la serie y el correlativo de todos los comprobantes de una determinada sede
            $generalidades_id = $this->session->userdata('session_generalidades_id');
            $data['numeraciones'] = $this->Numeracion_model->get_numeracion_comprobante($generalidades_id);

            $bc = array(
                array('link' => base_url(), 'page' => 'Inicio'),
                array('link' => '#', 'page' => 'Facturación' ),
                array('link' => '#', 'page' => 'Facturar comanda' ),
            ); //breadcrumbs 
            $meta = array( 'page_title' => 'Facturar comanda', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'facturar_comandas' );
            $this->page_construct('facturacion/pagos/nuevo', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('facturacion/pagos/comandas'));
        }
    }
    
    function guardar_pago(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        try {
            $data = array(  
                    'pago_id' => $this->input->post('pago_id'),
                    'comanda_id' => $this->input->post('comanda_id'),
                    'importe_total' => "", // De la comanda
                    'descuento' => ($this->input->post('descuento'))? $this->input->post('descuento') : 0,
                    'total_a_pagar' => ($this->input->post('total_a_pagar'))? $this->input->post('total_a_pagar') : 0,
                    'pago_tarjeta' => ($this->input->post('pago_tarjeta'))?$this->input->post('pago_tarjeta'):0,
                    'pago_efectivo' => "", //Calculado
                    'efectivo' => ($this->input->post('efectivo'))?$this->input->post('efectivo'):0,
                    'vuelto' => ($this->input->post('vuelto'))?$this->input->post('vuelto'):0,
                    'saldo' => $this->input->post('saldo'),
                    'total_pagado' => "",
                    'fecha_pago' => date("Y/m/d H:i:s"),
                    'cajero' => $this->session->userdata('session_usuario'),
                    'tipo_comprobante' => $this->input->post('tipo_comprobante'),
                    'cliente_id' => $this->input->post('cliente_id'),
                    'cliente_documento' => $this->input->post('cliente_documento'),
                    'cliente_nombre' => $this->input->post('cliente_nombre'),
                    'cliente_direccion' => $this->input->post('cliente_direccion'),
                    'cliente_telefono' => $this->input->post('cliente_telefono'),
                    'caja_id' => $oCaja->caja_id,
                    'observaciones' => $this->input->post('observaciones'),
                    'estado' => 'A',
                );
            //Verificamos que exista la comanda
            if( count($this->Comanda_model->get_one($data['comanda_id'])) == 0){
                throw new Exception("No se encontró la comanda a facturar");
            }
            $oComanda = ($this->Comanda_model->get_one($data['comanda_id']))[0];
            // Verificamos que no esté pagada
            if($oComanda->estado_pago == 'C'){
                throw new Exception("No se realizó el pago, debido a que la comanda ya está pagada.");
            }
            if($oComanda->estado_pago == 'X' || $oComanda->estado_atencion == 'X'){
                throw new Exception("No se realizó el pago, debido a que la comanda está anulada.");
            }
            //CONVERTIMOS LOS NUMERO A DECIMALES
            $data['descuento']  = floatval($data['descuento']);
            $data['total_a_pagar']  = floatval($data['total_a_pagar']);
            $data['pago_tarjeta']  = floatval($data['pago_tarjeta']);
            $data['efectivo']  = floatval($data['efectivo']);
            $data['vuelto']  = floatval($data['vuelto']);
            $data['saldo']  = floatval($data['saldo']);
            // VERIFICAMOS QUE TODOS SEAN NUMERO
            if(!is_numeric($data['descuento'])){
                throw new Exception("Descuento no válido");
            }
            if(!is_numeric($data['pago_tarjeta'])){
                throw new Exception("Pago con tarjeta no válido");
            }
            if(!is_numeric($data['saldo'])){
                throw new Exception("Saldo no válido");
            }
            //Calculamos el total a pagar y el total pagado
            $data['importe_total'] = floatval($oComanda->importe_total);
            $data['total_a_pagar'] = floatval($data["importe_total"] - $data["descuento"]);
            $data["total_pagado"] = floatval($data["efectivo"] + $data["pago_tarjeta"]);
            
            // Obtenemos la serie y el correlativo para el comproobante
            $generalidades_id = $this->session->userdata('session_generalidades_id');
            $numeracion = $this->Numeracion_model->get_numeracion_comprobante($generalidades_id,$data['tipo_comprobante']);
            if(count($numeracion) == 0){
                throw new Exception("No se encontró correlativo válido para el comprobante.");   
            }
            $numeracion = $numeracion[0];
            $data['serie'] = $numeracion->serie;
            $data['correlativo']= $numeracion->correlativo;
            $this->set_datos_obj_pago($data);
            // Validamos: Pago cuando es con tarjeta se debe pagar exacto.
            if($data['pago_tarjeta'] > 0 && $data['vuelto'] > 0){
                throw new Exception(CG::M_PagoConTarjetaDebeSerExacto, 1);
            }
            //Verificar que se pague la totalidad
            if($data["total_pagado"] < $data['total_a_pagar']){
                throw new Exception(CG::M_SeDebePagarLaTotalidad, 1);
            }
            if($data['saldo'] != 0){
                throw new Exception(CG::M_SeDebePagarLaTotalidad, 1);
            }
            if($data["total_pagado"] >= $data['total_a_pagar']){
                //Se pago todo y debe darse vuelto
                $data["total_pagado"] = floatval($data['total_a_pagar']);
                $data["pago_efectivo"] = floatval($data["efectivo"] - $data["vuelto"]);
            }
            $data['saldo'] = floatval($data["importe_total"] - $data["descuento"] - $data["total_pagado"]);
            // Guardar los campos obtenidos y calculados            
            $this->form_validation->set_rules('total_a_pagar', "saldo ", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            
            if ($this->form_validation->run() == true) {
                //Insertar el pago
                $pago_id = $this->Pago_model->insert($data);
                if(!$pago_id){
                    throw new Exception("No se pudo registrar el pago. Comuníquese con el área de sistemas.");
                }else{
                    //Actualizar comanda a pagada
                    $data["pago_id"] = $pago_id;
                    $this->Comanda_model->pagar_comanda($data);
                    //Actualizar el correlativo
                    $correlativo_num = $numeracion->correlativo_num;
                    $correlativo_num++;
                    $new_correlativo = $this->Site->rellenar_izquierda($correlativo_num,'0',5);
                    $this->Numeracion_model->update_numeracion($generalidades_id, $data['tipo_comprobante'],$new_correlativo,$correlativo_num);

                    $this->session->unset_userdata('oPago');
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Pago registrado correctamente.");

                    $this->set_session_enviar_pago_nube($pago_id);

                    // redirect($_SERVER["HTTP_REFERER"]);
                    $url = base_url('salon/comanda/editar/').$data['comanda_id'];
                    redirect($url);
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    // function historial_ventas(){
    function comandas_facturadas(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Caja' ),
                    array('link' => '#', 'page' => 'Comandas facturadas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Comandas facturadas', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'comandas_facturadas' );
        $this->page_construct('facturacion/pagos/comandas_facturadas', $meta, null);
    }
    function cargar_tabla_ventas_by_fechas(){
        // Listado de ventas desde una fecha de inicio hasta una fecha de fin
        $start_date = $this->input->post('start_date');//Año-Mes-Dia
        $end_date = $this->input->post('end_date');
        // $data['oComanda'] = ($this->Comanda_model->get_one($data['comanda_id']))[0];
        $data['pagos'] = $this->Pago_model->get_all_by_fechas($start_date, $end_date);
        $this->partial_view('facturacion/pagos/_tabla_pagadas_by_fechas.php',$data);
    }
    function detalle_comandas_facturadas(){
        // $data['comandas'] = $this->Comanda_model->get_all_by_caja(1);
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Caja' ),
                    array('link' => '#', 'page' => 'Detalle de comandas facturadas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Detalle de comandas facturadas', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'comandas_facturadas' );
        $this->page_construct('facturacion/pagos/detalle_comandas_facturadas', $meta, null);
    }
    function cargar_tabla_ventas_detalle_by_fechas(){
        // Listado de detalle de ventas desde una fecha de inicio hasta una fecha de fin
        $start_date = $this->input->post('start_date');//Año-Mes-Dia
        $end_date = $this->input->post('end_date');
        // $data['oComanda'] = ($this->Comanda_model->get_one($data['comanda_id']))[0];
        $data['venta_detalle'] = $this->Pago_model->get_ventas_detalle_by_fechas($start_date, $end_date);
        $this->partial_view('facturacion/pagos/_tabla_pagadas_detalle_by_fechas',$data);
    }
    function exportar_ventas(){
        $this->loaders->verifica_sesion();
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $pagos = $this->Pago_model->get_all_by_fechas($start_date, $end_date);
        // echo json_encode($comandas);exit();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Historial de pagos");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("O{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("P{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("Q{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("R{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("S{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("T{$cont_excel}")->getFont()->setBold(true);

        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Cod. Pago-Comanda');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Fecha comanda');
        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Modalidad');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'Mozo');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'Mesa');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'N° comens');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'N° productos');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'Estado atención');
        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'Fecha de pago');
        $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", 'Importe_total');
        $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'Descuento');
        $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'Total a pagar');
        $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Pago efectivo');
        $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", 'Pago tarjeta');
        $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", 'Total pagado');
        $this->excel->getActiveSheet()->setCellValue("P{$cont_excel}", 'Saldo');        
        $this->excel->getActiveSheet()->setCellValue("Q{$cont_excel}", 'Cajero');
        $this->excel->getActiveSheet()->setCellValue("R{$cont_excel}", 'Comprob');
        $this->excel->getActiveSheet()->setCellValue("S{$cont_excel}", 'Cliente');
        $this->excel->getActiveSheet()->setCellValue("T{$cont_excel}", 'Estado');

        foreach ($pagos as $key => $pago) {
            $cont_excel++;
            $codigo = $pago->pago_id.' - '.$pago->comanda_id;
            // Estado de pago
            if ($pago->estado == 'A') $estado_desc = 'Activo';
            if ($pago->estado =='X') $estado_desc = 'Anulado';
            // Estado de atencioon
            if ($pago->estado =='E') $estado_atencion_desc = 'En espera';
            if ($pago->estado =='A') $estado_atencion_desc = 'Atendido';
            if ($pago->estado =='F') $estado_atencion_desc = 'Finalizado';
            if ($pago->estado =='X') $estado_atencion_desc = 'Anulado'; 
            // Modalidad
            if ($pago->modalidad =='PRE') $modalidad_desc = 'Presencial';
            if ($pago->modalidad =='DEL') $modalidad_desc = 'Delivery';
            if ($pago->modalidad =='LLE') $modalidad_desc = 'Para llevar';

            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $codigo);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $pago->fecha_comanda);
            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $modalidad_desc);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $pago->mozo);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", $pago->mesa);
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", $pago->nro_comensales);
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", $pago->nro_productos);
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", $estado_atencion_desc);
            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", $pago->fecha_pago);
            $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", $pago->importe_total);
            $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", $pago->descuento);
            $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", $pago->total_a_pagar );
            $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", $pago->pago_efectivo);
            $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", $pago->pago_tarjeta);
            $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", $pago->total_pagado);
            $this->excel->getActiveSheet()->setCellValue("P{$cont_excel}", $pago->saldo);
            $this->excel->getActiveSheet()->setCellValue("Q{$cont_excel}", $pago->cajero);
            $this->excel->getActiveSheet()->setCellValue("R{$cont_excel}", $pago->tipo_comprobante);
            $this->excel->getActiveSheet()->setCellValue("S{$cont_excel}", $pago->cliente_nombre);
            $this->excel->getActiveSheet()->setCellValue("T{$cont_excel}", $estado_desc);
            // FORMATO DE NUMERO DECIMAL
            $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("O{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("P{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
        }
        $archivo_excel = "Historia_de_ventas."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // $objWriter->save('./files/'.$archivo_excel);
        $objWriter->save('php://output');
    }
    function exportar_ventas_detalle(){
        $this->loaders->verifica_sesion();
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $venta_detalle = $this->Pago_model->get_ventas_detalle_by_fechas($start_date, $end_date);
        // echo json_encode($comandas);exit();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Historial de ventas detalle");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(5);

        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);

        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        // $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        // $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
        // $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);

        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);

        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("O{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("P{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("Q{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("O{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("P{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("Q{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("R{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("S{$cont_excel}")->getFont()->setBold(true);
        // $this->excel->getActiveSheet()->getStyle("T{$cont_excel}")->getFont()->setBold(true);

        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Cod. comanda');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Cod. pago');

        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Cliente');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'DNI/RUC');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'Tipo cliente');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'Teléfono');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'Correo');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'Fec. Cumpleaños');

        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'Cod. produc');
        $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", 'Modalidad');
        $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'Fecha');
        $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'Cajero');   
        $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Producto');
        $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", 'Precio unit.');
        $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", 'cantidad');
        $this->excel->getActiveSheet()->setCellValue("P{$cont_excel}", 'Importe');
        $this->excel->getActiveSheet()->setCellValue("Q{$cont_excel}", 'Estado item');
        // $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'Estado pago');
        // $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'Total a pagar');
        // $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Pago efectivo');
        // $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", 'Pago tarjeta');
        // $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", 'Total pagado');
        // $this->excel->getActiveSheet()->setCellValue("P{$cont_excel}", 'Saldo');        
        // $this->excel->getActiveSheet()->setCellValue("Q{$cont_excel}", 'Cajero');
        // $this->excel->getActiveSheet()->setCellValue("R{$cont_excel}", 'Comprob');
        // $this->excel->getActiveSheet()->setCellValue("S{$cont_excel}", 'Cliente');
        // $this->excel->getActiveSheet()->setCellValue("T{$cont_excel}", 'Estado');

        foreach ($venta_detalle as $key => $item) {
            $cont_excel++;
            if ($item->estado_atencion == 'E') $estado_atencion_desc = "En espera";
            if ($item->estado_atencion == 'D') $estado_atencion_desc = "Despachado";
            if ($item->estado_atencion == 'P') $estado_atencion_desc = "En proceso";
            if ($item->estado_atencion == 'X') $estado_atencion_desc = "Anulado";
            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $item->comanda_id);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $item->pago_id);

            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $item->cliente_nombre);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $item->cliente_documento);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", $item->cliente_tipo);
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", $item->cliente_telefono);
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", $item->cliente_correo);
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", $item->cliente_fecha_nacimiento);

            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", $item->producto_id);
            $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", $item->modalidad);
            $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", $item->fecha_pago);
            $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", $item->cajero);
            $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", $item->producto);
            $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", $item->precio_unitario);
            $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", $item->cantidad);
            $this->excel->getActiveSheet()->setCellValue("P{$cont_excel}", $item->importe);
            $this->excel->getActiveSheet()->setCellValue("Q{$cont_excel}", $estado_atencion_desc);
            // FORMATO DE NUMERO DECIMAL
            $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("O{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("P{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
        }
        $archivo_excel = "Historia_de_ventas_detalle."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        // $objWriter->save('./files/'.$archivo_excel);
        $objWriter->save('php://output');
    }
    function web_service_get_comanda_pago($comanda_id){
        try {
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificado, por favor comuníquese con el área de soporte.");
            }
            if(count( $this->Pago_model->get_one_by_comanda($comanda_id)) == 0){
                throw new Exception("Error, no se encontró el pago de la comanda especificada.");
            }
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $oPago = ($this->Pago_model->get_one($oComanda->pago_id))[0];
            // $oPago = ($this->Pago_model->get_one_by_comanda($comanda_id))[0];

            $data['oGeneralidades'] = ($this->Generalidades_model->get_one())[0];
            $data['oComanda'] = $oComanda;
            $data['oPago'] = $oPago;

            echo json_encode($data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }
}
