<?php defined('BASEPATH') or exit('No direct script access allowed');

class Resumen_diario extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Documento_electronico_model');
        $this->load->model('Resumen_diario_model');
        $this->load->model('facsu/Ws_facsu_model');
        $this->load->library('session');
    }

    function index(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Facturación' ),
                    array('link' => '#', 'page' => 'Comprobantes Electrónicos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Resúmenes diarios', 'bc' => $bc, 'modulo'=>'facturacion', 'item' => 'resumen_diario' );
        $this->page_construct('facturacion/resumen_diario/index', $meta, null);
    }

	function ajax_cargar_tabla_boletas_por_fecha(){
	    // Listado de boletas electrónicas para resumen diario
	    $date = $this->input->post('date'); //dia para generar resumen
	    $data['boletas_e'] = $this->Documento_electronico_model->get_boletas_CAB_by_fecha($date);
	    $this->partial_view('facturacion/resumen_diario/_tabla_boletas_para_nuevo_resumen',$data);
	}

	function ajax_cantidad_boletas_por_fecha(){
	    $date = $this->input->post('date'); 
	    $data['boletas_e'] = $this->Documento_electronico_model->get_boletas_CAB_by_fecha($date);

	    //Retornar respuesta
        $response['success'] = true;
        $response['nro_boletas'] = count($data['boletas_e']);
        echo json_encode($response);
	}

	function generar_resumen_diario(){
		//Obtener boletas
		$date = $this->input->post('date'); 
	    $lstBoletas = $this->Documento_electronico_model->get_boletas_CAB_by_fecha($date);
	    if(count($lstBoletas) <= 0){
			$response['success'] = false;
			$response['mensaje'] = "No se encontraron boletas para el resumen diario.";
			echo json_encode($response);
			exit();
	    }

		//Verificar que los documentos no estén en otro resumen diario
		$existeEnOtroResumen  = false;
		foreach ($lstBoletas as $key => $boleta) {
			$idDocResumen = $boleta->serie.'-'.$boleta->correlativo;
			$otroResumen = $this->Resumen_diario_model->verificaExistenciaDeDocumentoEnResumen($idDocResumen);
			if(count($otroResumen) > 0){
				$existeEnOtroResumen  = true;
				break;
			}
		}

		if($existeEnOtroResumen){
			$response['success'] = false;
			$response['mensaje'] = "Las boleta ".$idDocResumen." se encuentra en otro resumen diario.";
			echo json_encode($response);
			exit();
		}

		//Asignamos data para la cabecera del resumen
		$dataResumen['fecEmision'] = date("Y/m/d H:i:s");
		$dataResumen['fecResumen'] = $date;
		$dataResumen['nroBoletas'] = count($lstBoletas);
		$dataResumen['sumValorVenta'] = 0;
		$dataResumen['sumTributos'] = 0;
		$dataResumen['sumTotalVenta'] = 0;
		$dataResumen['ticket'] = "";
		$dataResumen['estado'] = "G";
		$dataResumen['fecha_registro'] = date("Y/m/d H:i:s");
		$dataResumen['registrado_por'] = "";
		$valorVenta = 0;
		$totalTributos = 0;
		$totalVenta = 0;

		//Analizar los comprobantes electronicos
		foreach ($lstBoletas as $key => $boleta) {
			$valorVenta += $boleta->sumTotValVenta;
			$totalTributos += $boleta->sumTotTributos;
			$totalVenta += $boleta->sumPrecioVenta;

			//Asignar datos para el detalle del resumen
			// $dataResumenDetalle['resumen_id'] = "";
			$dataResumenDetalle[$key]['fecEmision'] = date("Y-m-d"); /* 1*/
			$dataResumenDetalle[$key]['fecResumen'] = $date; /* 2 */
			$dataResumenDetalle[$key]['tipDocResumen'] = $boleta->tipoComprobante; /* 3 */
			$dataResumenDetalle[$key]['idDocResumen'] = $boleta->serie.'-'.$boleta->correlativo;
			$dataResumenDetalle[$key]['tipDocUsuario'] = $boleta->tipDocUsuario; /* 5 */
			$dataResumenDetalle[$key]['numDocUsuario'] = $boleta->numDocUsuario;
			$dataResumenDetalle[$key]['tipMoneda'] = $boleta->tipMoneda;
			$dataResumenDetalle[$key]['totValGrabado'] = $boleta->sumTotValVenta;
			$dataResumenDetalle[$key]['totValExoneado'] = 0;
			$dataResumenDetalle[$key]['totValInafecto'] = 0; /* 10 */
			$dataResumenDetalle[$key]['totValExportado'] = 0;
			$dataResumenDetalle[$key]['totValGratuito'] = 0;
			$dataResumenDetalle[$key]['totOtroCargo'] = 0;
			$dataResumenDetalle[$key]['totImpCpe'] = $boleta->sumPrecioVenta;
			$dataResumenDetalle[$key]['tipDocModifico'] = ""; /* 15 */
			$dataResumenDetalle[$key]['serDocModifico'] = "";
			$dataResumenDetalle[$key]['numDocModifico'] = "";
			$dataResumenDetalle[$key]['tipRegPercepcion'] = ""; /* 18 */
			$dataResumenDetalle[$key]['porPercepcion'] = "";
			$dataResumenDetalle[$key]['monBasePercepcion'] = 0; /* 20 */
			$dataResumenDetalle[$key]['monPercepcion'] = 0;
			$dataResumenDetalle[$key]['monTotIncPercepcion'] = 0;
			$dataResumenDetalle[$key]['tipEstado'] = "1";

			$dataResumenDetalle[$key]['fecha_registro'] = "";
			$dataResumenDetalle[$key]['registrado_por'] = "";
			$dataResumenDetalle[$key]['fecha_modificacion'] = "";
			$dataResumenDetalle[$key]['modificado_por'] = "";
		}

		//Asignar montos totales
		$dataResumen['sumValorVenta'] = $valorVenta;
		$dataResumen['sumTributos'] = $totalTributos;
		$dataResumen['sumTotalVenta'] = $totalVenta;

		//insertar resumen y detalle
		$this->Resumen_diario_model->insert($dataResumen, $dataResumenDetalle);

		//Respuesta exitosa
		$response['success'] = true;
		$response['mensaje'] = "Resumen diario registrado correctamente.";
		echo json_encode($response);
		exit();
	}

	function ajax_tabla_obtener_resumenes(){
	    //Listado de rsúmenes diarios
	    $anio = $this->input->post('anio');
	    $mes = $this->input->post('mes');

	    $data['resumenes'] = $this->Resumen_diario_model->get_resumenes_por_mes_anio($anio, $mes);
	    $this->partial_view('facturacion/resumen_diario/_tabla_resumenes',$data);
	}

	 function ajax_cargar_modal_resumen($resumen_id){
        $data['generalidades'] = ($this->Site->get_generalidades())[0];
        $data['resumen'] = ($this->Resumen_diario_model->get_resumen_cab_por_id($resumen_id))[0];
        $data['detalle'] = $this->Resumen_diario_model->get_resumen_det_por_resumen($resumen_id);
        $this->partial_view('facturacion/resumen_diario/_modal_resumen',$data);
    }


	//Web service
	//Una venta adicional para enviar el comprobante electrònico, se debe cerrar cuando termina de enviar
    public  function popup_enviar_resumen_a_facsu($resumen_id){
        $oGeneralidades = $this->Site->get_generalidades();
        $oResumen = $this->Resumen_diario_model->get_resumen_cab_por_id($resumen_id);
        $items = $this->Resumen_diario_model->get_resumen_det_por_resumen($resumen_id);

        if(count($oGeneralidades) == 1 && count($oResumen) == 1 && count($items) > 0){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oResumen'] = $oResumen[0];
            $datos['items'] = $items;

            $rpta = $this->Ws_facsu_model->enviar_resumen_diario($datos);
            if(!$rpta['success']){
                $this->Site->registrar_log_error('A','Controler-Resumen_diario',$rpta['message']);
                echo $rpta['message'];
            }
            else{
                echo $rpta['message'];
            }
        }else{
            $log_mensaje = "No se envió el resumen diario al sistema de facturación FACSU";
            $this->Site->registrar_log_error('A','Controler-Resumen_diario',$log_mensaje);
        }
    }
}









