<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acceso extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('Acceso_model');
        $this->load->model('Modulo_model');
        $this->load->model('Opcion_model');
        $this->load->model('Generalidades_model');
        $this->load->library('session');
    }
	public function iniciar_sesion(){
        $data = array(  'usuario' => $this->input->post('usuario'),
                        'clave' => md5($this->input->post('clave')),
                    );
        
        $usuario = $this->Acceso_model->get_usuario($data);
        $oGeneralidades = ($this->Generalidades_model->get_one())[0];
        $data['oGeneralidades'] = $oGeneralidades;

        //Validar usuario
        if (count($usuario) != 1){
            $data['success'] = false;
            $data['mensaje'] = "Los datos ingresados no son válidos.";
            $data['page_title'] = 'Iniciar sesión';
            $this->load->view('seguridad/acceso/login',$data);
            return;
        }
        if($usuario[0]->estado == 'X'){
            $data['success'] = false;
            $data['mensaje'] = "El usuario está bloqueado.";
            $data['page_title'] = 'Iniciar sesión';
            $this->load->view('seguridad/acceso/login',$data);
            return;
        }

        //Usuario logeado corretamente
        $usuario_data['session_usuario_id'] = $usuario[0]->usuario_id;
        $usuario_data['session_empleado_id'] = $usuario[0]->empleado_id;
        $usuario_data['session_usuario'] = $usuario[0]->usuario;
        $usuario_data['session_clave'] = $usuario[0]->clave;
        $usuario_data['session_rol_id'] = $usuario[0]->rol_id;
        $usuario_data['session_rol'] = $usuario[0]->rol;
        $usuario_data['session_nombre_completo'] = $usuario[0]->nombre_completo;
        $usuario_data['session_mod_access'] = $this->Modulo_model->get_all_by_rol($usuario[0]->rol_id);
        $usuario_data['session_opc_access'] = $this->Opcion_model->get_all_by_rol($usuario[0]->rol_id);

        //Obtener generalidades
        $usuario_data['session_generalidades'] = $oGeneralidades;
        $usuario_data['session_generalidades_id'] = $oGeneralidades->generalidades_id;

        // iniciamos la session y guardamos los datos
        $this->loaders->iniciar_sesion($usuario_data);
        // $this->session->set_userdata($usuario_data);
        // echo $usuario_data['SES_Id_Medico'];
        if ($this->session->userdata('session_rol_id') == 1) redirect(base_url('main/panel'));  //ADMIN
        if ($this->session->userdata('session_rol_id') == 2) redirect(base_url('salon/salon'));  //CAJERO
        if ($this->session->userdata('session_rol_id') == 3) redirect(base_url('cocina/cocina'));  //COCINERO
        if ($this->session->userdata('session_rol_id') == 4) redirect(base_url('salon/salon'));  //MOZO
        redirect(base_url('main/panel'));
	}
	public function cerrar_sesion(){
  		$this->loaders->cerrar_sesion();
 	}
}
