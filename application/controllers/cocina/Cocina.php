<?php defined('BASEPATH') or exit('No direct script access allowed');

class Cocina extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Comanda_model');
        $this->load->model('Mesa_model');
        $this->load->model('Producto_model');
        $this->load->model('Caja_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
        $this->Caja_model->verificar_caja_aperturada();
    }
    function index(){
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        $data['items_espera'] = $this->Comanda_model->get_items_para_cocina_by_caja($oCaja->caja_id,'E');
        $data['items_prepa'] = $this->Comanda_model->get_items_para_cocina_by_caja($oCaja->caja_id,'P');
        $data['max_pendiente_cocina'] = $this->Comanda_model->max_pendiente_cocina($oCaja->caja_id);
        $data['nro_pedidos_anulados'] = $this->Comanda_model->nro_pedidos_anulados($oCaja->caja_id);
        // echo json_encode($data['items']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Cocina' ),
                    array('link' => '#', 'page' => 'Pedidos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Cocina - Pedidos', 'bc' => $bc, 'modulo'=>'cocina', 'item' => 'pedidos' );
        // echo 1;
        $this->page_construct('cocina/cocina/index', $meta, $data);
    }
    function consultas_valores_time(){
        // Devuelve el id del ultimo Item, con la finalidad de compare si se ha registrado uno nuevo
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        $data['max_pendiente_cocina'] = $this->Comanda_model->max_pendiente_cocina($oCaja->caja_id);
        $data['nro_pedidos_anulados'] = $this->Comanda_model->nro_pedidos_anulados($oCaja->caja_id);
        echo json_encode($data);
    }
    function cargar_tabla_pendientes_ajax(){
        // Cada ves que hay uno pedido nuevo, cargará mediantes ajax esa lista
        $oCaja = ($this->Caja_model->get_aperturada())[0];

        $data['items_espera'] = $this->Comanda_model->get_items_para_cocina_by_caja($oCaja->caja_id,'E');
        $this->partial_view('cocina/cocina/_tabla_pendientes', $data);
    }
    function pedidos_del_dia(){
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        $data['items'] = $this->Comanda_model->get_items_para_cocina_by_caja($oCaja->caja_id);
        // echo json_encode($data['items']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Cocina' ),
                    array('link' => '#', 'page' => 'Pedidos del día' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Cocina - Pedidos del día', 'bc' => $bc, 'modulo'=>'cocina', 'item' => 'pedidos_del_dia' );
        // echo 1;
        $this->page_construct('cocina/cocina/pedidos_del_dia', $meta, $data);       
    }
    function preparar_pedido($comanda_item_id){
        $this->loaders->verifica_sesion();
        $item = ($this->Comanda_model->get_item_by_comanda($comanda_item_id))[0];
        $producto = ($this->Producto_model->get_one($item->producto_id))[0];
        // echo json_encode($item);exit();
        // $producto = ($this->Producto_model->get_one($producto_id))[0];
        if( $this->Comanda_model->preparar_pedido($comanda_item_id) ){
            $this->session->set_flashdata('success', true);
            $mensaje = "El pedido: ".$producto->producto." está en preparación";
            $this->session->set_flashdata('message', $mensaje);
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para preparar el pedido.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function despachar_pedido($comanda_item_id){
        $this->loaders->verifica_sesion();
        // OBTENGO EL ITEM
        $item = ($this->Comanda_model->get_item_by_comanda($comanda_item_id))[0];
        // OBTENGO EL PRODUCTO
        $oProducto = ($this->Producto_model->get_one($item->producto_id))[0];
        // Obtengo la comanda
        $oComanda = ($this->Comanda_model->get_one($item->comanda_id))[0];
        // echo json_encode($oComanda);exit();
        //obtenemos todos los items de la comanda
        $items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
        // echo json_encode($items);exit();
        // Calcular los minutos que pasaron hasta ser atendido
        $min_espera = $this->Site->minutos_transcurridos($item->pedido_en,date("Y-m-d H:i:s") );
        // Nuevos datos
        $nueva_data = array(
                            'comanda_item_id' => $comanda_item_id,
                            'estado_atencion' => 'D',
                            'despachado_en' => date("Y/m/d H:i:s"),
                            'despachado_por' => $this->session->userdata('session_usuario'),
                            'tiempo_espera' => $min_espera,
                            );
        // echo json_encode($nueva_data);exit();
        if( $this->Comanda_model->despachar_pedido($nueva_data) ){
            // Verificar si fue despachado toda la comanda
            $sin_despachar = 0;
            foreach ($items as $key => $item2) {
                if($item2->comanda_item_id != $item->comanda_item_id){
                    if($item2->estado_atencion == 'E' || $item2->estado_atencion == 'P'){
                        $sin_despachar++;
                    }    
                }
            }
            if($sin_despachar == 0){
                // Todos los productos(items) fueron despachados
                //Modificamos la comanda
                $this->Comanda_model->comanda_atendida($item->comanda_id);
                // Modificamos la mesa
                if(!is_null($oComanda->mesa_id)){
                    $this->Mesa_model->cambiar_estado($oComanda->mesa_id, 'A');
                    $mensaje = "El pedido: ".$oProducto->producto." fue despachado.<br> <strong>No hay pedidos pendientes para esta mesa: ".$oComanda->mesa.".</strong>";
                }else{
                    $mensaje = "El pedido: ".$oProducto->producto." fue despachado.<br> <strong>No hay pedidos pendientes para la comanda.</strong>";
                }
                
            }else{
                $mensaje = "El pedido: ".$oProducto->producto." fue despachado.";    
            }
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', $mensaje);
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para decpachar el pedido.');
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }
    function regresar_a_pendiente($comanda_item_id){
        if( $this->Comanda_model->regresar_a_pendiente($comanda_item_id) ){
            $this->session->set_flashdata('success', true);
            $mensaje = "El producto regresó a estado pendiente.";
            $this->session->set_flashdata('message', $mensaje);
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para regresar el pedido a espera.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
}
