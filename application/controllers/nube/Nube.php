<?php defined('BASEPATH') or exit('No direct script access allowed');

class Nube extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Comanda_model');
        $this->load->model('Documento_electronico_model');
        $this->load->model('Pago_model');
        $this->load->model('Caja_model');
        $this->load->model('Multitabla_model');
        $this->load->model('nube/Ws_comanda_model');
        $this->load->model('nube/Ws_comprobante_model');
        $this->load->model('nube/Ws_pago_model');
        $this->load->model('nube/Ws_caja_model');
        
        //Asistencia
        $this->load->model('asistencia/Marcacion_model');
        $this->load->model('asistencia/Marcacion_sync_model');
        $this->load->model('nube/Ws_marcaciones_model');


        $this->load->library('session');
        
        // $this->Caja_model->verificar_caja_aperturada();
    }
    
    public  function ajax_popup_enviar_comanda_nube(){

        //Parametros
        $comanda_id   = (!isset($_REQUEST['comanda_id'])) ? ""  : trim($_REQUEST['comanda_id']);

        //obtener valores
        $oGeneralidades = $this->Site->get_generalidades();
        $buscarComanda = $this->Comanda_model->get_one($comanda_id);

        if(count($buscarComanda) <= 0){
            echo "Errror - Comanda no existe.";
            exit();
        }

        //Buscar comanda
        $oComanda = $buscarComanda[0];
        $oComanda->items = $this->Comanda_model->get_items_by_comanda($comanda_id);

        //Buscar caja
        $buscarCaja = $this->Caja_model->get_one($oComanda->caja_id);
        $oCaja = $buscarCaja[0];

        if(count($oGeneralidades) == 1){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oComanda'] = $oComanda;
            $datos['oCaja'] = $oCaja;
            
            $rpta = $this->Ws_comanda_model->enviar_comanda($datos);

            if(!$rpta['success']){
                $this->Site->registrar_log_error('A','Controler-Nube',$rpta['message']);
                echo $rpta['message'];
            }else{
                echo "Comanda enviada correctamente.";
                echo "<script type='text/javascript'>";
                // echo "window.close();";
                echo "</script>";
            }
        }else{
            $log_mensaje = "No se envió el documento electrónico a la NUBE SANTORINI.";
            $this->Site->registrar_log_error('A','Controler-Nube',$log_mensaje);
        }
    }

    public  function ajax_popup_enviar_comprobante_nube(){

        //Parametros
        $cabecera_id   = (!isset($_REQUEST['cabecera_id'])) ? ""  : trim($_REQUEST['cabecera_id']);

        //obtener valores
        $oGeneralidades = $this->Site->get_generalidades();
        $buscarComprobante = $this->Documento_electronico_model->get_cpbe_cab($cabecera_id);

        if(count($buscarComprobante) <= 0){
            echo "Error - Comprobante no existe.";
            exit();
        }

        //buscar comprobante
        $oComprobante = $buscarComprobante[0];
        $oComprobante->items = $this->Documento_electronico_model->get_cpbe_det_by_cab($cabecera_id);

        if(count($oGeneralidades) == 1){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oComprobante'] = $oComprobante;
            
            $rpta = $this->Ws_comprobante_model->enviar_comprobante($datos);

            if(!$rpta['success']){
                $this->Site->registrar_log_error('A','Controler-Nube',$rpta['message']);
                echo $rpta['message'];
            }else{
                echo "Comprobante enviado correctamente.";
                echo "<script type='text/javascript'>";
                // echo "window.close();";
                echo "</script>";
            }
        }else{
            $log_mensaje = "No se envió el Comrpobante a la NUBE SANTORINI.";
            $this->Site->registrar_log_error('A','Controler-Nube',$log_mensaje);
        }
    }

    public  function ajax_popup_enviar_pago_nube(){

        //Parametros
        $pago_id   = (!isset($_REQUEST['pago_id'])) ? ""  : trim($_REQUEST['pago_id']);

        //obtener valores
        $oGeneralidades = $this->Site->get_generalidades();
        $buscarPago = $this->Pago_model->get_one($pago_id);

        if(count($buscarPago) <= 0){
            echo "Error - Pago no existe.";
            exit();
        }
        $oPago = $buscarPago[0];

        //Buscar caja
        $buscarCaja = $this->Caja_model->get_one($oPago->caja_id);
        $oCaja = $buscarCaja[0];

        if(count($oGeneralidades) == 1){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oPago'] = $oPago;
            $datos['oCaja'] = $oCaja;
            
            $rpta = $this->Ws_pago_model->enviar_pago($datos);

            if(!$rpta['success']){
                $this->Site->registrar_log_error('A','Controler-Nube',$rpta['message']);
                echo $rpta['message'];
            }else{
                echo "Pago enviado correctamente.";
                echo "<script type='text/javascript'>";
                // echo "window.close();";
                echo "</script>";
            }
        }else{
            $log_mensaje = "No se envió el Pago a la NUBE SANTORINI.";
            $this->Site->registrar_log_error('A','Controler-Nube',$log_mensaje);
        }
    }

    public function ajax_popup_enviar_caja_nube(){
        //Parametros
        $caja_id   = (!isset($_REQUEST['caja_id'])) ? ""  : trim($_REQUEST['caja_id']);

        //obtener valores
        $oGeneralidades = $this->Site->get_generalidades();
        $buscarCaja = $this->Caja_model->get_one($caja_id);

        if(count($buscarCaja) <= 0){
            echo "Error - Caja no existe.";
            exit();
        }
        $oCaja = $buscarCaja[0];

        if(count($oGeneralidades) == 1){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oCaja'] = $oCaja;
            
            $rpta = $this->Ws_caja_model->enviar_caja($datos);

            if(!$rpta['success']){
                $this->Site->registrar_log_error('A','Controler-Nube',$rpta['message']);
                echo $rpta['message'];
            }else{
                echo "Caja enviada correctamente.";
                echo "<script type='text/javascript'>";
                // echo "window.close();";
                echo "</script>";
            }
        }else{
            $log_mensaje = "No se envió Caja a la NUBE SANTORINI.";
            $this->Site->registrar_log_error('A','Controler-Nube',$log_mensaje);
        }
    }

    //Método se llama desde Cerrar caja, no es Ajax ni popup
    public function enviar_marcaciones_nube(){

        $caja_id   = (!isset($_REQUEST['caja_id'])) ? ""  : trim($_REQUEST['caja_id']);

        //Validar cuando NO SE DEBE ENVIAR
        $var_sync_marcaciones = intval($this->obtener_variable('sync_marcaciones'));
        if($var_sync_marcaciones == 0){
            $this->session->set_flashdata('success', true);
            $mensaje = '<strong> Operación exitosa.</strong>.';
            $this->session->set_flashdata('message', $mensaje);
            $this->Caja_model->update_sync_marcaciones($caja_id);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //obtener MARCACIONES
        $lstMarcaciones = $this->Marcacion_model->obtener_marcaciones_para_nube();
        if(count($lstMarcaciones) <= 0){
            $this->session->set_flashdata('success', true);
            $mensaje = '<strong> Operación exitosa.</strong> No hay marcaciones pendientes de migración.';
            $this->session->set_flashdata('message', $mensaje);
            $this->Caja_model->update_sync_marcaciones($caja_id);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $datos['lstMarcaciones'] = $lstMarcaciones;
        $rpta = $this->Ws_marcaciones_model->enviar_marcaciones($datos);

        if(!$rpta['success']){
            $this->Site->registrar_log_error('A','Controler-Nube',$rpta['message']);
            $this->session->set_flashdata('success', false);
            $mensaje = $rpta['message'];
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Registrar sincronización
        $this->registrar_sync_marcaciones($lstMarcaciones);
        $this->Caja_model->update_sync_marcaciones($caja_id);

        //Devolver mensaje exitoso
        $this->session->set_flashdata('success', true);
        $mensaje = '<strong> Operación exitosa.</strong>.';
        $this->session->set_flashdata('message', $mensaje);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function registrar_sync_marcaciones($lstMarcaciones){
        foreach ($lstMarcaciones as $key => $oMarcacion) {
            $sync_guardar['transaction_id'] = $oMarcacion->transaction_id;
            $sync_guardar['sync_time'] = date("Y/m/d H:i:s");
            $this->Marcacion_sync_model->insert_sync($sync_guardar);
        }
    }
}






