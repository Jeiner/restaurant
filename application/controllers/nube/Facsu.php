<?php defined('BASEPATH') or exit('No direct script access allowed');

class Facsu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mesa_model');
        $this->load->model('Union_mesa_model');
        $this->load->model('Producto_model');
        $this->load->model('Comanda_model');
        $this->load->model('Documento_electronico_model');
        $this->load->model('Pago_model');
        $this->load->model('Caja_model');
        $this->load->model('Multitabla_model');
        $this->load->model('nube/Ws_comanda_model');
        $this->load->model('nube/Ws_comprobante_model');
        $this->load->model('nube/Ws_pago_model');
        //Asistencia
        $this->load->model('asistencia/Marcacion_model');
        $this->load->model('asistencia/Marcacion_sync_model');
        $this->load->model('nube/Ws_marcaciones_model');
        $this->load->model('facsu/Ws_facsu_model');


        $this->load->library('session');
        
        // $this->Caja_model->verificar_caja_aperturada();
    }
    
    public function procesar_cpbes_facsu(){

        $caja_id   = (!isset($_REQUEST['caja_id'])) ? ""  : trim($_REQUEST['caja_id']);
        $lstErrores = [];

        if($caja_id == null || $caja_id == ""){
            $mensaje = "Debe indicar el código de caja.";
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Obtener Generalidades
        $oGeneralidades = $this->Site->get_generalidades()[0];
        if($oGeneralidades->emitir_cpbe == 0){
            $mensaje = '<strong> Procesamiento exitoso.</strong> El sistema no tiene configurado emitir Cpbe.';
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', $mensaje);
            $this->Caja_model->update_sync_procesamiento_cpbe($caja_id);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Obtener comprobantes electrónicos a procesar
        $lstCpbes = $this->Documento_electronico_model->get_boletas_CAB_para_procesar();
        if($lstCpbes == null || count($lstCpbes) <= 0){
            $mensaje = "<strong> Procesamiento exitoso.</strong> No existen comprobantes electrónicos a procesar.";
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', $mensaje);
            $this->abrir_popup_procesar_cpbe();
            $this->Caja_model->update_sync_procesamiento_cpbe($caja_id);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Consultar en FACSU los comprobantes
        $rpta = $this->Ws_facsu_model->consultar_estado_cpbes_batch($lstCpbes);

        if(!$rpta['success']){
            $this->Site->registrar_log_error('A','Controler-facsu',$rpta['message']);
            $mensaje = "<strong> FACSU: </strong> ".$rpta['message'];
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Ver comporbantes de Facsu
        $lstCpbesFacsu = $rpta['data'];
        $lstCpbesFacsu = ($lstCpbesFacsu == null)? [] : $lstCpbesFacsu;

        $lstErroresCriticos = $this->buscar_errores_criticos($lstCpbes, $lstCpbesFacsu);

        //Actualizar estado de compobantes correctos (Aceptados por Sunat)
        $this->actualizar_estado_cpbe_por_sunat($lstCpbes);

        //para popup procesar comporbantes electrónicos en facsu //Ver acciones sessión
        $this->abrir_popup_procesar_cpbe();

        //Responder servicio
        if(count($lstErroresCriticos) <> 0){
            $mensaje = "";

            foreach ($lstErroresCriticos as $key => $oError) {
                $mensaje .= "".$oError."<br>";
            }

            $this->Site->registrar_log_error('A','Controler-facsu',$rpta['message']);
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }else{
            $mensaje = '<strong> Operación exitosa.</strong>.';
            $this->Caja_model->update_sync_procesamiento_cpbe($caja_id);
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function abrir_popup_procesar_cpbe(){
        //para popup procesar comporbantes electrónicos en facsu //Ver acciones sessión
        $this->session->set_userdata('procesar_cpbes_facsu',true);
    }

    private function actualizar_estado_cpbe_por_sunat($lstCpbes){
        foreach ($lstCpbes as $key => $oComprobante) {
            if($oComprobante->estadoSunatFacsu != null && $oComprobante->estadoSunatFacsu != ""){
                //Asigna estado
                $this->Documento_electronico_model->update_cpbe_cab_actualiza_estado($oComprobante->cabecera_id, $oComprobante->estadoSunatFacsu);
            }
        }
    }

    private function buscar_errores_criticos($lstCpbes, $lstCpbesFacsu){

        $lstErroresCriticos = [];

        foreach ($lstCpbes as $key => $oComprobante) {

            //Validar si comprobantes existe en Facsu
            $oComprobante->existe = false;
            $oComprobante->estadoSunatFacsu = "";
            $oComprobante->estadoSunatDesc = "";
            foreach ($lstCpbesFacsu as $key => $oComprobanteFacsu) {

                if($oComprobante->serie     == $oComprobanteFacsu->cSerie &&
                   $oComprobante->correlativo   == $oComprobanteFacsu->cCorrel &&
                   $oComprobante->fecEmision    == $oComprobanteFacsu->cFecEmi &&
                   $oComprobante->sumImpVenta   == $oComprobanteFacsu->nImpVen ){

                    $oComprobante->existe = true;
                    $oComprobante->estadoSunatFacsu = $oComprobanteFacsu->cIndSit;
                    $oComprobante->estadoSunatDesc = $oComprobanteFacsu->cIndSitDes;
                }
            }

            //Errores
            $numDoc = $oComprobante->serie."-".$oComprobante->correlativo;

            //Error por que no existe
            if(!$oComprobante->existe){
                $lstErroresCriticos[] = "El comprobante ".$numDoc." no se encontró en Facsu. Debe enviarse desde la sección 'Documentos electrónicos'.";
                continue;
            }

            //Error por estado rechazado u otro
            if($oComprobante->estadoSunatFacsu == "01" ||
                $oComprobante->estadoSunatFacsu == "02" ||
                $oComprobante->estadoSunatFacsu == "05" ||
                $oComprobante->estadoSunatFacsu == "06" ||
                $oComprobante->estadoSunatFacsu == "07" ||
                $oComprobante->estadoSunatFacsu == "08" ||
                $oComprobante->estadoSunatFacsu == "10"){

                // $lstErroresCriticos[] = "El comprobante ".$numDoc." tiene estado: ".$oComprobante->estadoSunatDesc.".";
                continue;
            }
        }

        return $lstErroresCriticos;
    }

}






