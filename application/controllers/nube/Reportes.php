<?php defined('BASEPATH') or exit('No direct script access allowed');

class Reportes extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('nube/Nube_reportes_model');
        $this->load->model('Producto_model');
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
        // $this->Caja_model->verificar_caja_aperturada();
    }

    // DATOS
    function nube_ventas_diarias(){
        $data['mes'] = $this->input->post('mes');
        $data['anio'] = $this->input->post('anio');
        $result = $this->Nube_reportes_model->ventas_diarias($data);
        echo json_encode($result);
    }
}
