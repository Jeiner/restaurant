<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pagos_programados extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Generalidades_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Proveedor_model');
        $this->load->model('Sede_model');
        $this->load->model('Pago_programado_model');
        $this->load->model('Pago_servicio_model');
        
        $this->load->library('session');
        $this->loaders->verifica_sesion();
    }
    function index(){
        try {
            // $data['pagos_programados'] = $this->Pago_programado_model->get_all();  //Listar todos los pagos programados
            $data['sedes'] = $this->Sede_model->get_all();  //Listar todos los proveedores
            $data['estados'] = $this->Multitabla_model->get_all_by_tipo(14); 
            // echo json_encode($data['pagos_programados']);exit();
            $bc = array(
                array('link' => base_url(), 'page' => 'Inicio'),
                array('link' => '#', 'page' => 'Contabilidad' ),
                array('link' => '#', 'page' => 'Pagos programados' ),
            ); //breadcrumbs 
            $meta = array( 'page_title' => 'Pagos programados', 'bc' => $bc, 'modulo'=>'contabilidad', 'item' => 'pagos_programados' );
            // $this->load->view('contabilidad/pagos_programados/nuevo',$data);
            $this->page_construct('contabilidad/pagos_programados/index', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('contabilidad/pagos_programados'));
        }
    }
    function ajax_cargar_tabla_programados(){
        $data['anual'] = $this->input->post('anual');//Año-Mes-Dia
        $data['mes_id'] = $this->input->post('mes_id');
        $data['sede_id'] = ($this->input->post('sede_id') == '')? "0" : $this->input->post('sede_id');
        $data['estado'] = ($this->input->post('estado') == '')? "0" : $this->input->post('estado');
        $data['pagos_programados'] = $this->Pago_programado_model->get_all_con_filtros($data['anual'], $data['mes_id'], $data['sede_id'], $data['estado']);
        $this->partial_view('contabilidad/pagos_programados/tabla_programados',$data);
    }
    function ajax_ver_pago_programado(){
        $pago_programado_id = $this->input->post('pago_programado_id');
        $data['oPago_programado'] = ($this->Pago_programado_model->get_one($pago_programado_id))[0];
        $this->partial_view('contabilidad/pagos_programados/_modal_ver_pago',$data);
    }
    function pagar_programado($pago_programado_id = 0){
        try {
            if (count($this->Pago_programado_model->get_one($pago_programado_id)) <= 0) {
                throw new Exception("No se encontró el pago programado.", 1);
            }
            $data['oPago_programado'] = ($this->Pago_programado_model->get_one($pago_programado_id))[0];
            $data['modos_pago'] = $this->Multitabla_model->get_all_by_tipo(15);;

            $bc = array(
                array('link' => base_url(), 'page' => 'Inicio'),
                array('link' => '#', 'page' => 'Contabilidad' ),
                array('link' => '#', 'page' => 'Pagar pago programado' ),
            ); //breadcrumbs 
            $meta = array( 'page_title' => 'Pagar pago programado', 'bc' => $bc, 'modulo'=>'contabilidad', 'item' => 'pagos_programados' );
            // $this->load->view('contabilidad/pagos_programados/nuevo',$data);
            $this->page_construct('contabilidad/pagos_programados/pagar_programado', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('contabilidad/pagos_programados'));
        }
    }
    function guardar_pago_de_programado(){
        try {
            $this->loaders->verifica_sesion();
            $data = array(  
                        'pago_programado_id' => $this->input->post('pago_programado_id'),
                        'sede_id' => $this->input->post('sede_id'),
                        'proveedor_id' => $this->input->post('proveedor_id'),
                        'descripcion' => $this->input->post('descripcion'),
                        'fecha_pago' => date("Y/m/d H:i:s"),
                        'modo_pago' => $this->input->post('modo_pago'),
                        'monto_pagado' => $this->input->post('monto_pagado'),
                        'observaciones' => $this->input->post('observaciones'),
                        'estado' => "A",
                    );
            $oPago_programado = ($this->Pago_programado_model->get_one($this->input->post('pago_programado_id')))[0];
            if($oPago_programado->estado == 'PAG'){
                throw new Exception("El pago programado ya está pagado", 1);
            }
            if($oPago_programado->estado == 'X'){
                throw new Exception("El pago programado esta anulado", 1);
            }
            if($this->Pago_servicio_model->insert($data)){
                $this->Pago_programado_model->cambiar_a_pagado($this->input->post('pago_programado_id'), $data['monto_pagado']);
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Pago registrado correctamente.");
                redirect(base_url('contabilidad/pagos_programados'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('contabilidad/pagos_programados'));
        }
    }
    function nuevo(){
        try {
            $data['proveedores'] = $this->Proveedor_model->get_all();  //Listar todos los proveedores
            $data['sedes'] = $this->Sede_model->get_all();  //Listar todos los sedes

            $bc = array(
                array('link' => base_url(), 'page' => 'Inicio'),
                array('link' => '#', 'page' => 'Contabilidad' ),
                array('link' => '#', 'page' => 'Programar pagos' ),
            ); //breadcrumbs 
            $meta = array( 'page_title' => 'Programar pagos', 'bc' => $bc, 'modulo'=>'contabilidad', 'item' => 'pagos_programados' );
            // $this->load->view('contabilidad/pagos_programados/nuevo',$data);
            $this->page_construct('contabilidad/pagos_programados/nuevo', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('contabilidad/pagos_programados'));
        }
    }
    function guardar(){  //Guardar un nuevo cronograma
        $this->loaders->verifica_sesion();
        $data = array(  
                    'anual' => $this->input->post('anual'),
                    'sede_id' => $this->input->post('sede_id'),
                    'proveedor_id' => $this->input->post('proveedor_id'),
                    'descripcion' => $this->input->post('descripcion'),
                    'monto_previsto' => $this->input->post('monto_previsto'),
                    'dia_alerta' => $this->input->post('dia_alerta'),
                    'observaciones' => $this->input->post('observaciones'),
                );
        $nro_meses = isset($_POST['mes_id']) ? sizeof($_POST['mes_id']) : 0; //Productos o insumos)
        try {
            if( $nro_meses == 0 ){
                throw new Exception("Debe seleccionar por lo menos un mes.");
            }
            for ($r = 0; $r < $nro_meses; $r++){
                $oPago_programado['sede_id'] = $data['sede_id'];
                $oPago_programado['anual'] = $data['anual'];
                $oPago_programado['mes'] = $_POST['mes_id'][$r];
                $oPago_programado['dia_alerta'] = ($data['dia_alerta'] == '')? null : $data['dia_alerta'];
                $oPago_programado['fecha_prevista_pago'] = $data['anual']."/".$_POST['mes_id'][$r]."/".$data['dia_alerta'];
                $oPago_programado['proveedor_id'] = $data['proveedor_id'];
                $oPago_programado['descripcion'] = $data['descripcion'];
                $oPago_programado['monto_previsto'] = ($data['monto_previsto'] == '')? null : $data['monto_previsto'];
                $oPago_programado['estado'] = "PEN";
                $oPago_programado['registrado_por'] = $this->session->userdata('session_usuario');

                $cronograma_pago_id = $this->Pago_programado_model->insert($oPago_programado);
                if(!$cronograma_pago_id){
                    throw new Exception("Problemas en el registro del cronograma.");
                }
            }
            $mensaje = "Cronograma registrado correctamente.<br> Se guardaron ".$nro_meses." pagos pendientes";
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', $mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}
