<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ticketera extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Comanda_model');
        $this->load->model('Pago_model');
        $this->load->model('Generalidades_model');
        $this->load->model('Multitabla_model');
        
        $this->load->library('session');
    }

    function imprimir_cuenta_local($comanda_id){
        // Llamar a la url de localhost para imprimir a ticketera
        try {

        	$ruta_ticket = $this->Site->get_ruta_proy_ticket();

            if(count($this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificado, por favor comuníquese con el área de soporte.");
            }

            //Obtener datos
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $data = json_encode($oComanda);

            //Imprimir
            $url = $ruta_ticket."imprimir_cuenta_local.php?comanda_id=".$comanda_id;
            redirect($url);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }

    function imprimir_ticket_cocina($comanda_id){
        // Llamar a la url de localhost para imprimir a ticketera
        try {
        	$ruta_ticket = $this->Site->get_ruta_proy_ticket();

            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificado, por favor comuníquese con el área de soporte.");
            }
            //Obtener datos
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $data = json_encode($oComanda);

            //Imprimir
            $url = $ruta_ticket."imprimir_para_cocina.php?comanda_id=".$comanda_id;
            redirect($url);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }

    //Desfasados
    function enviar_ticket_cocina($comanda_id){
        // Lo guardamos y abrimos cuando nos redirecciona a alguna venta 
        try {
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificado, por favor comuníquese con el área de soporte.");
            }
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $oGoneralidades = ($this->Generalidades_model->get_one())[0];
            $this->load->library('NumerosEnLetras'); 
            $this->load->library('Ticket');
            $this->pdf = new Ticket();
            
            $this->pdf->SetMargins(5, 5 , 5); //I - A -D
            // Agregamos una página
            $this->pdf->AddPage('P',array(70,200));
            // Define el alias para el número de página que se imprimirá en el pie
            $this->pdf->AliasNbPages();
            // * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
            $tam_letra = 8;
            $this->pdf->SetFont('Arial','B', $tam_letra+3); 
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->razon_social), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->SetFont('Arial','', $tam_letra); 
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->actividad), '0',0,'C','0');
            $this->pdf->Ln(5);
            $this->pdf->Cell(30,6, 'R.U.C.: '.utf8_decode($oGoneralidades->RUC), '0',0,'L','0');
            $this->pdf->Cell(30,6, 'Tel.: '.utf8_decode($oGoneralidades->telefono_1), '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->direccion), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->ubigeo), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->SetFont('Arial','B', $tam_letra); 
            $this->pdf->Cell(60,6, 'Local: '.utf8_decode($oGoneralidades->local), '0',0,'L','0');
            $this->pdf->Ln(1);


            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6,'Ticket para Cocina', '0',0,'C','0');
            $this->pdf->Ln(2);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            $this->pdf->Ln(5);
            $this->pdf->SetFont('Arial','',8);
            $this->pdf->Cell(12,6,utf8_decode('Código:'), '0',0,'L','0');
            $this->pdf->Cell(18,6,$this->Site->rellenar_izquierda($oComanda->comanda_id,'0',5) , '0',0,'L','0');
            $this->pdf->Cell(13,6,'Mozo: ', '0',0,'L','0');
            $this->pdf->Cell(20,6, strtoupper($oComanda->mozo), '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(10,6,'Fecha: ', '0',0,'L','0');
            $fecha = new DateTime($oComanda->fecha_comanda);
            $this->pdf->Cell(20,6, $fecha->format('d').'/'.$fecha->format('m').'/'.$fecha->format('Y'), '0',0,'L','0');
            $this->pdf->Cell(8,6,'Hora: ', '0',0,'L','0');
            $this->pdf->Cell(15,6, $fecha->format('h:i:s A'), '0',0,'L','0');
            $this->pdf->Ln(4);
            if ($oComanda->modalidad == 'PRE') {
                $this->pdf->Cell(10,6,'Mesa:', '0',0,'L','0');
                $this->pdf->Cell(20,6,$oComanda->mesa, '0',0,'L','0');
            }else{
                $this->pdf->Cell(10,6,'Modal.:', '0',0,'L','0');
                $this->pdf->Cell(20,6,$oComanda->modalidad_desc, '0',0,'L','0');
            }
            $this->pdf->Cell(15,6,'Nro Com:', '0',0,'L','0');
            $this->pdf->Cell(5,6,$oComanda->nro_comensales, '0',0,'L','0');
            $this->pdf->Ln(5);
            $this->pdf->Cell(8,6,'Cant.', '0',0,'L','0');
            $this->pdf->Cell(50,6,'Producto.', '0',0,'L','0');
            $this->pdf->Ln(1);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            // $this->pdf->Cell(5);$this->pdf->Cell(20,6, 'Cant', '0',0,'C','1');
            // $this->pdf->SetFillColor(228, 228, 228);
            // $this->pdf->Ln(2);
            foreach ($oComanda->items as $key => $item) {
                $this->pdf->SetFont('Arial','', 8);
                $this->pdf->Ln(4);
                $this->pdf->Cell(8,6, utf8_decode($item->cantidad), '0',0,'C','0');
                $this->pdf->SetFont('Arial','', 7);
                $this->pdf->Cell(50,6, utf8_decode($item->producto), 0,0,'L','0');
                if ($item->comentario != '') {
                    $this->pdf->Ln(3);
                    $this->pdf->Cell(8);
                    $this->pdf->SetFont('Arial','I', 7);
                    $this->pdf->Cell(50,6, utf8_decode($item->comentario), 0,0,'L','0');
                }
            }
            $this->pdf->SetFont('Arial','', 8);
            $this->pdf->Ln(1);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            $this->pdf->Ln(4);
            // $this->pdf->Cell(30,6,'SUB TOTAL: ', '0',0,'L','0');
            // $this->pdf->Cell(15,6,'S/.', '0',0,'L','0');
            // $this->pdf->Cell(15,6,$oComanda->importe_sin_IGV, '0',0,'R','0');
            // $this->pdf->Ln(4);
            // $this->pdf->Cell(30,6,'IGV(18%): ', '0',0,'L','0');
            // $this->pdf->Cell(15,6,'S/.', '0',0,'L','0');
            // $this->pdf->Cell(15,6,$oComanda->total_IGV, '0',0,'R','0');
            // $this->pdf->Ln(4);
            $this->pdf->Cell(30,6,'TOTAL: ', '0',0,'L','0');
            $this->pdf->Cell(15,6,'S/.', '0',0,'L','0');
            $this->pdf->Cell(15,6,$oComanda->importe_total, '0',0,'R','0');
            
            // $this->pdf->SetFont('Arial','B', 8); 
            $direccion = "reportes_pdf/ticket_cocina.pdf";
            $nombre = "Ticket_cocina";
            // $this->pdf->Output($direccion, 'F');
            // $this->pdf->Output('F', $direccion); //Guardar
            $this->pdf->Output('D', $direccion); //Ver
            header('Content-type: application/pdf;');
            header('Content-Disposition: inline; filename="'.$nombre.'"');
            // $mi_pdf = $direcci
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }

    function imprimir_cuenta($comanda_id){
        // Lo guardamos y abrimos cuando nos redirecciona a alguna venta 
        try {
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificado, por favor comuníquese con el área de soporte.");
            }
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $oGoneralidades = ($this->Generalidades_model->get_one())[0];
            $this->load->library('NumerosEnLetras'); 
            $this->load->library('Ticket');
            $this->pdf = new Ticket();
            
            $this->pdf->SetMargins(5, 5 , 5); //I - A -D
            // Agregamos una página
            $this->pdf->AddPage('P',array(70,200));
            // Define el alias para el número de página que se imprimirá en el pie
            $this->pdf->AliasNbPages();
            // * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
            $tam_letra = 8;
            $this->pdf->SetFont('Arial','B', $tam_letra+3); 
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->razon_social), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->SetFont('Arial','', $tam_letra); 
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->actividad), '0',0,'C','0');
            $this->pdf->Ln(5);
            $this->pdf->Cell(30,6, 'R.U.C.: '.utf8_decode($oGoneralidades->RUC), '0',0,'L','0');
            $this->pdf->Cell(30,6, 'Tel.: '.utf8_decode($oGoneralidades->telefono_1), '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->direccion), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->ubigeo), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->SetFont('Arial','B', $tam_letra); 
            $this->pdf->Cell(60,6, 'Local: '.utf8_decode($oGoneralidades->local), '0',0,'L','0');
            $this->pdf->Ln(1);


            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6,utf8_decode('Nota de pedido'), '0',0,'C','0');
            $this->pdf->Ln(2);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            $this->pdf->Ln(5);
            $this->pdf->SetFont('Arial','',8);
            $this->pdf->Cell(12,6,utf8_decode('Código:'), '0',0,'L','0');
            $this->pdf->Cell(18,6,$this->Site->rellenar_izquierda($oComanda->comanda_id,'0',5) , '0',0,'L','0');
            $this->pdf->Cell(13,6,'Mozo: ', '0',0,'L','0');
            $this->pdf->Cell(20,6, strtoupper($oComanda->mozo), '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(10,6,'Fecha: ', '0',0,'L','0');
            $fecha = new DateTime($oComanda->fecha_comanda);
            $this->pdf->Cell(20,6, $fecha->format('d').'/'.$fecha->format('m').'/'.$fecha->format('Y'), '0',0,'L','0');
            $this->pdf->Cell(8,6,'Hora: ', '0',0,'L','0');
            $this->pdf->Cell(15,6, $fecha->format('h:i:s A'), '0',0,'L','0');
            $this->pdf->Ln(4);
            if ($oComanda->modalidad == 'PRE') {
                $this->pdf->Cell(10,6,'Mesa:', '0',0,'L','0');
                $this->pdf->Cell(20,6,$oComanda->mesa, '0',0,'L','0');
            }else{
                $this->pdf->Cell(10,6,'Modal.:', '0',0,'L','0');
                $this->pdf->Cell(20,6,$oComanda->modalidad_desc, '0',0,'L','0');
            }
            $this->pdf->Cell(15,6,'Nro Com:', '0',0,'L','0');
            $this->pdf->Cell(5,6,$oComanda->nro_comensales, '0',0,'L','0');

            $this->pdf->Ln(4);
            $this->pdf->Cell(12,6,'Cliente: ', '0',0,'L','0');
            $this->pdf->Cell(30,6, $oComanda->cliente_nombre, '0',0,'L','0');
            if ($oComanda->modalidad == 'DEL') {
                $this->pdf->SetFont('Arial','',7);
                $this->pdf->Ln(4);
                $this->pdf->Cell(5,6, 'Dir.', '0',0,'L','0');
                $this->pdf->Cell(30,6, substr($oComanda->cliente_direccion, 0,50), '0',0,'L','0');
                $this->pdf->Ln(3);
                $this->pdf->Cell(10,6, 'Tel..', '0',0,'L','0');
                $this->pdf->Cell(30,6, substr($oComanda->cliente_telefono, 0,45), '0',0,'L','0');
            }

            $this->pdf->Ln(5);
            $this->pdf->Cell(8,6,'Cant.', '0',0,'L','0');
            $this->pdf->Cell(50,6,'Producto.', '0',0,'L','0');
            $this->pdf->Ln(1);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            // $this->pdf->Cell(5);$this->pdf->Cell(20,6, 'Cant', '0',0,'C','1');
            // $this->pdf->SetFillColor(228, 228, 228);
            // $this->pdf->Ln(2);
            foreach ($oComanda->items as $key => $item){
                if ($item->estado_atencion != 'X') {
                    $this->pdf->Ln(4);
                    $this->pdf->SetFont('Arial','', 7);
                    $this->pdf->Cell(60,6, utf8_decode($item->producto), 0,0,'L','0');
                    $this->pdf->Ln(3);
                    $this->pdf->Cell(15,6, utf8_decode($item->cantidad), '0',0,'C','0');
                    $this->pdf->Cell(15,6,'und' , '0',0,'C','0');
                    $this->pdf->Cell(15,6, utf8_decode($item->precio_unitario), '0',0,'C','0');
                    $this->pdf->Cell(15,6, utf8_decode($item->importe), '0',0,'R','0');
                }
            }
            $this->pdf->SetFont('Arial','', 8);
            $this->pdf->Ln(1);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(30,6,'SUB TOTAL: ', '0',0,'L','0');
            $this->pdf->Cell(15,6,'S/.', '0',0,'L','0');
            $this->pdf->Cell(15,6,$oComanda->importe_sin_IGV, '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(30,6,'IGV(18%): ', '0',0,'L','0');
            $this->pdf->Cell(15,6,'S/.', '0',0,'L','0');
            $this->pdf->Cell(15,6,$oComanda->total_IGV, '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(30,6,'TOTAL: ', '0',0,'L','0');
            $this->pdf->Cell(15,6,'S/.', '0',0,'L','0');
            $this->pdf->Cell(15,6,$oComanda->importe_total, '0',0,'R','0');
            
            
            $this->pdf->Ln(4);
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6,'www.santorini.com.pe', '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6,'GRACIAS POR SU PREFERENCIA', '0',0,'C','0');
            // $this->pdf->SetFont('Arial','B', 8); 
            $direccion = "reportes_pdf/cuenta_comanda.pdf";
            $nombre = "Cuenta de comanda";
            $this->pdf->Output('F', $direccion); //Guardar
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }

    //De pago
    function imprimir_ticket_pago($pago_id){
        try {
            if(count( $this->Pago_model->get_one($pago_id)) == 0){
                throw new Exception("Error, no se encontró el pago especificado, por favor comuníquese con el área de soporte.");
            }
            $oPago = ($this->Pago_model->get_one($pago_id))[0];
            $oComanda = ($this->Comanda_model->get_one($oPago->comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);
            $oGoneralidades = ($this->Generalidades_model->get_one())[0];
            // echo var_dump($oGoneralidades);exit();
            // echo json_encode($oComanda);exit();
            $this->load->library('NumerosEnLetras'); 
            $this->load->library('Ticket');          
            $this->pdf = new Ticket();
            $this->pdf->SetMargins(5, 5 , 5); //I - A -D
            // Agregamos una página
            $this->pdf->AddPage('P',array(70,200));
            // Define el alias para el número de página que se imprimirá en el pie
            $this->pdf->AliasNbPages();
            // * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
            $tam_letra = 8;
            $this->pdf->SetFont('Arial','B', $tam_letra+3); 
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->razon_social), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->SetFont('Arial','', $tam_letra); 
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->actividad), '0',0,'C','0');
            $this->pdf->Ln(5);
            $this->pdf->Cell(30,6, 'R.U.C.: '.utf8_decode($oGoneralidades->RUC), '0',0,'L','0');
            $this->pdf->Cell(30,6, 'Tel.: '.utf8_decode($oGoneralidades->telefono_1), '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->direccion), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6, utf8_decode($oGoneralidades->ubigeo), '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->SetFont('Arial','B', $tam_letra); 
            $this->pdf->Cell(60,6, 'Local: '.utf8_decode($oGoneralidades->local), '0',0,'L','0');
            $this->pdf->Ln(5);


            $this->pdf->SetFont('Arial','B',8);
            if ($oPago->tipo_comprobante == 'TCK') {
                $comprobante = 'Ticket: '.$oPago->serie.' - '.$oPago->correlativo;
                $this->pdf->Cell(60,6, $comprobante, '0',0,'C','0');
            }
            if ($oPago->tipo_comprobante == 'B') {
                $comprobante = 'Boleta: '.$oPago->serie.' - '.$oPago->correlativo;
                $this->pdf->Cell(60,6, $comprobante, '0',0,'C','0');
            }
            if ($oPago->tipo_comprobante == 'F') {
                $comprobante = 'Factura: '.$oPago->serie.' - '.$oPago->correlativo;
                $this->pdf->Cell(60,6, $comprobante, '0',0,'C','0');
            }
            if ($oPago->tipo_comprobante == 'O') {
                $this->pdf->Cell(60,6,'Comprobante de pago', '0',0,'L','0');
            }
            $this->pdf->SetFont('Arial','',8);
            $this->pdf->Ln(5);
            
            $this->pdf->Cell(12,6,utf8_decode('Código:'), '0',0,'L','0');
            $this->pdf->Cell(18,6,$this->Site->rellenar_izquierda($oComanda->comanda_id,'0',5) , '0',0,'L','0');
            $this->pdf->Cell(13,6,'Cajero: ', '0',0,'L','0');
            $this->pdf->Cell(20,6, strtoupper($oPago->cajero), '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(10,6,'Fecha: ', '0',0,'L','0');
            $fecha = new DateTime($oComanda->fecha_comanda);
            $this->pdf->Cell(20,6, $fecha->format('d').'/'.$fecha->format('m').'/'.$fecha->format('Y'), '0',0,'L','0');
            $this->pdf->Cell(8,6,'Hora: ', '0',0,'L','0');
            $this->pdf->Cell(15,6, $fecha->format('h:i:s A'), '0',0,'L','0');
            $this->pdf->Ln(4);
            
            if ($oComanda->modalidad == 'PRE') {
                $this->pdf->Cell(10,6,'Mesa:', '0',0,'L','0');
                $this->pdf->Cell(20,6,$oComanda->mesa, '0',0,'L','0');
            }else{
                $this->pdf->Cell(10,6,'Modal.:', '0',0,'L','0');
                $this->pdf->Cell(20,6,$oComanda->modalidad_desc, '0',0,'L','0');
            }
            $this->pdf->Ln(4);
            $this->pdf->Cell(12,6,'Cliente: ', '0',0,'L','0');
            $this->pdf->Cell(30,6, $oComanda->cliente_nombre, '0',0,'L','0');
            if ($oComanda->modalidad == 'DEL') {
                $this->pdf->SetFont('Arial','',7);
                $this->pdf->Ln(4);
                $this->pdf->Cell(10,6, 'Direcc.', '0',0,'L','0');
                $this->pdf->Cell(30,6, substr($oComanda->cliente_direccion, 0,45), '0',0,'L','0');
                $this->pdf->Ln(3);
                $this->pdf->Cell(10,6, 'Tel..', '0',0,'L','0');
                $this->pdf->Cell(30,6, substr($oComanda->cliente_telefono, 0,45), '0',0,'L','0');
            }
            $this->pdf->SetFont('Arial','',8);
            $this->pdf->Ln(5);
            $this->pdf->Cell(15,6,'Cant.', '0',0,'C','0');
            $this->pdf->Cell(15,6,'Und.', '0',0,'C','0');
            $this->pdf->Cell(15,6,'P.U.', '0',0,'C','0');
            $this->pdf->Cell(15,6,'Importe.', '0',0,'C','0');
            $this->pdf->Ln(1);
            $this->pdf->Cell(60,6,'______________________________________', '0',0,'C','0');
            foreach ($oComanda->items as $key => $item) {
                $this->pdf->Ln(4);
                $this->pdf->SetFont('Arial','', 7);
                $this->pdf->Cell(60,6, utf8_decode($item->producto), 0,0,'L','0');
                $this->pdf->Ln(3);
                $this->pdf->Cell(15,6, utf8_decode($item->cantidad), '0',0,'C','0');
                $this->pdf->Cell(15,6,'und' , '0',0,'C','0');
                $this->pdf->Cell(15,6, utf8_decode($item->precio_unitario), '0',0,'C','0');
                $this->pdf->Cell(15,6, utf8_decode($item->importe), '0',0,'R','0');
            }
            $this->pdf->Ln(1);
            $this->pdf->Cell(60,6,'___________________________________________', '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20);
            $this->pdf->Cell(20,6,'SUB TOTAL: ', '0',0,'L','0');
            $this->pdf->Cell(10,6,'S/. ', '0',0,'C','0');
            $this->pdf->Cell(10,6,$oComanda->importe_sin_IGV, '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20);
            $this->pdf->Cell(20,6,'IGV(18%): ', '0',0,'L','0');
            $this->pdf->Cell(10,6,'S/. ', '0',0,'C','0');
            $this->pdf->Cell(10,6,$oComanda->total_IGV, '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20);
            $this->pdf->Cell(20,6,'TOTAL: ', '0',0,'L','0');
            $this->pdf->Cell(10,6,'S/. ', '0',0,'C','0');
            $this->pdf->Cell(10,6,$oComanda->importe_total, '0',0,'R','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20,6,'Descuento: S/. '.$oPago->descuento, '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20,6,'Tarjeta: S/. '.$oPago->pago_tarjeta, '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20,6,'Efectivo: S/. '.$oPago->efectivo, '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20,6,'Vuelto: S/.'.$oPago->vuelto, '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(20,6,'Cajero: '.$oPago->cajero_desc, '0',0,'L','0');
            $this->pdf->Ln(4);
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6,'www.santorini.com.pe', '0',0,'C','0');
            $this->pdf->Ln(4);
            $this->pdf->Cell(60,6,'GRACIAS POR SU PREFERENCIA', '0',0,'C','0');
            // $this->pdf->SetFont('Arial','B', 8); 
            $direccion = "reportes_pdf/comprobante.pdf";
            $nombre = "Comprobante de pago";
            // $this->pdf->Output($direccion, 'F');
            $this->pdf->Output('F', $direccion); //Guardar
           
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}
