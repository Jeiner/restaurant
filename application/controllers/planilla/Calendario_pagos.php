<?php defined('BASEPATH') or exit('No direct script access allowed');

class Calendario_pagos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Empleado_model');
        $this->load->model('Sede_model');
        $this->load->model('Cargo_model');
        $this->load->model('Multitabla_model');
        $this->load->model('planilla/Empleado_pre_pago_model');
        $this->load->model('planilla/Empleado_pago_model');
        
        $this->load->library('session');
    }

    function calcular_total_pagar_periodo($sueldo_base_mensual, $oCalendario){
        $nTotalPagar = $sueldo_base_mensual;
        foreach ($oCalendario->oPeriodoPago->lstPrePagos as $key => $oPrepago) {
            $nTotalPagar += $oPrepago->importe * -1;
        }
        return $nTotalPagar;
    }

    function index(){

        $data['success'] = isset($data['success']) ? $data['success'] : $this->session->flashdata('success');
        $data['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        
        //Datos de entrada
        $mes   = (!isset($_REQUEST['mes'])) ? ""  : trim($_REQUEST['mes']);
        $anio   = (!isset($_REQUEST['anio'])) ? ""  : trim($_REQUEST['anio']);
        $cCalendario1Mes = ($mes == "") ? date('m') : $mes;
        $cCalendario1Anio = ($anio == "") ? date('Y') : $anio;

        //Generar calendarios
        $oCalendarioGeneral = $this->get_calendario_mensual($cCalendario1Mes, $cCalendario1Anio);
        $dFechaAnterior = date('Y-m-d', strtotime($oCalendarioGeneral->dFechaInicio . ' -1 day'));
        $cMesAnterior = date('m', strtotime($dFechaAnterior));
        $cAnioAnterior = date('Y', strtotime($dFechaAnterior));
        $oCalendarioAnterior = $this->get_calendario_mensual($cMesAnterior, $cAnioAnterior);

        //Data empleados
        $lstEmpleados = $this->Empleado_model->get_all_order_dia_pago();

        foreach ($lstEmpleados as $key => $oEmpleado) {
            
            $oEmpleado->sueldo_diario = round(($oEmpleado->sueldo_base_mensual / 30), 2);

            //Calendario anterior
            $oEmpleado->oCalendarioAnterior = $this->get_calendario_mensual($cMesAnterior, $cAnioAnterior, $oEmpleado->dia_pago);
            $oEmpleado->oCalendarioAnterior->lstPrePagos = $this->Empleado_pre_pago_model->get_prepagos_by_fechas(
                    $oEmpleado->oCalendarioAnterior->dFechaInicio,
                    $oEmpleado->oCalendarioAnterior->dFechaFin, 
                    $oEmpleado->empleado_id);
            $oEmpleado->oCalendarioAnterior->oPeriodoPago->lstPrePagos = $this->Empleado_pre_pago_model->get_prepagos_by_fechas(
                    $oEmpleado->oCalendarioAnterior->oPeriodoPago->dFechaInicio,
                    $oEmpleado->oCalendarioAnterior->oPeriodoPago->dFechaFin, 
                    $oEmpleado->empleado_id);
            $oEmpleado->oCalendarioAnterior->oPeriodoPago->nTotalPagar = $this->calcular_total_pagar_periodo(
                    $oEmpleado->sueldo_base_mensual, 
                    $oEmpleado->oCalendarioAnterior);

            //Calendario principal
            $oEmpleado->oCalendarioPrincipal = $this->get_calendario_mensual($cCalendario1Mes, $cCalendario1Anio, $oEmpleado->dia_pago);
            $oEmpleado->oCalendarioPrincipal->lstPagos = $this->Empleado_pago_model->get_pagos_by_fechas(
                    $oEmpleado->oCalendarioPrincipal->dFechaInicio,
                    $oEmpleado->oCalendarioPrincipal->dFechaFin, 
                    $oEmpleado->empleado_id);
            $oEmpleado->oCalendarioPrincipal->lstPrePagos = $this->Empleado_pre_pago_model->get_prepagos_by_fechas(
                    $oEmpleado->oCalendarioPrincipal->dFechaInicio,
                    $oEmpleado->oCalendarioPrincipal->dFechaFin, 
                    $oEmpleado->empleado_id);
            $this->validarPagoDePrepago($oEmpleado->oCalendarioPrincipal->lstPagos, $oEmpleado->oCalendarioPrincipal->lstPrePagos);

            $oEmpleado->oCalendarioPrincipal->oPeriodoPago->lstPrePagos = $this->Empleado_pre_pago_model->get_prepagos_by_fechas(
                    $oEmpleado->oCalendarioPrincipal->oPeriodoPago->dFechaInicio,
                    $oEmpleado->oCalendarioPrincipal->oPeriodoPago->dFechaFin, 
                    $oEmpleado->empleado_id);
            $oEmpleado->oCalendarioPrincipal->oPeriodoPago->nTotalPagar = $this->calcular_total_pagar_periodo(
                    $oEmpleado->sueldo_base_mensual, 
                    $oEmpleado->oCalendarioPrincipal);

        }

        //echo json_encode($lstEmpleados);exit();

        //Salida
        $data["oCalendarioGeneral"] = $this->get_calendario_mensual($cCalendario1Mes, $cCalendario1Anio);
        $data["oCalendarioAnterior"] = $oCalendarioAnterior;
        $data["lstEmpleados"] = $lstEmpleados;
        $data['cCalendario1Mes'] = $cCalendario1Mes;
        $data['cCalendario1Anio'] = $cCalendario1Anio;

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Planilla' ),
                    array('link' => '#', 'page' => 'Empleados' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Empleados', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'empleados' );
        $this->load->view('planilla/calendario_pagos/index', $data);
    }

    function validarPagoDePrepago($lstPagos, $lstPrepagos){
        foreach ($lstPrepagos as $key => $oPrepago) {
            $oPrepago->lPagado = false;

            foreach ($lstPagos as $key => $oPago) {
                if($oPrepago->fecha_pago == $oPago->fecha_pago){
                    $oPrepago->lPagado = true;
                }
            }
        }
    }

}
