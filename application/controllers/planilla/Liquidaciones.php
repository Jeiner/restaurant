<?php defined('BASEPATH') or exit('No direct script access allowed');

class Liquidaciones extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Empleado_model');
        $this->load->model('Sede_model');
        // $this->load->model('Cargo_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Liquidacion_model');
        $this->load->model('Concepto_model');
        $this->load->model('Reportes_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_liquidacion(){
        if($this->session->userdata('oLiquidacion')){
            return $this->session->userdata('oLiquidacion');
        }else{
            $oLiquidacion = new Liquidacion_model();
            $this->session->set_userdata('oLiquidacion', $oLiquidacion);
            return $this->session->userdata('oLiquidacion');
        }
    }
    function index(){
        $this->session->unset_userdata('oLiquidacion');
        
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Planilla' ),
                    array('link' => '#', 'page' => 'Gestionar liquidacion' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Liquidación', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'liquidaciones' );
        $this->page_construct('planilla/liquidaciones/index', $meta, null);
    }
    function ajax_tabla_liquidaciones(){
        $data['anual'] = $this->input->post('anual');//Año-Mes-Dia
        $data['mes_id'] = $this->input->post('mes_id');

        $data['liquidaciones'] = $this->Liquidacion_model->get_all_with_filtros($data['anual'], $data['mes_id']);
        $this->partial_view('planilla/liquidaciones/_tabla_liquidaciones',$data);
    }
    function ajax_cargar_detalle(){
        $liquidacion_id = $this->input->post('liquidacion_id');
        $data['oLiquidacion'] = ($this->Liquidacion_model->get_one($liquidacion_id))[0];
        $data['oLiquidacion']->items = $this->Liquidacion_model->get_detalle_by_liquidacion($liquidacion_id);
        // $data['sede_id'] = ($this->input->post('sede_id') == '')? "0" : $this->input->post('sede_id');
        $this->partial_view('planilla/liquidaciones/_modal_ver_detalle',$data);
    }
    function ajax_ultimos_pagos(){
        $empleado_id = $this->input->post('empleado_id');
        $data['oPagos'] = $this->Liquidacion_model->ultimos_pagos($empleado_id);
        $this->partial_view('planilla/liquidaciones/_tabla_ultimos_pagos',$data);
    }
    function nueva(){
        $data['oLiquidacion'] = $this->preparar_obj_liquidacion();
        $data['oLiquidacion']->fecha_pago = date('Y-m-d');
        $data['motivos'] = $this->Multitabla_model->get_all_by_tipo(13);
        $data['empleados'] = $this->Empleado_model->get_all();
        $data['conceptos'] = $this->Concepto_model->get_all();
        
        // echo json_encode($data['sedes']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Planilla' ),
                    array('link' => '#', 'page' => 'Nueva liquidación' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nueva liquidación', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'nueva_liquidacion' );
        $this->page_construct('planilla/liquidaciones/nueva', $meta, $data);
    }
    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            if (count($this->Empleado_model->get_one($this->input->post('empleado_id'))) == 0) {
                throw new Exception("No se encontró el empleado especificado.");
            }
            $oEmpleado = ($this->Empleado_model->get_one($this->input->post('empleado_id')))[0];
            // CALCULAMOS LOS TOTALES
            $total = 0; $total_rem = 0; $total_des = 0;
            $nro_items_rem = isset($_POST['concepto_rem_id']) ? sizeof($_POST['concepto_rem_id']) : 0; //nro de Remuneraciones
            $nro_items_des = isset($_POST['concepto_des_id']) ? sizeof($_POST['concepto_des_id']) : 0; //nro de Descuentos
            // Calculamos total remuneraciones
            for ($r = 0; $r < $nro_items_rem; $r++) {
                if($_POST['conceptos_rem'][$r] != ""){
                    $objConcepto = ($this->Concepto_model->get_one($_POST['concepto_rem_id'][$r]))[0];
                    $item = array(
                        'liquidacion_id' => 0,
                        'concepto_id' => $_POST['concepto_rem_id'][$r],
                        'importe' => $_POST['conceptos_rem'][$r],
                        'tipo_concepto' => $objConcepto->tipo_concepto,
                        'signo' => $objConcepto->signo,
                        'fecha_registro' => date("Y/m/d H:i:s"),
                        'registrado_por' => $this->session->userdata('session_usuario'),
                    );
                    $items[] = $item;
                    $total_rem += $_POST['conceptos_rem'][$r];
                }
            }
            // Calculamos total descuentos
            for ($r = 0; $r < $nro_items_des; $r++) {
                if($_POST['conceptos_des'][$r] != ""){
                    $objConcepto = ($this->Concepto_model->get_one($_POST['concepto_des_id'][$r]))[0];
                    $item = array(
                        'liquidacion_id' => 0,
                        'concepto_id' => $_POST['concepto_des_id'][$r],
                        'importe' => $_POST['conceptos_des'][$r],
                        'tipo_concepto' => $objConcepto->tipo_concepto,
                        'signo' => $objConcepto->signo,
                        'fecha_registro' => date("Y/m/d H:i:s"),
                        'registrado_por' => $this->session->userdata('session_usuario'),
                    );
                    $items[] = $item;
                    $total_des += $_POST['conceptos_des'][$r];
                }
            }
            $total = $total_rem - $total_des;
            if($total <= 0){
                throw new Exception("El total a pagar debe ser mayor a 0.");
            }
            $data = array(
                            'fecha_pago' => date("Y/m/d H:i:s"),
                            'empleado_id' => $this->input->post('empleado_id'),
                            'motivo' => $this->input->post('motivo'),
                            'sede_id' => $oEmpleado->sede_id,
                            'total_remuneraciones' => $total_rem,
                            'total_descuentos' => $total_des,
                            'total_a_pagar' => $total,
                            'estado' => "A",
                            'fecha_registro' => date("Y/m/d H:i:s"),
                            'registrado_por' => $this->session->userdata('session_usuario'),
                        );
            
            $this->form_validation->set_rules('empleado_id', "Código de empleado", 'required');
            $this->form_validation->set_rules('motivo', "Motivo de pago", 'required');

            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('liquidacion_id') == 0 ){
                    // /Nueva usuario
                    $liquidacion_id = $this->Liquidacion_model->insert($data, $items);
                    if(!$liquidacion_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar la liquidación.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Liquidación registrada correctamente.");
                    }
                    $this->session->unset_userdata('oLiquidacion');
                    $this->session->set_userdata('remove_local_liquidacion',true);
                    redirect(base_url('planilla/liquidaciones'));
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    function eliminar($liquidacion_id){
        $this->loaders->verifica_sesion();
        $oLiquidacion = $this->Liquidacion_model->get_one($liquidacion_id);

        if( $this->Liquidacion_model->eliminar_fisico($liquidacion_id) ){
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Liquidación eliminada correctamente.');
            redirect(base_url('planilla/liquidaciones'));
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'No se puede eliminar la liquidación.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function anular($liquidacion_id){
        $this->loaders->verifica_sesion();
        $oLiquidacion = $this->Liquidacion_model->get_one($liquidacion_id);

        if( $this->Liquidacion_model->anular_liquidacion($liquidacion_id) ){
            $this->Liquidacion_model->anular_detalle($liquidacion_id);
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Liquidación anulada correctamente.');
            redirect(base_url('planilla/liquidaciones'));
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'No se puede anular la liquidación.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function reporte(){
        $data['empleados'] = $this->Empleado_model->get_all();
        $data['sedes'] = $this->Sede_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Liquidaciones' ),
                    array('link' => '#', 'page' => 'Reporte' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Reporte de liquidaciones', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'liquidaciones' );
        $this->page_construct('planilla/liquidaciones/reporte', $meta, $data);
    }
    function ajax_liquidaciones_mensual(){
        $anual = $this->input->post('anual');//Año-Mes-Dia
        $empleado_id = $this->input->post('empleado_id');//Año-Mes-Dia
        $sede_id = $this->input->post('sede_id');//Año-Mes-Dia

        $result = $this->Reportes_model->liquidaciones_mensuales($anual, $empleado_id, $sede_id);
        echo json_encode($result);

    }
}
