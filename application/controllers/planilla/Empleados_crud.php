<?php defined('BASEPATH') or exit('No direct script access allowed');

class Empleados_crud extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Empleado_model');
        $this->load->model('Sede_model');
        $this->load->model('Cargo_model');
        $this->load->model('Multitabla_model');
        $this->load->model('planilla/Empleado_pre_pago_model');
        $this->load->model('Usuario_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }

    function crud_guardar_empleado_create($oDataEmpleado, $dataUsuario){
        //Validar
        $buscarEmpleado = $this->Empleado_model->get_one_by_documento($oDataEmpleado['nro_documento']);
        if(count($buscarEmpleado) > 0){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El personal ya existe. Nro documento:".$oDataEmpleado['nro_documento']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Validar usuario
        $buscarUsuario = $this->Usuario_model->get_by_nombre_usuario($dataUsuario['flag_usuario']);
        if($buscarUsuario != null){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El usuario ya existe. Usuario: ".$dataUsuario['flag_usuario']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        // /Registrar empleado
        $empleado_id = $this->Empleado_model->insert($oDataEmpleado);
        if(!$empleado_id){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar al empleado.');
        }else{
            $this->curd_guardar_usuario_de_empleado($empleado_id, $oDataEmpleado, $dataUsuario);
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Empleado registrado correctamente.");
        }
        $this->session->unset_userdata('oEmpleado');
        redirect(base_url('planilla/empleados'));
    }

    function crud_guardar_empleado_update($oDataEmpleado){
        // Editar empleado
        $rpta = $this->Empleado_model->update($oDataEmpleado);
        if(!$rpta){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para actualizar al empleado.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Empleado actualizado correctamente.");
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $data = array(  
                            'empleado_id' => $this->input->post('empleado_id'),
                            'tipo_documento' => "DNI",
                            'nro_documento' => $this->input->post('nro_documento'),
                            'nombres' => strtoupper($this->input->post('nombres')),
                            'ape_paterno' => strtoupper($this->input->post('ape_paterno')),
                            'ape_materno' => strtoupper($this->input->post('ape_materno')),
                            'nombre_completo' => strtoupper($this->input->post('nombres').' '.$this->input->post('ape_paterno').' '. $this->input->post('ape_materno')),
                            // 'datos_reniec' => $this->input->post('datos_reniec'),
                            'fecha_nacimiento' => ($this->input->post('fecha_nacimiento') != "")? $this->input->post('fecha_nacimiento') : null,
                            'sexo' => ($this->input->post('sexo') != "")? $this->input->post('sexo') : null,
                            'foto' => ($this->input->post('foto') != "")? $this->input->post('foto') : null,
                            // 'foto' => $this->input->post('foto'),
                            'telefono_1' => $this->input->post('telefono_1'),
                            // 'telefono_2' => $this->input->post('telefono_2'),
                            'correo' => $this->input->post('correo'),
                            // 'ubigeo' => $this->input->post('ubigeo'),
                            'direccion' => $this->input->post('direccion'),
                            // 'sede_id' => $this->input->post('sede_id'),
                            'tipo_contrato' => $this->input->post('tipo_contrato'),
                            'cargo_id' => $this->input->post('cargo_id'),
                            'fecha_ingreso' => ($this->input->post('fecha_ingreso') != "")? $this->input->post('fecha_ingreso') : null,
                            'fecha_salida' => ($this->input->post('fecha_salida') != "")? $this->input->post('fecha_salida') : null,
                            'sueldo_base_mensual' => ($this->input->post('sueldo_base_mensual') != "")? $this->input->post('sueldo_base_mensual') : null,
                            'sueldo_base_semanal' => ($this->input->post('sueldo_base_semanal') != "")? $this->input->post('sueldo_base_semanal') : null,
                            'cuenta_bancaria' => ($this->input->post('cuenta_bancaria') != "")? $this->input->post('cuenta_bancaria') : null,
                            'nro_cuenta' => $this->input->post('nro_cuenta'),
                            'estado' => $this->input->post('estado'),
                            'observaciones' => $this->input->post('observaciones'),

                            'dia_pago' => $this->input->post('dia_pago'),
                            'sucursal_codigo' => $this->input->post('sucursal_codigo')
                        );

            $dataUsuario = array(
                            'flag_usuario' => strtoupper($this->input->post('flag_usuario')),
                            'flag_usuario_rol' => $this->input->post('flag_usuario_rol'),
                            );

            $this->set_datos_obj_empleado($data);
            
            $this->form_validation->set_rules('nombres', "Nombres", 'required|min_length[2]');
            $this->form_validation->set_rules('ape_paterno', "Apellido paterno", 'required|min_length[2]');
            $this->form_validation->set_rules('ape_materno', "Apellido materno", 'required|min_length[2]');
            $this->form_validation->set_rules('fecha_nacimiento', "Fecha nacimiento", 'required');
            $this->form_validation->set_rules('sucursal_codigo', "Sucursal", 'required');
            $this->form_validation->set_rules('sueldo_base_mensual', "Sueldo base mensual", 'required');
            $this->form_validation->set_rules('cargo_id', "Cargo", 'required');
            $this->form_validation->set_rules('estado', "Estado", 'required');

            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('empleado_id') == 0 ){
                    $this->crud_guardar_empleado_create($data, $dataUsuario);
                }else{
                    $this->crud_guardar_empleado_update($data);
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('planilla/empleados'));
        }
    }

    function curd_guardar_usuario_de_empleado($empleado_id, $oDataEmpleado, $dataUsuario){
        $nueva_data_usuario = array(
            'empleado_id' => $empleado_id,
            'DNI' => $oDataEmpleado['nro_documento'],
            'nombre_completo' =>  $oDataEmpleado['nombre_completo'],
            'telefono' => $oDataEmpleado['telefono_1'],
            'correo' => $oDataEmpleado['correo'],
            'usuario' => $dataUsuario['flag_usuario'],
            'rol_id' => $dataUsuario['flag_usuario_rol'],
            'id_usuario_asistencia' => "",
            'ruta_foto' => "",
            'clave' =>  md5($oDataEmpleado['nro_documento'])
        );

        $usuario_id = $this->Usuario_model->insert($nueva_data_usuario);
        if(!$usuario_id){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar el usuario.');
            redirect($_SERVER["HTTP_REFERER"]);
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Usuario registrado correctamente.");
        }
    }

    function set_datos_obj_empleado($data){

        if($this->session->userdata('oEmpleado')){
            $oEmpleado = $this->session->userdata('oEmpleado');
        }else{
            $oEmpleado = new Empleado_model();
        }

        $oEmpleado->empleado_id =  $data['empleado_id'];
        $oEmpleado->tipo_documento =  $data['tipo_documento'];
        $oEmpleado->nro_documento =  $data['nro_documento'];
        $oEmpleado->nombres =  $data['nombres'];
        $oEmpleado->ape_paterno =  $data['ape_paterno'];
        $oEmpleado->ape_materno =  $data['ape_materno'];
        $oEmpleado->nombre_completo =  $data['nombre_completo'];
        // $oEmpleado->datos_reniec =  $data['datos_reniec'];
        $oEmpleado->fecha_nacimiento =  $data['fecha_nacimiento'];
        $oEmpleado->sexo =  $data['sexo'];
        // $oEmpleado->foto =  $data['foto'];
        $oEmpleado->telefono_1 =  $data['telefono_1'];
        // $oEmpleado->telefono_2 =  $data['telefono_2'];
        $oEmpleado->correo =  $data['correo'];
        // $oEmpleado->ubigeo =  $data['ubigeo'];
        $oEmpleado->direccion =  $data['direccion'];

        $oEmpleado->sede_id =  "0";
        $oEmpleado->tipo_contrato =  $data['tipo_contrato'];
        $oEmpleado->cargo_id =  $data['cargo_id'];
        $oEmpleado->fecha_ingreso =  $data['fecha_ingreso'];
        $oEmpleado->fecha_salida =  $data['fecha_salida'];
        $oEmpleado->sueldo_base_mensual =  $data['sueldo_base_mensual'];
        $oEmpleado->sueldo_base_semanal =  $data['sueldo_base_semanal'];
        $oEmpleado->cuenta_bancaria =  $data['cuenta_bancaria'];
        $oEmpleado->nro_cuenta =  $data['nro_cuenta'];
        $oEmpleado->estado =  $data['estado'];
        $oEmpleado->observaciones =  $data['observaciones'];

        $oEmpleado->dia_pago =  $data['dia_pago'];
        $oEmpleado->sucursal_codigo =  $data['sucursal_codigo'];

        $this->session->set_userdata('oEmpleado', $oEmpleado);
    }

}
