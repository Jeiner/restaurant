<?php defined('BASEPATH') or exit('No direct script access allowed');

class Empleados extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Empleado_model');
        $this->load->model('Sede_model');
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->model('Cargo_model');
        $this->load->model('Multitabla_model');
        $this->load->model('planilla/Empleado_pre_pago_model');
        $this->load->model('Usuario_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }

    function preparar_obj_empleado(){
        if($this->session->userdata('oEmpleado')){
            return $this->session->userdata('oEmpleado');
        }else{
            $oEmpleado = new Empleado_model();
            $this->session->set_userdata('oEmpleado', $oEmpleado);
            return $this->session->userdata('oEmpleado');
        }
    }

    function index(){
        //Parametros de entrada
        $filtro_estado = $this->input->get("filtro_estado");

        //
        $this->session->unset_userdata('oEmpleado');
        $lstEmpleados = $this->Empleado_model->get_all_by_filtros($filtro_estado);
        $nroEmpleadosPendientes = count($this->Empleado_model->get_all_by_filtros('L'));
        $nroEmpleadosSinUsuario = 0;

        foreach ($lstEmpleados as $key => $oEmpleado) {
            $oEmpleado->oSucursal = $this->Nube_sucursal_model->get_one_or_null_by_codigo($oEmpleado->sucursal_codigo);
            $oEmpleado->oCargo = $this->Cargo_model->get_one_or_null_by_codigo($oEmpleado->cargo_id);
            $oEmpleado->oUsuario = $this->Usuario_model->get_by_empleado_id($oEmpleado->empleado_id);
            if($oEmpleado->oUsuario == null){
                $nroEmpleadosSinUsuario++;
            }
        }

        $data['lstEmpleados'] = $lstEmpleados;
        $data['nroEmpleadosPendientes'] = $nroEmpleadosPendientes;
        $data['nroEmpleadosSinUsuario'] = $nroEmpleadosSinUsuario;
        
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Planilla' ),
                    array('link' => '#', 'page' => 'Empleados' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Empleados', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'empleados' );
        $this->page_construct('planilla/empleados/index', $meta, $data);
    }

    function nuevo(){
        $data['oEmpleado'] = $this->preparar_obj_empleado();
        // $data['sedes'] = $this->Sede_model->get_all();
        $data['lstSucursales'] = $this->Nube_sucursal_model->get_all_activos();
        $data['cargos'] = $this->Cargo_model->get_all();
        $data['tipos_contratos'] = $this->Multitabla_model->get_all_by_tipo(10);
        $data['bancos'] = $this->Multitabla_model->get_all_by_tipo(11);
        $data['lstRoles'] = $this->Usuario_model->get_all_roles();
        
        // echo json_encode($data['sedes']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Planilla' ),
                    array('link' => '#', 'page' => 'Nuevo empleado' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nuevo empleado', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'empleados' );
        $this->page_construct('planilla/empleados/nuevo', $meta, $data);
    }

    function editar($empleado_id = 0){
        try {
            $this->session->unset_userdata('oEmpleado');

            //Buscar empleado
            $buscarEmpleado = $this->Empleado_model->get_one($empleado_id);
            if (count($buscarEmpleado) == 0 ) {
                $message = "No se encontró el empleado para editar: ".$empleado_id;
                throw new Exception($message);
            }

            $data['oEmpleado'] = $buscarEmpleado[0];
            $data['oEmpleado']->oUsuario = $this->Usuario_model->get_by_empleado_id($empleado_id);
            $data['lstSucursales'] = $this->Nube_sucursal_model->get_all_activos();
            $data['cargos'] = $this->Cargo_model->get_all();
            $data['tipos_contratos'] = $this->Multitabla_model->get_all_by_tipo(10);
            $data['bancos'] = $this->Multitabla_model->get_all_by_tipo(11);
            $data['lstRoles'] = $this->Usuario_model->get_all_roles();
            // echo json_encode($data['oEmpleado']);exit();
            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Planilla' ),
                        array('link' => '#', 'page' => 'Editar empleado' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Editar empleado', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'empleados' );
            $this->page_construct('planilla/empleados/editar', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('planilla/empleados'));
        }
    }
    function eliminar($empleado_id = 0){
         try {
            $this->session->unset_userdata('oEmpleado');
            
            if (count($this->Empleado_model->get_one($empleado_id)) == 0 ) {
                throw new Exception("No se encontró el empleado que busca.");
            }
            $data['oEmpleado'] = ($this->Empleado_model->get_one($empleado_id))[0];
            if (count($pagos = $this->Empleado_model->pagos_by_empleado($empleado_id)) > 0) {
                throw new Exception("No se puede eliminar al empleado, debido a que tiene relaciones.");
            }
            $rpta = $this->Empleado_model->eliminar_fisico($empleado_id);
            if(!$rpta){
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Problemas para eliminar al empleado.');
            }else{
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Empleado eliminado correctamente.");
            }
            redirect(base_url('planilla/empleados'));
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('planilla/empleados'));
        }
    }
    function cumpleanios(){
        $data['empleados'] = $this->Empleado_model->get_all_by_cumples();
        $data['proximos'] = $this->Site->proximos_cumples();
        // echo json_encode($data['empleados']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Planilla' ),
                    array('link' => '#', 'page' => 'Cumpleaños' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Cumpleaños', 'bc' => $bc, 'modulo'=>'planilla', 'item' => 'cumpleanios' );
        $this->page_construct('planilla/empleados/cumpleanios', $meta, $data);
    }

    function ajax_proximos_cumples(){
        $data['proximos'] = $this->Site->proximos_cumples();
        $this->partial_view('general/_modal_proximos_cumples',$data);
    }
    
    function inactivar($empleado_id = 0){
        try {
            if (count($this->Empleado_model->get_one($empleado_id)) == 0 ) {
                throw new Exception("No se encontró el empleado que busca.");
            }
            $oEmpleado = ($this->Empleado_model->get_one($empleado_id))[0];

            //Dr de baja usuario
            $oEmpleado->oUsuario =  $this->Usuario_model->get_by_empleado_id($oEmpleado->empleado_id);
            if($oEmpleado->oUsuario != null){
                $this->Usuario_model->bloquear($oEmpleado->oUsuario->usuario_id);
            }
            
            //Dar de baja empleado
            $rpta = $this->Empleado_model->inactivar($empleado_id);
            
            if(!$rpta){
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Problemas para eliminar al empleado.');
            }else{
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Empleado eliminado correctamente.");
            }
            redirect(base_url('planilla/empleados'));
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('planilla/empleados'));
        }
    }
}
