<?php defined('BASEPATH') or exit('No direct script access allowed');

class Planilla extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Empleado_model');
        $this->load->model('planilla/Empleado_pre_pago_model');
        $this->load->model('planilla/Empleado_pago_model');
        $this->load->model('Multitabla_model');
        $this->load->library('session');
    }

    function ajax_abrir_modal_registrar_prepago(){
        $empleado_id = $this->input->post('empleado_id');
        $cFecha = $this->input->post('fecha');
        $oEmpleado = $this->Empleado_model->get_one($empleado_id)[0];
        $oEmpleado->sueldo_diario = round(($oEmpleado->sueldo_base_mensual / 30), 2);

        $dFechaActual = date('Y-m-d');

        //Mostrar alertas
        $oWarning = null;
        if($dFechaActual > $cFecha){
            $oWarning = new stdClass();
            $oWarning->cMessage = "La fecha de registro es menor a la fecha actual.";
        }

        //Pasar datos
        $data['lstTiposItem'] = $this->Multitabla_model->get_all_by_tipo(45);
        $data['oEmpleado'] = $oEmpleado;
        $data['cFecha'] = $cFecha;
        $data['cFechaActual'] = $dFechaActual;
        $data["oWarning"] = $oWarning;
        $this->partial_view_sin_sesion('planilla/calendario_pagos/_modal_registrar_prepago',$data);
    }

    function ajax_abrir_modal_registrar_pago(){
        $empleado_id = $this->input->post('empleado_id');
        $cFecha = $this->input->post('fecha');
        $nImporte = $this->input->post('importe');
        $fechaInicioPeriodo = $this->input->post('fechaInicioPeriodo');
        $fechaFinPeriodo = $this->input->post('fechaFinPeriodo');

        //
        $oEmpleado = $this->Empleado_model->get_one($empleado_id)[0];
        $oEmpleado->sueldo_diario = round(($oEmpleado->sueldo_base_mensual / 30), 2);
        $oEmpleado->lstPrePagosPeriodo = $this->Empleado_pre_pago_model->get_prepagos_by_fechas(
                    $fechaInicioPeriodo,
                    $fechaFinPeriodo, 
                    $oEmpleado->empleado_id);

        $dFechaActual = date('Y-m-d');

        //Mostrar alertas
        $oWarning = null;
        if($dFechaActual > $cFecha){
            $oWarning = new stdClass();
            $oWarning->cMessage = "La fecha de registro es menor a la fecha actual.";
        }

        //Pasar datos
        $data['oEmpleado'] = $oEmpleado;
        $data['cFecha'] = $cFecha;
        $data['nImporte'] = $nImporte;
        $data['cFechaActual'] = $dFechaActual;
        $data['fechaInicioPeriodo'] = $fechaInicioPeriodo;
        $data['fechaFinPeriodo'] = $fechaFinPeriodo;
        $data["oWarning"] = $oWarning;
        $this->partial_view_sin_sesion('planilla/calendario_pagos/_modal_registrar_pago',$data);
    }

    function guardar_empleado_pago(){
        $reg_pago_empleado_id = $this->input->post('reg_pago_empleado_id');
        $reg_pago_fecha = $this->input->post('reg_pago_fecha');
        $reg_pago_periodo = $this->input->post('reg_pago_periodo');
        $reg_pago_importe_total = $this->input->post('reg_pago_importe_total');
        $prepagos_id = $this->input->post('reg_pago_array_prepagos');
        $reg_pago_bloquear = $this->input->post('reg_pago_bloquear');
        $reg_pago_bloquear = ($reg_pago_bloquear == null) ? 0:1;

        //Validar campos
        if($reg_pago_empleado_id == "" || $reg_pago_empleado_id == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Empleado no válido.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if($reg_pago_fecha ==  "" || $reg_pago_fecha == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Fecha no válida.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if($reg_pago_importe_total ==  "" || $reg_pago_importe_total == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Importe no válido.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Registrar
        $data = array(
            'empresa_id' => 1,
            'sucursal_id' => 1,
            'empleado_id' => $reg_pago_empleado_id,
            'fecha_pago' => $reg_pago_fecha,
            'importe_total' => $reg_pago_importe_total,
            'numero_items' => count($prepagos_id),
            'bloquear_periodos' => $reg_pago_bloquear,
            'estado' => 'A',
        );

        $pago_id = $this->Empleado_pago_model->insert($data);

        if(!$pago_id){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar el pago.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Pago registrado correctamente.");
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }


    function guardar_pre_pago(){
        $reg_prepag_empleado_id = $this->input->post('reg_prepag_empleado_id');
        $reg_prepag_fecha = $this->input->post('reg_prepag_fecha');
        $reg_prepag_nombres = $this->input->post('reg_prepag_nombres');
        $reg_prepag_tipo_item = $this->input->post('reg_prepag_tipo_item');
        $reg_prepag_importe = $this->input->post('reg_prepag_importe');
        $reg_prepag_motivo = $this->input->post('reg_prepag_motivo');

        //Validar campos
        if($reg_prepag_empleado_id == "" || $reg_prepag_empleado_id == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Empleado no válido.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if($reg_prepag_fecha ==  "" || $reg_prepag_fecha == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Fecha no válida.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if($reg_prepag_tipo_item == "" || $reg_prepag_tipo_item == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Tipo no válido.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if($reg_prepag_importe ==  "" || $reg_prepag_importe == "0"){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Importe no válido.');
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Validar si hay un pago despues, ya no se puede 


        //Registrar
        $data = array(
            'empleado_id' => $reg_prepag_empleado_id,
            'importe' => $reg_prepag_importe,
            'tipo_concepto' => $reg_prepag_tipo_item,
            'signo' => -1,
            'comentario' => $reg_prepag_motivo,
            'fecha_pago' => $reg_prepag_fecha,
            'estado' => 'A',
        );

        $prepago_id = $this->Empleado_pre_pago_model->insert($data);

        if(!$prepago_id){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar el item.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Item registrado correctamente.");
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

}
