<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

 
class Marcacion_api extends REST_Controller {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nube/Nube_marcacion_model');
    }

    function index_get(){
        echo 1 ;
    }

    function insert_marcaciones_post(){
        
        $lstMarcaciones = json_decode($this->post('lstMarcaciones'));

        if(!isset($lstMarcaciones)){
            $this->response(array('success' => FALSE, 'message' => "Debe indicar marcaciones.", "id" => 0));
        }

        //Eliminar
        foreach ($lstMarcaciones as $key => $oMarcacion) {
            $buscarMarcacion = $this->Nube_marcacion_model->get_one($oMarcacion->transaction_id);
            if($buscarMarcacion != null && count($buscarMarcacion) > 0){
                $this->Nube_marcacion_model->delete_by_id($oMarcacion->transaction_id);
            }
        }

        //Insertar
        foreach ($lstMarcaciones as $key => $oMarcacion) {
            
            $oMarcacionGuardar['transaction_id'] = $oMarcacion->transaction_id;
            $oMarcacionGuardar['emp_id'] = $oMarcacion->emp_id;
            $oMarcacionGuardar['emp_code'] = $oMarcacion->emp_code;
            $oMarcacionGuardar['first_name'] = $oMarcacion->first_name;
            $oMarcacionGuardar['photo'] = $oMarcacion->photo;

            $oMarcacionGuardar['department_id'] = $oMarcacion->department_id;
            $oMarcacionGuardar['create_time'] = $oMarcacion->create_time;
            $oMarcacionGuardar['punch_state'] = $oMarcacion->punch_state;
            $oMarcacionGuardar['punch_time'] = $oMarcacion->punch_time;
            $oMarcacionGuardar['upload_time'] = $oMarcacion->upload_time;

            $oMarcacionGuardar['registrado_por'] = "API";
            $oMarcacionGuardar['fecha_registro'] = date("Y/m/d H:i:s");

            $this->Nube_marcacion_model->insert($oMarcacionGuardar);
        }

        $this->response(array('success' => TRUE, 'message' => "Registro exitoso", "id" => ""));
    }
    
   
}