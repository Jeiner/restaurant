<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

 
class Pago_api extends REST_Controller {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nube/Nube_pago_model');
        $this->load->model('nube/Nube_functions_model');
    }

    function index_get(){
        echo 1 ;
    }

    function insert_pago_post(){
        $data['empresa_codigo'] = $this->post('empresa_codigo');
        $data['sucursal_codigo'] = $this->post('sucursal_codigo');
        $data['comanda_codigo'] = $this->post('comanda_codigo');
        $data['pago_codigo'] = $this->post('pago_codigo');
        $data['fecha_registro_nube'] = date("Y/m/d H:i:s");

        $data['pago_id'] = $this->post('pago_id');
        $data['comanda_id'] = $this->post('comanda_id');
        $data['importe_total'] = $this->post('importe_total');
        $data['descuento'] = $this->post('descuento');
        $data['total_a_pagar'] = $this->post('total_a_pagar');
        $data['pago_efectivo'] = $this->post('pago_efectivo');
        $data['pago_tarjeta'] = $this->post('pago_tarjeta');
        $data['total_pagado'] = $this->post('total_pagado');
        $data['saldo'] = $this->post('saldo');
        $data['efectivo'] = $this->post('efectivo');
        $data['vuelto'] = $this->post('vuelto');
        $data['fecha_pago'] = $this->post('fecha_pago');
        $data['cajero'] = $this->post('cajero');
        $data['tipo_comprobante'] = $this->post('tipo_comprobante');
        $data['serie'] = $this->post('serie');
        $data['correlativo'] = $this->post('correlativo');
        $data['cliente_id'] = $this->post('cliente_id');
        $data['cliente_documento'] = $this->post('cliente_documento');
        $data['cliente_nombre'] = $this->post('cliente_nombre');
        $data['cliente_direccion'] = $this->post('cliente_direccion');
        $data['cliente_telefono'] = $this->post('cliente_telefono');
        $data['caja_id'] = $this->post('caja_id');
        $data['observaciones'] = $this->post('observaciones');
        $data['estado'] = $this->post('estado');
        $data['anulado_en'] = $this->post('anulado_en');
        $data['anulado_por'] = $this->post('anulado_por');
        $data['fecha_registro'] = $this->post('fecha_registro');
        $data['registrado_por'] = $this->post('registrado_por');
        $data['fecha_modificacion'] = $this->post('fecha_modificacion');
        $data['modificado_por'] = $this->post('modificado_por');
        $data['fecha_caja_apertura'] = $this->post('fecha_caja_apertura');

        //Registrar la trama
        $this->Nube_functions_model->registrar_trama($data, NUBE_PAGO);
        
        //Verificamos que se hayan enviado todos los datos
        $data_invalida = FALSE;
        foreach ($data as $key => $campo) {
            if(is_null($key)){
                $data_invalida = TRUE;
                $message = "Datos del comprobante no válidos: ".$key;
                break;
            }
        }
        if($data_invalida)
        {
            $this->response(array('success' => FALSE, 'message' => $message, "id" => 0));
        }
        
        // if(!isset($cToken)){
        //     $this->response(array('success' => FALSE, 'message' => "Token no válido.", "id" => 0));
        // }


        //Eliminar
        $this->Nube_pago_model->delete_by_codigo_pago($data['pago_codigo']);

        //Insertamos el comprobante electrónico
        $this->Nube_pago_model->insert($data);
        $this->response(array('success' => TRUE, 'message' => "Registro exitoso", "id" => ""));
    }
    
   
}