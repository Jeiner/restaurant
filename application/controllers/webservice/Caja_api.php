<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

 
class Caja_api extends REST_Controller {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nube/Nube_caja_model');
        $this->load->model('nube/Nube_functions_model');
    }

    function index_get(){
        echo 1 ;
    }

    function insert_caja_post(){
        $data['empresa_codigo'] = $this->post('empresa_codigo');
        $data['sucursal_codigo'] = $this->post('sucursal_codigo');
        $data['caja_codigo'] = $this->post('caja_codigo');
        $data['fecha_registro_nube'] = date("Y/m/d H:i:s");

        $data['caja_id'] = $this->post('caja_id');
        $data['aperturado_en'] = $this->post('aperturado_en');
        $data['aperturado_por'] = $this->post('aperturado_por');
        $data['monto_inicial'] = $this->post('monto_inicial');
        $data['cerrado_en'] = $this->post('cerrado_en');
        $data['cerrado_por'] = $this->post('cerrado_por');
        $data['total_ventas_efectivo'] = $this->post('total_ventas_efectivo');
        $data['total_gastos_efectivo'] = $this->post('total_gastos_efectivo');
        $data['saldo_caja'] = $this->post('saldo_caja');
        $data['total_ventas_tarjeta'] = $this->post('total_ventas_tarjeta');
        $data['total_gastos_tarjeta'] = $this->post('total_gastos_tarjeta');
        $data['monto_retirado'] = $this->post('monto_retirado');
        $data['remanente'] = $this->post('remanente');
        $data['observaciones'] = $this->post('observaciones');
        $data['estado'] = $this->post('estado');
        $data['fecha_registro'] = $this->post('fecha_registro');
        $data['registrado_por'] = $this->post('registrado_por');
        $data['fecha_modificacion'] = $this->post('fecha_modificacion');
        $data['modificado_por'] = $this->post('modificado_por');
        $data['fecha_sync_marcaciones'] = $this->post('fecha_sync_marcaciones');
        $data['fecha_sync_comandas'] = $this->post('fecha_sync_comandas');
        $data['fecha_procesar_cpbe'] = $this->post('fecha_procesar_cpbe');

        //Registrar la trama
        $this->Nube_functions_model->registrar_trama($data, NUBE_CAJA);

        //Verificamos que se hayan enviado todos los datos
        $data_invalida = FALSE;
        foreach ($data as $key => $campo) {
            if(is_null($key)){
                $data_invalida = TRUE;
                $message = "Datos de la Caja no válidos: ".$key;
                break;
            }
        }
        if($data_invalida)
        {
            $this->response(array('success' => FALSE, 'message' => $message, "id" => 0));
        }
        
        // if(!isset($cToken)){
        //     $this->response(array('success' => FALSE, 'message' => "Token no válido.", "id" => 0));
        // }

        //Eliminar
        $this->Nube_caja_model->delete_by_codigo_caja($data['caja_codigo']);

        //Cerrar cajas anteriores Abiertas por codigo de empresa y sucursal
        $this->Nube_caja_model->closed_previous($data['empresa_codigo'], $data['sucursal_codigo']);
        
        //Insertamos Caja
        $this->Nube_caja_model->insert($data);
        $this->response(array('success' => TRUE, 'message' => "Registro exitoso", "id" => ""));
    }
    
   
}