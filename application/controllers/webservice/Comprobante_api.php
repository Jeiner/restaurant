<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

 
class Comprobante_api extends REST_Controller {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nube/Nube_comprobante_model');
        $this->load->model('nube/Nube_functions_model');
    }

    function index_get(){
        echo 1 ;
    }

    function insert_comprobante_post(){
        $data['empresa_codigo'] = $this->post('empresa_codigo');
        $data['sucursal_codigo'] = $this->post('sucursal_codigo');
        $data['comanda_codigo'] = $this->post('comanda_codigo');
        $data['comprobante_codigo'] = $this->post('comprobante_codigo');
        $data['fecha_registro_nube'] = date("Y/m/d H:i:s");

        $data['comanda_id'] = $this->post('comanda_id');
        $data['pago_id'] = $this->post('pago_id');
        $data['cabecera_id'] = $this->post('cabecera_id');
        $data['tipoComprobante'] = $this->post('tipoComprobante');
        $data['serie'] = $this->post('serie');
        $data['correlativo'] = $this->post('correlativo');
        $data['tipoOperacion'] = $this->post('tipoOperacion');
        $data['fecEmision'] = $this->post('fecEmision');
        $data['horEmision'] = $this->post('horEmision');
        $data['fecVencimiento'] = $this->post('fecVencimiento');
        $data['codLocalEmisor'] = $this->post('codLocalEmisor');
        $data['tipDocUsuario'] = $this->post('tipDocUsuario');
        $data['numDocUsuario'] = $this->post('numDocUsuario');
        $data['rznSocialUsuario'] = $this->post('rznSocialUsuario');
        $data['tipMoneda'] = $this->post('tipMoneda');
        $data['sumTotTributos'] = $this->post('sumTotTributos');
        $data['sumTotValVenta'] = $this->post('sumTotValVenta');
        $data['sumPrecioVenta'] = $this->post('sumPrecioVenta');
        $data['sumDescTotal'] = $this->post('sumDescTotal');
        $data['sumOtrosCargos'] = $this->post('sumOtrosCargos');
        $data['sumTotalAnticipos'] = $this->post('sumTotalAnticipos');
        $data['sumImpVenta'] = $this->post('sumImpVenta');
        $data['ublVersionId'] = $this->post('ublVersionId');
        $data['customizationId'] = $this->post('customizationId');
        $data['codHash'] = $this->post('codHash');
        $data['estadoSunat'] = $this->post('estadoSunat');
        $aItems = json_decode($this->post('aItems'));

        //Registrar la trama
        $this->Nube_functions_model->registrar_trama($data, NUBE_COMPROBANTE_E);
        
        //Verificamos que se hayan enviado todos los datos
        $data_invalida = FALSE;
        foreach ($data as $key => $campo) {
            if(is_null($key)){
                $data_invalida = TRUE;
                $message = "Datos del comprobante no válidos: ".$key;
                break;
            }
        }
        if($data_invalida)
        {
            $this->response(array('success' => FALSE, 'message' => $message, "id" => 0));
        }
        
        if(!isset($aItems)){
            $this->response(array('success' => FALSE, 'message' => "Debe indicar items del comprobante.", "id" => 0));
        }

        // if(!isset($cToken)){
        //     $this->response(array('success' => FALSE, 'message' => "Token no válido.", "id" => 0));
        // }


        //Eliminar
        $this->Nube_comprobante_model->delete_by_codigo_comprobante($data['comprobante_codigo']);

        //Insertamos el comprobante electrónico
        $this->Nube_comprobante_model->insert($data, $aItems);
        $this->response(array('success' => TRUE, 'message' => "Registro exitoso", "id" => ""));
    }
    
   
}