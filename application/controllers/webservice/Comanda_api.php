<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

 
class Comanda_api extends REST_Controller {
  
    public function __construct()
    {
        parent::__construct();
        $this->load->model('nube/Nube_comanda_model');
        $this->load->model('nube/Nube_functions_model');
    }

    function index_get(){
        echo 1 ;
    }

    function insert_comanda_post(){
        $data['empresa_codigo'] = $this->post('empresa_codigo');
        $data['sucursal_codigo'] = $this->post('sucursal_codigo');
        $data['comanda_codigo'] = $this->post('comanda_codigo');
        $data['fecha_registro_nube'] = date("Y/m/d H:i:s");

        $data['comanda_id'] = $this->post('comanda_id');
        $data['modalidad'] = $this->post('modalidad');
        $data['fecha_comanda'] = $this->post('fecha_comanda');
        $data['mozo'] = $this->post('mozo');
        $data['mesa_id'] = $this->post('mesa_id');
        $data['mesa'] = $this->post('mesa');
        $data['nro_comensales'] = $this->post('nro_comensales');
        $data['prioridad'] = $this->post('prioridad');
        $data['nro_productos'] = $this->post('nro_productos');
        $data['pago_id'] = $this->post('pago_id');
        $data['tasa_IGV'] = $this->post('tasa_IGV');
        $data['total_IGV'] = $this->post('total_IGV');
        $data['importe_sin_IGV'] = $this->post('importe_sin_IGV');
        $data['importe_total'] = $this->post('importe_total');
        $data['descuento'] = $this->post('descuento');
        $data['total_pagado'] = $this->post('total_pagado');
        $data['fecha_pago'] = $this->post('fecha_pago');
        $data['pagado_por'] = $this->post('pagado_por');
        $data['tipo_comprobante'] = $this->post('tipo_comprobante');
        $data['cliente_id'] = $this->post('cliente_id');
        $data['cliente_documento'] = $this->post('cliente_documento');
        $data['cliente_nombre'] = $this->post('cliente_nombre');
        $data['cliente_direccion'] = $this->post('cliente_direccion');
        $data['cliente_telefono'] = $this->post('cliente_telefono');
        $data['union_mesas'] = $this->post('union_mesas');
        $data['caja_id'] = $this->post('caja_id');
        $data['estado_atencion'] = $this->post('estado_atencion');
        $data['estado_pago'] = $this->post('estado_pago');
        $data['atendida_en'] = $this->post('atendida_en');
        $data['prefacturada_en'] = $this->post('prefacturada_en');
        $data['prefacturada_por'] = $this->post('prefacturada_por');
        $data['finalizada_en'] = $this->post('finalizada_en');
        $data['finalizada_por'] = $this->post('finalizada_por');
        $data['anulado_en'] = $this->post('anulado_en');
        $data['anulado_por'] = $this->post('anulado_por');
        $data['anulado_motivo'] = $this->post('anulado_motivo');
        $data['fecha_registro'] = $this->post('fecha_registro');
        $data['registrado_por'] = $this->post('registrado_por');
        $data['fecha_modificacion'] = $this->post('fecha_modificacion');
        $data['modificado_por'] = $this->post('modificado_por');
        $data['comentario'] = $this->post('comentario');
        $data['cmd_comentario'] = $this->post('cmd_comentario');
        $data['sync'] = $this->post('sync');
        $data['fecha_sync'] = $this->post('fecha_sync');
        $data['con_comprobante_e'] = $this->post('con_comprobante_e');
        $data['correlativo'] = $this->post('correlativo');
        $data['cliente_tipo_documento'] = $this->post('cliente_tipo_documento');
        $data['cliente_correo'] = $this->post('cliente_correo');
        $data['canal'] = $this->post('canal');
        $data['fecha_caja_apertura'] = $this->post('fecha_caja_apertura');
        $aItems = json_decode($this->post('aItems'));

        //Registrar la trama
        $this->Nube_functions_model->registrar_trama($data, NUBE_COMANDA);

        //Verificamos que se hayan enviado todos los datos
        $data_invalida = FALSE;
        foreach ($data as $key => $campo) {
            if(is_null($key)){
                $data_invalida = TRUE;
                $message = "Datos del comprobante no válidos: ".$key;
                break;
            }
        }
        if($data_invalida)
        {
            $this->response(array('success' => FALSE, 'message' => $message, "id" => 0));
        }
        
        if(!isset($aItems)){
            $this->response(array('success' => FALSE, 'message' => "Debe indicar items del comprobante.", "id" => 0));
        }

        // if(!isset($cToken)){
        //     $this->response(array('success' => FALSE, 'message' => "Token no válido.", "id" => 0));
        // }


        //Eliminar
        $this->Nube_comanda_model->delete_by_codigo_comanda($data['comanda_codigo']);

        //Insertamos el comprobante electrónico
        $this->Nube_comanda_model->insert($data, $aItems);
        $this->response(array('success' => TRUE, 'message' => "Registro exitoso", "id" => ""));
    }
    
   
}