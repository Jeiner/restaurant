<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sucursales_crud extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Generalidades_model');
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->library('session');
    }

    function crud_guardar_sucursal_create($oDataSuc){
        //Existe la sucursal
        $buscarSucursal = $this->Nube_sucursal_model->get_by_codigo($oDataSuc['sucursal_codigo']);
        if(count($buscarSucursal) > 0){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El código de sucursal ya existe:".$oDataSuc['empresa_codigo']."-".$oDataSuc['sucursal_codigo']);
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $buscarSucursal = $this->Nube_sucursal_model->get_one_by_nombre($oDataSuc['sucursal_nombre']);
        if(count($buscarSucursal) > 0){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El nombre de sucursal ya existe: ".$oDataSuc['sucursal_nombre']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Registrar
        $rpta = $this->Nube_sucursal_model->insert($oDataSuc);
        if(!$rpta){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar la sucursal.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Sucursal registrada correctamente.");
        }
    }

    function crud_guardar_sucursal_update($oDataSuc){
        //Existe la sucursal
        $buscarSucursal = $this->Nube_sucursal_model->get_by_codigo($oDataSuc['sucursal_codigo']);
        if(count($buscarSucursal) <= 0){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El código de sucursal no existe:".$oDataSuc['empresa_codigo']."-".$oDataSuc['sucursal_codigo']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Actualizar
        $rpta = $this->Nube_sucursal_model->update($oDataSuc);
        if(!$rpta){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para actualizar la sucursal.');
            redirect($_SERVER["HTTP_REFERER"]);
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Sucursal actualizada correctamente.");
            $ruta_nueva = base_url('configuracion/sucursales/index');
            redirect($ruta_nueva);
        }
    }

    function crud_guardar_sucursal(){
        $this->loaders->verifica_sesion();
        $oDataSuc = array(
                        'empresa_codigo' => trim($this->input->post('empresa_codigo')),
                        'empresa_nombre' => $this->input->post('empresa_nombre'),
                        'sucursal_codigo' => trim($this->input->post('sucursal_codigo')),
                        'sucursal_nombre' => $this->input->post('sucursal_nombre'),
                        'ip_local' => $this->input->post('ip_local'),
                        'lugar' => $this->input->post('lugar'),
                        'estado' => $this->input->post('estado')
                    );

        $this->form_validation->set_rules('sucursal_codigo', "Código Sucursal", 'required');
        $this->form_validation->set_rules('empresa_codigo', "Código Empresa", 'required');
        $this->form_validation->set_rules('empresa_nombre', "Empresa", 'required');
        $this->form_validation->set_rules('sucursal_nombre', "Sucursal", 'required');
        $this->form_validation->set_rules('estado', "Estado", 'required');

        $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

        // echo json_encode($oDataSuc);
        // exit();

        //Registrar
        if ($this->form_validation->run() == true) {
            if($this->input->post('CRUD') == "C"){
                $this->crud_guardar_sucursal_create($oDataSuc);
            }else{
                $this->crud_guardar_sucursal_update($oDataSuc);
            }
        }else{
            $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
            $this->session->set_flashdata('message',$message);
            $this->session->set_flashdata('success',false);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
}
