<?php defined('BASEPATH') or exit('No direct script access allowed');

class Validaciones extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
        $this->load->library('session');
    }
    
    function index(){
        $data['lstValidaciones'] = $this->Validaciones_model->get_all();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Validaciones' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Validaciones', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'validaciones', 'select_tab' => 'validaciones' );
        $this->page_construct('configuracion/generalidades/validaciones', $meta, $data);
    }
}
