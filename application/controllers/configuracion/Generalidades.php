<?php defined('BASEPATH') or exit('No direct script access allowed');

class Generalidades extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Generalidades_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function index(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Generalidades' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Generalidades', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'generalidades', 'select_tab' => 'generalidades' );
        $this->page_construct('configuracion/generalidades/index', $meta, null);
    }
    function actualizar(){
        $this->loaders->verifica_sesion();
        $data = array(  'generalidades_id' => $this->input->post('generalidades_id'),
                        'RUC' => $this->input->post('RUC'),
                        'razon_social' => $this->input->post('razon_social'),
                        'actividad' => $this->input->post('actividad'),
                        'slogan' => $this->input->post('slogan'),
                        'nombre_comercial' => $this->input->post('nombre_comercial'),
                        'local' => $this->input->post('local'),
                        'direccion' => $this->input->post('direccion'),
                        'ubigeo' => $this->input->post('ubigeo'),
                        'contacto' => $this->input->post('contacto'),
                        'telefono_1' => $this->input->post('telefono_1'),
                        'telefono_2' => $this->input->post('telefono_2'),
                        'correo' => $this->input->post('correo'),

                        'tipo_sistema' => $this->input->post('tipo_sistema'),
                        'titulo_sistema' => $this->input->post('titulo_sistema'),
                        'url_icon' => $this->input->post('url_icon'),
                        'url_logo_login' => $this->input->post('url_logo_login'),
                        'tiene_delivery_online' => boolval($this->input->post('tiene_delivery_online')),
                        'emitir_cpbe' => boolval($this->input->post('emitir_cpbe')),
                        'api_cpbe_base' => $this->input->post('api_cpbe_base'),
                        'pagina_web' => $this->input->post('pagina_web'),
                        'token_facturador' => $this->input->post('token_facturador'),
                        'api_get_pedidos_online' => $this->input->post('api_get_pedidos_online'),
                    );
        $this->form_validation->set_rules('razon_social', "Razón social", 'required');
        $this->form_validation->set_rules('RUC', "RUC", 'required|min_length[11]');
        $this->form_validation->set_rules('telefono_1', "Teléfono", 'required');

        $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
        $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

        if ($this->form_validation->run() == true) {
            $rpta = $this->Generalidades_model->update($data);
            if(!$rpta){
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Problemas para actualizar las generalidades.');
            }else{
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Generalidades actualizada correctamente.");
            }
        }else{
            $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
            $this->session->set_flashdata('message',$message);
            $this->session->set_flashdata('success',false);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function seguridad(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Generalidades' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Generalidades', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'generalidades', 'select_tab' => 'seguridad' );
        $this->page_construct('configuracion/generalidades/seguridad', $meta, null);
    }
    function verificar_codigo_de_seguridad(){
        $this->loaders->verifica_sesion_ajax();
        $codigo =  md5($_POST['codigo']);
        $generalidades = ($this->Generalidades_model->get_one())[0];
        if($codigo == $generalidades->codigo_seguridad){
            echo  1;
        }else{
            echo  0;
        }
    }
    function actualizar_codigo_seguridad(){
        try {
            $this->loaders->verifica_sesion();
            $data = array(  
                'codigo_seguridad' => $this->input->post('codigo_seguridad'),
                'codigo_seguridad_2' => $this->input->post('codigo_seguridad_2'),
            );
            if($data['codigo_seguridad'] != $data['codigo_seguridad_2']){
                throw new Exception("Los códigos ingresados no coinciden.");
            }else{
                $this->form_validation->set_rules('codigo_seguridad', "Código de seguridad", 'required|min_length[4]');
                $this->form_validation->set_rules('codigo_seguridad_2', "Código de seguridad", 'required|min_length[4]');

                $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
                $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

                if ($this->form_validation->run() == true) {
                    $rpta = $this->Generalidades_model->actualizar_codigo_seguridad($data);
                    if(!$rpta){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para actualizar las el código de seguridad.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Código de seguridad actualizado correctamente.");
                    }
                }else{
                    $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                    $this->session->set_flashdata('message',$message);
                    $this->session->set_flashdata('success',false);
                }
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}
