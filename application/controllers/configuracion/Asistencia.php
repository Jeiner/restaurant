<?php defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Asistencia extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
        $this->load->model('Marcacion_model');
        $this->load->model('Usuario_model');
        $this->load->model('Site');
        $this->load->library('session');
    }

    function index(){
        $usuario_id = $this->input->get('usuario_id');
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');

        $data['usuario_id'] = ($usuario_id == null || $usuario_id == "")? 0 : $usuario_id;
        $data['fecha_inicio'] = ($fecha_inicio == null || $fecha_inicio == "")? null : $fecha_inicio;
        $data['fecha_fin'] = ($fecha_fin == null || $fecha_fin == "")? null : $fecha_fin;


        $data['lstUsuarios'] = $this->Usuario_model->get_all_order_nombre();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Asistencia' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Asistencia', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'asistencia' );
        $this->page_construct('configuracion/asistencia/index', $meta, $data);
    }

    function asignar_horario(){
        $data['lstValidaciones'] = $this->Validaciones_model->obtener_variables_array_nombre();
        $data['lstUsuarios'] = $this->Usuario_model->get_all_activos_order_nombre();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Asignar horario' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Asignar horario', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'asistencia' );
        $this->page_construct('configuracion/asistencia/asignar_horario', $meta, $data);
    }

    function guardar_horario(){

        try {
            $nro_usuarios = isset($_POST['usuario_id']) ? sizeof($_POST['usuario_id']) : 0; 
            if( $nro_usuarios == 0 ){
                throw new Exception("No se encontraron usuario.");
            }

            for ($r = 0; $r < $nro_usuarios; $r++) {
                $item = array(
                    'usuario_id' => $_POST['usuario_id'][$r],
                    'dni' => $_POST['dni'][$r],
                    'id_usuario_asistencia' => $_POST['id_usuario_asistencia'][$r],
                    'hora_entrada' => $_POST['hora_entrada'][$r],
                    'hora_salida' => $_POST['hora_salida'][$r],
                    'fecha_modificacion' => date("Y/m/d H:i:s"),
                    'modificado_por' => $this->session->userdata('session_usuario'),
                );

                if(strlen(trim($item['id_usuario_asistencia'])) < 4){
                    $lstMensajes[] = "DNI: ".$item['dni']." ID de asistencia no válido.";
                    continue;
                }

                if(strlen(trim($item['hora_entrada'])) != 5 || strlen(trim($item['hora_salida'])) != 5){
                    $lstMensajes[] = "DNI: ".$item['dni']." Hora de entrada o salida no válidos";
                    continue;
                }

                //Validar formato de hora
                $pattern="/^([0-1][0-9]|[2][0-3])[\:]([0-5][0-9])$/";
                if(!preg_match($pattern,$item['hora_entrada'])){
                    $lstMensajes[] = "DNI: ".$item['dni']." Formato de hora de entrada no válido";
                    continue;
                }
                if(!preg_match($pattern,$item['hora_salida'])){
                    $lstMensajes[] = "DNI: ".$item['dni']." Formato de hora de salida no válido";
                    continue;
                }

                $this->Usuario_model->update_asignar_horario($item);
            }

            //Devolver mensaje
            $lstMensajes = ($lstMensajes == null) ? [] : $lstMensajes;
            $mensajeFinal = "";
            foreach ($lstMensajes as $key => $oMensaje) {
                $mensajeFinal = $mensajeFinal.$oMensaje."<br>";
            }

            if(count($lstMensajes) > 0){
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', $mensajeFinal);
            }else{
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Horario guardado correctamente.");
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            
        }
    }

    function cargar_tabla_asistencias_by_fechas(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $filtro_usuario = $this->input->post('filtro_usuario');

        //Buscar usuarios
        $bsucarUsuario = $this->Usuario_model->get_one($filtro_usuario);
        $oUsuario = $bsucarUsuario[0];

        //Buscar asistencias
        $lstMarcaciones = $this->Marcacion_model->get_all_by_fechas($start_date, $end_date, $oUsuario->id_usuario_asistencia);
        
        //Recorrer fechas
        $fecha_incio = strtotime($start_date);
        $fecha_fin = strtotime($end_date);
        $contador = 0;
        for($i=$fecha_incio; $i<=$fecha_fin; $i+=86400){
            $oAsistencia = $this->crear_objeto_asistencia_por_dia_usuario(date("Y-m-d", $i), $oUsuario, $lstMarcaciones);
            $oAsistencia->index = $contador++;
            $lstAsistencias[] = $oAsistencia;

            //Verificar si hay marcaciones pasado la media noche
            foreach ($oAsistencia->marcaciones as $key => $oMarcacion) {
                if($oMarcacion->hora > "00:00" && $oMarcacion->hora < "04:00"){
                    $indexAnterior = count($lstAsistencias) - 2;
                    if($indexAnterior >= 0){
                        $oAsistenciaAnterior = $lstAsistencias[$indexAnterior];
                        if($oAsistenciaAnterior != null){
                            $oAsistenciaAnterior->hora_salida = $oMarcacion->hora;
                            $oAsistenciaAnterior->dif_salida = 0;
                            $oAsistenciaAnterior->estado_salida = 1;
                        }
                    }
                }
            }
        }

        $lstValidaciones = $this->Validaciones_model->obtener_variables_array_nombre();

        $data['oUsuario'] = $oUsuario;
        $data['lstAsistencias'] = $lstAsistencias;
        $data['lstValidaciones'] = $lstValidaciones;
        $this->partial_view('configuracion/asistencia/_tabla_asistencias_by_fechas',$data);
    }

    function crear_objeto_asistencia_por_dia_usuario($cFecha, $oUsuario, $lstMarcaciones){
        //Obtener minutos de tolerancia
        $lstValidaciones = $this->Validaciones_model->obtener_variables_array_nombre();
        $minTolEntrada = $lstValidaciones['min_tol_entrada'];         //Hasta "N" minutos
        $minTolSalida = ($lstValidaciones['min_tol_salida'] * -1);     //Antes de "N" minutos
        $horaIniciarMarcacionIngreso = "14:00";
        $horaIniciarMarcacionSalida = "21:00";

        //Iniciar variables
        $lstMarcacionesUsuario = null;
        $lstComentarios = null;

        $cHoraEntrada = "";
        $diffMinEntrada = 999;

        $cHoraSalida = "";
        $diffMinSalida = -999;

        //Encontrar marcaciones
        foreach ($lstMarcaciones as $key => $oMarcacion) {
            if($oMarcacion->fecha == $cFecha){
                $lstMarcacionesUsuario[] = $oMarcacion;

                //Hora entrada (Tomar la hora menor)
                if($oMarcacion->hora > $horaIniciarMarcacionIngreso){
                    if($cHoraEntrada == "" || $oMarcacion->hora < $cHoraEntrada){
                        $cHoraEntrada = $oMarcacion->hora;
                    }
                }

                //Hora salida (Tomar la hora mayor)
                if($oMarcacion->hora > $horaIniciarMarcacionSalida){
                    if($cHoraSalida == "" || $oMarcacion->hora > $cHoraSalida){
                        $cHoraSalida = $oMarcacion->hora;
                    }
                }
            }
        }

        //Calcular diferencia de entrada
        if($cHoraEntrada != ""){
            $diffMinEntrada = $this->Site->minutos_transcurridos_con_signo($cFecha." ".$cHoraEntrada, $cFecha." ".$oUsuario->hora_entrada);
        }

        ///Calcular diferencia de salida
        if($cHoraSalida != ""){
            $diffMinSalida = $this->Site->minutos_transcurridos_con_signo($cFecha." ".$cHoraSalida, $cFecha." ".$oUsuario->hora_salida);
        }

        //Devolver objeto
        $oAsistencia = new stdClass();
        $oAsistencia->index = 0;
        $oAsistencia->nombre_dia = $this->Site->dias[date("l", strtotime($cFecha))];
        $oAsistencia->fecha = $cFecha;
        $oAsistencia->hora_entrada_deseada = $oUsuario->hora_entrada;
        $oAsistencia->hora_entrada = $cHoraEntrada;
        $oAsistencia->dif_entrada = ($diffMinEntrada > $minTolEntrada) ? ($diffMinEntrada - $minTolEntrada) : 0;
        $oAsistencia->estado_entrada = ($diffMinEntrada > $minTolEntrada)? -1 : 1;

        $oAsistencia->hora_salida_deseada = $oUsuario->hora_salida;
        $oAsistencia->hora_salida = $cHoraSalida;
        $oAsistencia->dif_salida = $diffMinSalida;
        $oAsistencia->estado_salida = ($diffMinSalida < $minTolSalida)? -1 : 1;

        $oAsistencia->marcaciones = ($lstMarcacionesUsuario == null)? [] : $lstMarcacionesUsuario;

        //Agregar comentarios
        // if($diffMinEntrada != 999 && $diffMinEntrada > $minTolEntrada){
        //     $lstComentarios[] = "Diferencia en Entrada: ".$diffMinEntrada." min";
        // }

        // if($diffMinSalida != -999 && $diffMinSalida < $minTolSalida){
        //     $lstComentarios[] = "Diferencia en Salida: ".$diffMinSalida." min";
        // }

        $oAsistencia->comentarios = ($lstComentarios == null)? [] : $lstComentarios;

        return $oAsistencia;
    }

    function cargar_asistencia(){
        $data['lstValidaciones'] = $this->Validaciones_model->get_all();
        $buscarUltimaCarga = $this->Marcacion_model->get_ultima_fecha_carga();
        $data['oUltimaCarga'] = null;
        
        if(count($buscarUltimaCarga) > 0){
            $oUltimaCarga = $buscarUltimaCarga[0];
            $data['oUltimaCarga'] = $oUltimaCarga;
        }

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Validaciones' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Validaciones', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'validaciones' );
        $this->page_construct('configuracion/asistencia/cargar_asistencia', $meta, $data);
    }

    function importar_asistencias(){

        try {
            $config = array(
                'upload_path' => "./files/",
                'allowed_types' => "xlsx|xls",
                'overwrite' => TRUE,
                'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "768",
                'max_width' => "1024"
            );
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('archivo_excel'))
            {   
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            }
            else
            {
                $path       = 'files/';
                $file_data  = $this->upload->data();
                $file_name  = $path.$file_data['file_name'];
                $arr_file   = explode('.', $file_name);
                $extension  = end($arr_file);

                $reader     = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                $spreadsheet    = $reader->load($file_name);
                $sheet_data     = $spreadsheet->getActiveSheet()->toArray();

                $list           = [];

                //Leer columnas del archivo
                foreach($sheet_data as $key => $val) {
                    if($key > 1) {
                        $list [] = [
                                'id_usuario_asistencia' => $val[0],
                                'nombre'        => $val[1],
                                'departamento'  => $val[2],
                                'fecha'         => $val[3],
                                'hora'          => $val[4],
                                'estado'        => '',
                                'area'          => $val[6],
                                'serie'         => '',
                                'dispositivo'   => '',
                                'carga'         => $val[9],
                            ];
                    }
                }

                //Registrar marcación
                foreach ($list as $key => $oMarcacion) {
                    //Validar si ya esta registrado
                    $buscarMaracion = $this->Marcacion_model->get_by_usuario_fecha_hora($oMarcacion['id_usuario_asistencia'], $oMarcacion['fecha'], $oMarcacion['hora']);
                    if($buscarMaracion == null || count($buscarMaracion) == 0){
                        //insertar marcación
                        $this->Marcacion_model->insert($oMarcacion);
                    }
                }
            }

            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Archivo cargado correctamente.");
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function ajax_cargar_modal_asistencia(){
        $usuario_id =  $_POST['usuario_id'];
        
        $data['lstValidaciones'] = $this->Validaciones_model->obtener_variables_array_nombre();
        $data['lstUsuarios'] = $this->Usuario_model->get_all_order_nombre();

        //Buscar usuarios
        $bsucarUsuario = $this->Usuario_model->get_one($usuario_id);
        $oUsuario = $bsucarUsuario[0];

        $data['oUsuario'] = $oUsuario;
        $data['usuario_id'] = $usuario_id;
        $this->partial_view('configuracion/asistencia/_modal_ver_asistencia',$data);
    }

    function marcaciones(){
        $usuario_id = $this->input->get('usuario_id');
        $fecha_inicio = $this->input->get('fecha_inicio');
        $fecha_fin = $this->input->get('fecha_fin');

        $data['usuario_id'] = ($usuario_id == null || $usuario_id == "")? 0 : $usuario_id;
        $data['fecha_inicio'] = ($fecha_inicio == null || $fecha_inicio == "")? null : $fecha_inicio;
        $data['fecha_fin'] = ($fecha_fin == null || $fecha_fin == "")? null : $fecha_fin;


        $data['lstUsuarios'] = $this->Usuario_model->get_all_order_nombre();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Asistencia' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Asistencia', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'asistencia' );
        $this->page_construct('configuracion/asistencia/marcaciones', $meta, $data);
    }

    function cargar_tabla_marcaciones_by_fechas(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $filtro_usuario = $this->input->post('filtro_usuario');

        $filtro_id_usuario_asistencia = "*"; 
        //Buscar usuarios
        $bsucarUsuario = $this->Usuario_model->get_one($filtro_usuario);
        if(count($bsucarUsuario) <= 0){
            $filtro_id_usuario_asistencia = "*";
        }else{
            $oUsuario = $bsucarUsuario[0];
            $filtro_id_usuario_asistencia = $oUsuario->id_usuario_asistencia;
        }

        //Buscar asistencias
        $lstMarcaciones = $this->Marcacion_model->get_all_by_fechas($start_date, $end_date, $filtro_id_usuario_asistencia);

        $data['lstMarcaciones'] = $lstMarcaciones;
        $this->partial_view('configuracion/asistencia/_tabla_marcaciones_by_fechas',$data);
    }
}
