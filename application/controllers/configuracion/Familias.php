<?php defined('BASEPATH') or exit('No direct script access allowed');

class Familias extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Familia_model');
        $this->load->model('Producto_model');
        $this->load->library('session');
    }
    function index(){
        $data['oFamilia'] =  new Familia_model();
        $data['familias'] = $this->Familia_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Gestionar familias' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Familias de productos', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'familias' );
        $this->page_construct('configuracion/familias/index', $meta, $data);
    }
    function editar($familia_id){
        $oFamilia = $this->Familia_model->get_one($familia_id);
        $data['oFamilia'] = $oFamilia[0];
        $data['familias'] = $this->Familia_model->get_all();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Gestionar familias' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Familias de productos', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'familias' );
        $this->page_construct('configuracion/familias/index', $meta, $data);
    }

    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        $data = array(  'familia_id' => $this->input->post('familia_id'),
                        'familia' => $this->input->post('familia'),
                        'ubi_carta' => $this->input->post('ubi_carta')
                    );
        $this->form_validation->set_rules('familia', "Familia", 'required');
        $this->form_validation->set_rules('ubi_carta', "Ubicación", 'required');
        $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

        if ($this->form_validation->run() == true) {
            if( $this->input->post('familia_id') == 0 ){
                $familia_id = $this->Familia_model->insert($data);
                if(!$familia_id){
                    $this->session->set_flashdata('success', false);
                    $this->session->set_flashdata('message', 'Problemas para registrar la familia.');
                }else{
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Familia registrada correctamente.");
                }
            }else{
                $rpta = $this->Familia_model->update($data);
                if(!$rpta){
                    $this->session->set_flashdata('success', false);
                    $this->session->set_flashdata('message', 'Problemas para actualizar la familia.');
                }else{
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Familia actualizada correctamente.");
                }
            }
        }else{
            $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
            $this->session->set_flashdata('message',$message);
            $this->session->set_flashdata('success',false);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function eliminar($familia_id){
        try {
            $this->loaders->verifica_sesion();
            $productos = $this->Producto_model->get_by_familia($familia_id);
            if(count($productos) > 0){
                throw new Exception("No se puede eliminar la familia, debido a que tiene productos relacionados.");
            }
            if( $this->Familia_model->delete($familia_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Familia eliminada correctamente.');
                redirect(base_url('configuracion/familias'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar la familia.');
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}
