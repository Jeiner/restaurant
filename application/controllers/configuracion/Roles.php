<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('Modulo_model');
        $this->load->model('Opcion_model');
        $this->load->model('Rol_model');
        $this->load->library('session');
        // $this->load->model('Servicio_model');
    }
    public function asignar_accesos($rol_id = 0){
        $data['modulos'] = $this->Modulo_model->get_all();
        $data['opciones'] = $this->Opcion_model->get_all();
        $data['roles'] = $this->Rol_model->get_all();
        $data['accesos'] = $this->Rol_model->get_accesos_by_rol($rol_id);
        // echo json_encode($data['accesos']);exit();
        $data['rol_id'] = $rol_id;
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configutración' ),
                    array('link' => '#', 'page' => 'Gestionar accesos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Asignar accesos a rol', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'asignar_accesos' );
        $this->page_construct('configuracion/roles/asignar_accesos', $meta, $data);
    }
    public function agregar_acceso(){
        $this->loaders->verifica_sesion_ajax();
        $rol_id = $this->input->post('rol_id');
        $modulo_id = $this->input->post('modulo_id');
        $opcion_id = $this->input->post('opcion_id');
        $data = array(
                    'rol_id' => $rol_id,
                    'modulo_id' => ($modulo_id != 0)? $modulo_id : null,
                    'opcion_id' => ($opcion_id != 0)? $opcion_id : null,
                );
        $rpta = $this->Rol_model->agregar_acceso($data);
        if(!$rpta){
            $data_resp['success'] = false;
            $data_resp['mensaje'] = "Problemas al asignar el acceso.";
            echo json_encode($data_resp); exit();
        }else{
            $data_resp['success'] = true;
            $data_resp['mensaje'] = "Acceso asignado correctamente.";
            echo json_encode($data_resp); exit();
        }
    }
    public function quitar_acceso(){
        $this->loaders->verifica_sesion_ajax();
        $rol_id = $this->input->post('rol_id');
        $modulo_id = $this->input->post('modulo_id');
        $opcion_id = $this->input->post('opcion_id');
        $data = array(
                    'rol_id' => $rol_id,
                    'modulo_id' => ($modulo_id != 0)? $modulo_id : null,
                    'opcion_id' => ($opcion_id != 0)? $opcion_id : null,
                );
        $rpta = $this->Rol_model->quitar_acceso($data);
        if(!$rpta){
            $data_resp['success'] = false;
            $data_resp['mensaje'] = "Problemas al quitar el acceso.";
            echo json_encode($data_resp); exit();
        }else{
            $data_resp['success'] = true;
            $data_resp['mensaje'] = "Acceso retirado correctamente.";
            echo json_encode($data_resp); exit();
        }
    }
}
