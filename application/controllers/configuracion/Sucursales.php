<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sucursales extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Generalidades_model');
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->library('session');
    }

    function object_sucursal(){
        $oSucursal = new stdClass();
        $oSucursal->CRUD = "C"; //CRUD
        $oSucursal->empresa_codigo = "";
        $oSucursal->sucursal_codigo = "";
        $oSucursal->empresa_nombre = "";
        $oSucursal->sucursal_nombre = "";
        $oSucursal->ip_local = "";
        $oSucursal->estado = "A";
        $oSucursal->lugar = "";
        return $oSucursal;
    }

    function index(){
        $data['oSucursal'] =  $this->object_sucursal();
        $data['lstSucursales'] = $this->Nube_sucursal_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Sucursales' ),
                );
        $meta = array( 'page_title' => 'Sucursales', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'sucursales', 'select_tab' => 'sucursales' );
        $this->page_construct('configuracion/sucursales/index', $meta, $data);
    }

    function editar($empresa_codigo = null, $sucursal_codigo = null){
        //Valida datos de entrada
        if($empresa_codigo == null || $sucursal_codigo == null){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El código de la sucursal a editar no es válido.");
            $ruta_nueva = base_url('configuracion/sucursales/index');
            redirect($ruta_nueva);
        }

        //Buscar sucursal
        $buscarSucursal =  $this->Nube_sucursal_model->get_by_codigo($sucursal_codigo);
        if(count($buscarSucursal) <= 0){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El código de sucursal no existe para editar:".$empresa_codigo."-".$sucursal_codigo);
            $ruta_nueva = base_url('configuracion/sucursales/index');
            redirect($ruta_nueva);
        }

        //Preparar datos
        $data['oSucursal'] = $buscarSucursal[0];
        $data['oSucursal']->CRUD = "U";
        $data['lstSucursales'] = $this->Nube_sucursal_model->get_all();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuración' ),
                    array('link' => '#', 'page' => 'Sucursales' ),
                );
        $meta = array( 'page_title' => 'Sucursales', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'generalidades', 'select_tab' => 'sucursales' );
        $this->page_construct('configuracion/sucursales/index', $meta, $data);
    }
}
