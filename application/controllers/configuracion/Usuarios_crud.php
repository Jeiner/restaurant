<?php defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios_crud extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Usuario_model');
        $this->load->model('Empleado_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }

    function ajax_generar_nombre_usuario(){
        $nombres = $this->input->post('nombres');
        $ape_paterno = $this->input->post('ape_paterno');
        $ape_materno = $this->input->post('ape_materno');

        //Primera opción
        $usuarioGenerado = substr($nombres,0,1).$ape_paterno;
        $buscarUsuario = $this->Usuario_model->get_by_nombre_usuario($usuarioGenerado);
        if($buscarUsuario == null){
            $data_resp['success'] = true;
            $data_resp['data'] = $usuarioGenerado;
            echo json_encode($data_resp); exit();
        }

        //Segunda opcion
        $usuarioGenerado = $usuarioGenerado.substr($ape_materno,0,1);
        $buscarUsuario = $this->Usuario_model->get_by_nombre_usuario($usuarioGenerado);
        if($buscarUsuario == null){
            $data_resp['success'] = true;
            $data_resp['data'] = $usuarioGenerado;
            echo json_encode($data_resp); exit();
        }

        //Tercera opcion
        $usuarioGenerado = $usuarioGenerado.substr($ape_materno,1,1);
        $buscarUsuario = $this->Usuario_model->get_by_nombre_usuario($usuarioGenerado);
        if($buscarUsuario == null){
            $data_resp['success'] = true;
            $data_resp['data'] = $usuarioGenerado;
            echo json_encode($data_resp); exit();
        }

        //Cuarta opcion
        $usuarioGenerado = $usuarioGenerado.substr($ape_materno,2,1);
        $buscarUsuario = $this->Usuario_model->get_by_nombre_usuario($usuarioGenerado);
        if($buscarUsuario == null){
            $data_resp['success'] = true;
            $data_resp['data'] = $usuarioGenerado;
            echo json_encode($data_resp); exit();
        }else{
            $data_resp['success'] = false;
            $data_resp['message'] = "Error en generar usuario.";
            echo json_encode($data_resp); exit();
        }
    }

    function set_datos_obj_usuario($data){

        if($this->session->userdata('oUsuario')){
            $oUsuario = $this->session->userdata('oUsuario');
        }else{
            $oUsuario = new Usuario_model();
            $this->session->set_userdata('oUsuario', $oUsuario);
        }

        $oUsuario->usuario_id = $data['usuario_id'];
        $oUsuario->DNI = $data['DNI'];
        $oUsuario->usuario = $data['usuario'];
        $oUsuario->rol_id = $data['rol_id'];
        $oUsuario->id_usuario_asistencia = $data['id_usuario_asistencia'];
        $oUsuario->empleado_id = $data['empleado_id'];
        $this->session->set_userdata('oUsuario', $oUsuario);
    }

    function crud_guardar_usuario_create($oDataUsuario){
    	//Validar que el empleado no tenga un usuario registrado
    	$buscarUsuario = $this->Usuario_model->get_by_empleado_id($oDataUsuario['empleado_id']);
    	if($buscarUsuario != null){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El empleado ya tiene un usuario asignado. ID de empleado:".$oDataUsuario['empleado_id']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

    	//Valida que el nombre de usuario no exista
    	$buscarUsuario = $this->Usuario_model->get_by_nombre_usuario($oDataUsuario['usuario']);
    	if($buscarUsuario != null){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El nombre de usuario ya existe: ".$oDataUsuario['usuario']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //Generar clave
        $oDataUsuario['clave'] = md5($oDataUsuario['DNI']);

    	// /Nuevo usuario
        $usuario_id = $this->Usuario_model->insert($oDataUsuario);
        if(!$usuario_id){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar el usuario.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Usuario registrado correctamente.");
        }
        redirect(base_url('configuracion/usuarios'));
    }

    function crud_guardar_usuario_update($oDataUsuario){
    	//Validar que el usuario exista para actualziar
    	$buscarUsuario = $this->Usuario_model->get_by_usuario_id($oDataUsuario['usuario_id']);
    	if($buscarUsuario == null){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "El usuario no existe. ID de Usuario:".$oDataUsuario['usuario_id']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

    	// Editar usuario
        $nueva_data = array(
            'usuario_id' => $oDataUsuario['usuario_id'],
            'DNI' => $oDataUsuario['DNI'],
            'nombre_completo' =>  $oDataUsuario['nombre_completo'],
            'telefono' => $oDataUsuario['telefono'],
            'correo' => $oDataUsuario['correo'],
            'usuario' => $oDataUsuario['usuario'],
            'rol_id' => $oDataUsuario['rol_id'],
            'id_usuario_asistencia' => $oDataUsuario['id_usuario_asistencia'],
            'ruta_foto' => $oDataUsuario['ruta_foto'],
            'empleado_id' => $oDataUsuario['empleado_id'],
            'estado' => $oDataUsuario['estado'],
        );

        $rpta = $this->Usuario_model->update($nueva_data);

        if(!$rpta){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para actualizar el usuario.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Usuario actualizado correctamente.");
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $data = array(  'empleado_id' => $this->input->post('empleado_id'),
            				'usuario_id' => $this->input->post('usuario_id'),
                            'DNI' => $this->input->post('DNI'),
                            'nombre_completo' =>  $this->input->post('nombre_completo'),
                            'telefono' => $this->input->post('telefono'),
                            'correo' => $this->input->post('correo'),
                            'usuario' => $this->input->post('usuario'),
                            'rol_id' => $this->input->post('rol_id'),
                            'id_usuario_asistencia' => $this->input->post('id_usuario_asistencia'),
                            'ruta_foto' => $this->input->post('ruta_foto'),
                            'estado' => $this->input->post('estado'),
                        );

            //Validaciones
            $this->form_validation->set_rules('DNI', "DNI", 'required|min_length[8]');
            $this->form_validation->set_rules('nombre_completo', "Apellidos y nombres", 'required|min_length[1]');
            $this->form_validation->set_rules('usuario', "Nombre de usuario", 'required');
            $this->form_validation->set_rules('rol_id', "Rol", 'required');

            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('usuario_id') == 0 ){
                    $this->crud_guardar_usuario_create($data);
                }else{
                    $this->crud_guardar_usuario_update($data);
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('usuarios'));
        }
    }

    function actualizar_clave(){
        $cambiar_clave_usuario_id = $this->input->post('cambiar_clave_usuario_id');
        $cambiar_clave_1 = $this->input->post('cambiar_clave_1');
        $cambiar_clave_2 = $this->input->post('cambiar_clave_2');

        if($cambiar_clave_1 != $cambiar_clave_2){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', "Las contraseñas deben ser iguales.");
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $dataUsu['clave'] = md5($cambiar_clave_1);

        $rpta = $this->Usuario_model->update_cambiar_clave($cambiar_clave_usuario_id, $dataUsu);

         if(!$rpta){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para actualizar la contraseña.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Contraseña actualizada correctamente.");
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

}
