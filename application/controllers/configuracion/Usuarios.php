<?php defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Usuario_model');
        $this->load->model('Empleado_model');
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->model('Cargo_model');
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_usuario(){
        if($this->session->userdata('oUsuario')){
            return $this->session->userdata('oUsuario');
        }else{
            $oUsuario = new Usuario_model();
            $this->session->set_userdata('oUsuario', $oUsuario);
            return $this->session->userdata('oUsuario');
        }
    }

    function index(){
        $this->session->unset_userdata('oUsuario');
        $data['usuarios'] = $this->Usuario_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Configuracion' ),
                    array('link' => '#', 'page' => 'Gestionar usuarios' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Usuarios', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'usuarios' );
        $this->page_construct('configuracion/usuarios/index', $meta, $data);
    }

    function nuevo(){
        try {
            //Parametros de entrada
            $empleado_id = $this->input->get("empleado_id");
            if($empleado_id ==  null || $empleado_id == ""){
                $message = "Debe seleccionar un empleado para crear usuario."."<br>ID del empleado No válido ".$empleado_id;
                throw new Exception($message);
            }

            //Buscar empleado
            $buscarEmpleado = $this->Empleado_model->get_one($empleado_id);
            if (count($buscarEmpleado) == 0 ) {
                $message = "No se encontró el empleado para crear usuario: ".$empleado_id;
                throw new Exception($message);
            }
            $oEmpleado = $buscarEmpleado[0];

            $data['oUsuario'] = $this->preparar_obj_usuario();
            $data['oEmpleado'] = $oEmpleado;
            $data['roles'] = $this->Usuario_model->get_all_roles();

            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Configuracion' ),
                        array('link' => '#', 'page' => 'Gestionar usuarios' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Registrar usuario', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'usuarios' );
            $this->page_construct('configuracion/usuarios/nuevo', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function editar(){
        $this->session->unset_userdata('oUsuario');
        try {
            //Parametros de entrada
            $usuario_id = $this->input->get("usuario_id");
            if($usuario_id ==  null || $usuario_id == ""){
                $message = "El ID del usuario a editar no es válido.".$usuario_id;
                throw new Exception($message);
            }

            //Buscar usuario
            $buscarUsuario = $this->Usuario_model->get_one($usuario_id);
            if (count($buscarUsuario) == 0 ) {
                $message = "No se encontró el usuario a editar. ID del usuario: ".$usuario_id;
                throw new Exception($message);
            }
            $oUsuario = $buscarUsuario[0];

            //Buscar empleado
            $buscarEmpleado = $this->Empleado_model->get_one($oUsuario->empleado_id);
            if (count($buscarEmpleado) == 0 ) {
                $message = "No se encontró el empleado para el usuario. ID del empleado: ".$oUsuario->empleado_id;
                throw new Exception($message);
            }
            $oEmpleado = $buscarEmpleado[0];

            $data['lstSucursales'] = $this->Nube_sucursal_model->get_all_activos();
            $data['cargos'] = $this->Cargo_model->get_all();
            $data['roles'] = $this->Usuario_model->get_all_roles();
            $data['oUsuario'] = $oUsuario;
            $data['oEmpleado'] = $oEmpleado;
            
            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Configuracion' ),
                        array('link' => '#', 'page' => 'Gestionar usuarios' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Editar usuario', 'bc' => $bc, 'modulo'=>'configuracion', 'item' => 'usuarios' );
            $this->page_construct('configuracion/usuarios/editar', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    
    function bloquear($usuario_id){
        $this->loaders->verifica_sesion();
        if( $this->Usuario_model->bloquear($usuario_id) ){
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Usuario bloqueado correctamente.');
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'No se puede bloquear el usuario.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function activar($usuario_id){
        $this->loaders->verifica_sesion();
        if( $this->Usuario_model->activar($usuario_id) ){
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Usuario activado correctamente.');
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'No se puede activar el usuario.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function eliminar($usuario_id){
        $this->loaders->verifica_sesion();
        $oUsuario = $this->Usuario_model->get_one($usuario_id);

        $this->session->set_flashdata('success', false);
        $this->session->set_flashdata('message', 'Debe verificar que no haya ordenes referenciadas a esta usuario.');
        redirect($_SERVER["HTTP_REFERER"]);

            if( $this->Usuario_model->bloquear($usuario_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Usuario eliminado correctamente.');
                redirect(base_url('usuarios'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar el usuario.');
            }
        redirect($_SERVER["HTTP_REFERER"]);
    }
}
