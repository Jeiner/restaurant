<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sync extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Mesa_model');
        // $this->load->model('Union_mesa_model');
        // $this->load->model('Producto_model');
        $this->load->model('Comanda_model');
        $this->load->model('Pago_model');
        $this->load->model('Generalidades_model');
        $this->load->model('Caja_model');
        $this->load->library('session');
        // $this->Caja_model->verificar_caja_aperturada();
    }
    function index(){

    }
    //inserts
    function enviar_comandasd()
    {
        $data['comanda_id'] = $comanda_id;
        $data['modalidad'] = $modalidad;
        $data['fecha_comanda'] = $fecha_comanda;
        $data['mozo'] = $mozo;
        $data['mesa_id'] = $mesa_id;
        $data['mesa'] = $mesa;
        $data['nro_comensales'] = $nro_comensales;
        $data['prioridad'] = $prioridad;
        $data['nro_productos'] = $nro_productos;
        $data['pago_id'] = $pago_id;
        $data['tasa_IGV'] = $tasa_IGV;
        $data['total_IGV'] = $total_IGV;
        $data['importe_sin_IGV'] = $importe_sin_IGV;
        $data['importe_total'] = $importe_total;
        $data['descuento'] = $descuento;
        $data['total_pagado'] = $total_pagado;
        $data['fecha_pago'] = $fecha_pago;
        $data['pagado_por'] = $pagado_por;
        $data['tipo_comprobante'] = $tipo_comprobante;
        $data['cliente_id'] = $cliente_id;
        $data['cliente_documento'] = $cliente_documento;
        $data['cliente_nombre'] = $cliente_nombre;
        $data['cliente_direccion'] = $cliente_direccion;
        $data['cliente_telefono'] = $cliente_telefono;
        $data['union_mesas'] = $union_mesas;
        $data['caja_id'] = $caja_id;
        $data['estado_atencion'] = $estado_atencion;
        $data['estado_pago'] = $estado_pago;
        $data['atendida_en'] = $atendida_en;
        $data['prefacturada_en'] = $prefacturada_en;
        $data['prefacturada_por'] = $prefacturada_por;
        $data['finalizada_en'] = $finalizada_en;
        $data['finalizada_por'] = $finalizada_por;
        $data['anulado_en'] = $anulado_en;
        $data['anulado_por'] = $anulado_por;
        $data['anulado_motivo'] = $anulado_motivo;
        $data['fecha_registro'] = $fecha_registro;
        $data['registrado_por'] = $registrado_por;
        $data['fecha_modificacion'] = $fecha_modificacion;
        $data['modificado_por'] = $modificado_por;
        $data['comentario'] = $comentario;
        $data['cmd_comentario'] = $cmd_comentario;
        echo $comanda_id."//";
        $this->db->insert('comanda', $data);
    }
    //Envia comanda, items  y pagos de las comandas
    function enviar_comandas($sistema = ''){
        try {
            //Obtenemos Las generalidades de la base de datos en producción     1:Quintanas, 2:Primavera
            $generalidades = ($this->Generalidades_model->get_one())[0];
            if($generalidades->db_name_sync == ''){
                throw new Exception("No se encontró base de datos para sincronizar.");
            }
            //Obtenemos las comandas
            $comandas = $this->Comanda_model->sync_get_comandas_not_sync();
            if(count($comandas) <= 0) {
                echo "No hay comandas para sincronizar.";
            }
            // echo json_encode($comandas);exit();
            echo "<h4>Sincronizar comandas </h4>";
            echo "Cantidad de comandas a sincronizar: ".count($comandas)."<br>";
            echo "Sincronizando comandas...<br><br>";
            echo "<table style='font:13px/20px normal Helvetica, Arial, sans-serif;' class='table table-bordered'> <tbody>";
            echo "  <tr>
                        <td><strong>    Còdigo          </strong></td>
                        <td><strong>    Mesa/Moda       </strong></td>
                        <td><strong>    Mozo            </strong></td>
                        <td><strong>    Importe         </strong></td>
                        <td><strong>    Imp. pagado     </strong></td>
                        <td><strong>    Estado atención     </strong></td>
                        <td><strong>    Estado pago     </strong></td>
                    </tr>";
            //Obtenemos la base de datos remota
            $db_remoto = $this->load->database($generalidades->db_name_sync, TRUE);
            //Enviamos cada comanda
            foreach ($comandas as $key => $cmd){
                //Obtenemos item para cada comanda
                $items = $this->Comanda_model->get_items_by_comanda($cmd->comanda_id);
                //obtenemos el o los pagos de la comanda
                $oPagos = $this->Pago_model->get_one_by_comanda($cmd->comanda_id);
                //Enviamos  los datos 
                $rpta = $this->Comanda_model->sync_enviar_comanda($cmd,$items,$oPagos,$db_remoto);
                if($rpta >= 0){
                    $this->Comanda_model->comanda_esta_sincronizada($cmd->comanda_id);
                    echo "  <tr>
                                <td>    ".$cmd->comanda_id."      </td>
                                <td>    ".$cmd->mesa."   </td>
                                <td>    ".$cmd->mozo."        </td>
                                <td>    ".$cmd->importe_total."     </td>
                                <td>    ".$cmd->total_pagado."     </td>
                                <td>    ".$cmd->estado_atencion."     </td>
                                <td>    ".$cmd->estado_pago."     </td>
                            </tr>";
                }else{
                    echo "<tr>
                            <td colspan='4'><strong> Error en la comanda ".$cmd->comanda_id." </strong></td>
                        </tr>";
                }
            }
            echo "</tbody></table><br>";
            echo "Sincronización correcta";
            // set_time_limit(120);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            // redirect(base_url('salon/salon'));
            echo $log_mensaje;
        }
    }
}






