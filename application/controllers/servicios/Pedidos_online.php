<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pedidos_online extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('online/OnlinePedido_model');
    }
    function index(){
        
    }

    function ajax_obtenerPedidosOnline(){

        $oGeneralidades = ($this->Site->get_generalidades())[0];
        $nroPedidosPendientes = 0;
        if($oGeneralidades->tiene_delivery_online){
            $nroPedidosPendientes = $this->OnlinePedido_model->obtenerPedidosPendientes();
        }else{
            $nroPedidosPendientes = 0;
        }

        //Retornar respuesta
        $response['success'] = true;
        $response['nroPedidosPendientes'] = $nroPedidosPendientes;
        echo json_encode($response);
 
    }
}
