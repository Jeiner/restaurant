<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ws_facsu extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
    }
    public function enviar_cbe_a_facsu($cabecera_id){
        //Comprobar Conexión con servidor remoto
        $url_servidor = $this->Site->get_ip_facsu();
        $rpta = @get_headers($url_servidor);
        // if (!is_array($rpta)) {
        //     $log_mensaje = "No hay respuesta del servidor FACSU, NO se envió el comprobante electrónico: ".$cabecera_id;
        //     $this->Site->registrar_log_error('A','Controller-Documento_electronico',$log_mensaje);
        //     return false;
        // }        
        $oGeneralidades = $this->Site->get_generalidades();
        $oCabecera = $this->Documento_electronico_model->get_cpbe_cab($cabecera_id);
        $items = $this->Documento_electronico_model->get_cpbe_det_by_cab($cabecera_id);

        if(count($oGeneralidades) == 1 && count($oCabecera) == 1 && count($items) > 0){
            $oGeneralidades = $oGeneralidades[0];
            $oCabecera = $oCabecera[0];
            $items = $items;
            $oCliente = $this->Cliente_model->get_by_doc($oCabecera->numDocUsuario);
            $oCliente = $oCliente[0];
        }

        //Enviar a webservice el comprobante electronico
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, 'http://localhost/facsu/webservice/facsu_api/facmcbe');
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'nEmpId' => "1",
            // 'nCodSuc'
            'cNumRuc' => $oGeneralidades->RUC,
            'cTipCbe' => $oCabecera->tipoComprobante,
            'cSerie' => $oCabecera->serie,
            'cCorrel' => $oCabecera->correlativo,
            'cNumDoc' => $oCabecera->serie.'-'.$oCabecera->correlativo,

            'comanda_id' => $oCabecera->comanda_id,
            'pago_id' => $oCabecera->pago_id,

            'cTipOpe' => $oCabecera->tipoOperacion,
            'cFecEmi' => $oCabecera->fecEmision,
            'cHorEmi' => $oCabecera->horEmision,
            'cFecVen' => $oCabecera->fecVencimiento,
            'cLocEmi' => $oCabecera->codLocalEmisor,
            'cTIpUsu' => $oCabecera->tipDocUsuario,
            'cDocUsu' => $oCabecera->numDocUsuario,
            'cNomUsu' => $oCabecera->rznSocialUsuario,
            'cTipMon' => $oCabecera->tipMoneda,

            'nTotTri' => $oCabecera->sumTotTributos,
            'nValVen' => $oCabecera->sumTotValVenta,
            'nPreVen' => $oCabecera->sumPrecioVenta,
            'nTotDes' => $oCabecera->sumDescTotal,
            'nOtrCar' => $oCabecera->sumOtrosCargos,
            'nTotAnt' => $oCabecera->sumTotalAnticipos,
            'nImpVen' => $oCabecera->sumImpVenta,

            'cUblId' => $oCabecera->ublVersionId,
            'cCusId' => $oCabecera->customizationId,

            'cCodHas' => "",
            'cIndSit' => "",
            'cEnvCoe' => 0,

            'cTmpDir' => $oCliente->direccion,
            'cTmpCoe' => $oCliente->correo,

            'tFecReg' => date("Y/m/d H:i:s")
        ));
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        $result = json_decode($buffer);

        if(!$result->success){
            $log_mensaje = "No se envío el documento electrónico al sistema de facturación FACSU: ".$result->message;
            $this->Site->registrar_log_error('A','Controler-Documento_electronico',$log_mensaje);
            return;
        }
        $nCbeId = $result->id;
        
        echo "Existo";
        echo json_encode($result);exit();


        echo json_encode($oCabecera);exit();

        if(count($oGeneralidades) == 1 && count($oCabecera) == 1 && count($items) > 0){
            $datos['oGeneralidades'] = $oGeneralidades[0];
            $datos['oCabecera'] = $oCabecera[0];
            $datos['items'] = $items;
            $oCliente = $this->Cliente_model->get_by_doc($datos['oCabecera']->numDocUsuario);
            $datos['oCliente'] = $oCliente[0];
            $this->Ws_facturacion->insertar_comprobante_electronico($datos);
        }else{
            $log_mensaje = "No se envío el documento electrónico al sistema de facturación FACSU";
            $this->Site->registrar_log_error('A','Controler-Documento_electronico',$log_mensaje);
        }
    }
    
    //Inserta cabecera del comprobante
    public function insertar_comprobante_electronico($datos){
    	$oGeneralidades = $datos['oGeneralidades'];
    	$oCabecera = $datos['oCabecera'];
    	$items = $datos['items'];
    	$oCliente = $datos['oCliente'];

		$facmcbe['nEmpId'] = "1";
		// $facmcbe['nCodSuc'] = ;
		$facmcbe['cNumRuc'] = $oGeneralidades->RUC;
		$facmcbe['cTipCbe'] = $oCabecera->tipoComprobante;
		$facmcbe['cSerie'] = $oCabecera->serie;
		$facmcbe['cCorrel'] = $oCabecera->correlativo;
		$facmcbe['cNumDoc'] = $oCabecera->serie.'-'.$oCabecera->correlativo;

		$facmcbe['comanda_id'] = $oCabecera->comanda_id;
		$facmcbe['pago_id'] = $oCabecera->pago_id;

		$facmcbe['cTipOpe'] = $oCabecera->tipoOperacion;
		$facmcbe['cFecEmi'] = $oCabecera->fecEmision;
		$facmcbe['cHorEmi'] = $oCabecera->horEmision;
		$facmcbe['cFecVen'] = $oCabecera->fecVencimiento;
		$facmcbe['cLocEmi'] = $oCabecera->codLocalEmisor;
		$facmcbe['cTIpUsu'] = $oCabecera->tipDocUsuario;
		$facmcbe['cDocUsu'] = $oCabecera->numDocUsuario;
		$facmcbe['cNomUsu'] = $oCabecera->rznSocialUsuario;
		$facmcbe['cTipMon'] = $oCabecera->tipMoneda;

		$facmcbe['nTotTri'] = $oCabecera->sumTotTributos;
		$facmcbe['nValVen'] = $oCabecera->sumTotValVenta;
		$facmcbe['nPreVen'] = $oCabecera->sumPrecioVenta;
		$facmcbe['nTotDes'] = $oCabecera->sumDescTotal;
		$facmcbe['nOtrCar'] = $oCabecera->sumOtrosCargos;
		$facmcbe['nTotAnt'] = $oCabecera->sumTotalAnticipos;
		$facmcbe['nImpVen'] = $oCabecera->sumImpVenta;

		$facmcbe['cUblId'] = $oCabecera->ublVersionId;
		$facmcbe['cCusId'] = $oCabecera->customizationId;
		$facmcbe['cCodHas'] = "";
		$facmcbe['cIndSit'] = "";

		$facmcbe['cTmpDir'] = $oCliente->direccion;
		$facmcbe['cTmpCoe'] = $oCliente->correo;

		$facmcbe['tFecReg'] = date("Y/m/d H:i:s");
		// $facmcbe['cUsuReg'] = $oCabecera->;
		// $facmcbe['tFecMod'] = $oCabecera->;
		// $facmcbe['cUsuMod'] = $oCabecera->;
    	$nCbeId = $this->Facmcbe_model->insert($facmcbe);
    	$this->insert_detalle_comprobante_electronico($nCbeId, $items);
    }
    private function insert_detalle_comprobante_electronico($nCbeId, $items){
    	foreach ($items as $key => $oItem) {
    		// $data['nCodDet'] = 
			$facdcbe['nCbeId'] = $nCbeId;

			$facdcbe['comanda_item_id'] = $oItem->comanda_item_id;
			$facdcbe['comanda_id'] = $oItem->comanda_id;
			$facdcbe['importe'] = $oItem->importe;

			$facdcbe['codUnidadMedida'] = $oItem->codUnidadMedida;
			$facdcbe['ctdUnidadItem'] = $oItem->ctdUnidadItem;
			$facdcbe['codProducto'] = $oItem->codProducto;
			$facdcbe['codProductoSUNAT'] = $oItem->codProductoSUNAT;
			$facdcbe['desItem'] = $oItem->desItem;
			$facdcbe['mtoValorUnitario'] = $oItem->mtoValorUnitario;
			$facdcbe['sumTotTributosItem'] = $oItem->sumTotTributosItem;
			$facdcbe['codTriIGV'] = $oItem->codTriIGV;
			$facdcbe['mtoIgvItem'] = $oItem->mtoIgvItem;
			$facdcbe['mtoBaseIgvItem'] = $oItem->mtoBaseIgvItem;
			$facdcbe['nomTributoIgvItem'] = $oItem->nomTributoIgvItem;
			$facdcbe['codTipTributoIgvItem'] = $oItem->codTipTributoIgvItem;
			$facdcbe['tipAfeIGV'] = $oItem->tipAfeIGV;
			$facdcbe['porIgvItem'] = $oItem->porIgvItem;
			$facdcbe['codTriISC'] = $oItem->codTriISC;
			$facdcbe['mtoIscItem'] = $oItem->mtoIscItem;
			$facdcbe['mtoBaseIscItem'] = $oItem->mtoBaseIscItem;
			$facdcbe['nomTributoIscItem'] = $oItem->nomTributoIscItem;
			$facdcbe['codTipTributoIscItem'] = $oItem->codTipTributoIscItem;
			$facdcbe['tipSisISC'] = $oItem->tipSisISC;
			$facdcbe['porIscItem'] = $oItem->porIscItem;
			$facdcbe['codTriOtroItem'] = $oItem->codTriOtroItem;
			$facdcbe['mtoTriOtroItem'] = $oItem->mtoTriOtroItem;
			$facdcbe['mtoBaseTriOtroItem'] = $oItem->mtoBaseTriOtroItem;
			$facdcbe['nomTributoIOtroItem'] = $oItem->nomTributoIOtroItem;
			$facdcbe['codTipTributoIOtroItem'] = $oItem->codTipTributoIOtroItem;
			$facdcbe['porTriOtroItem'] = $oItem->porTriOtroItem;
			$facdcbe['mtoPrecioVentaUnitario'] = $oItem->mtoPrecioVentaUnitario;
			$facdcbe['mtoValorVentaItem'] = $oItem->mtoValorVentaItem;
			$facdcbe['mtoValorReferencialUnitario'] = $oItem->mtoValorReferencialUnitario;

			// $facdcbe['tFecReg'] = $oItem->;
			// $facdcbe['cUsuReg'] = $oItem->;
			// $facdcbe['tFecMod'] = $oItem->;
			// $facdcbe['cUsuMod'] = $oItem->;
			$this->Facdcbe_model->insert($facdcbe);
    	}
    }
}

