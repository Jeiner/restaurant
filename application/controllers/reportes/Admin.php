<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Reportes_model');
        $this->load->model('Producto_model');
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
        // $this->Caja_model->verificar_caja_aperturada();
    }
    function reporte_ventas_mensual(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Reportes' ),
                    array('link' => '#', 'page' => 'Reporte de ventas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nueva Comanda', 'bc' => $bc, 'modulo'=>'reportes', 'item' => 'reporte_ventas');
        $this->page_construct('reportes/grafico_ventas_mensuales', $meta, null);
    }
    function reporte_ventas_diarias(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Reportes' ),
                    array('link' => '#', 'page' => 'Reporte de ventas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nueva Comanda', 'bc' => $bc, 'modulo'=>'reportes', 'item' => 'reporte_ventas');
        $this->page_construct('reportes/grafico_ventas_diarias', $meta, null);
    }
    function ranking_mozos_mensual(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Reportes' ),
                    array('link' => '#', 'page' => 'Reporte de ventas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nueva Comanda', 'bc' => $bc, 'modulo'=>'reportes', 'item' => 'ranking_mozos');
        $this->page_construct('reportes/ranking_mozos_mensual', $meta, null);
    }
    function ranking_productos(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Reportes' ),
                    array('link' => '#', 'page' => 'Ranking de productos vendidos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Ranking de productos', 'bc' => $bc, 'modulo'=>'reportes', 'item' => 'ranking_productos');
        $this->page_construct('reportes/ranking_productos', $meta, null);
    }
    function historico_por_producto(){
        $data['productos'] = $this->Producto_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Reportes' ),
                    array('link' => '#', 'page' => 'Histórico por producto' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Histórico por productos', 'bc' => $bc, 'modulo'=>'reportes', 'item' => 'ranking_productos');
        $this->page_construct('reportes/historico_por_producto', $meta, $data);
    }




    // DATOS
    function ventas_mensuales(){
        $data['anio'] = $this->input->post('anio');
        // $data['anio'] = '2018';
        $result = $this->Reportes_model->ventas_mensuales($data);
        echo json_encode($result);
    }
    function ventas_diarias(){
        $data['mes'] = $this->input->post('mes');
        $data['anio'] = $this->input->post('anio');
        // $data['mes'] = '6';
        // $data['anio'] = '2018';
        $result = $this->Reportes_model->ventas_diarias($data);
        echo json_encode($result);
    }
    function ventas_mozos_mensual(){
        $anio = $this->input->post('anio');
        $result = $this->Reportes_model->ranking_mozos_mensual($anio, "0");
        echo json_encode($result);
    }
    function ranking_productos_by_fechas(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $result = $this->Reportes_model->ranking_productos_by_fechas($start_date, $end_date);
        echo json_encode($result);
    }
    function historico_producto_by_mes(){
        $producto_id = $this->input->post('producto_id');
        $anual = $this->input->post('anual');
        $result = $this->Reportes_model->historico_producto_by_mes($producto_id, $anual);
        echo json_encode($result);   
    }
}
