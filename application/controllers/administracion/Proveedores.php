<?php defined('BASEPATH') or exit('No direct script access allowed');

class Proveedores extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Proveedor_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_proveedor(){
        if($this->session->userdata('oProveedor')){
            return $this->session->userdata('oProveedor');
        }else{
            $oProveedor = new Proveedor_model();
            $this->session->set_userdata('oProveedor', $oProveedor);
            return $this->session->userdata('oProveedor');
        }
    }
    function set_datos_obj_proveedor($data){
        $oProveedor =  $this->preparar_obj_proveedor();

        $oProveedor->proveedor_id = $data['proveedor_id'];
        $oProveedor->RUC        = $data['RUC'];
        $oProveedor->razon_social = $data['razon_social'];
        $oProveedor->direccion  = $data['direccion'];
        $oProveedor->contacto   = $data['contacto'];
        $oProveedor->telefono   = $data['telefono'];
        $oProveedor->correo     = $data['correo'];
        $oProveedor->productos  = $data['productos'];
        $oProveedor->estado     = $data['estado'];

        $this->session->set_userdata('oProveedor', $oProveedor);
    }
    function index(){
        $this->session->unset_userdata('oProveedor');
        $data['proveedores'] = $this->Proveedor_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar proveedores' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Proveedores', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'proveedores' );
        $this->page_construct('administracion/proveedores/index', $meta, $data);
    }
    function nuevo(){
        $data['oProveedor'] = $this->preparar_obj_proveedor();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar proveedores' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Registrar proveedor', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'proveedores' );
        $this->page_construct('administracion/proveedores/nuevo', $meta, $data);
    }
    function editar($proveedor_id){
        $this->session->unset_userdata('oProveedor');
        $oProveedor = $this->Proveedor_model->get_one($proveedor_id);
        $data['oProveedor'] = $oProveedor[0];
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar proveedores' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Proveedores', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'proveedores' );
        $this->page_construct('administracion/proveedores/editar', $meta, $data);
    }



    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $data = array(  'proveedor_id' => $this->input->post('proveedor_id'),
                            'RUC' => $this->input->post('RUC'),
                            'razon_social' => $this->input->post('razon_social'),
                            'direccion' => $this->input->post('direccion'),
                            'contacto' => $this->input->post('contacto'),
                            'telefono' => $this->input->post('telefono'),
                            'correo' => $this->input->post('correo'),
                            'productos' => $this->input->post('productos'),
                            'estado' => $this->input->post('estado'),
                        );
            $this->set_datos_obj_proveedor($data);
            $this->form_validation->set_rules('razon_social', "Razon social", 'required');
            $this->form_validation->set_rules('RUC', "RUC", 'required|min_length[11]');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('proveedor_id') == 0 ){
                    $proveedor_id = $this->Proveedor_model->insert($data);
                    if(!$proveedor_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar el proveedor.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Proveedor registrado correctamente.");
                    }
                }else{
                    $rpta = $this->Proveedor_model->update($data);
                    if(!$rpta){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para actualizar el proveedor.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Proveedor actualizado correctamente.");
                    }
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('administracion/proveedores'));
        }
    }
    function eliminar($proveedor_id){
        $this->loaders->verifica_sesion();
        $oProveedor = $this->Proveedor_model->get_one($proveedor_id);

        $this->session->set_flashdata('success', false);
        $this->session->set_flashdata('message', 'Debe verificar que no haya productos referenciadas a esta proveedor.');        
        redirect($_SERVER["HTTP_REFERER"]);

            if( $this->Proveedor_model->delete($proveedor_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Proveedor eliminado correctamente.');
                redirect(base_url('administracion/proveedores'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar el proveedor.');
            }
        redirect($_SERVER["HTTP_REFERER"]);
    }
}
