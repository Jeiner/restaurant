<?php defined('BASEPATH') or exit('No direct script access allowed');
  
class Clientes extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Cliente_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Pide');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_cliente(){
        if($this->session->userdata('oCliente')){
            return $this->session->userdata('oCliente');
        }else{
            $oCliente = new Cliente_model();
            $this->session->set_userdata('oCliente', $oCliente);
            return $this->session->userdata('oCliente');
        }
    }
    function set_datos_obj_cliente($data){
        $oCliente =  $this->preparar_obj_cliente();

        $oCliente->cliente_id = $data['cliente_id'];
        $oCliente->tipo_cliente = $data['tipo_cliente'];
        $oCliente->documento = $data['documento'];
        $oCliente->ape_paterno = $data['ape_paterno'];
        $oCliente->ape_materno = $data['ape_materno'];
        $oCliente->nombres = $data['nombres'];
        $oCliente->nombre_completo = $data['nombre_completo'];
        $oCliente->fecha_nacimiento = $data['fecha_nacimiento'];
        $oCliente->sexo = $data['sexo'];
        $oCliente->telefono = $data['telefono'];
        $oCliente->correo = $data['correo'];
        $oCliente->direccion = $data['direccion'];
        $oCliente->estado = $data['estado'];

        $this->session->set_userdata('oCliente', $oCliente);
    }
    function index(){
        $this->session->unset_userdata('oCliente');
        $data['clientes'] = $this->Cliente_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar clientes' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Clientes', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'clientes' );
        $this->page_construct('administracion/clientes/index', $meta, $data);
    }
    function nuevo(){
        $data['oCliente'] = $this->preparar_obj_cliente();
        $data['documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar clientes' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Registrar cliente', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'clientes' );
        $this->page_construct('administracion/clientes/nuevo', $meta, $data);
    }
    function editar($cliente_id){
        $this->session->unset_userdata('oCliente');
        $oCliente = $this->Cliente_model->get_one($cliente_id);
        $data['oCliente'] = $oCliente[0];
        $data['documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar clientes' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Editar cliente', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'clientes' );
        $this->page_construct('administracion/clientes/editar', $meta, $data);
    }
    function get_one_ajax(){
        $this->loaders->verifica_sesion_ajax();
        $cliente_id = $this->input->post('cliente_id');
        $oCliente = $this->Cliente_model->get_one($cliente_id);
        echo json_encode($oCliente);
    }
    //Metodo que busca un cliente por nro de documento en BD y en servicios SUNAT y RENIEC
    function json_buscar_por_documento($nro_doc){
        $data = null;
        $response['success'] = false;
        $response['msg'] = "DNI no válido";
        $response['data'] = null;

        if(trim($nro_doc) == ""){
            $response['success'] = false;
            $response['msg'] = "DNI no válido";
            echo json_encode($response); exit();
        }
        //Buscamos el DNI en BASE DE DATOS
        $cliente = $this->Cliente_model->get_by_doc($nro_doc);
        if(count($cliente) > 0){            
            $cliente = $cliente[0];
            $data['cliente_id'] = $cliente->cliente_id;
            $data['sunat_tipo_documento_id'] = $cliente->sunat_tipo_documento_id;
            $data['documento'] = $nro_doc;
            $data['cliente'] = $cliente->nombre_completo;
            $data['nombres'] = $cliente->nombres;
            $data['ape_paterno'] = $cliente->ape_paterno;
            $data['ape_materno'] = $cliente->ape_materno;
            $data['telefono'] = $cliente->telefono;
            $data['correo'] = $cliente->correo;
            $data['direccion'] = $cliente->direccion;
            $data['ubigeo'] = "";
            $data['fecha_nacimiento'] = $cliente->fecha_nacimiento;

            $response['success'] = true;
            $response['data'] = $data;
            $response['msg'] = "Cliente encontrado";

            echo json_encode($response); 
            exit();
        }

        //Buscamos en los servicios, consultamos si es DNI o RUC
        if(strlen($nro_doc) == 8){
            $oPersona = $this->Pide->reniec($nro_doc);            
            if($oPersona['success']){
                $data['cliente_id'] = 0;
                $data['sunat_tipo_documento_id'] = 1;
                $data['documento'] = $nro_doc;
                $data['nombres'] = $oPersona['data']->NOMBRES;
                $data['ape_paterno'] = $oPersona['data']->APPAT;
                $data['ape_materno'] = $oPersona['data']->APMAT;
                $data['cliente'] = $data['ape_paterno'].' '.$data['ape_materno'].', '.$data['nombres'];
                $data['telefono'] = "";
                $data['correo'] = "";
                $data['direccion'] = $oPersona['data']->DIRECCION;
                $data['ubigeo'] = $oPersona['data']->UBIGEO;
                $data['fecha_nacimiento'] = "";

                $response['success'] = true;
                $response['msg'] = $oPersona['msg'];
                $response['data'] = $data;
            }else{
                $response['success'] = false;
                $response['msg'] = $oPersona['msg'];
            }
            echo json_encode($response); exit();
        }
        if(strlen($nro_doc) == 11){
            $oCliente = $this->Pide->sunat($nro_doc);
            if($oCliente['success']){
                $data['cliente_id'] = 0;
                $data['sunat_tipo_documento_id'] = 6;
                $data['documento'] = $oCliente['data']->numeroDocumento;
                $data['nombres'] = "";
                $data['ape_paterno'] = "";
                $data['ape_materno'] = "";
                $data['cliente'] = $oCliente['data']->nombre;
                $data['telefono'] = "";
                $data['correo'] = "";
                $data['direccion'] = "";
                $data['ubigeo'] = "";
                $data['fecha_nacimiento'] = "";

                $response['success'] = true;
                $response['data'] = $data;
                $response['msg'] = $oCliente['msg'];
            }else{
                $response['success'] = false;
                $response['msg'] = $oCliente['msg'];
            }
            echo json_encode($response); exit();
        }
        $response['success'] = false;
        $response['msg'] = "No se ha encontrado resultados.";
        echo json_encode($response);
        exit();
    }
    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $data = array(  'cliente_id' => $this->input->post('cliente_id'),
                            'sunat_tipo_documento_id' => $this->input->post('sunat_tipo_documento_id'),
                            'documento' => $this->input->post('documento'),
                            'ape_paterno' => $this->input->post('ape_paterno'),
                            'ape_materno' => $this->input->post('ape_materno'),
                            'nombres' => $this->input->post('nombres'),
                            'nombre_completo' =>  $this->input->post('nombre_completo'),
                            'fecha_nacimiento' => ($this->input->post('fecha_nacimiento') != "")? $this->input->post('fecha_nacimiento'):null,
                            'sexo' => $this->input->post('sexo'),
                            'contacto' => $this->input->post('contacto'),
                            'telefono' => $this->input->post('telefono'),
                            'correo' => $this->input->post('correo'),
                            'direccion' => $this->input->post('direccion'),
                            'estado' => 'A',
                        );
            if($this->input->post('sunat_tipo_documento_id') == '6'){
                $data['ape_paterno'] = null;
                $data['ape_materno'] = null;
                $data['nombres'] = null;
                $data['fecha_nacimiento'] = null;
                $data['sexo'] = null;
            }
            $this->set_datos_obj_cliente($data);
            //Si es nuevo que obligue a ingresar tipo y numero de documento
            //Si se editar, estos serán obtenidos de los datos anteriores
            if($data['cliente_id'] == 0){
                $this->form_validation->set_rules('sunat_tipo_documento_id', "Tipo de documento", 'required');
                $this->form_validation->set_rules('documento', "Documento", 'required');
            }else{
                $oCliente = ($this->Cliente_model->get_one($data['cliente_id']))[0];
                $data['sunat_tipo_documento_id'] = $oCliente->sunat_tipo_documento_id;
                $data['documento'] = $oCliente->documento;
            }
            $this->form_validation->set_rules('nombre_completo', "Cliente", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('cliente_id') == 0 ){
                    $cliente_id = $this->Cliente_model->insert($data);
                    if(!$cliente_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar el cliente.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Cliente registrado correctamente.");
                    }
                }else{
                    $rpta = $this->Cliente_model->update($data);
                    if(!$rpta){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para actualizar el cliente.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Cliente actualizado correctamente.");
                    }
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('clientes'));
        }
    }
    function eliminar($cliente_id){
        $this->loaders->verifica_sesion();
        $oCliente = $this->Cliente_model->get_one($cliente_id);

        $this->session->set_flashdata('success', false);
        $this->session->set_flashdata('message', 'Debe verificar que no haya ordenes referenciadas a esta cliente.');        
        redirect($_SERVER["HTTP_REFERER"]);

            if( $this->Cliente_model->delete($cliente_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Cliente eliminado correctamente.');
                redirect(base_url('clientes'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar el cliente.');
            }
        redirect($_SERVER["HTTP_REFERER"]);
    }

}
