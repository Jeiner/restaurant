<?php defined('BASEPATH') or exit('No direct script access allowed');

class Productos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Producto_model');
        $this->load->model('Familia_model');
        $this->load->model('Insumo_model');
        $this->load->model('Multitabla_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_producto(){
        if($this->session->userdata('oProducto')){
            return $this->session->userdata('oProducto');
        }else{
            $oProducto = new Producto_model();
            $this->session->set_userdata('oProducto', $oProducto);
            return $this->session->userdata('oProducto');
        }
    }
    function set_datos_obj_producto($data){
        $oProducto =  $this->preparar_obj_producto();

        $oProducto->producto_id = $data['producto_id'];
        $oProducto->producto = $data['producto'];
        $oProducto->unidad = $data['unidad'];
        $oProducto->precio_costo = $data['precio_costo'];
        $oProducto->precio_unitario = $data['precio_unitario'];
        $oProducto->para_cocina = $data['para_cocina'];
        $oProducto->manejar_stock = $data['manejar_stock'];
        $oProducto->stock = $data['stock'];
        $oProducto->stock_minimo = $data['stock_minimo'];
        $oProducto->stock_maximo = $data['stock_maximo'];
        $oProducto->fecha_vencimiento = $data['fecha_vencimiento'];
        $oProducto->familia_id = $data['familia_id'];
        $oProducto->foto = $data['foto'];
        $oProducto->codigo_barras = $data['codigo_barras'];
        $oProducto->descripcion = $data['descripcion'];
        $oProducto->estado = $data['estado'];
        $oProducto->ubi_carta = $data['ubi_carta'];

        $this->session->set_userdata('oProducto', $oProducto);
    }
    function index(){
        $this->session->unset_userdata('oProducto');
        $data['productos'] = $this->Producto_model->get_all();
        // $data['insumos'] = $this->Insumo_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar productos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Productos', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'productos' );
        $this->page_construct('administracion/productos/index', $meta, $data);
    }
    function nuevo(){
        $data['oProducto'] = $this->preparar_obj_producto();
        $data['familias'] = $this->Familia_model->get_all('A');
        $data['insumos'] = $this->Insumo_model->get_all();
        $data['unidades'] = $this->Multitabla_model->get_all_by_tipo(1);
        // echo json_encode($data['oProducto']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar productos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Registrar producto', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'productos' );
        $this->page_construct('administracion/productos/nuevo', $meta, $data);
    }
    function editar($producto_id){
        $this->session->unset_userdata('oProducto');
        $oProducto = $this->Producto_model->get_one($producto_id);
        $data['oProducto'] = $oProducto[0];
        $data['oProducto']->insumos = $this->Producto_model->get_insumos_by_producto($producto_id);
        $this->session->set_userdata('oProducto', $data['oProducto']);
        // echo json_encode($data['oProducto']->insumos);exit();
        $data['familias'] = $this->Familia_model->get_all();
        $data['insumos'] = $this->Insumo_model->get_all();
        $data['unidades'] = $this->Multitabla_model->get_all_by_tipo(1);
        // echo json_encode($data['oProducto']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar productos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Editar producto', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'productos' );
        $this->page_construct('administracion/productos/editar', $meta, $data);
    }
    function cargar_modal_kit(){
        //Carga y muestra el modal con la lista de insumos para determinado producto.
        $this->loaders->verifica_sesion_ajax();
        $producto_id = $this->input->post('producto_id'); 
        $data['producto_id'] = $producto_id;
        $data['insumos'] = $this->Producto_model->get_insumos_by_producto($producto_id);
        $this->partial_view('administracion/productos/_modal_kit',$data);
    }
    function exportar_excel(){
        $this->loaders->verifica_sesion();
        $productos = $this->Producto_model->get_all();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Productos");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Código de producto');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Producto');
        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Unidad');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'Precio costo');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'Precio unitario');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'Cocina');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'Manejar stock');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'stock');
        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'stock_minimo');
        $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", 'stock_maximo');
        $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'familia');
        $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'descripcion');
        foreach ($productos as $key => $producto) {
            $cont_excel++;
            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $producto->producto_id);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $producto->producto);
            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $producto->unidad);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $producto->precio_costo);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", $producto->precio_unitario);
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", $producto->para_cocina);
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", $producto->manejar_stock);
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", $producto->stock);
            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", $producto->stock_minimo); 
            $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", $producto->stock_maximo);
            $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", $producto->familia);
            $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", $producto->descripcion);
        }
        $archivo_excel = "Listado_de_productos."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo_excel Excel.
        // $objWriter->save('./files/'.$archivo_excel);
        $objWriter->save('php://output');
    }
    //OPERACIONES PARA LOS INSUMOS
    function agregar_insumo(){
        $this->loaders->verifica_sesion_ajax();
        $insumo_id = $this->input->post('insumo_id');
        $cantidad = ($this->input->post('cantidad'))? $this->input->post('cantidad') : 0;
        $insumo = ($this->Insumo_model->get_one($insumo_id))[0];

        $data['oProducto'] = $this->preparar_obj_producto();
        
        $existe = false; $index = -1;
        foreach ($data['oProducto']->insumos as $key => $insu) {
            if($insu->insumo_id == $insumo_id){
                $existe = true;
                $index = $key;
                break;
            }
        }
        if($existe){
            //Si existe y la cantidad es 0, quiere decir que lo esta agregando,
            //Pero si la cantidad es mayor a 0 quiere decir que esta modificando la cantidad de un isnumo que ya existe.
            //En este último caso es valido.
            if($cantidad == 0){
                echo 'existe';
                exit();    
            }else{
                ($data['oProducto']->insumos)[$index]->cantidad = $cantidad;
            }            
        }else{
            $insumo->cantidad = $cantidad;
            array_push($data['oProducto']->insumos, $insumo);    
        }
        $this->partial_view('administracion/productos/_insumos_seleccionados',$data);
    }
    function quitar_insumo(){
        $this->loaders->verifica_sesion_ajax();
        $insumo_id = $this->input->post('insumo_id');
        $data['oProducto'] = $this->preparar_obj_producto();
        $existe = false; $index = -1;
        foreach ($data['oProducto']->insumos as $key => $insu) {
            if($insu->insumo_id == $insumo_id){
                $existe = true;
                $index = $key;
                break;
            }
        }
        if($existe){
            unset(($data['oProducto']->insumos)[$index]);
        }else{
            echo "504"; //No se encontro el producto que se desea quitar de la comanda
            exit();
        }
        $this->partial_view('administracion/productos/_insumos_seleccionados',$data);
    }

    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $nueva_data = array(  'producto_id' => $this->input->post('producto_id'),
                            'producto' => $this->input->post('producto'),
                            'unidad' => $this->input->post('unidad'),
                            'precio_costo' => ($this->input->post('precio_costo')) != ""? $this->input->post('precio_costo') : null,
                            'precio_unitario' => $this->input->post('precio_unitario'),
                            'para_cocina' => $this->input->post('para_cocina'),
                            'manejar_stock' => $this->input->post('manejar_stock'),
                            'stock' => ($this->input->post('manejar_stock')) == 1? $this->input->post('stock') : null,
                            'stock_minimo' => ($this->input->post('manejar_stock')) == 1? $this->input->post('stock_minimo') : null, 
                            'stock_maximo' => ($this->input->post('manejar_stock')) == 1? $this->input->post('stock_maximo') : null, 
                            'fecha_vencimiento' => $this->input->post('fecha_vencimiento'),
                            'familia_id' => $this->input->post('familia_id'),
                            'foto' => $this->input->post('foto'),
                            'codigo_barras' => $this->input->post('codigo_barras'),
                            'ubi_carta' => $this->input->post('ubi_carta'),
                            'descripcion' => $this->input->post('descripcion'),
                            'estado' => $this->input->post('estado'),
                        );
        
            $this->set_datos_obj_producto($nueva_data);
            $data['oProducto'] = $this->preparar_obj_producto();

            $this->form_validation->set_rules('familia_id', "Familia", 'required');
            $this->form_validation->set_rules('producto', "Producto", 'required|min_length[3]');
            $this->form_validation->set_rules('unidad', "Unidad", 'required');
            $this->form_validation->set_rules('precio_unitario', "Precio unitario", 'required');
            if($this->input->post('manejar_stock') == 1){
                $this->form_validation->set_rules('stock', "Stock actual", 'required');
                $this->form_validation->set_rules('stock_minimo', "Stock mínimo", 'required');
                $this->form_validation->set_rules('stock_maximo', "Stock máximo", 'required');
            }
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('producto_id') == 0 ){
                    $producto_id = $this->Producto_model->insert($nueva_data, $data['oProducto']->insumos);
                    if(!$producto_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar el producto.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Producto registrado correctamente.");
                        redirect(base_url('administracion/productos'));
                    }
                }else{
                    $rpta = $this->Producto_model->update($nueva_data, $data['oProducto']->insumos);
                    if(!$rpta){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para actualizar el producto.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Producto actualizado correctamente.");
                        redirect(base_url('administracion/productos'));
                    }
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('administracion/productos'));
        }
    }
    function eliminar($producto_id){
        $this->loaders->verifica_sesion();
        $oProducto = $this->Producto_model->get_one($producto_id);

        $this->session->set_flashdata('success', false);
        $this->session->set_flashdata('message', 'Debe verificar que no haya ordenes referenciadas a esta producto.');        
        redirect($_SERVER["HTTP_REFERER"]);

            if( $this->Producto_model->delete($producto_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Producto eliminado correctamente.');
                redirect(base_url('administracion/productos'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar el producto.');
            }
        redirect($_SERVER["HTTP_REFERER"]);
    }


    // OTRAS OPERACIONES
    function cargar_tabla_productos_sm(){
        $this->loaders->verifica_sesion_ajax();
        $familia_id = $this->input->post('familia_id');
        $ordenar_por = $this->input->post('ordenar_por');

        $data['productos'] = $this->Producto_model->get_by_familia($familia_id, $ordenar_por);
        $this->partial_view('administracion/productos/_table_productos_sm',$data);
    }
}
