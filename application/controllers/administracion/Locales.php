<?php defined('BASEPATH') or exit('No direct script access allowed');
  
class Locales extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->model('nube/Nube_comanda_model');
        $this->load->model('nube/Nube_pago_model');
        $this->load->model('nube/Nube_comprobante_model');
        $this->load->model('nube/Nube_caja_model');
        
        // $this->load->model('Multitabla_model');
        // $this->load->model('Pide');
        // $this->load->library('session');
        $this->load->library('session');
    }

    function index(){

        $fecha_ini   = (!isset($_REQUEST['fecha_ini'])) ? ""  : trim($_REQUEST['fecha_ini']);
        $fecha_fin   = (!isset($_REQUEST['fecha_fin'])) ? ""  : trim($_REQUEST['fecha_fin']);

        $fecha_ini = ($fecha_ini == "") ? date('Y-m-d') : $fecha_ini;
        $fecha_fin = ($fecha_fin == "") ? date('Y-m-d') : $fecha_fin;

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Control de mesas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => '', 'bc' => $bc, 'modulo'=>'', 'item' => '' );
        $meta['hide_menus'] = true;

        //Obtener comandas, pagos y comprobantes
        //Verificar caja
        $lstSucursales = $this->Nube_sucursal_model->get_all();
        foreach ($lstSucursales as $key => $oSucursal) {
            $oSucursal->comandas_resumen = $this->get_resumen_comandas($fecha_ini, $fecha_fin, $oSucursal->empresa_codigo, $oSucursal->sucursal_codigo );
            $oSucursal->pagos_resumen = $this->get_resumen_pagos($fecha_ini, $fecha_fin, $oSucursal->empresa_codigo, $oSucursal->sucursal_codigo );
            $oSucursal->comprobantes_resumen = $this->get_resumen_comprobantes($fecha_ini, $fecha_fin, $oSucursal->empresa_codigo, $oSucursal->sucursal_codigo);

            $oSucursal->comandas_anuladas = $this->Nube_comanda_model->get_anuladas($fecha_ini, $fecha_fin, $oSucursal->empresa_codigo, $oSucursal->sucursal_codigo);
            $oSucursal->comandas_items_anuladas = $this->Nube_comanda_model->get_anuladas_detalle($fecha_ini, $fecha_fin, $oSucursal->empresa_codigo, $oSucursal->sucursal_codigo);

            //Obtener caja aperturada
            $oSucursal->caja_open = false;
            $oSucursal->caja_aperturado_en = "";
            $buscarCajaAbierta = $this->Nube_caja_model->get_caja_abierta($oSucursal->empresa_codigo, $oSucursal->sucursal_codigo);
            if(count($buscarCajaAbierta) >= 1){
                $oSucursal->caja_open = true;
                $oSucursal->caja_aperturado_en = $buscarCajaAbierta[0]->aperturado_en;
            }
            
        }
        
        //Devolver datos
        $meta['fecha_ini'] = $fecha_ini;
        $meta['fecha_fin'] = $fecha_fin;
        $meta['lstSucursales'] = $lstSucursales;

        //$this->page_construct('administracion/locales/index', $meta, $data);
        $this->load->view('administracion/locales/index', $meta);
    }

    public function get_resumen_comandas($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo){
        $resumenComandas = $this->Nube_comanda_model->get_totales($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo);

        if(count($resumenComandas) <= 0){
            return null;
        }

        $oResumen = $resumenComandas[0];
        if($oResumen->cantidad == null){
            $oResumen->cantidad = 0;
        }
        if($oResumen->importe_total == null){
            $oResumen->importe_total = 0;
        }

        return $oResumen;
    }

    public function get_resumen_pagos($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo){
        $resumenPagos = $this->Nube_pago_model->get_totales($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo);
        if(count($resumenPagos) <= 0){
            return null;
        }

        $oResumen = $resumenPagos[0];
        if($oResumen->cantidad == null){
            $oResumen->cantidad = 0;
        }
        if($oResumen->importe_total == null){
            $oResumen->importe_total = 0;
        }

        return $oResumen;
    }

    public function get_resumen_comprobantes($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo){
        $resumenComprobantes = $this->Nube_comprobante_model->get_totales($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo);
        if(count($resumenComprobantes) <= 0){
            return null;
        }

        $oResumen = $resumenComprobantes[0];
        if($oResumen->cantidad == null){
            $oResumen->cantidad = 0;
        }
        if($oResumen->importe_total == null){
            $oResumen->importe_total = 0;
        }

        return $oResumen;
    }

    public function ajax_modal_ver_comandas(){

        $sucursal_codigo   = (!isset($_REQUEST['sucursal_codigo'])) ? ""  : trim($_REQUEST['sucursal_codigo']);
        $empresa_codigo   = (!isset($_REQUEST['empresa_codigo'])) ? ""  : trim($_REQUEST['empresa_codigo']);
        $fecha_ini   = (!isset($_REQUEST['fecha_ini'])) ? ""  : trim($_REQUEST['fecha_ini']);
        $fecha_fin   = (!isset($_REQUEST['fecha_fin'])) ? ""  : trim($_REQUEST['fecha_fin']);

        $lstComandas = $this->Nube_comanda_model->get_comandas_by_fechas($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo);

        foreach ($lstComandas as $key => $oComanda) {
            $oComanda->detalle = $this->Nube_comanda_model->get_detalle_por_comanda($oComanda->comanda_codigo);
            $oComanda->detalle_anulados = 0;

            foreach ($oComanda->detalle as $key => $oDetalle) {
                if($oDetalle->estado_atencion == "X"){
                    $oComanda->detalle_anulados++;
                }
            }
        }

        $data['lstComandas'] = $lstComandas;
        $data['fecha_ini'] = $fecha_ini;

        $this->load->view('administracion/locales/_modal_ver_comandas',$data);
    }

    public function ajax_modal_ver_pagos(){

        $sucursal_codigo   = (!isset($_REQUEST['sucursal_codigo'])) ? ""  : trim($_REQUEST['sucursal_codigo']);
        $empresa_codigo   = (!isset($_REQUEST['empresa_codigo'])) ? ""  : trim($_REQUEST['empresa_codigo']);
        $fecha_ini   = (!isset($_REQUEST['fecha_ini'])) ? ""  : trim($_REQUEST['fecha_ini']);
        $fecha_fin   = (!isset($_REQUEST['fecha_fin'])) ? ""  : trim($_REQUEST['fecha_fin']);

        $lstPagos = $this->Nube_pago_model->get_pagos_by_fechas($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo);
        $data['lstPagos'] = $lstPagos;
        $data['fecha_ini'] = $fecha_ini;

        $this->load->view('administracion/locales/_modal_ver_pagos',$data);
    }

    public function ajax_modal_ver_comprobantes(){

        $sucursal_codigo   = (!isset($_REQUEST['sucursal_codigo'])) ? ""  : trim($_REQUEST['sucursal_codigo']);
        $empresa_codigo   = (!isset($_REQUEST['empresa_codigo'])) ? ""  : trim($_REQUEST['empresa_codigo']);
        $fecha_ini   = (!isset($_REQUEST['fecha_ini'])) ? ""  : trim($_REQUEST['fecha_ini']);
        $fecha_fin   = (!isset($_REQUEST['fecha_fin'])) ? ""  : trim($_REQUEST['fecha_fin']);

        $lstComprobantes = $this->Nube_comprobante_model->get_comprobantes_by_fechas($fecha_ini, $fecha_fin, $empresa_codigo, $sucursal_codigo);
        $data['lstComprobantes'] = $lstComprobantes;
        $data['fecha_ini'] = $fecha_ini;

        $this->load->view('administracion/locales/_modal_ver_comprobantes',$data);
    }

    public function ajax_obtener_array_para_pie_mensual(){

        //Parametros de entrada
        $fecha_ini   = (!isset($_REQUEST['fecha_ini'])) ? ""  : trim($_REQUEST['fecha_ini']);
        $fecha_date = strtotime($fecha_ini);
        $anio = date("Y", $fecha_date);
        $mes = date("m", $fecha_date);
        $dia = date("d", $fecha_date);

        //Obtener totales
        $totales_por_dia = $this->Nube_comanda_model->get_totales_por_dia_mes_anio($dia, $mes, $anio);
        $total = 0;
        $array_pie = [];

        //Calcular el total, el 100%
        foreach ($totales_por_dia as $key => $value) {
            $total += round($value->importe, 2);
        }

        //Calcular porcentaje
        foreach ($totales_por_dia as $key => $value) {
            $porcentaje = round(round($value->importe, 2) * 100 / $total, 2);

            $array_pie[] = array( 
                                'name'  => strtoupper($value->empresa_nombre).'<br>'.$value->sucursal_nombre.'<br>', 
                                'y'     => $porcentaje, 
                                'drilldown'=>$value->sucursal_nombre
                            );
        }

        echo json_encode($array_pie);
    }

    public function ajax_obtener_array_para_pie_mensual_agrupa_modalidad(){

        //Parametros de entrada
        $fecha_ini   = (!isset($_REQUEST['fecha_ini'])) ? ""  : trim($_REQUEST['fecha_ini']);
        $fecha_date = strtotime($fecha_ini);
        $anio = date("Y", $fecha_date);
        $mes = date("m", $fecha_date);
        $dia = date("d", $fecha_date);

        //Obtener totales
        $totales_por_dia = $this->Nube_comanda_model->get_totales_por_dia_mes_anio_agrupa_modalidad($dia, $mes, $anio);
        $total = 0;
        $array_pie = [];

        //Calcular el total, el 100%
        foreach ($totales_por_dia as $key => $value) {
            $total += round($value->cantidad, 2);
            switch ($value->modalidad) {
                case 'DEL':
                    $value->modalidad = "DELIV";
                    break;
                case 'LLE':
                    $value->modalidad = "LLEVA";
                    break;
                case 'PRE':
                    $value->modalidad = "PRESE";
                    break;
                default:
                    // code...
                    break;
            }
        }

        //Calcular porcentaje
        foreach ($totales_por_dia as $key => $value) {
            $porcentaje = round(round($value->cantidad, 2) * 100 / $total, 2);

            $array_pie[] = array( 
                                'name'  => strtoupper($value->modalidad).' ('.$value->cantidad.')<br>', 
                                'y'     => $porcentaje, 
                                'drilldown'=>$value->modalidad
                            );
        }

        echo json_encode($array_pie);
    }
}
