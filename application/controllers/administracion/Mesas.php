<?php defined('BASEPATH') or exit('No direct script access allowed');

class Mesas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mesa_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function index(){
        $data['oMesa'] =  new Mesa_model();
        $data['mesas'] = $this->Mesa_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar mesas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Mesas', 'bc' => $bc,'modulo'=>'administracion', 'item' => 'mesas' );
        $this->page_construct('administracion/mesas/index', $meta, $data);
    }
    function editar($mesa_id){
        $oMesa = $this->Mesa_model->get_one($mesa_id);
        $data['oMesa'] = $oMesa[0];
        $data['mesas'] = $this->Mesa_model->get_all();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar mesas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Mesas', 'bc' => $bc,'modulo'=>'administracion', 'item' => 'mesas' );
        $this->page_construct('administracion/mesas/index', $meta, $data);
    }

    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        $data = array(  'mesa_id' => $this->input->post('mesa_id'),
                        'capacidad' => $this->input->post('capacidad'),
                        'mesa' => $this->input->post('mesa')
                    );
        $this->form_validation->set_rules('mesa', "Mesa", 'required');
        $this->form_validation->set_rules('capacidad', "Capacidad", 'required');

        $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

        if ($this->form_validation->run() == true) {
            if( $this->input->post('mesa_id') == 0 ){
                $mesa_id = $this->Mesa_model->insert($data);
                if(!$mesa_id){
                    $this->session->set_flashdata('success', false);
                    $this->session->set_flashdata('message', 'Problemas para registrar la mesa.');
                }else{
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Mesa registrada correctamente.");
                }
            }else{
                $rpta = $this->Mesa_model->update($data);
                if(!$rpta){
                    $this->session->set_flashdata('success', false);
                    $this->session->set_flashdata('message', 'Problemas para actualizar la mesa.');
                }else{
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Mesa actualizada correctamente.");
                }
            }
        }else{
            $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
            $this->session->set_flashdata('message',$message);
            $this->session->set_flashdata('success',false);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function eliminar($mesa_id){
        try {
            $this->loaders->verifica_sesion();
            $comandas = $this->Mesa_model->comandas_by_mesa($mesa_id);
            if(count($comandas) > 0){
                throw new Exception("No se puede eliminar la mesa, debido a que tiene comandas relacionados.");
            }
            if( $this->Mesa_model->delete($mesa_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Mesa eliminada correctamente.');
                redirect(base_url('administracion/mesas'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar la mesa.');
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}
