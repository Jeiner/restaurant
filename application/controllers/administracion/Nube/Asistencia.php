<?php defined('BASEPATH') or exit('No direct script access allowed');
  
class Asistencia extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->model('nube/Nube_comanda_model');
        $this->load->model('nube/Nube_pago_model');
        $this->load->model('nube/Nube_comprobante_model');
        $this->load->model('nube/Nube_marcacion_model');
        $this->load->model('nube/Nube_marcacion_empleado_model');
        // $this->load->model('Multitabla_model');
        // $this->load->model('Pide');
        // $this->load->library('session');
        $this->load->library('session');
    }

    function index(){

        $anio   = (!isset($_REQUEST['anio'])) ? ""  : trim($_REQUEST['anio']);
        $mes   = (!isset($_REQUEST['mes'])) ? ""  : trim($_REQUEST['mes']);

        $anio = ($anio == "") ? date('Y') : $anio;
        $mes = ($mes == "") ? date('m') : $mes;

        //Obtener empleados
        $lstEmpleados = $this->Nube_marcacion_empleado_model->get_empleados();

        //Obtener marcaciones
        $lstMarcaciones = $this->Nube_marcacion_model->get_marcaciones_por_anio_mes($anio, $mes);

        //Asignar marcaciones a cada empleado
        //Asignar configuraciones
        //Asignar asistenca, hora de entrada y salida
        $lstAsistenciaBase = $this->obtenerAsistenciaBase($anio, $mes);
        foreach ($lstEmpleados as $key => $oEmpleado) {
            $oEmpleado->lstMarcaciones = $this->desglosar_marcaciones_por_empleado($lstMarcaciones, $oEmpleado->emp_code);
            $oEmpleado->lstAsistencia = $this->asignarHoraEntradaSalida($oEmpleado, $lstAsistenciaBase);
            $oEmpleado->resumen = $this->resumenAsistencia($oEmpleado->lstAsistencia);
        }

        //Meses disponibles a mostrar
        $lstMesesMostrar = $this->calcularMesesParaMostrar($anio, $mes, 2, 2);

        //Obtener última actualización
        $ultimaActualizacion = $this->obtenerUltimaActualizacion();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Asistencias' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => '', 'bc' => $bc, 'modulo'=>'', 'item' => '' );
        $meta['hide_menus'] = true;


        $meta['anio'] = $anio;
        $meta['mes'] = $mes;
        $meta['lstEmpleados'] = $lstEmpleados;
        $meta['lstMesesMostrar'] = $lstMesesMostrar;
        $meta['ultimaActualizacion'] = $ultimaActualizacion;
        
        

        $this->load->view('administracion/nube/asistencia/index', $meta);
    }

    function obtenerUltimaActualizacion(){
        $ultimoRegistro = $this->Nube_marcacion_model->obtenerUltimoRegistro();
        if($ultimoRegistro == null || count($ultimoRegistro) <= 0){
            return "";
        }
        $dateUltimaActualizacion = strtotime($ultimoRegistro[0]->fecha_registro);
        $ultimaActualizacion = date("d-m-Y H:i:s", $dateUltimaActualizacion);

        if(date('Y-m-d') == date("Y-m-d", $dateUltimaActualizacion)){
            $ultimaActualizacion = "Hoy ".date("H:i:s", $dateUltimaActualizacion);
        }

        return $ultimaActualizacion;
    }

    function calcularMesesParaMostrar($anio, $mes, $nroMesesPrev, $nroMesesPost){
        $lstMesesMostrar = [];
        $lstMesesPrevios = [];
        $primerDia = strtotime($anio.'-'.$mes.'-'.'01');

        //Encontrar meses previos
        $diaDelMes = $primerDia;
        for ($i=0; $i < $nroMesesPrev; $i++) {
            $diaDelMes  = strtotime("-1 month", $diaDelMes);
            $anioAnterior = date("Y", $diaDelMes);
            $mesAnterior = date("m", $diaDelMes);

            $oMes['anio'] = $anioAnterior;
            $oMes['mes'] = $mesAnterior;
            $oMes['mesAbrev'] = $this->obtenerAbreviaturaMes($mesAnterior);

            $lstMesesPrevios[] = $oMes;
        }
        for ($i=count($lstMesesPrevios); $i > 0 ; $i--) { 
            $lstMesesMostrar[] = $lstMesesPrevios[$i-1];
        }

        //Encontrar meses posteriores
        $diaDelMes = $primerDia;
        for ($i=0; $i < $nroMesesPost + 1; $i++) { 
            $anioAnterior = date("Y", $diaDelMes);
            $mesAnterior = date("m", $diaDelMes);
            $diaDelMes  = strtotime("+1 month", $diaDelMes);

            $oMes['anio'] = $anioAnterior;
            $oMes['mes'] = $mesAnterior;
            $oMes['mesAbrev'] = $this->obtenerAbreviaturaMes($mesAnterior);

            $lstMesesMostrar[] = $oMes;
        }

        return $lstMesesMostrar;
    }

    function resumenAsistencia($lstAsistencias){
        $resumen['contadorTardanzas'] = 0;
        $resumen['contadorFaltas'] = 0;
        $resumen['contadorPuntuales'] = 0;
        foreach ($lstAsistencias as $key => $oAsistencia) {
            if($oAsistencia['estado_entrada'] == 1){
                $resumen['contadorPuntuales']++;
            }

            if($oAsistencia['estado_entrada'] == -1){
                $resumen['contadorTardanzas']++;
            }

            //Faltas
            if($oAsistencia['estado_entrada'] == -2){
                $resumen['contadorFaltas']++;
            }
        }
        return $resumen;
    }
    function asignarHoraEntradaSalida($oEmpleado, $lstAsistenciaBase){
        $minTolEntrada = 10;         //Hasta "N" minutos
        $horaIniciarMarcacionIngreso = "14:00";
        $horaIniciarMarcacionSalida = "20:00";
        $lstAsistenciasLocal = [];
        $fechaActual = date('Y-m-d');

        //Encontrar marcaciones
        foreach ($lstAsistenciaBase as $key => $oAsistencia) {

            $cHoraEntrada = "";
            $diffMinEntrada = 999;

            $cHoraSalida = "";
            $diffMinSalida = -999;

            $cFecha = $oAsistencia['fecha'];
            $lstMarcacionesPorDia = [];

            //Escoger la hora de netrada y salida para cada día
            foreach ($oEmpleado->lstMarcaciones as $key => $oMarcacion) {
                if($cFecha == $oMarcacion->fecha){

                    $lstMarcacionesPorDia[] = $oMarcacion;

                    //Hora entrada (Tomar la hora menor)
                    if($oMarcacion->hora > $horaIniciarMarcacionIngreso && $oMarcacion->hora < $horaIniciarMarcacionSalida){
                        if($cHoraEntrada == "" || $oMarcacion->hora < $cHoraEntrada){
                            $cHoraEntrada = $oMarcacion->hora;
                        }
                    }

                    //Hora salida (Tomar la hora mayor)
                    if($oMarcacion->hora > $horaIniciarMarcacionSalida){
                        if($cHoraSalida == "" || $oMarcacion->hora > $cHoraSalida){
                            $cHoraSalida = $oMarcacion->hora;
                        }
                    }
                }
            }

            //Asignar hora de entrada y salida
            $oAsistencia['lst_marcaciones'] = $lstMarcacionesPorDia;
            $oAsistencia['entrada'] = $cHoraEntrada;
            $oAsistencia['salida'] = $cHoraSalida;

            $oAsistencia['estado_entrada'] = 0;
            //Calcular diferencia de entrada
            if($cHoraEntrada != "" && $oEmpleado != null){
                $entrada_config = $oEmpleado->hora_entrada;
                $diffMinEntrada = $this->Site->minutos_transcurridos_con_signo($cFecha." ".$cHoraEntrada, $cFecha." ".$entrada_config);
                $oAsistencia['estado_entrada'] = ($diffMinEntrada > $minTolEntrada)? -1 : 1;
            }

            //Faltas
            if($cHoraEntrada == ""){

                $date_ingreso = strtotime($oEmpleado->fecha_ingreso);
                $fecha_ingreso = date('Y-m-d', $date_ingreso);

                if($oAsistencia['fecha'] < $fechaActual && $oAsistencia['fecha'] > $fecha_ingreso){
                   $oAsistencia['estado_entrada'] = -2;
                }
            }

            ///Calcular diferencia de salida
            if($cHoraSalida != "" && $oEmpleado != null){
                $salida_config = $oEmpleado->hora_salida;
                $diffMinSalida = $this->Site->minutos_transcurridos_con_signo($cFecha." ".$cHoraSalida, $cFecha." ".$salida_config);
            }

            $lstAsistenciasLocal[] = $oAsistencia;
        }

        return $lstAsistenciasLocal;
    }

    function verificarTardanza(){

    }

    function obtenerAsistenciaBase($anio, $mes){
        $number = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
        $lstDiasPorMes = [];
        $fecha = strtotime($anio.'-'.$mes.'-'.'01');

        for ($i=0; $i < $number; $i++) {
            $dia['fecha'] = date("Y-m-d", $fecha);
            $fecha = $fecha +86400;

            $dia['dia'] = $i+1;
            $dia['mes'] = $mes;
            $dia['anio'] = $anio;

            $dia['lst_marcaciones'] = [];
            $dia['entrada'] = "";
            $dia['salida'] = "";
            $dia['flag_tardanza'] = 0;
            $lstDiasPorMes[] = $dia;
        }

        return $lstDiasPorMes;
    }

    function desglosar_marcaciones_por_empleado($lstMarcaciones, $emp_code){
        $lstMarcacionesPorEmpleado = [];
        foreach ($lstMarcaciones as $key => $oMarcacion) {
            //Desglosar hora y minutos
            $d = strtotime($oMarcacion->punch_time);
            $oMarcacion->fecha = date("Y-m-d", $d);
            $oMarcacion->hora = date("H:i", $d);

            if($oMarcacion->emp_code == $emp_code){
                $lstMarcacionesPorEmpleado[] = $oMarcacion;
            }
        }
        return $lstMarcacionesPorEmpleado;
    }

    function ajax_detalle_por_usuario(){

        $mes   = (!isset($_REQUEST['mes'])) ? ""  : trim($_REQUEST['mes']);
        $anio   = (!isset($_REQUEST['anio'])) ? ""  : trim($_REQUEST['anio']);
        $emp_code   = (!isset($_REQUEST['emp_code'])) ? ""  : trim($_REQUEST['emp_code']);

        //Obtener empleados
        $buscarEmpleado = $this->Nube_marcacion_empleado_model->get_empleado_por_code($emp_code);
        $oEmpleado = $buscarEmpleado[0];

        //Obtener marcaciones
        $lstMarcaciones = $this->Nube_marcacion_model->get_marcaciones_por_anio_mes_empleado($anio, $mes, $emp_code);

        //Asignar marcaciones a cada empleado
        //Asignar configuraciones
        //Asignar asistenca, hora de entrada y salida
        $lstAsistenciaBase = $this->obtenerAsistenciaBase($anio, $mes);
        $oEmpleado->lstMarcaciones = $this->desglosar_marcaciones_por_empleado($lstMarcaciones, $oEmpleado->emp_code);
        $oEmpleado->lstAsistencia = $this->asignarHoraEntradaSalida($oEmpleado, $lstAsistenciaBase);
        $oEmpleado->resumen = $this->resumenAsistencia($oEmpleado->lstAsistencia);


        $data['fechaActual'] = date('Y-m-d');
        $data['oEmpleado'] = $oEmpleado;
        $data['mes'] = $mes;
        $data['anio'] = $anio;
        $data['mesAbreviatura'] = $this->obtenerAbreviaturaMes($mes);
        $data['mesNombre'] = $this->obtenerNombreMes($mes);

        $this->load->view('administracion/nube/asistencia/modal_asistencia_detalle',$data);
    }

}
