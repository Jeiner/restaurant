<?php defined('BASEPATH') or exit('No direct script access allowed');

class Insumos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Insumo_model');
        $this->load->model('Multitabla_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_insumo(){
        if($this->session->userdata('oInsumo')){
            return $this->session->userdata('oInsumo');
        }else{
            $oInsumo = new Insumo_model();
            $this->session->set_userdata('oInsumo', $oInsumo);
            return $this->session->userdata('oInsumo');
        }
    }
    function set_datos_obj_insumo($data){
        $oInsumo =  $this->preparar_obj_insumo();

        $oInsumo->insumo_id = $data['insumo_id'];
        $oInsumo->insumo = $data['insumo'];
        $oInsumo->precio_costo = $data['precio_costo'];
        $oInsumo->unidad = $data['unidad'];
        $oInsumo->stock = $data['stock'];
        $oInsumo->stock_minimo = $data['stock_minimo'];
        $oInsumo->stock_maximo = $data['stock_maximo'];
        $oInsumo->descripcion = $data['descripcion'];
        $oInsumo->estado = $data['estado'];

        $this->session->set_userdata('oInsumo', $oInsumo);
    }
    function index(){
        $this->session->unset_userdata('oInsumo');
        $data['insumos'] = $this->Insumo_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar insumos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Insumos', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'insumos' );
        $this->page_construct('administracion/insumos/index', $meta, $data);
    }
    function nuevo(){
        $data['oInsumo'] = $this->preparar_obj_insumo();
        $data['unidades'] = $this->Multitabla_model->get_all_by_tipo(1);
        // echo json_encode($data['unidad']); exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar insumos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Registrar insumo', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'insumos' );
        $this->page_construct('administracion/insumos/nuevo', $meta, $data);
    }
    function editar($insumo_id){
        $this->session->unset_userdata('oInsumo');
        $data['oInsumo'] = ($this->Insumo_model->get_one($insumo_id))[0];
        $data['unidades'] = $this->Multitabla_model->get_all_by_tipo(1);
        // echo json_encode($data['oInsumo']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar insumos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Editar insumo', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'insumos' );
        $this->page_construct('administracion/insumos/editar', $meta, $data);
    }



    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $data = array(  'insumo_id' => $this->input->post('insumo_id'),
                            'insumo' => $this->input->post('insumo'),
                            'precio_costo' => ($this->input->post('precio_costo')) != ""? $this->input->post('precio_costo') : null,
                            'unidad' => $this->input->post('unidad'),
                            'stock' => $this->input->post('stock'),
                            'stock_minimo' => ($this->input->post('stock_minimo')) != ""? $this->input->post('stock_minimo') : null,
                            'stock_maximo' => ($this->input->post('stock_maximo')) != ""? $this->input->post('stock_maximo') : null,
                            'descripcion' => $this->input->post('descripcion'),
                            'estado' => $this->input->post('estado'),
                        );
            // echo json_encode($data);exit();
            $this->set_datos_obj_insumo($data);
            $this->form_validation->set_rules('insumo', "Insumo", 'required|min_length[3]');
            $this->form_validation->set_rules('unidad', "Unidad", 'required');
            $this->form_validation->set_rules('stock', "Stock actual", 'required');
            
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('insumo_id') == 0 ){
                    $insumo_id = $this->Insumo_model->insert($data);
                    if(!$insumo_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar el insumo.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Insumo registrado correctamente.");
                        redirect(base_url('administracion/insumos'));
                    }
                }else{
                    $rpta = $this->Insumo_model->update($data);
                    if(!$rpta){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para actualizar el insumo.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Insumo actualizado correctamente.");
                        redirect(base_url('administracion/insumos'));
                    }
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('administracion/insumos'));
        }
    }
    function eliminar($insumo_id){
        $this->loaders->verifica_sesion();
        $oInsumo = $this->Insumo_model->get_one($insumo_id);

        $this->session->set_flashdata('success', false);
        $this->session->set_flashdata('message', 'Debe verificar que no haya ordenes referenciadas a esta insumo.');        
        redirect($_SERVER["HTTP_REFERER"]);

            if( $this->Insumo_model->delete($insumo_id) ){
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Insumo eliminado correctamente.');
                redirect(base_url('administracion/insumos'));
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'No se puede eliminar el insumo.');
            }
        redirect($_SERVER["HTTP_REFERER"]);
    }


    // OTRAS OPERACIONES
    function cargar_tabla_insumos_sm(){
        // $familia_id = $this->input->post('familia_id');
        // $data['insumos'] = $this->Insumo_model->get_by_familia($familia_id);
        // $this->load->view('administracion/insumos/_table_insumos_sm',$data);
    }
}
