<?php defined('BASEPATH') or exit('No direct script access allowed');

class Comanda extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Comanda_model');
        $this->load->model('Mesa_model');
        $this->load->model('Union_mesa_model');
        $this->load->model('Producto_model');
        $this->load->model('Familia_model');
        $this->load->model('Insumo_model');
        $this->load->model('Cliente_model');
        $this->load->model('Caja_model');
        $this->load->model('Pago_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Generalidades_model');
        $this->load->model('Documento_electronico_model');
        $this->load->model('Comentario_model');
        $this->load->model('Variables_model');
        $this->load->model('Comunicacion_baja_model');

        $this->load->library('session');
        // $this->loaders->verifica_sesion();
        // $this->Caja_model->verificar_caja_aperturada();
    }
    function preparar_obj_comanda(){
        if($this->session->userdata('oComanda')){
            return $this->session->userdata('oComanda');
        }else{
            $oComanda = new Comanda_model();
            $this->session->set_userdata('oComanda', $oComanda);
            return $this->session->userdata('oComanda');
        }
    }
    function set_datos_obj_comanda($data){
        $oComanda =  $this->preparar_obj_comanda();

        $oComanda->comanda_id = $data['comanda_id'];
        $oComanda->fecha_comanda = $data['fecha_comanda'];
        $oComanda->modalidad = $data['modalidad'];
        $oComanda->mozo = $data['mozo'];
        $oComanda->mesa_id = $data['mesa_id'];
        $oComanda->mesa = $data['mesa'];
        $oComanda->nro_comensales = $data['nro_comensales'];
        $oComanda->nro_productos = $data['nro_productos'];
        $oComanda->importe_total = $data['importe_total'];

        $oComanda->caja_id = $data['caja_id'];
        $oComanda->cliente_id = $data['cliente_id'];
        $oComanda->cliente_documento = $data['cliente_documento'];
        $oComanda->cliente_nombre = $data['cliente_nombre'];
        $oComanda->cliente_telefono = $data['cliente_telefono'];
        $oComanda->cliente_direccion = $data['cliente_direccion'];

        $oComanda->estado_atencion = $data['estado_atencion'];
        $oComanda->estado_pago = $data['estado_pago'];
        $oComanda->cliente_tipo_documento = "1";

        $this->session->set_userdata('oComanda', $oComanda);
    }
    function index(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        // echo json_encode($this->preparar_obj_comanda()); exit();
        $mesa_id = $this->input->get('mesa_id');
        $data['oMesa'] = ($this->Mesa_model->get_one($mesa_id))[0];
        
        if($data['oMesa']->estado == 'L'){ //Si esta libre
            $ruta_nueva = base_url('salon/comanda/nueva')."?mesa_id=".($data['oMesa'])->mesa_id;
            redirect($ruta_nueva);
        }else{ 
            //Si esta ocupada, verifico que haya una comanda de esa mesa que no este finalizada.
            if(count($this->Comanda_model->get_one_por_mesa_actual($mesa_id)) == 0){
                //Verificar si dicha mesa esta unida

                // No esta unida 
                $this->session->set_flashdata('success',false);
                $this->session->set_flashdata('message',"No se encontró la comanda para dicha mesa o ya finalizó.");
                redirect(base_url('salon/salon'));
            }else{
                // obtenemos la comanda actual de la mesa 
                $data['oComanda'] = ($this->Comanda_model->get_one_por_mesa_actual($mesa_id))[0];
                $ruta_ver = base_url('salon/comanda/editar/').$data['oComanda']->comanda_id;
                redirect($ruta_ver);
            }
        }
    }
    function nueva(){
        $this->Caja_model->verificar_caja_aperturada();
        try {
            $mesa_id = $this->input->get('mesa_id');
            if(count( $this->Mesa_model->get_one($mesa_id)) == 0){
                throw new Exception("Error: No se encontró la mesa especificada. Porfavor comuníquese con el área de soporte.");
            }else{
                $data['oMesa'] = ($this->Mesa_model->get_one($mesa_id))[0];
            }
            if($data['oMesa']->estado != 'L'){
                throw new Exception("Lo sentimos, al parecer la mesa ya está ocupada.");
            }
            $data['oCaja'] = ($this->Caja_model->get_aperturada())[0];
            // $data['productos'] = $this->Producto_model->get_all();
            $data['familias'] = $this->Familia_model->get_all('A');
            $data['modalidades'] = $this->Multitabla_model->get_all_by_tipo(3);
            $data['canales'] = $this->Multitabla_model->get_all_by_tipo(40);
            $data['tipos_documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);
            $data['productos'] = $this->Producto_model->get_all();
            $data['kits'] = $this->Producto_model->get_insumos_all();
            // $data['insumos'] = $this->Insumo_model->get_all();
            // $data['clientes'] = $this->Cliente_model->get_all();
            //Cuando se viene a crear una nueva comanda despues de una edición eliminamos la session de la comanda anterior
            $data['oComanda'] = $this->preparar_obj_comanda();
            if( $data['oComanda']->comanda_id != 0 ){
                $this->session->unset_userdata('oComanda');
            }
            $data['oComanda'] = $this->preparar_obj_comanda();
            $data['oComanda']->mozo = $this->session->userdata('session_usuario');
            $data['oComanda']->modalidad = "PRE";
            $data['oComanda']->mesa = $data['oMesa']->mesa;
            $data['oComanda']->mesa_id = $data['oMesa']->mesa_id;
            $data['oComanda']->caja_id = $data['oCaja']->caja_id;

            // $data['oComanda']->fecha_comanda = date("d/m/Y H:i:s");
            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Salón' ),
                        array('link' => '#', 'page' => 'Nueva Comanda' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Nueva Comanda', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'salon');
            $this->page_construct('salon/comanda/nueva', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }
    function delivery(){
        $this->Caja_model->verificar_caja_aperturada();
        $data['oCaja'] = ($this->Caja_model->get_aperturada())[0];
        // $data['productos'] = $this->Producto_model->get_all();
        $data['familias'] = $this->Familia_model->get_all();
        $data['modalidades'] = $this->Multitabla_model->get_all_by_tipo(3);
        $data['canales'] = $this->Multitabla_model->get_all_by_tipo(40);
        $data['tipos_documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);
        $data['clientes'] = $this->Cliente_model->get_all();
        //Cuando se viene a crear una nueva comanda despues de una edición eliminamos la session de la comanda anterior
        $data['oComanda'] = $this->preparar_obj_comanda();
        if( $data['oComanda']->comanda_id != 0 ){
            $this->session->unset_userdata('oComanda');
        }
        $data['oComanda'] = $this->preparar_obj_comanda();
        $data['oComanda']->mozo = $this->session->userdata('session_usuario');
        $data['oComanda']->mesa = "";
        $data['oComanda']->mesa_id = "";
        $data['oComanda']->caja_id = $data['oCaja']->caja_id;
        $data['oComanda']->modalidad = "DEL";
        $data['productos'] = $this->Producto_model->get_all();
        $data['kits'] = $this->Producto_model->get_insumos_all();
        // $data['oComanda']->fecha_comanda = date("d/m/Y H:i:s");
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Nueva Comanda' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nueva Comanda', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'salon');
        $this->page_construct('salon/comanda/nueva', $meta, $data);
    }
    function para_llevar(){
        $this->Caja_model->verificar_caja_aperturada();
        $data['oCaja'] = ($this->Caja_model->get_aperturada())[0];
        // $data['productos'] = $this->Producto_model->get_all();
        $data['familias'] = $this->Familia_model->get_all();
        $data['modalidades'] = $this->Multitabla_model->get_all_by_tipo(3);
        $data['canales'] = $this->Multitabla_model->get_all_by_tipo(40);
        $data['tipos_documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);
        $data['clientes'] = $this->Cliente_model->get_all();
        //Cuando se viene a crear una nueva comanda despues de una edición eliminamos la session de la comanda anterior
        $data['oComanda'] = $this->preparar_obj_comanda();
        if( $data['oComanda']->comanda_id != 0 ){
            $this->session->unset_userdata('oComanda');
        }
        $data['oComanda'] = $this->preparar_obj_comanda();
        $data['oComanda']->mozo = $this->session->userdata('session_usuario');
        $data['oComanda']->mesa = "";
        $data['oComanda']->mesa_id = "";
        $data['oComanda']->caja_id = $data['oCaja']->caja_id;
        $data['oComanda']->modalidad = "LLE";
        $data['productos'] = $this->Producto_model->get_all();
        $data['kits'] = $this->Producto_model->get_insumos_all();
        // $data['oComanda']->fecha_comanda = date("d/m/Y H:i:s");
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Nueva Comanda' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Nueva Comanda', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'salon');
        $this->page_construct('salon/comanda/nueva', $meta, $data);
    }
    function editar($comanda_id){
        try {
            $this->Caja_model->verificar_caja_aperturada();
            // echo $comanda_id;exit()
            if(count($this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, No se encontró la comanda que especificada.");
                return;
            }

            $data['productos'] = $this->Producto_model->get_by_familia(); //Para agregar nuevos productos a la comanda
            $data['comprobantes'] = $this->Multitabla_model->get_all_by_tipo(7);
            $data['comprobante_sunat'] = $this->Multitabla_model->get_all_by_tipo(17);
            $data['modalidades'] = $this->Multitabla_model->get_all_by_tipo(3);
            $data['canales'] = $this->Multitabla_model->get_all_by_tipo(40);
            $data['tipos_documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);

            // Datos de la comanda
            $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
            $data['oComanda']->items = $this->Comanda_model->get_items_by_comanda($comanda_id);

            //Cliente en la comanda
            $data['oCliente'] = $this->cliente_desde_comanda($data['oComanda']);

            //Pagos
            $pagos = $this->Pago_model->get_one_by_comanda($comanda_id);
            $data['oPago'] = (count($pagos) <= 0)? null :  $pagos[0];

            //Comprobantes electrónicos
            $data['oCpbeCabecera'] = $this->Documento_electronico_model->get_cpbe_CAB_by_comanda($comanda_id);
            $data['oCpbeDetalle'] = $this->Documento_electronico_model->get_cpbe_DET_by_comanda($comanda_id);
            $data['lstCpbeBajas'] = $this->Comunicacion_baja_model->get_bajas_by_comanda($comanda_id);

            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Salón' ),
                        array('link' => '#', 'page' => 'Detalle de Comanda' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Detalle de Comanda', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'salon' );
            $this->page_construct('salon/comanda/editar', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('facturacion/pagos/comandas'));
        }
    }

    function ver($comanda_id){
        try {
            // $comanda_id = $this->input->get('comanda_id');
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda, por favor comuníquese con el área de soporte.");
            }else{
                $data['comprobantes'] = $this->Multitabla_model->get_all_by_tipo(7);
                $data['comprobante_sunat'] = $this->Multitabla_model->get_all_by_tipo(17);
                $data['tipos_documento_identidad'] = $this->Multitabla_model->get_all_by_tipo(22);

                // Datos de la comanda
                $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
                $data['oComanda']->items = $this->Comanda_model->get_items_by_comanda($comanda_id);

                //Cliente en la comanda
                $data['oCliente'] = $this->cliente_desde_comanda($data['oComanda']);

                //Pagos
                $pagos = $this->Pago_model->get_one_by_comanda($comanda_id);
                $data['oPago'] = (count($pagos) <= 0)? null :  $pagos[0];

                //Comprobantes electrónicos
                $data['oCpbeCabecera'] = $this->Documento_electronico_model->get_cpbe_CAB_by_comanda($comanda_id);
                $data['oCpbeDetalle'] = $this->Documento_electronico_model->get_cpbe_DET_by_comanda($comanda_id);

                $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Ver comanda' ),
                ); //breadcrumbs 
                $meta = array( 'page_title' => 'Ver comanda', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'historial_comandas' );
                $this->page_construct('salon/comanda/ver', $meta, $data);

            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('facturacion/pagos/comandas'));
        }
    }
    
    function agregar_comentario_item(){
        $producto_id = $this->input->post('producto_id');
        $comentario = $this->input->post('comentario');

        // obtenemos la comanda con los items
        $data['oComanda'] = $this->preparar_obj_comanda();
        $existe = false; $index = -1;
        //Buscamos el item 
        foreach ($data['oComanda']->items as $key => $item){
            if($item->producto_id == $producto_id){
                $item->comentario = $comentario;
                break;
            }
        }
        $this->partial_view('salon/comanda/_productos_seleccionados',$data);
    }
    function agregar_producto_adicional(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        try {
            $data = array(  'comanda_id' => $this->input->post('comanda_id'),
                            'producto_id' => $this->input->post('add_producto_id'),
                            'cantidad' => $this->input->post('add_cantidad'),
                            'comentario' => $this->input->post('add_comentario'),
                            );
            $this->form_validation->set_rules('comanda_id', "Comanda", 'required');
            $this->form_validation->set_rules('add_producto_id', "Producto", 'required');
            $this->form_validation->set_rules('add_cantidad', "Cantidad", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

            if ($this->form_validation->run() == true) {
                // Obtengo la comand
                $oComanda = ($this->Comanda_model->get_one($data['comanda_id']))[0];
                $oProducto = ($this->Producto_model->get_one($data['producto_id']))[0];
                if($oComanda->estado_atencion == 'F'){
                    throw new Exception("No se puede agregar un producto adicional a la comanda, debido a que ya está finalizada.");
                }
                if($oComanda->estado_atencion == 'X'){
                    throw new Exception("No se puede agregar un producto adicional a la comanda, debido a que esta anulada.");
                }
                if($oComanda->estado_pago == 'C'){
                    throw new Exception("No se puede agregar un producto adicional a la comanda, debido a que ya está pagada.");
                }
                if($oComanda->estado_pago == 'X'){
                    throw new Exception("No se puede agregar un producto adicional a la comanda, debido a que está anulada.");
                }
                // Verificamos que la cantidad sea mayor a 0
                if( $data['cantidad'] <= 0){
                    throw new Exception("No se puede vender una cantidad menor o igual a 0.");
                }
                // Validación de stock de insumos y productos.
                // Verificamos que haya stock, siempre y cuando se maneje stock
                if($oProducto->manejar_stock == 1){
                    if($oProducto->stock < $data['cantidad']){
                        throw new Exception("No hay stock suficiente.");
                    }
                }else{
                    //Verificamos que haya stock insumos si tiene por lo menos 1
                    $kit = $this->Producto_model->get_insumos_by_producto($data['producto_id']);
                    foreach ($kit as $key => $item){
                        $cantidad_insumo = $data['cantidad'] * $item->cantidad;
                        $insumo = ($this->Insumo_model->get_one($item->insumo_id))[0];
                        if($insumo->stock < $cantidad_insumo){
                            $mensaje = "No hay stock suficiente del insumo: ".$insumo->insumo.".";
                            throw new Exception($mensaje);
                        }
                    }
                } 
                $item = array(
                    // 'comanda_item_id' => $_POST['comanda_item_id'][$r],
                    'comanda_id' => $this->input->post('comanda_id'),
                    'producto_id' => $this->input->post('add_producto_id'),
                    'producto' => $oProducto->producto,
                    'comentario' => $this->input->post('add_comentario'),
                    'modalidad' => $oComanda->modalidad,
                    'para_cocina' => $oProducto->para_cocina,
                    'precio_unitario' => $oProducto->precio_unitario,
                    'cantidad' => $this->input->post('add_cantidad'),
                    'importe' => $oProducto->precio_unitario * $this->input->post('add_cantidad'),
                    'pedido_en' => date("Y/m/d H:i:s"),
                    // 'cocinero_id' => $_POST['cocinero_id'][$r],
                    // 'cocinero' => $_POST['cocinero'][$r],
                    'estado_atencion' => 'E',
                    'fecha_registro' => date("Y/m/d H:i:s"),
                    'registrado_por' => $this->session->userdata('session_usuario'),

                    'manejar_stock' => $oProducto->manejar_stock,
                );
                // Agrego el item a la comanda
                $rpta = $this->Comanda_model->agregar_producto_adicional($item);
                if(!$rpta){
                    throw new Exception("Problemas al agregar el producto.");
                }else{
                     // Actualizo la comanda
                    $this->actualizar_importe_comanda($oComanda->comanda_id);
                    // Actualizo el stock
                    $this->reducir_stock($item);
                    // Cambiar el estado de la mesa
                    $this->Mesa_model->cambiar_estado($oComanda->mesa_id, 'O');  //Ocupdo
                    $this->Comanda_model->cambiar_estado_atencion($oComanda->comanda_id, 'E'); //En espera
                    
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Se agregó el producto correctamente.");
                    // Para la impresión a cocina, estos datos se enviarán a la vista "EDITAR".
                    $this->session->set_userdata('enviar_adicional_cocina',true); //Para enviar el adicional a cocina
                    $this->session->set_userdata('comanda_item_id',$rpta);   //Codigo del producto adicional
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
   
// 
    function reiniciar_comanda(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $this->session->unset_userdata('oComanda');
        redirect($_SERVER["HTTP_REFERER"]);
    }
    function cargar_modal_clientes(){
        $data['clientes'] = $this->Cliente_model->get_all();
        $this->partial_view('salon/comanda/_modal_clientes_select',$data);
    }
    // function prefacturar(){
    //     try {
    //         // echo json_encode($this->input->post());exit();
    //         $this->Caja_model->verificar_caja_aperturada();
    //         $this->loaders->verifica_sesion();
    //         $data = array(  'comanda_id' => $this->input->post('comanda_id'),
    //                         'tipo_comprobante' => $this->input->post('tipo_comprobante'),
    //                         'cliente_documento' => $this->input->post('cliente_documento'),
    //                         'cliente_nombre' => $this->input->post('cliente_nombre'),
    //                         'cliente_direccion' => $this->input->post('cliente_direccion'),
    //                         'cliente_telefono' => $this->input->post('cliente_telefono'),
    //                         'prefacturada_en' => date("Y/m/d H:i:s"),
    //                         'cliente_id' => null,
    //                     );
    //         //Obtengo el cliente si hay DNI
    //         if ($data['cliente_documento'] != "") {
    //             if ( strlen(trim($data['cliente_documento'])) != 8 && strlen(trim($data['cliente_documento'])) != 11 ) {
    //                 throw new Exception("Documento del cliente no válido, debe tener 8 u 11 caracteres.");
    //             }
    //             if ( strlen(trim($data['cliente_nombre'])) < 2 ) {
    //                 throw new Exception("Debe ingresar los nombres del cliente.");
    //             }
    //             if(count($this->Cliente_model->get_by_doc($data['cliente_documento'])) == 0){
    //                 $data_cliente = array(  
    //                         'documento' => $this->input->post('cliente_documento'),
    //                         'nombre_completo' => $this->input->post('cliente_nombre'),
    //                         'telefono' => $this->input->post('cliente_direccion'),
    //                         'direccion' => $this->input->post('cliente_telefono'),
    //                         'fecha_registro' => date("Y/m/d H:i:s"),
    //                         'registrado_por' => $this->session->userdata('session_usuario'),
    //                     );
    //                 $data['cliente_id'] = $this->Cliente_model->insert($data_cliente);
    //             }else{
    //                 $oCliente = ($this->Cliente_model->get_by_doc($data['cliente_documento']))[0];
    //                 $data['cliente_id'] = $oCliente->cliente_id;
    //             }
    //         }
    //         $this->form_validation->set_rules('comanda_id', "Comanda", 'required');

    //         if ($this->form_validation->run() == true) {
    //             $oComanda = ($this->Comanda_model->get_one($data['comanda_id']))[0];
    //             $rpta = $this->Comanda_model->prefacturar($data);
    //             if(!$rpta){
    //                 $this->session->set_flashdata('success', false);
    //                 $this->session->set_flashdata('message', 'Problemas para prefacturar la comanda.');
    //                 redirect($_SERVER["HTTP_REFERER"]);
    //             }else{
    //                 $this->Mesa_model->cambiar_estado($oComanda->mesa_id, 'P');
    //                 $this->session->set_flashdata('success', true);
    //                 $this->session->set_flashdata('message', "Comanda prefacturada correctamente.");
    //                 redirect($_SERVER["HTTP_REFERER"]);
    //             }
    //         }else{
    //             $this->session->set_flashdata('success', false);
    //             $this->session->set_flashdata('message', 'Problemas al prefacturar la comanda.');
    //             redirect($_SERVER["HTTP_REFERER"]);
    //         }
    //         redirect($_SERVER["HTTP_REFERER"]);
    //     } catch (Exception $e) {
    //         $this->session->set_flashdata('success',false);
    //         $this->session->set_flashdata('message',$e->getMessage());
    //         // Guardar en un log
    //         $log_mensaje = $e->getMessage()." --> ".$e->getFile();
    //         log_message('error', $log_mensaje);
    //         redirect($_SERVER["HTTP_REFERER"]);
    //     }
    // }
    function finalizar_comanda(){
        try {
            $this->loaders->verifica_sesion();
            $comanda_id = $this->input->get('comanda_id');
            //Obtener datos de la comanda
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($comanda_id);
            //Verificar que esté pagada
            if ($oComanda->estado_pago == 'P') {
                throw new Exception("Debe facturar la comanda para finalizar.");
            }
            //Verificafr estado de los items
            foreach ($oComanda->items as $key => $item) {
                if($item->estado_atencion == 'E' || $item->estado_atencion == 'P'){
                    throw new Exception("Para finalizar la comanda, debe despachar todos los productos.");
                    break;
                }
            }
            //Verificar si se paga con tarjeta, se debe emitir comprobante electrónico
            // $oPago = ($this->Pago_model->get_one_by_comanda($oComanda->comanda_id))[0];
            // if($oPago->pago_tarjeta != 0){
            //     if($oComanda->con_comprobante_e != "C"){
            //         throw new Exception("La comanda fue pagada con tarjeta. Debe emitir comprobante electrónico.");
            //     }
            // }
            //Finalizar comanda
            $rpta = $this->Comanda_model->finalizar_comanda($comanda_id);
            if(!$rpta){
                throw new Exception("Problemas para finalizar la comanda.");
            }else{
                // Liberar mesa si la comanda fue presencial
                if($oComanda->modalidad == 'PRE'){
                    $this->Mesa_model->cambiar_estado($oComanda->mesa_id, 'L');
                }
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Comanda finalizada correctamente.");

                $this->set_session_enviar_comanda_nube($comanda_id);
                redirect(base_url('salon/salon'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    function anular_item($comanda_item_id){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        try {
            // Obtengo el item y el producto
            $item = ($this->Comanda_model->get_item_by_comanda($comanda_item_id))[0];
            $producto = ($this->Producto_model->get_one($item->producto_id))[0];
            $comanda = ($this->Comanda_model->get_one($item->comanda_id))[0];
            $item->manejar_stock = $producto->manejar_stock;
            // $this->aumentar_stock($item);
            if($comanda->estado_pago == 'C'){
                throw new Exception("No se puede anular el pedido, debido a que la comanda ya ah sido pagada.");
            }
            if($item->estado_atencion == 'P'){
                throw new Exception("No se puede anular el pedido, debido a que ya está en preparación.");
            }
            if($item->estado_atencion == 'D'){
                throw new Exception("No se puede anular el pedido, debido a que ya se ah despachado.");
            }
            if($item->estado_atencion == 'X'){
                throw new Exception("No se puede anular el pedido, debido a que ya está anulado.");
            }
            if( $this->Comanda_model->anular_item($comanda_item_id) ){
                //Actualizamos el importe total de la comanda
                $this->actualizar_importe_comanda($comanda->comanda_id);
                $this->aumentar_stock($item);

                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', 'Item anulado correctamente');
                redirect($_SERVER["HTTP_REFERER"]);
            }else{
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Problemas para anular el item.');
                redirect(base_url('salon/salon'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
        // echo $comanda_item_id;exit();
    }
    function actualizar_importe_comanda($comanda_id){
        $this->Caja_model->verificar_caja_aperturada();
        $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
        $oComanda->items = $this->Comanda_model->get_items_by_comanda($comanda_id);
        // echo json_encode($oComanda);exit();
        //Verificafr estado de los items
        $importe_total = 0; $nro_productos = 0;
        foreach ($oComanda->items as $key => $item) {
             $importe_total += $item->precio_unitario * $item->cantidad;
             $nro_productos += $item->cantidad;
        }
        $this->Comanda_model->actualizar_importe_comanda($comanda_id, $importe_total, $nro_productos);
    }
    // OPERACIONES A BASE DE DATOS
    function guardar(){ //Guardar comanda

        //obtener igv
        $igv = $this->obtener_igv();

        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        try {
            $oCaja = ($this->Caja_model->get_aperturada())[0];
            // Datos de la comanda
            $nueva_data = array(
                'comanda_id' => $this->input->post('comanda_id'),
                'fecha_comanda' => date("Y/m/d H:i:s"),
                'modalidad' => $this->input->post('modalidad'),
                'mozo' => $this->session->userdata('session_usuario'),
                'mesa_id' => $this->input->post('mesa_id'),
                'mesa' => $this->input->post('mesa'),
                'nro_comensales' => $this->input->post('nro_comensales'),
                'cmd_comentario' => $this->input->post('cmd_comentario'),
                'nro_productos' => 0,       //Asignar luego
                'tasa_IGV' => $igv,       //Asignar luego
                'total_IGV' => 0,       //Asignar luego
                'importe_sin_IGV' => 0,       //Asignar luego
                'importe_total' => 0,       //Asignar luego
                'caja_id' => $oCaja->caja_id,
                'cliente_id' => $this->input->post('cliente_id'),
                'cliente_tipo_documento' => $this->input->post('cliente_tipo_documento'),
                'cliente_documento' => $this->input->post('cliente_documento'),
                'cliente_nombre' => $this->input->post('cliente_nombre'),
                'cliente_telefono' => $this->input->post('cliente_telefono'),
                'cliente_direccion' => $this->input->post('cliente_direccion'),
                'cliente_correo' => $this->input->post('cliente_correo'),
                'estado_atencion' => 'E',
                'estado_pago' => 'P',
                'canal' => $this->input->post('canal'),

                'fecha_registro' => date("Y/m/d H:i:s"),
                'registrado_por' => $this->session->userdata('session_usuario'),
            );
            // ASIGNAMOS VALORES a la nueva comanda y la obtenemos de la ssession
            //VALIDAMOS el formulario
            $this->form_validation->set_rules('modalidad', "Modalidad", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            if($this->input->post('modalidad') == "PRE"){
                $this->form_validation->set_rules('mesa_id', "Mesa", 'required');
            }else{
                $nueva_data['mesa_id'] = null;
                $nueva_data['mesa'] = null;
            }

            //obtener igv
            $igv = $this->obtener_igv();
            $parametro_sin_igv = floatval(1 + floatval($igv));

            //Obtener correlativo por día de pedidos por modalidad
            $correlativo_por_dia = $this->get_correlativo_diario($nueva_data['modalidad'], $oCaja->caja_id);
            $nueva_data['correlativo'] = $correlativo_por_dia;

            //CONSULTAMOS LA MESA
            if (count($this->Mesa_model->get_one($this->input->post('mesa_id'))) > 0) {
                $mesa = ($this->Mesa_model->get_one($this->input->post('mesa_id')))[0];
                if($mesa->estado != 'L'){
                    throw new Exception("La mesa especificada no esta libre. Porfavor comuníquese con el área de soporte.");
                }
            }

            // CALCULAMOS el IMPORTE TOTAL y el nro de productos vendidos
            $importe_total = 0;
            $nro_productos = isset($_POST['producto_id']) ? sizeof($_POST['producto_id']) : 0; //Productos o insumos)
            if( $nro_productos == 0 ){
                throw new Exception("Debe seleccionar productos para la comanda.");
            }
            for ($r = 0; $r < $nro_productos; $r++) {
                $objProducto = ($this->Producto_model->get_one($_POST['producto_id'][$r]))[0];
                $item = array(
                    // 'comanda_item_id' => $_POST['comanda_item_id'][$r],
                    // 'comanda_id' => $_POST['comanda_id'][$r],
                    'producto_id' => $_POST['producto_id'][$r],
                    'producto' => $objProducto->producto,
                    'comentario' => $_POST['comentario'][$r],
                    'modalidad' => $this->input->post('modalidad'),
                    'para_cocina' => $objProducto->para_cocina,
                    'precio_unitario' => $objProducto->precio_unitario,
                    'cantidad' => $_POST['cantidad'][$r],
                    'importe' => $objProducto->precio_unitario * $_POST['cantidad'][$r],
                    'pedido_en' => date("Y/m/d H:i:s"),
                    // 'cocinero_id' => $_POST['cocinero_id'][$r],
                    // 'cocinero' => $_POST['cocinero'][$r],
                    'estado_atencion' => 'E',
                    'fecha_registro' => date("Y/m/d H:i:s"),
                    'registrado_por' => $this->session->userdata('session_usuario'),

                    'manejar_stock' => $objProducto->manejar_stock,
                );
                $items[] = $item;
                $importe_total += ($objProducto->precio_unitario * $_POST['cantidad'][$r]);
            }
            $nueva_data['nro_productos'] = $nro_productos;
            $nueva_data['importe_total'] = $importe_total;
            $nueva_data['importe_sin_IGV'] = $importe_total/$parametro_sin_igv;
            $nueva_data['total_IGV'] = $nueva_data['importe_total'] - $nueva_data['importe_sin_IGV'];

            //Verificamos cliente
            $error_cliente = $this->validar_datos_cliente($nueva_data);
            if($error_cliente != ""){
                throw new Exception($error_cliente);
            }

            //Registramos cliente o  si ya esta registrado capturamos el id
            if(trim($nueva_data['cliente_documento']) != ""){
                $cliente_id = $this->validar_registro_cliente($nueva_data);
                if($cliente_id > 0){
                    $nueva_data['cliente_id'] = $cliente_id;
                }
            }

            // INSERTAMOS
            if ($this->form_validation->run() == true) {
                if( $this->input->post('comanda_id') == 0 ){
                    //NUEVA COMANDA
                    $comanda_id = $this->Comanda_model->insert($nueva_data, $items);
                    if(!$comanda_id){
                        throw new Exception("Problemas para registrar la comanda.");
                    }else{
                        // Cambiar de estado a la mesa, si la modadlidad es presencial
                        if ($this->input->post('modalidad') == 'PRE') {
                            $this->Mesa_model->cambiar_estado($nueva_data['mesa_id'], 'O');
                        }
                        // Reducir el stock de productos y de insumos
                        foreach ($items as $key => $item) {
                            $this->reducir_stock($item);
                        }
                        // Dejar de recordar la variable ssession de la comanda
                        $this->session->unset_userdata('oComanda');
                        $this->session->set_flashdata('success', true);
                        $mensaje_correcto = "Comanda registrada correctamente.<br>Código de comanda:". $comanda_id;
                        $this->session->set_flashdata('message', $mensaje_correcto);
                        // Vriables sesion para imprimir directamente el ticket a cocina y limpiar localstorage
                        $this->session->set_userdata('remove_local_comanda',true);
                        $this->session->set_userdata('enviar_ticket_cocina',true);
                        $this->session->set_userdata('comanda_id_para_ticket',$comanda_id);

                        $this->set_session_enviar_comanda_nube($comanda_id);
                        redirect(base_url('salon/salon'));
                    }
                }else{
                    // EDITAR COMANDA
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // LOG
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    function reducir_stock($item){
        // echo json_encode($item);exit();
        $producto_id = $item['producto_id'];
        $cantidad = $item['cantidad'];//Numero de productos que se pretende vender

        if($item['manejar_stock'] == 1){
            $this->Producto_model->actualiza_stock($producto_id, (-1)*$cantidad);
        }else{
            //Vemos si tiene insumos
            $insumos = $this->Producto_model->get_insumos_by_producto($producto_id);
            foreach ($insumos as $key => $insumo) {
                $insumo_id = $insumo->insumo_id;
                // $cantidad_preparacion = $insumo->cantidad;
                $cantidad_gastada = $insumo->cantidad * $cantidad;
                $this->Insumo_model->actualiza_stock($insumo_id, (-1)*$cantidad_gastada);
            }
        }
    }
    function aumentar_stock($item){
        $producto_id = $item->producto_id;
        $cantidad = $item->cantidad;//Numero de productos que se pretende vender
        // echo json_encode($item->manejar_stock);exit();
        if($item->manejar_stock == 1){
            $this->Producto_model->actualiza_stock($producto_id, (1)*$cantidad);
        }else{
            //Vemos si tiene insumos
            $insumos = $this->Producto_model->get_insumos_by_producto($producto_id);
            foreach ($insumos as $key => $insumo) {
                $insumo_id = $insumo->insumo_id;
                // $cantidad_preparacion = $insumo->cantidad ;
                $cantidad_gastada = $insumo->cantidad * $cantidad;
                $this->Insumo_model->actualiza_stock($insumo_id, (1)*$cantidad_gastada);
            }
        }
    }
    function anular_comanda(){
        $this->Caja_model->verificar_caja_aperturada();
        try {
            $data = array(  'comanda_id' => $this->input->post('anular_comanda_id'),
                            'anulado_en' => date("d/m/Y H:i:s"),
                            'anulado_por' => $this->session->userdata('session_usuario'),
                            'anulado_motivo' => $this->input->post('anular_motivo'),
                            'estado_atencion' => 'X',
                            'estado_pago' => 'X',
                            );
            // Verificar que la comanda no este pagada
            $oComanda = ($this->Comanda_model->get_one($data['comanda_id']))[0];
            if ($oComanda->estado_pago == 'C') {//Cancelado, pagado
                throw new Exception("No se puede anular la comanda, debido a que ya está pagada.");
            }
            if ($oComanda->estado_pago == 'X') {//Cancelado, pagado
                throw new Exception("No se puede anular la comanda, debido a que ya está anulada.");
            }
            // Verificar el estado de los pedidos, que estén en espera
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($data['comanda_id']);
            foreach ($oComanda->items as $key => $item) {
                if ($item->estado_atencion == 'P' || $item->estado_atencion == 'D') {
                    throw new Exception("No se puede anular, debido a que hay pedidos que están en preparación y/o despachados. Debe anular primero dichos pedidos.");
                }
            }
            // echo json_encode($oComanda->items);exit();
            $rpta = $this->Comanda_model->anular_comanda($data);
            if(!$rpta){
                throw new Exception("Problemas para anular la comanda.");
            }else{
                // Cambiar de estado a la mesa, si la modadlidad es presencial
                if ($oComanda->modalidad == 'PRE') {
                    $this->Mesa_model->cambiar_estado($oComanda->mesa_id, 'L');
                }
                // $this->anular_item();
                // Aumentar el stock
                foreach ($oComanda->items as $key => $item) {
                    if ($item->estado_atencion != 'X') {
                        if( $this->Comanda_model->anular_item($item->comanda_item_id) ){
                            //Actualizamos el importe total de la comanda
                            $producto = ($this->Producto_model->get_one($item->producto_id))[0];
                            $item->manejar_stock = $producto->manejar_stock;
                            $this->aumentar_stock($item);
                        }
                    }
                }
                // Actualizamos el importe total de la comanda
                $this->actualizar_importe_comanda($oComanda->comanda_id);

                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Comanda anulada correctamente.");

                $this->set_session_enviar_comanda_nube($oComanda->comanda_id);

                redirect(base_url('salon/salon'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // LOG
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    function web_service_get_comanda($comanda_id){
        try {
            if(count( $this->Comanda_model->get_one($comanda_id)) == 0){
                throw new Exception("Error, no se encontró la comanda especificado, por favor comuníquese con el área de soporte.");
            }
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oComanda->items = $this->Comanda_model->get_items_by_comanda($oComanda->comanda_id);

            $data['oGeneralidades'] = ($this->Generalidades_model->get_one())[0];
            $data['oComanda'] = $oComanda;

            echo json_encode($data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }
    function web_service_item_adicional($comanda_item_id){
        try {
            if(count( $this->Comanda_model->get_item_by_comanda($comanda_item_id)) == 0){
                throw new Exception("Error, No se encontró el item adicional.");
            }
            $item = ($this->Comanda_model->get_item_by_comanda($comanda_item_id))[0];
            $oComanda = ($this->Comanda_model->get_one($item->comanda_id))[0];

            $data['oGeneralidades'] = ($this->Generalidades_model->get_one())[0];
            $data['oItem'] = $item;
            $data['oComanda'] = $oComanda;

            echo json_encode($data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('salon/salon'));
        }
    }
    function get_correlativo_diario($modalidad, $caja_id){
        $rpta = ($this->Comanda_model->get_correlativo_por_dia($modalidad, $caja_id))[0];
        return $rpta->correlativo;
    }
    function cliente_desde_comanda($comanda){
        //Crea objeto cliente, desde los datos de la comanda.
        $data['oCliente'] = new Cliente_model();
        $data['oCliente']->cliente_id = $comanda->cliente_id;
        $data['oCliente']->documento = $comanda->cliente_documento;
        $data['oCliente']->nombre_completo = $comanda->cliente_nombre;
        $data['oCliente']->direccion = $comanda->cliente_direccion;
        $data['oCliente']->telefono = $comanda->cliente_telefono;
        $data['oCliente']->correo = $comanda->cliente_correo;
        $data['oCliente']->sunat_tipo_documento_id = $comanda->cliente_tipo_documento;

        return $data['oCliente'];
    }

    function validar_datos_cliente($data_comanda){
        $tipo_documento = trim($data_comanda['cliente_tipo_documento']);
        $documento = trim($data_comanda['cliente_documento']);
        $nombres = trim($data_comanda['cliente_nombre']);
        $correo = trim($data_comanda['cliente_correo']);
        $mensaje = "";

        //Valida documento de identidad
        //Si se ingreso documento, deberá ingresarse tipo documento y nombre
        if($documento != ""){
            if($nombres == ""){
                $mensaje = $mensaje."Debe indicar nombre del cliente.<br>";
            }
            if($tipo_documento == ""){
                $mensaje = $mensaje."Debe indicar tipo documento de identidad del cliente.<br>";
            }
        }

        //Valida tipo documento
        if($tipo_documento != ""){
            if($tipo_documento == "1"){ //DNI
                if(strlen($documento) != 8){
                    $mensaje = $mensaje."El número de DNI debe tener 8 caracteres.<br>";
                }
            }

            if($tipo_documento == "6"){ //RUC
                if(strlen($documento) != 11){
                    $mensaje = $mensaje."El número de RUC debe tener 11 caracteres.<br>";
                }
            }
        }

        //Valida correo siempre y cuando se haya ingresado
        if($correo != ""){
            if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
                $mensaje = $mensaje."El correo indicado no es válido.<br>";
            }
        }

        //Mensaje final   
        if($mensaje != ""){
            $mensaje = "Error en datos del cliente: <br>".$mensaje;
        }
        
        return $mensaje;        
    }

    function validar_registro_cliente($data_comanda){

        //Validar datos
        if(trim($data_comanda['cliente_documento']) == ""){
            return -1;
        }
        if(trim($data_comanda['cliente_nombre']) == ""){
            return -1;
        }

        //Buscar cliente
        $clientes = $this->Cliente_model->get_by_doc($data_comanda['cliente_documento']);
        if(count($clientes) >= 1){
            $oCliente = $clientes[0];
            return $oCliente->cliente_id;
        }

        //Setear datos de cliente
        $data_cliente = array(  
                'documento' => $data_comanda['cliente_documento'],
                'nombre_completo' => $data_comanda['cliente_nombre'],
                'telefono' => $data_comanda['cliente_telefono'],
                'direccion' => $data_comanda['cliente_direccion'],
                'correo' => $data_comanda['cliente_correo'],
                'sunat_tipo_documento_id' => $data_comanda['cliente_tipo_documento'],
                'fecha_registro' => date("Y/m/d H:i:s"),
                'registrado_por' => $this->session->userdata('session_usuario'),
            );
        $cliente_id = $this->Cliente_model->insert($data_cliente);
        return $cliente_id;
    }

    function actualizar_comanda_client(){
         try {
            $data['comanda_id'] = $this->input->post('comanda_id');
            $data['cliente_tipo_documento'] = $this->input->post('cliente_tipo_documento');
            $data['cliente_documento'] = $this->input->post('cliente_documento');
            $data['cliente_nombre'] = $this->input->post('cliente_nombre');
            $data['cliente_direccion'] = $this->input->post('cliente_direccion');
            $data['cliente_telefono'] = $this->input->post('cliente_telefono');
            $data['cliente_correo'] = $this->input->post('cliente_correo');


            $rpta = $this->Comanda_model->actualizar_comanda_cliente($data);
            if($rpta){
                $response['success'] = true;
                $response['msg'] = $rpta;
                $response['data'] = "";
                echo json_encode($response); 
                exit();
            }

            $response['success'] = false;
            $response['msg'] = "Error en la actualización";
            $response['data'] = "";
            echo json_encode($response); 
            exit();
            
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }

    }

    function ajax_abrir_modal_agregar_comentario(){
        $producto_id = $this->input->post('producto_id');

        //Producto
        $buscarProducto = $this->Producto_model->get_one($producto_id);
        if(count($buscarProducto) > 0){
            $oProducto = $buscarProducto[0];
        }else{
            $oProducto = null;
        }

        //Familia
        $buscarFamilia = $this->Familia_model->get_one($oProducto->familia_id);
        if(count($buscarFamilia) > 0){
            $oFamilia = $buscarFamilia[0];
        }else{
            $oFamilia = null;
        }

        $lstComentarios = $this->Comentario_model->get_all_by_familia($oProducto->familia_id);

        $data['lstComentarios'] = $lstComentarios;
        $data['oProducto'] = $oProducto;
        $data['oFamilia'] = $oFamilia;
        $this->partial_view('salon/comanda/_modal_agregar_comentario', $data);
    }
}
