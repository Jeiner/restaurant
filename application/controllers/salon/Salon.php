<?php defined('BASEPATH') or exit('No direct script access allowed');

class Salon extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mesa_model');
        $this->load->model('Union_mesa_model');
        $this->load->model('Producto_model');
        $this->load->model('Comanda_model');
        $this->load->model('Caja_model');
        $this->load->model('Multitabla_model');
        $this->load->library('session');
        
        // $this->Caja_model->verificar_caja_aperturada();
    }
    function index(){
        $this->Caja_model->verificar_caja_aperturada();
        $data['oCaja'] = ($this->Caja_model->get_aperturada())[0];
        $data['mesas'] = $this->Mesa_model->get_all();
        // $data['productos'] = $this->Producto_model->get_all();
        $data['comandas'] = $this->Comanda_model->get_all_by_caja($data['oCaja']->caja_id);
        $data['nro_comandas'] = count($data['comandas']);
        $data['nro_finalizadas'] = 0;
        $data['nro_enespera'] = 0;
        $data['nro_atendidas'] = 0;
        foreach ($data['comandas'] as $key => $comd) {
            if ($comd->estado_atencion == 'F') $data['nro_finalizadas'] += 1; 
            if ($comd->estado_atencion == 'E') $data['nro_enespera'] += 1; 
            if ($comd->estado_atencion == 'A') $data['nro_atendidas'] += 1; 
        }
        $data['uniones'] = $this->Union_mesa_model->get_all_by_caja($data['oCaja']->caja_id);
        // echo var_dump($data['uniones']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Control de mesas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Salón - Control de mesas', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'salon' );
        $this->page_construct('salon/index', $meta, $data);
    }
    function consulta_totales(){ //Obtiene la cantidad totales de comandas
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        $comandas = $this->Comanda_model->get_all_by_caja($oCaja->caja_id);
        $data['nro_comandas'] = count($comandas);
        $data['nro_finalizadas'] = 0;
        $data['nro_enespera'] = 0;
        $data['nro_atendidas'] = 0;
        foreach ($comandas as $key => $comd) {
            if ($comd->estado_atencion == 'F') $data['nro_finalizadas'] += 1; 
            if ($comd->estado_atencion == 'E') $data['nro_enespera'] += 1; 
            if ($comd->estado_atencion == 'A') $data['nro_atendidas'] += 1; 
        }
        echo json_encode($data);
    }
    function historial_comandas(){
        $data['modalidades'] = $this->Multitabla_model->get_all_by_tipo(3);
        $data['canales'] = $this->Multitabla_model->get_all_by_tipo(40);

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Historial de comandas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Historial de comandas', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'historial_comandas' );
        $this->page_construct('salon/historial_comandas', $meta, $data);
    }

    function cargar_tabla_comandas_by_fechas(){
        // Listado de comandas desde una fecha de inicio hasta una fecha de fin
        //Año-Mes-Dia
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $filtro_modalidad = $this->input->post('filtro_modalidad');
        $filtro_canal = $this->input->post('filtro_canal');
        
        $data['comandas'] = $this->Comanda_model->get_all_by_fechas($start_date, $end_date, $filtro_modalidad, $filtro_canal);
        $this->partial_view('salon/comanda/_tabla_comandas_by_fechas',$data);
    }

    function ultimas_comandas(){
        $data['comandas'] = [];
        if (count($this->Caja_model->get_aperturada()) > 0) {
            $oCaja = ($this->Caja_model->get_aperturada())[0];
            $data['comandas'] = $this->Comanda_model->get_all_by_caja($oCaja->caja_id);
        }
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Salón' ),
                    array('link' => '#', 'page' => 'Últimas comandas' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Últimas comandas', 'bc' => $bc, 'modulo'=>'salon', 'item' => 'salon' );
        $this->page_construct('salon/ultimas_comandas', $meta, $data);
    }
    function exportar_comandas(){
        $this->loaders->verifica_sesion();
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        $filtro_modalidad = $this->input->get('filtro_modalidad');
        $filtro_canal = $this->input->get('filtro_canal');

        $comandas = $this->Comanda_model->get_all_by_fechas($start_date, $end_date, $filtro_modalidad, $filtro_canal);
        // echo json_encode($comandas);exit();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Historial de comandas");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Cód. comanda');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Modalidad');
        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Fecha');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'Mozo');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'Mesa');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'Importe total');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'Descuento');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'Total pagado');        
        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'Tipo comprobante');
        $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", 'Cliente');
        $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'Cód. Caja');
        $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'Estado atención');
        $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Estado pago');
        $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", 'Canal');
        foreach ($comandas as $key => $comanda) {
            $cont_excel++;
            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $comanda->comanda_id);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $comanda->modalidad_desc);
            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $comanda->fecha_comanda);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $comanda->mozo);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", $comanda->mesa);
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", number_format(round($comanda->importe_total,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", $comanda->descuento);
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", $comanda->total_pagado);
            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", $comanda->tipo_comprobante); 
            $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", $comanda->cliente_nombre);
            $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", $comanda->caja_id);
            $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", $comanda->estado_atencion_desc);
            $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", $comanda->estado_pago_desc);
            $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", $comanda->canal_desc);
        }
        $archivo_excel = "Historia_de_comandas."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo_excel Excel.
        // $objWriter->save('./files/'.$archivo_excel);
        $objWriter->save('php://output');

        // echo $end_date;exit();
        // $data['oComanda'] = ($this->Comanda_model->get_one($data['comanda_id']))[0];
        // $data['comandas'] = $this->Comanda_model->get_all_by_fechas($start_date, $end_date);
        // $this->partial_view('salon/comanda/_tabla_comandas_by_fechas',$data);
    }
    function unir_mesas(){
        try {
            $mesas_id = $this->input->post('mesas');
            $comanda_id = $this->input->post('comanda_id_para_unir');

            if (count($mesas_id) == 0) {
                throw new Exception("Error, Debe seleccionar por lo menos una mesa para unir.");
            }
            if (count($this->Comanda_model->get_one($comanda_id)) == 0) {
                throw new Exception("Error, No se encontró la comanda especificada.");
            }
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            if (count($this->Mesa_model->get_one($oComanda->mesa_id)) == 0) {
                throw new Exception("Error, No se encontró la mesa principal.");
            }
            $oMesaPrincipal = ($this->Mesa_model->get_one($oComanda->mesa_id))[0];
            if($oComanda->estado_atencion == 'F'){
                throw new Exception("No se puede unir la mesa, debido a que la comanda ya está finalizada.");
            }
            if($oComanda->estado_atencion == 'X'){
                throw new Exception("No se puede unir la mesa, debido a que la comanda está anulada.");
            }
            if($oComanda->estado_pago == 'C'){
                throw new Exception("No se puede unir la mesa, debido a que la comanda ya está pagada.");
            }
            if($oComanda->estado_pago == 'X'){
                throw new Exception("No se puede unir la mesa, debido a que la comanda está anulada.");
            }
            // echo var_dump($oComanda);exit();
            $nuevas_mesas = $oComanda->mesa;
            foreach ($mesas_id as $key => $mesa_id) {
                $oMesa = ($this->Mesa_model->get_one($mesa_id))[0];
                $nuevas_mesas .= ", ".$oMesa->mesa;
                $data = array(  
                    'comanda_id' => $comanda_id,
                    'mesa_principal_id' => $oComanda->mesa_id,
                    'mesa_principal' => $oComanda->mesa,
                    'mesa_unida_id' => $mesa_id,
                    'caja_id' => $oComanda->caja_id,
                    'estado' => $oMesaPrincipal->estado,
                );
                // inserto cada union
                $this->Union_mesa_model->insert($data);
                //modifico el estado de cada mesa unida 
                $this->Mesa_model->cambiar_estado($data['mesa_unida_id'], $oMesaPrincipal->estado);
            }
            // modificar algunos datos de la comanda(mesa y unida)
            $this->Comanda_model->actualizar_por_union($comanda_id,$nuevas_mesas);

            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','La unión de mesas se realizó correctamente.');
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
    function ajax_cargar_modal_unir_mesas(){
        // Para la union de meses
        $this->loaders->verifica_sesion_ajax();
        $comanda_id =  $_POST['comanda_id'];
        $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
        $data['listaMesas'] = $this->Mesa_model->get_all();
        $this->partial_view('salon/comanda/_modal_unir_mesas',$data);
    }
    function ajax_cargar_modal_editar_datos_generales_comanda(){
        // Para editar datos generales de una comanda
        $this->loaders->verifica_sesion_ajax();
        $comanda_id =  $_POST['comanda_id'];
        $data['modalidades'] = $this->Multitabla_model->get_all_by_tipo(3);
        $data['canales'] = $this->Multitabla_model->get_all_by_tipo(40);
        $data['listaMesas'] = $this->Mesa_model->get_all();
        $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
        $this->partial_view('salon/comanda/_modal_editar_datos_generales',$data);
    }
    // Carga el model que nos permitirá cambiar una mesa de la comanda
    public function ajax_cargar_modal_cambiar_mesa(){
        $this->loaders->verifica_sesion_ajax();
        $comanda_id =  $_POST['comanda_id'];
        $data['oComanda'] = ($this->Comanda_model->get_one($comanda_id))[0];
        $data['listaMesas'] = $this->Mesa_model->get_all();
        $data['listaUniones'] = $this->Union_mesa_model->get_all_by_comanda($comanda_id);
        $this->partial_view('salon/comanda/_modal_cambiar_mesa',$data);
    }
    //Cambia la mesa de la comanda
    public function cambiar_mesa(){
        try {
            $comanda_id =  $_POST['cambiar_mesa_comanda_id'];
            if(count($_POST['cambiar_mesa_actual']) != 1){
                throw new Exception("Debe seleccionar una mesa para reemplazar.");
            }
            if(count($_POST['cambiar_mesa_nueva']) != 1){
                throw new Exception("Debe seleccionar una nueva mesa.");
            }
            $mesa_actual_id =  ($_POST['cambiar_mesa_actual'])[0];
            $mesa_nueva_id =  ($_POST['cambiar_mesa_nueva'])[0];
            //Obtenemos la comanda y la mesa
            $oComanda = ($this->Comanda_model->get_one($comanda_id))[0];
            $oMesaActual = $this->Mesa_model->get_one($mesa_actual_id);
            $oMesaNueva = $this->Mesa_model->get_one($mesa_nueva_id);
            if (count($oMesaActual) == 0) {
                throw new Exception("Error, No se encontró la mesa.");
            }
             if (count($oMesaNueva) == 0) {
                throw new Exception("Error, No se encontró la mesa.");
            }
            $oMesaActual = $oMesaActual[0];
            $oMesaNueva = $oMesaNueva[0];
            //Validaciones de comanda
            switch ($oComanda->estado_atencion) {
                case "F": 
                    throw new Exception("No se puede modificar la comanda, debido a que ya está finalizada.");
                    break;
                case "X":
                    throw new Exception("No se puede modificar la comanda, debido a que está anulada.");
                    break;
            }
            switch ($oComanda->estado_pago) {
                case "C": 
                    throw new Exception("No se puede modificar la mesa, debido a que la comanda ya está pagada.");
                    break;
                case "X":
                    throw new Exception("No se puede modificar la mesa, debido a que la comanda está anulada.");
                    break;
            }
            //Validaciones de nueva mesa
            if($oMesaNueva->estado != 'L'){
                $mensaje = "La mesa seleccionada ".$oMesaNueva->mesa." no está libre. Debe seleccionar otra mesa.";
                throw new Exception($mensaje);
            }
            if($oComanda->union_mesas == 0){
                $nueva_data_comanda['mesa_id'] = $oMesaNueva->mesa_id;
                $nueva_data_comanda['mesa'] = $oMesaNueva->mesa;
            }else{
                $nueva_data_comanda['mesa_id'] = $oComanda->mesa_id;//Si no cambia mesa principal, se mantiene este id.
                $nueva_data_comanda['mesa'] = "";
                //Obtener las uniones
                $uniones = $this->Union_mesa_model->get_all_by_comanda($comanda_id);
                foreach ($uniones as $key => $union){
                    //Si era la principal
                    if($oMesaActual->mesa_id == $union->mesa_principal_id){
                        $nueva_data_comanda['mesa_id'] = $oMesaNueva->mesa_id;
                        $nueva_data_union['mesa_principal_id'] = $oMesaNueva->mesa_id;
                        $nueva_data_union['mesa_principal'] = $oMesaNueva->mesa;
                        $this->Union_mesa_model->update($union->union_id, $nueva_data_union);
                    }else{
                        //Si No fue la principal,
                        if($oMesaActual->mesa_id == $union->mesa_unida_id){
                            $nueva_data_union['mesa_unida_id'] = $oMesaNueva->mesa_id;
                            $this->Union_mesa_model->update($union->union_id, $nueva_data_union);
                        }
                    }
                }
                //Obtenemos los nombres de las nuevas mesas
                $nuevas_uniones = $this->Union_mesa_model->get_all_by_comanda($comanda_id);
                foreach ($nuevas_uniones as $key2 => $nueva_union){
                    if($key2 == 0){
                        $nueva_data_comanda['mesa'] .= $nueva_union->mesa_principal;
                    }
                    $oMesa = ($this->Mesa_model->get_one($nueva_union->mesa_unida_id))[0];
                    $nueva_data_comanda['mesa'] .= ', '.$oMesa->mesa;
                }
            }
            //Actualizamos la comanda
            $this->Comanda_model->actualizar_comanda($comanda_id,$nueva_data_comanda );
            //Cambiar estado de las mesas actual y nueva
            $this->Mesa_model->cambiar_estado($oMesaNueva->mesa_id, $oMesaActual->estado );
            $this->Mesa_model->cambiar_estado($oMesaActual->mesa_id, 'L');
            
            $this->session->set_flashdata('success',true);
            $this->session->set_flashdata('message','La actualización de mesas se realizó correctamente.');
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}






