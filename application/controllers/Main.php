<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('Acceso_model');  
        $this->load->model('Estadisticas_model');
        $this->load->model('Caja_model');
        $this->load->model('Generalidades_model');
        $this->load->library('session');
        // $this->load->model('Servicio_model');
    }
	public function index(){
        // Pagina por defecto
		if (!$this->session->userdata('session_usuario')){
			//Si no existe la session
            $data['oGeneralidades'] = ($this->Generalidades_model->get_one())[0];
            $data['page_title'] = 'Iniciar sesión';
    		$this->load->view('seguridad/acceso/login',$data);
    	}
    	else{
            if (strtoupper($this->session->userdata('session_rol_usuario')) == 'ADMINISTRADOR'){
            	redirect(base_url('main/panel'));
            }
            if (strtoupper($this->session->userdata('session_rol_usuario')) == 'COCINERO'){
            	redirect(base_url('cocina/cocina'));
            }
            redirect(base_url('main/panel'));
    	}
	}
	public function panel(){
        if(count($this->Caja_model->ultima_caja()) > 0){
            $data['oCaja'] = ($this->Caja_model->ultima_caja())[0];    
        }else{
            $data['oCaja'] = null;
        }
        
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Panel de control', 'bc' => $bc, 'modulo'=>'', 'item' => '' );
        $this->page_construct('general/panel', $meta, $data);
	}
    public function obtener_estadisticas(){
        // estadísticas para mostrar en el menu principal
        // if(count())
        if(count($this->Caja_model->ultima_caja()) == 0){
            $data['success'] = false;
            $data['mensaje'] = "No se puede mostrar estadísticas, debido a que no hay datos.";
            echo json_encode($data);
            exit();
        }
        $data['oCaja'] = ($this->Caja_model->ultima_caja())[0];
        if (count($this->Estadisticas_model->get_comandas_by_caja($data['oCaja']->caja_id)) == 0) {
            $data['success'] = false;
            $data['mensaje'] = "No se puede mostrar estadísticas, debido a que aún no se han realizado ventas.";
            
        }else{
            $data['success'] = true;
            $totales = ($this->Estadisticas_model->totales($data['oCaja']->caja_id))[0];
            $prom_x_mesa = ($this->Estadisticas_model->comsumo_promedio_x_mesa($data['oCaja']->caja_id))[0];
            $mozo_del_dia = ($this->Estadisticas_model->mozo_del_dia($data['oCaja']->caja_id))[0];
            $tiempos = ($this->Estadisticas_model->tiempo_promedio_atencion($data['oCaja']->caja_id))[0];
            $pagos_x_tipo = ($this->Estadisticas_model->pagos_x_tipo($data['oCaja']->caja_id))[0];
            $atenciones = ($this->Estadisticas_model->atenciones($data['oCaja']->caja_id))[0];
            
            
            $data['total_ventas'] = ($totales->total_ventas != null) ? $totales->total_ventas : "0.00";
            $data['total_descuentos'] = ($totales->total_descuentos != null) ? $totales->total_descuentos : "0.00";
            $data['pago_efectivo'] = ($pagos_x_tipo->pago_efectivo != null) ? $pagos_x_tipo->pago_efectivo : "0.00";
            $data['pago_tarjeta'] = ($pagos_x_tipo->pago_tarjeta != null) ? $pagos_x_tipo->pago_tarjeta : "0.00";
            $data['total_pagado'] = ($pagos_x_tipo->total_pagado != null) ? $pagos_x_tipo->total_pagado : "0.00";
            $data['atenciones_pre'] = ($atenciones->atenciones_pre != null) ? $atenciones->atenciones_pre : "0";
            $data['atenciones_del'] = ($atenciones->atenciones_del != null) ? $atenciones->atenciones_del : "0";
            $data['atenciones_lle'] = ($atenciones->atenciones_lle != null) ? $atenciones->atenciones_lle : "0";
            $data['atenciones_anu'] = ($atenciones->atenciones_anu != null) ? $atenciones->atenciones_anu : "0";
            
            $data['promedio_x_mesa'] = ($prom_x_mesa->promedio_x_mesa != null) ? number_format(round($prom_x_mesa->promedio_x_mesa,2), 2, '.', ' '): "0.00"; 
            $data['nro_comandas_x_mesa'] = ($prom_x_mesa->nro_comandas != null) ? $prom_x_mesa->nro_comandas : "0.00"; 
            $data['mozo_del_dia'] = ($mozo_del_dia->nombre_completo != null) ? substr($mozo_del_dia->nombre_completo,0,18).'...' : ""; 
            $data['nro_comandas_de_mozo'] = ($mozo_del_dia->nro_comandas != null) ? $mozo_del_dia->nro_comandas : ""; 
            $data['tiempo_min_atencion'] = ($tiempos->tiempo_min_atencion != null) ? number_format(round($tiempos->tiempo_min_atencion,2), 1, '.', ' '): "0.00"; 
            $data['tiempo_max_atencion'] = ($tiempos->tiempo_max_atencion != null) ? number_format(round($tiempos->tiempo_max_atencion,2), 1, '.', ' '): "0.00"; 
        }
        echo json_encode($data);
    }
	function pruebas(){
        $this->load->view('general/pruebas',null);
    }
}
