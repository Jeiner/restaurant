<?php defined('BASEPATH') or exit('No direct script access allowed');

class Ingresos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Ingreso_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Insumo_model');
        $this->load->model('Producto_model');
        $this->load->model('Ingreso_item_model');
        $this->load->model('Proveedor_model');
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_ingreso(){
        if($this->session->userdata('oIngreso')){
            return $this->session->userdata('oIngreso');
        }else{
            $oIngreso = new Ingreso_model();
            $oIngreso->fecha_ingreso = date('Y-m-d');

            $this->session->set_userdata('oIngreso', $oIngreso);
            return $this->session->userdata('oIngreso');
        }
    }
    function historial(){
        // $data['ingresos'] = $this->Ingreso_model->get_all_by_caja(1);
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Inventario' ),
                    array('link' => '#', 'page' => 'Historial de ingresos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Historial de ingresos', 'bc' => $bc, 'modulo'=>'inventario', 'item' => 'historial_ingresos' );
        $this->page_construct('inventario/ingresos/historial', $meta, null);
    }
    function historial_detalle(){
         $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Inventario' ),
                    array('link' => '#', 'page' => 'Historial de detalle de ingresos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Historial de detalle de ingresos', 'bc' => $bc, 'modulo'=>'inventario', 'item' => 'historial_ingresos' );
        $this->page_construct('inventario/ingresos/historial_detalle', $meta, null);
    }
    function cargar_tabla_ingresos_detalle_by_fechas(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data['detalle'] = $this->Ingreso_model->get_all_items_by_fechas($start_date, $end_date);
        $this->partial_view('inventario/ingresos/_tabla_ingresos_detalle_by_fechas',$data);
    }
    function cargar_tabla_ingresos_by_fechas(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data['ingresos'] = $this->Ingreso_model->get_all_by_fechas($start_date, $end_date);
        $this->partial_view('inventario/ingresos/_tabla_ingresos_by_fechas',$data);
    }
    function nuevo(){
        $data['oIngreso'] = $this->preparar_obj_ingreso();
        $data['productos'] = $this->Ingreso_model->get_view_productos_insumos();
        $data['motivos_ingreso'] = $this->Multitabla_model->get_all_by_tipo(6); //
        $data['tipos_comprobante'] = $this->Multitabla_model->get_all_by_tipo(7); //
        $data['proveedores'] = $this->Proveedor_model->get_all(); //

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Inventario' ),
                    array('link' => '#', 'page' => 'Ingreso de productos e insumos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Inventario', 'bc' => $bc, 'modulo'=>'inventario', 'item' => 'nuevo_ingreso' );
        $this->page_construct('inventario/ingresos/nuevo', $meta, $data);
    }
    function editar($ingreso_id){
        $oIngreso = $this->Ingreso_model->get_one($ingreso_id);
        $data['oIngreso'] = $oIngreso[0];
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Gestionar ingresos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Editar ingreso', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'ingresos' );
        $this->page_construct('inventario/ingresos/editar', $meta, $data);
    }
    function ver($ingreso_id){
        $data['oIngreso'] = ($this->Ingreso_model->get_one($ingreso_id))[0];
        $data['oIngreso']->items = $this->Ingreso_model->get_items_by_ingreso($ingreso_id);
        // echo json_encode($data['oIngreso']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Administración' ),
                    array('link' => '#', 'page' => 'Ver ingreso' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Ver ingreso', 'bc' => $bc, 'modulo'=>'inventario', 'item' => 'ingresos' );
        $this->page_construct('inventario/ingresos/ver', $meta, $data);
    }

    function get_one_ajax(){
        $this->loaders->verifica_sesion_ajax();
        $ingreso_id = $this->input->post('ingreso_id');
        $oIngreso = $this->Ingreso_model->get_one($ingreso_id);
        echo json_encode($oIngreso);
    }
    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->loaders->verifica_sesion();
        try {
            $this->form_validation->set_rules('motivo', "Motivo", 'required');
            $this->form_validation->set_rules('importe_total', "Importe total", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

            $data = array(  
                        'ingreso_id' => $this->input->post('ingreso_id'),
                        'fecha_ingreso' => date("Y/m/d H:i:s"),
                        'motivo' => $this->input->post('motivo'),
                        'usuario' => $this->session->userdata('session_usuario'),
                        'nro_productos' => 0,
                        'tasa_IGV' => $this->input->post('tasa_IGV'),
                        'importe_sin_IGV' => $this->input->post('importe_sin_IGV'),
                        'total_IGV' => $this->input->post('total_IGV'),
                        'importe_total' => $this->input->post('importe_total'),
                        'proveedor_id' => $this->input->post('proveedor_id'),
                        'tipo_comprobante' => $this->input->post('tipo_comprobante'),
                        'nro_comprobante' => $this->input->post('nro_comprobante'),
                        'estado_pago' => 'C',
                        'observaciones' => $this->input->post('observaciones'),
                        );
            // CALCULAMOS el IMPORTE TOTAL y el nro de productos vendidos
            $importe_total = 0;
            $nro_productos = isset($_POST['producto_id']) ? sizeof($_POST['producto_id']) : 0; //Productos o insumos)
            for ($r = 0; $r < $nro_productos; $r++) {
                    $tipo = ($_POST['producto_id'][$r] == '') ?  "Insumo" : "Producto";
                    $item = array(
                        'tipo' => $tipo,
                        'producto_id' => $_POST['producto_id'][$r],
                        'insumo_id' => $_POST['insumo_id'][$r],
                        'producto_insumo' => $_POST['producto_insumo'][$r],
                        'unidad_desc' => $_POST['unidad_desc'][$r],
                        'cantidad' => $_POST['cantidad'][$r],
                        'precio_costo' => $_POST['precio_costo'][$r],
                        'importe' => $_POST['importe'][$r],
                        'fecha_vencimiento' => $_POST['fecha_vencimiento'][$r],
                        'estado' => 'A',
                        'fecha_registro' => date("Y/m/d H:i:s"),
                        'registrado_por' => $this->session->userdata('session_usuario'),
                    );
                    $items[] = $item;
                    $importe_total += $_POST['importe'][$r];
            }
            $data['nro_productos'] = $nro_productos;
            $data['importe_total'] = $importe_total;
            
            if ($this->form_validation->run() == true) {
                if( $this->input->post('ingreso_id') == 0 ){
                    // insertar ingreso y los items
                    $ingreso_id = $this->Ingreso_model->insert($data, $items);
                    // Aumentar el stock de los items
                    foreach ($items as $key => $item) {
                        $this->aumentar_stock($item);
                    }
                    if(!$ingreso_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar el ingreso.');
                    }else{
                        $mensaje = "Ingreso registrado correctamente. Código: ".$ingreso_id;
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', $mensaje);
                        $this->session->set_userdata('remove_local_ingreso',true);
                        redirect(base_url('inventario/ingresos/historial'));
                    }
                }else{
                    // $rpta = $this->Ingreso_model->update($data);
                    // if(!$rpta){
                    //     $this->session->set_flashdata('success', false);
                    //     $this->session->set_flashdata('message', 'Problemas para actualizar el ingreso.');
                    // }else{
                    //     $this->session->set_flashdata('success', true);
                    //     $this->session->set_flashdata('message', "Cliente actualizado correctamente.");
                    // }
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('inventario/ingresos/historial'));
        }
    }

    function exportar_ingresos(){
        $this->loaders->verifica_sesion();
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $ingresos = $this->Ingreso_model->get_all_by_fechas($start_date, $end_date);
        // echo json_encode($ingresos);exit();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Historial de ingresos");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Cód.');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Fecha');
        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Motivo');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'usuario');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'nro_productos');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'tasa IGV');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'Importe sin IGv');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'Total IGV');        
        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'Importe total');
        $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", 'Proveedor');
        $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'Tipo comprobante');
        $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'Nro comprobante');
        $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Observaciones');
        // $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Observaciones');
        foreach ($ingresos as $key => $ingreso) {
            $cont_excel++;
            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $ingreso->ingreso_id);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $ingreso->fecha_ingreso);
            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $ingreso->motivo_desc);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $ingreso->usuario);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", $ingreso->nro_productos);
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", number_format(round($ingreso->tasa_IGV,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", number_format(round($ingreso->importe_sin_IGV,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", number_format(round($ingreso->total_IGV,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", number_format(round($ingreso->importe_total,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", $ingreso->proveedor);
            $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", $ingreso->tipo_comprobante_desc);
            $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", $ingreso->nro_comprobante);
            $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", $ingreso->observaciones);
        }
        $archivo_excel = "Historia_de_ingresos."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo_excel Excel.
        // $objWriter->save('./files/'.$archivo_excel);
        $objWriter->save('php://output');
    }
    function exportar_ventas_detalle(){
        $this->loaders->verifica_sesion();
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $items = $this->Ingreso_model->get_all_items_by_fechas($start_date, $end_date);
        // echo json_encode($items);exit();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Ingresos de productos");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Cód.');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Fecha');
        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Tipo');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'Producto/Insumo');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'Precio costo');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'Cantidad');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'Importe');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'Fecha vencimiento');        
        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'Estado');
        foreach ($items as $key => $item) {
            $cont_excel++;
            if ($item->insumo_id == '0') $tipo = 'Producto';
            else $tipo = 'Insumo';
            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $item->ingreso_detalle_id);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $item->fecha_registro);
            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $tipo);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $item->producto_insumo);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", number_format(round($item->precio_costo,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", $item->cantidad);
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", number_format(round($item->importe,2), 2, '.', ' '));
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", ($item->fecha_vencimiento == '0000-00-00') ? '' : $item->fecha_vencimiento );
            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", $item->estado);
        }
        $archivo_excel = "ingreso_productos."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo_excel Excel.
        // $objWriter->save('./files/'.$archivo_excel);
        $objWriter->save('php://output');
    }
    // stock
    function aumentar_stock($item){
        $producto_id = $item['producto_id'];
        $insumo_id = $item['insumo_id'];
        $cantidad = $item['cantidad'];  //Numero de productos que se pretende vender
        if($producto_id != "" ){
            if ($producto_id > 0) {
                $this->Producto_model->actualiza_stock($producto_id, (1)*$cantidad);
            }
        }
        if ($insumo_id != "") {
            if  ($insumo_id > 0) {
                echo $insumo_id.' - '.$can;
                $this->Insumo_model->actualiza_stock($insumo_id, (1)*$cantidad);
            }
        }
    }
}
