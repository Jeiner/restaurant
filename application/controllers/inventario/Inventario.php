<?php defined('BASEPATH') or exit('No direct script access allowed');

class Inventario extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Ingreso_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Insumo_model');
        $this->load->model('Producto_model');
        $this->load->model('Ingreso_item_model');
        $this->load->model('Proveedor_model');
        
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function productos_por_agotar(){
        $data['productos_por_agotar'] = $this->Producto_model->productos_por_agotar();
        // echo json_encode($productos_por_agotar);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Inventario' ),
                    array('link' => '#', 'page' => 'Productos por agotar' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Productos por agotar', 'bc' => $bc, 'modulo'=>'inventario', 'item' => 'productos_por_gotar' );
        $this->page_construct('inventario/productos_por_agotar', $meta, $data);
    }
    function insumos_por_agotar(){
        $data['insumos_por_agotar'] = $this->Producto_model->insumos_por_agotar();
        // echo json_encode($data['insumos_por_agotar']);exit();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Inventario' ),
                    array('link' => '#', 'page' => 'Insumos por agotar' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Insumos por agotar', 'bc' => $bc, 'modulo'=>'inventario', 'item' => 'productos_por_gotar' );
        $this->page_construct('inventario/insumos_por_agotar', $meta, $data);
    }
    // function editar($ingreso_id){
    //     $oIngreso = $this->Ingreso_model->get_one($ingreso_id);
    //     $data['oIngreso'] = $oIngreso[0];
    //     $bc = array(
    //                 array('link' => base_url(), 'page' => 'Inicio'),
    //                 array('link' => '#', 'page' => 'Administración' ),
    //                 array('link' => '#', 'page' => 'Gestionar ingresos' ),
    //             ); //breadcrumbs 
    //     $meta = array( 'page_title' => 'Editar ingreso', 'bc' => $bc, 'modulo'=>'administracion', 'item' => 'ingresos' );
    //     $this->page_construct('inventario/ingresos/editar', $meta, $data);
    // }
}
