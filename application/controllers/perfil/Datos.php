<?php defined('BASEPATH') or exit('No direct script access allowed');

class Datos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
        $this->load->model('Marcacion_model');
        $this->load->model('Usuario_model');
        $this->load->model('nube/Nube_sucursal_model');
        $this->load->model('Multitabla_model');
        $this->load->model('Empleado_model');
        $this->load->model('Cargo_model');
        $this->load->model('Site');
        $this->load->library('session');
    }

    function index(){

        $empleado_id = $this->session->userdata('session_empleado_id');

        //Buscar empleado
        $buscarEmpleado = $this->Empleado_model->get_one($empleado_id);
        if (count($buscarEmpleado) == 0 ) {
            $message = "No se encontró datos del empleado: ".$empleado_id;
            throw new Exception($message);
        }

        $data['oEmpleado'] = $buscarEmpleado[0];
        $data['oUsuario'] = $this->Usuario_model->get_by_empleado_id($empleado_id);
        $data['lstSucursales'] = $this->Nube_sucursal_model->get_all_activos();
        $data['cargos'] = $this->Cargo_model->get_all();
        $data['tipos_contratos'] = $this->Multitabla_model->get_all_by_tipo(10);
        $data['bancos'] = $this->Multitabla_model->get_all_by_tipo(11);
        $data['lstRoles'] = $this->Usuario_model->get_all_roles();

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Perfil' ),
                    array('link' => '#', 'page' => 'Datos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Mis datos', 'bc' => $bc, 'modulo'=>'perfil', 'item' => 'mis_datos' );
        $this->page_construct('perfil/datos/index', $meta, $data);
    }
}