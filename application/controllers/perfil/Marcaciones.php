<?php defined('BASEPATH') or exit('No direct script access allowed');

class Marcaciones extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Validaciones_model');
        $this->load->model('Marcacion_model');
        $this->load->model('Usuario_model');
        $this->load->model('Empleado_model');
        $this->load->model('Site');
        $this->load->library('session');
    }

    function listar(){

        //Datos de mes
        $mes   = (!isset($_REQUEST['mes'])) ? ""  : trim($_REQUEST['mes']);
        $anio   = (!isset($_REQUEST['anio'])) ? ""  : trim($_REQUEST['anio']);
        $filtro_mes = ($mes == "") ? date('m') : $mes;
        $filtro_anio = ($anio == "") ? date('Y') : $anio;
        $dFechaInicio = date('Y-m-d', strtotime($filtro_anio.'-'.$filtro_mes.'-'.'01'));

        $empleado_id = $this->session->userdata('session_empleado_id');

        //Data
        $data['lstMarcaciones'] = $this->Marcacion_model->get_marcaciones_mensuales_by_empleado_id($empleado_id, $filtro_anio, $filtro_mes);
        $data['dFechaInicio'] = $dFechaInicio;

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Perfil' ),
                    array('link' => '#', 'page' => 'Marcacion' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Mis marcaciones', 'bc' => $bc, 'modulo'=>'perfil', 'item' => 'mis_marcaciones' );
        $this->page_construct('perfil/marcaciones/listar', $meta, $data);
    }

    function nueva(){

        $empleado_id = $this->session->userdata('session_empleado_id');

        //Buscar empleado
        $buscarEmpleado = $this->Empleado_model->get_one($empleado_id);
        if (count($buscarEmpleado) == 0) {
            $message = "No se encontró el empleado para editar: ".$empleado_id;
            throw new Exception($message);
        }

        //Data
        $data['oEmpleado'] = $buscarEmpleado[0];

        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Perfil' ),
                    array('link' => '#', 'page' => 'Marcacion' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Registrar marcacion', 'bc' => $bc, 'modulo'=>'perfil', 'item' => 'nueva_marcacion' );
        $this->page_construct('perfil/marcaciones/nueva', $meta, $data);
    }

    function guardar_marcacion(){
        $dataMarcacion = array(  
                    'empleado_id' => $this->input->post('txt_empleado_id'),
                    'nombre' => $this->input->post('txt_nombres'),
                    'fecha' => $this->input->post('txt_fecha'),
                    'hora' => $this->input->post('txt_hora'),
                    'foto' => $this->input->post('txt_foto'),
                    'carga' => date('Y-m-d H:i:s')
                );

        $rpta = $this->Marcacion_model->insert($dataMarcacion);

        if(!$rpta){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'Problemas para registrar la marcacion.');
        }else{
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', "Marcacion registrada correctamente.");
        }
        redirect(base_url('perfil/marcaciones/listar'));
    }
}