<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pide extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    function sunat($RUC = ""){
    	$ruta_consulta = 'http://localhost:50/sunat/?documento='.$RUC;
        $data = array('accion' => '');
        $curl = curl_init($ruta_consulta);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_COOKIEJAR, $cookies);
        // curl_setopt($curl, CURLOPT_COOKIEFILE, $cookies);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.12) Gecko/2009070611 Firefox/3.0.12");
        $resultado = curl_exec($curl);
        $res_json = json_decode($resultado);
        // curl_close($curl);
        if( $res_json->success ){
            $response['success'] = true;
            $response['data'] = $res_json->result;
            $response['msg'] = "";
        }else{
            $response['success'] = false;
            $response['msg'] = $res_json->msg;
        }
        echo json_encode($response);
        // echo $res_json->resultado->APPAT;        
    }
    function reniec($DNI){
    	// $ruta_consulta = 'http://sst.onflyingcloud.com/webservice/reniec/get/'.$DNI;
        $ruta_consulta = 'http://regionlalibertad.gob.pe/pide/reniec/consultadni/'.$DNI;
        
        $data = array('accion' => '');
        $curl = curl_init($ruta_consulta);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_COOKIEJAR, $cookies);
        // curl_setopt($curl, CURLOPT_COOKIEFILE, $cookies);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.12) Gecko/2009070611 Firefox/3.0.12");
        $resultado = curl_exec($curl);
        $res_json = json_decode($resultado);
        if($res_json == null){
            $response['success'] = false;
            $response['msg'] = "No se ha encontrado resultados.";
            echo json_encode($response); exit();
        }
        if($res_json->status == 1){
            $response['success'] = true;
            $response['data'] = $res_json->resultado;
            $response['msg'] = "";
        }else{
            $response['success'] = false;
            $response['msg'] = $res_json->message;
        }
        // curl_close($curl);
        echo json_encode($response);
        // echo $res_json->resultado->APPAT;     
    }
}
