<?php defined('BASEPATH') or exit('No direct script access allowed');

class Caja extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Caja_model');
        $this->load->model('Comanda_model');
        $this->load->model('Pago_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
    }
    function preparar_obj_caja(){
        if($this->session->userdata('oCaja')){
            return $this->session->userdata('oCaja');
        }else{
            $oCaja = new Caja_model();
            $this->session->set_userdata('oCaja', $oCaja);
            return $this->session->userdata('oCaja');
        }
    }
    function set_datos_obj_caja($data){
        $oCaja =  $this->preparar_obj_caja();

        $oCaja->estado = $data['estado'];

        $this->session->set_userdata('oCaja', $oCaja);
    }
    function apertura_cierre(){
        $this->session->unset_userdata('oCaja');
        if(count($this->Caja_model->get_aperturada()) == 0){
            $data['oCaja'] = false;
        }else{
            $data['oCaja'] = ($this->Caja_model->get_aperturada())[0];
            $data['movimientos_caja'] = $this->Caja_model->movimientos_caja($data['oCaja']->caja_id);
            $data['movimientos_tarjeta'] = $this->Caja_model->movimientos_tarjeta($data['oCaja']->caja_id);
        }
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Caja' ),
                    array('link' => '#', 'page' => 'Apertura - Cierre' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Apertura - Cierre de caja', 'bc' => $bc, 'modulo'=>'caja', 'item' => 'apertura_cierre' );
        $this->page_construct('caja/caja/apertura_cierre', $meta, $data);
    }
    function historial_cierres(){
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'caja' ),
                    array('link' => '#', 'page' => 'Historial de cierres de caja' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Historial de cierres de caja', 'bc' => $bc, 'modulo'=>'caja', 'item' => 'historial_cierres' );
        $this->page_construct('caja/caja/historial_cierres', $meta, null);
    }
    // AJAX
    function cargar_modal_apertura(){
        $data['aperturado_por'] = "Juan Haro";
        $data['aperturado_en'] = date("d/m/Y H:i:s"); 
        $this->partial_view('caja/caja/_modal_apertura',$data);
    }
    function cargar_tabla_cierres_by_fechas(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        // $data['oComanda'] = ($this->Comanda_model->get_one($data['comanda_id']))[0];
        $data['cierres'] = $this->Caja_model->get_all_by_fechas($start_date, $end_date);
        // echo json_encode($data['cierres']);exit();
        $this->partial_view('caja/caja/_tabla_cierres_by_fechas',$data);
    }
    // function cargar_tablas_detalle(){
    //     $caja_id = $this->input->post('caja_id');
    //     // echo $caja_id;exit();
    //     $data['oCaja'] = ($this->Caja_model->get_one($caja_id))[0];
    //     $data['movimientos_caja'] = $this->Caja_model->movimientos_caja($data['oCaja']->caja_id);
    //     $data['movimientos_tarjeta'] = $this->Caja_model->movimientos_tarjeta($data['oCaja']->caja_id);
    //     $this->partial_view('caja/caja/_tablas_detalle',$data);
    // }
    function detalle($caja_id){
        $data['oCaja'] = ($this->Caja_model->get_one($caja_id))[0];
        $data['movimientos_caja'] = $this->Caja_model->movimientos_caja($data['oCaja']->caja_id);
        $data['movimientos_tarjeta'] = $this->Caja_model->movimientos_tarjeta($data['oCaja']->caja_id);
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'caja' ),
                    array('link' => '#', 'page' => 'Detalle de caja' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Detalle de caja', 'bc' => $bc, 'modulo'=>'caja', 'item' => 'historial_cierres' );
        $this->page_construct('caja/caja/detalle', $meta, $data);
    }
    function exportar_cierres(){
        $this->loaders->verifica_sesion();
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $cajas = $this->Caja_model->get_all_by_fechas($start_date, $end_date);
        // echo json_encode($comandas);exit();
        $this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Historial de cierres de caja");
        $cont_excel = 1;
        //Le aplicamos ancho las columnas.
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        //Le aplicamos negrita a los títulos de la cabecera.
        $this->excel->getActiveSheet()->getStyle("A{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("B{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("C{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("E{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("F{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("N{$cont_excel}")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("O{$cont_excel}")->getFont()->setBold(true);
        //Definimos los títulos de la cabecera.
        $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", 'Cód.');
        $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", 'Aperturado en');
        $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", 'Aperturado por');
        $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", 'Monto inicial');
        $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", 'Cerrado en');
        $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", 'Cerrado por');
        $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", 'Total ventas efectivo');
        $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", 'Total gastos efectivo');
        $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", 'Saldo en caja');
        $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", 'Total ventas tarjeta');
        $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", 'Total gastos tarjeta ');
        $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", 'Monto retirado');
        $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", 'Remanente');
        $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", 'Observaciones');
        $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", 'Estado');
        foreach ($cajas as $key => $caja) {
            $cont_excel++;
            if ($caja->estado == 'A') {
                $estado_desc = "Aperturada";
            }else{
                $estado_desc = "Cerrada";
            }
            $this->excel->getActiveSheet()->setCellValue("A{$cont_excel}", $caja->caja_id);
            $this->excel->getActiveSheet()->setCellValue("B{$cont_excel}", $caja->aperturado_en);
            $this->excel->getActiveSheet()->setCellValue("C{$cont_excel}", $caja->aperturado_por);
            $this->excel->getActiveSheet()->setCellValue("D{$cont_excel}", $caja->monto_inicial);
            $this->excel->getActiveSheet()->setCellValue("E{$cont_excel}", $caja->cerrado_en);
            $this->excel->getActiveSheet()->setCellValue("F{$cont_excel}", $caja->cerrado_por);
            $this->excel->getActiveSheet()->setCellValue("G{$cont_excel}", $caja->total_ventas_efectivo);
            $this->excel->getActiveSheet()->setCellValue("H{$cont_excel}", $caja->total_gastos_efectivo);
            $this->excel->getActiveSheet()->setCellValue("I{$cont_excel}", $caja->saldo_caja); 
            $this->excel->getActiveSheet()->setCellValue("J{$cont_excel}", $caja->total_ventas_tarjeta);
            $this->excel->getActiveSheet()->setCellValue("K{$cont_excel}", $caja->total_gastos_tarjeta);
            $this->excel->getActiveSheet()->setCellValue("L{$cont_excel}", $caja->monto_retirado);
            $this->excel->getActiveSheet()->setCellValue("M{$cont_excel}", $caja->remanente);
            $this->excel->getActiveSheet()->setCellValue("N{$cont_excel}", $caja->observaciones);
            $this->excel->getActiveSheet()->setCellValue("O{$cont_excel}", $estado_desc);

            

            $this->excel->getActiveSheet()->getStyle("D{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("G{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("H{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("I{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("J{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("K{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("L{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
            $this->excel->getActiveSheet()->getStyle("M{$cont_excel}")->getNumberFormat()->setFormatCode('0.00');
        }

        $archivo_excel = "Historial_cierres_caja."."xls";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$archivo_excel.'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //Hacemos una salida al navegador con el archivo_excel Excel.
        $objWriter->save('php://output');
    }

    // OPERACIONES A BASE DE DATOS
    function aperturar(){
        $this->loaders->verifica_sesion();
        try {
            $data = array(  'aperturado_en' => date("Y/m/d H:i:s"),
                            'aperturado_por' => $this->session->userdata('session_usuario'),
                            'monto_inicial' => $this->input->post('monto_inicial'),
                        );
            $this->form_validation->set_rules('aperturado_en', "Fecha apertura", 'required');
            $this->form_validation->set_rules('aperturado_por', "Aperturado por", 'required');
            $this->form_validation->set_rules('monto_inicial', "Monto inicial", 'required');
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');

            if ($this->form_validation->run() == true) {
                $caja_id = $this->Caja_model->aperturar($data);
                if(!$caja_id){
                    $this->session->set_flashdata('success', false);
                    $this->session->set_flashdata('message', 'Problemas para aperturar la caja.');
                }else{
                    $this->session->set_flashdata('success', true);
                    $this->session->set_flashdata('message', "Caja aperturada correctamente.");

                    //popup para enviar la apertura a la NUBE
                    $this->set_session_enviar_caja_nube($caja_id);
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('facturacion/caja'));
        }
    }

    function cerrar($caja_id){
        $this->loaders->verifica_sesion();

        try {
            if(count($this->Caja_model->get_aperturada()) == 0){
                throw new Exception("no se encontró la caja aperturada.");
            }
            // Obtenemos la caja
            $oCaja = ($this->Caja_model->get_aperturada())[0];
            // Vemos si la caja abierta con la que enviaron coincide.
            if($caja_id != $oCaja->caja_id){
                throw new Exception("Error en la operación, se pretende cerrar una caja diferente a la actual aperturada.");
            }

            //Validar operaciones de cierre
            $rptaValida = $this->validarOperacionesCierre($oCaja);
            if(!$rptaValida['success']){
                throw new Exception($rptaValida['message']);
            }

            // Obtengo todas las comanda de dicha caja
            $comandas = $this->Comanda_model->get_all_by_caja($caja_id);
            //Verifico que todas estén pagadas
            $nro_comandas_sin_pago = 0;
            $nro_comandas_sin_atencion = 0;
            foreach ($comandas as $key => $comanda) {
                if($comanda->estado_pago == 'P'){ //Si esta pendiente
                    if ($comanda->estado_atencion != 'X') {
                        $nro_comandas_sin_pago++;
                    }
                }
                if($comanda->estado_atencion == 'E' || $comanda->estado_atencion == 'A'){
                    $nro_comandas_sin_atencion++;
                }
            }
            if($nro_comandas_sin_pago > 0){
                $mensaje = "NO puede cerrar la caja debido a que existen ".$nro_comandas_sin_pago." comandas pendientes de pago.";
                throw new Exception($mensaje);
            }
            if($nro_comandas_sin_atencion > 0){
                $mensaje = "NO puede cerrar la caja debido a que existen ".$nro_comandas_sin_atencion." comandas pendientes de finalizar atención.";
                throw new Exception($mensaje);
            }
            //Datos para el cierre de caja
            $nueva_data = array('caja_id' => $caja_id,
                                'cerrado_en' => date("Y/m/d H:i:s"),
                                'cerrado_por' => $this->session->userdata('session_usuario'),
                                'total_ventas_efectivo' => $this->input->post('total_ventas_efectivo'),
                                'total_gastos_efectivo' => $this->input->post('total_gastos_efectivo'),
                                'saldo_caja' => $this->input->post('saldo_caja'),
                                'total_ventas_tarjeta' => $this->input->post('total_ventas_tarjeta'),
                                'total_gastos_tarjeta' => $this->input->post('total_gastos_tarjeta'),
                                'observaciones' => $this->input->post('observaciones'),
                                'estado' => 'C',
                                );
            // OBTENER TOTALES 
            $movimientos_caja = $this->Caja_model->movimientos_caja($oCaja->caja_id);
            $movimientos_tarjeta = $this->Caja_model->movimientos_tarjeta($oCaja->caja_id);
            // Iniciales
            $total_ventas_efectivo = 0;
            $total_gastos_efectivo = 0;
            $total_ventas_tarjeta = 0;
            $total_gastos_tarjeta = 0;
            // Calculamos
            foreach ($movimientos_caja as $key => $mov) {
                if ($mov->operacion == "Venta") {
                    $total_ventas_efectivo += $mov->monto;
                }
                if ($mov->operacion == "Gasto") {
                    $total_gastos_efectivo += $mov->monto;
                }
            }
            foreach ($movimientos_tarjeta as $key => $mov) {
                if ($mov->operacion == "Venta") {
                    $total_ventas_tarjeta += $mov->monto;
                }
                if ($mov->operacion == "Gasto") {
                    $total_gastos_tarjeta += $mov->monto;
                }
            }
            $total_ventas = $total_ventas_efectivo + $total_ventas_tarjeta;
            $total_gastos = $total_gastos_efectivo + $total_gastos_tarjeta;
            $saldo_caja = $oCaja->monto_inicial + $total_ventas - $total_gastos;
            $nueva_data['total_ventas_efectivo'] = $total_ventas_efectivo;
            $nueva_data['total_gastos_efectivo'] = $total_gastos_efectivo;
            $nueva_data['total_ventas_tarjeta'] = $total_ventas_tarjeta;
            $nueva_data['total_gastos_tarjeta'] = $total_gastos_tarjeta;
            $nueva_data['saldo_caja'] = $saldo_caja;

            // echo json_encode($nueva_data);exit();
            $rpta = $this->Caja_model->cerrar($nueva_data);
            if(!$rpta){
                $this->session->set_flashdata('success', false);
                $this->session->set_flashdata('message', 'Problemas para cerrar la caja.');
                redirect($_SERVER["HTTP_REFERER"]);
            }else{
                $this->session->set_flashdata('success', true);
                $this->session->set_flashdata('message', "Caja cerrada correctamente.");


                //popup para enviar la caja cierre a la NUBE
                $this->set_session_enviar_caja_nube($caja_id);

                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            // Guardar en un log
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function validarOperacionesCierre($oCaja){
        $response['success'] = true;
        $response['message'] = "";

        //Validar sincronización de asistencias
        $var_sync_marcaciones = intval($this->obtener_variable('sync_marcaciones'));
        if($var_sync_marcaciones == 1){
            if($oCaja->fecha_sync_marcaciones == null){
                $response['success'] = false;
                $response['message'] = "Debe sincronizar las asistencias del personal.";
                return $response;
            }
        }

        //Validar sincronización de procesar boletas y facturas FACSU
        $var_sync_procesar_cbpes = intval($this->obtener_variable('sync_facsu_procesar_cpbes'));
        if($var_sync_procesar_cbpes == 1){
            if($oCaja->fecha_procesar_cpbe == null){
                $response['success'] = false;
                $response['message'] = "Debe procesar los comprobantes electrónicos.";
                return $response;
            }
        }

        return $response;
    }

}
