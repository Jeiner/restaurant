<?php defined('BASEPATH') or exit('No direct script access allowed');

class Gastos extends CI_Controller
{
    public function __construct()
    {        
        parent::__construct();
        $this->load->model('Gasto_model');
        $this->load->model('Caja_model');
        $this->load->model('Multitabla_model');
        $this->load->library('session');
        // $this->loaders->verifica_sesion();
        // $this->Caja_model->verificar_caja_aperturada();
    }
    function preparar_obj_gasto(){
        if($this->session->userdata('oGasto')){
            return $this->session->userdata('oGasto');
        }else{
            $oGasto = new Gasto_model();
            $this->session->set_userdata('oGasto', $oGasto);
            return $this->session->userdata('oGasto');
        }
    }
    function set_datos_obj_gasto($data){
        $oGasto =  $this->preparar_obj_gasto();

        $oGasto->gasto_id = $data['gasto_id'];
        $oGasto->fecha = $data['fecha'];
        $oGasto->usuario = $data['usuario'];
        $oGasto->caja_id = $data['caja_id'];
        $oGasto->monto = $data['monto'];
        $oGasto->tipo = $data['tipo'];
        $oGasto->descripcion = $data['descripcion'];
        $oGasto->estado = $data['estado'];

        $this->session->set_userdata('oGasto', $oGasto);
    }
    function index(){
        $this->session->unset_userdata('oGasto');
        $data['gastos'] = $this->Gasto_model->get_all();
        $bc = array(
                    array('link' => base_url(), 'page' => 'Inicio'),
                    array('link' => '#', 'page' => 'Caja' ),
                    array('link' => '#', 'page' => 'Gastos' ),
                ); //breadcrumbs 
        $meta = array( 'page_title' => 'Gastos', 'bc' => $bc, 'modulo'=>'caja', 'item' => 'gastos' );
        $this->page_construct('caja/gastos/index', $meta, $data);
    }
    function nuevo(){
        try {
            $this->Caja_model->verificar_caja_aperturada();
            $data['oGasto'] = $this->preparar_obj_gasto();
            $data['oGasto']->fecha = date("d/m/Y H:i:s");
            $data['oGasto']->usuario = $this->session->userdata('session_usuario');
            $data['tipos'] = $this->Multitabla_model->get_all_by_tipo(2); //Tipos de gasto Efect, Tarj.
            // echo json_encode($data['oGasto']);exit();
            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Caja' ),
                        array('link' => '#', 'page' => 'Nuevo gasto' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Nuevo gasto', 'bc' => $bc, 'modulo'=>'caja', 'item' => 'gastos' );
            $this->page_construct('caja/gastos/nuevo', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('caja/gastos'));
        }
    }
    function ver($gasto_id){
        // Ver Detalle de un gasto
        // $this->Caja_model->verificar_caja_aperturada();
        try {
            if(count( $this->Gasto_model->get_one($gasto_id)) == 0){
                throw new Exception("Error: No se encontró el gasto especificado. Porfavor comuníquese con el área de soporte.");
            }
            $this->session->unset_userdata('oGasto');
            $data['oGasto'] = ($this->Gasto_model->get_one($gasto_id))[0];
            $data['tipos'] = $this->Multitabla_model->get_all_by_tipo(2); //Tipos de gasto Efect, Tarj.
            // echo json_encode($data['oGasto']);exit();
            $bc = array(
                        array('link' => base_url(), 'page' => 'Inicio'),
                        array('link' => '#', 'page' => 'Caja' ),
                        array('link' => '#', 'page' => 'Ver gasto' ),
                    ); //breadcrumbs 
            $meta = array( 'page_title' => 'Ver gasto', 'bc' => $bc, 'modulo'=>'caja', 'item' => 'gastos' );
            $this->page_construct('caja/gastos/ver', $meta, $data);
        } catch (Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            $log_mensaje = $e->getMessage()." --> ".$e->getFile();
            log_message('error', $log_mensaje);
            redirect(base_url('caja/gastos'));
        }
    }

    // OPERACIONES A BASE DE DATOS
    function guardar(){
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        try {
            $data = array(  'gasto_id' => $this->input->post('gasto_id'),
                            'fecha' => date("Y/m/d H:i:s"),
                            'usuario' => $this->session->userdata('session_usuario'),
                            'caja_id' => $oCaja->caja_id,
                            'monto' => $this->input->post('monto'),
                            'tipo' => $this->input->post('tipo'),
                            'descripcion' => $this->input->post('descripcion'),
                            'estado' => 'A',
                        );
            $this->set_datos_obj_gasto($data);
            $this->form_validation->set_rules('monto', "Monto gastado", 'required');
            $this->form_validation->set_rules('tipo', "Tipo de gasto", 'required');
            
            $this->form_validation->set_message('required', '%s: El campo es obligatorio.');
            $this->form_validation->set_message('min_length', '%s: El mínimo de caracteres es %s.');

            if ($this->form_validation->run() == true) {
                if( $this->input->post('gasto_id') == 0 ){
                    $gasto_id = $this->Gasto_model->insert($data);
                    if(!$gasto_id){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para registrar el gasto.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Gasto registrado correctamente.");
                        redirect(base_url('caja/gastos'));
                    }
                }else{
                    $rpta = $this->Gasto_model->update($data);
                    if(!$rpta){
                        $this->session->set_flashdata('success', false);
                        $this->session->set_flashdata('message', 'Problemas para actualizar el gasto.');
                    }else{
                        $this->session->set_flashdata('success', true);
                        $this->session->set_flashdata('message', "Gasto actualizado correctamente.");
                        redirect(base_url('caja/gastos'));
                    }
                }
            }else{
                $message = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));                
                $this->session->set_flashdata('message',$message);
                $this->session->set_flashdata('success',false);
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } catch (\Exception $e) {
            $this->session->set_flashdata('success',false);
            $this->session->set_flashdata('message',$e->getMessage());
            redirect(base_url('caja/gastos'));
        }
    }
    function anular($gasto_id){  //Metodo sin retorno de vista
        $this->Caja_model->verificar_caja_aperturada();
        $this->loaders->verifica_sesion();
        $oGasto = ($this->Gasto_model->get_one($gasto_id))[0];
        $oCaja = ($this->Caja_model->get_aperturada())[0];
        // echo json_encode($oGasto);exit();
        $data = array(  'gasto_id' => $gasto_id,
                        'anulado_en' => date("Y/m/d H:i:s"),
                        'anulado_por' => $this->session->userdata('session_usuario'),
                        'estado' => 'X',
                    );
        // echo json_encode($data);exit();
        if($oGasto->caja_id != $oCaja->caja_id){
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'No se puede anular el gasto, debido a que la caja de dicho movimiento está cerrada.');
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if( $this->Gasto_model->anular($data) ){
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', 'Gasto anulado correctamente.');
            redirect(base_url('caja/gastos'));
        }else{
            $this->session->set_flashdata('success', false);
            $this->session->set_flashdata('message', 'No se puede anular el gasto.');
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }
    // OTRAS OPERACIONES
}
