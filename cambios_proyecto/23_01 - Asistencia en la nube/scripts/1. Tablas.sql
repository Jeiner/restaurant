DROP TABLE IF EXISTS nube_marcacion;
CREATE TABLE nube_marcacion(
	transaction_id INT NOT NULL,
    emp_id INT NOT NULL,
    emp_code VARCHAR(100) NOT NULL,
    first_name VARCHAR(200) NULL,
    photo VARCHAR(100) NULL,
    department_id INT NULL,
    create_time DATETIME NULL,
    punch_state varchar(05),
    punch_time DATETIME NULL,
    upload_time DATETIME NULL,
    
    registrado_por VARCHAR(100),
    fecha_registro DATETIME,
    modificado_por VARCHAR(100),
    fecha_modificacion DATETIME
);

DROP TABLE IF EXISTS nube_marcacion_empleado;
CREATE TABLE nube_marcacion_empleado(
	emp_id INT NOT NULL,
    emp_code VARCHAR(100) NOT NULL,
    first_name VARCHAR(200) NULL,
    hora_entrada VARCHAR(5),
    hora_salida VARCHAR(5),
    estado bit not null,
    
    registrado_por VARCHAR(100) NULL,
    fecha_registro DATETIME NULL,
    modificado_por VARCHAR(100) NULL,
    fecha_modificacion DATETIME 
);

ALTER TABLE nube_marcacion_empleado
ADD fecha_ingreso DATETIME NULL;


ALTER TABLE caja
ADD fecha_sync_marcaciones DATETIME;

ALTER TABLE caja
ADD fecha_sync_comandas DATETIME;
