select * from variable
CREATE TABLE variable(
	variable_id int not null auto_increment primary key,
    nombre_variable varchar(20) not null unique,
    valor_variable varchar(50) not null,
    estado varchar(1) not null,
    fecha_registro datetime null,
    registrado_por varchar(20) null,
    fecha_modificacion datetime null,
    modificado_por varchar(20) null
);

insert into variable (nombre_variable,valor_variable,estado,fecha_registro, registrado_por) 
	values('igv','0.18','A',now(),'jharo');
    