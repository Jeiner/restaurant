#La primera cara (pizzas)
#select * from producto where (familia_id = 1) order by producto_id asc limit 21;
update producto set ubi_carta = 1 where (familia_id = 1 ) and producto_id <= 21;

#Segunda cara
#select * from producto where (familia_id = 1) and producto_id > 21 order by producto_id asc limit 18;
update producto set ubi_carta = 2 where (familia_id = 1 ) and producto_id > 21 and producto_id <= 39 ;

#Tercera cara
#select * from producto where (familia_id = 1) and producto_id > 39 order by producto_id asc limit 21;
update producto set ubi_carta = 3 where (familia_id = 1) and producto_id > 39 and producto_id <= 60 ;

#Cuarta cara
#select * from producto where (familia_id = 1) and producto_id > 60 order by producto_id asc limit 21;
update producto set ubi_carta = 4 where (familia_id = 1) and producto_id > 60 and producto_id <= 81 ;

#Quinta cara //De un gusto
#select * from producto where (familia_id = 1 or familia_id = 2) and producto_id > 81 order by producto_id asc limit 30;
update producto set ubi_carta = 5 where (familia_id = 1 or familia_id = 2) and producto_id > 81 and producto_id <= 111 ;

#Quinta cara //Agregados
#select * from producto where (familia_id = 3) and producto_id > 111 order by producto_id asc limit 9;
update producto set ubi_carta = 5 where (familia_id = 3) and producto_id > 111 and producto_id <= 120 ;

#Sexta  //Pastas
#select * from producto where (familia_id = 4) and producto_id > 122 order by producto_id asc limit 14;
update producto set ubi_carta = 6 where (familia_id = 4) and producto_id > 122 and producto_id <= 136 ;

#Septima // Sangrias
#select * from producto where (familia_id = 5) and producto_id > 122 order by producto_id asc limit 12;
update producto set ubi_carta = 7 where (familia_id = 5) and producto_id > 136 and producto_id <= 148 ;

#Septima // Vinos
#select * from producto where (familia_id = 6) and producto_id > 122 order by producto_id asc limit 15;
update producto set ubi_carta = 7 where (familia_id = 6) and producto_id > 148 and producto_id <= 163 ;

#Octaba // Licores
#select * from producto where (familia_id = 7) and producto_id > 163 order by producto_id asc limit 7;
update producto set ubi_carta = 8 where (familia_id = 7) and producto_id > 163 and producto_id <= 170 ;

#Octaba // Bebidas
#select * from producto where (familia_id = 8) and producto_id > 170 order by producto_id asc limit 3;
update producto set ubi_carta = 8 where (familia_id = 8) and producto_id > 170 and producto_id <= 173 ;

#Novena //jugos
#select * from producto where (familia_id = 9) and producto_id > 175 order by producto_id asc limit 21;
update producto set ubi_carta = 9 where (familia_id = 9) and producto_id > 175 and producto_id <= 196 ;

#Novena //Infusiones
#select * from producto where (familia_id = 10) and producto_id > 196 order by producto_id asc limit 5;
update producto set ubi_carta = 9 where (familia_id = 10) and producto_id > 196 and producto_id <= 201 ;


/*
	ACTUALIZACIÓN PARA NUEVOS PRODUCTOS (FUGGAZA, CONTINENTAL)
*/
#select * from producto where producto like '%fuga%';
update producto set ubi_carta = 4 where producto_id in ('316','317','318');

#select * from producto where producto like '%contine%';
update producto set ubi_carta = 3 where producto_id in ('313','314','315');

/*
	ACTUALIZAR PARA PRESENTACION MEGA (Mega)
*/

#Primera
#select * from producto where producto like '%Mega%' and producto_id >= 319 and producto_id <= 325;
update producto set ubi_carta = 1 where producto like '%Mega%' and producto_id >= 319 and producto_id <= 325;

#Segunda
#select * from producto where producto like '%Mega%' and producto_id >= 326 and producto_id <= 330;
#select * from producto where producto_id = 311;
update producto set ubi_carta = 2 where producto like '%Mega%' and producto_id >= 326 and producto_id <= 330;
update producto set ubi_carta = 2 where producto_id = 311;
#Tercera
#select * from producto where producto like '%Mega%' and producto_id >= 331 and producto_id <= 337;
update producto set ubi_carta = 3 where producto like '%Mega%' and producto_id >= 331 and producto_id <= 337;

#Cuardta
#select * from producto where producto like '%Mega%' and producto_id >= 338 and producto_id <= 344;
update producto set ubi_carta = 4 where producto like '%Mega%' and producto_id >= 338 and producto_id <= 344;

#Quinta
#select * from producto where producto like '%Mega%' and producto_id >= 345 and producto_id <= 359;
update producto set ubi_carta = 5 where producto like '%Mega%' and producto_id >= 345 and producto_id <= 359;

COMMIT;