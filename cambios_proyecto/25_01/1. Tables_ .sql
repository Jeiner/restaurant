

ALTER TABLE empleado
ADD dia_pago CHAR(02);


DROP TABLE IF EXISTS empleado_pre_pago;
CREATE TABLE IF NOT EXISTS empleado_pre_pago (
  pre_pago_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  empleado_id INT NOT NULL,
  importe decimal(9,2) NOT NULL,
  tipo_concepto varchar(3) DEFAULT NULL COMMENT 'REM, DES, ADE, TAR, FAL',
  signo int(11) NOT NULL COMMENT '+1, -1, 0',
  comentario varchar(250) DEFAULT NULL,
  fecha_pago datetime,
  pago_id INT NULL,

  estado varchar(1) NOT NULL DEFAULT 'A' COMMENT 'A:activo, X:Anulado',
  fecha_registro datetime DEFAULT CURRENT_TIMESTAMP,
  registrado_por varchar(100) DEFAULT NULL,
  fecha_modificacion datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  modificado_por varchar(100) DEFAULT NULL
) 

DROP TABLE IF EXISTS empleado_pago;
CREATE TABLE IF NOT EXISTS empleado_pago(
  pago_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  empresa_id INT NOT NULL,
  sucursal_id INT NOT NULL,
  empleado_id INT NOT NULL,
  fecha_pago DATETIME NOT NULL,
  importe_total decimal(9,2) NOT NULL,
  numero_items INT NOT NULL,
  bloquear_periodos BIT NULL,
    
  estado varchar(1) NOT NULL DEFAULT 'A' COMMENT 'A:activo, X:Anulado',
  fecha_registro datetime DEFAULT CURRENT_TIMESTAMP,
  registrado_por varchar(100) DEFAULT NULL,
  fecha_modificacion datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  modificado_por varchar(100) DEFAULT NULL
);


ALTER TABLE multitabla
ADD columna_aux_1 VARCHAR(50) NULL;