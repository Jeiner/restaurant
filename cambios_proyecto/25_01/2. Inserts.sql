

INSERT INTO `modulo` (`modulo_id`, `modulo`, `icono`, `prioridad`, `para_seleccionar`, `estado`, `fecha_registro`, `tipo_sistema`) 
VALUES ('11', 'Planilla', '<i class=\"menu-icon fa fa-users\"></i>', '13', 'planilla', 'A', '2025-01-06 17:06:11', 'PROD');


INSERT INTO `opcion` (`opcion_id`, `modulo_id`, `opcion`, `icono`, `prioridad`, `controlador`, `accion`, `para_seleccionar`, `estado`, `fecha_registro`, `registrado_por`, `fecha_modificacion`, `modificado_por`, `tipo_sistema`) 
VALUES
(35, 11, 'Empleados', '', 5, 'empleados', 'index', 'empleados', 'A', '2018-08-13 17:06:11', NULL, NULL, NULL, 'PROD'),
(36, 11, 'Liquidaciones', '', 4, 'liquidaciones', 'index', 'liquidaciones', 'A', '2018-08-13 17:06:11', NULL, NULL, NULL, 'PROD'),
(37, 11, 'Nueva liquidación', '', 3, 'liquidaciones', 'nueva', 'nueva_liquidacion', 'A', '2018-08-13 17:06:11', NULL, NULL, NULL, 'PROD'),
(38, 11, 'Cumpleaños', '', 2, 'empleados', 'cumpleanios', 'cumpleanios', 'A', '2018-08-13 17:06:11', NULL, NULL, NULL, 'PROD');


INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `orden`) VALUES ('12', 'ADE', 'ADELANTO', 'A', '0');
INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `orden`) VALUES ('12', 'TAR', 'TARDANZA', 'A', '0');
INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `orden`) VALUES ('12', 'FAL', 'FALTA', 'A', '0');

INSERT INTO `tipo_multitabla` (`tipo_multitabla_id`, `tipo_multitabla`) VALUES ('45', 'TIPO CONCEPTO PREPAGO');
INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `columna_aux_1`) VALUES ('45', 'ADE', 'ADELANTO', 'A', '#bg-item-adelanto');
INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `columna_aux_1`) VALUES ('45', 'FAL', 'FALTA', 'A', '#bg-item-falta');
INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `columna_aux_1`) VALUES ('45', 'TAR', 'TARDANZA', 'A', '#bg-item-tardanza');
INSERT INTO `multitabla` (`tipo_multitabla_id`, `valor`, `descripcion`, `estado`, `columna_aux_1`) VALUES ('45', 'DES', 'DESCUENTO', 'A', '#bg-item-descuento');
