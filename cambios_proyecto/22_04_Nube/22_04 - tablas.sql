
/**/
ALTER TABLE generalidades
ADD empresa_codigo VARCHAR(3) NULL;

ALTER TABLE generalidades
ADD sucursal_codigo VARCHAR(3) NULL;

UPDATE generalidades SET empresa_codigo = '001', sucursal_codigo = '002' WHERE generalidades_id = 1;

INSERT INTO validacion (validacion_id, nombre_variable, validacion, estado, comentario, valor, tipo, fecha_registro, registrado_por) 
VALUES ('012', 'url_enviar_comanda_nube', 'Enviar comanda a la nube', 0, '', 'http://santorini.com.pe/administracion/webservice/comanda_api/insert_comanda', 'URL', '20220915', 'SYS');

INSERT INTO validacion (validacion_id, nombre_variable, validacion, estado, comentario, valor, tipo, fecha_registro, registrado_por) 
VALUES ('013', 'url_enviar_comprobante_nube', 'Enviar comprobante electrónico a la nube', 0, '', 'http://santorini.com.pe/administracion/webservice/comprobante_api/insert_comprobante', 'URL', '20220915', 'SYS');

INSERT INTO validacion (validacion_id, nombre_variable, validacion, estado, comentario, valor, tipo, fecha_registro, registrado_por) 
VALUES ('014', 'url_enviar_pago_nube', 'Enviar pago a la nube', 0, '', 'http://santorini.com.pe/administracion/webservice/pago_api/insert_pago', 'URL', '20220915', 'SYS');


drop table if exists nube_sucursal;
CREATE TABLE nube_sucursal(
	empresa_codigo VARCHAR(03) NOT NULL,
    sucursal_codigo VARCHAR(03) NOT NULL,
    empresa_nombre VARCHAR(20) NOT NULL,
    sucursal_nombre VARCHAR(20) NOT NULL,
    primary key(empresa_codigo, sucursal_codigo)
);

INSERT INTO nube_sucursal(empresa_codigo, sucursal_codigo, empresa_nombre, sucursal_nombre) values
('001','001','Santorini','Trujillo-Quintanas'),
('001','002','Santorini','Trujillo-Primavera'),
('002','001','Sanbrasa','Trujillo-Quintanas');

DROP TABLE IF EXISTS nube_pago;
CREATE TABLE IF NOT EXISTS nube_pago (
	pago_codigo varchar(25) not null PRIMARY KEY, /*emp, suc, pago*/
	comanda_codigo varchar(25) not null, /*emp, suc, comanda*/
	empresa_codigo varchar(3) not null,
	sucursal_codigo varchar(3) not null,
    fecha_registro_nube datetime null,
    
    pago_id int(11) NOT NULL,
    comanda_id int(11) NOT NULL,
    importe_total decimal(9,2) NOT NULL,
    descuento decimal(9,2) NOT NULL,
    total_a_pagar decimal(9,2) NOT NULL,
    pago_efectivo decimal(9,2) NOT NULL,
    pago_tarjeta decimal(9,2) NOT NULL,
    total_pagado decimal(9,2) NOT NULL,
    saldo decimal(9,2) NOT NULL,
    efectivo decimal(9,2) NOT NULL COMMENT 'cantidad con la que pago',
    vuelto decimal(9,2) NOT NULL COMMENT 'vuelto',
    fecha_pago datetime NOT NULL,
    cajero varchar(150) DEFAULT NULL,
    tipo_comprobante char(3) DEFAULT NULL COMMENT 'B:Boleta, F:Factura',
    serie varchar(5) NOT NULL,
    correlativo varchar(6) NOT NULL,
    cliente_id int(11) DEFAULT NULL,
    cliente_documento varchar(11) DEFAULT NULL,
    cliente_nombre varchar(250) DEFAULT NULL,
    cliente_direccion varchar(250) DEFAULT NULL,
    cliente_telefono varchar(250) DEFAULT NULL,
    caja_id int(11) NOT NULL,
    observaciones varchar(250) DEFAULT NULL,
    estado char(1) NOT NULL COMMENT 'A:Activa, X:Anulada',
    anulado_en datetime DEFAULT NULL,
    anulado_por varchar(100) DEFAULT NULL,
    fecha_registro datetime DEFAULT CURRENT_TIMESTAMP,
    registrado_por varchar(100) DEFAULT NULL,
    fecha_modificacion datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) DEFAULT NULL
);

DROP TABLE IF EXISTS nube_cpbe_cabecera;
CREATE TABLE IF NOT EXISTS nube_cpbe_cabecera (
	comprobante_codigo varchar(50) not null primary key, /*emp, suc, serie, correlativo*/
	comanda_codigo varchar(25) not null, /*emp, suc, comanda*/
	empresa_codigo varchar(3) not null,
	sucursal_codigo varchar(3) not null,
    fecha_registro_nube datetime null,
    
    comanda_id int(11) NOT NULL,
    pago_id int(11) NOT NULL,
    cabecera_id int(11) NOT NULL,
    tipoComprobante varchar(5) NOT NULL,
    serie varchar(6) NOT NULL,
    correlativo varchar(10) NOT NULL,
    tipoOperacion varchar(4) NOT NULL,
    fecEmision varchar(10) NOT NULL,
    horEmision varchar(14) NOT NULL,
    fecVencimiento varchar(10) NOT NULL,
    codLocalEmisor varchar(5) NOT NULL,
    tipDocUsuario varchar(10) NOT NULL,
    numDocUsuario varchar(15) NOT NULL,
    rznSocialUsuario varchar(100) NOT NULL,
    tipMoneda varchar(5) NOT NULL,
    sumTotTributos decimal(9,2) NOT NULL,
    sumTotValVenta decimal(9,2) NOT NULL,
    sumPrecioVenta decimal(9,2) NOT NULL,
    sumDescTotal decimal(9,2) NOT NULL,
    sumOtrosCargos decimal(9,2) NOT NULL,
    sumTotalAnticipos decimal(9,2) NOT NULL,
    sumImpVenta decimal(9,2) NOT NULL,
    ublVersionId varchar(5) NOT NULL,
    customizationId varchar(5) NOT NULL,
    codHash varchar(100) DEFAULT NULL,
    estadoSunat varchar(2) DEFAULT NULL
);

DROP TABLE IF EXISTS nube_cpbe_detalle;
CREATE TABLE IF NOT EXISTS nube_cpbe_detalle (
	comprobante_codigo varchar(50) not null primary key, /*emp, suc, serie, correlativo*/
	comanda_codigo varchar(25) not null, /*emp, suc, comanda*/
    
    comanda_item_id int(11) NOT NULL,
    comanda_id int(11) DEFAULT NULL,
    cabecera_id int(11) DEFAULT NULL,
    importe decimal(9,2) NOT NULL,
    codUnidadMedida varchar(5) NOT NULL,
    ctdUnidadItem int(11) NOT NULL,
    codProducto varchar(10) NOT NULL,
    codProductoSUNAT varchar(5) NOT NULL,
    desItem varchar(100) NOT NULL,
    mtoValorUnitario decimal(9,2) NOT NULL,
    sumTotTributosItem decimal(9,2) NOT NULL,
    codTriIGV varchar(10) NOT NULL,
    mtoIgvItem decimal(9,2) NOT NULL,
    mtoBaseIgvItem decimal(9,2) NOT NULL,
    nomTributoIgvItem varchar(5) NOT NULL,
    codTipTributoIgvItem varchar(5) NOT NULL,
    tipAfeIGV varchar(10) NOT NULL,
    porIgvItem decimal(9,2) NOT NULL,
    codTriISC varchar(10) NOT NULL,
    mtoIscItem decimal(9,2) NOT NULL,
    mtoBaseIscItem decimal(9,2) NOT NULL,
    nomTributoIscItem varchar(50) NOT NULL,
    codTipTributoIscItem varchar(10) NOT NULL,
    tipSisISC varchar(10) NOT NULL,
    porIscItem varchar(10) NOT NULL,
    codTriOtroItem varchar(10) NOT NULL,
    mtoTriOtroItem decimal(9,2) NOT NULL,
    mtoBaseTriOtroItem decimal(9,2) NOT NULL,
    nomTributoIOtroItem varchar(10) NOT NULL,
    codTipTributoIOtroItem varchar(10) NOT NULL,
    porTriOtroItem varchar(10) NOT NULL,
    mtoPrecioVentaUnitario decimal(9,2) NOT NULL,
    mtoValorVentaItem decimal(9,2) NOT NULL,
    mtoValorReferencialUnitario decimal(9,2) NOT NULL
);

drop table if exists nube_comanda;
create table nube_comanda(
	comanda_codigo varchar(25) not null primary key, /*emp, suc, comanda*/
	empresa_codigo varchar(3) not null,
	sucursal_codigo varchar(3) not null,
    fecha_registro_nube datetime null,
    
	comanda_id int(11) NOT NULL,
	modalidad char(3) NOT NULL COMMENT 'PRE:presencial, DEL:Delivery, LLE:Llevar,',
	fecha_comanda datetime NOT NULL,
	mozo varchar(100) DEFAULT NULL,
	mesa_id int(11) DEFAULT NULL,
	mesa varchar(50) DEFAULT NULL,
	nro_comensales int(11) DEFAULT NULL,
	prioridad int(11) DEFAULT NULL,
	nro_productos int(11) DEFAULT NULL,
	pago_id int(11) DEFAULT NULL,
	tasa_IGV decimal(9,2) DEFAULT NULL,
	total_IGV decimal(9,2) DEFAULT NULL,
	importe_sin_IGV decimal(9,2) DEFAULT NULL,
	importe_total decimal(9,2) DEFAULT NULL,
	descuento decimal(9,2) DEFAULT NULL,
	total_pagado decimal(9,2) DEFAULT NULL,
	fecha_pago datetime DEFAULT NULL,
	pagado_por varchar(50) DEFAULT NULL,
	tipo_comprobante char(3) DEFAULT NULL COMMENT 'B:Boleta, F:Factura',
	cliente_id int(11) DEFAULT NULL,
	cliente_documento varchar(11) DEFAULT NULL,
	cliente_nombre varchar(250) DEFAULT NULL,
	cliente_direccion varchar(250) DEFAULT NULL,
	cliente_telefono varchar(250) DEFAULT NULL,
	union_mesas int(11) NOT NULL DEFAULT '0',
	caja_id int(11) NOT NULL,
	estado_atencion char(1) NOT NULL DEFAULT 'E' COMMENT 'E:En espera, A:Atendido, F:Finalizado, X:Anulado',
	estado_pago char(1) NOT NULL DEFAULT 'P' COMMENT 'P:Pendiente,  C:Cancelado, X:Anulado',
	atendida_en datetime DEFAULT NULL,
	prefacturada_en datetime DEFAULT NULL,
	prefacturada_por varchar(50) DEFAULT NULL,
	finalizada_en datetime DEFAULT NULL,
	finalizada_por varchar(50) DEFAULT NULL,
	anulado_en datetime DEFAULT NULL,
	anulado_por varchar(50) DEFAULT NULL,
	anulado_motivo varchar(500) DEFAULT NULL,
	fecha_registro datetime DEFAULT NULL,
	registrado_por varchar(100) DEFAULT NULL,
	fecha_modificacion datetime DEFAULT NULL,
	modificado_por varchar(100) DEFAULT NULL,
	comentario varchar(250) DEFAULT NULL,
	cmd_comentario varchar(250) DEFAULT NULL,
	sync bit(1) NOT NULL DEFAULT b'0',
	fecha_sync datetime DEFAULT NULL,
	con_comprobante_e char(1) DEFAULT NULL,
	correlativo int(11) DEFAULT NULL COMMENT 'Por modalidad y día',
	cliente_tipo_documento char(2) DEFAULT NULL,
	cliente_correo varchar(100) DEFAULT NULL,
	canal char(2) NOT NULL DEFAULT 'OT'
);

drop table if exists nube_comanda_item;
create table nube_comanda_item(
	comanda_codigo varchar(25),
    
	comanda_item_id int(11) NOT NULL,
	comanda_id int(11) NOT NULL,
	producto_id int(11) NOT NULL,
	producto varchar(550) NOT NULL,
	manejar_stock int(11) DEFAULT NULL,
	comentario varchar(500) DEFAULT NULL COMMENT 'agregados o comentarios',
	modalidad char(3) NOT NULL COMMENT 'PRE:presencial, DEL:Delivery, LLE:Llevar,',
	para_cocina int(11) NOT NULL,
	precio_unitario decimal(9,2) NOT NULL,
	cantidad int(11) NOT NULL,
	importe decimal(9,2) NOT NULL,
	pedido_en datetime DEFAULT CURRENT_TIMESTAMP,
	cocinero_id int(11) DEFAULT NULL,
	cocinero varchar(50) DEFAULT NULL,
	estado_atencion char(1) NOT NULL DEFAULT 'P' COMMENT 'E:En espera, P:Proceso, D:Despachado, X:Anulado',
	despachado_en datetime DEFAULT NULL,
	despachado_por varchar(50) DEFAULT NULL,
	tiempo_espera int(11) DEFAULT NULL COMMENT 'Hasta ser despachado, en minutos',
	anulado_en datetime DEFAULT NULL,
	anulado_por varchar(50) DEFAULT NULL,
	fecha_registro datetime DEFAULT CURRENT_TIMESTAMP,
	registrado_por varchar(100) DEFAULT NULL,
	fecha_modificacion datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
	modificado_por varchar(100) DEFAULT NULL, 
    primary key(comanda_codigo, comanda_item_id)
)


