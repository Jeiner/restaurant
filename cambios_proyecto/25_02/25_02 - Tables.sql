/* nube_sucursal*/
ALTER TABLE nube_sucursal DROP PRIMARY KEY;
ALTER TABLE nube_sucursal ADD PRIMARY KEY (sucursal_codigo);

ALTER TABLE nube_sucursal
MODIFY COLUMN sucursal_codigo CHAR(6) NOT NULL;

ALTER TABLE nube_sucursal
ADD ip_local VARCHAR(150) DEFAULT NULL;

ALTER TABLE nube_sucursal
ADD lugar VARCHAR(50) NULL,
ADD estado CHAR(1) NOT NULL DEFAULT 'A',
ADD fecha_registro DATETIME DEFAULT CURRENT_TIMESTAMP,
ADD usuario_registro VARCHAR(50) NULL,
ADD fecha_modificacion DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
ADD usuario_modificacion VARCHAR(50) NULL;


/* empleado */
ALTER TABLE empleado
MODIFY COLUMN nro_documento VARCHAR(15) NOT NULL;

ALTER TABLE empleado
ADD sucursal_codigo CHAR(06) NOT NULL;


/* Usuario */
ALTER TABLE usuario
ADD COLUMN empleado_id INT NULL;

ALTER TABLE usuario
ADD COLUMN cambiar_clave BIT NOT NULL DEFAULT 1;

ALTER TABLE usuario
MODIFY COLUMN DNI VARCHAR(25) NOT NULL;


/*Marcaciones*/
ALTER TABLE marcacion
ADD COLUMN empleado_id INT NOT NULL, 
ADD COLUMN foto LONGBLOB  NULL;