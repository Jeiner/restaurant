TRUNCATE TABLE nube_sucursal;

INSERT INTO `nube_sucursal` (`empresa_codigo`, `sucursal_codigo`, `empresa_nombre`, `sucursal_nombre`, `ip_local`) VALUES
('001', '001001', 'Santorini', 'Quintanas', 'http://192.168.1.12:50/santorini_quintanas/salon/salon'),
('001', '001002', 'Santorini', 'Primavera', 'http://192.168.1.13:50/santorini_red/salon/salon'),
('001', '001003', 'Santorini', 'La Merced', 'http://192.168.18.21:50/santorini_red/salon/salon'),
('002', '002001', 'Sanbrasa', 'Quintanas', 'http://192.168.1.117:50/san_brasa_quintanas');

INSERT INTO `opcion` (`opcion_id`, `modulo_id`, `opcion`, `prioridad`, `controlador`, `accion`, `para_seleccionar`, `estado`, `tipo_sistema`) 
VALUES ('50', '1', 'Sucursales', '3', 'sucursales', 'index', 'sucursales', 'A', 'PROD');

INSERT INTO `opcion` (`opcion_id`, `modulo_id`, `opcion`, `prioridad`, `controlador`, `accion`, `para_seleccionar`, `estado`, `tipo_sistema`)
VALUES ('51', '9', 'Calendario pagos', '5', 'calendario_pagos', 'index', 'calendario_pagos', 'A', 'PROD');

/* Usuarios */
UPDATE usuario
INNER JOIN empleado ON usuario.DNI = empleado.nro_documento
SET usuario.empleado_id = empleado.empleado_id
WHERE usuario.empleado_id IS NULL;

