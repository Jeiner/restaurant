/* Actualizar controlador de documentos electronicos */
UPDATE opcion SET 
		opcion = 'Documentos electrónicos',
		controlador = 'documento_electronico',
		accion = 'ver_documentos_electronicos',
        para_seleccionar ='documentos_electronicos' 
        WHERE opcion_id = 31;
COMMIT