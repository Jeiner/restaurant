/*Agregar facturador SUNAT en el menu*/
INSERT INTO OPCION (opcion_id,modulo_id,opcion,icono,prioridad,controlador,accion,para_seleccionar,estado,fecha_registro,registrado_por,tipo_sistema) VALUES
				(32,5,'Facturador SUNAT', '',1,'sunat','facturador','facturador_sunat','A',NOW(),'JHARO','PROD');				
/*Agregar permiso de acceso a administrador para la opcion*/
INSERT INTO acceso (acceso_id, rol_id, modulo_id, opcion_id, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(86, 1, NULL, 32, '2019-03-02 17:57:03', NULL, NULL, NULL);			

/*Agregar tipo multitabla respuesta SUNAR*/
insert into tipo_multitabla (tipo_multitabla_id, tipo_multitabla) values(37,'SUNAT - ESTADO DE DOCUMENTOS ELECTRÓNICOS (IND_SITU)');

/*Agregar multitabla - estado de los documentos electronicos enviados a sunat */
insert into multitabla(tipo_multitabla_id, valor, descripcion, estado) values
					(37,'01','Por generar xml','A'),
                    (37,'02','Xml generado','A'),
                    (37,'03','Enviado y aceptado sunat','A'),
                    (37,'04','Enviado y aceptado sunat con obs.','A'),
                    (37,'05','Rechazado por sunat','A'),
                    (37,'06','Con errores','A'),
                    (37,'07','Por validar xml','A'),
                    (37,'08','Enviado a sunat por procesar','A'),
                    (37,'09','Enviado a sunat procesando','A'),
                    (37,'10','Rechazado por sunat','A'),
                    (37,'11','Enviado y aceptado sunat','A'),
                    (37,'12','Enviado y aceptado sunat con obs','A');
                
COMMIT;

