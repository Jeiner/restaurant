ALTER TABLE caja
ADD fecha_procesar_cpbe DATETIME;

ALTER TABLE variable
MODIFY nombre_variable varchar(50);

ALTER TABLE variable
MODIFY valor_variable varchar(150);


/**/
UPDATE cpbe_cabecera SET estadoSunat = '02' where cabecera_id >= 1 AND YEAR(fecEmision) <= 2022;



/**/
INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('3', 'facsu_generar_xml', 'http://18.188.109.204:50/facsu/sin_sesion/facturacion_electronica/generar_archivos_sunat', 'A', '2023-02-09 00:00:00', 'jharo');

INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('4', 'facsu_procesar_cpbes', 'http://18.188.109.204:50/facsu/sin_sesion/facturacion_electronica/procesar_boletas_facturas', 'A', '2023-02-09 00:00:00', 'jharo');

INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('5', 'sync_facsu_procesar_cpbes', '1', 'A', '2023-03-06', 'jharo');


/**/
INSERT INTO variable (variable_id, nombre_variable, valor_variable, estado, fecha_registro, registrado_por) 
VALUES ('6', 'max_hora_apertura_caja', '15', 'A', '2023-03-06', 'jharo');


/**/
INSERT INTO validacion (validacion_id, nombre_variable, validacion, estado, valor, tipo, fecha_registro, registrado_por) 
VALUES ('016', 'url_enviar_caja_nube', 'Enviar caja a la nube', 0, 'http://santorini.com.pe/administracion/webservice/caja_api/insert_caja', 'URL', NOW(), 'SYS');

