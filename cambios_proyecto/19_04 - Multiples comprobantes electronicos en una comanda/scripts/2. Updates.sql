/*Actualiza la comanda para los nuevos campos de facturación*/
UPDATE comanda c
INNER JOIN pago p ON c.comanda_id = p.comanda_id
SET c.con_comprobante_e = p.con_comprobante_e;

UPDATE comanda set con_comprobante_e = 'C' where con_comprobante_e = '1';


/*Actualizar cpbe_detalle, indicar cabecera_id*/
UPDATE cpbe_detalle d
INNER JOIN cpbe_cabecera c on c.comanda_id = d.comanda_id
set d.cabecera_id = c.cabecera_id;

commit;




