/*Eliminar clave primaria de cabecera*/
/*Para poder manejear multiple documentos electronicos para UNA COMANDA*/
alter table cpbe_cabecera
  drop primary key;
  
/*AGREGAMOS COLUMNA IND AUTOINCREMENT*/
alter table cpbe_cabecera
	add cabecera_id int not null auto_increment primary key;
    
/*COLOCAMOS LA NUEVA COLUMNA AL INICIO (PRIMERO)*/
alter table cpbe_cabecera
	MODIFY cabecera_id int not null auto_increment
	AFTER pago_id;

/*---DETALLE----*/
/*COLOCAMOS la columna comanda_id despues de comanda_item_id*/
alter table cpbe_detalle
	MODIFY comanda_id int 
	AFTER comanda_item_id;

/*Agregar columna cabecera_id*/
alter table cpbe_detalle 
	add cabecera_id int ;

/*COLOCAMOS la columna cabecera_id  despues de comanda_id*/
alter table cpbe_detalle
	MODIFY cabecera_id int 
	AFTER comanda_id;

/*Agregar comprobante electronico en comanda*/
alter table comanda 
	add con_comprobante_e char(1);

/* Eliminar datos de documnto electronico de PAGO*/


commit;

