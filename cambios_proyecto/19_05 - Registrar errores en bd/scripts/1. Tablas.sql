
create table log_error(
	log_error_id int auto_increment not null primary key,
    tipo_error char(02) not null,
    archivo varchar(50) not null,
    mensaje varchar(255) not null,
    
    fecha_registro datetime default CURRENT_TIMESTAMP,
    registrado_por varchar(25) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null

);