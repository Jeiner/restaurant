
ALTER TABLE nube_sucursal
ADD ip_local VARCHAR(100) NOT NULL DEFAULT '';

UPDATE `nube_sucursal` SET `ip_local` = 'http://192.168.1.117/santorini_quintanas' 
WHERE (`empresa_codigo` = '001') and (`sucursal_codigo` = '001');
UPDATE `nube_sucursal` SET `ip_local` = 'http://192.168.1.117/santorini_primavera' 
WHERE (`empresa_codigo` = '001') and (`sucursal_codigo` = '002');
UPDATE `nube_sucursal` SET `ip_local` = 'http://192.168.1.117/sn_brasa_quintanas' 
WHERE (`empresa_codigo` = '002') and (`sucursal_codigo` = '001');

ALTER TABLE nube_comanda
ADD fecha_caja_apertura DATETIME;

ALTER TABLE nube_pago
ADD fecha_caja_apertura DATETIME