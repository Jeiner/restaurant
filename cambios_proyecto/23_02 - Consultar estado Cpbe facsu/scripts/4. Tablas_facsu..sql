DROP TABLE IF EXISTS systvar;
CREATE TABLE IF NOT EXISTS systvar (
  nVarId int(11) NOT NULL AUTO_INCREMENT,
  cNomVar varchar(20) NOT NULL,
  cValVar varchar(50) NOT NULL, /*Valor*/
  cEstado varchar(1) NOT NULL,
  
  tFecReg datetime DEFAULT NULL,
  cUsuReg varchar(20) DEFAULT NULL,
  tFecMod datetime DEFAULT NULL,
  cUsuMod varchar(20) DEFAULT NULL,
  PRIMARY KEY (nVarId),
  UNIQUE KEY nombre_variable (cNomVar)
);

INSERT INTO systvar (nVarId, cNomVar, cValVar, cEstado, tFecReg, cUsuReg) 
VALUES ('1', 'fecha_proceso_cpbe', '2023-01-01', 'A', '2023-03-06', 'JHAROG');