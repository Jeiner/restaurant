ALTER TABLE caja
ADD fecha_procesar_cpbe DATETIME;

ALTER TABLE variable
MODIFY nombre_variable varchar(50);

ALTER TABLE variable
MODIFY valor_variable varchar(150);