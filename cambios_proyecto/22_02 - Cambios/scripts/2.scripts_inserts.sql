
INSERT INTO OPCION (opcion_id, modulo_id, opcion, icono, prioridad, controlador, accion, para_seleccionar, estado, fecha_registro, fecha_modificacion) VALUES 
('33', '1', 'Validaciones', ' ', '5', 'validaciones', 'index', 'validaciones', 'A', NOW(), NOW()),
('34', '1', 'Asistencia de personal', ' ', '2', 'asistencia', 'index', 'asistencia', 'A', NOW(), NOW());

INSERT INTO ACCESO (rol_id, opcion_id, fecha_registro) VALUES
('1', '33', NOW()),
('1', '34', NOW());

INSERT INTO tipo_multitabla (tipo_multitabla_id, tipo_multitabla) VALUES
 ('41', 'TIPO DE VALIDACIÓN (VARIABLE)');

INSERT INTO multitabla (tipo_multitabla_id, valor, descripcion, estado, orden) VALUES
 ('41', 'URL', 'LINK - URL', 'A', '1'),
 ('41', 'CAR', 'CARPETA', 'A', '2'),
 ('41', 'IMG', 'IMAGEN', 'A', '3'),
 ('41', 'TCK', 'IMPRESION TICKETERA', 'A', '3');

INSERT INTO VALIDACION (validacion_id, nombre_variable, validacion, estado, comentario, valor, tipo, fecha_registro, registrado_por) VALUES 
('003', 'carp_reporte_pdf', 'Verificar carpeta para reporte PDF de cpbe', 0, 'Ejem: C:/wamp64/www/nombre_sistema/reportes_pdf/', '/reportes_pdf/', 'CAR', NOW(), 'SYS'),
('004', 'url_facebook_fan_page' ,'URL Fan Page Facebook', 0, '', 'https://www.facebook.com/Pizzeria-Santorini-1500364683510878/', 'URL', NOW(), 'SYS'),
('005', 'url_ver_pedidos_online' ,'URL ver pedidos online', 0, '', 'https://deliveryadmin.santorini.com.pe/administracion/pedidos/', 'URL', NOW(), 'SYS'),
('006', 'url_obtener_nro_pedidos_online' ,'URL obtener nro pedidos online', 0, '', 'https://apidelivery.santorini.com.pe/sinsesion/obtenerNroPedidosPendientes/', 'URL', NOW(), 'SYS'),
('007', 'min_tol_entrada' ,'Minutos de tolerancia para entrada', 0, '', '10', 'NUM', NOW(), 'SYS'),
('008', 'min_tol_salida' ,'Minutos de tolerancia para salida', 0, '', '10', 'NUM', NOW(), 'SYS'),
('009', 'url_imp_tickets', 'URL para la impresión de tickets', 0, '', 'http://localhost:50/ticket/', 'URL', NOW(), 'SYS'),
('010', 'imp_tck_cocina', 'Impresión tickets en COCINA', 0, '../ticket/imprimir_para_cocina.php?comanda_id=1', 'COCINA_02', 'TCK', NOW(), 'SYS'),
('011', 'imp_tck_caja', 'Impresión tickets en CAJA', 0, '../ticket/imprimir_cuenta_local.php?comanda_id=1', 'CAJA_01', 'TCK', NOW(), 'SYS');


INSERT INTO COMENTARIO (comentario_id, familia_id, comentario, orden) VALUES
('1', '8', 'HELADO(A)', '1'),
('2', '8', 'SIN HELAR', '2'),
('3', '8', 'HIELO APARTE', '3'),
('4', '1', 'Para Llevar', '10'),
('5', '1', 'SIN aceitunas', '11'),
('6', '1', 'SIN tomates', '12'),
('7', '1', 'SIN Pimentón', '12'),
('8', '1', 'SIN Champignones', '12'),
('9', '2', 'Para Llevar', '10'),
('10', '2', 'SIN aceitunas', '11'),
('11', '2', 'SIN tomates', '12'),
('12', '2', 'SIN Pimentón', '12'),
('13', '2', 'SIN Champignones', '12'),
('14', '4', 'Para Llevar', '10');
