ALTER TABLE generalidades
ADD token_facturador VARCHAR(200) NULL;

ALTER TABLE generalidades
ADD api_get_pedidos_online VARCHAR(500) NULL;


CREATE TABLE VALIDACION(
	validacion_id CHAR(3) NOT NULL PRIMARY KEY,
    nombre_variable VARCHAR(50) NOT NULL,
    validacion VARCHAR(200) NOT NULL,
    estado BIT NOT NULL,
    comentario VARCHAR(200) NULL,
    valor VARCHAR(100) NOT NULL,
    tipo VARCHAR(3) null,
    
    fecha_registro DATETIME,
	registrado_por VARCHAR(100),
	fecha_modificacion DATETIME, 
	modificado_por VARCHAR(100)
);

CREATE TABLE COMENTARIO(
    comentario_id int not null primary key auto_increment,
    familia_id int null,
    producto_id int null,
    comentario VARCHAR(100) not null, 
    orden int not null
);


ALTER TABLE usuario
ADD id_usuario_asistencia VARCHAR(25) NULL;

ALTER TABLE usuario
ADD ruta_foto VARCHAR(100) NULL;

CREATE TABLE MARCACION(
    marcacion_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    id_usuario_asistencia VARCHAR(20) NOT NULL ,
    nombre VARCHAR(200) NOT NULL,
    departamento VARCHAR(200) NOT NULL,
    fecha DATE NOT NULL,
    hora VARCHAR(20) NOT NULL,
    estado VARCHAR(50) NOT NULL,
    area VARCHAR(50)  NULL,
    serie VARCHAR(100)  NULL,
    dispositivo VARCHAR(100)  NULL,
    carga VARCHAR(100)  NULL
);

CREATE TABLE ARCHIVO_CARGADO(
    archivo_id int not null primary key auto_increment,
    nombre_archivo varchar(200) not null,
    fecha_registro datetime null
);


ALTER TABLE usuario
ADD hora_entrada VARCHAR(10) NULL;

ALTER TABLE usuario
ADD hora_salida VARCHAR(10) NULL;