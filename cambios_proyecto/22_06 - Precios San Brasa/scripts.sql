

UPDATE `san_brasa`.`generalidades` SET `nombre_comercial` = 'SAN BRASA' WHERE (`generalidades_id` = '1');


UPDATE `san_brasa`.`generalidades` SET `empresa_codigo` = '002', `sucursal_codigo` = '001' WHERE (`generalidades_id` = '1');

INSERT INTO `producto` (`producto_id`, `producto`, `unidad`, `precio_costo`, `precio_unitario`, `para_cocina`, `manejar_stock`, `stock`, `stock_minimo`, `stock_maximo`, `fecha_vencimiento`, `familia_id`, `foto`, `codigo_barras`, `descripcion`, `estado`, `fecha_registro`, `registrado_por`, `fecha_modificacion`, `modificado_por`, `ubi_carta`) VALUES
(68, 'ALITAS BROASTER FAMILIAR', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '15 alitas, Papas fritas, ensalada fresca cremas', 'A', '2022-10-18 00:24:55', NULL, NULL, NULL, '02020102'),
(69, 'ALITAS BROASTER PERSONAL', 'und', NULL, '18.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '15 alitas, Papas fritas, ensalada fresca cremas', 'A', '2022-10-18 00:25:28', NULL, NULL, NULL, '02020202');
COMMIT;

ALTER TABLE producto
ADD precio_unitario_anterior DECIMAL(9,2);

UPDATE producto SET precio_unitario_anterior = precio_unitario where producto_id > 0;



update producto set precio_unitario = '57' , fecha_modificacion = '20221018' where producto_id = '1';
update producto set precio_unitario = '32' , fecha_modificacion = '20221018' where producto_id = '2';
update producto set precio_unitario = '19' , fecha_modificacion = '20221018' where producto_id = '3';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '4';
update producto set precio_unitario = '20' , fecha_modificacion = '20221018' where producto_id = '5';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '6';
update producto set precio_unitario = '23' , fecha_modificacion = '20221018' where producto_id = '7';
update producto set precio_unitario = '23' , fecha_modificacion = '20221018' where producto_id = '8';
update producto set precio_unitario = '23' , fecha_modificacion = '20221018' where producto_id = '9';
update producto set precio_unitario = '23' , fecha_modificacion = '20221018' where producto_id = '10';
update producto set precio_unitario = '21' , fecha_modificacion = '20221018' where producto_id = '11';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '12';
update producto set precio_unitario = '14' , fecha_modificacion = '20221018' where producto_id = '18';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '19';
update producto set precio_unitario = '11' , fecha_modificacion = '20221018' where producto_id = '20';
update producto set precio_unitario = '15' , fecha_modificacion = '20221018' where producto_id = '21';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '23';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '24';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '25';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '26';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '27';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '28';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '29';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '30';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '31';
update producto set precio_unitario = '16' , fecha_modificacion = '20221018' where producto_id = '32';
update producto set precio_unitario = '11' , fecha_modificacion = '20221018' where producto_id = '33';
update producto set precio_unitario = '17' , fecha_modificacion = '20221018' where producto_id = '34';
update producto set precio_unitario = '4' , fecha_modificacion = '20221018' where producto_id = '44';
update producto set precio_unitario = '5' , fecha_modificacion = '20221018' where producto_id = '45';
update producto set precio_unitario = '6' , fecha_modificacion = '20221018' where producto_id = '46';
update producto set precio_unitario = '8' , fecha_modificacion = '20221018' where producto_id = '47';
update producto set precio_unitario = '10' , fecha_modificacion = '20221018' where producto_id = '48';
