CREATE TABLE nube_trama(
    trama_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    empresa_codigo VARCHAR(3) NOT NULL,
    sucursal_codigo VARCHAR(3) NOT NULL,
    fecha_registro_nube DATETIME NOT NULL,
    tipo_trama VARCHAR(50) NOT NULL,
    trama TEXT NOT NULL
);

ALTER TABLE nube_cpbe_detalle
DROP PRIMARY KEY;