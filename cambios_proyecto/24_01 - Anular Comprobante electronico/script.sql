
CREATE TABLE IF NOT EXISTS cpbe_baja (
  baja_id INT NOT NULL primary key auto_increment,
  comanda_id INT NOT NULL,
  fecGeneracion varchar(10) not null,
  fecComunicacion varchar(10) not null,
  tipDocBaja varchar(5) not null,
  numDocBaja varchar(20) not null,
  desMotivoBaja varchar(100) not null,
  estadoSunat varchar(2)  null,

  estado char(1) NOT NULL COMMENT 'A:ACTIVO, X:ANULADO',
  fecha_registro datetime ,
  registrado_por varchar(100) DEFAULT NULL,
  fecha_modificacion datetime DEFAULT NULL,
  modificado_por varchar(100) DEFAULT NULL
)