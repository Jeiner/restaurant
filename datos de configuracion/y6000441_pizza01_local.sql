/*

drop database y6000441_pizza01;
create database y6000441_pizza01;
use y6000441_pizza01;

*/

create table tipo_multitabla(
	tipo_multitabla_id int not null auto_increment primary key,
    tipo_multitabla varchar(100) null
);
create table multitabla(
	multitabla_id int not null auto_increment primary key,
    tipo_multitabla_id int not null references tipo_multitabla(tipo_multitabla_id),
    valor varchar(25) not null,
    descripcion  varchar(150) not null,
    estado char(1) not null  default 'A' comment 'A:activo, X:Inactivo'
);

/*	drop table	generalidades	*/
create table generalidades(
	generalidades_id int not null auto_increment primary key,
	RUC char(11)  null,
    razon_social  varchar(200) null,
    nombre_comercial varchar(200) null,
    direccion varchar(250) null,
    ubigeo varchar(250) null,
    contacto varchar(250) null,
    telefono_1 varchar(100) null,
    telefono_2 varchar(100) null,
    correo varchar(150) null,
    logo  varchar(100) null,
    fecha_instalacion varchar(100) null,
    codigo_activacion  varchar(100) null
);
create table familia(
/*		drop table mesa 		*/
	familia_id int not null auto_increment primary key,
    familia varchar(200)  not null,
	nro_productos int null default 0,
    imagen varchar(250) null,
    estado char(1) not null  default 'A' comment 'A:activo, X:Inactivo',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/* 	select * from familia*/
create table mesa(
	mesa_id int not null auto_increment primary key,
    mesa varchar(150) null unique,
    capacidad int null,
    estado char(1) not null default 'L' comment 'L:Libre, O:Ocupada, A:Atendida P: Por salir',
    color varchar(10) null, 
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*	select * from  mesa 	*/
create table proveedor(
	proveedor_id int not null auto_increment primary key,
    RUC varchar(11) null unique,
    razon_social varchar(100) null,
    direccion varchar(150) null,
    contacto varchar(100) null,
    telefono varchar(25) null,
    correo varchar(60) null,
    productos varchar(255) null,
    estado char(1) not null default 'A' comment 'A:activo, X:Inactivo',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*select * from  proveedor*/
/*		drop table insumo		*/
/*		select * from insumo 	*/
/*select * from multitabla*/
create table insumo(
	insumo_id int not null auto_increment primary key,
    insumo varchar(255) not null,
    unidad varchar(10) not null,
    precio_costo decimal(9,4) null,
    stock  decimal(9,4) null,
    stock_minimo decimal(9,4) null,
    stock_maximo decimal(9,4) null,
    fecha_vencimiento date null,
    descripcion varchar(255)  null,
    estado char(1) not null default 'A' comment 'A:activo, X:Inactivo',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*select * from  proveedor*/
/*		drop table producto		*/
/*		select * from producto 	*/
create table producto(
	producto_id int not null auto_increment primary key,
    producto varchar(255) not null unique,
    unidad varchar(10) not null,
    /*insumo int not null default 0 comment '1:Si, 0:NO', */
    precio_costo decimal(9,2) null,
    precio_unitario decimal(9,2) null,
    para_cocina int null,
    /*unidades varchar(100) null,*/
    manejar_stock int not null,
    stock int null,
    stock_minimo int null,
    stock_maximo int null,
    fecha_vencimiento date null,
    familia_id int not null references familia(familia_id),
    /*proveedor_id int  null references proveedor(proveedor_id),    */
    foto varchar(8000)  null,
    codigo_barras varchar(255)  null,
    descripcion varchar(255)  null,
    estado char(1) not null default 'A' comment 'A:activo, X:Inactivo',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*		drop table insumos			*/
create table insumos(
	producto_id int not null references producto(producto_id),
    insumo_id int not null references insumo(insumo_id),
    cantidad decimal(9,3) not null,
    unidad varchar(10) null,
    primary key(producto_id, insumo_id),
    
	fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*ALTER TABLE producto ADD INDEX indice_producto_id (producto_id);*/
/*CREATE INDEX indice_familia_prod ON producto(familia_id);*/
/*  	drop table cliente 	*/
create table cliente(
	cliente_id int not null auto_increment primary key,
    tipo_cliente char(01) not null comment 'N:natural, J:Juridico',
    documento varchar(11) null unique,
    ape_paterno varchar(100)  null,
    ape_materno varchar(100)  null,
    nombres varchar(100)  null,
    nombre_completo varchar(200) not null comment'Razon social, si es jurídico',
    fecha_nacimiento date null,
    sexo char(1) null comment 'M:masculino, F:Femenino',
    contacto varchar(100) null,
    telefono varchar(50) null,
    correo varchar(100) null,
    direccion varchar(250) null,
    estado char(1) not null default 'A' comment 'A:activo, X:Inactivo',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);

/*		drop table caja 	*/
create table caja(
	caja_id int not null auto_increment primary key,
    aperturado_en datetime not null,
    aperturado_por varchar(150) not null,
    monto_inicial decimal(9,2) not null,
    cerrado_en  datetime null,
    cerrado_por varchar(150)  null,
    
    total_ventas_efectivo decimal(9,2)  null,
    total_gastos_efectivo decimal(9,2)  null,
    saldo_caja decimal(9,2)  null,
    total_ventas_tarjeta decimal(9,2)  null,
    total_gastos_tarjeta decimal(9,2)  null,
    
    
    monto_retirado decimal(9,2)  null,
    remanente decimal(9,2)  null,
    observaciones varchar(255)  null,
    estado char(1) not null  default 'A' comment 'A:Aperturada, C:Cerrada',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*		select * from producto	*/
/*delete from producto where producto_id >= 7*/
/*		drop table comanda		*/
/*		select * from  comanda		*/
create table comanda(
	comanda_id int not null auto_increment primary key,
    modalidad char(03) not null  comment 'PRE:presencial, DEL:Delivery, LLE:Llevar,',
    fecha_comanda datetime not null,
    mozo varchar(100) null,
    mesa_id int  null references mesa(mesa_id),
    mesa varchar(50) null,
    nro_comensales int null,
    prioridad int null,
    nro_productos int null,
    /*PAGO*/
    pago_id int null,
    importe_total decimal(9,2) null,
    descuento decimal(9,2) null,
    total_pagado decimal(9,2) null,
    fecha_pago datetime null,
    pagado_por varchar(50) null,
    tipo_comprobante char(1) null comment 'B:Boleta, F:Factura',
    cliente_id int null references cliente(cliente_id),
    cliente_documento varchar(11) null,
    cliente_nombre varchar(250) null,
    cliente_direccion varchar(250) null,
    cliente_telefono varchar(250) null,
    /**/
    union_mesas int not null default 0,
    caja_id int  not null references caja(caja_id),
    estado_atencion char(1) not null default 'E' comment 'E:En espera, A:Atendido, F:Finalizado, X:Anulado',
    estado_pago char(1) not null default 'P' comment 'P:Pendiente,  C:Cancelado, X:Anulado',
    
    atendida_en datetime null,
    prefacturada_en datetime null,
    prefacturada_por varchar(50) null,
    finalizada_en datetime null,
    finalizada_por varchar(50) null,
    anulado_en datetime null,
    anulado_por varchar(50) null,
    anulado_motivo varchar(500) null,
    
    fecha_registro datetime  null /*default CURRENT_TIMESTAMP*/,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null /*ON UPDATE CURRENT_TIMESTAMP*/,
    modificado_por varchar(100) null
);
/*		drop table comanda_item		*/
/*		select * from  caja		*/
create table comanda_item(
	comanda_item_id int not null auto_increment primary key,
	comanda_id int not null references comanda(comanda_id),
    producto_id int not null references producto(producto_id),
    producto varchar(550) not null,
    comentario varchar(500) null comment 'agregados o comentarios', 
    modalidad char(03) not null comment 'PRE:presencial, DEL:Delivery, LLE:Llevar,',
    para_cocina int not null, /*Obtenida del producto*/
    precio_unitario decimal(9,2) not null,/*Obtenida del producto*/
    cantidad int not null,
    importe decimal(9,2) not null,
    pedido_en datetime null default CURRENT_TIMESTAMP,
    cocinero_id int null references usuario(usuario),
    cocinero varchar(50) null,
    estado_atencion char(1) not null  default 'P' comment 'E:En espera, P:Proceso, D:Despachado, X:Anulado',
    
    despachado_en datetime null,
    despachado_por varchar(50) null,
    tiempo_espera int null comment 'Hasta ser despachado, en minutos', 
    
    anulado_en datetime null,
    anulado_por varchar(50) null,
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*		drop table pago		*/
create table pago(
	pago_id int not null auto_increment primary key,
    comanda_id int not null references comanda(comanda_id),
    importe_total decimal(9,2) not null,
    descuento decimal(9,2) not null,
    total_a_pagar decimal(9,2) not null,
    pago_efectivo decimal(9,2) not null,
    pago_tarjeta decimal(9,2) not null,
    total_pagado decimal(9,2) not null,
    saldo decimal(9,2) not null,
    efectivo decimal(9,2) not null comment 'cantidad con la que pago', 
    vuelto decimal(9,2) not null comment 'vuelto',
    fecha_pago datetime not null,
    cajero varchar(150) null,
    tipo_comprobante char(1) null comment 'B:Boleta, F:Factura',
    cliente_id int null references cliente(cliente_id),
    cliente_documento varchar(11) null,
    cliente_nombre varchar(250) null,
    cliente_direccion varchar(250) null,
    cliente_telefono varchar(250) null,
    caja_id int not null references caja(caja_id),
    observaciones varchar(250) null,
    estado char(1) not null comment 'A:Activa, X:Anulada',
    
    
    anulado_en datetime null ,
    anulado_por varchar(100) null,
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*update comanda_item set estado_atencion = 'P' where comanda_id = 1 and producto_id= 707*/
/*		SELECT * FROM mesa WHERE 	*/
/*		drop table gasto		*/
/*		select * from  gasto		*/
create table gasto(
	gasto_id int not null auto_increment primary key,
    fecha datetime not null,
    usuario varchar(150) null,
	caja_id int not null,
    monto decimal(9,2) not null,
    tipo char(05) not null comment 'tipo multi: 2', /*tIPO DE GASTO*/
    descripcion varchar(250)  null,
    estado char(1) not null default 'A' comment 'A:activo, X:Anulado',
    
    anulado_en datetime null default CURRENT_TIMESTAMP,
    anulado_por varchar(100) null,
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);

/*		drop table rol_usuario			*/
create table rol(
	rol_id int not null auto_increment primary key,
    rol varchar(100) not null,
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);

/*		drop table usuario			*/
create table usuario(
	usuario_id int not null auto_increment primary key,
    usuario varchar(100) not null unique,
    clave varchar(100) not null,
    rol_id int not null references rol(rol_id),
    DNI varchar(8) null,
    nombres varchar(100) null,
    ape_paterno varchar(100) null,
    ape_materno varchar(100) null,
    nombre_completo varchar(250) null,
    fecha_nacimiento date null,
    sexo char(1) null,
    telefono varchar(25) null,
    correo varchar(100) null,
    direccion varchar(200) null,
    codigo_activacion varchar(50) null,
    estado char(1) not null default 'A' comment 'A:activo, X:Eliminado',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);

/*
	drop table modulo
*/
create table modulo(
	modulo_id  int not null auto_increment primary key,
    modulo varchar(150) null,
    icono varchar(150) null,
    prioridad int not null,    
    para_seleccionar varchar(50) null,
    estado char(1) not null default 'A' comment 'A:activo, X:Eliminado',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);
/*
	drop table opcion
*/
create table opcion(
	opcion_id  int not null auto_increment primary key,
    modulo_id int not null references modulo(modulo_id),
    opcion varchar(150) null,
    icono varchar(150) null,
    prioridad int not null,
    controlador varchar(150) null,
    accion varchar(150) null,
    para_seleccionar varchar(50) null,
    estado char(1) not null default 'A' comment 'A:activo, X:Eliminado',
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);

create table acceso(
	acceso_id  int not null auto_increment primary key,
    rol_id int not null references rol(rol_id),
    modulo_id int   references modulo(modulo_id),
    opcion_id int   references opcion(opcion_id),
    
    fecha_registro datetime null default CURRENT_TIMESTAMP,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null ON UPDATE CURRENT_TIMESTAMP,
    modificado_por varchar(100) null
);

/*
	drop table union_mesa
*/
create table union_mesa(
	union_id int not null auto_increment primary key,
	comanda_id int not null references comanda(comanda_id),
	mesa_principal_id int not null references mesa(mesa_id),
    mesa_principal varchar(50) null,
    mesa_unida_id int not null references mesa(mesa_id),
    caja_id int not null references caja(caja_id),
    /*	Estado igual que la mesa principal	*/
    estado char(1)
);

create table ingreso(
	ingreso_id  int not null auto_increment primary key,
    fecha_ingreso  datetime not null,
    motivo varchar(4) not null comment 'COMP:Compra, ACTU:Actualización',
    usuario  varchar(100) null,
    nro_productos  int null,
    tasa_IGV decimal(9,2) null,
    importe_sin_IGV decimal(9,2) null,
    total_IGV decimal(9,2) null,
    importe_total decimal(9,2) null,
    
    proveedor_id int null references proveedor(proveedor_id),
    tipo_comprobante  char(1) null comment 'B:Boleta, F:Factura',
    nro_comprobante varchar(20) null,
    estado_pago char(1) not null default 'P' comment 'P:Pendiente,  C:Cancelado, X:Anulado',
    
    observaciones varchar(250) null,
    anulado_en datetime null,
    anulado_por varchar(50) null,
    anulado_motivo varchar(500) null,
    
    fecha_registro datetime  null /*default CURRENT_TIMESTAMP*/,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null /*ON UPDATE CURRENT_TIMESTAMP*/,
    modificado_por varchar(100) null
);

create table ingreso_item(
	ingreso_detalle_id int not null auto_increment primary key,
	ingreso_id int not null references ingreso(ingreso_id),
    producto_id int not null references producto(producto_id),
    insumo_id int not null references insumo(insumo_id),
    producto_insumo varchar(250) null comment 'Producto o insumo',
    cantidad decimal(9,2) not null,
    precio_costo decimal(9,2) not null,
    importe decimal(9,2) not null,
    fecha_vencimiento date null,
    comentario varchar(150) null,
    estado char(2) null comment 'ACT:Activo, AGO:Agotado, ANU:Anulado',
        
    fecha_registro datetime  null /*default CURRENT_TIMESTAMP*/,
    registrado_por varchar(100) null,
    fecha_modificacion datetime null /*ON UPDATE CURRENT_TIMESTAMP*/,
    modificado_por varchar(100) null
);

