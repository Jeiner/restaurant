/*Se creó esta vista ya que en las compras puedo seleccionar un producto o un insumo*/
drop view IF EXISTS  view_productos_insumos;
create view view_productos_insumos
as
select 
	'Insumo' as tipo,
	i.insumo_id as 'insumo_id',
    null as 'producto_id',
    i.insumo as 'producto_insumo',
    m.descripcion as 'unidad_desc',
    i.precio_costo
    from insumo i
    left join multitabla m on m.valor = i.unidad and m.tipo_multitabla_id = 1
union
select 
	'Producto' as tipo,
	null as 'insumo_id',
	p.producto_id as 'producto_id',
    p.producto as 'producto_insumo',
    m.descripcion as 'unidad_desc',
    p.precio_costo 
    from  producto p 
    left join multitabla m on m.valor = p.unidad and m.tipo_multitabla_id = 1
    where p.manejar_stock = 1

/**select *  from view_productos_insumos*/


