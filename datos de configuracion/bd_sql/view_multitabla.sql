/*
	drop view view_multitabla
*/
create view view_multitabla
as
	select 
		m.multitabla_id,
		tm.tipo_multitabla,
		m.valor,
        m.descripcion,
        m.estado
		from multitabla m 
        inner join tipo_multitabla tm on tm.tipo_multitabla_id = m.tipo_multitabla_id 
        order by m.tipo_multitabla_id,m.multitabla_id  desc
