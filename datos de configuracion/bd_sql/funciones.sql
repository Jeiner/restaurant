/*TOTAL DE VENTAS*/
select
	sum(importe_total) as 'total_ventas',
    sum(descuento) as 'total_descuento',
    caja_id
    from comanda 
    where caja_id = 1 and estado_atencion <> 'X'
/* COMSUMO PROMEDIO POR MESA */
select 
	sum(importe_total) as 'importe_total',
	count(*) as'nro_comandas',
	sum(importe_total) / count(*) as 'promedio'
	from comanda
	where caja_id = 1 and mesa_id is  not null and estado_atencion <> 'X'
/*	MOZO DEL DIA	*/
select 
	mozo,
    count(*) as 'nro_comandas',
    sum(importe_total)
	from comanda 
    where caja_id = 1 and estado_atencion <> 'X'
    group by mozo
    order by nro_comandas desc
    limit 1
/*	VENTAS ANULADAS	*/
select 
	count(*) as 'nro_anuladas'
	from comanda 
    where caja_id = 1 and estado_atencion = 'X'
    
/* TEIMPO PROMEDIO DE ATENCIONES */
select
	AVG(TIMESTAMPDIFF(MINUTE, fecha_comanda, finalizada_en)) AS 'tiempo_prom_atencion'
	from comanda
     where caja_id = 1 and estado_atencion = 'X'
     
select * from caja
    