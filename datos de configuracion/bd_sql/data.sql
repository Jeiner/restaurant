INSERT INTO generalidades (generalidades_id, RUC, razon_social, actividad, slogan, nombre_comercial, local, direccion, ubigeo, contacto, telefono_1, telefono_2, correo, logo, fecha_instalacion, codigo_activacion, codigo_seguridad) VALUES
(1, '20481200670', 'SANTORINI E.I.R.L', 'Pizzas y comida italiana', 'Verdadero sabor artesano',NULL,'Trujillo - Las Quintanas' ,'Av. Carlos Valderrama N°.575 Las Quintanas', 'Trujillo - Trujillo - La Libertad', '', '044 - 291718', '', '', NULL, NULL, NULL,md5('santorini'));
INSERT INTO proveedor (proveedor_id, RUC, razon_social, direccion, contacto, telefono, correo, productos, estado, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, '10477908158', 'JUAN JEINY HARO GUTIERREZ', 'HUERTA BELLA MZ F\' LT 05 LA LIBERTAD - TRUJILO - TRUJILLO', 'JUAN HARO GUTIERREZ', '987050724', 'jeiner.24@gmail.com', 'Desarrollo, implementación, capacitación y asesoramiento de componentes de software.', 'A', '2018-06-11 21:44:59', NULL, '2018-06-14 20:13:42', NULL);

INSERT INTO caja (caja_id, aperturado_en, aperturado_por, monto_inicial, cerrado_en, cerrado_por, total_ventas_efectivo, total_gastos_efectivo, saldo_caja, total_ventas_tarjeta, total_gastos_tarjeta, monto_retirado, remanente, observaciones, estado, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, NOW(), 'jharo', '0.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', '2018-06-20 16:14:02', NULL, NULL, NULL);

insert into numeracion(tipo_comprobante,serie, correlativo, correlativo_num ) values
('F', 'F001', '00001',1),
('B', 'B001', '00001',1),
('TCK', 'T001', '00001',1);

    
insert into modulo(modulo_id, modulo,icono, prioridad, para_seleccionar) values
	(1,'Configuración','<i class="menu-icon fa fa-cog"></i>',10,'configuracion'),
    (2,'Administración','<i class="menu-icon fa fa-desktop"></i>',9,'administracion'),
    (3,'Cocina','<i class="menu-icon fa fa-cutlery"></i>',8,'cocina'),
    (4,'Salón','<i class="menu-icon fa fa-star"></i>',7,'salon'),
    (5,'Facturación','<i class="menu-icon fa fa-calculator"></i>',6,'facturacion'),
    (6,'Caja','<i class="menu-icon fa fa-lock"></i>',5,'caja'),
    (7,'Inventario','<i class="menu-icon fa fa-cart-plus"></i>',4,'inventario'),
    (8,'Reportes','<i class="menu-icon fa fa-bar-chart"></i>',3,'reportes');
    
insert into opcion(opcion_id,modulo_id, opcion, icono, prioridad, controlador, accion,para_seleccionar)values
	(1,1,'Datos de la empresa','',4,'generalidades','index','generalidades'),
    (2,1,'Roles de usuarios','',3,'roles','asignar_accesos','asignar_accesos'),
    (3,1,'Usuarios','',2,'usuarios','','usuarios'),
    (4,1,'Familias de productos','',1,'familias','','familias'),
    (5,2,'Mesas','',6,'mesas','','mesas'),
    (6,2,'Productos','',5,'productos','','productos'),
    (7,2,'Insumos','',4,'insumos','','insumos'),
    (8,2,'Clientes','',3,'clientes','','clientes'),
    (9,2,'Proveedores','',2,'proveedores','','proveedores'),    
    (10,3,'Pedidos','',5,'cocina','','pedidos'),    
    (11,3,'Pedidos del día','',2,'cocina','pedidos_del_dia','pedidos_del_dia'),
    (12,4,'Salón','',6,'salon','','salon'),
    (13,4,'Historial de comandas','',5,'salon','historial_comandas','historial_comandas'),
    (14,5,'Facturar comandas','',4,'pagos','comandas','facturar_comandas'),
    (15,5,'Comandas facturadas','',3,'pagos','comandas_facturadas','comandas_facturadas'),
    (16,6,'Apertura/Cierre','',6,'caja','apertura_cierre','apertura_cierre'),
    (17,6,'Historial de cierres','',5,'caja','historial_cierres','historial_cierres'),
    (18,7,'Historial de ingresos','',5,'ingresos','historial','historial_ingresos'),
    (19,7,'Nuevo ingreso','',4,'ingresos','nuevo','nuevo_ingreso'),
    (20,7,'Productos por agotar ','',2,'inventario','productos_por_agotar','productos_por_agotar'),
    (21,7,'Productos por vencer','',2,'inventario','productos_por_vencer','productos_por_vencer'),
    
    (22,8,'Histórico de ventas','',4,'admin','reporte_ventas_mensual','reporte_ventas'),
    (23,8,'Ranking de productos','',3,'admin','ranking_productos','ranking_productos'),
    (24,8,'Ranking de mozos','',2,'admin','ranking_mozos_mensual','ranking_mozos');
    
    
    
    
        
insert into rol(rol_id, rol) values
	(1,'ADMINISTRADOR'),
    (2,'CAJERO'),
    (3,'COCINERO'),
    (4,'MOZO');
    
INSERT INTO `usuario` (`usuario_id`,`usuario`, `clave`, `rol_id`, `DNI`, `nombres`, `ape_paterno`, `ape_materno`, `nombre_completo`, `fecha_nacimiento`, `sexo`, `telefono`, `correo`, `direccion`, `codigo_activacion`, `estado`, `fecha_registro`, `registrado_por`, `fecha_modificacion`, `modificado_por`) VALUES
(1,'jharo', md5('Jeiner'), 1, '47790815', 'JUAN JEINY', 'HARO', 'GUTIERREZ', 'HARO GUTIERREZ, JUAN JEINY', NULL, NULL, '987 050 724', 'jeiner.24@gmail.com', 'Huerta Bella Mz F\' Lt. 05', NULL, 'A', '2018-05-18 01:42:10', NULL, NULL, NULL),
(2, 'scarrion', '81dc9bdb52d04dc20036dbd8313ed055', 1, '26957220', '', '', '', 'CARRION CRUZ, SANTIAGO', NULL, 'M', '', '', NULL, NULL, 'A', '2018-06-01 18:51:00', NULL, NULL, NULL),
(3, 'cocina', '81dc9bdb52d04dc20036dbd8313ed055', 3, '11111111', '', '', '', 'Cocina', NULL, 'M', '', '', NULL, NULL, 'A', '2018-06-04 19:17:21', NULL, '2018-06-04 19:20:29', NULL),
(4, 'romario', '81dc9bdb52d04dc20036dbd8313ed055', 4, '74235162', '', '', '', 'DOMINGUEZ GOMEZ, ROMARIO', NULL, 'M', '', '', NULL, NULL, 'A', '2018-06-04 20:11:35', NULL, NULL, NULL),
(5, 'vmantilla', '1f06e1900a930387a5376eb1d3d65746', 1, '73274533', NULL, NULL, NULL, 'MANTILLA CHAVEZ, VICTOR ENRIQUE', NULL, 'M', '', '', NULL, NULL, 'A', '2018-06-20 22:22:18', NULL, NULL, NULL);
    
insert into tipo_multitabla(tipo_multitabla_id, tipo_multitabla) values
	(1,'UNIDADES'),
    (2,'TIPO DE GASTO'),
    (3,'MODALIDAD DE COMANDA'),
    (4,'ESTADO ATENCION COMANDA'),
    (5,'ESTADO PAGO COMANDA'),
    (6,'MOTIVO DE INGRESO'),
    (7,'TIPO DE COMPROBANTE');

insert into multitabla(multitabla_id, tipo_multitabla_id, valor, descripcion) values
	(1,1,'und','UNIDADES'),
	(2,1,'g','GRAMOS'),
    (3,1,'Kg','KILOGRAMOS'),
    (4,2,'EFEC','EFECTIVO'),
    (5,2,'TARJ','TARJETA'),
    (6,3,'PRE','PRESENCIAL'),
    (7,3,'DEL','DELIVERY'),
    (8,3,'LLE','PARA LLEVAR'),
    
    (9,4,'E','EN ESPERA'),
    (10,4,'A','AENDIDO'),
    (11,4,'F','FINALIZADO'),
    (12,4,'X','ANULADO'),
    
    (13,5,'P','PENDIENTE'),
    (14,5,'C','CANCELADO'),
    (15,5,'X','ANULADO'),
    (16,6,'COMP','COMPRA'),
    (17,6,'ACTU','ACTUALIZACION'),
    
    (18,7,'F','FACTURA'),
    (19,7,'B','BOLETA'),
    (20,7,'TCK','TICKET'),
    
    (21,7,'O','OTRO');
  
insert into mesa(mesa_id,mesa,capacidad,estado) values
	(1,'M-01',3,'L'),
    (2,'M-02',3,'L'),
    (3,'M-03',3,'L'),
    (4,'M-04',3,'L'),
    (5,'M-05',3,'L'),
    (6,'M-06',3,'L'),
    (7,'M-07',3,'L'),
    (8,'M-08',3,'L'),
    (9,'M-09',3,'L'),
    (10,'M-10',3,'L'),
    (11,'M-11',3,'L'),
    (12,'M-12',3,'L'),
    (13,'M-13',3,'L');
    

/*select * from familia*/
insert into familia(familia_id, familia) values
	(1,'PIZZAS'),
    (2,'PIZZAS DE UN GUSTO'),
    (3,'AGREGADOS'),
    (4,'PASTAS'),
    (5,'SANGRÍAS'),
    (6,'VINOS'),
    (7,'LICORES'),
    (8,'BEBIDAS'),
    (9,'JUGOS'),
    (10,'INFUSIONES');
/*		select * from tipo_multitabla		*/





INSERT INTO producto (producto_id, producto, unidad, precio_costo, precio_unitario, para_cocina, manejar_stock, stock, stock_minimo, stock_maximo, fecha_vencimiento, familia_id, foto, codigo_barras, descripcion, estado, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 'ESPECIAL SANTORINI (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, salame, chorizo, tocino, aceitunas, pimentón.', 'A', '2018-05-15 21:13:13', NULL, '2018-05-18 18:26:58', NULL),
(2, 'ESPECIAL SANTORINI (Familiar)', 'und', NULL, '47.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, salame, chorizo, tocino, aceitunas, pimentón.', 'A', '2018-05-15 21:14:08', NULL, '2018-05-18 18:26:58', NULL),
(3, 'ESPECIAL SANTORINI (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, salame, chorizo, tocino, aceitunas, pimentón.', 'A', '2018-05-15 21:14:29', NULL, '2018-05-18 18:26:58', NULL),
(4, 'SANTORINI (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, pepperoni, tocino, pimentón.', 'A', '2018-05-15 21:17:46', NULL, '2018-05-18 18:26:58', NULL),
(5, 'SANTORINI (Familiar)', 'und', NULL, '47.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, pepperoni, tocino, pimentón.', 'A', '2018-05-15 21:18:11', NULL, '2018-05-18 18:26:58', NULL),
(6, 'SANTORINI (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, pepperoni, tocino, pimentón.', 'A', '2018-05-15 21:18:53', NULL, '2018-05-18 18:26:58', NULL),
(7, 'CAMPESINA (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, pimentón verde, espárrago, albahaca, aceitunas verdes.', 'A', '2018-05-15 21:19:56', NULL, '2018-05-18 18:26:58', NULL),
(8, 'CAMPESINA (Familiar)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, pimentón verde, espárrago, albahaca, aceitunas verdes.', 'A', '2018-05-15 21:20:25', NULL, '2018-05-18 18:26:58', NULL),
(9, 'CAMPESINA (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, pimentón verde, espárrago, albahaca, aceitunas verdes.', 'A', '2018-05-15 21:20:39', NULL, '2018-05-18 18:26:58', NULL),
(10, 'SANTORINI CHEESE (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, champignones, chorizo, tocino, aceitunas y extra de queso.', 'A', '2018-05-15 21:22:06', NULL, '2018-05-18 18:26:58', NULL),
(11, 'SANTORINI CHEESE (Familiar)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, champignones, chorizo, tocino, aceitunas y extra de queso.', 'A', '2018-05-15 21:22:39', NULL, '2018-05-18 18:26:58', NULL),
(12, 'SANTORINI CHEESE (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, champignones, chorizo, tocino, aceitunas y extra de queso.', 'A', '2018-05-15 21:23:25', NULL, '2018-05-18 18:26:58', NULL),
(13, 'SALSICCIE (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, salame, chorizo, tocino, cabanossi.', 'A', '2018-05-15 23:07:43', NULL, '2018-05-18 18:26:58', NULL),
(14, 'SALSICCIE (Familiar)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, salame, chorizo, tocino, cabanossi.', 'A', '2018-05-15 23:08:17', NULL, '2018-05-18 18:26:58', NULL),
(15, 'SALSICCIE (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, salame, chorizo, tocino, cabanossi.', 'A', '2018-05-15 23:08:50', NULL, '2018-05-18 18:26:58', NULL),
(16, 'BERNARDINA (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, salame, chorizo, cabanossi, pimentón. ', 'A', '2018-05-15 23:09:53', NULL, '2018-05-18 18:26:58', NULL),
(17, 'BERNARDINA (Familiar)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, salame, chorizo, cabanossi, pimentón. ', 'A', '2018-05-15 23:10:29', NULL, '2018-05-18 18:26:58', NULL),
(18, 'BERNARDINA (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, salame, chorizo, cabanossi, pimentón. ', 'A', '2018-05-15 23:10:52', NULL, '2018-05-18 18:26:58', NULL),
(19, 'PARMESANA (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pepperoni, albahaca, queso parmesano.', 'A', '2018-05-15 23:11:27', NULL, '2018-05-18 18:26:58', NULL),
(20, 'PARMESANA (Familiar)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pepperoni, albahaca, queso parmesano.', 'A', '2018-05-15 23:11:49', NULL, '2018-05-18 18:26:58', NULL),
(21, 'PARMESANA (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pepperoni, albahaca, queso parmesano.', 'A', '2018-05-15 23:12:10', NULL, '2018-05-18 18:26:58', NULL),
(22, 'LA MIXTA (Mediana)', 'und', NULL, '27.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, salame, chorizo, tocino, aceitunas, espárragos, piña, durazno.', 'A', '2018-05-15 23:14:31', NULL, '2018-05-18 18:26:58', NULL),
(23, 'LA MIXTA (Familiar)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, salame, chorizo, tocino, aceitunas, espárragos, piña, durazno.', 'A', '2018-05-15 23:14:37', NULL, '2018-05-18 18:26:58', NULL),
(24, 'LA MIXTA (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, champignones, salame, chorizo, tocino, aceitunas, espárragos, piña, durazno.', 'A', '2018-05-15 23:14:42', NULL, '2018-05-18 18:26:58', NULL),
(25, 'PRIMAVERA (Mediana)', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, espárragos, salame, jamón, chorizo, aceitunas verdes, pimentón, durazno.', 'A', '2018-05-15 23:16:06', NULL, '2018-05-18 18:26:58', NULL),
(26, 'PRIMAVERA (Familiar)', 'und', NULL, '39.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, espárragos, salame, jamón, chorizo, aceitunas verdes, pimentón, durazno.', 'A', '2018-05-15 23:16:09', NULL, '2018-05-18 18:26:58', NULL),
(27, 'PRIMAVERA (Extra)', 'und', NULL, '47.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, espárragos, salame, jamón, chorizo, aceitunas verdes, pimentón, durazno.', 'A', '2018-05-15 23:16:14', NULL, '2018-05-18 18:26:58', NULL),
(28, 'CUATRO ESTACIONES (Extra)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Salame, pimentón, tomate, champignones, jamón, aceitunas verdes.', 'A', '2018-05-15 23:17:18', NULL, '2018-05-18 18:26:58', NULL),
(29, 'CUATRO ESTACIONES (Familiar)', 'und', NULL, '39.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Salame, pimentón, tomate, champignones, jamón, aceitunas verdes.', 'A', '2018-05-15 23:17:20', NULL, '2018-05-18 18:26:58', NULL),
(30, 'CUATRO ESTACIONES (Mediana)', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Salame, pimentón, tomate, champignones, jamón, aceitunas verdes.', 'A', '2018-05-15 23:17:23', NULL, '2018-05-18 18:26:58', NULL),
(31, 'VEGETARIANA (Extra)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, espárragos, champignones, pimentón, aceitunas verdes.', 'A', '2018-05-15 23:18:42', NULL, '2018-05-18 18:26:58', NULL),
(32, 'VEGETARIANA (Familiar)', 'und', NULL, '39.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, espárragos, champignones, pimentón, aceitunas verdes.', 'A', '2018-05-15 23:18:44', NULL, '2018-05-18 18:26:58', NULL),
(33, 'VEGETARIANA (Mediana)', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, espárragos, champignones, pimentón, aceitunas verdes.', 'A', '2018-05-15 23:18:46', NULL, '2018-05-18 18:26:58', NULL),
(34, 'HAWAIANA BACÓN (Extra)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña, tocino.', 'A', '2018-05-15 23:19:52', NULL, '2018-05-18 18:26:58', NULL),
(35, 'HAWAIANA BACÓN (Familiar)', 'und', NULL, '39.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña, tocino.', 'A', '2018-05-15 23:19:54', NULL, '2018-05-18 18:26:58', NULL),
(36, 'HAWAIANA BACÓN (Mediana)', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña, tocino.', 'A', '2018-05-15 23:19:56', NULL, '2018-05-18 18:26:58', NULL),
(37, 'HAWAIANA (Extra)', 'und', NULL, '46.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña, durazno.', 'A', '2018-05-15 23:21:10', NULL, '2018-05-18 18:26:58', NULL),
(38, 'HAWAIANA (Familiar)', 'und', NULL, '39.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña, durazno.', 'A', '2018-05-15 23:21:12', NULL, '2018-05-18 18:26:58', NULL),
(39, 'HAWAIANA (Mediana)', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña, durazno.', 'A', '2018-05-15 23:21:14', NULL, '2018-05-18 18:26:58', NULL),
(40, 'POLLO TROPICAL (Mediana)', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, piña, durazno.', 'A', '2018-05-28 13:05:05', NULL, NULL, NULL),
(41, 'POLLO TROPICAL (Familiar)', 'und', NULL, '38.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, piña, durazno.', 'A', '2018-05-28 13:05:16', NULL, NULL, NULL),
(42, 'POLLO TROPICAL (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, piña, durazno.', 'A', '2018-05-28 13:05:16', NULL, NULL, NULL),
(43, 'BACÓN CHEEESE (Mediana)', 'und', NULL, '24.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tocino, piña y extra queso.', 'A', '2018-05-28 13:06:37', NULL, NULL, NULL),
(44, 'BACÓN CHEEESE (Familiar)', 'und', NULL, '36.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tocino, piña y extra queso.', 'A', '2018-05-28 13:06:38', NULL, NULL, NULL),
(45, 'BACÓN CHEEESE (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tocino, piña y extra queso.', 'A', '2018-05-28 13:06:40', NULL, NULL, NULL),
(46, 'VERONA (Mediana)', 'und', NULL, '24.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, jamón y chorizo.', 'A', '2018-05-28 13:07:48', NULL, NULL, NULL),
(47, 'VERONA (Familiar)', 'und', NULL, '36.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, jamón y chorizo.', 'A', '2018-05-28 13:07:50', NULL, NULL, NULL),
(48, 'VERONA (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, jamón y chorizo.', 'A', '2018-05-28 13:07:52', NULL, NULL, NULL),
(49, 'CORLEONE (Mediana)', 'und', NULL, '24.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, champignone y samale.', 'A', '2018-05-28 13:09:02', NULL, NULL, NULL),
(50, 'CORLEONE (Familiar)', 'und', NULL, '36.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, champignone y samale.', 'A', '2018-05-28 13:09:07', NULL, NULL, NULL),
(51, 'CORLEONE (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Pollo, champignone y samale.', 'A', '2018-05-28 13:09:08', NULL, NULL, NULL),
(52, 'AL CAPONE (Mediana)', 'und', NULL, '24.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'jamón, salame, pimentón y aceitunas.', 'A', '2018-05-28 13:10:29', NULL, NULL, NULL),
(53, 'AL CAPONE (Familiar)', 'und', NULL, '36.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'jamón, salame, pimentón y aceitunas.', 'A', '2018-05-28 13:10:30', NULL, NULL, NULL),
(54, 'AL CAPONE (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'jamón, salame, pimentón y aceitunas.', 'A', '2018-05-28 13:10:31', NULL, NULL, NULL),
(55, 'TROPICAL (Mediana)', 'und', NULL, '24.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña.', 'A', '2018-05-28 13:11:24', NULL, NULL, NULL),
(56, 'TROPICAL (Familiar)', 'und', NULL, '36.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña.', 'A', '2018-05-28 13:11:26', NULL, NULL, NULL),
(57, 'TROPICAL (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, piña.', 'A', '2018-05-28 13:11:27', NULL, NULL, NULL),
(58, 'FLORENTINA (Mediana)', 'und', NULL, '24.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, champignones, aceitunas verdes.', 'A', '2018-05-28 13:12:24', NULL, NULL, NULL),
(59, 'FLORENTINA (Familiar)', 'und', NULL, '36.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, champignones, aceitunas verdes.', 'A', '2018-05-28 13:12:27', NULL, NULL, NULL),
(60, 'FLORENTINA (Extra)', 'und', NULL, '45.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate, champignones, aceitunas verdes.', 'A', '2018-05-28 13:12:28', NULL, NULL, NULL),
(61, 'VENECIANA (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'champignones, espárragos, tocino.', 'A', '2018-05-28 13:13:59', NULL, NULL, NULL),
(62, 'VENECIANA (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'champignones, espárragos, tocino.', 'A', '2018-05-28 13:14:04', NULL, NULL, NULL),
(63, 'VENECIANA (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'champignones, espárragos, tocino.', 'A', '2018-05-28 13:14:05', NULL, NULL, NULL),
(64, 'NAPOLITANA (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate en rodajas, champignones y oregano.', 'A', '2018-05-28 13:15:04', NULL, NULL, NULL),
(65, 'NAPOLITANA (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate en rodajas, champignones y oregano.', 'A', '2018-05-28 13:15:06', NULL, NULL, NULL),
(66, 'NAPOLITANA (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Tomate en rodajas, champignones y oregano.', 'A', '2018-05-28 13:15:11', NULL, NULL, NULL),
(67, 'SICILIANA (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, aceitunas y pimentón', 'A', '2018-05-28 13:16:16', NULL, NULL, NULL),
(68, 'SICILIANA (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, aceitunas y pimentón', 'A', '2018-05-28 13:16:22', NULL, NULL, NULL),
(69, 'SICILIANA (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Champignones, aceitunas y pimentón.', 'A', '2018-05-28 13:16:23', NULL, NULL, NULL),
(70, 'ROMANA (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, espárragos y albahaca, ', 'A', '2018-05-28 13:17:39', NULL, NULL, NULL),
(71, 'ROMANA (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, espárragos y albahaca, ', 'A', '2018-05-28 13:17:40', NULL, NULL, NULL),
(72, 'ROMANA (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, tomate, espárragos y albahaca, ', 'A', '2018-05-28 13:17:41', NULL, NULL, NULL),
(73, 'CANCÚN (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, champignones y piña.', 'A', '2018-05-28 13:18:40', NULL, NULL, NULL),
(74, 'CANCÚN (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, champignones y piña.', 'A', '2018-05-28 13:18:41', NULL, NULL, NULL),
(75, 'CANCÚN (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón, champignones y piña.', 'A', '2018-05-28 13:18:52', NULL, NULL, NULL),
(76, 'AMERICANA (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón y tomate en rodajas.', 'A', '2018-05-28 13:19:47', NULL, NULL, NULL),
(77, 'AMERICANA (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón y tomate en rodajas.', 'A', '2018-05-28 13:19:51', NULL, NULL, NULL),
(78, 'AMERICANA (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Jamón y tomate en rodajas.', 'A', '2018-05-28 13:19:53', NULL, NULL, NULL),
(79, 'MARGARITA (Mediana)', 'und', NULL, '22.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Sólo queso mozzarella.', 'A', '2018-05-28 13:20:48', NULL, NULL, NULL),
(80, 'MARGARITA (Familiar)', 'und', NULL, '33.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Sólo queso mozzarella.', 'A', '2018-05-28 13:20:50', NULL, NULL, NULL),
(81, 'MARGARITA (Extra)', 'und', NULL, '42.00', 1, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL, 'Sólo queso mozzarella.', 'A', '2018-05-28 13:20:51', NULL, NULL, NULL),
(82, 'DE PEPPERONNI (Mediana)', 'und', NULL, '28.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:23:43', NULL, NULL, NULL),
(83, 'DE PEPPERONNI (Familiar)', 'und', NULL, '47.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:23:45', NULL, NULL, NULL),
(84, 'DE PEPPERONNI (Extra)', 'und', NULL, '55.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:23:46', NULL, NULL, NULL),
(85, 'DE TOCINO (Mediana)', 'und', NULL, '26.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:24:22', NULL, NULL, NULL),
(86, 'DE TOCINO (Familiar)', 'und', NULL, '43.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:24:23', NULL, NULL, NULL),
(87, 'DE TOCINO (Extra)', 'und', NULL, '50.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:24:28', NULL, NULL, NULL),
(88, 'DE CHORIZO (Mediana)', 'und', NULL, '26.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:25:14', NULL, NULL, NULL),
(89, 'DE CHORIZO (Familiar)', 'und', NULL, '43.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:25:15', NULL, NULL, NULL),
(90, 'DE CHORIZO (Extra)', 'und', NULL, '50.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:25:20', NULL, NULL, NULL),
(91, 'DE JAMÓN (Mediana)', 'und', NULL, '26.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:26:01', NULL, NULL, NULL),
(92, 'DE JAMÓN (Familiar)', 'und', NULL, '43.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:26:02', NULL, NULL, NULL),
(93, 'DE JAMÓN (Extra)', 'und', NULL, '50.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:26:09', NULL, NULL, NULL),
(94, 'DE SALAME (Mediana)', 'und', NULL, '26.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:26:54', NULL, NULL, NULL),
(95, 'DE SALAME (Familiar)', 'und', NULL, '43.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:26:55', NULL, NULL, NULL),
(96, 'DE SALAME (Extra)', 'und', NULL, '50.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:26:57', NULL, NULL, NULL),
(97, 'DE CHAMPIGNONES (Mediana)', 'und', NULL, '26.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:27:38', NULL, NULL, NULL),
(98, 'DE CHAMPIGNONES (Familiar)', 'und', NULL, '43.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:27:39', NULL, NULL, NULL),
(99, 'DE CHAMPIGNONES (Extra)', 'und', NULL, '50.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 13:27:40', NULL, NULL, NULL),
(100, 'DE ESPÁRRAGOS (Mediana)', 'und', NULL, '20.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:22:38', NULL, NULL, NULL),
(101, 'DE ESPÁRRAGOS (Familiar)', 'und', NULL, '30.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:22:40', NULL, NULL, NULL),
(102, 'DE ESPÁRRAGOS (Extra)', 'und', NULL, '40.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:22:41', NULL, NULL, NULL),
(103, 'DE TOMATE (Familiar)', 'und', NULL, '30.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:23:30', NULL, NULL, NULL),
(104, 'DE TOMATE (Mediana)', 'und', NULL, '20.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:23:30', NULL, NULL, NULL),
(105, 'DE TOMATE (Extra)', 'und', NULL, '40.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:23:30', NULL, NULL, NULL),
(106, 'DE PIÑA (Familiar)', 'und', NULL, '30.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:24:19', NULL, NULL, NULL),
(107, 'DE PIÑA (Mediana)', 'und', NULL, '20.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:24:19', NULL, NULL, NULL),
(108, 'DE PIÑA (Extra)', 'und', NULL, '40.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:24:19', NULL, NULL, NULL),
(109, 'DE ACEITUNAS (Mediana)', 'und', NULL, '20.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:24:57', NULL, NULL, NULL),
(110, 'DE ACEITUNAS (Familiar)', 'und', NULL, '30.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:25:06', NULL, NULL, NULL),
(111, 'DE ACEITUNAS (Extra)', 'und', NULL, '40.00', 1, 0, NULL, NULL, NULL, NULL, 2, NULL, NULL, '', 'A', '2018-05-28 14:25:08', NULL, NULL, NULL),
(112, 'QUESO (Mediano)', 'und', NULL, '3.50', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:10:05', NULL, NULL, NULL),
(113, 'QUESO (Familiar)', 'und', NULL, '5.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:10:06', NULL, NULL, NULL),
(114, 'QUESO (Extra)', 'und', NULL, '8.50', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:10:08', NULL, NULL, NULL),
(115, 'Embutidos (Mediano)', 'und', NULL, '3.50', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:12:44', NULL, NULL, NULL),
(116, 'Embutidos (Familiar)', 'und', NULL, '5.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:12:46', NULL, NULL, NULL),
(117, 'Embutidos (Extra)', 'und', NULL, '7.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:12:48', NULL, NULL, NULL),
(118, 'VEGETALES (Mediano)', 'und', NULL, '3.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:16:34', NULL, NULL, NULL),
(119, 'VEGETALES (Familiar)', 'und', NULL, '4.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:16:36', NULL, NULL, NULL),
(120, 'VEGETALES (Extra)', 'und', NULL, '6.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:16:37', NULL, NULL, NULL),
(121, 'PAN ESPECIAL (Unidad)', 'und', NULL, '1.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, 'Jamón, mozarella, oregano', 'A', '2018-05-30 08:17:52', NULL, NULL, NULL),
(122, 'Porcion de pan al ajo x 6 unidades', 'und', NULL, '4.00', 1, 0, NULL, NULL, NULL, NULL, 3, NULL, NULL, '', 'A', '2018-05-30 08:18:16', NULL, NULL, NULL),
(123, 'LASAGNA QUESO, JAMÓN Y CHAMPIGNONES.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:29', NULL, NULL, NULL),
(124, 'LASAGNA DE POLLO, CHAMPIGNONES Y TOCINO.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:31', NULL, NULL, NULL),
(125, 'LASAGNA A LA BOLOGNESA.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:34', NULL, NULL, NULL),
(126, 'RAVIOLES A LA BOLOGNESA.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:37', NULL, NULL, NULL),
(127, 'RAVIOLES EN SALSA DE CHAMPIGNONES.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:39', NULL, NULL, NULL),
(128, 'RAVIOLES A LO SANTORINI.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:41', NULL, NULL, NULL),
(129, 'CANELONES.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:43', NULL, NULL, NULL),
(130, 'SPAGUETTI A LA BOLOGNESA.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:24:45', NULL, NULL, NULL),
(131, 'SPAGUETTI A LO ALFREDO.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:25:51', NULL, NULL, NULL),
(132, 'SPAGUETTI EN SALSA DE CHAMPIGNONES.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:25:53', NULL, NULL, NULL),
(133, 'FETUCCINI EN SALSA DE CHAMPIGNONES.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:25:55', NULL, NULL, NULL),
(134, 'FETUCCINI A LOS ALFREDO.', 'und', NULL, '23.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:25:57', NULL, NULL, NULL),
(135, 'FETUCCINI MIXTO.', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:26:11', NULL, NULL, NULL),
(136, 'PAPPARDELLE A LO SANTORINI.', 'und', NULL, '25.00', 1, 0, NULL, NULL, NULL, NULL, 4, NULL, NULL, '', 'A', '2018-05-30 08:26:12', NULL, NULL, NULL),
(137, 'SANGRÍA SANTORINI (Copa)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino tinto, durazno, piña y manzana.', 'A', '2018-05-30 08:32:22', NULL, NULL, NULL),
(138, 'SANGRÍA SANTORINI (Media)', 'und', NULL, '22.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino tinto, durazno, piña y manzana.', 'A', '2018-05-30 08:32:26', NULL, NULL, NULL),
(139, 'SANGRÍA SANTORINI (Jarra)', 'und', NULL, '34.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino tinto, durazno, piña y manzana.', 'A', '2018-05-30 08:32:29', NULL, NULL, NULL),
(140, 'SANGRÍA ESPAÑOLA (Copa)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino tinto, durazno, zumo de naranja piña y manzana.', 'A', '2018-05-30 08:34:25', NULL, '2018-05-30 08:43:20', NULL),
(141, 'SANGRÍA ESPAÑOLA (Media)', 'und', NULL, '22.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino tinto, durazno, zumo de naranja piña y manzana.', 'A', '2018-05-30 08:34:29', NULL, NULL, NULL),
(142, 'SANGRÍA ESPAÑOLA (Jarra)', 'und', NULL, '34.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino tinto, durazno, zumo de naranja piña y manzana.', 'A', '2018-05-30 08:34:31', NULL, NULL, NULL),
(143, 'SANGRÍA HAWAIANA (Copa)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino blanco, durazno, piña y manzana.', 'A', '2018-05-30 08:35:48', NULL, NULL, NULL),
(144, 'SANGRÍA HAWAIANA (Media)', 'und', NULL, '22.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino blanco, durazno, piña y manzana.', 'A', '2018-05-30 08:35:50', NULL, NULL, NULL),
(145, 'SANGRÍA HAWAIANA (Jarra)', 'und', NULL, '34.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, 'Vino blanco, durazno, piña y manzana.', 'A', '2018-05-30 08:35:53', NULL, NULL, NULL),
(146, 'SANGRÍA SIMPLE (Copa)', 'und', NULL, '7.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, '', 'A', '2018-05-30 08:36:45', NULL, NULL, NULL),
(147, 'SANGRÍA SIMPLE (Media)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, '', 'A', '2018-05-30 08:36:46', NULL, NULL, NULL),
(148, 'SANGRÍA SIMPLE (Jarra)', 'und', NULL, '28.00', 0, 0, NULL, NULL, NULL, NULL, 5, NULL, NULL, '', 'A', '2018-05-30 08:36:48', NULL, NULL, NULL),
(149, 'NAVARRO CORREAS (Copa)', 'und', NULL, '12.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:39:18', NULL, NULL, NULL),
(150, 'NAVARRO CORREAS (Botella)', 'und', NULL, '42.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:39:19', NULL, '2018-05-30 09:18:15', NULL),
(151, 'CASILLERO DEL DIABLO (Copa)', 'und', NULL, '12.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:39:21', NULL, NULL, NULL),
(152, 'CASILLERO DEL DIABLO (Botella)', 'und', NULL, '42.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:39:22', NULL, '2018-05-30 09:18:15', NULL),
(153, 'FOND DE CAVE (Copa)', 'und', NULL, '12.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:39:24', NULL, NULL, NULL),
(154, 'FOND DE CAVE (Botella)', 'und', NULL, '42.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:39:28', NULL, '2018-05-30 09:18:15', NULL),
(155, 'BENJAMÍN (Copa)', 'und', NULL, '12.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:41:25', NULL, NULL, NULL),
(156, 'BENJAMÍN (Botella)', 'und', NULL, '42.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:41:27', NULL, '2018-05-30 09:18:15', NULL),
(157, 'FRONTERA (Copa)', 'und', NULL, '12.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:41:28', NULL, NULL, NULL),
(158, 'FRONTERA (Botella)', 'und', NULL, '42.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:41:29', NULL, '2018-05-30 09:18:15', NULL),
(159, 'TACAMA ROSÉ (Copa)', 'und', NULL, '12.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:41:31', NULL, NULL, NULL),
(160, 'TACAMA ROSÉ (Botella)', 'und', NULL, '35.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:41:33', NULL, '2018-05-30 09:18:15', NULL),
(161, 'TABERNERO BORGOÑA (Copa)', 'und', NULL, '11.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:42:42', NULL, '2018-05-30 09:19:08', NULL),
(162, 'TABERNERO BORGOÑA (Botella)', 'und', NULL, '35.00', 0, 0, NULL, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:42:45', NULL, '2018-05-30 08:43:12', NULL),
(163, 'Cover (Botella)', 'und', NULL, '10.00', 0, 1, 3, NULL, NULL, NULL, 6, NULL, NULL, '', 'A', '2018-05-30 08:42:48', NULL, '2018-05-30 09:18:15', NULL),
(164, 'COCTEL SANTORINI (Copa)', 'und', NULL, '10.00', 1, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:44:46', NULL, NULL, NULL),
(165, 'PISCO SOUR (Copa)', 'und', NULL, '10.00', 1, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:45:02', NULL, NULL, NULL),
(166, 'COCTEL ALGARROBINA (Copa)', 'und', NULL, '9.00', 1, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:45:22', NULL, NULL, NULL),
(167, 'CHILCANO DE PISCO (Copa)', 'und', NULL, '10.00', 1, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:45:39', NULL, NULL, NULL),
(168, 'ANIZ (Copa)', 'und', NULL, '11.00', 1, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:45:56', NULL, NULL, NULL),
(169, 'MENTA (Copa)', 'und', NULL, '7.00', 1, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:46:10', NULL, NULL, NULL),
(170, 'GIN CON GIN (Copa)', 'und', NULL, '11.00', 0, 0, NULL, NULL, NULL, NULL, 7, NULL, NULL, '', 'A', '2018-05-30 08:46:24', NULL, NULL, NULL),
(171, 'CERVEZA CUSQUEÑA DE TRIGO 330 ml ', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 8, NULL, NULL, '', 'A', '2018-05-30 08:47:17', NULL, NULL, NULL),
(172, 'CERVEZA CUSQUEÑA NEGRA 330 ml', 'und', NULL, '6.00', 0, 0, NULL, NULL, NULL, NULL, 8, NULL, NULL, '', 'A', '2018-05-30 08:47:47', NULL, NULL, NULL),
(173, 'GASEOSA PERSONAL 237ml', 'und', NULL, '3.00', 0, 1, 30, 8, 20, NULL, 8, NULL, NULL, '', 'A', '2018-05-30 08:48:04', NULL, '2018-05-30 09:15:52', NULL),
(174, 'GASEOSA DE LITRO', 'und', NULL, '7.00', 0, 1, 15, 5, 20, NULL, 8, NULL, NULL, '', 'A', '2018-05-30 08:48:16', NULL, '2018-05-30 09:15:32', NULL),
(175, 'CAGEOSA LITRO Y MEDIO ', 'und', NULL, '9.00', 0, 1, 6, 3, 10, NULL, 8, NULL, NULL, '', 'A', '2018-05-30 08:48:31', NULL, '2018-05-30 09:16:24', NULL),
(176, 'CHICHA MORADA (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:45', NULL, NULL, NULL),
(177, 'CHICHA MORADA (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:50', NULL, NULL, NULL),
(178, 'CHICHA MORADA (Jarra)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:52', NULL, NULL, NULL),
(179, 'DE DURAZNO (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:52', NULL, NULL, NULL),
(180, 'DE DURAZNO (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:53', NULL, NULL, NULL),
(181, 'DE DURAZNO (Jarra)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:54', NULL, NULL, NULL),
(182, 'DE PIÑA (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:51:59', NULL, NULL, NULL),
(183, 'DE PIÑA (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:52:00', NULL, NULL, NULL),
(184, 'DE PIÑA (Jarra)', 'und', NULL, '15.00', 1, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:52:03', NULL, NULL, NULL),
(185, 'LIMONADA (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:20', NULL, NULL, NULL),
(186, 'LIMONADA (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:21', NULL, NULL, NULL),
(187, 'LIMONADA (Jarra)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:22', NULL, NULL, NULL),
(188, 'LIMONADA FROZEN (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:22', NULL, NULL, NULL),
(189, 'LIMONADA FROZEN (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:23', NULL, NULL, NULL),
(190, 'LIMONADA FROZEN (Jarra)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:24', NULL, NULL, NULL),
(191, 'LIMONADA ROSA BLOG (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:25', NULL, NULL, NULL),
(192, 'LIMONADA ROSA BLOG (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:26', NULL, NULL, NULL),
(193, 'LIMONADA ROSA BLOG (Jarra)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:54:27', NULL, NULL, NULL),
(194, 'MARACUYA (Vaso)', 'und', NULL, '5.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:55:53', NULL, NULL, NULL),
(195, 'MARACUYA (Media)', 'und', NULL, '8.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:55:55', NULL, '2018-05-30 09:03:23', NULL),
(196, 'MARACUYA (Jarra)', 'und', NULL, '15.00', 0, 0, NULL, NULL, NULL, NULL, 9, NULL, NULL, '', 'A', '2018-05-30 08:55:57', NULL, '2018-05-30 09:03:19', NULL),
(197, 'CAFÉ PASADO (Vaso)', 'und', NULL, '4.00', 0, 0, NULL, NULL, NULL, NULL, 10, NULL, NULL, '', 'A', '2018-05-30 08:57:25', NULL, NULL, NULL),
(198, 'MANZANILLA (Vaso)', 'und', NULL, '3.00', 0, 0, NULL, NULL, NULL, NULL, 10, NULL, NULL, '', 'A', '2018-05-30 08:57:26', NULL, NULL, NULL),
(199, 'TÉ (Vaso)', 'und', NULL, '3.00', 0, 0, NULL, NULL, NULL, NULL, 10, NULL, NULL, '', 'A', '2018-05-30 08:57:28', NULL, NULL, NULL),
(200, 'ANIS (Vaso)', 'und', NULL, '3.00', 0, 0, NULL, NULL, NULL, NULL, 10, NULL, NULL, '', 'A', '2018-05-30 08:57:29', NULL, NULL, NULL),
(201, 'HIERBALUISA (Vaso)', 'und', NULL, '3.00', 0, 0, NULL, NULL, NULL, NULL, 10, NULL, NULL, '', 'A', '2018-05-30 08:57:31', NULL, NULL, NULL);


INSERT INTO insumo (insumo_id, insumo, unidad, precio_costo, stock, stock_minimo, stock_maximo, fecha_vencimiento, descripcion, estado, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 'SALAME', 'g', '0.0145', '6000.0000', '2000.0000', '10000.0000', NULL, '', 'A', '2018-05-15 23:39:03', NULL, NULL, NULL),
(2, 'CHAMPIGNONES', 'g', '0.0876', '12000.0000', '3000.0000', '5000.0000', NULL, '', 'A', '2018-05-15 23:39:33', NULL, NULL, NULL);





INSERT INTO acceso (acceso_id, rol_id, modulo_id, opcion_id, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 1, 1, NULL, '2018-05-25 18:37:32', NULL, NULL, NULL),
(2, 1, NULL, 2, '2018-05-25 18:37:32', NULL, NULL, NULL),
(3, 1, NULL, 1, '2018-05-25 18:37:32', NULL, NULL, NULL),
(4, 1, NULL, 3, '2018-05-25 18:37:33', NULL, NULL, NULL),
(5, 1, NULL, 4, '2018-05-25 18:37:33', NULL, NULL, NULL),
(6, 1, 2, NULL, '2018-05-25 18:37:34', NULL, NULL, NULL),
(7, 1, NULL, 5, '2018-05-25 18:37:34', NULL, NULL, NULL),
(8, 1, NULL, 7, '2018-05-25 18:37:35', NULL, NULL, NULL),
(9, 1, NULL, 8, '2018-05-25 18:37:35', NULL, NULL, NULL),
(10, 1, NULL, 6, '2018-05-25 18:37:36', NULL, NULL, NULL),
(11, 1, NULL, 9, '2018-05-25 18:37:36', NULL, NULL, NULL),
(12, 1, 3, NULL, '2018-05-25 18:37:39', NULL, NULL, NULL),
(13, 1, NULL, 10, '2018-05-25 18:37:39', NULL, NULL, NULL),
(14, 1, NULL, 11, '2018-05-25 18:37:39', NULL, NULL, NULL),
(15, 1, 4, NULL, '2018-05-25 18:37:40', NULL, NULL, NULL),
(16, 1, NULL, 12, '2018-05-25 18:37:41', NULL, NULL, NULL),
(17, 1, 5, NULL, '2018-05-25 18:37:41', NULL, NULL, NULL),
(18, 1, NULL, 13, '2018-05-25 18:37:41', NULL, NULL, NULL),
(19, 1, NULL, 14, '2018-05-25 18:37:42', NULL, NULL, NULL),
(20, 1, NULL, 15, '2018-05-25 18:37:43', NULL, NULL, NULL),
(21, 1, NULL, 16, '2018-05-25 18:37:43', NULL, NULL, NULL),
(22, 1, 6, NULL, '2018-05-25 18:37:43', NULL, NULL, NULL),
(24, 1, NULL, 17, '2018-05-25 18:37:46', NULL, NULL, NULL),
(42, 2, 5, NULL, '2018-06-06 22:48:02', NULL, NULL, NULL),
(26, 2, NULL, 13, '2018-05-25 18:38:10', NULL, NULL, NULL),
(27, 2, NULL, 14, '2018-05-25 18:38:11', NULL, NULL, NULL),
(28, 2, NULL, 15, '2018-05-25 18:38:11', NULL, NULL, NULL),
(29, 2, 6, NULL, '2018-05-25 18:38:12', NULL, NULL, NULL),
(41, 2, NULL, 16, '2018-06-06 22:48:02', NULL, NULL, NULL),
(40, 2, NULL, 17, '2018-06-06 22:48:01', NULL, NULL, NULL),
(44, 3, 3, NULL, '2018-06-06 22:48:16', NULL, NULL, NULL),
(58, 3, 4, NULL, '2018-06-23 00:40:08', NULL, NULL, NULL),
(43, 4, 4, NULL, '2018-06-06 22:48:08', NULL, NULL, NULL),
(54, 2, 4, NULL, '2018-06-23 00:39:54', NULL, NULL, NULL),
(52, 4, 5, NULL, '2018-06-23 00:38:02', NULL, NULL, NULL),
(38, 1, NULL, 18, '2018-05-30 09:25:50', NULL, NULL, NULL),
(45, 1, 7, NULL, '2018-06-13 10:53:20', NULL, NULL, NULL),
(46, 1, NULL, 19, '2018-06-13 10:53:20', NULL, NULL, NULL),
(47, 1, NULL, 21, '2018-06-13 10:53:21', NULL, NULL, NULL),
(48, 1, NULL, 20, '2018-06-13 10:53:22', NULL, NULL, NULL),
(49, 4, NULL, 12, '2018-06-23 00:37:59', NULL, NULL, NULL),
(50, 4, NULL, 13, '2018-06-23 00:37:59', NULL, NULL, NULL),
(53, 4, NULL, 14, '2018-06-23 00:38:03', NULL, NULL, NULL),
(55, 2, NULL, 12, '2018-06-23 00:39:54', NULL, NULL, NULL),
(56, 3, NULL, 10, '2018-06-23 00:40:06', NULL, NULL, NULL),
(57, 3, NULL, 11, '2018-06-23 00:40:06', NULL, NULL, NULL),
(59, 3, NULL, 12, '2018-06-23 00:40:08', NULL, NULL, NULL),

(60, 1, 8, NULL, '2018-05-25 18:37:32', NULL, NULL, NULL),
(61, 1, NULL, 22, '2018-05-25 18:37:32', NULL, NULL, NULL),
(62, 1, NULL, 23, '2018-05-25 18:37:32', NULL, NULL, NULL),
(63, 1, NULL, 24, '2018-05-25 18:37:32', NULL, NULL, NULL);














/*SEGUNDA ETAPA EN SANTORINI - PAGO DE PERSONAL*/
/*TRABAJAREMOS CON ESQUEMAS*/

insert into modulo(modulo_id,modulo,icono,prioridad,para_seleccionar,estado,tipo_sistema) values
(9,'Planilla','<i class="menu-icon fa fa-users"></i>',11,'planilla','A', 'ADMI'),
(10,'Contabilidad','<i class="menu-icon fa fa-money"></i>',12,'contabilidad','A','ADMI');
	
    /**
			select* from multitabla
    **/
insert into opcion (opcion_id,modulo_id,opcion, icono, prioridad, controlador,accion,para_seleccionar,estado,tipo_sistema)values
(26,9,'Empleados','',5,'empleados','index','empleados','A','ADMI'),
(27,9,'Liquidaciones','',4,'liquidaciones','index','liquidaciones','A', 'ADMI'),
(28,9,'Nueva liquidación','',3,'liquidaciones','nueva','nueva_liquidacion','A', 'ADMI'),
(29,9,'Cumpleaños','',2,'empleados','cumpleanios','cumpleanios','A', 'ADMI'),
(30,10,'Programar pagos','',3,'pagos_programados','nuevo','pagos_programados','A', 'ADMI');



insert into tipo_multitabla(tipo_multitabla_id, tipo_multitabla) values
	(8,'TIPO DE DOCUMENTO - PERSONA NATURAL'),
	(9,'SEXO'),
	(10,'TIPO CONTRATO DE PERSONAL'),
	(11,'CUENTAS BANCARIAS'),
	(12,'TIPO DE CONCEPTO EN PLANILLA'),
	(13,'MOTIVO PAGO DE PERSONAL'),
    (14,'ESTADO DE CRONOGRAMA PAGO'),
    (15,'MODO DE PAGO'),
    (16,'TIPO DE SISTEMA');

insert into multitabla(multitabla_id, tipo_multitabla_id, valor, descripcion) values
	(22,8,'DNI','DOCUMENTO NACIONAL DE IDENTIDAD'),
	(23,9,'F','FEMENINO'),
    (24,9,'M','MASCULINO'),
    (25,10,'RH','RECIBO POR HONORARIOS'),
    (26,10,'CAS','CONTRATO ANUAL DE SERVICIOS'),
    (27,10,'OTR','OTRO'),
    (28,11,'BCP','BCP - CTA. AHORROS'),
    (29,11,'BBVA','BBVA CONTINENTAL'),
    (30,12,'REM','REMUNERACIONES'),
    (31,12,'DES','DESCUENTOS'),
    (32,13,'SEM','PAGO SEMANAL'),
    (33,13,'MEN','PAGO MENSUAL'),
    (34,13,'ADE','ADELANTO'),
    (35,14,'PEN','PENDIENTE'),
    (36,14,'PAG','PAGADO'),
    (37,14,'POS','POSTERGADO'),
    (38,14,'X','ANULADO'),
    (39,15,'E','EFECTIVO'),
    (40,15,'T','TARJETA'),
    (41,16,'PROD','PRODUCCIÓN'),
    (42,16,'ADMI','ADMINISTRACION');
    
    

INSERT INTO sede (sede_id, sede, ubigeo, direccion, telefono_1, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 'LAS QUINTANAS - TRUJILLO', NULL, NULL, NULL, '2018-08-02 19:14:25', NULL, '2018-08-05 23:10:29', NULL),
(2, 'PRIMAVERA - TRUJILLO', NULL, NULL, NULL, '2018-08-02 19:14:25', NULL, NULL, NULL);

INSERT INTO cargo (cargo_id, cargo, estado, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 'ADMINISTRADOR', 'A', '2018-08-06 20:43:50', NULL, NULL, NULL),
(2, 'SUPERVISOR', 'A', '2018-08-06 20:44:15', NULL, NULL, NULL),
(3, 'CAJERO', 'A', '2018-08-06 20:44:15', NULL, NULL, NULL),
(4, 'MOZO', 'A', '2018-08-06 20:44:15', NULL, NULL, NULL),
(5, 'COCINERO', 'A', '2018-08-06 20:44:15', NULL, NULL, NULL);


INSERT INTO empleado (empleado_id, tipo_documento, nro_documento, nombres, ape_paterno, ape_materno, nombre_completo, datos_reniec, fecha_nacimiento, sexo, foto, telefono_1, telefono_2, correo, ubigeo, direccion, sede_id, tipo_contrato, cargo_id, fecha_ingreso, fecha_salida, sueldo_base_mensual, sueldo_base_semanal, cuenta_bancaria, nro_cuenta, estado, observaciones, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 'DNI', '26957220', 'SANTIAGO', 'CARRION', 'CRUZ', 'SANTIAGO CARRION CRUZ', 0, '2018-08-06', 'M', NULL, '', NULL, '', NULL, 'LA LIBERTAD/TRUJILLO/TRUJILLO - AV. CARLOS VALDERRAMA 575 URB. LAS QUINTANAS', 1, 'OTR', 1, NULL, NULL, NULL, NULL, NULL, '', 'A', '', '2018-08-06 20:36:42', NULL, NULL, NULL),
(2, 'DNI', '71004300', 'LUCIA ANGELICA', 'QUEVEDO', 'GOMEZ', 'LUCIA ANGELICA QUEVEDO GOMEZ', 0, '1999-08-13', 'F', NULL, '991419312', NULL, 'luciangelicaqg@hotmail.com', NULL, 'LA LIBERTAD/TRUJILLO/HUANCHACO - CILLA HUANCHACO MZ 4 LT 21', 1, 'OTR', 3, NULL, NULL, NULL, NULL, NULL, '', 'A', '', '2018-08-06 20:34:38', NULL, '2018-08-06 20:44:41', NULL),
(3, 'DNI', '74235162', 'ROMARIO', 'DOMINGUEZ', 'GOMEZ', 'ROMARIO DOMINGUEZ GOMEZ', 0, '2018-08-06', 'M', NULL, '', NULL, '', NULL, 'LA LIBERTAD/TRUJILLO/LA ESPERANZA - CALLE MAC GREGOR 245', 1, 'OTR', 3, NULL, NULL, NULL, NULL, NULL, '', 'A', '', '2018-08-06 20:35:51', NULL, '2018-08-06 20:44:43', NULL),
(4, 'DNI', '73274533', 'VICTOR ENRIQUE', 'MANTILLA', 'CHAVEZ', 'VICTOR ENRIQUE MANTILLA CHAVEZ', 0, '2018-08-06', 'M', NULL, '', NULL, '', NULL, 'LA LIBERTAD/TRUJILLO/TRUJILLO - URB. MONSERRATE ETAPA 5TA MZ. A-2 LT. 10', 1, 'OTR', 2, NULL, NULL, NULL, NULL, NULL, '', 'A', '', '2018-08-06 20:37:15', NULL, '2018-08-06 20:44:32', NULL),
(5, 'DNI', '74597952', 'DANNA JACKELINE', 'TRUJILLO', 'SANTISTEBAN', 'DANNA JACKELINE TRUJILLO SANTISTEBAN', 0, '2018-08-06', 'F', NULL, '', NULL, '', NULL, 'LA LIBERTAD/TRUJILLO/TRUJILLO - CALLE ALEJANDRIA MZ. H LT. 6 URB. TRUPAL', 2, 'OTR', 3, NULL, NULL, NULL, NULL, NULL, '', 'A', '', '2018-08-06 20:48:46', NULL, NULL, NULL),
(6, 'DNI', '73873148', 'LUIS MIGUEL', 'MEDINA', 'DELGADO', 'LUIS MIGUEL MEDINA DELGADO', 0, '2018-08-06', 'M', NULL, '', NULL, '', NULL, 'CAJAMARCA/CUTERVO/SANTO TOMAS - VIA MARAÑON S/N', 2, 'OTR', 3, NULL, NULL, NULL, NULL, NULL, '', 'A', '', '2018-08-06 20:49:53', NULL, NULL, NULL);

INSERT INTO concepto (concepto_id, concepto, tipo_concepto, signo, estado, fecha_registro, registrado_por, fecha_modificacion, modificado_por) VALUES
(1, 'SUELDO BASE', 'REM', 1, 'A', '2018-08-09 21:19:22', NULL, NULL, NULL),
(2, 'GRATIFICACIÓN', 'REM', 1, 'A', '2018-08-09 21:19:22', NULL, NULL, NULL),
(3, 'HORAS EXTRA', 'REM', 1, 'A', '2018-08-09 21:19:22', NULL, NULL, NULL),
(4, 'POR FALTAS', 'DES', -1, 'A', '2018-08-09 21:19:22', NULL, NULL, NULL);




