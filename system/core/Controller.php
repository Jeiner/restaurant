<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		
		// Modelos por defecto
		$this->load->model('Site');
        $this->load->model('Validaciones_model');
        $this->load->model('Variables_model');
		// $this->load->model('Modulo_model');
		// $this->load->model('Opcion_model');
		log_message('info', 'Controller Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}
	function page_construct($page, $meta = array(), $data = array()) {
		//Verifica la session en todas las cargas de vista, pero no las de ajax.
		$this->loaders->verifica_sesion();
		$meta['modulos'] = $this->session->userdata('session_mod_access');
		$meta['opciones'] = $this->session->userdata('session_opc_access');

		// Datos
		$meta['success'] = isset($data['success']) ? $data['success'] : $this->session->flashdata('success');
        $meta['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['oGeneralidades'] = ($this->Site->get_generalidades())[0];
        if ($meta['oGeneralidades']->tipo_sistema == null || trim($meta['oGeneralidades']->tipo_sistema) == "" ) {
        	echo "SANTORINI <br>";
        	echo "No se especificó la modalidad del sistema. Administración o producción local.";
        	return false;
        }else{
        	if (trim($meta['oGeneralidades']->tipo_sistema) == 'PROD' || trim($meta['oGeneralidades']->tipo_sistema) == 'ADMI') {
        		$this->session->set_userdata('session_tipo_sistema',$meta['oGeneralidades']->tipo_sistema);
        	}else{
        		echo "SANTORINI <br>";
        		echo "La modalidad espeficicada del sistema no es válido.";
        		return false;
        	}
        }
        $meta['total_productos_por_agotar'] = $this->Site->total_productos_por_agotar();
        $meta['total_insumos_por_agotar'] = $this->Site->total_insumos_por_agotar();
        $meta['proximos_cumples'] = $this->Site->proximos_cumples();
        
        //Variables de 
        $meta['session_usuario'] = $this->session->userdata('session_usuario');
        $data['session_usuario'] = $this->session->userdata('session_usuario');
        $meta['session_nombre_completo'] = $this->session->userdata('session_nombre_completo');
        $data['session_nombre_completo'] = $this->session->userdata('session_nombre_completo');
        // $meta['info'] = $this->site->getNotifications();

        //Obtener variables
        $lstVariables = $this->Validaciones_model->obtener_variables_array_nombre();
        $meta['lstVariables'] = $lstVariables;
        $data['igv'] = $this->obtener_igv();

        //Mostrar vista
        $this->load->view('general/header', $meta);
        $this->load->view( $page, $data);
        $this->load->view('general/footer');
    }
    function partial_view($page, $data = array()){
    	// Devulve un array con success = false y un mensaje  si no hay sesion
    	$this->loaders->verifica_sesion_ajax();
    	// 
    	if(isset($data['success'])){
    		if(!$data['success']){
    			echo json_encode($data); exit();
    		}
    	}
    	// Cargar la vista u objeto
		$this->load->view($page,$data);
    }
    function partial_view_sin_sesion($page, $data = array()){
        // Devulve un array con success = false y un mensaje  si no hay sesion
        // $this->loaders->verifica_sesion_ajax();
        // 
        if(isset($data['success'])){
            if(!$data['success']){
                echo json_encode($data); exit();
            }
        }
        // Cargar la vista u objeto
        $this->load->view($page,$data);
    }
    function get_database($db_name){
    	if($db_name == "quintanas"){
    		
    	}
    	$db = $this->load->database('sant_quin', TRUE);
    }

    function obtener_igv(){
        $igv = 0;
        $valor_IGV = $this->obtener_variable(NOMBRE_VAR_IGV);
        if($valor_IGV != null && $valor_IGV != ""){
            $igv = floatval($valor_IGV);
        }

        return $igv;
    }

    function obtener_variable($nombre_variable){
        $buscarVariable = $this->Variables_model->get_by_nombre_variable($nombre_variable);
        $oVariable = null;

        if(count($buscarVariable) >= 1){
            $oVariable = $buscarVariable[0];
        }

        if($oVariable == null){
            return "";
        }else{
            return $oVariable->valor_variable;
        }
    }

    //Métodos para enviar a la nube
    function set_session_enviar_comanda_nube($comanda_id){
        if(!$this->validar_si_migra_info_a_nube()){
            return;
        }
        $this->session->set_userdata('ses_enviar_comanda_a_nube',true);
        $this->session->set_userdata('ses_enviar_comanda_a_nube_comanda_id',$comanda_id);
    }

    function set_session_enviar_comprobante_nube($cabecera_id){
        if(!$this->validar_si_migra_info_a_nube()){
            return;
        }
        $this->session->set_userdata('ses_enviar_comprobante_a_nube',true);
        $this->session->set_userdata('ses_enviar_comprobante_a_nube_cabecera_id',$cabecera_id);
    }

    function set_session_enviar_pago_nube($pago_id){
        if(!$this->validar_si_migra_info_a_nube()){
            return;
        }
        $this->session->set_userdata('ses_enviar_pago_a_nube',true);
        $this->session->set_userdata('ses_enviar_pago_a_nube_pago_id',$pago_id);
    }

    function set_session_enviar_caja_nube($caja_id){
        if(!$this->validar_si_migra_info_a_nube()){
            return;
        }
        $this->session->set_userdata('ses_enviar_caja_a_nube',true);
        $this->session->set_userdata('ses_enviar_caja_a_nube_caja_id',$caja_id);
    }

    function validar_si_migra_info_a_nube(){
        $migrar_a_nube = $this->obtener_variable(MIGRAR_INFO_A_NUBE);
        $migrar_a_nube = ($migrar_a_nube || $migrar_a_nube == 1)? true : false;
        return $migrar_a_nube;
    }

    public function obtenerAbreviaturaMes($mes){
        $lstMeses = array(
            '1'=>'ENE',
            '2'=>'FEB',
            '3'=>'MAR',
            '4'=>'ABR',
            '5'=>'MAY',
            '6'=>'JUN',
            '7'=>'JUL',
            '8'=>'AGO',
            '9'=>'SET',
            '10'=>'OCT',
            '11'=>'NOV',
            '12'=>'DIC'
        );

        return $lstMeses[intval($mes)];
    }

    public function obtenerNombreMes($mes){
        $lstMeses = array(
            '1'=>'ENERO',
            '2'=>'FEBRERO',
            '3'=>'MARZO',
            '4'=>'ABRIL',
            '5'=>'MAYO',
            '6'=>'JUNIO',
            '7'=>'JULIO',
            '8'=>'AGOSTO',
            '9'=>'SETIEMBRE',
            '10'=>'OCTUBRE',
            '11'=>'NOVIEMBRE',
            '12'=>'DICICIEMBRE'
        );

        return $lstMeses[intval($mes)];
    }

    public function obtenerDescDia($cDia){
        $days_dias = array(
            'Monday'=>'LUN',
            'Tuesday'=>'MAR',
            'Wednesday'=>'MIE',
            'Thursday'=>'JUE',
            'Friday'=>'VIE',
            'Saturday'=>'SAB',
            'Sunday'=>'DOM'
        );
        return $days_dias[$cDia];
    }

    public function get_calendario_mensual($cMes, $cAnio, $cDiaPago = null){
        $oCalendario = new stdClass();
        $oCalendario->cMesDesc = $this->obtenerNombreMes(intval($cMes));
        $oCalendario->cMes = $cMes;
        $oCalendario->cAnio = $cAnio;
        $oCalendario->nNumeroDias = cal_days_in_month(CAL_GREGORIAN, $cMes, $cAnio);
        $oCalendario->nNumeroDiaActual = intval(date('d'));
        $oCalendario->dFechaInicio = date('Y-m-d', strtotime($cAnio.'-'.$cMes.'-'.'01'));
        $oCalendario->dFechaFin = date('Y-m-d', strtotime($cAnio.'-'.$cMes.'-'.$oCalendario->nNumeroDias));
        $oCalendario->dFechaActual = date('Y-m-d');
        $oCalendario->oPeriodoPago = null;

        //Obtener días de mes
        $arrayDiasDeMes = [];
        for ($i=0; $i < $oCalendario->nNumeroDias; $i++) { 
            $oDia = new stdClass();
            $nDia = $i + 1;
            $oDia->dFecha = date('Y-m-d', strtotime($cAnio.'-'.$cMes.'-'.$nDia));
            $oDia->cMes = $cMes;
            $oDia->cAnio = $cAnio;
            $oDia->cDia = date('d', strtotime($oDia->dFecha));
            $oDia->cDiaDesc = $this->obtenerDescDia(date('l', strtotime($oDia->dFecha)));

            $arrayDiasDeMes[] = $oDia;
        }
        $oCalendario->lstDias = $arrayDiasDeMes;

        //Asignar periodo de pago
        $oPeriodoPago = new stdClass();
        if($cDiaPago != null){
            $oPeriodoPago->cDiaPago = $cDiaPago;
            $oPeriodoPago->dDiaPago = date('Y-m-d', strtotime($cAnio.'-'.$cMes.'-'.$cDiaPago));
            if(intval($oCalendario->nNumeroDias) < intval($cDiaPago)){
                $oPeriodoPago->cDiaPago = date('d', strtotime($oCalendario->dFechaFin));
                $oPeriodoPago->dDiaPago = date('Y-m-d', strtotime($cAnio.'-'.$cMes.'-'.$oPeriodoPago->cDiaPago));
            }
            $oPeriodoPago->dFechaInicio = date('Y-m-d', strtotime($oPeriodoPago->dDiaPago . ' -1 month'. ' +1 day'));
            $oPeriodoPago->dFechaFin = date('Y-m-d', strtotime($oPeriodoPago->dDiaPago));
        }

        $oCalendario->oPeriodoPago = $oPeriodoPago;
        return $oCalendario;
    }

}
