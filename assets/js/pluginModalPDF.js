(function(a){
	a.createModal=function(b){
	defaults={
		title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false,fichaLabID:""
	}
	;var b=a.extend({}
	,defaults,b);var c=(b.scrollable===true)? 'style="max-height: 560px;overflow-y: auto;"':"";
	html='<div class="modal fade" id="myModal">\
			<div class="modal-dialog modal-lg" >\
	  			<div class="modal-content">\
	  			<a href="javascript:;" class=" btn btn-xs btn-inverse display" data-dismiss="modal" aria-hidden="true" style="margin-left:100%;position:absolute;border:3px solid #fff;">\
					<i class="ace-icon fa fa-times bigger-130"></i>\
				</a>';
	html+='<div class="modal-body" style="height:600px!important;" '+c+">";
	html+=b.message;
	html+="</div>";
	
	html+="</div>";
	html+="</div>";
	html+="</div>";
	a("body").prepend(html);
	a("#myModal").modal().on("hidden.bs.modal",function(){
		a(this).remove()}
	)}
}
)(jQuery);